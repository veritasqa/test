package com.parts.viewability;

import java.util.ArrayList;
import java.util.List;

//import org.openqa.selenium.By;

public class ObjectRepository extends ValueRepository{
	
	
	List<String> Part = new ArrayList<String>() ;
	List<String> Action = new ArrayList<String>() ;
	List<String> Results = new ArrayList<String>() ;
	public int Total_parts	 ;
	
	 // locator type 
	
 	public static String locatorType ="xpath";
 	
 	//Login Page
 	
 	public static final String UserName_Path = "//*[@id='txtUserName']"; 
 	public static final String Password_Path = "//*[@id='txtPassword']";
 	public static final String Login_Btn_Path = "//*[@id='btnSubmit']";
 	
 	public static final String Logout_Btn_Path = ".//*[@id='lbnLogout']";
 	
 	public static final String Materials_Btn_Path = ".//*[@id='ctl00_Navigation_divLiList']/li[2]/a/span";
 	public static final String Admin_Btn_Path = ".//*[@id='Navigation_divLiList']/li[4]/a/span";
 	
 	public static final String Manage_Inventory_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[7]/a/span";
 	public static final String Form_Number_Path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
 	public static final String Search_Btn_Path = ".//*[@id='cphContent_btnSearch']";
 	public static final String Edit_Btn_Path = ".//*[@id='ctl00_cphContent_Results_ctl00_ctl04_lnkEdit']";
 	
 	// Parameters For ParentChild Window (Find on COmmon Methods)
 //	List<String> ID = new ArrayList<String>() ;
 	public String Window_Handle = "";
 	
 	public static final String Viewability_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Arrow']";
 	public static final String Viewability_dropdown = ".//*[@id='ctl00_cphContent_ddlViewability_Input']";
 	
 	public static final String Viewability_input  = ".//*[@id='ctl00_cphContent_ddlViewability_Input']";

  
 	public static final String Admin_Only_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[2]";
 	
	public static final String Not_Viewable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[5]";
 	
	public static final String Orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[4]";

 	public static final String Viewable_not_orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[3]";

 
 	
 	public static final String Save_Btn_Path = ".//*[@id='cphContent_btnSave']";
 	public static final String Ok_Btn_Path = ".//*[@id='cphContent_NavigationBar_btnOk']";
 	
 	public static final String Clear_Btn_Path = ".//*[@id='ctl00_cphContent_btnClear']";
 	
}
