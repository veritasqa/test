
package com.parts.viewability;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class PartsView extends CommonMethods {

	@Test

	public void Partsview() throws InterruptedException, IOException {

		// // Initialization for Text File
		// StringBuilder appender = new StringBuilder();
		// String seperator = System.getProperty("line.separator");
		// appender.append("Test Report
		// ").append(seperator).append("------------").append(seperator);

		for (int i = 0; i < Total_parts; i++) {
			Implicit_Wait(driver);
			String Parts = Part.get(i).toString().trim();
			String Activity = Action.get(i).toString().trim();

			// On main Page
			Hover_Over_Element(locatorType, Admin_Btn_Path, driver);
			// Thread.sleep(500);
			Click_On_Element(locatorType, Manage_Inventory_Path, driver);
			// On Inventory page
			Clear_Type_Charecters(locatorType, Form_Number_Path, Parts, driver);

			Click_On_Element(locatorType, Search_Btn_Path, driver);

			// to check If there is no part available
			if (Is_Element_Present(locatorType, Edit_Btn_Path, driver))

			{

				Click_On_Element(locatorType, Edit_Btn_Path, driver);

				// appender.append(seperator).append("The Part:
				// ").append(Parts).append(" Available And ");

				if (Activity.equalsIgnoreCase("Skip")) {

					// appender.append(seperator).append("Part
					// ").append(Parts).append(" Is skipped").append(seperator);
					Results.add("Skipped");
				}

				else {

					// Excel reading logic for both viewable or orderable

					do {
						if (Activity.equalsIgnoreCase("Not Viewable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Not_Viewable_Path, driver);

						}

						else if (Activity.equalsIgnoreCase("Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Viewable Not Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Viewable_not_orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Admin Only"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Admin_Only_Path, driver);

						} else if (Activity.isEmpty())

						{
							break;
						} else

						{
							Results.add(
									"Take look at the second column in the excel sheet whether it contains correct action?");

						}

						// Saving the settings
						Click_On_Element(locatorType, Save_Btn_Path, driver);
						Click_On_Element(locatorType, Ok_Btn_Path, driver);

						newview = Get_Attribute(locatorType, Viewability_input, Attribute_Value, driver);

					} while (!Activity.equalsIgnoreCase(newview));

					// appender.append(" Successfully made the Part:
					// ").append(Parts).append(" " + newview)
					// .append(seperator);
					Results.add(newview);

				}

			}

			else {
				// appender.append(seperator).append("Part
				// ").append(Parts).append(" Is Not
				// Available").append(seperator);
				Results.add("Not Available");

			}

		}
		// OutPut Writer
		// if (appender.length() != 0) {
		// System.out.println("Preparing Test Report ......" + seperator);
		// System.out.println("Test Report Is Ready");
		// }

	}

	@BeforeSuite
	public void beforeClass() throws IOException, InterruptedException {

		Implicit_Wait(driver);
		Open_Browser(Browser, Browserpath);
		Open_Url_Window_Max(URL, driver);
		Clear_Type_Charecters(locatorType, UserName_Path, UserName, driver);
		Clear_Type_Charecters(locatorType, Password_Path, Password, driver);
		Click_On_Element(locatorType, Login_Btn_Path, driver);
		// Thread.sleep(500);
		Part = getexcel(Excel_File_Path);
		Action = getexcelcol2(Excel_File_Path);
		Total_parts = Part.size();
		System.out.println(Total_parts);
	}

	@AfterSuite
	public void afterClass() throws IOException {
		Writeexcel();
		Click_On_Element(locatorType, Logout_Btn_Path, driver);
		Close_Browser(driver);
		Quit_Browser(driver);

	}

}
