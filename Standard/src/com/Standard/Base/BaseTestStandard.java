package com.Standard.Base;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.Standard.Methods.CommonMethodsStandard;

public class BaseTestStandard extends CommonMethodsStandard {

	@BeforeSuite
	public void openbrowser() throws IOException, InterruptedException {

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		Implicit_Wait();
	}

	@AfterSuite
	public void Aftersuite() throws InterruptedException {

		if (OrderNumberlist.isEmpty() == false) {

			Cancelallorders();
		}
	}

	public void login(String Username, String Password) throws IOException, InterruptedException {

		if (Element_Is_Present(ID, Std_Login_UserName_ID)) {
			Click(Xpath, Std_Login_AdminloginBTN_Path);
			Type(ID, Std_Login_UserName_ID, Username);
			Type(ID, Std_Login_Password_ID, Password);
			Click(ID, Std_Login_LoginBtn_ID);
			Assert.assertTrue(Element_Is_Present(ID, Std_LP_Logout_ID));
			Implicit_Wait();
		}

		else {
			logout();
			Type(ID, Std_Login_UserName_ID, Username);
			Type(ID, Std_Login_Password_ID, Password);
			Click(ID, Std_Login_LoginBtn_ID);
			Assert.assertTrue(Element_Is_Present(ID, Std_Login_LoginBtn_ID));
			Implicit_Wait();
		}

	}

	public void logout() throws IOException, InterruptedException {

		Click(Xpath, Std_LP_Home_path);
		Click(ID, Std_LP_Logout_ID);
		//ExplicitWait_Element_Clickable(ID, Std_Login_LoginBtn_ID);

	}

	public void FulfilmentSearch(String Part) throws InterruptedException {
		Type(ID, Std_FulfilmentSearchField_ID, Part);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);

	}

	public void FulfilmentSearchWithoutGroupSearch(String Part) throws InterruptedException {
		Type(ID, Std_FulfilmentSearchField_ID, Part);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_SR_SearchMaterialsBy_Btn_ID);

	}

	public void GroupSearch(String Groupname) throws InterruptedException {
		Type(ID, Std_GS_Groupname_ID, Groupname);
		Click(ID, Std_GS_SearchButton_ID);
		ExplicitWait_Element_Visible(Xpath, Std_GroupSelect(Groupname));
		Click(Xpath, Std_GroupSelect(Groupname));
	}

	public void Datepicker2(String CalXpath, String Cal_locvalue, String CalLocatormonthtype, String CalMonth,
			String Get_Month_MMM, String Get_Date_d, String Get_Year_YYYY) throws InterruptedException {

		Click(CalXpath, Cal_locvalue);
		Wait_ajax();
		Click(CalLocatormonthtype, CalMonth);
		Click(Xpath, ".//a[text()='" + Get_Month_MMM + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Year_YYYY + "']");
		Wait_ajax();
		Click(Xpath, ".//a[text()='" + Get_Year_YYYY + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//input[@value='OK']");
		Click(Xpath, ".//input[@value='OK']");
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Date_d + "']");
		Click(Xpath, ".//a[text()='" + Get_Date_d + "']");
	}

	public void Datepicker(String Month, String Ok_Btn) throws InterruptedException {

		Click(ID, Month);
		Click(Xpath, ".//a[text()='" + Get_Futuredate("MMM") + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Ok_Btn);
		Click(ID, Ok_Btn);
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
		Click(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
	}

	public void DatepickerTodaysDate(String MonthLocator, String Month, String Ok_Btn) throws InterruptedException {

		Click(MonthLocator, Month);
		Click(Xpath, ".//a[text()='" + Get_Todaydate("MMM") + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='" + Get_Todaydate("YYYY") + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Ok_Btn);
		Click(ID, Ok_Btn);
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
		Click(Xpath, ".//a[text()='" + Get_Todaydate("d") + "']");
	}

	public void DatepickerPastDate(String MonthLocator, String Month, String Ok_Btn) throws InterruptedException {

		Click(MonthLocator, Month);
		Click(Xpath, ".//a[text()='" + Get_Pastdate("MMM") + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='" + Get_Pastdate("YYYY") + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Ok_Btn);
		Click(ID, Ok_Btn);
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
		Click(Xpath, ".//a[text()='" + Get_Pastdate("d") + "']");
	}

	public String Radioxpathfromlabel(String Text) {

		return "//*[@id = '" + Get_Attribute(Xpath, "//*[text()='" + Text + "']", "for") + "']";

	}

	public void Selectfilter(String Filter, String Filteroption) throws InterruptedException {

		Click(Xpath, Filter);
		Wait_ajax();
		Click(Xpath, ".//span[text()='" + Filteroption + "']");
		Wait_ajax();

	}

	public void Deletecarts() throws InterruptedException {

		if (Element_Is_Displayed(Xpath, Containstextpath("span", "My Cart ("))) {
			Click(Xpath, Containstextpath("span", "My Cart"));
			while (Element_Is_Displayed(Xpath, Std_SCP_Deletepart_path)) {
				Click(Xpath, Std_SCP_Deletepart_path);
				Wait_ajax();
				Click(Xpath, Textpath("a", "Yes"));
				Wait_ajax();

			}
			Click(Xpath, Std_LP_Home_path);
		}
	}

	public void Clear() throws InterruptedException {

		if (Element_Is_Displayed(Xpath, Textpath("a", "Clear"))) {
			Click(Xpath, Textpath("a", "Clear"));
			Wait_ajax();
		}

	}

	public void ClickHome() throws InterruptedException {

		if (Element_Is_Displayed(Xpath, Std_LP_Home_path)) {
			Click(Xpath, Std_LP_Home_path);
			Wait_ajax();
		}

	}

	public void Cancelallorders() throws InterruptedException {

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		for (String Onum : OrderNumberlist) {

			Click(Xpath, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
			Type(Xpath, Inventory_Ordernumber_Path, Onum);
			Click(Xpath, Inventory_Search_Btn_Path);
			Click(Xpath, Inventory_Edit_Btn_Path);

			if (Get_Attribute(Xpath, Inventory_Order_Status, "Value").equalsIgnoreCase("NEW")) {

				Click(Xpath, Inventory_Cancelorder_Btn_Path);
				Accept_Alert();
				Reporter.log("Order No '" + Onum + "' is cancelled");
			}
			Click(Xpath, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		}

		Click(Xpath, Inventory_logout_btn_Path);
		ExplicitWait_Element_Visible(Xpath, Inventory_Username_Path);

	}

}
