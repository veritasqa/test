package com.Standard.Values;

import java.util.ArrayList;
import java.util.List;

import org.testng.asserts.SoftAssert;

public class StandardValueRepository {

	// Elements - LocatorType

	public static final String Xpath = "xpath";
	public static final String ID = "id";
	public static final String Tag = "tag";
	public static final String Name = "name";
	public static final String CSSselector = "cssSelector";
	public static final String LinkText = "linkText";
	public static final String PartialLinkText = "partialLinkText";
	public static final String Classname = "classname";
	public static String SplprjTicketNumber = "000624";
	public static String SplprjTicketNumber2 = "";
	public static List<String> SplprjTicketNumberlist = new ArrayList<String>();
	public static List<String> OrderNumberlist = new ArrayList<String>();

	public SoftAssert softAssert;

	public static String Browser = "chrome";

	// File path for Chrome Driver

	public static String Browserpath = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Driver\\chromedriver.exe";

	public static String SI_TC_2_3_11_1_9_File = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\Desert.jpg";
	public static String SI_TC_2_3_11_1_6_File = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\testasai.xlsx";

	// URLs

	public static final String URL = "https://www.veritas-solutions.net/TheStandard/login.aspx";
	public static final String Production_URL = "https://www.veritas-solutions.net/TheStandard/login.aspx";
	public static final String Inventory_URL = "https://www.veritas-solutions.net/Inventory/login.aspx";

	// Filepath

	public static String PostcardMaillist5path = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\MailListTemplate - Correct (5).xls";
	public static String PostcardMaillist6path = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\MailListTemplate - Correct.xls";
	public static String StandardLogopath = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\Standard_Logo.jpg";

	// Credentials

	public static final String AdminUsername = "qaauto";
	public static final String AdminPassword = "qaauto";

	public static final String UserName1 = "qaauto1";
	public static final String Password1 = "qaauto1";

	public static final String BasicUserName = "qaautobasic";
	public static final String BasicPassword = "qaautobasic";

	public static final String BasicUserName1 = "qaautobasic1";
	public static final String BasicPassword1 = "qaautobasic1";

	public static final String BasicUserName2 = "qaautobasic2";
	public static final String BasicPassword2 = "qaautobasic2";

	public static final String UserFName = "QAauto";
	public static final String UserLName = "Automation";

	public static final String UserFName1 = "QAauto1";
	public static final String UserLName1 = "Automation";

	public static final String BasicUserFName = "qaautobasic";
	public static final String BasicUserLName = "automation";

	public static final String BasicUserFName1 = "qaautobasic1";
	public static final String BasicUserLName1 = "automation";

	public static final String BasicUserFName2 = "qaautobasic2";
	public static final String BasicUserLName2 = "automation";

	public static final String EnUserName = "qaesadmin";
	public static final String EnPassword = "qaesadmin";

	public static final String Inventory_UserName = "yannamalai";
	public static final String Inventory_Password = "yannamalai590";

	public static final String Email = "ver.qaauto@rrd.com";
	public static final String Address = "913 Commerce Ct";
	public static final String City = "Buffalo Grove";
	public static final String State = "Illinois";
	public static final String Zip = "60089";

	// Groups and Contacts
	public static final String VerQA1 = "Veritas QA 1";

	public static String OrderNumber = "5564338";
	public static String Supporttickenumber = "";
	public static String Clienttickenumber = "";
	public static List<String> Supporttickenumberlist = new ArrayList<String>();
	public static List<String> Clienttickenumberlist = new ArrayList<String>();
	public static String[] OrderNumberArray;

	// Group Names

	public static final String Veritas_QA_1_group = "Veritas QA 1";
	public static final String Veritas_QA_2_group = "Veritas QA 2";

	// Fullfillment searches

	public static final String Critical_illness_Search = "Critical illness";
	public static final String Critical_illness_Partname = "Critical Illness All States or Specified Disease NY & VT � CH";

	public static final String Flyer_Search = "Flyer";
	public static final String Flyer_Partname = "Travel Assistance Flyer";

	public static final String Accident_Search = "Accident";
	public static final String Accident_Partname = "Accident - CH";

	public static final String Postcards_Search = "Postcards Accident (AI)";

	public static final String Enrollmentbooklet_Search = "Enrollment Booklet";
	public static final String Enrollmentbooklet_Partname = "Enrollment Booklet";

	public static final String Enrollmentbooklet_iframe = "oRadWindowShoppingCart";
	public static final String Enrollmentbookletdetails_iframe = "oRadWindowComponentDetails";

	public static final String STD_Search = "STD";
	public static final String STD_Partname = "STD Non-contributory - B@G";

	public static final String Postcard = "Postcards Accident (AI)";
	public static final String BasicLife = "Basic Life {with AD&D} � B@G";
	public static final String TravelAssistanceFlyer = "Travel Assistance Flyer";
	public static final String EnrollmentForm = "Enrollment Form";
	public static final String Accident = "Accident - CH";

}
