package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class PersonalizationandModification extends BaseTestStandard {

	public String ClickViewEdit(String OrderNo) throws InterruptedException {

		return ".//td[text()='" + OrderNo + "']//following::a[1][text()='View/Edit']";
	}

	public String MO_Gen_EditStatus(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[10]/a";

	}

	public static final String PDFonlymessage_1 = "No documents will be printed for this order. You will receive an email once the documents are available for download";
	public static final String PDFonlymessage_2 = "Please click \"Checkout\" below to complete your order";
	public static final String PDFerrormessage_Path = "//*[@id='divRightSideEmail']/div/div/span";

	@Test(enabled = true, priority = 1)
	public void SI_TC_2_6_1_6_1() throws InterruptedException, AWTException {

		// Create an order, select Print & PDF, select Modify, verify
		// modification / personalization email, and verify special project
		// -Admin
		Robot robot = new Robot();
		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_QuantityOKbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("strong", "There are items in your cart with a quantity less than")),
				"There are items in your cart with a quantity less than is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_CP_QuantityOKbtn_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "Quantity for SI 14684-D (517) has been changed to 5 unit(s).")),
				"Quantity for SI 14684-D (517) has been changed to 5 unit(s). is not displayed");
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(ID, Std_SCP_Modification_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SCP_Deliverymethod_Path).trim().toUpperCase().equalsIgnoreCase("FEDEX_GROUND"),
				"FEDEX_GROUND is not displayed");
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("HOLD"),
				"HOLD is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_OrderNo_Path, OrderNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed under 'Order #' column in 'Special Projects : View Tickets' grid ");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Modification"),
				"'Modification' is not displayed under 'Job Type' column for the <Order #>");
		Click(Xpath, ClickViewEdit(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("Modification"),
				"Modification'  is not displays in 'Job Type' drop down field");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobType_Path, "disabled").equalsIgnoreCase("true"),
				"'Job Type' field is grayed out and user is able to change the value");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(OrderNumber),
				"<Order #> is not displayed in 'Order #' field in 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_OrderNoReq_Path),
				"A red asterisk * is not displayed by the 'Order No' field on  'General' tab");
		Type(Xpath, SPP_Gen_OrderNo_Path, " ");
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order # Is Required")),
				"'Order # Is Required' message is not displayed near the 'Order #' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_OrderNo_Path),
				"Order # Is Required is not displayed");
		softAssert.assertAll();
		Type(Xpath, SPP_Gen_OrderNo_Path, OrderNumber);
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		ExplicitWait_Element_Visible(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Viewbtn_Path);
		Click(Xpath, MO_Gen_EditStatus(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Update"));
		Select_lidropdown(MO_Gen_ChangeOrderStatus_Path, Xpath, "Resume");
		Click(Xpath, Textpath("a", "Update"));
		Refresh();
		Wait_ajax();
		Assert.assertEquals(Get_Text(Xpath, MO_Gen_Status_Path), "NEW", "NEW' is not displayed for 'Status' field");
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);

		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 2)
	public void SI_TC_2_6_1_6_2() throws InterruptedException, AWTException, IOException {

		// Create an order, select PDF only, select Modify, verify modification
		// / personalization email - Admin/

		OpenUrl_Window_Max(URL);
		login(AdminUsername, AdminPassword);

		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(ID, Std_SCP_Modification_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", PDFonlymessage_1)),
				"'No documents will be printed for this order. You will receive an email once the documents are available for download. is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, PDFerrormessage_Path).contains(PDFonlymessage_2),
				"Please click 'Checkout' below to complete your order.' message is not displayed");
		softAssert.assertAll();
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("NEW"),
				"NEW is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 3)
	public void SI_TC_2_6_1_6_3() throws InterruptedException, AWTException, IOException {

		// Create an order, select Print & PDF, select Personalize, and verify
		// modification / personalization email, and verify special project -
		// Admin

		Robot robot = new Robot();

		OpenUrl_Window_Max(URL);
		login(AdminUsername, AdminPassword);
		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_QuantityOKbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("strong", "There are items in your cart with a quantity less than")),
				"There are items in your cart with a quantity less than is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_CP_QuantityOKbtn_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "Quantity for SI 14684-D (517) has been changed to 5 unit(s).")),
				"Quantity for SI 14684-D (517) has been changed to 5 unit(s). is not displayed");
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(ID, Std_SCP_Personal_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SCP_Deliverymethod_Path).trim().toUpperCase().equalsIgnoreCase("FEDEX_GROUND"),
				"FEDEX_GROUND is not displayed");
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("HOLD"),
				"HOLD is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_OrderNo_Path, OrderNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed under 'Order #' column in 'Special Projects : View Tickets' grid ");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Personalized"),
				"'Personalized' is not displayed under 'Job Type' column for the <Order #>");
		Click(Xpath, ClickViewEdit(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("Personalized"),
				"Modification'  is not displays in 'Job Type' drop down field");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobType_Path, "disabled").equalsIgnoreCase("true"),
				"'Job Type' field is grayed out and user is able to change the value");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(OrderNumber),
				"<Order #> is not displayed in 'Order #' field in 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_OrderNoReq_Path),
				"A red asterisk * is not displayed by the 'Order No' field on  'General' tab");
		Type(Xpath, SPP_Gen_OrderNo_Path, " ");
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order # Is Required")),
				"'Order # Is Required' message is not displayed near the 'Order #' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_OrderNo_Path),
				"Order # Is Required is not displayed");
		softAssert.assertAll();
		Type(Xpath, SPP_Gen_OrderNo_Path, OrderNumber);
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		ExplicitWait_Element_Visible(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Viewbtn_Path);
		Click(Xpath, MO_Gen_EditStatus(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Update"));
		Select_lidropdown(MO_Gen_ChangeOrderStatus_Path, Xpath, "Resume");
		Click(Xpath, Textpath("a", "Update"));
		Refresh();
		Wait_ajax();
		Assert.assertEquals(Get_Text(Xpath, MO_Gen_Status_Path), "NEW", "NEW' is not displayed for 'Status' field");
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);

		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 4)
	public void SI_TC_2_6_1_6_4() throws InterruptedException, AWTException, IOException {

		// Create an order, select PDF only, select Personalize, and verify
		// modification / personalization email, and verify special project -
		// Admin

		OpenUrl_Window_Max(URL);
		login(AdminUsername, AdminPassword);

		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(ID, Std_SCP_Personal_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", PDFonlymessage_1)),
				"'No documents will be printed for this order. You will receive an email once the documents are available for download. is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, PDFerrormessage_Path).contains(PDFonlymessage_2),
				"Please click 'Checkout' below to complete your order.' message is not displayed");
		softAssert.assertAll();
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("NEW"),
				"NEW is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 5)
	public void SI_TC_2_6_1_6_5() throws InterruptedException, AWTException, IOException {

		// Create an order, select Print & PDF, select Modify, verify
		// modification / personalization email, and verify special project
		// -Basic

		Robot robot = new Robot();
		OpenUrl_Window_Max(URL);
		login(BasicUserName, BasicPassword);
		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_QuantityOKbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("strong", "There are items in your cart with a quantity less than")),
				"There are items in your cart with a quantity less than is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_CP_QuantityOKbtn_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "Quantity for SI 14684-D (517) has been changed to 5 unit(s).")),
				"Quantity for SI 14684-D (517) has been changed to 5 unit(s). is not displayed");
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(ID, Std_SCP_Modification_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SCP_Deliverymethod_Path).trim().toUpperCase().equalsIgnoreCase("FEDEX_GROUND"),
				"FEDEX_GROUND is not displayed");
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("HOLD"),
				"HOLD is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Logout"));

		login(AdminUsername, AdminPassword);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_OrderNo_Path, OrderNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed under 'Order #' column in 'Special Projects : View Tickets' grid ");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Modification"),
				"'Modification' is not displayed under 'Job Type' column for the <Order #>");
		Click(Xpath, ClickViewEdit(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("Modification"),
				"Modification'  is not displays in 'Job Type' drop down field");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobType_Path, "disabled").equalsIgnoreCase("true"),
				"'Job Type' field is grayed out and user is able to change the value");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(OrderNumber),
				"<Order #> is not displayed in 'Order #' field in 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_OrderNoReq_Path),
				"A red asterisk * is not displayed by the 'Order No' field on  'General' tab");
		Type(Xpath, SPP_Gen_OrderNo_Path, " ");
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order # Is Required")),
				"'Order # Is Required' message is not displayed near the 'Order #' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_OrderNo_Path),
				"Order # Is Required is not displayed");
		softAssert.assertAll();
		Type(Xpath, SPP_Gen_OrderNo_Path, OrderNumber);
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		ExplicitWait_Element_Visible(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Viewbtn_Path);
		Click(Xpath, MO_Gen_EditStatus(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Update"));
		Select_lidropdown(MO_Gen_ChangeOrderStatus_Path, Xpath, "Resume");
		Click(Xpath, Textpath("a", "Update"));
		Refresh();
		Wait_ajax();
		Assert.assertEquals(Get_Text(Xpath, MO_Gen_Status_Path), "NEW", "NEW' is not displayed for 'Status' field");
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);

		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 6)
	public void SI_TC_2_6_1_6_6() throws InterruptedException, AWTException, IOException {

		// Create an order, select PDF only, select Modify, verify modification
		// / personalization email - Admin/

		Robot robot = new Robot();

		OpenUrl_Window_Max(URL);
		login(BasicUserName, BasicPassword);

		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(ID, Std_SCP_Modification_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", PDFonlymessage_1)),
				"'No documents will be printed for this order. You will receive an email once the documents are available for download. is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, PDFerrormessage_Path).contains(PDFonlymessage_2),
				"Please click 'Checkout' below to complete your order.' message is not displayed");
		softAssert.assertAll();
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("NEW"),
				"NEW is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(URL);
		login(AdminUsername, AdminPassword);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_OrderNo_Path, OrderNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed under 'Order #' column in 'Special Projects : View Tickets' grid ");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Modification"),
				"'Modification' is not displayed under 'Job Type' column for the <Order #>");
		Click(Xpath, ClickViewEdit(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("Modification"),
				"Modification'  is not displays in 'Job Type' drop down field");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobType_Path, "disabled").equalsIgnoreCase("true"),
				"'Job Type' field is grayed out and user is able to change the value");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(OrderNumber),
				"<Order #> is not displayed in 'Order #' field in 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_OrderNoReq_Path),
				"A red asterisk * is not displayed by the 'Order No' field on  'General' tab");
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 7)
	public void SI_TC_2_6_1_6_7() throws InterruptedException, AWTException, IOException {

		// Create an order, select Print & PDF, select Personalize, and verify
		// modification / personalization email, and verify special project -
		// Admin

		Robot robot = new Robot();

		OpenUrl_Window_Max(URL);
		login(BasicUserName, BasicPassword);

		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_QuantityOKbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("strong", "There are items in your cart with a quantity less than")),
				"There are items in your cart with a quantity less than is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_CP_QuantityOKbtn_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "Quantity for SI 14684-D (517) has been changed to 5 unit(s).")),
				"Quantity for SI 14684-D (517) has been changed to 5 unit(s). is not displayed");
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(ID, Std_SCP_Personal_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SCP_Deliverymethod_Path).trim().toUpperCase().equalsIgnoreCase("FEDEX_GROUND"),
				"FEDEX_GROUND is not displayed");
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("HOLD"),
				"HOLD is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Logout"));

		login(AdminUsername, AdminPassword);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_OrderNo_Path, OrderNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed under 'Order #' column in 'Special Projects : View Tickets' grid ");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Personalized"),
				"'Personalized' is not displayed under 'Job Type' column for the <Order #>");
		Click(Xpath, ClickViewEdit(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("Personalized"),
				"Modification'  is not displays in 'Job Type' drop down field");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobType_Path, "disabled").equalsIgnoreCase("true"),
				"'Job Type' field is grayed out and user is able to change the value");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(OrderNumber),
				"<Order #> is not displayed in 'Order #' field in 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_OrderNoReq_Path),
				"A red asterisk * is not displayed by the 'Order No' field on  'General' tab");
		Type(Xpath, SPP_Gen_OrderNo_Path, " ");
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order # Is Required")),
				"'Order # Is Required' message is not displayed near the 'Order #' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_OrderNo_Path),
				"Order # Is Required is not displayed");
		softAssert.assertAll();
		Type(Xpath, SPP_Gen_OrderNo_Path, OrderNumber);
		Click(Xpath, SPP_Gen_Save_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		ExplicitWait_Element_Visible(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Viewbtn_Path);
		Click(Xpath, MO_Gen_EditStatus(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Update"));
		Select_lidropdown(MO_Gen_ChangeOrderStatus_Path, Xpath, "Resume");
		Click(Xpath, Textpath("a", "Update"));
		Refresh();
		Wait_ajax();
		Assert.assertEquals(Get_Text(Xpath, MO_Gen_Status_Path), "NEW", "NEW' is not displayed for 'Status' field");
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);

		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@Test(enabled = true, priority = 8)
	public void SI_TC_2_6_1_6_8() throws InterruptedException, AWTException, IOException {

		// Create an order, select PDF only, select Personalize, and verify
		// modification / personalization email, and verify special project -
		// Admin

		Robot robot = new Robot();
		OpenUrl_Window_Max(URL);
		login(BasicUserName, BasicPassword);

		Type(ID, Std_FulfilmentSearchField_ID, Flyer_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Addtocart(Flyer_Partname));
		Click(Xpath, Addtocart(Flyer_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(ID, Std_SCP_Personal_Radio_ID);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", PDFonlymessage_1)),
				"'No documents will be printed for this order. You will receive an email once the documents are available for download. is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, PDFerrormessage_Path).contains(PDFonlymessage_2),
				"Please click 'Checkout' below to complete your order.' message is not displayed");
		softAssert.assertAll();
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("NEW"),
				"NEW is not displayed in status");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(URL);
		login(AdminUsername, AdminPassword);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_OrderNo_Path, OrderNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed under 'Order #' column in 'Special Projects : View Tickets' grid ");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Personalized"),
				"'Modification' is not displayed under 'Job Type' column for the <Order #>");
		Click(Xpath, ClickViewEdit(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("Personalized"),
				"Modification'  is not displays in 'Job Type' drop down field");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobType_Path, "disabled").equalsIgnoreCase("true"),
				"'Job Type' field is grayed out and user is able to change the value");
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(OrderNumber),
				"<Order #> is not displayed in 'Order #' field in 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_OrderNoReq_Path),
				"A red asterisk * is not displayed by the 'Order No' field on  'General' tab");
		Click(Xpath, Textpath("a", "Logout"));

		OpenUrl_Window_Max(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_logout_btn_Path);

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
		Deletecarts();
	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		ClickHome();
		Deletecarts();
		Clear();

	}

}
