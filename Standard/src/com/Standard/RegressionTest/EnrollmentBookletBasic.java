package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class EnrollmentBookletBasic extends BaseTestStandard {

	@Test(enabled = true, priority = 1)
	public void SI_TC_2_6_1_1_10() throws InterruptedException, IOException {

		// verify user is able to add booklet and other forms in the cart and
		// able to generate proof - Basic

		Type(ID, Std_FulfilmentSearchField_ID, Enrollmentbooklet_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
						.equalsIgnoreCase(Enrollmentbooklet_Partname),
				Enrollmentbooklet_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Enrollmentbooklet_Partname));
		Thread.sleep(5000);
		Switch_To_Iframe(Enrollmentbooklet_iframe);
		Thread.sleep(5000);
		Assert.assertTrue(Element_Is_selected(Xpath, Requiredck("Booklet Cover")),
				"Booklet Cover is not Selected as required");
		Assert.assertFalse(Element_Is_Enabled(Xpath, Requiredck("Booklet Cover")),
				"Booklet Cover required checkbox is click able");
		Assert.assertTrue(Element_Is_selected(Xpath, Requiredck("Summary Letter")),
				"Summary Letter is not Selected as required");
		Assert.assertFalse(Element_Is_Enabled(Xpath, Requiredck("Summary Letter")),
				"Summary Letter required checkbox is click able");
		Click(Xpath, ClickExcludedbooklet("Accident - CH"));
		Assert.assertTrue(Element_Is_selected(Xpath, In_Ex_checkbox("Accident - CH")),
				" \"Include\" button is not displayed in 'Green' color for 'Accident - CH' and is 'checked off'");
		Click(Xpath, ClickExcludedbooklet("Employee Assistance Flyer - 6 Session"));
		Assert.assertTrue(Element_Is_selected(Xpath, In_Ex_checkbox("Employee Assistance Flyer - 6 Session")),
				" \"Include\" button is not displayed in 'Green' color for 'Employee Assistance Flyer - 6 Session' and is 'checked off'");
		Click(Xpath, ClickExcludedbooklet(Critical_illness_Partname));
		Assert.assertTrue(Element_Is_selected(Xpath, In_Ex_checkbox("Employee Assistance Flyer - 6 Session")),
				" \"Include\" button is not displayed in 'Green' color for 'Employee Assistance Flyer - 6 Session' and is 'checked off'");
		Click(Xpath, ClickExcludedbooklet("STD Voluntary - CH"));
		Assert.assertTrue(Element_Is_selected(Xpath, In_Ex_checkbox("STD Voluntary - CH")),
				" \"Include\" button is not displayed in 'Green' color for 'STD Voluntary - CH' and is 'checked off'");
		Click(Xpath, containspath("input", "value", "Add To Cart"));
		Switch_To_Default();
		Wait_ajax();
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Search);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Wait_ajax();
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), "Employee Assistance Flyer - 6 Session");
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
						.equalsIgnoreCase("Employee Assistance Flyer - 6 Session"),
				"Employee Assistance Flyer - 6 Session is not displayed in search results");
		Click(Xpath, Addtocart("Employee Assistance Flyer - 6 Session"));
		Wait_ajax();
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Booklet Cover*")).equalsIgnoreCase("#004ea8"),
				"\"Booklet Cover*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "Will be published online (no DST)");
		Click(Xpath, containspath("input", "id", "CoverImage_0"));
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Summary Letter*")).equalsIgnoreCase("#004ea8"),
				"\"Summary Letter*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Premier");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$1,000");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$75");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Civil Union Partner (CUP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "500");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "month");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, containspath("select", "id", "CHContract"), "Contract 2000");
		Select_DropDown_VisibleText(Xpath, containspath("select", "id", "CHCAPlan"), "Public");
		Type(Xpath, containspath("input", "id", "CHSTDBenefitPercentage"), "50");
		Type(Xpath, containspath("input", "id", "CHMaxSTDBenefitAmount"), "50");
		Type(Xpath, containspath("input", "id", "CHSTDMinWeeklyBenefit"), "50");
		Type(Xpath, containspath("input", "id", "CHBenefitWaitingPeriodDaysAccident"), "50");
		Type(Xpath, containspath("input", "id", "CHBenefitWaitingPeriodDaysIllness"), "50");
		Type(Xpath, containspath("input", "id", "CHMaxBenefitPeriodDays"), "50");
		Select_DropDown_VisibleText(Xpath, containspath("select", "id", "CHPayrollFrequency"), "Weekly");
		Select_DropDown_VisibleText(Xpath, containspath("select", "id", "CHBAGIncludeEmployeeLanguage"),
				"L.L.C. Owner Employees");
		Select_DropDown_VisibleText(Xpath, containspath("select", "id", "CHBAGExcludeEmployeeLanguage"),
				"L.L.C. Owner Employees");
		Type(Xpath, containspath("input", "id", "CHHoursPerWeek"), "50");
		Select_DropDown_VisibleText(Xpath, containspath("select", "id", "CHHoursFrequency"), "week");
		Select_DropDown_VisibleText(Xpath,
				containspath("select", "id", "CHEligibilityWaitingPeriodExistingPolicyOptions"), "None");
		Select_DropDown_VisibleText(Xpath,
				containspath("select", "id", "CHEligibilityWaitingPeriodAlreadyMemberOptions"), "None");
		Select_DropDown_VisibleText(Xpath,
				containspath("select", "id", "CHEligibilityWaitingPeriodBecomesEligibleOptions"), "None");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Premier");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$1,000");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$75");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Civil Union Partner (CUP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "500");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "month");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, ClickProof(Enrollmentbooklet_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Enrollmentbooklet_Search));
		Click(Xpath, ClickProof(Accident_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Accident_Partname));

	}

	@Test(enabled = true, priority = 2, dependsOnMethods = "SI_TC_2_5_1_2_7")
	public void SI_TC_2_6_1_1_11() throws InterruptedException, IOException {

		// Verify upon including and excluding an item proof button disappears
		// from the checkout page and user is able to generate the proof from
		// Set Custom page - Basic

		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Thread.sleep(5000);
		Click(Xpath, ClickIncludebooklet_Details(Accident_Partname));
		Assert.assertTrue(Element_Is_Displayed(Xpath, ClickExcludedbooklet_Details(Accident_Partname)),
				"the button is changed to \"Excluded\" for 'Accident - CH' ");
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, ClickProofed(Enrollmentbooklet_Search)),
				"Proofed button for 'Enrollment Booklet' part is displayed");
		Click(Xpath, ClickVariable(Enrollmentbooklet_Search));
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, ClickProof(Enrollmentbooklet_Search)),
				"Proof button for 'Enrollment Booklet' part is not displayed");
		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Thread.sleep(5000);
		Click(Xpath, ClickExcludedbooklet_Details(Flyer_Partname));
		Assert.assertTrue(Element_Is_Displayed(Xpath, ClickIncludebooklet_Details(Flyer_Partname)),
				"Included button is not displayed in 'Green' color for 'Travel Assistance Flyer' and is 'checked off'");
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, ClickProof(Enrollmentbooklet_Search)),
				"Proof button for 'Enrollment Booklet' part is displayed");
		Click(Xpath, ClickVariable(Enrollmentbooklet_Search));

		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Travel Assistance Flyer")),
				"Travel Assistance Flyer is not displayed in Set Customizable Options page");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Enrollmentbooklet_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Enrollmentbooklet_Search));

	}

	@Test(enabled = true, priority = 3, dependsOnMethods = "SI_TC_2_5_1_2_8")
	public void SI_TC_2_6_1_1_12() throws InterruptedException, IOException {

		// Verify Copy button on details page will copy the item and its answers
		// to an instance of that item outside of the booklet - Basic

		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Thread.sleep(5000);
		Click(Xpath, ClickCopy_Details("Booklet Cover"));
		ExplicitWait_Element_Visible(Xpath, Textpath("*", "Item Has been Updated"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("*", "Item Has been Updated")),
				"Item Has been Updated is not displayed");
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Wait_ajax();
		Click(Xpath, ClickVariable("Booklet Cover"));
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Booklet Cover*")).equalsIgnoreCase("#004ea8"),
				"\"Booklet Cover*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof("Booklet Cover"));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed("Booklet Cover"));
	}

	@Test(enabled = true, priority = 4, dependsOnMethods = "SI_TC_2_5_1_2_9")
	public void SI_TC_2_6_1_1_1() throws InterruptedException, IOException {

		// verify user can "Copy from" items into a booklet except the copy will
		// not be associated with a parent booklet - basic

		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Thread.sleep(5000);
		Select_DropDown_VisibleText(Xpath, Copyfromdd_Details(Accident_Partname), "Accident - CH (AICH1216) ");
		Click(Xpath, Copyfromupdate_Details(Accident_Partname));
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Wait_ajax();
		Click(Xpath, ClickVariable(Enrollmentbooklet_Search));

		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Accident - CH*")),
				"Accident - CH* is not displayed");
		Click(Xpath, Textpath("li", "Accident - CH*"));
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Enrollmentbooklet_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Enrollmentbooklet_Search));
	}

	@Test(enabled = true, priority = 5, dependsOnMethods = "SI_TC_2_5_1_2_10")
	public void SI_TC_2_6_1_1_2() throws InterruptedException, IOException {

		// verify already included items in the booklet allow users to select a
		// replacement from items that exist outside of the booklet in the same
		// Cart Group -Basic

		Type(Xpath, Enterpartcustomtext(Accident_Partname), "QA Test");
		Click(Xpath, ClickUpdatebtn(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Thread.sleep(5000);
		Select_DropDown_VisibleText(Xpath, Replacewithdd_Details(Accident_Partname),
				"Accident - CH (AICH1216) {QA Test}");
		Click(Xpath, Replacewithbtn_Details(Accident_Partname));
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Wait_ajax();
		Click(Xpath, ClickVariable(Enrollmentbooklet_Search));
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Textpath("li", "Accident - CH*"));
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Enrollmentbooklet_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Enrollmentbooklet_Search));
	}

	@Test(enabled = true, priority = 6, dependsOnMethods = "SI_TC_2_5_1_2_11")
	public void SI_TC_2_6_1_13() throws InterruptedException, IOException {

		// Verify basic user can't include and exclude the items on the details
		// page if the cart is approved - basic

		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(EnUserName, EnPassword);
		Deletecarts();
		Click(Xpath, Std_LP_Bell_Path);
		Type(Xpath, AC_Createdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(AC_Createdatefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, ClickApprove(Enrollmentbooklet_Search));
		Wait_ajax();
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, ClickApprove("Booklet Cover"));
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approved");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Click(Xpath, Std_SCP_Send_Message_Path);
		Type(Xpath, Std_SCP_Send_Message_Path, "QA Enrollment Services Approved the Order");
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).contains(BasicUserLName + ", " + BasicUserFName),
				"User name is not displayed Sent to drop down");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(BasicUserName, BasicPassword);
		Click(Xpath, Containstextpath("span", "My Cart"));
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("label", "Approved")),
				"Approved is not displayed in shopping cart page");
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		Wait_ajax();
		Click(Xpath, ClickVariableNoteditable(Enrollmentbooklet_Search));

		Click(Xpath, ClickVariable_Yes(Enrollmentbooklet_Search));
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage1)),
				Copymessage1 + " is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage2)),
				Copymessage2 + " is not displayed");
		Click(Xpath, Containstextpath("span", "My Cart"));
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, ClickVariableNoteditable(Accident_Partname));
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage1)),
				Copymessage1 + " is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage2)),
				Copymessage2 + " is not displayed");
		Click(Xpath, Containstextpath("span", "My Cart"));
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, ClickVariableNoteditable("Booklet Cover"));
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage1)),
				Copymessage1 + " is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage2)),
				Copymessage2 + " is not displayed");
		softAssert.assertAll();
		Click(Xpath, Containstextpath("span", "My Cart"));
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, ClickIncludebooklet_Details(Flyer_Partname)),
				"Included button is not displayed in 'Green' color for 'Travel Assistance Flyer' and is 'checked off'");
		Click(Xpath, ClickExcludedbooklet_Details("Dental - CH"));
		Assert.assertFalse(Element_Is_Displayed(Xpath, ClickIncludebooklet_Details("Dental - CH")),
				"the button is not changed to Included for 'Dental - CH' ");
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Wait_ajax();

	}

	@BeforeClass(enabled = true)
	public void beforeclass() throws IOException, InterruptedException {

		login(BasicUserName, BasicPassword);
		Deletecarts();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = false)
	public void afterMethod() {

	}

}
