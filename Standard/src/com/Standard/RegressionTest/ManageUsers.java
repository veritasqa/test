package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class ManageUsers extends BaseTestStandard {

	public void Selectfilter(String Filter, String Filteroption) throws InterruptedException {

		Click(Xpath, Filter);
		Wait_ajax();
		Click(Xpath, ".//span[text()='" + Filteroption + "']");
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);

	}

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[2]");
	}

	@Test(priority = 1, enabled = false)
	public void SI_TC_2_3_10_1_1() throws InterruptedException {

		// Validate Search Grid is displayed with all options

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageUsers_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MU_Title_Path), "'Manage Users' page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MU_UserNamecol_Path),
				"'Username' column header is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MU_Firstnamecol_Path),
				"'Firstname' column header is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MU_Lastnamecol_Path),
				"'Lastname' column header is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MU_Emailcol_Path),
				"'Email' column header is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MU_Activecol_Path),
				"'Active' column header is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MU_SSOuserscol_Path),
				"SSO users column header is not displayed  above the grid on 'Manage Users' page");

		softAssert.assertAll();
	}

	@Test(priority = 2, enabled = false)
	public void SI_TC_2_3_10_1_3() throws InterruptedException {

		// Validate Edit option opens on Search Result Row and allows you to
		// Edit the user

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageUsers_path);
		Type(Xpath, MU_UserName_Path, AdminUsername);
		Click(Xpath, MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", AdminUsername)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Clickedituser(AdminUsername);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Type(Xpath, MUAdd_Address_Path, "913 Commerce Court-123");
		Click(Xpath, MUEdit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Clickedituser(AdminUsername);
		Assert.assertTrue(Get_Attribute(Xpath, MUAdd_Address_Path, "value").equalsIgnoreCase("913 Commerce Court-123"),
				"'913 Commerce Court-123' is not displayed in 'Address' field");
		Type(Xpath, MUAdd_Address_Path, "913 Commerce Court");
		Click(Xpath, MUEdit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
	}

	@Test(priority = 3, enabled = false)
	public void SI_TC_2_3_10_1_4() throws InterruptedException {

		// Validate Add User link opens "Add User mode" and user is allowed to
		// enter values

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageUsers_path);
		Click(Xpath, MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Type(Xpath, MUAdd_Username_Path, "QAUSER");
		Type(Xpath, MUAdd_Userpwd_Path, "Password");
		Type(Xpath, MUAdd_Firstname_Path, "qaauto");
		Type(Xpath, MUAdd_Lastname_Path, "SI_TC_2_3_10_1_4");
		Type(Xpath, MUAdd_Email_Path, Email);
		Type(Xpath, MUAdd_Address_Path, Address);
		Type(Xpath, MUAdd_City_Path, City);
		Select_DropDown_VisibleText(Xpath, MUAdd_State_Path, "Illinois");
		Type(Xpath, MUAdd_Zip_Path, Zip);
		Select_DropDown_VisibleText(Xpath, MUAdd_Costcenter_Path, "9999 - Veritas");
		softAssert.assertTrue(Element_Is_selected(Xpath, MUAdd_Activeck_Path),
				" 'Active User' checkbox is not checked for 'Active' field");
		softAssert.assertFalse(Element_Is_selected(Xpath, MUAdd_SSOck_Path),
				" 'SSO User' checkbox is checked for 'Active' field");
		Click(Xpath, MUAdd_SSOck_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MUAdd_Usergroup_Path), "'User Group' field is not displayed");
		softAssert.assertFalse(Element_Is_selected(Xpath, MUAdd_EMODUsersCk_Path),
				" 'EMOD users' checkbox is checked for 'Active' field");
		Click(Xpath, MUAdd_EMODUsersCk_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MUAdd_Bussinessowner_Path),
				"'Bussiness owner' field is not displayed");
		softAssert.assertTrue(Element_Is_selected(Xpath, MUAdd_Thestandardck_Path),
				" 'The Standarad' checkbox is not checked for 'Active' field");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, MUAdd_Thestandardck_Path),
				" 'TheStandard' checkbox is not read only");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MUAdd_Security_Path), "'Security' field is not displayed");
		softAssert.assertFalse(Element_Is_selected(Xpath, MUAdd_VeritasAdminck_Path),
				" 'Veritas Admin' checkbox is checked for 'Active' field");
		Click(Xpath, MUAdd_VeritasAdminck_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MUAdd_Assignedfirm_Path),
				"'Assigned firm' field is not displayed");
		softAssert.assertAll();
		Click(Xpath, MUAdd_Cancelbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);

	}

	@Test(priority = 4, enabled = true)
	public void SI_TC_2_3_10_1_2() throws InterruptedException {

		// Validate that filtering functionality works appropriately

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageUsers_path);
		Type(Xpath, MU_UserName_Path, AdminUsername);
		Click(Xpath, MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", AdminUsername)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Selectfilter(MU_UserNamefilter_Path, "NoFilter");
		Type(Xpath, MU_Firstname_Path, UserFName);
		Click(Xpath, MU_Firstnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", UserFName)),
				"Entered <User FName> is not displayed under the 'User FName' column header");
		Selectfilter(MU_Firstnamefilter_Path, "NoFilter");
		Type(Xpath, MU_Lastname_Path, UserLName);
		Click(Xpath, MU_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", UserLName)),
				"Entered <User LName> is not displayed under the 'User LName' column header");
		Selectfilter(MU_Lastnamefilter_Path, "NoFilter");
		Type(Xpath, MU_Email_Path, Email);
		Click(Xpath, MU_Emailfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Email)),
				"Entered <Email> is not displayed under the 'Email' column header");
		Selectfilter(MU_Emailfilter_Path, "NoFilter");
		Click(Xpath, MU_Activeck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, MU_Activeck1_Path),
				"Checked 'Active' checkboxes  are not displayed on 'Manage Users' page");
		Selectfilter(MU_Activefilter_Path, "NoFilter");
		Click(Xpath, MU_SSO_Checkbox_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, MU_SSock1_Path),
				"Checked 'SSO' checkboxes  are not displayed on 'Manage Users' page");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, Std_LP_Home_path);

	}
}
