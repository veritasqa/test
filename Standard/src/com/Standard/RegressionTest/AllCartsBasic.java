package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class AllCartsBasic extends BaseTestStandard {

	@Test(enabled = true, priority = 1)
	public void SI_TC_2_4_1_5() throws InterruptedException {

		// Verify filter functionality and column headings (Basic)Cart
		// Management-Basic

		Clear();
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, AC_Title_Path), " 'All Carts' page is not displayed");

		Type(Xpath, AC_Legalname_Path, Veritas_QA_1_group);
		Click(Xpath, AC_Legalnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Legalname1_Path).equalsIgnoreCase(Veritas_QA_1_group),
				Veritas_QA_1_group + " is not displayed in 'Legal name'  result field");
		Selectfilter(AC_Legalnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Policyno_Path, "VerQA");
		Click(Xpath, AC_Policynofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Policynumber1_Path).toLowerCase().trim().contains("verqa"),
				"Entered Policy# are not displayed in 'Policy#' result field");
		Selectfilter(AC_Policynofilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_State_Path, "IL");
		Click(Xpath, AC_Statefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_State1_Path).equalsIgnoreCase("IL"),
				"Carts with state <State> are not displayed in the 'All Carts' grid");
		Selectfilter(AC_Statefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Firstname_Path, "qaautobasic");
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).toLowerCase().trim().contains("qaautobasic"),
				"the carts with entered First name are not displayed in the 'All Carts' grid");
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Lastname_Path, "automation");
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).toLowerCase().trim().contains("automation"),
				"the carts with entered last name are not displayed in the 'All Carts' grid");
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Firstname_Path, "Enrollment");
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "No Carts Found.")),
				" 'No Carts Found.' message is not displayed in 'All Carts' grid - Firstname filter");
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Lastname_Path, "Services");
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "No Carts Found.")),
				" 'No Carts Found.' message is not displayed in 'All Carts' grid - Lastname filter");
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Createdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(AC_Createdatefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Createdate1_Path).trim().equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				" Entered Create date results not displayed in 'Policy#' result field");
		Selectfilter(AC_Createdatefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Description_Path, "SIC");
		Selectfilter(AC_Descriptionfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Description1_Path).contains("SIC"),
				"the carts with entered Description are not displayed in the 'All Carts' grid");
		Selectfilter(AC_Descriptionfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Selectfilter(AC_Mycartfilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertFalse(Element_Is_selected(Xpath, AC_Mycart1_Path),
				"unchecked rows are not displayed under the 'My Cart' column");
		Selectfilter(AC_Mycartfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 2)
	public void SI_TC_2_4_1_6() throws InterruptedException {

		// Verify paging and page size functionality (Basic)

		Clear();
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"page '1' is not highlighted and displayed in 'All Carts' grid");
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"page '2' is not highlighted and displayed in 'All Carts' grid");
		Click(Xpath, AC_Nextpagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"page '3' is not highlighted and displayed in 'All Carts' grid");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Click(Xpath, AC_Lastpagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination(Get_Text(Xpath, AC_Itemsin_Path).trim())),
				"Last page is not highlighted and displayed in 'All Carts' grid");
		String Secondlastpage = String.valueOf(Integer.parseInt(Get_Text(Xpath, AC_Itemsin_Path).trim()) - 1);
		Click(Xpath, AC_Previouspagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination(Secondlastpage)),
				"Second Last page is not highlighted and displayed in 'All Carts' grid");
		Click(Xpath, AC_Firstpagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted and displayed in 'All Carts' grid - First page button");
		Select_lidropdown(AC_Pagesize_Path, Xpath, "20");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted and displayed in 'All Carts' grid - After set page size");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AC_20items_Path),
				"'20' carts are not displayed in page # '1' in the 'All Carts' grid");

	}

	@Test(enabled = true, priority = 3)
	public void SI_TC_2_4_1_7() throws InterruptedException {

		// Verify the expand, collapsed and proof button feature (Basic)

		Clear();
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Wait_ajax();
		Thread.sleep(3000);
		Click(Xpath, AC_Expand1_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Proof1_Path);
		Click(Xpath, AC_Proof1_Path);
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), "the 'Proof' is not opened and displayed in a new tab");
		Switch_Old_Tab();
		Click(Xpath, AC_Expand1_Path);

	}

	@Test(enabled = true, priority = 4)
	public void SI_TC_2_4_1_8AndSI_TC_2_4_1_9() throws InterruptedException, IOException {

		// Verify basic user can see carts for same "Cost centers" (Basic)

		Clear();
		Click(Xpath, Std_LP_Home_path);
		Click(ID, Std_LP_Logout_ID);
		login(BasicUserName2, BasicPassword2);
		pre_req();
		Click(Xpath, Std_LP_Home_path);
		Click(ID, Std_LP_Logout_ID);

		// Test cases starts here

		login(BasicUserName, BasicPassword);
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName2);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName2);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).toLowerCase().trim().contains("qaautobasic"),
				" <Other Basic User's User Name> is not displayed in 'All Carts' grid");
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).toLowerCase().trim().contains("automation"),
				" <Other Basic User's User Name> is not displayed in 'All Carts' grid");
		softAssert.assertAll();

		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" 'Critical Illness All States or Specified Disease NY & VT � CH  ' is displayed in the 'Shopping Cart' page");
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName2);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName2);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		softAssert.assertFalse(Get_Text(Xpath, AC_Firstname1_Path).toLowerCase().trim().contains("qaautobasic"),
				" the cart created in the pre-condition by the <Other Basic User's User Name> is displayed in 'All Carts' grid");
		softAssert.assertFalse(Get_Text(Xpath, AC_Lastname1_Path).toLowerCase().trim().contains("automation"),
				" the cart created in the pre-condition by the <Other Basic User's User Name> is displayed in 'All Carts' grid");
		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 5)
	public void SI_TC_2_4_1_10() throws InterruptedException, IOException {

		// Verify Basic user is able to see and send the cart of the different
		// cost centers to the enrollment services for approval (Basic)

		Clear();
		Click(Xpath, Std_LP_Home_path);
		Click(ID, Std_LP_Logout_ID);
		login(BasicUserName1, BasicPassword1);
		pre_req();
		Click(Xpath, Std_LP_Home_path);
		Click(ID, Std_LP_Logout_ID);

		// Test Case Starts here

		login(BasicUserName, BasicPassword);
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName1);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName1);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).toLowerCase().trim().contains("qaautobasic"),
				" <Other Basic User's User Name> is not displayed in 'All Carts' grid");
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).toLowerCase().trim().contains("automation"),
				" <Other Basic User's User Name> is not displayed in 'All Carts' grid");
		softAssert.assertAll();

		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" 'Critical Illness All States or Specified Disease NY & VT � CH  ' is displayed in the 'Shopping Cart' page");
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Std_LP_Allcarts_path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName1);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName1);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		softAssert.assertFalse(Get_Text(Xpath, AC_Firstname1_Path).toLowerCase().trim().contains("qaautobasic"),
				" the cart created in the pre-condition by the <Other Basic User's User FName> is displayed in 'All Carts' grid");
		softAssert.assertFalse(Get_Text(Xpath, AC_Lastname1_Path).toLowerCase().trim().contains("automation"),
				" the cart created in the pre-condition by the <Other Basic User's User LName> is displayed in 'All Carts' grid");
		softAssert.assertAll();

	}

	public void pre_req() throws InterruptedException {

		Clear();
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(BasicUserName, BasicPassword);
		// Deletecarts();
		// pre_req();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, Std_LP_Home_path);

	}

}
