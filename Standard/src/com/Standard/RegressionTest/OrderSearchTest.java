package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class OrderSearchTest extends BaseTestStandard {
	public static String OrderSearchNumber = "";

	// Verify search fields are working fine on Order Search page
	@Test
	public void SI_TC_2_2_2_1_1ANDSI_TC_2_2_2_1_2ANDSI_TC_2_2_2_1_3() throws InterruptedException {
		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Order Search"));

		Type(ID, Std_OS_OrderNumber_ID, OrderSearchNumber);
		Click(ID, Std_OS_SearchBtn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath(OrderSearchNumber, "*")), "Ordernumber  Mismatch");

		Clear(ID, Std_OS_OrderNumber_ID);
		Click(ID, Std_OS_StatusArrow_ID);

		Click(Xpath, li_value("NEW"));
		Wait_ajax();
		Click(ID, Std_OS_SearchBtn_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OS_FirstStatus_Path).equalsIgnoreCase("NEW"), "Status mismatch");
		Click(Xpath, li_value("- Please Select -"));
		Wait_ajax();

		// Step 18
		Type(ID, Std_OS_Group_ID, "Veritas QA 1");
		Wait_ajax();
		Click(ID, Std_OS_SearchBtn_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OS_FirstGroupName_Path).equalsIgnoreCase("Veritas QA 1"),
				"GroupName Mismatch");

		Clear(ID, Std_OS_Group_ID);

		Type(ID, Std_OS_Policynumber_ID, "VerQA Test 1");
		Wait_ajax();
		Click(ID, Std_OS_SearchBtn_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OS_FirstpolicyNumber_path).equalsIgnoreCase("VerQA Test 1"),
				"Policy Number Mismatch");

		Clear(ID, Std_OS_Policynumber_ID);

		Type(ID, Std_OS_Lastname_ID, "Test");
		Wait_ajax();
		Click(ID, Std_OS_SearchBtn_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OS_FirstRecipient_Path).equalsIgnoreCase("QA Test"),
				"RecipientName Mismatch");
		Clear(ID, Std_OS_Lastname_ID);

		Click(ID, Std_OS_StartDatebtn_ID);
		DatepickerPastDate(ID, Std_OS_Startmonth_ID, Std_OS_Calender_OKBtn_ID);

		Click(ID, Std_OS_EndDateBtn_ID);
		DatepickerTodaysDate(ID, Std_OS_EndMonth_ID, Std_OS_Calender_OKBtn_ID);

		Click(ID, Std_OS_SearchBtn_ID);

		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Order Search"));

		// Verify clicking on 'View' button takes user to Order Confirmation
		// page and user is able to copy order on Order Confirmation page
		// SI_TC_2_2_2_1_2
		Type(ID, Std_OS_OrderNumber_ID, OrderSearchNumber);
		Click(ID, Std_OS_SearchBtn_ID);
		Wait_ajax();

		Click(Xpath, Std_OC_ViewBtn(OrderSearchNumber));
		Wait_ajax();

		Click(ID, Std_OC_CopyBtn_ID);

		Click(Xpath, Std_SCP_ExpandAllCart_Path);

		Type(ID, Std_SCP_FirstQty_ID, "5");

		Click(Xpath, Selectbuttonshoppingcartbytitle(Postcard, "Update"));

		Click(Xpath, Std_SCP_YesBtn_Path);

		Click(ID, Std_SCP_SI_PrintandPdf_ID);
		Type(ID, Std_SCP_SI_Name_ID, "QA test");
		Type(ID, Std_SCP_SI_Address_ID, "913 Commerce Ct");
		Type(ID, Std_SCP_SI_City_ID, "Buffalo Grove");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, "60089");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_Country_ID, "United States");
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_DeliveryDatepicker_ID);
		Datepicker(Std_SCP_DeliveryDatepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Type(ID, Std_SCP_SI_Ship_Adres_override_ID, "THIS IS QA TEST ORDER. DO NOT SHIP");
		Click(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);

		// SI.TC.2.2.2.1.3
		// Verify clicking on 'View' button takes user on Order Confirmation
		// page and user is able to Submit Cancel Request on Order Confirmation
		// page

		Click(Xpath, Std_OC_SubmitCancelReqBtn_Path);

		Accept_Alert();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("An order cancel request has been submitted.", "*")),
				"An order cancel request has been submitted. --Message is missing");
		Click(ID, Std_OC_CloseBtn_ID);

	}

	// Verify pagination functionality works appropriately on Search Order page
	@Test
	public void SI_TC_2_2_2_1_4() throws InterruptedException {
		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Order Search"));

		Click(Xpath, Std_OC_PageNumber_Path("2"));
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Std_OS_Higlightedpagenumber_Path).equalsIgnoreCase("2"),
				"Wrong page Higlightned");

		Click(Xpath, Std_OS_NextpageBtn_path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Std_OS_Higlightedpagenumber_Path).equalsIgnoreCase("3"),
				"Wrong page Higlightned");

		Click(Xpath, Std_OS_lastPageBtn_Path);
		Wait_ajax();
		Click(Xpath, Std_OS_FirstPageBtn_path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Std_OS_Higlightedpagenumber_Path).equalsIgnoreCase("1"),
				"Wrong page Higlightned");

		Click(ID, Std_OS_PageSizeArrow_ID);
		Click(Xpath, li_value("20"));
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_OS_Firstrecordingrid_Path), "First record is Missing");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_OS_Nineteenthrecordingrid_path),
				"Nineenteenth record is Missing");

	}

	public void OrderPlace() throws InterruptedException {
		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Type(ID, Std_SR_SrchMetBy_FirstQTY_ID, "5");
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(ID, Std_SCP_SI_PrintandPdf_ID);
		Type(ID, Std_SCP_SI_Name_ID, "QA test");
		Type(ID, Std_SCP_SI_Address_ID, "913 Commerce Ct");
		Type(ID, Std_SCP_SI_City_ID, "Buffalo Grove");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, "60089");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_Country_ID, "United States");
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_DeliveryDatepicker_ID);
		Datepicker(Std_SCP_DeliveryDatepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Type(ID, Std_SCP_SI_Ship_Adres_override_ID, "THIS IS QA TEST ORDER. DO NOT SHIP");
		Click(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
		OrderSearchNumber = Get_Text(Xpath, Std_OS_FirstOrderNumber_Path);
		Click(ID, Std_OC_OKBtn_ID);
		Thread.sleep(5000);

	}

	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);

	}

}
