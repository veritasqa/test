package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class ManageOrders extends BaseTestStandard {

	// NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);

	public String MO_Searchresults(String PartName) {

		return ".//td[text()='" + PartName + "']";

	}

	public String MO_SearchresultsSelect(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a[1]";

	}

	public String MO_SearchresultsCopy(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a[2]";

	}

	public String MO_SearchresultsCollaboration(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a[3]";

	}

	public String MO_Gen_SelectOrder(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a";

	}

	public String MO_Gen_EditStatus(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[10]/a";

	}

	@Test(priority = 1, enabled = true)
	public void SI_TC_2_3_7_1_1() throws InterruptedException {

		// Ensure Select order, search order, and search results grids appear
		// appropriately

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Assert.assertTrue(Get_Text(Xpath, MO_SelectOrdertitle_Path).trim().contains("Select Order"),
				"'Select Order' section is not displays in header bar");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_SelectOrderfield_Path),
				"'Select Order' field is not displays in 'Select Order' section ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Viewbtn_Path), "'View' button is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Copybtn_Path), "'Copy' button is not displays");

		Assert.assertTrue(Get_Text(Xpath, MO_Searchordertitle_Path).trim().contains("Search Order"),
				"'Search Order' section is not displays in header bar");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Ordernofield_Path),
				"'Order Number' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Trackingnofield_Path),
				"'Tracking No' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Typedropdown_Path),
				"'Type' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Userfield_Path),
				"'User' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipperfield_Path),
				"'Shipper' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Statusdrop_Path),
				"'Status' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Startdatefield_Path),
				"'Start Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Enddatefield_Path),
				"'End Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_RecipientName_Path),
				"'Recipient' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Addressfield_Path),
				"'Address' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Cityfield_Path),
				"'City' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Statefield_Path),
				"'State' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Zipfield_Path),
				"'Zip' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Searchbtn_Path),
				"'Search' button is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Clearbtn_Path),
				"'Clear' button is not displays in 'Search order' section");

		Assert.assertTrue(Get_Text(Xpath, MO_SearchResultstitle_Path).trim().contains("Search Results"),
				"'Search Results' section is not displays in header bar");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_OrderNo_Path).trim().equalsIgnoreCase("Order#"),
				" 'Order #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Orderedby_Path).trim().equalsIgnoreCase("Ordered by"),
				" 'Ordered by' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Trackingno_Path).trim().equalsIgnoreCase("Tracking#"),
				" 'Tracking #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Orderdate_Path).trim().equalsIgnoreCase("Order Date"),
				" 'Order Date' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Shipper_Path).trim().equalsIgnoreCase("Shipper"),
				" 'Shipper' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Status_Path).trim().equalsIgnoreCase("Status"),
				" 'Status' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Recipient_Path).trim().equalsIgnoreCase("Recipient"),
				" 'Recipient' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_State_Path).trim().equalsIgnoreCase("State/Province"),
				" 'State/Province' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_OrderType_Path).trim().equalsIgnoreCase("OrderType"),
				" 'OrderType' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Noresult_Path).trim().equalsIgnoreCase("No Order Records Found."),
				" 'No Order Records Found.'is not displays in 'Search Results' grid");

		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = true)
	public void SI_TC_2_3_7_1_2() throws InterruptedException, IOException {

		// Verify Clear functionality in Search Order works

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_Ordernofield_Path, OrderNumber);
		Type(Xpath, MO_Trackingnofield_Path, "123456");
		Select_lidropdown(MO_Typedropdown_arrow_Path, Xpath, "THESTANDARD_FULFILLMENT");
		Type(Xpath, MO_Userfield_Path, AdminUsername);
		Type(Xpath, MO_Shipperfield_Path, "UPS Ground");
		Type(Xpath, MO_Startdatefield_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, MO_Enddatefield_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, MO_RecipientName_Path, UserFName + " " + UserLName);
		Type(Xpath, MO_Addressfield_Path, "913 Commerce ct");
		Type(Xpath, MO_Cityfield_Path, "Buffalo Grove");
		Type(Xpath, MO_Statefield_Path, "IL");
		Type(Xpath, MO_Zipfield_Path, "60089-2375");
		Click(Xpath, MO_Clearbtn_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Ordernofield_Path, "value").isEmpty(),
				"'Order #' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Trackingnofield_Path, "value").isEmpty(),
				"'Tracking #' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Statusdrop_Path, "value").isEmpty(),
				"'Status' dropdown is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Typedropdown_Path, "value").isEmpty(),
				"'Type' dropdown is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Userfield_Path, "value").isEmpty(),
				"'User' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Shipperfield_Path, "value").isEmpty(),
				"'Shipper' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_RecipientName_Path, "value").isEmpty(),
				"'Recipient' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Startdatefield_Path, "value").isEmpty(),
				"'Start date' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Enddatefield_Path, "value").isEmpty(),
				"'End Date' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Addressfield_Path, "value").isEmpty(),
				"'Address' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Cityfield_Path, "value").isEmpty(),
				"'City' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Statefield_Path, "value").isEmpty(),
				"'State/Province' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Zipfield_Path, "value").isEmpty(),
				"'Zip/Postal Code' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");

		softAssert.assertAll();

	}

	@Test(priority = 3, enabled = true)
	public void SI_TC_2_3_7_1_3() throws InterruptedException, IOException {

		// Verify search order copy functionality

		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Search);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Wait_ajax();
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Premier");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$1,000");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$75");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Civil Union Partner (CUP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "500");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "month");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Accident_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Accident_Partname));
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, MO_Searchbtn_Path);
		Click(Xpath, MO_SearchresultsCopy(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				"Order copied Successfully is not displayed");
		Thread.sleep(3000);
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Accident - CH")),
				"Accident - CH is not displayed");
		Click(Xpath, ClickVariable(Accident_Partname));
		Click(Xpath, containspath("a", "class", "buttonAction right editVariablesYes"));
		ExplicitWait_Element_Clickable(Xpath, Std_COP_SaveAndContinueBTN_ID);
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Accident_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Accident_Partname));
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Copied Order Number is " + OrderNumber);

	}

	@Test(priority = 4, enabled = true)
	public void SI_TC_2_3_7_1_4() throws InterruptedException, IOException {

		// Validate that select order - view functionality works appropriately

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, MO_Viewbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Gen_OrderSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MO_GeneralTab_Path), "General Tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path), "'Status' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Ticketnumber_Path),
				"'Ticker No column' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Username_Path), "'User name column' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Orderdate_Path), "'Order Date' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Shipdate_Path), "'Shipdate' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Closedate_Path), "'Close date' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Canceldate_Path), "'Cancel Date' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Ordertype_Path), "'Order Type' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Backorder_Path), "'Back Order' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Ohhold_Path), "'On Hold' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_SelectOrder_Path),
				"'Select Order button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_EditStatus_Path),
				"'Edit Status button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_OrderSearchbtn_Path),
				"'Order Search button' is not displayed");
		softAssert.assertAll();

		Click(Xpath, MO_ItemsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Items_Nextbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path), "'Status' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Stockumber_Path),
				"'Stock #' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Description_Path),
				"'Description' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_CustDesc_Path),
				"'Custom Description' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_TranslationReq_Path),
				"'Translation requested' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_State_Path),
				"'State' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Qty_Path),
				"'Qty' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Shipped_Path),
				"'Shipped' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Backorder_Path),
				"'Backorder' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Print_Path),
				"'Print' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Fulfillment_Path),
				"'Fulfillment' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Chargeback_Path),
				"'Chargeback' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Shipping_Path),
				"'Shipping' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Total_Path),
				"'Total' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Efile_Path),
				"'E file' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side");
		softAssert.assertAll();

		Click(Xpath, MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Shipping_Nextbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path),
				"'Status' is not displayed - Shipping tab");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed - Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Address_Path),
				"'Address' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Shippinginst_Path),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Reqdelievery_Path),
				"'Required Delivery' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Shipmethod_Path),
				"'Ship Method' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Shippinginfo_Path),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Trackingno_Path),
				"'Tracking Number' column is not displays in the Shipping tab");
		softAssert.assertAll();

		Click(Xpath, MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Mailling tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path),
				"'Status' is not displayed - Mailling tab");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed - Mailling tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Maillistfilebtn_Path),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		Click(Xpath, MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertAll();

		Click(Xpath, MO_AttachmentTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Attachment_Savebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path),
				"'Status' is not displayed - Attachment tab");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed - Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Type_Path),
				"'Attachment Type' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Createuser_Path),
				"'Create user' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Createdate_Path),
				"'Create Date' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Linktofile_Path),
				"'Link to File' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Refreshicon_Path),
				" 'Refresh' link is not displays on the right side of the page in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_AddAttachicon_Path),
				"'Add Attachment is not ' link displays in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Savebtn_Path),
				"'Save' button is not displays below 'Refresh' button in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Attachments Tab");
		softAssert.assertAll();
	}

	@Test(priority = 6, enabled = true)
	public void SI_TC_2_3_7_1_5() throws InterruptedException, IOException {

		// Validate that select order copy functionality works appropriately

		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Search);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Wait_ajax();
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Premier");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$1,000");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$75");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Civil Union Partner (CUP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "500");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "month");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Accident_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Accident_Partname));
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, MO_Copybtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				"Order copied Successfully is not displayed");
		Thread.sleep(3000);
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Accident - CH")),
				"Accident - CH is not displayed");
		Click(Xpath, ClickVariable(Accident_Partname));
		Click(Xpath, containspath("a", "class", "buttonAction right editVariablesYes"));
		ExplicitWait_Element_Clickable(Xpath, Std_COP_SaveAndContinueBTN_ID);
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Accident_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Accident_Partname));
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Copied Order Number is " + OrderNumber);
		Click(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		Accept_Alert();
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Cancelreqsubmittedclose_Path);
		Click(Xpath, Std_CP_Cancelreqsubmittedclose_Path);

	}

	@Test(priority = 7, enabled = true)
	public void SI_TC_2_3_7_1_6() throws InterruptedException, IOException {

		// Validate Collaboration works appropriately

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, MO_Searchbtn_Path);
		Click(Xpath, MO_SearchresultsCollaboration(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Get_Title().toLowerCase().contains("support tickets"),
				"Support Ticket page is not displayed");
		Supporttickenumber = Get_Text(Xpath, STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		Reporter.log("Created Support Ticket# is " + Supporttickenumber);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, STM_NewStatus_Path).equalsIgnoreCase("#8BC67D"),
				"New Status is not displayed");
		Click(Xpath, STM_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_Createcomment_Path);
		Type(Xpath, STM_Commenttext_Path, "QA Test123");
		Click(Xpath, STM_Createcomment_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, STM_Comment1_Path).equalsIgnoreCase("QA Test123"),
				" 'QA test 123' is not displayed under the 'Comment' column");
		Click(Xpath, STM_UpdateTicketbtn_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

	}

	@Test(priority = 5, enabled = false)
	public void SI_TC_2_3_7_1_7() throws InterruptedException, IOException {

		// Verify that you can select an order and works appropriately

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, MO_Searchbtn_Path);
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, MO_Gen_OrderSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MO_GeneralTab_Path), "General Tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path), "'Status' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Ticketnumber_Path),
				"'Ticker No column' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Username_Path), "'User name column' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Orderdate_Path), "'Order Date' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Shipdate_Path), "'Shipdate' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Closedate_Path), "'Close date' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Canceldate_Path), "'Cancel Date' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Ordertype_Path), "'Order Type' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Backorder_Path), "'Back Order' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Ohhold_Path), "'On Hold' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_SelectOrder_Path),
				"'Select Order button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_EditStatus_Path),
				"'Edit Status button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_OrderSearchbtn_Path),
				"'Order Search button' is not displayed");
		softAssert.assertAll();

		Click(Xpath, MO_ItemsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Items_Nextbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path), "'Status' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Stockumber_Path),
				"'Stock #' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Description_Path),
				"'Description' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_CustDesc_Path),
				"'Custom Description' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_TranslationReq_Path),
				"'Translation requested' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_State_Path),
				"'State' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Qty_Path),
				"'Qty' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Shipped_Path),
				"'Shipped' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Backorder_Path),
				"'Backorder' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Print_Path),
				"'Print' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Fulfillment_Path),
				"'Fulfillment' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Chargeback_Path),
				"'Chargeback' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Shipping_Path),
				"'Shipping' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Total_Path),
				"'Total' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Efile_Path),
				"'E file' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side");
		softAssert.assertAll();

		Click(Xpath, MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Shipping_Nextbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path),
				"'Status' is not displayed - Shipping tab");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed - Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Address_Path),
				"'Address' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Shippinginst_Path),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Reqdelievery_Path),
				"'Required Delivery' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Shipmethod_Path),
				"'Ship Method' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Shippinginfo_Path),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Trackingno_Path),
				"'Tracking Number' column is not displays in the Shipping tab");
		softAssert.assertAll();

		Click(Xpath, MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Mailling tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path),
				"'Status' is not displayed - Mailling tab");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed - Mailling tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Maillistfilebtn_Path),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		Click(Xpath, MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertAll();

		Click(Xpath, MO_AttachmentTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Attachment_Savebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_Statustitle_Path),
				"'Status' is not displayed - Attachment tab");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed - Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Type_Path),
				"'Attachment Type' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Createuser_Path),
				"'Create user' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Createdate_Path),
				"'Create Date' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Linktofile_Path),
				"'Link to File' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Refreshicon_Path),
				" 'Refresh' link is not displays on the right side of the page in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_AddAttachicon_Path),
				"'Add Attachment is not ' link displays in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Savebtn_Path),
				"'Save' button is not displays below 'Refresh' button in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Attachments Tab");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
		Deletecarts();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		ClickHome();

	}

}
