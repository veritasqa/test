package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class FullFillmentOrderClearCart extends BaseTestStandard {

	//Validate that it is removed when you click the cancel order button
	@Test(enabled = true)
	public void SI_TC_2_6_1_3_1() throws InterruptedException{
		
		FulfilmentSearch(TravelAssistanceFlyer);
		
		GroupSearch(VerQA1);	
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		
		Click(Xpath,Selectbuttonshoppingcartbytitle("Travel Assistance Flyer", "Remove"));
		Accept_Alert();
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ItemremovedMsg_Path),"Item(s) have been removed from your cart. message is Not displayed in red color");

	}
	
	//Validate that in the mini shopping cart if you change qty to 0 and click update the part is removed
	@Test
	public void SI_TC_2_6_1_3_2() throws InterruptedException{
		FulfilmentSearchWithoutGroupSearch(EnrollmentForm);
		//GroupSearch(VerQA1);	
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Thread.sleep(5000);
		Type(ID, Std_SR_MSC_Qtyfield_ID, "0");
		Click(ID, Std_SR_MSC_UpdateBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, "//*[text()='EBGEF0717 has been removed from your cart']"),"EBGEF0717 has been removed from your cart- Message Not Displayed");
	}
	
	//Validate on the check out page that the remove button removes part from cart
	@Test
	public void SI_TC_2_6_1_3_3() throws InterruptedException{
		Thread.sleep(5000);
		Type(ID, Std_SR_SearchMaterialsBy_Txt_ID, TravelAssistanceFlyer);
		Click(ID, Std_SR_SearchMaterialsBy_Btn_ID);
	//	GroupSearch(VerQA1);	
		Thread.sleep(5000);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Thread.sleep(5000);
		Type(ID, Std_SR_SearchMaterialsBy_Txt_ID, Postcard);
		Click(ID, Std_SR_SearchMaterialsBy_Btn_ID);
		Thread.sleep(5000);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
		
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		
		Click(Xpath,Selectbuttonshoppingcartbytitle(Postcard, "Remove"));
		Accept_Alert();
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ItemremovedMsg_Path),"Item(s) have been removed from your cart. message is Not displayed in red color");

		
	}
	
	
	@BeforeTest
	public void PostcardLogin() throws IOException, InterruptedException{
		login(AdminUsername, AdminPassword);
	}
}
