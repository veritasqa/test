package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class AllCartsAdmin extends BaseTestStandard {

	@Test(enabled = false, priority = 1)
	public void SI_TC_2_4_1_1() throws InterruptedException {

		// Verify filter functionality and column headings (Admin)

		Clear();
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		String PolicyNo = Get_Text(Xpath, AC_Policynumber1_Path).trim();
		System.out.println("Policy no is " + PolicyNo);
		String Createdate = Get_Text(Xpath, AC_Createdate1_Path).trim();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AC_Title_Path), " 'All Carts' page is not displayed");
		Assert.assertTrue(Get_Attribute(Xpath, AC_Firstname_Path, "value").equalsIgnoreCase("Enrollment"),
				"'Enrollment' is not populated in 'First Name' field");
		Assert.assertTrue(Get_Attribute(Xpath, AC_Lastname_Path, "value").equalsIgnoreCase("Services"),
				"'Services' is not populated in 'Last Name' field");
		Assert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).equalsIgnoreCase("Enrollment"),
				"'Enrollment' is not populated in 'First Name' result field");
		Assert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).equalsIgnoreCase("Services"),
				"'Services' is not populated in 'Last Name'  result field");
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Legalname_Path, Veritas_QA_1_group);
		Click(Xpath, AC_Legalnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Legalname1_Path).equalsIgnoreCase(Veritas_QA_1_group),
				Veritas_QA_1_group + " is not displayed in 'Legal name'  result field");
		Selectfilter(AC_Legalnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Policyno_Path, PolicyNo);
		Click(Xpath, AC_Policynofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Policynumber1_Path).trim().contains(PolicyNo),
				"Entered Policy# are not displayed in 'Policy#' result field");
		Selectfilter(AC_Policynofilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_State_Path, "IL");
		Click(Xpath, AC_Statefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_State1_Path).equalsIgnoreCase("IL"),
				"Carts with state <State> are not displayed in the 'All Carts' grid");
		Selectfilter(AC_Statefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Firstname_Path, "Enrollment");
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).equalsIgnoreCase("Enrollment"),
				"the carts with entered First name are not displayed in the 'All Carts' grid");
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Lastname_Path, "Services");
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).equalsIgnoreCase("Services"),
				"the carts with entered last name are not displayed in the 'All Carts' grid");
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Createdate_Path, Createdate);
		Selectfilter(AC_Createdatefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Createdate1_Path).trim().equalsIgnoreCase(Createdate),
				" Entered Create date results not displayed in 'Policy#' result field");
		Selectfilter(AC_Createdatefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Type(Xpath, AC_Description_Path, "SIC");
		Selectfilter(AC_Descriptionfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Description1_Path).contains("SIC"),
				"the carts with entered Description are not displayed in the 'All Carts' grid");
		Selectfilter(AC_Descriptionfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Selectfilter(AC_Mycartfilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertFalse(Element_Is_selected(Xpath, AC_Mycart1_Path),
				"unchecked rows are not displayed under the 'My Cart' column");
		Selectfilter(AC_Mycartfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		softAssert.assertAll();

	}

	@Test(enabled = false, priority = 2)
	public void SI_TC_2_4_1_2() throws InterruptedException {

		// Verify paging and page size functionality (Admin)

		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"page '1' is not highlighted and displayed in 'All Carts' grid");
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"page '2' is not highlighted and displayed in 'All Carts' grid");
		Click(Xpath, AC_Nextpagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"page '3' is not highlighted and displayed in 'All Carts' grid");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Click(Xpath, AC_Lastpagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination(Get_Text(Xpath, AC_Itemsin_Path).trim())),
				"Last page is not highlighted and displayed in 'All Carts' grid");
		String Secondlastpage = String.valueOf(Integer.parseInt(Get_Text(Xpath, AC_Itemsin_Path).trim()) - 1);
		Click(Xpath, AC_Previouspagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination(Secondlastpage)),
				"Second Last page is not highlighted and displayed in 'All Carts' grid");
		Click(Xpath, AC_Firstpagebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted and displayed in 'All Carts' grid - First page button");
		Select_lidropdown(AC_Pagesize_Path, Xpath, "20");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted and displayed in 'All Carts' grid - After set page size");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AC_20items_Path),
				"'20' carts are not displayed in page # '1' in the 'All Carts' grid");

	}

	@Test(enabled = false, priority = 3)
	public void SI_TC_2_4_1_3() throws InterruptedException {

		// Verify the expand, collapsed and proof button feature (Admin)

		// Pre - Req

		Clear();
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");
		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		Wait_ajax();

		// Test Case starts here
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Firstname_Path, UserFName);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Wait_ajax();
		Type(Xpath, AC_Lastname_Path, UserLName);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Wait_ajax();
		Click(Xpath, AC_Expand1_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Proof1_Path);
		Click(Xpath, AC_Proof1_Path);
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), "the 'Proof' is not opened and displayed in a new tab");
		Switch_Old_Tab();
		Click(Xpath, AC_Expand1_Path);

	}

	@Test(enabled = true, priority = 4)
	public void SI_TC_2_4_1_4() throws InterruptedException, IOException {

		// Verify Admin user can see carts for all "Cost centers" and is able to
		// approve cart of the other cost centers (Admin)
		Clear();
		Thread.sleep(5000);
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(ID, Std_LP_Logout_ID);

		// Basic user preq
		login(BasicUserName1, BasicPassword1);
		Deletecarts();
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(ID, Std_LP_Logout_ID);

		// Test case starts here

		login(UserName1, Password1);
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Firstname_Path, UserFName);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, UserLName);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).equalsIgnoreCase(UserFName),
				"<Other Admin User's User Name> is not displayed in 'All Carts' grid - Other Admin user");
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).equalsIgnoreCase(UserLName),
				"<Other Admin User's User Name> is not displayed in 'All Carts' grid- Other Admin user");
		softAssert.assertAll();
		Click(Xpath, AC_Viewhistorybtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Cancel"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("string", "Cart Group has no send history")),
				"'Cart Group has no send history' is not displayed in a pop up message");
		Click(Xpath, Textpath("a", "Cancel"));
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName1);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName1);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).equalsIgnoreCase(BasicUserFName1),
				"<Other Basic User's User FName> is not displayed in 'All Carts' grid - Other Basic user");
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).equalsIgnoreCase(BasicUserLName1),
				"<Other Basic User's User LName> is not displayed in 'All Carts' grid - Other Basic user");
		softAssert.assertAll();
		Click(Xpath, AC_Viewhistorybtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Cancel"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("string", "Cart Group has no send history")),
				"'Cart Group has no send history' is not displayed in a pop up message - Other Basic user");
		Click(Xpath, Textpath("a", "Cancel"));
		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Send"));
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Sendto_Path, "automation, qaautobasic1");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approved");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");

		Type(Xpath, Std_SCP_Send_Message_Path, "Approved");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Selectfilter(AC_FirstnameFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Firstname_Path, BasicUserFName1);
		Click(Xpath, AC_FirstnameFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_FirstnameFilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Type(Xpath, AC_Lastname_Path, BasicUserLName1);
		Click(Xpath, AC_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Selectfilter(AC_Lastnamefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		softAssert.assertTrue(Get_Text(Xpath, AC_Firstname1_Path).equalsIgnoreCase(BasicUserFName1),
				"<Other Admin User's User FName> is not displayed in 'All Carts' grid - Other Admin user - After approved");
		softAssert.assertTrue(Get_Text(Xpath, AC_Lastname1_Path).equalsIgnoreCase(BasicUserLName1),
				"<Other Admin User's User LName> is not displayed in 'All Carts' grid- Other Admin user - After approved");
		softAssert.assertAll();
		Click(Xpath, AC_Viewhistorybtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Cancel"));
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Approved")),
				"'Approved' is not displayed in textbox ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Acquire")),
				"'Acquire' is not displayed in textbox ");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
		Deletecarts();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, Std_LP_Home_path);

	}

}
