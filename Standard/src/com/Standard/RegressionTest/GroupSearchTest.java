package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class GroupSearchTest extends BaseTestStandard {

	// Verify user is able to Add New Group
	@Test(enabled = true, priority = 1)
	public void SI_TC_2_2_1_1_1ANDSI_TC_2_2_1_1_2() throws InterruptedException, AWTException {
		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Group Search"));
		Click(ID, Std_GS_AddNewGroup_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_GroupleagalName_ID)),
				"* sign is missing in Group Legal name");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_OrgTypeArrow_ID)),
				"* sign is missing inORg Type");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_AccounttypeArrow_ID)),
				"* sign is missing in Account type");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_CompanyArrow_ID)),
				"* sign is missing in Company");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_StateofSitusArrow_ID)),
				"* sign is missing inStateof Situs");

		ExplicitWait_Element_Visible(ID, Std_GS_NC_Policynumber_ID);
		Type(ID, Std_GS_NC_Policynumber_ID, "QA Test");
		Type(ID, Std_GS_NC_GroupleagalName_ID, "QA Test Sample1");
		Click(ID, Std_GS_NC_OrgTypeArrow_ID);
		Wait_ajax();
		Hover(Xpath, li_value("S Corporation"));
		Click(Xpath, li_value("S Corporation"));

		Click(ID, Std_GS_NC_AccounttypeArrow_ID);
		Wait_ajax();
		Hover(Xpath, li_value("Regional < 2,500 Lives"));
		Click(Xpath, li_value("Regional < 2,500 Lives"));

		Wait_ajax();
		Click(ID, Std_GS_NC_CompanyArrow_ID);
		Wait_ajax();
		Hover(Xpath, li_value("SIC"));
		Click(Xpath, li_value("SIC"));

		Wait_ajax();
		Click(ID, Std_GS_NC_StateofSitusArrow_ID);
		Wait_ajax();
		Hover(Xpath, li_value("Illinois"));
		Click(Xpath, li_value("Illinois"));

		Click(ID, Std_GS_NC_OverrideAdresschangeCheckBox_ID);
		Wait_ajax();

		Type(ID, Std_GS_NC_ShipToName_ID, "QA Automation");
		Type(ID, Std_GS_NC_ShipAddress1_ID, "913 Commerce Ct");
		Type(ID, Std_GS_NC_ShipAddress2_ID, "Test");
		Wait_ajax();

		Type(ID, Std_GS_NC_ShiptoCity_ID, "Buffalo Grove");
		Click(ID, Std_GS_NC_ShiptoStateArrow_ID);
		Wait_ajax();
		Hover(Xpath, li_value("Illinois"));
		Click(Xpath, li_value("Illinois"));
		Wait_ajax();

		Type(ID, Std_GS_NC_ShipZip_ID, "60089");
		Click(ID, Std_GS_NC_UpdateBtn_ID);

		// Verify error messages are displaying correctly on Groups Page
		// SI.TC.2.2.1.1.2
		Wait_ajax();
		Type(ID, Std_GS_Groupname_ID, "Q");
		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("Please enter at least 1 valid search filter", "*"));

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("Please enter at least 1 valid search filter", "*")),
				"Please enter at least 1 valid search filter message not displayed");
		// *[text()='Please enter at least 1 valid search filter']
		Wait_ajax();

	}

	// Verify search filter is working fine on Group Search page
	@Test(enabled = true, priority = 2)
	public void SI_TC_2_2_1_1_3ANDSI_TC_2_2_1_1_4() throws InterruptedException {
		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Group Search"));
		Type(ID, Std_GS_Groupname_ID, "QA Test Sample1");
		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_Refreshbtn_ID), "Clear Button is Missing");

		// Assert.assertTrue (Element_Is_Displayed(Xpath,Textpath("QA Test
		// Sample1", "td"))," is not displayed under 'Group's Legal Name' column
		// in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("S Corporation", "td")),
				" is not displayed under 'Org Type' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("SIC", "td")),
				" is not displayed under 'Company' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("IL", "td")),
				"is not displayed under 'State Of Situs' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("QA Test", "td")),
				"is not displayed under  'Policy Number' column in 'Groups' grid");

		// SI.TC.2.2.1.1.4
		// Verify user is able to edit the group and user is able to delete the
		// group
		Click(Xpath, Std_GroupEditOrDelete("QA Test Sample1", "Edit"));
		Wait_ajax();
		Click(ID, Std_GS_EC_OrgTypeArrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("C Corporation"));
		Wait_ajax();

		Type(ID, Std_GS_EC_Policynumber_ID, "QA Test1");
		Click(ID, Std_GS_EC_UpdateBtn_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath,
				Textpath(
						"The Shipping Address has been updated. Click on the Add/Update button when you are ready to save the information.",
						"*")),
				"Error Message not displayes");
		Click(ID, Std_GS_EC_UpdateBtn_ID);
		Wait_ajax();
		Type(ID, Std_GS_Groupname_ID, "QA Test Sample1");
		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();
		Click(Xpath, Std_GroupEditOrDelete("QA Test Sample1", "Edit"));
		Wait_ajax();
		Assert.assertTrue(
				Get_Attribute(Xpath, Std_GS_inputXpath("Policy Number"), "value").equalsIgnoreCase("QA Test1"),
				"Policy Number MisMatch");
		// Assert.assertTrue(Get_Attribute(Xpath, Std_GS_inputXpath("Group's
		// Legal Name:"), "value").equalsIgnoreCase("QA Test Sample1"),"Group's
		// Legal Name: MisMatch");
		Assert.assertTrue(
				Get_Attribute(Xpath, Std_GS_inputXpath("Org Type:"), "value").equalsIgnoreCase("C Corporation"),
				"Org Type: Mismatch");

		Assert.assertTrue(Get_Attribute(Xpath, Std_GS_inputXpath("Group Account Type:"), "value")
				.equalsIgnoreCase("Regional < 2,500 Lives"), "Group Account Type: Mismatch");
		Assert.assertTrue(Get_Attribute(Xpath, Std_GS_inputXpath("Company:"), "value").equalsIgnoreCase("SIC"),
				"Company: Mismatch");
		Assert.assertTrue(
				Get_Attribute(Xpath, Std_GS_inputXpath("State Of Situs:"), "value").equalsIgnoreCase("Illinois"),
				"State Of Situs: MisMatch");

		Type(ID, Std_GS_Groupname_ID, "QA Test Sample1");
		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_Refreshbtn_ID), "Clear Button is Missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("QA Test Sample1", "td")),
				" is not displayed under 'Group's Legal Name' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("C Corporation", "td")),
				" is not displayed under 'Org Type' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("SIC", "td")),
				" is not displayed under 'Company' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("IL", "td")),
				"is not displayed under 'State Of Situs' column in 'Groups' grid");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("QA Test1", "td")),
				"is not displayed under  'Policy Number' column in 'Groups' grid");

		Click(Xpath, Std_GroupEditOrDelete("QA Test Sample1", "Delete"));

		Wait_ajax();

		// Switch_To_Iframe("confirm1515762920930");
		Click(Xpath, Textpath("OK", "*"));
		// Switch_To_Default();
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("Group removed!", "*")),
				"Group removed! Message is missing");

		Type(ID, Std_GS_Groupname_ID, "QA Test Sample1");
		Type(ID, Std_GS_PolicyNumber_ID, "QA Test1");
		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("No matches found.", "*")),
				"No matches found. Message is missing");

	}

	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
	}

}
