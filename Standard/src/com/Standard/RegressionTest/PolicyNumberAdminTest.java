package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class PolicyNumberAdminTest extends BaseTestStandard {
	// Verify Group Information and Shipping Information update to Groups page
	// display appropriately - Admin
	@Test
	public void SI_TC_2_6_1_7_1ANDSI_TC_2_6_1_7_2ANDSI_TC_2_6_1_7_3() throws InterruptedException {
		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Group Search"));
		Click(ID, Std_GS_AddNewGroup_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_GroupleagalName_ID)),
				"* sign is missing in Group Legal name");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_OrgTypeArrow_ID)),
				"* sign is missing inORg Type");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_AccounttypeArrow_ID)),
				"* sign is missing in Account type");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_CompanyArrow_ID)),
				"* sign is missing in Company");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_GS_MandatoryStarPath(Std_GS_NC_StateofSitusArrow_ID)),
				"* sign is missing inStateof Situs");

		ExplicitWait_Element_Visible(ID, Std_GS_NC_Policynumber_ID);
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_ShipToName_ID), "Ship to name field is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_ShipAddress1_ID), "Ship to address 1 field is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_ShipAddress2_ID), "Ship to address 2 field is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_ShiptoCity_ID), " Ship to City is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_ShiptoStateArrow_ID), "Ship to State  is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_ShipZip_ID), "Ship to Zip is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Std_GS_NC_OverrideAdresschangeCheckBox_ID), "Ship to  is missing");

		// SI_TC_2_6_1_7_2
		// Verify Warning Messages for Policy Number and when Company doesn't
		// match State of Situs on Groups Page when adding a Group - Admin
		Type(ID, Std_GS_NC_Policynumber_ID, "VerQA Test 3");
		Type(ID, Std_GS_NC_GroupleagalName_ID, "Veritas QA Policy Test");
		Click(ID, Std_GS_NC_OrgTypeArrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Association"));
		Wait_ajax();
		Click(ID, Std_GS_NC_CompanyArrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("SIC"));
		Wait_ajax();
		Click(ID, Std_GS_NC_StateofSitusArrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("New York"));

		Click(ID, Std_GS_NC_UpdateBtn_ID);
		Wait_ajax();
		Thread.sleep(3000);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("State of Situs does not match Company code. Please change your answer if needed",
								"*")),
				"State of Situs does not match Company code. Please change your answer if needed message not displayed");
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath(
								"This policy holder number or name already exists. To continue click Add/Update; to revise click Reset.",
								"*")),
				"This policy holder number or name already exists. To continue click Add/Update; to revise click Reset. message not displayed");

		Click(ID, Std_GS_NC_ResetBtn_ID);

		// SI.TC.2.6.1.7.3
		// Verify Warning Message for Policy Number when editing a group on the
		// groups page and when editing the policy number on the cart page -
		// Admin

		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Group Search"));
		Type(ID, Std_GS_Groupname_ID, "Veritas QA Policy Test");
		Type(ID, Std_GS_PolicyNumber_ID, "VerQA Test 3");

		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();

		Click(Xpath, Std_GroupEditOrDelete("Veritas QA Policy Test", "Edit"));

		Click(ID, Std_GS_EC_OrgTypeArrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("C Corporation"));
		Wait_ajax();
		Click(ID, Std_GS_EC_UpdateBtn_ID);
		Wait_ajax();
		Thread.sleep(3000);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath(
								"This policy holder number or name already exists. To continue click Add/Update; to revise click Reset.",
								"*")),
				"This policy holder number or name already exists. To continue click Add/Update; to revise click Reset. message not displayed");
		Wait_ajax();
		Click(ID, Std_GS_EC_UpdateBtn_ID);
		Wait_ajax();
		Thread.sleep(3000);
		Click(Xpath, Std_GroupSelect("Veritas QA Policy Test"));
		Wait_ajax();
		Thread.sleep(4000);
		FulfilmentSearchWithoutGroupSearch(Accident);
		Wait_ajax();
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Accident));
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		Click(Xpath, Std_COP_ExpandBTN_Path);

		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "Yes");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_CHAutomobileAmount_path, "$500");
		Select_DropDown_VisibleText(Xpath, Std_CHHMSAmount_path, "$50");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Domestic Partner (DP)");
		Type(Xpath, Std_CHHoursPerWeek_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_CHHoursFrequency_path, "week");

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		Hover(Xpath, Std_SCP_i_hovericon_path);
		Click(Xpath, Std_SCP_i_hovericon_Editbtn_path);

		// *[text()='After this update, all proof files in the cart will need to
		// be resaved. Do you wish to continue?']

		Type(Xpath, Std_SCP_i_hovericon_PolicyNumber_Path, "VerQA Test 2");
		Click(Xpath, Std_SCP_i_hovericon_Savebtn_path);

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath(
								"After this update, all proof files in the cart will need to be resaved. Do you wish to continue?",
								"*")),
				"After this update, all proof files in the cart will need to be resaved. Do you wish to continue? message not displayed");

		Click(Xpath, Std_SCP_i_hovericon_Yesbtn_path);

		Wait_ajax();
		Hover(Xpath, Std_SCP_i_hovericon_path);
		Click(Xpath, Std_SCP_i_hovericon_Editbtn_path);

		Assert.assertTrue(
				Get_Attribute(Xpath, Std_SCP_i_hovericon_PolicyNumber_Path, "value").equalsIgnoreCase("VerQA Test 2"),
				"VerQA Test 2 is missing in policy number field");
		Click(Xpath, Std_SCP_i_hovericon_Nobtn_path);

		Click(Xpath, Selectbuttonshoppingcartbytext(Accident, "Delete"));

		Accept_Alert();

		Hover(Xpath, NavigationTab("Tools"));
		Click(Xpath, NavigationTab("Group Search"));

		Type(ID, Std_GS_Groupname_ID, "Veritas QA Policy Test");

		Click(ID, Std_GS_SearchButton_ID);
		Wait_ajax();

		Click(Xpath, Std_GroupEditOrDelete("Veritas QA Policy Test", "Edit"));

		Click(ID, Std_GS_EC_OrgTypeArrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Association"));
		Wait_ajax();
		Click(ID, Std_GS_EC_UpdateBtn_ID);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath(
								"This policy holder number or name already exists. To continue click Add/Update; to revise click Reset.",
								"*")),
				"This policy holder number or name already exists. To continue click Add/Update; to revise click Reset. message not displayed");
		Click(ID, Std_GS_EC_UpdateBtn_ID);
	}

	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
	}

}
