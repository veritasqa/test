package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class Lockorders extends BaseTestStandard {

	public static final String Copymessage1 = "Reorder/multi-ship within 30 days: Select the save button, proof and reorder.";
	public static final String Copymessage2 = "Reorder/modify after 30 days: Send to Enrollment Services for review and approval.";

	@Test(enabled = true, priority = 1)
	public void SI_TC_2_6_1_4_1() throws InterruptedException, IOException {

		// Verify user can't update the order copied through Order Confirmation
		// Page - Basic user

		login(BasicUserName, BasicPassword);
		Deletecarts();
		Type(ID, Std_FulfilmentSearchField_ID, Accident_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1!")),
				"\"Successfully Added 1!\" message is not displayed");
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$500");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$50");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "week");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(EnUserName, EnPassword);
		Deletecarts();
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Createdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(AC_Createdatefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approved");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Click(Xpath, Std_SCP_Send_Message_Path);
		Type(Xpath, Std_SCP_Send_Message_Path, "QA Enrollment Services Approved the Order");
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).contains(BasicUserLName + ", " + BasicUserFName),
				"User name is not displayed Sent to drop down");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(BasicUserName, BasicPassword);
		Click(Xpath, Containstextpath("span", "My Cart"));
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_YesBtn_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SCP_Deliverymethod_Path).trim().toUpperCase().equalsIgnoreCase("FEDEX_GROUND"),
				"FEDEX_GROUND is not displayed");
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Click(Xpath, Textpath("a", "Copy"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				" \"Order copied Successfully\" message is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, containspath("i", "title", "This cart is locked")),
				" \"Lock\" icon is not displayed");
		Click(Xpath, containspath("a", "title", "Edit Variables"));
		Click(Xpath, Std_SCP_EditvariableYesBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage1)),
				Copymessage1 + " is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage2)),
				Copymessage2 + " is not displayed");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, containspath("i", "title", "This cart is locked")),
				" \"Lock\" icon is removed");
		Click(Xpath, Std_SCP_Deletepart_path);
		Click(Xpath, Std_SCP_DeleteYesBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" the cart  \"Veritas QA 1 - VerQA Test 1\" with 'Accident - CH' piece is not removed");

	}

	@Test(enabled = false, priority = 2)
	public void SI_TC_2_6_1_4_2() throws InterruptedException, IOException {

		// Verify user can't update the new items included, excluded, replaced
		// and copy from in the booklet copied through My Recent orders - Basic
		// user

		login(BasicUserName, BasicPassword);
		Deletecarts();
		Type(ID, Std_FulfilmentSearchField_ID, Enrollmentbooklet_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
						.equalsIgnoreCase(Enrollmentbooklet_Partname),
				Enrollmentbooklet_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Enrollmentbooklet_Partname));
		Switch_To_Iframe(Enrollmentbooklet_iframe);
		Click(Xpath, ClickExcludedbooklet("Accident - CH"));
		Assert.assertTrue(Element_Is_selected(Xpath, In_Ex_checkbox("Accident - CH")),
				" \"Include\" button is not displayed in 'Green' color for 'Accident - CH' and is 'checked off'");
		Click(Xpath, ClickExcludedbooklet("Employee Assistance Flyer - 6 Session"));
		Assert.assertTrue(Element_Is_selected(Xpath, In_Ex_checkbox("Employee Assistance Flyer - 6 Session")),
				" \"Include\" button is not displayed in 'Green' color for 'Employee Assistance Flyer - 6 Session' and is 'checked off'");
		Click(Xpath, containspath("input", "value", "Add To Cart"));
		Switch_To_Default();
		Wait_ajax();
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), STD_Search);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase().equalsIgnoreCase(STD_Partname),
				STD_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(STD_Partname));
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Partname);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Booklet Cover*")).equalsIgnoreCase("#004ea8"),
				"\"Booklet Cover*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No DST; No online publishing");
		Click(Xpath, containspath("input", "id", "CoverImage_0"));
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Summary Letter*")).equalsIgnoreCase("#004ea8"),
				"\"Summary Letter*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Accident - CH*")).equalsIgnoreCase("#004ea8"),
				"\"Accident - CH*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Premier");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$1,000");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$75");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Civil Union Partner (CUP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "500");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "month");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Assert.assertTrue(
				GetCSS_Backgroundcolor(Xpath, Textpath("li", "STD Non-contributory - B@G*"))
						.equalsIgnoreCase("#004ea8"),
				"\"STD Non-contributory - B@G*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Std_ContractLanguageEra_path);
		Select_DropDown_VisibleText(Xpath, Std_ContractLanguageEra_path, "Contract 2000 - All states excluding CA");
		StandardDatePicker(Xpath, Std_PolicyEffectiveDate_path, Get_Todaydate("MMM"), Get_Todaydate("YYYY"),
				Get_Todaydate("d"));
		Select_DropDown_VisibleText(Xpath, Std_Istheweeklybenefitaflatamount_path, "No");
		Wait_ajax();
		Type(Xpath, Std_STDWeeklyBenefitPercentagePart1_path, "100");
		Type(Xpath, Std_InsuredPredisabilityEarningsPart2_path, "100");
		Type(Xpath, Std_MaximumWeeklyBenefitAmount_path, "100");
		Type(Xpath, Std_MinimumWeeklyBenefitAmount_path, "100");
		Type(Xpath, Std_MaximumBenefitPeriod_path, "100");
		Type(Xpath, Std_BenefitWaitingPeriodforAccident_path, "100");
		Type(Xpath, Std_BenefitWaitingPeriodforSickness_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_OrgType_path, "Partners");
		Type(Xpath, Std_workinghours_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_WorkHoursfrequency_path, "week");
		Select_DropDown_VisibleText(Xpath, Std_Option1_path, "date you become a member");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Textpath("li", "Accident - CH*")).equalsIgnoreCase("#004ea8"),
				"\"Accident - CH*\"  is not highlighted and not displayed under 'Enrollment Booklet' in 'Items' section");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$500");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$50");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "week");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(EnUserName, EnPassword);
		Deletecarts();
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Createdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(AC_Createdatefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, ClickProof(Enrollmentbooklet_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Enrollmentbooklet_Search));
		ExplicitWait_Element_Clickable(Xpath, ClickProof(STD_Partname));
		Click(Xpath, ClickProof(STD_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(STD_Partname));
		ExplicitWait_Element_Clickable(Xpath, ClickProof(Accident_Partname));
		Click(Xpath, ClickProof(Accident_Partname));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Accident_Partname));

		// ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, ClickApprove(Enrollmentbooklet_Search));
		Wait_ajax();
		Click(Xpath, ClickApprove(STD_Partname));
		Wait_ajax();
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approved");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Click(Xpath, Std_SCP_Send_Message_Path);
		Type(Xpath, Std_SCP_Send_Message_Path, "QA Enrollment Services Approved the Order");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(BasicUserName, BasicPassword);
		Click(Xpath, Containstextpath("span", "My Cart"));
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_YesBtn_Path);
		Wait_ajax();
		Type(Xpath, ClickQty(Enrollmentbooklet_Search), "5");
		Click(Xpath, Clickupdate(Enrollmentbooklet_Search));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Type(Xpath, ClickQty(STD_Partname), "5");
		Click(Xpath, Clickupdate(STD_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Type(Xpath, ClickQty(Accident_Partname), "5");
		Click(Xpath, Clickupdate(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Click(Xpath, Textpath("a", "Copy"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				" \"Order copied Successfully\" message is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Lockicon(Enrollmentbooklet_Search)),
				" \"Lock\" icon is not displayed for " + Enrollmentbooklet_Search);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Lockicon(Accident_Partname)),
				" \"Lock\" icon is not displayed for " + Accident_Partname);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Lockicon(STD_Partname)),
				" \"Lock\" icon is not displayed for " + STD_Partname);
		Click(Xpath, Textpath("a", "Details"));
		Switch_To_Iframe(Enrollmentbookletdetails_iframe);
		Click(Xpath, ClickIncludebooklet_Details(Accident_Partname));
		Assert.assertFalse(Element_Is_Displayed(Xpath, ClickExcludedbooklet_Details(Accident_Partname)),
				"the button is changed to \"Excluded\" for 'Accident - CH' ");
		Click(Xpath, ClickExcludedbooklet_Details("STD Voluntary - CH"));
		Assert.assertFalse(Element_Is_Displayed(Xpath, ClickIncludebooklet_Details("STD Voluntary - CH")),
				" the button is changed to \"Included\" for 'STD Voluntary - CH' ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Copybooklet_Details(Accident_Partname)),
				"\"Copy\" button is not displayed for the included  'Accident - CH' piece in the pop up window");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Copybooklet_Details("Summary Letter")),
				"\"Copy\" button is not displayed for the included  'Summary Letter' piece in the pop up window");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Copybooklet_Details("Booklet Cover")),
				"\"Copy\" button is not displayed for the included  'Booklet Cover' piece in the pop up window");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Copybooklet_Details("Employee Assistance Flyer - 6 Session")),
				"\"Copy\" button is not displayed for the included  'Employee Assistance Flyer - 6 Session' piece in the pop up window");
		Switch_To_Default();
		Click(Xpath, containspath("a", "title", "Close"));
		Click(Xpath, Std_SCP_Deletepart_path);
		Click(Xpath, Std_SCP_DeleteYesBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" the cart  \"Veritas QA 1 - VerQA Test 1\" with 'Accident - CH' piece is not removed");
		softAssert.assertAll();

	}

	@Test(enabled = false, priority = 3)
	public void SI_TC_2_6_1_4_3() throws InterruptedException, IOException {

		// Verify user having different cost center can't update the order
		// copied through Order Confirmation Page - Basic user

		login(BasicUserName, BasicPassword);
		Deletecarts();
		Type(ID, Std_FulfilmentSearchField_ID, Postcards_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Postcards_Search), Postcards_Search + " is not displayed in search results");
		Click(Xpath, Addtocart(Postcards_Search));
		Wait_ajax();
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, ClickProof(Postcards_Search));
		Click(Xpath, ClickProof(Postcards_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Postcards_Search));
		Type(Xpath, ClickQty(Postcards_Search), "5");
		Click(Xpath, Clickupdate(Postcards_Search));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order no is " + OrderNumber);
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		// Test Case Starts here

		login(BasicUserName1, BasicPassword1);
		Addwidget(Add_Widgets_Path, Orderlookup_Path);
		Type(Xpath, Wid_Xpath(Orderlookup_Wid, Orderlookup_Orderno_Path), OrderNumber);
		Click(ID, Std_FulfilmentSearchField_ID);
		System.out.println(Wid_Xpath(Orderlookup_Wid, Orderlookup_View_Path));
		Wait_ajax();
		MoveandClick(Xpath, Wid_Xpath(Orderlookup_Wid, Orderlookup_View_Path));

		// Click(Xpath, Wid_Xpath(Orderlookup_Wid, Orderlookup_View_Path));
		Wait_ajax();
		Assert.assertTrue(Get_Title().contains("Confirmation"), "Order Confirmation pagee is not displayed");
		Click(Xpath, Textpath("a", "Copy"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				" \"Order copied Successfully\" message is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Lockicon(Postcards_Search)),
				" \"Lock\" icon is not displayed for " + Postcards_Search);
		Click(Xpath, containspath("a", "title", "Variables Not Editable"));
		Click(Xpath, Std_SCP_EditvariableYesBtn_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage1)),
				Copymessage1 + " is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", Copymessage2)),
				Copymessage2 + " is not displayed");
		softAssert.assertAll();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, ClickProof(Postcards_Search));
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Clickable(Xpath, ClickProofed(Postcards_Search));
		Click(Xpath, Std_SCP_Pdfonly_path);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order no is " + OrderNumber);

	}

	@Test(enabled = false, priority = 4)
	public void SI_TC_2_6_1_4_4() throws InterruptedException, IOException {

		// Verify user can copy order through my recent orders widget, update
		// quantity, and delete items from the cart - Basic user

		login(BasicUserName, BasicPassword);
		Deletecarts();
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Partname);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$500");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$50");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "week");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(EnUserName, EnPassword);
		Deletecarts();
		Click(Xpath, Std_LP_Bell_Path);
		ExplicitWait_Element_Clickable(Xpath, AC_Acquirebtn1_Path);
		Type(Xpath, AC_Createdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(AC_Createdatefilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, AC_Loadingpath_Path);
		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		Wait_ajax();
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a[1]", "Send"));
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approved");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Click(Xpath, Std_SCP_Send_Message_Path);
		Type(Xpath, Std_SCP_Send_Message_Path, "QA Enrollment Services Approved the Order");
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).contains(BasicUserLName + ", " + BasicUserFName),
				"User name is not displayed Sent to drop down");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "Logout"));

		login(BasicUserName, BasicPassword);
		Click(Xpath, Containstextpath("span", "My Cart"));
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_YesBtn_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(
				Get_Text(Xpath, Std_SCP_Deliverymethod_Path).trim().toUpperCase().equalsIgnoreCase("FEDEX_GROUND"),
				"FEDEX_GROUND is not displayed");
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Click(Xpath, Std_LP_Home_path);

		Addwidget(Add_Widgets_Path, MyRecentOrders_Path);
		Click(Xpath, MyRecentOrdersview(OrderNumber));
		Click(Xpath, Textpath("a", "Copy"));
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		Type(Xpath, ClickQty(Accident_Partname), "5");
		Click(Xpath, Clickupdate(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Wait_ajax();
		Click(Xpath, Std_SCP_Deletepart_path);
		Click(Xpath, Std_SCP_DeleteYesBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" the cart  \"Veritas QA 1 - VerQA Test 1\" with 'Accident - CH' piece is not removed");

	}

	@Test(enabled = false, priority = 5)
	public void SI_TC_2_6_1_4_5() throws InterruptedException, IOException {

		// Verify user can update the order copied through Manage orders - Admin
		// user

		login(AdminUsername, AdminPassword);
		Deletecarts();
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Partname);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$500");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$50");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "week");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Type(Xpath, ClickQty(Accident_Partname), "5");
		Click(Xpath, Clickupdate(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println(OrderNumber);

		NavigateMenu(Std_LP_Admin_path, Std_LP_ManageOrders_path);
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Copybtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				" \"Order copied Successfully\" message is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ExpandCart1_Path);

		Click(Xpath, containspath("a", "title", "Edit Variables"));
		Click(Xpath, Std_SCP_EditvariableYesBtn_Path);
		Wait_ajax();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Type(Xpath, ClickQty(Accident_Partname), "5");
		Click(Xpath, Clickupdate(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Copied Order Number is " + OrderNumber);
		System.out.println(OrderNumber);

	}

	@Test(enabled = false, priority = 6)
	public void SI_TC_2_6_1_4_6() throws InterruptedException, IOException {

		// Verify user can update the order copied through Order Look up widget
		// - Admin user

		login(AdminUsername, AdminPassword);
		Deletecarts();
		Type(Xpath, containspath("input", "id", Std_FulfilmentSearchField_ID), Accident_Partname);
		Click(Xpath, containspath("input", "id", Std_FulfilmentSearchbtn_ID));
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase()
				.equalsIgnoreCase(Accident_Partname), Accident_Partname + " is not displayed in search results");
		Click(Xpath, Addtocart(Accident_Partname));
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_AutomobileAccidentAmount_path, "$500");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenceScreeningAmount_path, "$50");
		Select_DropDown_VisibleText(Xpath, Std_CHAIPayrollFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_CHSpouseCoverageLanguage_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Frequencyofworkinghours_path, "week");
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Type(Xpath, ClickQty(Accident_Partname), "5");
		Click(Xpath, Clickupdate(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Clickable(ID, Std_SCP_Checkout_ID);
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println(OrderNumber);

		Addwidget(Add_Widgets_Path, Orderlookup_Path);
		Type(Xpath, Wid_Xpath(Orderlookup_Wid, Orderlookup_Orderno_Path), OrderNumber);
		Click(ID, Std_FulfilmentSearchField_ID);
		System.out.println(Wid_Xpath(Orderlookup_Wid, Orderlookup_Copy_Path));
		Wait_ajax();
		MoveandClick(Xpath, Wid_Xpath(Orderlookup_Wid, Orderlookup_Copy_Path));

		// Click(Xpath, Wid_Xpath(Orderlookup_Wid, Orderlookup_View_Path));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Order copied Successfully")),
				" \"Order copied Successfully\" message is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ExpandCart1_Path);
		Click(Xpath, containspath("a", "title", "Edit Variables"));
		Click(Xpath, Std_SCP_EditvariableYesBtn_Path);
		Wait_ajax();
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, ClickApprove(Accident_Partname));
		Wait_ajax();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_QuantityOKbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("strong", "There are items in your cart with a quantity less than")),
				"There are items in your cart with a quantity less than is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_CP_QuantityOKbtn_Path);
		Type(Xpath, ClickQty(Accident_Partname), "5");
		Click(Xpath, Clickupdate(Accident_Partname));
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "Quantity for AICH1216 has been changed to 5 unit(s).")),
				"Quantity for AICH1216 has been changed to 5 unit(s). is not displayed");
		Wait_ajax();
		Click(Xpath, Std_SCP_Deletepart_path);
		Click(Xpath, Std_SCP_DeleteYesBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" the cart  \"Veritas QA 1 - VerQA Test 1\" with 'Accident - CH' piece is not removed");
	}

	@BeforeClass(enabled = false)
	public void beforeclass() throws IOException, InterruptedException {

		login(BasicUserName, BasicPassword);
		Deletecarts();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		ClickHome();
		Deletecarts();
		Clear();
		Click(Xpath, Textpath("a", "Logout"));

	}

}
