package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class TrainingGuide extends BaseTestStandard{

	//Validate that it is removed when you click the cancel order button
	@Test
	public void SI_TC_2_6_1_3_1(){
		Hover(Xpath, NavigationTab("Tools"));
		Assert.assertTrue(Element_Is_Present(Xpath, "//a[@href='/TheStandard/Uploads/Assets/EMOD_field_orders_v5.pdf']"),"Pdf File not found");
	}
	
	
	
	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException{
		login(AdminUsername, AdminPassword);
	}
	
}
