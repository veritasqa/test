package com.Standard.RegressionTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class LoginTest extends BaseTestStandard {

	// verify Active user login

	@Test(enabled = true, priority = 1)
	public void SI_TC_1_0_1_2() throws InterruptedException {
		Click(Xpath, Std_Login_AdminloginBTN_Path);
		Type(ID, Std_Login_UserName_ID, AdminUsername);
		Type(ID, Std_Login_Password_ID, AdminPassword);
		Click(ID, Std_Login_LoginBtn_ID);
		ExplicitWait_Element_Visible(ID, Std_LP_Announcement_Tab_ID);
		Assert.assertTrue(Element_Is_Displayed(ID, Std_LP_Announcement_Tab_ID),
				"Announcement tab is missing in landing page");
		Assert.assertTrue(Get_Text(ID, Std_LP_Username_ID).trim().equalsIgnoreCase("Qaauto Automation"),
				"Username is missing Landing page");
	}

	// Verify the error message is displayed upon "Invalid user name and
	// password" combination

	@Test(enabled = true, priority = 2)
	public void SI_TC_1_0_2_1() throws InterruptedException {

		Type(ID, Std_Login_UserName_ID, AdminUsername);
		Type(ID, Std_Login_Password_ID, "sample");
		Click(ID, Std_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_InvalidCredentials_Msg_path),
				"Invalid User crediential Message is not Displayed");
	}

	// Verify the error message when user login without username

	@Test(enabled = true, priority = 3)
	public void SI_TC_1_0_2_2() throws InterruptedException {
		Click(Xpath, Std_Login_AdminloginBTN_Path);
		Type(ID, Std_Login_Password_ID, AdminPassword);
		Click(ID, Std_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_Username_ErrorMsg_path),
				"UserName Error Message not Displayed");
	}

	// Verify the error message when user login without password

	@Test(enabled = true, priority = 4)
	public void SI_TC_1_0_2_3() throws InterruptedException {
		
		// Click(Xpath, Std_Login_AdminloginBTN_Path);
		
		Type(ID, Std_Login_UserName_ID, AdminUsername);
		Clear(ID, Std_Login_Password_ID);
		Click(ID, Std_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_Password_ErrorMsg_Path),
				"Password Error Message not Displayed");
	}

	// Verify error message when user name and password is not provided for
	// login
	@Test(enabled = true, priority = 5)
	public void SI_TC_1_0_2_4() throws InterruptedException {

		// Click(Xpath, Std_Login_AdminloginBTN_Path);
		Clear(ID, Std_Login_UserName_ID);
		Clear(ID, Std_Login_Password_ID);
		Click(ID, Std_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_Username_ErrorMsg_path),
				"UserName Error Message not Displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_Password_ErrorMsg_Path),
				"Password Error Message not Displayed");
	}

}
