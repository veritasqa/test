package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class AdminSupport extends BaseTestStandard {

	// Support : Veritas Tickets
	public static final String SVT = "VeritasSupportTickets";
	// Support : Client Tickets
	public static final String SCT = "ClientSupportTickets";

	public static final String CurrentVeritaspage = ".//a[@class='rgCurrentPage' and contains(@href,'" + SVT
			+ "')]//span";
	public static final String CurrentClientpage = ".//a[@class='rgCurrentPage' and contains(@href,'" + SCT
			+ "')]//span";

	public static final String SI_TC_2_3_1_3_2_file = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\PDF to upload.pdf";

	public String NavigatePage(String Pageno, String Ticketname) {

		return ".//a[contains(@href,'" + Ticketname + "')]//span[text()='" + Pageno + "']";
	}

	public String CurrentPage(String Pageno, String Ticketname) {

		return ".//a[@class='rgCurrentPage' and contains(@href,'" + Ticketname + "')]//span[text()='" + Pageno + "']";
	}

	public void Createveritasticket() throws InterruptedException {

		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Click(Xpath, ST_VT_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, ST_CST_Createbtn_Path);
		Select_DropDown_VisibleText(Xpath, ST_CST_Categorydd_Path, "Other");
		Wait_ajax();
		Type(Xpath, ST_CST_Commentfield_Path, "Test");
		Select_DropDown_VisibleText(Xpath, ST_CST_SubCategorydd_Path, "Other");
		Click(Xpath, ST_CST_Createbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);

		Supporttickenumber = Get_Text(Xpath, STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		System.out.println("Created Support ticket # is " + Supporttickenumber);
		Reporter.log(Supporttickenumber);

	}

	@Test(priority = 3, enabled = true)
	public void SI_TC_2_3_1_1_1() throws InterruptedException {

		// Verify support page opens with Veritas/Client tickets search grid

		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		ExplicitWait_Element_Clickable(Xpath, ST_CT_Addticketicon_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Veritasticketitle_Path),
				"Support : Veritas Tickets' grid is not displayed on the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_TicketNotxt_Path), " 'Ticket #' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_OrderNotxt_Path), " 'order #'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_SubCattxt_Path), " 'Sub category'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Stocknotxt_Path),
				" 'Product code'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Submittedbytxt_Path),
				" 'Submitted by'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Updatedontxt_Path),
				" 'Updated on'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Teamtxt_Path), " 'Team'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Statustxt_Path), " 'Status'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_Clientticketitle_Path),
				"Support : Client Tickets' grid is not displayed on the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_TicketNotxt_Path),
				" 'Ticket #' column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_OrderNotxt_Path),
				" 'order #'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_SubCattxt_Path),
				" 'Sub category'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_Stocknotxt_Path),
				" 'Product code'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_Submittedbytxt_Path),
				" 'Submitted by'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_Updatedontxt_Path),
				" 'Updated on'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_Teamtxt_Path), " 'Team'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ST_CT_Statustxt_Path),
				" 'Status'column is not displayed in CT");
		softAssert.assertAll();

	}

	@Test(priority = 4, enabled = true)
	public void SI_TC_2_3_1_1_2() throws InterruptedException {

		// Support Page- paging and page size functionality

		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		ExplicitWait_Element_Clickable(Xpath, ST_CT_Addticketicon_Path);

		Click(Xpath, NavigatePage("2", SVT));
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("2", SVT)),
				"Page '2' is not highlighted - Veritas ticket");
		Type(Xpath, ST_VT_Pagesizetxt_Path, "1");
		Click(Xpath, ST_VT_Changtbn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertFalse(Element_Is_Displayed(Xpath, ST_VT_Secondrow_Path),
				"Only one ticket <Ticket#> is not dispayed on the 'Change Request : Veritas Tickets' grid");
		Type(Xpath, ST_VT_Pagenotxt_Path, "2");
		Click(Xpath, ST_VT_Gobtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("2", SVT)),
				"Page '2' is not highlighted - Veritas ticket");
		Click(Xpath, ST_VT_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("3", SVT)),
				"Page '3' is not highlighted - Veritas ticket");
		Click(Xpath, ST_VT_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		String Veritaslastpageno = Get_Text(Xpath, CurrentVeritaspage).trim();
		System.out.println(Get_Text(Xpath, ST_VT_Pagesizeof_Path).substring(
				Get_Text(Xpath, ST_VT_Pagesizeof_Path).length() - 2, Get_Text(Xpath, ST_VT_Pagesizeof_Path).length()));

		Assert.assertEquals(Veritaslastpageno,
				Get_Text(Xpath, ST_VT_Pagesizeof_Path).substring(
						Get_Text(Xpath, ST_VT_Pagesizeof_Path).indexOf(" ") + 1,
						Get_Text(Xpath, ST_VT_Pagesizeof_Path).length()),
				"VT");

		Click(Xpath, ST_VT_Prevpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertNotEquals(Get_Text(Xpath, CurrentVeritaspage).trim(), Veritaslastpageno,
				"<Second Last Page> is not highlighted");
		Click(Xpath, ST_VT_Firstpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("1", SVT)),
				"Page '1' is not highlighted - Veritas ticket");

		Click(Xpath, NavigatePage("2", SCT));
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("2", SCT)),
				"Page '2' is not highlighted - Client ticket");
		Type(Xpath, ST_CT_Pagesizetxt_Path, "1");
		Click(Xpath, ST_CT_Changtbn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, ST_CT_Secondrow_Path),
				"Only one ticket <Ticket#> is not dispayed on the 'Change Request : Client Tickets' grid");
		Type(Xpath, ST_CT_Pagenotxt_Path, "2");
		Click(Xpath, ST_CT_Gobtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("2", SCT)),
				"Page '2' is not highlighted - Client ticket");
		Click(Xpath, ST_CT_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("3", SCT)),
				"Page '3' is not highlighted - Client ticket");
		Click(Xpath, ST_CT_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		String Clientlastpageno = Get_Text(Xpath, CurrentClientpage).trim();
		Assert.assertEquals(Clientlastpageno,
				Get_Text(Xpath, ST_CT_Pagesizeof_Path).substring(
						Get_Text(Xpath, ST_CT_Pagesizeof_Path).indexOf(" ") + 1,
						Get_Text(Xpath, ST_CT_Pagesizeof_Path).length()),
				"Test");

		Click(Xpath, ST_CT_Prevpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertNotEquals(Get_Text(Xpath, CurrentClientpage).trim(), Clientlastpageno,
				"<Second Last Page> is not highlighted");
		Click(Xpath, ST_CT_Firstpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("1", SCT)),
				"Page '1' is not highlighted - Client ticket");

	}

	@Test(priority = 5, enabled = true)
	public void SI_TC_2_3_1_1_3() throws InterruptedException {

		// Validate that the refresh button works appropriately

		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Click(Xpath, ST_VT_Refreshicon_Path);
		Click(Xpath, ST_CT_Refreshicon_Path);

	}

	@Test(priority = 6, enabled = true)
	public void SI_TC_2_3_1_1_4() throws InterruptedException, AWTException {

		// Validate Filter functionality works for all the column heading

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Type(Xpath, ST_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertEquals(Supporttickenumber, Get_Text(Xpath, ST_VT_TicketNo1_Path),
				"the search result for the <Ticket #>  entered is not displayed under the  'Ticket #' column - Veritas ticket");
		Clear(Xpath, ST_TicketNotxt_Path);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_OrderNotxt_Path, OrderNumber);
		Selectfilter(ST_OrderNofilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertEquals(OrderNumber, Get_Text(Xpath, ST_VT_OrderNo1_Path),
				"the search result for the <Order #>  entered is not displayed under the  'Order #' column - Veritas ticket");
		Selectfilter(ST_OrderNofilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_SubCattxt_Path, "Cancel");
		Selectfilter(ST_SubCatfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertEquals("Cancel Order", Get_Text(Xpath, ST_VT_SubCat1_Path),
				"the search result for the Sub category  entered is not displayed under the  'Sub category' column - Veritas ticket");
		Selectfilter(ST_SubCatfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_Stocknotxt_Path, "POST");
		Selectfilter(ST_Stocknofilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertTrue(Get_Text(Xpath, ST_VT_Stockno1_Path).toLowerCase().contains("post"),
				" <POST> is not displayed under 'Stock#' column");
		Selectfilter(ST_Stocknofilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_Submittedbytxt_Path, UserFName + " " + UserLName);
		Selectfilter(ST_Submittedbyfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertEquals(UserFName + " " + UserLName, Get_Text(Xpath, ST_VT_Submittedby1_Path),
				"the search result for the User names  entered is not displayed under the  'Submittedby' column - Veritas ticket");
		Selectfilter(ST_Submittedbyfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_Updatedontxt_Path, Get_Todaydate("MM/dd/YYY"));
		Selectfilter(ST_Updatedonfilter_Path, "LessThan");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Selectfilter(ST_Updatedonfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_Teamtxt_Path, "Order Admin");
		Selectfilter(ST_Teamfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertTrue(Get_Text(Xpath, ST_VT_Team1_Path).toLowerCase().contains("order admin"),
				" <Order Admin> is not displayed under 'Team' column - Veritas ticket");
		Selectfilter(ST_Teamfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Type(Xpath, ST_Statustxt_Path, "Closed");
		Selectfilter(ST_Statusfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		Assert.assertEquals("Closed", Get_Text(Xpath, ST_VT_Status1_Path),
				"the search result for the status entered is not displayed under the  'Status' column - Veritas ticket");
		Selectfilter(ST_Statusfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);

		// Client tickets

		Type(Xpath, ST_CT_TicketNotxt_Path, Clienttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertEquals(Clienttickenumber, Get_Text(Xpath, ST_CT_TicketNo1_Path),
				"the search result for the <Ticket #>  entered is not displayed under the  'Ticket #' column - Veritas ticket");
		Clear(Xpath, ST_CT_TicketNotxt_Path);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Type(Xpath, ST_CT_OrderNotxt_Path, "123");// replace Orderno;
		Selectfilter(ST_CT_OrderNofilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertEquals("123", Get_Text(Xpath, ST_CT_OrderNo1_Path),
				"the search result for the <Order #>  entered is not displayed under the  'Order #' column - Veritas ticket");
		Selectfilter(ST_CT_OrderNofilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Type(Xpath, ST_CT_SubCattxt_Path, "Other");
		Selectfilter(ST_CT_SubCatfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertEquals("Other", Get_Text(Xpath, ST_CT_SubCat1_Path),
				"the search result for the Sub category  entered is not displayed under the  'Sub category' column - Veritas ticket");
		Selectfilter(ST_CT_SubCatfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Type(Xpath, ST_CT_Stocknotxt_Path, "POST");
		Selectfilter(ST_CT_Stocknotxtfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, ST_CT_Stockno1_Path).toLowerCase().contains("post"),
				" <POST> is not displayed under 'Stock#' column");
		Selectfilter(ST_CT_Stocknotxtfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Type(Xpath, ST_CT_Submittedbytxt_Path, UserFName + " " + UserLName);
		Selectfilter(ST_CT_Submittedbyfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertEquals(UserFName + " " + UserLName, Get_Text(Xpath, ST_CT_Submittedon1_Path),
				"the search result for the User names  entered is not displayed under the  'Submittedby' column - Veritas ticket");
		Selectfilter(ST_CT_Submittedbyfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Type(Xpath, ST_CT_Updatedontxt_Path, Get_Todaydate("MM/dd/YYY"));
		Selectfilter(ST_CT_Updatedonfilter_Path, "LessThan");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Selectfilter(ST_CT_Updatedonfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Type(Xpath, ST_CT_Teamtxt_Path, "Order Admin");
		Selectfilter(ST_CT_Teamfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, ST_CT_Team1_Path).toLowerCase().contains("Order Admin"),
				" <Order Admin> is not displayed under 'Team' column - Veritas ticket");
		Selectfilter(ST_CT_Teamfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Type(Xpath, ST_CT_Statustxt_Path, "Closed");
		Selectfilter(ST_CT_Statusfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);
		Assert.assertEquals("Closed", Get_Text(Xpath, ST_CT_Status1_Path),
				"the search result for the status entered is not displayed under the  'Status' column - Veritas ticket");
		Selectfilter(ST_CT_Statusfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

	}

	@Test(priority = 2, enabled = true)
	public void SI_TC_2_3_1_3_2() throws InterruptedException, AWTException {

		// Add a new support ticket in "Veritas Tickets" section
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Click(Xpath, ST_VT_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, ST_CST_Createbtn_Path);
		Select_DropDown_VisibleText(Xpath, ST_CST_Categorydd_Path, "Other");
		Thread.sleep(2000);
		Type(Xpath, ST_CST_Commentfield_Path, "Test");
		Select_DropDown_VisibleText(Xpath, ST_CST_SubCategorydd_Path, "Other");
		Click(Xpath, ST_CST_Createbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketbtn_Path),
				"new support ticket page is not displayed");
		Supporttickenumber = Get_Text(Xpath, STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		Reporter.log(Supporttickenumber);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_Ticketno_Path), "Ticket # is not displayed");
		Assert.assertTrue(Get_DropDown(Xpath, STM_Category_Path).equalsIgnoreCase("Other"),
				"'Other' is not displayed for 'Category' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, STM_SubCat_Path).equalsIgnoreCase("Other"),
				"'Other' is not displayed for 'Sub Category' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, STM_Assignedto_Path).equalsIgnoreCase("Veritas"),
				"'Veritas' is not displayed for 'Assigned to' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, STM_Veritasteam_Path).equalsIgnoreCase("Fulfillment"),
				"'Fulfillment' is not displayed for 'Veritas Team to' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, STM_Clientteam_Path).equalsIgnoreCase("Inventory Admin"),
				"'Inventory Admin' is not displayed for 'Client Team to' drop down");
		Assert.assertTrue(Get_Text(Xpath, STM_Comment1_Path).equalsIgnoreCase("Test"),
				"'Test' is not displayed for 'Comment' column");

	}

	@Test(priority = 7, enabled = true, dependsOnMethods = "SI_TC_2_3_1_3_2")
	public void SI_TC_2_3_1_3_1ANDSI_TC_2_3_1_3_3() throws InterruptedException, AWTException {

		// Validate user is able to update the Ticket on Edit/View Page

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Type(Xpath, ST_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);
		Click(Xpath, ST_VT_Edit1_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, STM_Ticketno_Path).trim().equalsIgnoreCase(Supporttickenumber),
				" the ticket is not opened and displayed with the <Ticket #> displayed on the top right side of the page ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_NewStatus_Path), " 'New' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Inprogress_Path),
				" 'Inprogress' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Hold_Path), " 'Hold' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Closedstatus_Path), " 'Closed' button is not displayed");
		softAssert.assertAll();
		Click(Xpath, STM_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_Createcomment_Path);
		Type(Xpath, STM_Commenttext_Path, "QA Test");
		// Type(Xpath, STM_Attachment_Path, SI_TC_2_3_1_3_2_file);

		// Click(Xpath, STM_Attachment_Path);
		// Fileupload(SI_TC_2_3_1_3_2_file);
		// ExplicitWait_Element_Visible(Xpath, ST_Remove_Path);
		Click(Xpath, STM_Createcomment_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, STM_Comment1_Path).equalsIgnoreCase("QA Test"),
				"'QA Test' is not displayed for 'Comment' column");
		softAssert.assertTrue(
				Get_Text(Xpath, STM_User1_Path).toLowerCase().equalsIgnoreCase(UserFName + " " + UserLName),
				"'QA Test' is not displayed for 'Comment' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Createdate1_Path), "Create Date is not displayed");
		// softAssert.assertTrue(Element_Is_Displayed(Xpath,
		// STM_Attachment1_Path),
		// " 'File' is not displayed under 'Attachment' column");
		softAssert.assertAll();
		Click(Xpath, STM_Inprogress_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

		// Close the ticket and verify the ticket does not show up on the
		// Support page

		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Type(Xpath, ST_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);
		Assert.assertEquals("In-Progress", Get_Text(Xpath, ST_VT_Status1_Path),
				"'In-Progress' is not displayed under 'Status' column");
		Click(Xpath, ST_VT_Edit1_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);
		Click(Xpath, STM_Closedstatus_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Type(Xpath, ST_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_VT_Loading_Path);
		Assert.assertEquals("Closed", Get_Text(Xpath, ST_VT_Status1_Path),
				"'Closed' is not displayed under 'Status' column");

	}

	@Test(priority = 1, enabled = true)
	public void SI_TC_2_3_1_2_1AndSI_TC_2_3_1_2_2AndSI_TC_2_3_1_2_3() throws InterruptedException, AWTException {

		// Add New Support Ticket in "Clients" section

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Click(Xpath, ST_CT_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, ST_CST_Createbtn_Path);
		Select_DropDown_VisibleText(Xpath, ST_CST_Categorydd_Path, "Other");
		Thread.sleep(2000);
		Type(Xpath, ST_CST_Commentfield_Path, "Test");
		Select_DropDown_VisibleText(Xpath, ST_CST_SubCategorydd_Path, "Other");
		Click(Xpath, ST_CST_Createbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketbtn_Path),
				"new support ticket page is not displayed");
		Clienttickenumber = Get_Text(Xpath, STM_Ticketno_Path).trim();
		Clienttickenumberlist.add(Clienttickenumber);
		Reporter.log(Clienttickenumber);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_Ticketno_Path), "Ticket # is not displayed");

		// Validate user is able to update the Ticket on Edit/View Page (Client)

		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_NewStatus_Path), " 'New' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Inprogress_Path),
				" 'Inprogress' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Hold_Path), " 'Hold' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, STM_Closedstatus_Path), " 'Closed' button is not displayed");
		softAssert.assertAll();
		Click(Xpath, STM_Inprogress_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

		// Close the ticket and verify the ticket does not show up on the
		// Support page (Client section)

		Click(Xpath, STM_Closedstatus_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Type(Xpath, ST_CT_TicketNotxt_Path, Clienttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, ST_CT_Loading_Path);

		Assert.assertEquals("Closed", Get_Text(Xpath, ST_CT_Status1_Path),
				"'Closed' is not displayed under 'Status' column");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, Std_LP_Home_path);

	}

}
