package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class FullfillmentcustompieceBasic extends BaseTestStandard {

	@Test(priority = 1, enabled = true)
	public void SI_TC_2_6_1_3AndSI_TC_2_6_1_4() throws InterruptedException {

		// Verify user is able to update $ sign in all the other columns except
		// column A , non-numeric value should stay same and hitting Format
		// button shouldn�t add the $ sign if it has a $ sign already (basic

		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase().equalsIgnoreCase(
				Critical_illness_Partname), Critical_illness_Partname + " is not displayed in search results");
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Typewebtable(Xpath, _$formantcells("Non-Tobacco Rate Table", "1"), "1");
		Typewebtable(Xpath, _$formantcells("Non-Tobacco Rate Table", "2"), "2");
		Typewebtable(Xpath, _$formantcells("Non-Tobacco Rate Table", "3"), "3");
		Typewebtable(Xpath, _$formantcells("Non-Tobacco Rate Table", "4"), "4");
		Typewebtable(Xpath, _$formantcells("Non-Tobacco Rate Table", "5"), "5");
		Click(Xpath, _$formantbtn("Non-Tobacco Rate Table"));

		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Non-Tobacco Rate Table", "1")).contains("1"),
				"'1' is not displayed under column 'A' in 'Non-Tobacco Rate Table'");
		softAssert.assertFalse(Get_Text(Xpath, _$formantcells("Non-Tobacco Rate Table", "1")).contains("$"),
				"'$' is displayed near '10' under column 'A' in 'Non-Tobacco Rate Table' ");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Non-Tobacco Rate Table", "2")).equalsIgnoreCase("$2.00"),
				"'$2.00' is not displayed under column 'B' in 'Non-Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Non-Tobacco Rate Table", "3")).equalsIgnoreCase("$3.00"),
				"'$3.00' is not displayed under column 'C' in 'Non-Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Non-Tobacco Rate Table", "4")).equalsIgnoreCase("$4.00"),
				"'$4.00' is not displayed under column 'D' in 'Non-Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Non-Tobacco Rate Table", "5")).equalsIgnoreCase("$5.00"),
				"'$5.00' is not displayed under column 'E' in 'Non-Tobacco Rate Table'");
		// softAssert.assertAll();
		Typewebtable(Xpath, _$formantcells("Tobacco Rate Table", "1"), "Test");
		Typewebtable(Xpath, _$formantcells("Tobacco Rate Table", "2"), "QA");
		Typewebtable(Xpath, _$formantcells("Tobacco Rate Table", "3"), "3.9999");
		Typewebtable(Xpath, _$formantcells("Tobacco Rate Table", "4"), "$4.25");
		Typewebtable(Xpath, _$formantcells("Tobacco Rate Table", "5"), "-50");
		Click(Xpath, _$formantcells("Tobacco Rate Table", "4"));
		RightClick(Xpath, _$formantcells("Tobacco Rate Table", "5"));
		Click(Xpath, Textpath("div", "Insert column on the right"));
		Typewebtable(Xpath, _$formantcells("Tobacco Rate Table", "6"), "abc");
		Click(Xpath, _$formantbtn("Tobacco Rate Table"));
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Tobacco Rate Table", "1")).equalsIgnoreCase("Test"),
				"'Test10' is not displayed under column 'A' in 'Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Tobacco Rate Table", "2")).equalsIgnoreCase("QA"),
				"'QA Test' is not displayed under column 'B' in 'Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Tobacco Rate Table", "3")).equalsIgnoreCase("$4.00"),
				"'$30.00' is not displayed under column 'C' in 'Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Tobacco Rate Table", "4")).equalsIgnoreCase("$4.25"),
				"'$40.25' is not displayed under column 'D' in 'Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Tobacco Rate Table", "5")).equalsIgnoreCase("-$50.00"),
				"'Test10' is not displayed under column 'E' in 'Tobacco Rate Table'");
		softAssert.assertTrue(Get_Text(Xpath, _$formantcells("Tobacco Rate Table", "6")).equalsIgnoreCase("abc"),
				"'-$65.00' is not displayed under column 'F' in 'Tobacco Rate Table'");
		softAssert.assertAll();

		// Place an order and Verify user is able to select Delivery Time if
		// Delivery Method is Overnight (basic)

		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Critical_illness_Partname)),
				"'Critical Illness All States or Specified Disease NY & VT � CH' is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Your Shopping Cart Is Empty")),
				" 'Critical Illness All States or Specified Disease NY & VT � CH  ' is displayed in the 'Shopping Cart' page");
		Click(ID, Std_LP_Logout_ID);
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(BasicUserName, BasicPassword);
		Deletecarts();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, Std_LP_Home_path);

	}

}
