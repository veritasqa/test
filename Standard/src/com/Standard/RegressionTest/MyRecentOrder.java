package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class MyRecentOrder extends BaseTestStandard {

	// Create a new order
	@Test
	public void SI_TC_2_5_2_1_1AndSI_TC_2_5_2_1_2() throws InterruptedException {
		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Type(ID, Std_SR_SrchMetBy_FirstQTY_ID, "5");
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(ID, Std_SCP_SI_PrintandPdf_ID);
		Type(ID, Std_SCP_SI_Name_ID, "QA test");
		Type(ID, Std_SCP_SI_Address_ID, "913 Commerce Ct");
		Type(ID, Std_SCP_SI_City_ID, "Buffalo Grove");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, "60089");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_Country_ID, "United States");
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_DeliveryDatepicker_ID);
		Datepicker(Std_SCP_DeliveryDatepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Type(ID, Std_SCP_SI_Ship_Adres_override_ID, "THIS IS QA TEST ORDER. DO NOT SHIP");
		Click(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
		Click(ID, Std_OC_OKBtn_ID);
		Thread.sleep(5000);
		// SI.TC.2.5.2.1.2
		// Copy order from widget
		Click(Xpath, OrderCopyORViewBtn(OrderNumber, "Copy"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_Ordersuccess_Msg_Path),
				"Order Copied Successfully message is displayed");
		Click(Xpath, Std_SCP_ExpandAllCart_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Selectbuttonshoppingcartbytitle("Postcards Accident (AI)", "Update"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_OrderQTYUpdateMSg_Path),
				"Quantity for POSTCARDAI0816 has been changed to 5 unit(s). message is NOt displayed");

		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_YesBtn_Path);

		// Step 51
		Click(ID, Std_SCP_SI_PrintandPdf_ID);
		Type(ID, Std_SCP_SI_Name_ID, "QA test");
		Type(ID, Std_SCP_SI_Address_ID, "913 Commerce Ct");
		Type(ID, Std_SCP_SI_City_ID, "Buffalo Grove");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, "60089");
		Select_DropDown_VisibleText(ID, Std_SCP_SI_Country_ID, "United States");
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Click(ID, Std_SCP_DeliveryDatepicker_ID);
		Datepicker(Std_SCP_DeliveryDatepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Type(ID, Std_SCP_SI_Ship_Adres_override_ID, "THIS IS QA TEST ORDER. DO NOT SHIP");
		Click(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
		Click(ID, Std_OC_OKBtn_ID);
	}

	// Latest 5 orders displayed by creation date in widget
	@Test(enabled = false)
	public void SI_TC_2_5_2_1_3() throws InterruptedException {
		int i;
		for (i = 0; i <= 5; i++) {
			FulfilmentSearch(Postcard);
			GroupSearch(VerQA1);
			Type(ID, Std_SR_SrchMetBy_FirstQTY_ID, "5");
			Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
			ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
			Click(ID, Std_SR_MSC_CheckoutBtn_ID);
			Wait_ajax();
			Click(ID, Std_COP_SaveAndContinueBTN_ID);
			Wait_ajax();
			Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
			Switch_New_Tab();
			Switch_Old_Tab();
			Click(Xpath, Std_SCP_CheckoutBtn_Path);
			Click(ID, Std_SCP_SI_PrintandPdf_ID);
			Type(ID, Std_SCP_SI_Name_ID, "QA test");
			Type(ID, Std_SCP_SI_Address_ID, "913 Commerce Ct");
			Type(ID, Std_SCP_SI_City_ID, "Buffalo Grove");
			Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
			Type(ID, Std_SCP_SI_Zip_ID, "60089");
			Select_DropDown_VisibleText(ID, Std_SCP_SI_Country_ID, "United States");
			Click(Xpath, Std_SCP_Next_Btn_Path);
			Click(ID, Std_SCP_DeliveryDatepicker_ID);
			Datepicker(Std_SCP_DeliveryDatepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
			Type(ID, Std_SCP_SI_Ship_Adres_override_ID, "THIS IS QA TEST ORDER. DO NOT SHIP");
			Click(ID, Std_SCP_Checkout_ID);
			Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
					"5 is displayed under 'Ordered' column");
			OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
			OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
			OrderNumberArray[i] = Get_Text(ID, Std_OC_OrderNumder_ID);
			Click(ID, Std_OC_OKBtn_ID);
		}

		for (i = 0; i <= 5; i++) {

			Assert.assertTrue(Element_Is_Displayed(Xpath, "//*[text()='" + OrderNumberArray[i] + "']"),
					"Five orders Displays");
		}

	}

	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
	}

}
