package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class ManageAnnouncement extends BaseTestStandard {

	public static String Announcement = "QA Test" + Get_Todaydate("MM/d/YYYY HH:mm:SS");
	public static String EditAnnouncement = "QA Test for Announcement" + Get_Todaydate("MM/d/YYYY HH:mm:SS");
	public static String HyperlinkURL_Stage = "https://staging.veritas-solutions.net/TheStandard/";
	public static String HyperlinkURL_Prod = "https://www.veritas-solutions.net/TheStandard/";
	public static String Itemof;

	@Test(enabled = true, priority = 1)
	public void SI_TC_2_3_4_1_1() throws InterruptedException {

		// Validate page opens and appropriate fields/tables are displaying

		NavigateMenu(Std_LP_Admin_path, Std_LP_Manageannouncements_path);
		ExplicitWait_Element_Visible(Xpath, MA_Title_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MA_Title_Path), "'Manage Announcements' page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Announcement_Path),
				"'Announcement' search field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Announcementfilter_Path),
				" 'Filter' icon is not displayed by 'Announcement' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Startdate_Path),
				"'Start Date' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Startdatefilter_Path),
				" 'Filter' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_StartdateCalicon_Path),
				" 'Calendar' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Inactivedate_Path),
				" 'Inactive Date' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Inactivedatefilter_Path),
				" 'Filter' icon is not displayed by 'Inactive Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_InactivedateCalicon_Path),
				" 'Calendar' icon is not displayed by 'Inactive Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Sort_Path),
				" 'Sort' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Sortfilter_Path),
				"'Filter' icon is not displayed by 'Sort' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_OnlyActiveck_Path),
				" 'Only Active' checkbox is not displayed  on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_OnlyActivefilter_Path),
				" 'Filter' icon is not displayed by 'Only Active' checkbox");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Addannouncementbtn_Path),
				"'Add Announcement' button is not displayed on 'Manage Announcements' page at the bottom of the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MA_Refreshbn_Path),
				" 'Refresh' button is not displayed on 'Manage Announcements' page at the bottom of the page");
		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 2)
	public void SI_TC_2_3_4_1_2() throws InterruptedException, AWTException {

		// Add Announcement

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Manageannouncements_path);
		ExplicitWait_Element_Visible(Xpath, MA_Title_Path);
		Click(Xpath, MA_Addannouncementbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, MA_Createannouncementbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Type(Xpath, MA_AddAnnouncement_Path, "QA Test");
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Switch_To_Default();
		Click(Xpath, MA_Boldbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("strong", "QA Test")),
				"'QA Test' is not displayed in bold");
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Switch_To_Default();
		Click(Xpath, MA_Underlinebtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Assert.assertTrue(Get_Text(Xpath, containspath("span", "style", "underline")).equalsIgnoreCase("QA Test"),
				"QA Test' is not displayed in bold and Underlined");
		Switch_To_Default();
		Click(Xpath, MA_Italicbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "QA Test")),
				" 'QA Test' is not displayed in bold , Underlined and Italics");
		Type(Xpath, MA_AddAnnouncement_Path, Announcement);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Switch_To_Default();
		Click(Xpath, MA_Boldbtn_Path);
		Click(Xpath, MA_Underlinebtn_Path);
		Click(Xpath, MA_Italicbtn_Path);
		Datepicker2(Xpath, MA_AddStartdateCal_Path, Xpath, MA_AddStartdateCalmonth_Path, Get_Todaydate("MMM"),
				Get_Todaydate("d"), Get_Todaydate("YYYY"));
		Click(Xpath, MA_AddStartdateTime_Path);
		Click(Xpath, Textpath("a", "1:00 AM"));
		Datepicker2(Xpath, MA_AddinactivedateCal_Path, Xpath, MA_AddinactivedateCalmonth_Path, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		Click(Xpath, MA_AddinactivedateTime_Path);
		Click(Xpath, Textpath("a", "1:00 AM"));
		softAssert.assertTrue(Element_Is_Enabled(Xpath, MA_AddEMODUsersck_Path),
				"'EMOD Users' checkbox is not enabled for 'User Group' field ");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, MA_AddEnrollmentservices_Path),
				" 'Enrollment Services' checkbox is not disabled for 'User Group' field ");
		softAssert.assertAll();
		Click(Xpath, MA_AddEMODUsersck_Path);
		Type(Xpath, MA_AddSort_Path, "1");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MA_Cancelbtn_Path), "Cancel button is not displayed");
		Click(Xpath, MA_Createannouncementbtn_Path);
		Wait_ajax();
		Click(Xpath, Std_LP_Home_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MA_Createannouncement_Path(Announcement)),
				"'QA Test' is not displayed in 'Announcements' widget on  'Home' page ");

	}

	@Test(enabled = true, priority = 3, dependsOnMethods = "SI_TC_2_3_4_1_2")
	public void SI_TC_2_3_4_1_3() throws InterruptedException, AWTException {

		// Edit the Announcement

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Manageannouncements_path);
		ExplicitWait_Element_Visible(Xpath, MA_Title_Path);
		Type(Xpath, MA_Announcement_Path, Announcement);
		Selectfilter(MA_Announcementfilter_Path, "EqualTo");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", Announcement)),
				"Searched result 'QA Test' is not displayed under 'Announcement' column with 'Edit' link");
		Click(Xpath, MA_Edit1_Path);
		ExplicitWait_Element_Clickable(Xpath, MA_Updateammouncementbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Type(Xpath, MA_AddAnnouncement_Path, EditAnnouncement);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Switch_To_Default();
		Click(Xpath, MA_Boldbtn_Path);
		Click(Xpath, MA_Underlinebtn_Path);
		Click(Xpath, MA_Italicbtn_Path);
		Click(Xpath, MA_Boldbtn_Path);
		Click(Xpath, MA_Underlinebtn_Path);
		Click(Xpath, MA_Italicbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("body", EditAnnouncement)),
				"'QA Test  for Announcement' is displayed in bold/Underlined/Italics");
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		Switch_To_Default();
		Click(Xpath, MA_Hyperlinkbtn_Path);
		Switch_To_Iframe("Window");
		ExplicitWait_Element_Clickable(Xpath, Containstextpath("button", "OK"));
		Type(Xpath, MA_HyperlinkURL_Path, HyperlinkURL_Stage);
		Select_lidropdown(MA_HyperlinkTarget_Path, Xpath, "Browser Window");
		Type(Xpath, MA_HyperlinkTooltip_Path, "Home");
		Click(Xpath, Containstextpath("button", "OK"));
		Switch_To_Default();
		Click(Xpath, MA_Updateammouncementbtn_Path);
		Click(Xpath, Std_LP_Home_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MA_Editannouncement_Path(EditAnnouncement)),
				"QA Test  for Announcement' is not displayed in 'Announcements' widget on  'Home' page ");
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", EditAnnouncement)),
				"'View All Announcements' button not displayed  in 'Announcements' widget on 'Home' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, containspath("a", "title", "Home")),
				"'Home' tooltip text is not displayed");
		Click(Xpath, containspath("a", "title", "Home"));
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertTrue(Get_Title().contains("Landing Page"), "'Home' page is not displayed");

	}

	@Test(enabled = true, priority = 4)
	public void SI_TC_2_3_4_1_4() throws InterruptedException, AWTException {

		// Activate/Deactivate Announcements

		Announcement = "QA Test" + Get_Todaydate("MM/d/YYYY HH:mm:SS");
		NavigateMenu(Std_LP_Admin_path, Std_LP_Manageannouncements_path);
		ExplicitWait_Element_Visible(Xpath, MA_Title_Path);
		Click(Xpath, MA_Addannouncementbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, MA_Createannouncementbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, MA_Announcement_iFrame_ID));
		Type(Xpath, MA_AddAnnouncement_Path, Announcement);
		Switch_To_Default();
		Datepicker2(Xpath, MA_AddStartdateCal_Path, Xpath, MA_AddStartdateCalmonth_Path, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		Click(Xpath, MA_AddStartdateTime_Path);
		Click(Xpath, Textpath("a", "1:00 AM"));
		Datepicker2(Xpath, MA_AddinactivedateCal_Path, Xpath, MA_AddinactivedateCalmonth_Path, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		Click(Xpath, MA_AddinactivedateTime_Path);
		Click(Xpath, Textpath("a", "5:00 AM"));
		Click(Xpath, MA_AddEMODUsersck_Path);
		Type(Xpath, MA_AddSort_Path, "1");
		Click(Xpath, MA_Createannouncementbtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MA_Title_Path), "'Manage Announcements' page is not displayed");
		Type(Xpath, MA_Announcement_Path, Announcement);
		Selectfilter(MA_Announcementfilter_Path, "EqualTo");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Announcement)),
				"Searched result 'QA Testing' is not displayed under 'Announcement' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("td", Get_Futuredate("M/d/YYYY") + " 1:00:00 AM")),
				" <Future Date & Time- MM/DD/YYYY HH:MM:SS AM/PM> is  not displayed under 'Start Date' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("td", Get_Futuredate("M/d/YYYY") + " 5:00:00 AM")),
				" <Future Date & Time- MM/DD/YYYY HH:MM:SS AM/PM> is  not displayed under 'Inactive Date' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "1")),
				"'1' is not displayed under 'Sort' column");
		softAssert.assertFalse(Element_Is_selected(Xpath, MA_Onlyactiveck1_Path),
				"'Only Active' checkbox is checked off for 'QA Testing'");
		softAssert.assertAll();
		Click(Xpath, MA_Activate1_Path);
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)), "'QA Testing' is not displayed");
		NavigateMenu(Std_LP_Admin_path, Std_LP_Manageannouncements_path);
		ExplicitWait_Element_Visible(Xpath, MA_Title_Path);
		Type(Xpath, MA_Announcement_Path, Announcement);
		Selectfilter(MA_Announcementfilter_Path, "EqualTo");
		Click(Xpath, MA_Activate1_Path);
		Click(Xpath, Std_LP_Home_path);
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Testing' is displayed after deactivated");

	}

	@Test(enabled = true, priority = 5)
	public void SI_TC_2_3_4_1_5() throws InterruptedException, AWTException {

		// Validate Filtering Functionality

		NavigateMenu(Std_LP_Admin_path, Std_LP_Manageannouncements_path);
		ExplicitWait_Element_Visible(Xpath, MA_Title_Path);
		Itemof = Get_Text(Xpath, MA_Itemof_Path).trim();

		Type(Xpath, MA_Announcement_Path, "QA Test");
		Selectfilter(MA_Announcementfilter_Path, "Contains");
		softAssert.assertTrue(Get_Text(Xpath, MA_Announcement1_Path).trim().toLowerCase().contains("qa test"),
				"'QA Test' search result  is not displayed under  'Announcement'  column");
		Selectfilter(MA_Announcementfilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Annmouncement");

		Type(Xpath, MA_Startdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(MA_Startdatefilter_Path, "GreaterThan");
		softAssert.assertTrue(
				Get_Text(Xpath, MA_Startdate1_Path).trim().toLowerCase().contains(Get_Todaydate("M/d/YYYY")),
				" <MM/DD/YYYY>  search result is not displayed under 'Start Date' column");
		Selectfilter(MA_Startdatefilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Start Date");

		Type(Xpath, MA_Inactivedate_Path, Get_Futuredate("M/d/YYYY"));
		Selectfilter(MA_Inactivedatefilter_Path, "GreaterThan");
		softAssert.assertTrue(
				Get_Text(Xpath, MA_Inactivedate1_Path).trim().toLowerCase().contains(Get_Futuredate("M/d/YYYY")),
				" <MM/DD/YYYY>  search result is not displayed under 'Inactive Date' column");
		Selectfilter(MA_Inactivedatefilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Inactive date");

		Type(Xpath, MA_Sort_Path, "1");
		Selectfilter(MA_Sortfilter_Path, "EqualTo");
		softAssert.assertTrue(Get_Text(Xpath, MA_Sort1_Path).trim().toLowerCase().contains("1"),
				"'1' search result  is displayed under  'Sort'  column");
		Selectfilter(MA_Sortfilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Sort");

		Click(Xpath, MA_OnlyActiveck_Path);
		Selectfilter(MA_OnlyActivefilter_Path, "EqualTo");
		softAssert.assertTrue(Element_Is_selected(Xpath, MA_Onlyactiveck1_Path),
				"only the checked rows are displayed under the 'Only Active' column");
		Selectfilter(MA_OnlyActivefilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - OnlyActive");

		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 6)
	public void SI_TC_2_3_4_1_6() throws InterruptedException, AWTException {

		// Validate Sorting functionality

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		ClickHome();

	}

}
