package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class BasicPostCardTest extends BaseTestStandard {

	// Verify user is able to upload mailist for postcard piece (Basic)
	@Test
	public void SI_TC_2_6_1_5_10ANDSI_TC_2_6_1_5_11() throws InterruptedException, AWTException, IOException {
		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Std_COP_Items_Postcard_Path).trim().equalsIgnoreCase("Postcards Accident (AI)*"),
				"Postcards Accident(AI)* is  not highlighted on left side of the page under 'Items' header");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Click(Xpath, Radioxpathfromlabel("Announce Enrollment Dates (Only complete I. ENROLLMENT SECTION below)"));
		Select_DropDown_VisibleText(ID, Std_COP_EnrollmentHeader_ID, "Open Enrollment Is Coming Soon");
		StandardDatePicker(ID, Std_COP_EnrollmentStartDate_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));
		StandardDatePicker(ID, Std_COP_EnrollmentEndDate_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));

		// Maillist upload steps 22-32
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist6path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Assert.assertTrue(Get_Text(ID, Std_COP_Maillist_uploadMSg_ID).equalsIgnoreCase(Std_COP_Maillist_uploadMSg),
				" Mail list uploaded message is not displayed in red color");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "4")).equalsIgnoreCase("0"),
				"0 is Not displayed under 'State' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "5")).equalsIgnoreCase("00000"),
				"00000 is Not displayed under 'Zip' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "6")).equalsIgnoreCase("False"),
				"False is Not displayed under 'Required Fields' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "7")).equalsIgnoreCase("True"),
				"True is Not displayed under 'Is Valid' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();

		Assert.assertTrue(
				Get_Attribute(ID, Std_COP_EmployeeMailList_textField_ID, "value")
						.equalsIgnoreCase("6 Recipients Uploaded."),
				"6 Recipients Uploaded. is not displayed in 'Employee Mail List' field");

		// To remove uploaded maillist steps 32-47
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Click(ID, Std_COP_Maillist_RemoveBtn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Text(ID, Std_COP_Maillist_uploadMSg_ID).equalsIgnoreCase(Std_COP_Maillist_RemoveMsg),
				"Mail list removed message was not displayed in red color");
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist5path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();
		Assert.assertTrue(
				Get_Attribute(ID, Std_COP_EmployeeMailList_textField_ID, "value")
						.equalsIgnoreCase("5 Recipients Uploaded."),
				"5 Recipients Uploaded. is not displayed in 'Employee Mail List' field");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		// steps 48
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_ProofBtn_Path);
		Assert.assertTrue(Get_Attribute(ID, Std_SCP_FirstQty_ID, "value").equalsIgnoreCase("5"),
				"5 is displayed in 'Qty' field for 'Postcards Accident (AI)' piece");

		Assert.assertTrue(Get_Attribute(ID, Std_SCP_FirstQty_ID, "disabled").equalsIgnoreCase("disabled"),
				"User is  able to edit the value in the 'Qty' field");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EnvelopIcon_Path), "Envelope icon was not displayed");
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Next_Btn_Path);

		// steps-57
		Click(ID, Std_SCP_Datepicker_ID);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");

		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");

		Click(ID, Std_SCP_Checkout_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		Click(ID, Std_OC_OKBtn_ID);

	}

	public void SI_TC_2_6_1_5_12ANDSI_TC_2_6_1_5_13() throws IOException, InterruptedException, AWTException {

		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));

		// adding basic life to cart
		Type(ID, Std_SR_SearchMaterialsBy_Txt_ID, BasicLife);
		Click(ID, Std_SR_SearchMaterialsBy_Btn_ID);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(BasicLife));

		// Adding travel assistance Flyer
		Type(ID, Std_SR_SearchMaterialsBy_Txt_ID, TravelAssistanceFlyer);
		Click(ID, Std_SR_SearchMaterialsBy_Btn_ID);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(TravelAssistanceFlyer));

		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();

		// Steps 22
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Click(Xpath, Radioxpathfromlabel("Announce Meeting Dates (Only complete III. MEETING SECTION below)"));
		Select_DropDown_VisibleText(ID, Std_COP_SelectMeeting1Header_ID, "Attend a meeting:");
		StandardDatePicker(ID, Std_COP_SelectMeeting1Date_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));
		Type(ID, Std_COP_SelectMeeting1Time_ID, "3:00");
		Click(ID, Std_COP_SelectMeeting1PM_ID);
		Type(ID, Std_COP_SelectMeeting1Location_ID, "Conference Room");

		// Maillist upload
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist5path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();

		// Image Upload
		Click(ID, Std_COP_LogoUpload_Btn_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_FileUpload_ID);
		Fileupload(StandardLogopath);
		Click(ID, Std_COP_UploadBtn_ID);
		Assert.assertTrue(
				Get_Text(ID, Std_COP_UploadMessage_ID).trim()
						.equalsIgnoreCase("Standard_Logo.jpg successfully uploaded"),
				"The message Standard_Logo.jpg successfully uploaded is not displayed next to 'Clear' button");
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		// Basic Life {with AD&D} � B@G* options select

		StandardDatePicker(ID, Std_COP_BLPolicyEffectiveDate_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));
		Select_DropDown_VisibleText(ID, Std_COP_BLEmployeeScedule_ID, "Flat Amount");
		Select_DropDown_VisibleText(ID, Std_COP_BLIncludeEmployeeLanguage_ID, "None");
		Select_DropDown_VisibleText(ID, Std_COP_BLExcludeEmployeeLanguage_ID, "None");
		Type(ID, Std_COP_BLNumberOfWorkHoursPerWeek_ID, "20");
		Select_DropDown_VisibleText(ID, Std_COP_BLHoursFrequency_ID, "week");

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Send"));
		Assert.assertTrue(
				Get_DropDown(Xpath, Std_SCP_Send_Sendto_Path).equalsIgnoreCase("Enrollment Services Approvals"),
				"'Enrollment Services Approvals' is not displayed  by default in 'Send To' drop down");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approval Requested");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Kindly request you to approve");
		Click(Xpath, Textpath("a[1]", "Send"));
		Click(Xpath, Textpath("a", "Logout"));
		// admin

		login(AdminUsername, AdminPassword);

		Click(Xpath, Std_LP_Bell_Path);

		Click(Xpath, AC_Acquirebtn1_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_SendBtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Textpath("a", "Send"));
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Sendto_Path, "automation, qaautobasic1");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_Action_Path, "Approved");
		Select_DropDown_VisibleText(Xpath, Std_SCP_Send_PolicyType_Path, "New Group With Sold Proposal ");
		Type(Xpath, Std_SCP_Send_Message_Path, "Approved");
		Click(Xpath, Textpath("a[1]", "Send"));

		Click(Xpath, Textpath("a", "Logout"));

		// Login as basic user again
		login(BasicUserName, BasicPassword);

		// Step - 82

		Click(ID, Std_LP_MyCart_ID);

		Click(Xpath, Std_SCP_ExpandAllCart_Path);

		// delete basic life
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ErrorMsg_path),
				"Order has a mix of normal and maillist items- pop up message is displayed");
		Click(Xpath, Std_SCP_EP_OKBtn_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Basic Life {with AD&D} � B@G", "Remove"));
		Accept_Alert();

		// delete travel assistance flyer
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ErrorMsg_path),
				"Order has a mix of normal and maillist items- pop up message is displayed");
		Click(Xpath, Std_SCP_EP_OKBtn_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Travel Assistance Flyer", "Remove"));
		Accept_Alert();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);

		Click(Xpath, Std_SCP_Next_Btn_Path);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");
		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");
		Click(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
		Click(ID, Std_OC_OKBtn_ID);
		Wait_ajax();

		Type(Xpath, Std_LP_OL_OrderNumber_Path, OrderNumber);
		Click(Xpath, Std_LP_OL_CopyBtn_Path);

		Click(Xpath, Std_SCP_ExpandAllCart_Path);

		Click(Xpath, Selectbuttonshoppingcartbytitle("Postcards Accident (AI)", "Remove"));
		Accept_Alert();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ItemremovedMsg_Path),
				"Item(s) have been removed from your cart. message is displayed");

	}

	@BeforeTest
	public void PostcardLogin() throws IOException, InterruptedException {
		login(BasicUserName, BasicPassword);
	}

}
