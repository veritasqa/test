package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class FullfillmentOrder_EndtoEnd extends BaseTestStandard {

	@Test(enabled = true, priority = 1)
	public void SI_TC_1_0_2_1() throws InterruptedException {

		// Verify the error message is displayed upon "Invalid user name and
		// password" combination

		Click(Xpath, Std_Login_AdminloginBTN_Path);
		Type(ID, Std_Login_UserName_ID, AdminUsername);
		Type(ID, Std_Login_Password_ID, "sample");
		Click(ID, Std_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_InvalidCredentials_Msg_path),
				"Invalid User crediential Message is not Displayed");
	}

	@Test(enabled = true, priority = 2)
	public void SI_TC_1_0_2_2() throws InterruptedException {

		// Verify the error message when user login without username

		Click(Xpath, Std_Login_AdminloginBTN_Path);
		Clear(ID, Std_Login_UserName_ID);
		Type(ID, Std_Login_Password_ID, AdminPassword);
		Click(ID, Std_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_Login_Username_ErrorMsg_path),
				"UserName Error Message not Displayed");
	}

	@Test(enabled = true, priority = 3)
	public void SI_TC_1_0_1_2() throws InterruptedException {

		// verify Active user login

		Type(ID, Std_Login_UserName_ID, AdminUsername);
		Type(ID, Std_Login_Password_ID, AdminPassword);
		Click(ID, Std_Login_LoginBtn_ID);
		ExplicitWait_Element_Visible(ID, Std_LP_Announcement_Tab_ID);
		Assert.assertTrue(Element_Is_Displayed(ID, Std_LP_Announcement_Tab_ID),
				"Announcement tab is missing in landing page");
		Assert.assertTrue(Get_Text(ID, Std_LP_Username_ID).trim().equalsIgnoreCase("Qaauto Automation"),
				"Username is missing Landing page");
	}

	@Test(priority = 4, enabled = true, dependsOnMethods = "SI_TC_1_0_1_2")
	public void SI_TC_2_6_1_2_1() throws InterruptedException, IOException {

		// Validate an order is placed
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase().equalsIgnoreCase(
				Critical_illness_Partname), Critical_illness_Partname + " is not displayed in search results");
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Critical_illness_Partname)),
				"'Critical Illness All States or Specified Disease NY & VT � CH' is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		Wait_ajax();
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);
		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Visible(Xpath, Std_SCP_Deliverymethod_Path);
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		logout();

	}

	@BeforeClass
	public void Beforeclass() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void BeforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod()
	public void AfterMethod() throws InterruptedException {

	}

	@AfterClass()
	public void Afterclass() throws InterruptedException {

	}

}
