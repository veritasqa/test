package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class AdminPostcard extends BaseTestStandard {

	// Verify user is able to upload mailist for postcard piece (Admin)
	@Test
	public void SI_TC_2_6_1_5_1ANDSI_TC_2_6_1_5_3() throws InterruptedException, AWTException {
		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Std_COP_Items_Postcard_Path).trim().equalsIgnoreCase("Postcards Accident (AI)*"),
				"Postcards Accident(AI)* is  not highlighted on left side of the page under 'Items' header");
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Click(Xpath, Radioxpathfromlabel("Announce Enrollment Dates (Only complete I. ENROLLMENT SECTION below)"));
		Select_DropDown_VisibleText(ID, Std_COP_EnrollmentHeader_ID, "Open Enrollment Is Coming Soon");
		StandardDatePicker(ID, Std_COP_EnrollmentStartDate_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));
		StandardDatePicker(ID, Std_COP_EnrollmentEndDate_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));

		// Maillist upload steps 22-32
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist6path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Assert.assertTrue(Get_Text(ID, Std_COP_Maillist_uploadMSg_ID).equalsIgnoreCase(Std_COP_Maillist_uploadMSg),
				" Mail list uploaded message is not displayed in red color");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "4")).equalsIgnoreCase("0"),
				"0 is Not displayed under 'State' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "5")).equalsIgnoreCase("00000"),
				"00000 is Not displayed under 'Zip' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "6")).equalsIgnoreCase("False"),
				"False is Not displayed under 'Required Fields' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Assert.assertTrue(Get_Text(Xpath, MailcontactPath("Priyanka", "7")).equalsIgnoreCase("True"),
				"True is Not displayed under 'Is Valid' column for first name 'Priyanka' in the last row of 'Mail List Contents' grid");
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();

		Assert.assertTrue(
				Get_Attribute(ID, Std_COP_EmployeeMailList_textField_ID, "value")
						.equalsIgnoreCase("6 Recipients Uploaded."),
				"6 Recipients Uploaded. is not displayed in 'Employee Mail List' field");

		// To remove uploaded maillist steps 32-47
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Click(ID, Std_COP_Maillist_RemoveBtn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Text(ID, Std_COP_Maillist_uploadMSg_ID).equalsIgnoreCase(Std_COP_Maillist_RemoveMsg),
				"Mail list removed message was not displayed in red color");
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist5path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();
		Assert.assertTrue(
				Get_Attribute(ID, Std_COP_EmployeeMailList_textField_ID, "value")
						.equalsIgnoreCase("5 Recipients Uploaded."),
				"5 Recipients Uploaded. is not displayed in 'Employee Mail List' field");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		// SI.TC.2.6.1.5.3

		// Verify quantity field and Shipping Information page is working
		// appropriately (Admin)

		// steps 48
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_ProofBtn_Path);
		Assert.assertTrue(Get_Attribute(ID, Std_SCP_FirstQty_ID, "value").equalsIgnoreCase("5"),
				"5 is displayed in 'Qty' field for 'Postcards Accident (AI)' piece");

		Assert.assertTrue(Get_Attribute(ID, Std_SCP_FirstQty_ID, "disabled").equalsIgnoreCase("disabled"),
				"User is  able to edit the value in the 'Qty' field");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EnvelopIcon_Path), "Envelope icon was not displayed");
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Next_Btn_Path);

		// steps-57
		Click(ID, Std_SCP_Datepicker_ID);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");

		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");

		Click(ID, Std_SCP_Checkout_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		Click(ID, Std_OC_OKBtn_ID);

	}

	// Verify user is not able to checkout the post card piece with regular
	// order (Admin)
	@Test
	public void SI_TC_2_6_1_5_2ANDSI_TC_2_6_1_5_4() throws InterruptedException, AWTException {

		// Steps 1 to 21
		// adding postcard to cart
		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));

		// adding basic life to cart
		Type(ID, Std_SR_SearchMaterialsBy_Txt_ID, BasicLife);
		Click(ID, Std_SR_SearchMaterialsBy_Btn_ID);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(BasicLife));

		// Adding travel assistance Flyer
		Type(ID, Std_SR_SearchMaterialsBy_Txt_ID, TravelAssistanceFlyer);
		Click(ID, Std_SR_SearchMaterialsBy_Btn_ID);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(TravelAssistanceFlyer));

		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();

		// Steps 22
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Click(Xpath, Radioxpathfromlabel("Announce Meeting Dates (Only complete III. MEETING SECTION below)"));
		Select_DropDown_VisibleText(ID, Std_COP_SelectMeeting1Header_ID, "Attend a meeting:");
		StandardDatePicker(ID, Std_COP_SelectMeeting1Date_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));
		Type(ID, Std_COP_SelectMeeting1Time_ID, "3:00");
		Click(ID, Std_COP_SelectMeeting1PM_ID);
		Type(ID, Std_COP_SelectMeeting1Location_ID, "Conference Room");

		// Maillist upload
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist5path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();

		// Image Upload
		Click(ID, Std_COP_LogoUpload_Btn_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_FileUpload_ID);
		Fileupload(StandardLogopath);
		Click(ID, Std_COP_UploadBtn_ID);
		Assert.assertTrue(
				Get_Text(ID, Std_COP_UploadMessage_ID).trim()
						.equalsIgnoreCase("Standard_Logo.jpg successfully uploaded"),
				"The message Standard_Logo.jpg successfully uploaded is not displayed next to 'Clear' button");
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		// Basic Life {with AD&D} � B@G* options select

		StandardDatePicker(ID, Std_COP_BLPolicyEffectiveDate_ID, Get_Futuredate("MMM"), Get_Futuredate("YYYY"),
				Get_Futuredate("d"));
		Select_DropDown_VisibleText(ID, Std_COP_BLEmployeeScedule_ID, "Flat Amount");
		Select_DropDown_VisibleText(ID, Std_COP_BLIncludeEmployeeLanguage_ID, "None");
		Select_DropDown_VisibleText(ID, Std_COP_BLExcludeEmployeeLanguage_ID, "None");
		Type(ID, Std_COP_BLNumberOfWorkHoursPerWeek_ID, "20");
		Select_DropDown_VisibleText(ID, Std_COP_BLHoursFrequency_ID, "week");

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		// shopping cart
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EnvelopIcon_Path), "Envelope icon was not displayed");
		Click(Xpath, Selectbuttonshoppingcartbytext("Basic Life {with AD&D} � B@G", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();

		Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();
		// Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Basic Life {with AD&D} � B@G", "Approve"));

		// step 59
		// delete basic life
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ErrorMsg_path),
				"Order has a mix of normal and maillist items- pop up message is displayed");
		Click(Xpath, Std_SCP_EP_OKBtn_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Basic Life {with AD&D} � B@G", "Remove"));
		Accept_Alert();

		// delete travel assistance flyer
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ErrorMsg_path),
				"Order has a mix of normal and maillist items- pop up message is displayed");
		Click(Xpath, Std_SCP_EP_OKBtn_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Travel Assistance Flyer", "Remove"));
		Accept_Alert();
		Click(Xpath, Std_SCP_CheckoutBtn_Path);

		// step-
		Click(Xpath, Std_SCP_Next_Btn_Path);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");
		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");
		Click(ID, Std_SCP_Checkout_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
		Click(ID, Std_OC_OKBtn_ID);

		// SI.TC.2.6.1.5.4
		// Verify Copy button is working fine for Order look up widget (Admin)
		Type(Xpath, Std_LP_OL_OrderNumber_Path, OrderNumber);
		Click(Xpath, Std_LP_OL_CopyBtn_Path);

		Click(Xpath, Std_SCP_ExpandAllCart_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Postcards Accident (AI)", "Remove"));
		Accept_Alert();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_EP_ItemremovedMsg_Path),
				"Item(s) have been removed from your cart. message is not displayed");

	}

	// Verify postcard order will go on hold if user selects modification option
	// (Admin)
	@Test
	public void SI_TC_2_6_1_5_5AndI_TC_2_6_1_5_6AndI_TC_2_6_1_5_7AndI_TC_2_6_1_5_8AndI_TC_2_6_1_5_9()
			throws InterruptedException, AWTException {
		FulfilmentSearch(Postcard);
		GroupSearch(VerQA1);
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		ExplicitWait_Element_Visible(Xpath, MSC_ProductText(Postcard));
		Click(ID, Std_SR_MSC_CheckoutBtn_ID);
		Wait_ajax();

		Click(Xpath, Std_COP_ExpandBTN_Path);
		Click(Xpath, Radioxpathfromlabel(
				"Provide a URL to one of the following: Groups Web page, DST, Enrollment booklet, Coverage Highlights (Only complete II. URL SECTION)"));

		// UrlSection Step-18

		Click(ID, Std_COP_Yesbtn_ID);
		Type(ID, Std_COP_URLtxtBox_ID, "www.standard.com");
		Type(ID, Std_COP_ReturnAdressName_ID, "Veritas");
		Type(ID, Std_COP_ReturnAd_Dept_ID, "Quality");
		Type(ID, Std_COP_Returnad_Street_ID, "913 Commerce Street");
		Type(ID, Std_COP_ReturnAd_City_ID, "Buffalo Grove");
		Type(ID, Std_COP_ReturnAd_State_ID, "IL");
		Type(ID, Std_COP_ReturnAd_Zip_ID, "60089");

		// Steps-26
		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist5path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();

		// Step 40
		Click(Xpath, Std_SCP_CheckoutBtn_Path);

		Click(ID, Std_SCP_Modification_Radio_ID);

		Click(Xpath, Std_SCP_Next_Btn_Path);

		Click(ID, Std_SCP_Datepicker_ID);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");

		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");

		Click(ID, Std_SCP_Checkout_ID);

		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));

		Assert.assertTrue(Get_Text(ID, Std_OC_OrderStatus_ID).trim().equalsIgnoreCase("HOLD"),
				"HOLD button not displayed for 'Status' field");
		Click(ID, Std_OC_CopyBtn_ID);

		Click(ID, Std_SCP_ExpandAllCart_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Postcards Accident (AI)", "Edit Variables"));
		Click(Xpath, Std_SCP_variableYes_Path);

		// step-63
		Click(Xpath, Std_COP_ExpandBTN_Path);

		Click(ID, Std_COP_EmployeeMailList_Upload_ID);
		Switch_To_Iframe(Std_COP_FrameName);
		Thread.sleep(5000);
		Click(ID, Std_COP_Maillselect1_ID);
		Fileupload(PostcardMaillist5path);
		Click(ID, Std_COP_Maillist_UploadBTN_ID);
		ExplicitWait_Element_Not_Visible(ID, Std_COP_Maillist_fetchingsign_ID);
		Click(ID, Std_COP_Maillist_CloseForm_ID);
		Thread.sleep(5000);
		Switch_To_Default();

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();

		Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();

		Click(Xpath, Std_SCP_CheckoutBtn_Path);

		Click(ID, Std_SCP_Personalisation_Radio_ID);

		Click(Xpath, Std_SCP_Next_Btn_Path);

		Click(ID, Std_SCP_Datepicker_ID);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");

		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");

		Click(ID, Std_SCP_Checkout_ID);

		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		OrderNumber = Get_Text(ID, Std_OC_OrderNumder_ID);
		Assert.assertTrue(Get_Text(ID, Std_OC_OrderStatus_ID).trim().equalsIgnoreCase("HOLD"),
				"HOLD button not displayed for 'Status' field");
		Click(ID, Std_OC_OKBtn_ID);

		// SI.TC.2.6.1.5.8

		// Verify Copy button is working fine for My Recent Orders widget
		// (Admin)

		OrderCopyORViewBtn(OrderNumber, "Copy");

		Click(ID, Std_SCP_ExpandAllCart_Path);
		Click(Xpath, Selectbuttonshoppingcartbytitle("Postcards Accident (AI)", "Edit Variables"));
		Click(Xpath, Std_SCP_variableYes_Path);

		Click(Xpath, Std_COP_ExpandBTN_Path);

		Type(ID, Std_COP_InstructionsText_ID, "Qa Test");

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		// SI.TC.2.6.1.5.9

		// Verify user is able to place an order for Postcard without uploading
		// mailist (Admin)

		Click(ID, Std_SCP_ExpandAllCart_Path);
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Selectbuttonshoppingcartbytitle("Postcards Accident (AI)", "Update"));

		Click(Xpath, Selectbuttonshoppingcartbytext("Postcards Accident (AI)", "Proof"));
		Switch_New_Tab();
		Switch_Old_Tab();

		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_Next_Btn_Path);

		Click(ID, Std_SCP_Datepicker_ID);
		Datepicker(Std_SCP_Datepicker_Month_ID, Std_SCP_Datepicker_OKBtn_ID);
		Assert.assertTrue(Get_Text(ID, Std_SCP_MailingNote_ID).trim().equalsIgnoreCase(Std_SCP_MailingNote),
				"Mailing note Below delivery date mismatch");

		Assert.assertTrue(Get_Text(ID, Std_SCP_Deliverymethod_ID).trim().equalsIgnoreCase("USPS"),
				"USPS is Not displayed for 'Delivery Method' field");

		Click(ID, Std_SCP_Checkout_ID);

		Assert.assertTrue(Get_Text(Xpath, Std_OC_Quantity_Path).trim().equalsIgnoreCase("(5/0)"),
				"5 is displayed under 'Ordered' column");
		OrderNumberlist.add(Get_Text(ID, Std_OC_OrderNumder_ID));
		Click(ID, Std_OC_OKBtn_ID);

	}

	@BeforeTest
	public void PostcardLogin() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
	}

	// *[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl13_btnUpdate']//following::a[@title="Approved"]
	// *[text()='Basic Life {with AD&D} � B@G']//following::a[@title="Approve"]

}
