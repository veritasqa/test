package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class FullFillmentOrder extends BaseTestStandard {

	@Test(priority = 1, enabled = true)
	public void SI_TC_2_6_1_2_1() throws InterruptedException {

		// Validate an order is placed
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase().equalsIgnoreCase(
				Critical_illness_Partname), Critical_illness_Partname + " is not displayed in search results");
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Critical_illness_Partname)),
				"'Critical Illness All States or Specified Disease NY & VT � CH' is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_ProofedBtn_Path),
				" 'Proof' button name is not changed to 'Proofed'");
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		// ExplicitWait_Element_Visible(Xpath, Std_SCP_ApprovedBtn_Path);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Std_SCP_ApprovedBtn_Path).equalsIgnoreCase("#85c07f"),
				"the 'Approve' button color is not changed to 'green'");
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);

		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Visible(Xpath, Std_SCP_Deliverymethod_Path);
		//Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("New"),
				"New is not displayed in status");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);

	}

	@Test(priority = 2, enabled = true)
	public void SI_TC_2_6_1_2_2() throws InterruptedException {

		// Validate on confirmation page submit a cancel request and verify a
		// support ticket is created

		ExplicitWait_Element_Clickable(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		Click(Xpath, Std_CP_Submitcancelrequestbtn_Path);
		Assert.assertTrue(Alert_Text().equalsIgnoreCase("Do you want to cancel this order?"),
				"Do you want to cancel this order? alert is not displayed");
		Accept_Alert();
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Cancelreqsubmittedclose_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_Cancelreqsubmittedmesg_Path),
				"An order cancel request has been submitted' pop up message is not displayed with 'Close' button");
		Click(Xpath, Std_CP_Cancelreqsubmittedclose_Path);

	}

	@Test(priority = 3, enabled = true)
	public void SI_TC_2_6_1_2_3AndSI_TC_2_6_1_2_4() throws InterruptedException {

		// Validate cancel request ticket appears on support page

		System.out.println(OrderNumber + "Order No");
		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Wait_ajax();
		Type(Xpath, ST_OrderNotxt_Path, OrderNumber);
		Selectfilter(ST_OrderNofilter_Path, "Contains");
		Assert.assertEquals(OrderNumber, Get_Text(Xpath, ST_VT_OrderNo1_Path),
				"the search result for the <Order #>  entered is not displayed under the  'Order #' column - Veritas ticket");

		// Verify cancel request ticket works appropriately

		Click(Xpath, ST_VT_Edit1_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_UpdateTicketbtn_Path);
		Supporttickenumber = Get_Text(Xpath, STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		Reporter.log("Created Support Ticket# is " + Supporttickenumber);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, STM_NewStatus_Path).equalsIgnoreCase("#8BC67D"),
				"New Status is not displayed");
		Click(Xpath, STM_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, STM_Createcomment_Path);
		Type(Xpath, STM_Commenttext_Path, "QA Test");
		Click(Xpath, STM_Createcomment_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ST_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, STM_Comment1_Path).equalsIgnoreCase("QA Test"),
				"'QA Test' is not displayed for 'Comment' column");
		Click(Xpath, STM_UpdateTicketbtn_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");
		Click(Xpath, STM_Closedstatus_Path);
		ExplicitWait_Element_Visible(Xpath, STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

		NavigateMenu(Std_LP_Admin_path, Std_LP_Support_path);
		Assert.assertNotEquals(OrderNumber, Get_Text(Xpath, ST_VT_OrderNo1_Path),
				"the closed <Ticket #> is displayed in 'Change Request : Veritas Tickets' grid");
		Type(Xpath, ST_Statustxt_Path, "Closed");
		Selectfilter(ST_Statusfilter_Path, "Contains");
		Assert.assertEquals(OrderNumber, Get_Text(Xpath, ST_VT_OrderNo1_Path),
				"the closed <Ticket #> is not displayed in 'Change Request : Veritas Tickets' grid");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {
		login(AdminUsername, AdminPassword);
		Deletecarts();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

	}

}
