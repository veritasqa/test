package com.Standard.RegressionTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Standard.Base.BaseTestStandard;

public class SpecialProjects extends BaseTestStandard {

	public String Duedate;

	public void Addsplproject() throws InterruptedException {

		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		SplprjTicketNumber = Get_Text(Xpath, SPP_Gen_TicketNo_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		System.out.println("Created ticket number is " + SplprjTicketNumber);
		Reporter.log("Created ticket number is " + SplprjTicketNumber);

	}

	public String ClickViewEdit(String Ticketno) throws InterruptedException {

		return ".//a[text()='" + Ticketno + "']//following::a[1][text()='View/Edit']";
	}

	public String Status(String Ticketno) throws InterruptedException {

		return ".//a[text()='" + Ticketno + "']//following::td[4]";
	}

	public String History(String Changehistory) {

		return ".//td[contains(text(),'" + Changehistory + "')]";
	}

	@Test(enabled = false, priority = 1)

	public void SI_TC_2_3_11_1_3() throws InterruptedException {

		// Verify "Add Ticket", "View/Edit", "Refresh" button displays on the
		// Special Projects table.

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Addticketicon_Path),
				"'Add Ticket' button is not displays on the bottom of the 'Special Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Refreshicon_Path),
				" 'Refresh' button is not displays on the bottom of the 'Special Projects' page");
		// softAssert.assertTrue(Element_Is_Displayed(Xpath,
		// SPP_ViewEdit1_Path),
		// " 'View/Edit' button is not displays for all the rows in 'Special
		// Projects : View Tickets' grid on the 'Special Projects' page");
		softAssert.assertAll();
	}

	@Test(enabled = false, priority = 2)
	public void SI_TC_2_3_11_1_1() throws InterruptedException, AWTException {

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Addsplproject();
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Duedate = Get_Attribute(Xpath, SPP_Prog_Duedate_Path, "value").trim();
		Wait_ajax();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Type(Xpath, SPP_TicketNo_Path, SplprjTicketNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"<Ticket Number>  is not displayed under  'Ticket #' column  in 'Special Projects : View Tickets' grid ");
		Clear(Xpath, SPP_TicketNo_Path);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Type(Xpath, SPP_Jobtitle_Path, "QA Test");
		Click(Xpath, SPP_JobtitleFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_JobtitleFilter_Path, "EqualTo");
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtitle1_Path).trim().equalsIgnoreCase("QA Test"),
				" 'QA Test' is not displayed under 'Job Title' column in 'Special Projects : View Tickets' grid ");
		Wait_ajax();
		Selectfilter(SPP_JobtitleFilter_Path, "NoFilter");
		Select_lidropdown(SPP_Jobtypearrow_Path, Xpath, "Other");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Other"),
				" 'Other' is not displayed in all rows under 'Job Type' column in 'Special Projects : View Tickets' grid ");
		Select_lidropdown(SPP_Jobtypearrow_Path, Xpath, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Select_lidropdown(SPP_Statusarrow_Path, Xpath, "New");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Status1_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displayed in all rows under 'Status' column in 'Special Projects : View Tickets' grid ");
		Wait_ajax();
		Select_lidropdown(SPP_Statusarrow_Path, Xpath, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Type(Xpath, SPP_Duedate_Path, Duedate);
		Click(Xpath, SPP_DuedateFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_DuedateFilter_Path, "EqualTo");
		Assert.assertTrue(Get_Text(Xpath, SPP_Duedate1_Path).trim().equalsIgnoreCase(Duedate),
				"Entered  <MM/DD/YYYY>  is not displayed under 'Due Date' column in 'Special Projects : View Tickets' grid  ");
		Selectfilter(SPP_DuedateFilter_Path, "NoFilter");
		Click(Xpath, SPP_Rushck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, SPP_Rushck1_Path),
				"only the checked checkboxes rows are not displayed under  'Rush' column in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_Rushck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, SPP_Closedck1_Path),
				"only the checked checkboxes rows are not displayed under  'Closed' column in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);

	}

	@Test(enabled = false, priority = 3)
	public void SI_TC_2_3_11_4() throws InterruptedException {

		// Validate "Add ticket button",
		// Edit/View button takes to "General Special projects" page

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, SPP_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job title' field is not displays  in 'General' tab on 'Special Projects' page");
		Click(Xpath, SPP_Gen_Cancel_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_ViewEdit1_Path);
		Click(Xpath, SPP_ViewEdit1_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job title' field is not displays  in 'General' tab on 'Special Projects' page");
		Click(Xpath, SPP_Gen_Cancel_Path);

	}

	@Test(enabled = false, priority = 4)
	public void SI_TC_2_3_11_1_5() throws InterruptedException {

		// Add a new special project and verify new ticket appears on Special
		// projects: view tickets table

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_TicketNo_Path),
				"New <Ticket Number> is not displayed for the 'Ticket #' field");
		SplprjTicketNumber = Get_Text(Xpath, SPP_Gen_TicketNo_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		System.out.println("Created ticket number is " + SplprjTicketNumber);
		Reporter.log("Created ticket number is " + SplprjTicketNumber);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				"New <Ticket Number> is not displayed under the 'Ticket #' column on the 'Special Projects: View Tickets ' grid");

	}

	@Test(enabled = false, priority = 5)
	public void SI_TC_2_3_11_1_7() throws InterruptedException {

		// Validate change history updates made

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA is Testing");
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		Click(Xpath, SPP_HistoryTab_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_History_DateTime1_Path).trim().contains(Get_Todaydate("M/d/yyyy")),
				"the date and time of the description added<MM/DD/YYYY HH:MM:SS AM/PM> is not displayed under 'Date/Time' column in the 'History' tab");
		Assert.assertTrue(Get_Text(Xpath, SPP_History_User1_Path).trim().equalsIgnoreCase(UserFName + " " + UserLName),
				"the <User's first name&Last name> is not displayed under the 'User' column in the 'History' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Job Title From")),
				"'Changed Description From '' To 'QA is Testing''  is not displayed under 'Details' Column in the 'History' tab - 1");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("QA is Testing")),
				"'Changed Description From '' To 'QA is Testing''  is not displayed under 'Details' Column in the 'History' tab - 2");
	}

	@Test(enabled = false, priority = 6)
	public void SI_TC_2_3_11_1_8() throws InterruptedException {

		// Verify changes are not saved on clicking Cancel button

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Type(Xpath, SPP_Gen_Description_Path, "QA is Retesting");
		Click(Xpath, SPP_Gen_Cancel_Path);
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		Assert.assertFalse(Get_Attribute(Xpath, SPP_Gen_Description_Path, "value").equalsIgnoreCase("QA is Retesting"),
				" 'QA is retesting' is displayed in the 'Description' field");

	}

	@Test(enabled = true, priority = 7)
	public void SI_TC_2_3_11_1_9() throws InterruptedException, AWTException {

		// Validate the cancel functionality

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Click(Xpath, SPP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
		Type(Xpath, SPP_Prog_Comment_Path, "'QA is validating cancel functionality");
		Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
		Click(Xpath, SPP_Prog_Attachment_Path);
		Fileupload(SI_TC_2_3_11_1_9_File);
		Wait_ajax();
		Click(Xpath, SPP_Prog_Submit_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Attachment1_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed by the 'Status' field on the header of the 'Progress/Files' tab");
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				" <Ticket #> is not displayed under the 'Ticket #' column on the 'Special Projects : View Tickets' grid");
		Assert.assertTrue(Get_Text(Xpath, Status(SplprjTicketNumber)).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed under 'Status' column for the copied <Ticket #> on 'Special Projects : View Tickets' grid");

	}

	@Test(enabled = true, priority = 7)
	public void SI_TC_2_3_11_1_6() throws InterruptedException, AWTException {

		// Validate all the Fields/Buttons of "General", Progress/Files, Costs
		// are enabled in Special Projects is working fine

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job title' field is not displays  in 'General' tab on 'Special Projects' page");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobTitle_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Job Title' field");
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA Test 123");
		softAssert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"'-Select-'  is not displays in 'Job Type' drop down field");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Description_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Description' field");
		Type(Xpath, SPP_Gen_Description_Path, "QA Test 123");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Gen_Createdby_Path).trim().equalsIgnoreCase(AdminUsername),
				" <username> is not displays for 'Created By' field and is read only");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Order No' field");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Costcenter_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Cost center' field");
		Type(Xpath, SPP_Gen_Costcenter_Path, "9999");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Splinst_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Special Instructions' field");
		Type(Xpath, SPP_Gen_Splinst_Path, "QA Test");
		softAssert.assertFalse(Element_Is_selected(Xpath, SPP_Gen_Isrush_Path), "'Is Rush?' checkbox is checked");
		Click(Xpath, SPP_Gen_Isrush_Path);
		softAssert.assertFalse(Element_Is_selected(Xpath, SPP_Gen_IsQuoteneeded_Path),
				"'Is Quote needed?' checkbox is checked");
		Click(Xpath, SPP_Gen_IsQuoteneeded_Path);
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page");
		softAssert.assertAll();
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Click(Xpath, SPP_Prog_Duedatecal_Path);
		Wait_ajax();
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						containspath("td", "class", "rcOutOfRange") + "//following::" + "span[text()='"
								+ Get_Pastdate("d") + "']"),
				"<Past Date - MM/DD/YYYY> from 'Calendar' icon is selectable ");
		Click(Xpath, Textpath("a", Get_Todaydate("d")));
		Wait_ajax();
		Click(Xpath, SPP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
		Type(Xpath, SPP_Prog_Comment_Path, "QA Test");
		Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
		Click(Xpath, SPP_Prog_Attachment_Path);
		Fileupload(SI_TC_2_3_11_1_6_File);
		Wait_ajax();
		Click(Xpath, SPP_Prog_Submit_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Attachment1_Path);
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		softAssert.assertAll();
		Click(Xpath, SPP_CostsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Costs_Addcostitemicon_Path);
		Type(Xpath, SPP_Costs_Quote_Path, "0.1");
		Type(Xpath, SPP_Costs_Printcost_Path, "0.1");
		Type(Xpath, SPP_Costs_VeritasPost_Path, "0.1");
		Type(Xpath, SPP_Costs_FFCost_Path, "0.1");
		Type(Xpath, SPP_Costs_ClientPost_Path, "0.1");
		Type(Xpath, SPP_Costs_Rushcost_Path, "0.1");
		Click(Xpath, SPP_Costs_Billedck_Path);
		Click(Xpath, SPP_Costs_Addcostitemicon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Costs_Insert_Path);
		Type(Xpath, SPP_Costs_Description_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SPP_Costs_Type_Path, "Veritas Postage");
		Datepicker2(Xpath, SPP_Costs_Billdatecal_Path, Xpath, SPP_Costs_BilldateCal_Month_Path, Get_FutureTime("MMM"),
				Get_FutureTime("d"), Get_FutureTime("YYYY"));
		Type(Xpath, SPP_Costs_Billammount_Path, "0.0");
		Click(Xpath, SPP_Costs_Insert_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Costs_Edit1_Path);
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Costs Tab");
		Click(Xpath, SPP_InventoryTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Inventory_Addoprojbtn_Path);
		Type(Xpath, SPP_Inventory_StockNo_Path, "AICH1216");
		Click(Xpath, ".//strong[text()='FormNumber:']");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "AICH1216")),
				"'AICH1216' is not displayed in 'Stock #'  field");
		Click(Xpath, SPP_Inventory_Addoprojbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Inventory_Delete1_Path);
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		softAssert.assertAll();
		Click(Xpath, SPP_HistoryTab_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_History_DateTime_Path),
				" 'Date/Time'  column not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_History_User_Path), " 'User'  column not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_History_Details_Path), " 'Details'  column not displays");
		softAssert.assertAll();
	}

	@Test(enabled = true, priority = 8)
	public void SI_TC_2_3_11_1_2() throws InterruptedException, AWTException {

		// Validate the combination of filter functionality

		// Pre-Condition

		Robot robot = new Robot();
		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);
		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		Type(Xpath, SPP_Gen_OrderNo_Path, "1737932");
		Click(Xpath, SPP_Gen_JobTitle_Path);
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		Thread.sleep(3000);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Isrush_Path);
		Click(Xpath, SPP_Gen_Isrush_Path);
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		SplprjTicketNumber = Get_Text(Xpath, SPP_Gen_TicketNo_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		System.out.println("Created ticket number is " + SplprjTicketNumber);
		Reporter.log("Created ticket number is " + SplprjTicketNumber);
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Type(Xpath, SPP_Prog_Duedate_Path, Get_Todaydate("MM/d/YYYY"));
		Click(Xpath, SPP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
		Type(Xpath, SPP_Prog_Comment_Path, "QA Test");
		Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);

		// Test case starts here

		NavigateMenu(Std_LP_Admin_path, Std_LP_Specialproject_path);

		Type(Xpath, SPP_TicketNo_Path, SplprjTicketNumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Type(Xpath, SPP_Jobtitle_Path, "QA");
		Click(Xpath, SPP_JobtitleFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_JobtitleFilter_Path, "Contains");
		Type(Xpath, SPP_Duedate_Path, Get_Todaydate("MM/d/YYYY"));
		Click(Xpath, SPP_DuedateFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_DuedateFilter_Path, "EqualTo");
		softAssert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"<Ticket Number>  is not displayed under  'Ticket #' column  in 'Special Projects : View Tickets' grid ");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Jobtitle1_Path).trim().equalsIgnoreCase("QA"),
				" 'QA' is not displayed under 'Job Title' column in 'Special Projects : View Tickets' grid ");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Duedate1_Path).trim().equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"Due date  <MM/DD/YYYY>  is not displayed under 'Due Date' column in 'Special Projects : View Tickets' grid  ");
		softAssert.assertAll();
		Clear(Xpath, SPP_TicketNo_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_JobtitleFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_DuedateFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"the selected filter result is not cleared and all the rows in the 'Special Projects : View Tickets' grid are displayed");
		Select_lidropdown(SPP_Jobtypearrow_Path, Xpath, "Other");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Select_lidropdown(SPP_Statusarrow_Path, Xpath, "New");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Other"),
				" 'Other' is not displayed in all rows under 'Job Type' column in 'Special Projects : View Tickets' grid ");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Status1_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displayed in all rows under 'Status' column in 'Special Projects : View Tickets' grid ");
		softAssert.assertAll();
		Wait_ajax();
		Select_lidropdown(SPP_Jobtypearrow_Path, Xpath, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Select_lidropdown(SPP_Statusarrow_Path, Xpath, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"the selected filter result is not cleared and all the rows in the 'Special Projects : View Tickets' grid are displayed - 2");
		Type(Xpath, SPP_OrderNo_Path, "1737932");
		Click(Xpath, SPP_OrderNoFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_OrderNoFilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_Rushck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, SPP_OrderNo1_Path).trim().equalsIgnoreCase("1737932"),
				"the entered <Order Number> is not displayed under the 'Order #' column");
		softAssert.assertTrue(Element_Is_selected(Xpath, SPP_Rushck1_Path),
				"only the checked checkboxes rows are not displayed under  'Rush' column in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_Rushck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_OrderNoFilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"the selected filter result is not cleared and all the rows in the 'Special Projects : View Tickets' grid are displayed - 3");

		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Type(Xpath, SPP_TicketNo_Path, Get_Text(Xpath, SPP_TicketNo1_Path));
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		softAssert.assertTrue(
				Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(Get_Text(Xpath, SPP_TicketNo1_Path)),
				"<Ticket Number>  is not displayed under  'Ticket #' column  in 'Special Projects : View Tickets' grid ");
		softAssert.assertTrue(Element_Is_selected(Xpath, SPP_Closedck1_Path),
				"only the checked checkboxes rows are not displayed under  'Closed' column in 'Special Projects : View Tickets' grid ");
		softAssert.assertAll();
		Clear(Xpath, SPP_TicketNo_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"the selected filter result is not cleared and all the rows in the 'Special Project - 4");
	}

	// ADD SP Test Cases

	@Test(enabled = false, priority = 9)
	public void SI_TC_2_3_11_1_1_1AndSI_TC_2_3_11_1_1_2() throws InterruptedException, AWTException {

		// Validate upon clicking "Add Special Projects" takes to General
		// Special projects

		Hover(Xpath, Std_LP_Admin_path);
		Hover(Xpath, Std_LP_Specialproject_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Add Special Project")),
				"'Add Special Project' is not displayed");
		Click(Xpath, Textpath("span", "Add Special Project"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job Title' field is not displayed  in 'General' tab on 'Special Projects' page");

		// SI_TC_2_3_11_1_1_2

		// Create a special project without entering data and Verify the error
		// messages

		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobTitle_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Job Title' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitleReq_Path),
				"A red asterisk * is not displayed by the 'Job Title' field on  'General' tab");
		softAssert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"'-Select-'  is not displays in 'Job Type' drop down field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTypeReq_Path),
				" a red asterisk * is not displayed by the 'Job Type' field on  'General' tab");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Description_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Description' field");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_OrderNo_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Order No' field");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Costcenter_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Cost center' field");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Splinst_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Special Instructions' field");
		softAssert.assertFalse(Element_Is_selected(Xpath, SPP_Gen_Isrush_Path), "'Is Rush?' checkbox is checked");
		softAssert.assertFalse(Element_Is_selected(Xpath, SPP_Gen_IsQuoteneeded_Path),
				"'Is Quote needed?' checkbox is checked");
		softAssert.assertAll();
		Click(Xpath, SPP_Gen_Create_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Jobtitle_Path),
				"Job Title IS Required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Jobtype_Path),
				"Job Type Is Required is not displayed");
		softAssert.assertAll();
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Fulfillment Order Request");
		Click(Xpath, SPP_Gen_Create_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"You must enter a value in the following fields: is not displayed - 2");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Jobtitle_Path),
				"Job Title IS Required is not displayed - 2");
		softAssert.assertAll();
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "- Select -");
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA");
		Click(Xpath, SPP_Gen_Create_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Title_Path),
				"You must enter a value in the following fields: is not displayed - 3");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Error_Jobtype_Path),
				"Job Type Is Required is not displayed - 3");
		softAssert.assertAll();
		Click(Xpath, SPP_ProgramTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_UnSuccessmsg_Path),
				" 'Create the Special Project, before proceeding.' error message is not displayed - Program Tab");
		Click(Xpath, SPP_CostsTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_UnSuccessmsg_Path),
				" 'Create the Special Project, before proceeding.' error message is not displayed - Costs Tab");
		Click(Xpath, SPP_InventoryTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_UnSuccessmsg_Path),
				" 'Create the Special Project, before proceeding.' error message is not displayed - Inventory Tab");
		Click(Xpath, SPP_HistoryTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_UnSuccessmsg_Path),
				" 'Create the Special Project, before proceeding.' error message is not displayed - History Tab");
		softAssert.assertAll();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(AdminUsername, AdminPassword);

	}

	@BeforeMethod
	public void beforeMethod() {

		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		ClickHome();

	}

}
