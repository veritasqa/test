package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class OrderLookUpTest extends BaseTestStandard {

	// Search by date selection
	@Test(enabled = false)
	public void SI_TC_2_5_1_1_1() throws InterruptedException {

		Click(Xpath, Std_LP_OL_FromDateicon_Path);
		DatepickerPastDate(Xpath, Std_LP_OL_FromDateMonth_Path, Std_LP_OL_CalenderOKBtn_ID);
		Click(Xpath, Std_LP_OL_ToDateicon_Path);
		DatepickerTodaysDate(Xpath, Std_LP_OL_ToDateMonth_Path, Std_LP_OL_CalenderOKBtn_ID);
		Wait_ajax();
		Click(Xpath, Std_LP_OL_Searchbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, Std_OS_FirstDate_Path).trim().contains(Get_Todaydate("M/d/YYYY")),
				"Verify the orders created between selected the 'From'<Past Date - MM/DD/YYYY> and 'To' <Current Date - MM/DD/YYYY> are displayed in 'Order Search' grid  on  'Order Search' page");
		Click(Xpath, Std_LP_Home_path);

	}

	@Test(enabled = true)
	public void SI_TC_2_5_1_1_2ANDSI_TC_2_5_1_1_3() throws InterruptedException {

		// Validate an order is placed
		Type(ID, Std_FulfilmentSearchField_ID, Critical_illness_Search);
		Click(ID, Std_FulfilmentSearchbtn_ID);
		ExplicitWait_Element_Clickable(ID, Std_GS_SearchButton_ID);
		Type(ID, Std_GS_Groupname_ID, "veri");
		Click(ID, Std_GS_SearchButton_ID);
		Click(Xpath, Std_GroupSelect(Veritas_QA_1_group));
		ExplicitWait_Element_Visible(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Assert.assertTrue(Get_Text(Xpath, Std_SR_SrchMetBy_Firstoption_path).trim().toLowerCase().equalsIgnoreCase(
				Critical_illness_Partname), Critical_illness_Partname + " is not displayed in search results");
		Click(Xpath, Std_SR_SrchMetBy_FirstAddcartBtn_path);
		Click(ID, Std_SR_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Std_COP_SaveAndContinueBTN_ID);
		Click(Xpath, Std_COP_ExpandBTN_Path);
		Select_DropDown_VisibleText(Xpath, Std_DSTQuestion_path, "No");
		Select_DropDown_VisibleText(Xpath, Std_CHPlanType_path, "Select");
		Select_DropDown_VisibleText(Xpath, Std_Underwriter_path, "Simplified Issue");
		Select_DropDown_VisibleText(Xpath, Std_HealthMaintenanceScreening_path, "50");
		Select_DropDown_VisibleText(Xpath, Std_Transportationbenefitamount_path, "150");
		Select_DropDown_VisibleText(Xpath, Std_Lodgingbenefitamount_path, "100");
		Select_DropDown_VisibleText(Xpath, Std_SampleClaimBenefitAmount_path, "$15,000");
		Select_DropDown_VisibleText(Xpath, Std_EmployeesPremiumDeductionFrequency_path, "Weekly");
		Select_DropDown_VisibleText(Xpath, Std_SelectAgeBands_path, "5-year Age bands");
		Select_DropDown_VisibleText(Xpath, Std_RateType_path, "Issue Age, Non-Tobacco/Tobacco Rates");

		Select_DropDown_VisibleText(Xpath, Std_Spousecoverageincludes_path, "Domestic Partner (DP)");
		Type(Xpath, Std_Hourlyrequirementtxt_path, "20");
		Select_DropDown_VisibleText(Xpath, Std_Hoursactivelyatworkfrequency_path, "Week");
		Select_DropDown_VisibleText(Xpath, Std_ReoccurrenceBeneft_path, "6");
		Select_DropDown_VisibleText(Xpath, Std_PreXperiod_path, "3/12");
		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Critical_illness_Partname)),
				"'Critical Illness All States or Specified Disease NY & VT � CH' is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_ProofedBtn_Path),
				" 'Proof' button name is not changed to 'Proofed'");
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		Wait_ajax();
		// ExplicitWait_Element_Visible(Xpath, Std_SCP_ApprovedBtn_Path);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Std_SCP_ApprovedBtn_Path).equalsIgnoreCase("#85c07f"),
				"the 'Approve' button color is not changed to 'green'");
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);

		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));

		ExplicitWait_Element_Visible(Xpath, Std_SCP_Deliverymethod_Path);
		Thread.sleep(5000);
		Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID, "Veritas-9999");

		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("New"),
				"New is not displayed in status");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Click(ID, Std_OC_OKBtn_ID);

		Wait_ajax();
		Type(Xpath, Std_LP_OL_OrderNumber_Path, OrderNumber);
		Click(Xpath, Std_LP_OL_viewBtn_Path);

		Assert.assertTrue(Get_Text(Xpath, Std_CP_OrderNo_Path).equalsIgnoreCase(OrderNumber), "OrdernumberMismatch");
		Click(ID, Std_OC_OKBtn_ID);

		Wait_ajax();
		Type(Xpath, Std_LP_OL_OrderNumber_Path, OrderNumber);
		Click(Xpath, Std_LP_OL_CopyBtn_Path);
		Wait_ajax();
		Click(Xpath, Std_SCP_ExpandAllCart_Path);
		Click(Xpath, Selectbuttonshoppingcartbytext(Critical_illness_Partname, "Variables"));

		Click(ID, Std_COP_SaveAndContinueBTN_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_CheckoutBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Critical_illness_Partname)),
				"'Critical Illness All States or Specified Disease NY & VT � CH' is not displayed in 'Shopping Cart' page");
		Click(Xpath, Std_SCP_ProofBtn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		ExplicitWait_Element_Visible(Xpath, Std_SCP_ProofedBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_SCP_ProofedBtn_Path),
				" 'Proof' button name is not changed to 'Proofed'");
		Click(Xpath, Std_SCP_ApproveBtn_Path);
		// ExplicitWait_Element_Visible(Xpath, Std_SCP_ApprovedBtn_Path);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, Std_SCP_ApprovedBtn_Path).equalsIgnoreCase("#85c07f"),
				"the 'Approve' button color is not changed to 'green'");
		Type(ID, Std_SCP_FirstQty_ID, "5");
		Click(Xpath, Std_SCP_UpdateBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Std_CP_Loading_Path);
		Click(Xpath, Std_SCP_CheckoutBtn_Path);
		Click(Xpath, Std_SCP_PrintandPdfonly_path);
		Type(ID, Std_SCP_SI_Name_ID, "QA Test");
		Type(ID, Std_SCP_SI_Address_ID, Address);
		Type(ID, Std_SCP_SI_City_ID, City);
		Select_DropDown_VisibleText(ID, Std_SCP_SI_State_ID, "Illinois");
		Type(ID, Std_SCP_SI_Zip_ID, Zip);
		Click(Xpath, Std_SCP_Next_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, Std_SCP_Editaddress_Btn_Path);

		Datepicker2(ID, Std_SCP_DeliveryDatepicker_ID, ID, Std_SCP_DeliveryDatepicker_Month_ID, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));

		ExplicitWait_Element_Visible(Xpath, Std_SCP_Deliverymethod_Path);

		// Select_DropDown_VisibleText(ID, Std_SCP_Shippingcostcenter_ID,
		// "Veritas-9999");
		Click(ID, Std_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(Xpath, Std_CP_Okbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, Std_CP_Status_Path).trim().toLowerCase().equalsIgnoreCase("New"),
				"New is not displayed in status");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Std_CP_OrderNo_Path), " 'Order No is not displayed");
		OrderNumber = Get_Text(Xpath, Std_CP_OrderNo_Path).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Click(ID, Std_OC_OKBtn_ID);

	}

	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException {

		login(AdminUsername, AdminUsername);
		Deletecarts();
	}

}
