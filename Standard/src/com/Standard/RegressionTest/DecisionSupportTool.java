package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Standard.Base.BaseTestStandard;

public class DecisionSupportTool extends BaseTestStandard{
    
	//Verify *New* Decision Support Tool widget displays on Home Page and displays appropriate text - basic User
	@Test
	public void SI_TC_2_8_1_2() throws IOException, InterruptedException{
		login(AdminUsername, AdminPassword);
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Std_LP_DST_Path),"*New* Decision Support Tool widget is displayed under 'Training Guidelines' widget on 'Home' page");
        Click(Xpath, Std_LP_DST_Collapse_Path);
        Click(Xpath, Std_LP_DST_Expand_Path);
	    Assert.assertTrue(Get_Text(ID, Std_LP_SupportMessage_ID).equalsIgnoreCase(Std_LP_SupportMessage),"Support Message missing");
        logout();
			
	}
	
	//Verify *New* Decision Support Tool widget displays on Home Page and displays appropriate text - Admin User
	@Test
	public void SI_TC_2_8_1_1() throws IOException, InterruptedException{
		login(BasicUserName, BasicPassword);
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Std_LP_DST_Path),"*New* Decision Support Tool widget is displayed under 'Training Guidelines' widget on 'Home' page");
        Click(Xpath, Std_LP_DST_Collapse_Path);
        Click(Xpath, Std_LP_DST_Expand_Path);
	    Assert.assertTrue(Get_Text(ID, Std_LP_SupportMessage_ID).equalsIgnoreCase(Std_LP_SupportMessage),"Support Message missing");
        logout();
			
	}
	
}
