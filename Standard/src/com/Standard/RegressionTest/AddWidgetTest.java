package com.Standard.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import com.Standard.Base.BaseTestStandard;

public class AddWidgetTest extends BaseTestStandard {

	// Select and add widget to landing page
	public void SI_TC_2_5_1_1() throws InterruptedException {

		Hover(Xpath, Std_LP_AddWidget_Path);
		Click(ID, Std_LP_Widget_Orderlookup_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_LP_Widget_OrderlookupTitle_Path).equalsIgnoreCase("Order Lookup"),
				"Order Lookup widget not displayed");
		Hover(Xpath, Std_LP_AddWidget_Path);
		Click(ID, Std_LP_Widget_RecentOrders_ID);
		Assert.assertTrue(Get_Text(Xpath, Std_LP_Widget_RecentOrdersTitle_Path).equalsIgnoreCase("My Recent Orders"),
				"My Recent Orders widget not displayed");

	}

	public void SI_TC_2_5_1_3() throws InterruptedException {
		Click(Xpath, Std_LP_OL_CollapseBtn_Path);
		Click(Xpath, Std_LP_OL_CollapseBtn_Path);
		Click(Xpath, Std_LP_OL_CloseBtn_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, Std_LP_Widget_OrderlookupTitle_Path),
				"Order Lookup widget  displayed");
		Hover(Xpath, Std_LP_AddWidget_Path);
		Click(ID, Std_LP_Widget_Orderlookup_ID);
		Click(Xpath, Std_LP_MRO_CollapseBtn_Path);
		Click(Xpath, Std_LP_MRO_CollapseBtn_Path);
		Click(Xpath, Std_LP_MRO_CloseBtn_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, Std_LP_Widget_RecentOrdersTitle_Path),
				"My Recent Orders widget  displayed");
		Hover(Xpath, Std_LP_AddWidget_Path);
		Click(ID, Std_LP_Widget_RecentOrders_ID);

	}

	@BeforeTest
	public void MyrecentOrdersLogin() throws IOException, InterruptedException {
		login(BasicUserName, BasicPassword);
	}
}
