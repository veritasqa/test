package com.Standard.OR;

public class SearchResultsPage extends OrderSearchPage {

	public static String Addtocart(String Piecename) {

		return ".//span[contains(text(),'" + Piecename + "')]//following::div[3]//a[contains(text(),'Cart')]";

	}

	// Search materials by - SrchMet
	public static final String Std_SR_SearchMaterialsBy_Txt_ID = "ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword";

	public static final String Std_SR_SearchMaterialsBy_Btn_ID = "ctl00_ctl00_cphContent_cphRightSideHeader_btnAdvancedSearch";

	public static final String Std_SR_SrchMetBy_Firstoption_path = "//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/div[1]/span[1]";
	public static final String Std_SR_SrchMetBy_FirstAddcartBtn_path = "//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]";

	public static final String Std_SR_SrchMetBy_FirstQTY_ID = "ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_txtQuantity";

	public static final String Std_SR_Checkout_ID = "ctl00_ctl00_cphContent_cphRightSideContent_btnSearchResultsCheckOut";

	// Mini Shopping Cart -MSC

	public static String MSC_ProductText(String Partname) {
		return "//a[text()='" + Partname + "']";

	}

	public static final String Std_SR_MSC_Qtyfield_ID = "ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_txtQuantity";
	public static final String Std_SR_MSC_UpdateBtn_ID = "ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnUpdate";
	public static final String Std_SR_MSC_CheckoutBtn_ID = "ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout";

}
