package com.Standard.OR;

public class ManageAnnouncements extends LoginPage {

	public static final String MA_Title_Path = ".//h1[contains(text(),'Manage Announcements')]";
	public static final String MA_Announcement_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']";
	public static final String MA_Announcementfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']";
	public static final String MA_Startdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFStartDate_dateInput']";
	public static final String MA_Startdatefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_StartDate']";
	public static final String MA_StartdateCalicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFStartDate_popupButton']";
	public static final String MA_Inactivedate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFInactiveDate_dateInput']";
	public static final String MA_Inactivedatefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_InactiveDate']";
	public static final String MA_InactivedateCalicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFInactiveDate_popupButton']";
	public static final String MA_Sort_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RNTBF_Sort']";
	public static final String MA_Sortfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Sort']";

	public static final String MA_OnlyActiveck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterCheckBox_IsActive']";
	public static final String MA_OnlyActivefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_IsActive']";
	public static final String MA_Announcement1_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]";
	public static final String MA_Startdate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[3]";
	public static final String MA_Inactivedate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[4]";
	public static final String MA_Sort1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[5]";
	public static final String MA_Onlyactiveck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl04_ctl00']";
	public static final String MA_Edit1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl04_EditButton']";
	public static final String MA_Activate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a";
	public static final String MA_Addannouncementbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String MA_Refreshbn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String MA_Firstpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String MA_Previouspagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String MA_Nextpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String MA_Lastpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String MA_Pagenotext_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String MA_Pageof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String MA_Pagegobtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String MA_Pagesize_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String MA_Pagechangebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String MA_Itemof_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";

	// Add Announcements

	public static final String MA_Announcement_iFrame_ID = ".//iframe[contains(@id,'Iframe')]";
	public static final String MA_Boldbtn_Path = ".//span[@class='Bold']";
	public static final String MA_Underlinebtn_Path = ".//span[@class='Underline']";
	public static final String MA_Italicbtn_Path = ".//span[@class='Italic']";
	public static final String MA_Hyperlinkbtn_Path = ".//span[@class='LinkManager']";
	public static final String MA_HyperlinkURL_Path = "//*[@id='LinkURL']";
	public static final String MA_HyperlinkTarget_Path = "//*[@id='SkinnedLinkTargetCombo']";
	public static final String MA_HyperlinkTooltip_Path = "//*[@id='LinkTooltip']";
	public static final String MA_AddAnnouncement_Path = "/html/body";
	public static final String MA_AddStartdate_Path = ".//b[text()='Start Date:']//following::input[1][@type='text']";
	public static final String MA_AddStartdateCal_Path = ".//b[text()='Start Date:']//following::a[1]";
	public static final String MA_AddStartdateCalmonth_Path = ".//td[contains(@id,'rdtpStartDate_calendar_Title')]";
	public static final String MA_AddStartdateTime_Path = ".//a[contains(@id,'rdtpStartDate_timePopupLink')]";
	public static final String MA_Addinactivedate_Path = ".//b[text()='Inactive Date:']//following::input[1][@type='text']";
	public static final String MA_AddinactivedateCal_Path = ".//b[text()='Inactive Date:']//following::a[1]";
	public static final String MA_AddinactivedateCalmonth_Path = ".//td[contains(@id,'rdtpInactiveDate_calendar_Title')]";
	public static final String MA_AddinactivedateTime_Path = ".//a[contains(@id,'rdtpInactiveDate_timePopupLink')]";
	public static final String MA_AddEMODUsersck_Path = ".//b[text()='User Group:']//following::input[1][@type='checkbox']";
	public static final String MA_AddEnrollmentservices_Path = ".//b[text()='User Group:']//following::input[2][@type='checkbox']";
	public static final String MA_AddSort_Path = ".//b[text()='Sort:']//following::input[1][@type='text']";
	public static final String MA_Createannouncementbtn_Path = ".//a[text()='Create Announcement']";
	public static final String MA_Updateammouncementbtn_Path = ".//a[text()='Update Announcement']";
	public static final String MA_Cancelbtn_Path = ".//a[text()='Cancel']";

	public static String MA_Createannouncement_Path(String Announcement) {

		return ".//em[text()='Announcements']//following::em[contains(text(),'" + Announcement + "')]";

	}

	public static String MA_Editannouncement_Path(String Announcement) {

		return ".//em[text()='Announcements']//following::a[contains(text(),'" + Announcement + "')]";

	}

}