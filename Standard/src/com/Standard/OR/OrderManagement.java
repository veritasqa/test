package com.Standard.OR;

public class OrderManagement extends OrderConfirmationPage {

	// Select Order
	
	public static final String MO_SelectOrdertitle_Path = ".//*[@id='divContent clearfix']/div[2]/h1";
	public static final String MO_SelectOrderfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlOrders_Input']";
	public static final String MO_Viewbtn_Path = ".//a[text()='View']";
	public static final String MO_Copybtn_Path = ".//a[text()='Copy']";
	public static final String MO_SelectOrderloading_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlOrders_LoadingDiv']";

	// Search Order
	
	public static final String MO_Searchordertitle_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	public static final String MO_Ordernofield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtOrderNumber']";
	public static final String MO_Trackingnofield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtTrackingNumber']";
	public static final String MO_Typedropdown_arrow_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlOrderType_Arrow']";
	public static final String MO_Typedropdown_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlOrderType_Input']";
	public static final String MO_Userfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtCreateUser']";
	public static final String MO_Shipperfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtShipper']";
	public static final String MO_Statusdrop_arrow_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlStatus_Arrow']";
	public static final String MO_Statusdrop_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlStatus_Input']";
	public static final String MO_Startdatefield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadStartDate_dateInput']";
	public static final String MO_Startdatecalender_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadStartDate_popupButton']";
	public static final String MO_Enddatefield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadEndDate_dateInput']";
	public static final String MO_Enddatecalender_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadEndDate_popupButton']";
	public static final String MO_RecipientName_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtShipToName']";
	public static final String MO_Addressfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtAddress1']";
	public static final String MO_Cityfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtCity']";
	public static final String MO_Statefield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtState']";
	public static final String MO_Zipfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtZip']";
	public static final String MO_Searchbtn_Path = ".//a[text()='Search']";
	public static final String MO_Clearbtn_Path = ".//a[text()='Clear']";

	// Search Results
	
	public static final String MO_SearchResultstitle_Path = ".//*[@id='divContent clearfix']/div[8]/h1";
	public static final String MO_SR_OrderNo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[1]/a";
	public static final String MO_SR_Orderedby_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[2]/a";
	public static final String MO_SR_Trackingno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[3]/a";
	public static final String MO_SR_Orderdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[4]/a";
	public static final String MO_SR_Shipper_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[5]/a";
	public static final String MO_SR_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[6]/a";
	public static final String MO_SR_Recipient_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[7]/a";
	public static final String MO_SR_State_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[8]/a";
	public static final String MO_SR_OrderType_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00']/thead/tr/th[9]/a";
	public static final String MO_SR_Noresult_Path = ".//*[text()='No Order Records Found.']";
	public static final String MO_SR_OrderDate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00__0']/td[4]";
	public static final String MO_SR_Loadingpanel_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection3_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection3_rgOrders']";

	// Select Order/Edit - General Tab

	public static final String MO_GeneralTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[1]/a/span/span/span";
	public static final String MO_Gen_ManageOrdertitle_Path = ".//*[@id='divContent clearfix']/div[3]/h1/span[1]";
	public static final String MO_Gen_ManageOrderNo_Path = "//*[@id='ctl00_cphContent_lblOrderNumber']";
	public static final String MO_Gen_Statustitle_Path = "//*[@id='divContent clearfix']/div[3]/h1/span[2]/span[1]";
	public static final String MO_Gen_ManageOrdernumber_Path = ".//*[@id='ctl00_cphContent_lblOrderNumber']";
	public static final String MO_Gen_Ticketnumber_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[1]/span";
	public static final String MO_Gen_Username_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[2]/span";
	public static final String MO_Gen_Orderdate_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[3]/span";
	public static final String MO_Gen_Shipdate_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[4]/span";
	public static final String MO_Gen_Closedate_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[5]/span";
	public static final String MO_Gen_Canceldate_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[6]/span";
	public static final String MO_Gen_Ordertype_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]/span";
	public static final String MO_Gen_Backorder_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[8]/a";
	public static final String MO_Gen_Ohhold_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[9]/a";
	public static final String MO_Gen_SelectOrder_Path = ".//a[text()='Select Order']";
	public static final String MO_Gen_EditStatus_Path = ".//a[text()='Edit Status']";
	public static final String MO_Gen_Ticketnumber1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[1]";
	public static final String MO_Gen_Selectbtn1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[10]/a";
	public static final String MO_Gen_Status_Path = ".//*[@id='ctl00_cphContent_lblOrderStatus']";
	public static final String MO_Gen_OrderSearchbtn_Path = ".//a[text()='� Order Search']";
	public static final String MO_Gen_ChangeOrderStatus_Path = "//*[@id='ctl00_cphContent_RadGrid1_ctl00_ctl05_ddlOrderStatus_Arrow']";

	// Select Order/Edit - Items Tab

	public static final String MO_ItemsTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[2]/a/span/span/span";
	public static final String MO_Items_Stockumber_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[1]";
	public static final String MO_Items_Description_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[2]";
	public static final String MO_Items_CustDesc_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[3]";
	public static final String MO_Items_TranslationReq_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[4]/a";
	public static final String MO_Items_State_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[5]/a";
	public static final String MO_Items_Qty_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[6]/a";
	public static final String MO_Items_Shipped_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]/a";
	public static final String MO_Items_Backorder_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[8]/a";
	public static final String MO_Items_Print_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[9]/span";
	public static final String MO_Items_Fulfillment_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[10]/span";
	public static final String MO_Items_Chargeback_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[11]/span";
	public static final String MO_Items_Postage_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[12]/span";
	public static final String MO_Items_Shipping_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]/a";
	public static final String MO_Items_Total_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[13]/span";
	public static final String MO_Items_Efile_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[14]";
	public static final String MO_Items_Stockno1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[1]";
	public static final String MO_Items_Costcenterfield_Path = ".//*[@id='ctl00_cphContent_txtCostCenter']";
	public static final String MO_Items_Updatebtn_Path = ".//*[@id='ctl00_cphContent_btnUpdateCostCenter']";
	public static final String MO_Items_Backbtn_Path = ".//*[@id='ctl00_cphContent_btnBillingBack']";
	public static final String MO_Items_Nextbtn_Path = ".//*[@id='ctl00_cphContent_btnBillingNext']";
	public static final String MO_Items__Path = "";

	// Select Order/Edit - Shipping Tab

	public static final String MO_ShippingTab_Path = ".//span[text()='Shipping']";
	public static final String MO_Shipping_Address_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[1]";
	public static final String MO_Shipping_Shippinginst_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[2]";
	public static final String MO_Shipping_Reqdelievery_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[3]";
	public static final String MO_Shipping_Shipmethod_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[4]";
	public static final String MO_Shipping_Shippinginfo_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[5]";
	public static final String MO_Shipping_Trackingno_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[6]";
	public static final String MO_Shipping_Upgradedshipping_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]";
	public static final String MO_Shipping_Nextbtn_Path = ".//*[@id='ctl00_cphContent_btnShippingNext']";
	public static final String MO_Shipping_Backbtn_Path = ".//*[@id='ctl00_cphContent_btnShippingBack']";
	public static final String MO_Shipping_1ZTest_Path = ".//a[text()='1ZTest']";

	// Select Order/Edit - Mail list Tab Recipients Tab

	public static final String MO_Maillisttab_Path = ".//span[text()='Mail List Recipients']";
	public static final String MO_Maillist_Ticketnumber_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[1]/span";
	public static final String MO_Maillist_Firstname_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[2]/a";
	public static final String MO_Maillist_lastname_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[3]/a";
	public static final String MO_Maillist_Company_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[4]/a";
	public static final String MO_Maillist_Address1_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[5]/a";
	public static final String MO_Maillist_City_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[6]/a";
	public static final String MO_Maillist_Sate_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[7]/a";
	public static final String MO_Maillist_Zip_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[8]/a";
	public static final String MO_Maillist_Phone_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[9]/a";
	public static final String MO_Maillist_Trackingno_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[10]";
	public static final String MO_Maillist_Backbtn_Path = ".//*[@id='ctl00_cphContent_btnItemsBack']";
	public static final String MO_Maillist_Nextbtn_Path = ".//*[@id='ctl00_cphContent_btnItemsNext']";
	public static final String MO_Maillist_Maillistfilebtn_Path = ".//*[@id='ctl00_cphContent_btnMailListCleaned']";
	public static final String MO_Maillist__Path = "";

	// Select Order/Edit - Attachments Tab

	public static final String MO_AttachmentTab_Path = ".//span[text()='Attachments']";
	public static final String MO_Attachment_Type_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[1]/a";
	public static final String MO_Attachment_Createuser_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[2]/a";
	public static final String MO_Attachment_Createdate_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[3]/a";
	public static final String MO_Attachment_Linktofile_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[4]";
	public static final String MO_Attachment_AddAttachicon_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String MO_Attachment_AddAttachment_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String MO_Attachment_Refreshicon_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String MO_Attachment_Refresh_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String MO_Attachment_Addattachdropdown_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl02_ctl02_ddlType']";
	public static final String MO_Attachment_Choosefile_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl02_ctl02_fupAttachment']";
	public static final String MO_Attachment_Saveattachment_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl02_ctl02_btnSaveAttachment']";
	public static final String MO_Attachment_Backbtn_Path = ".//*[@id='ctl00_cphContent_btnAttachmentsBack']";
	public static final String MO_Attachment_Savebtn_Path = ".//*[@id='ctl00_cphContent_btnSave']";
	public static final String MO_Attachment__Path = "";

}
