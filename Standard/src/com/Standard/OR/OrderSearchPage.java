package com.Standard.OR;

public class OrderSearchPage  extends OrderManagement{
	
	public static final String Std_OS_OrderNumber_ID ="ctl00_cphContent_txtOrderNumber";
	public static final String Std_OS_StatusArrow_ID = "ctl00_cphContent_ddlStatus_Arrow";
	public static final String Std_OS_TrackingNumber_ID = "ctl00_cphContent_txtTrackingNumber";
	public static final String Std_OS_Group_ID = "ctl00_cphContent_txtOrganization";
	public static final String Std_OS_Policynumber_ID = "ctl00_cphContent_txtPolicyNumber";
	public static final String Std_OS_Firstname_ID = "ctl00_cphContent_txtFirstName";
	public static final String Std_OS_Lastname_ID = "ctl00_cphContent_txtLastName";
	
	public static final String Std_OS_StartDatebtn_ID = "ctl00_cphContent_RadStartDate_popupButton";
	public static final String Std_OS_Startmonth_ID= "ctl00_cphContent_RadStartDate_calendar_Title";
	
	
	public static final String Std_OS_EndDateBtn_ID = "ctl00_cphContent_RadEndDate_popupButton";
	public static final String Std_OS_EndMonth_ID= "ctl00_cphContent_RadEndDate_calendar_Title";
	public static final String Std_OS_Calender_OKBtn_ID= "rcMView_OK";
	
	public static final String Std_OS_SearchBtn_ID = "ctl00_cphContent_btnSearch";
	public static final String Std_OS_FirstOrderNumber_Path= "//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[1]";
	public static final String Std_OS_FirstStatus_Path ="//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[3]";
	public static final String Std_OS_FirstGroupName_Path="//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[7]";
	public static final String Std_OS_FirstpolicyNumber_path="//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[8]";
	public static final String Std_OS_FirstRecipient_Path = "//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[4]";
	public static final String Std_OS_FirstDate_Path= "//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[6]";
	
	
	public String Std_OC_ViewBtn(String Ordernumber){
		return "//*[text()='"+Ordernumber+"']//following::a[1][text()='View']";
	}
	
	
	
	// Pagination
	
	public String Std_OC_PageNumber_Path(String Pagenumber){
		return "//span[text()='"+Pagenumber+"']";
	}
	
	
	public static final String Std_OS_Higlightedpagenumber_Path= "//a[@onclick='return false;']";
	public static final String Std_OS_NextpageBtn_path= "//*[@class='rgPageNext']";
	public static final String Std_OS_PreviousPageBtn_Path= "//*[@class='rgPagePrev']";
	public static final String Std_OS_lastPageBtn_Path= "//*[@class='rgPageLast']";
	public static final String Std_OS_FirstPageBtn_path= "//*[@class='rgPageFirst']";
	public static final String Std_OS_PageSizeArrow_ID= "ctl00_cphContent_rgdOrders_ctl00_ctl03_ctl01_PageSizeComboBox_Arrow";
	
	
	public static final String Std_OS_Firstrecordingrid_Path= "//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']";
	public static final String Std_OS_Nineteenthrecordingrid_path= "//*[@id='ctl00_cphContent_rgdOrders_ctl00__19']";
}
