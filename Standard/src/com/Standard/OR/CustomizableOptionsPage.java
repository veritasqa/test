package com.Standard.OR;

public class CustomizableOptionsPage extends AllCartsOR {

	public static final String Copymessage1 = "Reorder/multi-ship within 30 days: Select the save button, proof and reorder.";
	public static final String Copymessage2 = "Reorder/modify after 30 days: Send to Enrollment Services for review and approval.";

	public static final String Std_COP_ExpandBTN_Path = "//a[text()='Expand All']";
	public static final String Std_COP_ContentBTN_Path = "//a[text()='Content']";
	public static final String Std_COP_VariableBTN_Path = "//a[text()='Variables']";
	public static final String Std_COP_SaveBtn_ID = "ctl00_cphContent_btnSave";
	public static final String Std_COP_SaveAndContinueBTN_ID = "ctl00_cphContent_btnSaveContinue";
	public static final String Std_COP_Loading_Path = ".//*[@id='bigSide']/div/div[5]";

	// Postcard
	public static final String Std_COP_EnrollmentHeader_ID = "PDSelectEnrollmentHeader";
	public static final String Std_COP_EnrollmentStartDate_ID = "PDEnrollmentStartDate";
	public static final String Std_COP_EnrollmentEndDate_ID = "PDEnrollmentEndDate";
	public static final String Std_COP_EmployeeMailList_Upload_ID = "PDMailList";
	public static final String Std_COP_EmployeeMailList_textField_ID = "filePDMailList";
	public static final String Std_COP_LogoUpload_Btn_ID = "ImageUpload";
	public static final String Std_COP_InstructionsText_ID = "InstructionsText";

	// URL Section

	public static final String Std_COP_Yesbtn_ID = "PDIncludeURL_0";
	public static final String Std_COP_NoBtn_ID = "PDIncludeURL_1";
	public static final String Std_COP_URLtxtBox_ID = "PDMeetingURL";

	// mailingdetails and Logo
	public static final String Std_COP_ReturnAdressName_ID = "PDReturnName";
	public static final String Std_COP_ReturnAd_Dept_ID = "PDReturnDept";
	public static final String Std_COP_Returnad_Street_ID = "PDReturnStreet";
	public static final String Std_COP_ReturnAd_City_ID = "PDReturnCity";
	public static final String Std_COP_ReturnAd_State_ID = "PDReturnState";
	public static final String Std_COP_ReturnAd_Zip_ID = "PDReturnZip";

	// Meeting section
	public static final String Std_COP_SelectMeeting1Header_ID = "PDSelectMeetingHeader";
	public static final String Std_COP_SelectMeeting1Date_ID = "PDSelectMeetingDate1";
	public static final String Std_COP_SelectMeeting1Time_ID = "PDMeetingTime1";
	public static final String Std_COP_SelectMeeting1AM_ID = "PDMeeting1AMPM_0";
	public static final String Std_COP_SelectMeeting1PM_ID = "PDMeeting1AMPM_1";
	public static final String Std_COP_SelectMeeting1Location_ID = "PDMeetingLocation1";

	public static final String Std_COP_ItemsheaderFirstITem_Path = "//*[@id='ctl00_cphContent_customItemsTabs']/li";

	// BasicLife
	public static final String Std_COP_BLPolicyEffectiveDate_ID = "BAGEffectiveDate";
	public static final String Std_COP_BLEmployeeScedule_ID = "BAGEmployeeScheduleType";
	public static final String Std_COP_BLIncludeEmployeeLanguage_ID = "CHBAGIncludeEmployeeLanguage";
	public static final String Std_COP_BLExcludeEmployeeLanguage_ID = "CHBAGExcludeEmployeeLanguage";
	public static final String Std_COP_BLNumberOfWorkHoursPerWeek_ID = "BAGNumberOfWorkHoursPerWeek";
	public static final String Std_COP_BLHoursFrequency_ID = "BAGHoursFrequency";
	public static final String Std_COP_BLExistingPolicyOptions_ID = "BAGEligibilityWaitingPeriodExistingPolicyOptions";
	public static final String Std_COP_BLAlreadyMemberOptions_ID = "BAGEligibilityWaitingPeriodAlreadyMemberOptions";
	public static final String Std_COP_BLEligibleOptions_ID = "BAGEligibilityWaitingPeriodBecomesEligibleOptions";

	// Items column on the left
	public static final String Std_COP_Items_Postcard_Path = "//li[text()='Postcards Accident (AI)*']";
	public static final String Std_COP_Items_BasicLife_path = "//li[text()='Basic Life {with AD&D} � B@G*']";
	public static final String Std_COP_Items_Flyer_Path = "//li[text()='Travel Assistance Flyer']";
	public static final String Std_COP_Items_Criticalillness_Path = "//li[text()='Critical Illness All States or Specified Disease NY & VT � CH*']";

	// Frame for maillist upload
	public static final String Std_COP_FrameName = "oRadWindowFileUpload";

	// *[@id="fupMailListListContainer"]/li/span
	public static final String Std_COP_MaillistSelect_Path = "//input[@id='fupMailListfile0']";
	public static final String Std_COP_Maillselect1_ID = "fupMailListListContainer";
	public static final String Std_COP_Maillist_UploadBTN_ID = "btnMailList";
	public static final String Std_COP_Maillist_RemoveBtn_ID = "btnRemove";

	public static final String Std_COP_Maillist_uploadMSg_ID = "divMailList";
	public static final String Std_COP_Maillist_uploadMSg = "Mail list uploaded";
	public static final String Std_COP_Maillist_RemoveMsg = "Mail list removed.";

	public static final String Std_COP_Maillist_CloseForm_ID = "CloseButton";
	public static final String Std_COP_Maillist_fetchingsign_ID = "loadingDiv";

	// Image upload frame

	public static final String Std_COP_FileUpload_ID = "imgUpload";
	public static final String Std_COP_UploadBtn_ID = "btnUpload";
	public static final String Std_COP_ClearBtn_ID = "btnClear";
	public static final String Std_COP_UploadMessage_ID = "lblUploadMsg";

	public String MailcontactPath(String Firstname, String rownumber) {
		return "//td[text()='" + Firstname + "']//following::td[" + rownumber + "]";
	}

	// Customizable options
	public static final String Std_DSTQuestion_path = ".//*[@id='DSTQuestion']";
	public static final String Std_CHPlanType_path = ".//*[@id='CHPlanType']";
	public static final String Std_CHAutomobileAmount_path = ".//*[@id='CHAutomobileAmount']";
	public static final String Std_CHHMSAmount_path = ".//*[@id='CHHMSAmount']";
	public static final String Std_CHAIPayrollFrequency_path = ".//*[@id='CHAIPayrollFrequency']";
	public static final String Std_CHSpouseCoverageLanguage_path = ".//*[@id='CHSpouseCoverageLanguage']";
	public static final String Std_CHHoursPerWeek_path = ".//*[@id='CHHoursPerWeek']";
	public static final String Std_CHHoursFrequency_path = ".//*[@id='CHHoursFrequency']";
	public static final String Std_flatamount_path = "//*[@id='CHCIElectCoverage']";
	public static final String Std_Underwriter_path = ".//*[@id='CHEOIReqd']";
	public static final String Std_HealthMaintenanceScreening_path = "//*[@id='CHHealthMaintenanceScreeningBenefitAmt']";
	public static final String Std_Transportationbenefitamount_path = "//*[@id='CHTransportationBenefitAmt']";
	public static final String Std_Lodgingbenefitamount_path = "//*[@id='CHLodgingBenefitAmt']";
	public static final String Std_SampleClaimBenefitAmount_path = "//*[@id='CHCIBenefitAmt']";
	public static final String Std_EmployeesPremiumDeductionFrequency_path = "//*[@id='CHCIPayrollFrequency']";
	public static final String Std_SelectAgeBands_path = "//*[@id='CIRateTableAgeBands']";
	public static final String Std_RateType_path = ".//*[@id='CHRateTableSelection']";

	public static final String Std_Spousecoverageincludes_path = "//*[@id='CHSpouseCoverageLanguage']";
	public static final String Std_Hourlyrequirementtxt_path = "//*[@id='CHHoursPerWeek']";
	public static final String Std_Hoursactivelyatworkfrequency_path = ".//*[@id='CHHoursActivelyAtWorkFrequency']";
	public static final String Std_Frequencyofworkinghours_path = ".//*[@id='CHHoursFrequency']";

	public static final String Std_ReoccurrenceBeneft_path = "//*[@id='CHReoccurrenceTreatmentFreePeriod']";
	public static final String Std_PreXperiod_path = "//*[@id='CHPreXPeriod']";
	public static final String Std_AutomobileAccidentAmount_path = ".//*[@id='CHAutomobileAmount']";
	public static final String Std_HealthMaintenceScreeningAmount_path = ".//*[@id='CHHMSAmount']";

	public static final String Std_ContractLanguageEra_path = ".//*[@id='BAGContract']";
	public static final String Std_PolicyEffectiveDate_path = ".//*[@id='BAGEffectiveDate']";
	public static final String Std_Istheweeklybenefitaflatamount_path = ".//*[@id='BAGMonthlyBenefitFlatAmt']";
	public static final String Std_STDWeeklyBenefitPercentagePart1_path = ".//*[@id='BAGSTDPercentage']";
	public static final String Std_InsuredPredisabilityEarningsPart2_path = ".//*[@id='BAGWeeklyPDE']";
	public static final String Std_MaximumWeeklyBenefitAmount_path = ".//*[@id='BAGMaxWeeklyBenefit']";
	public static final String Std_MinimumWeeklyBenefitAmount_path = ".//*[@id='BAGMinWeeklyBenefit']";
	public static final String Std_MaximumBenefitPeriod_path = ".//*[@id='BAGMaxBenefitPeriod']";
	public static final String Std_BenefitWaitingPeriodforAccident_path = ".//*[@id='BAGAccidentBenefits']";
	public static final String Std_BenefitWaitingPeriodforSickness_path = ".//*[@id='BAGSicknessBenefits']";

	public static final String Std_OrgType_path = ".//*[@id='CHBAGIncludeEmployeeLanguage']";
	public static final String Std_workinghours_path = ".//*[@id='BAGNumberOfWorkHoursPerWeek']";
	public static final String Std_WorkHoursfrequency_path = ".//*[@id='BAGHoursFrequency']";
	public static final String Std_Option1_path = ".//*[@id='BAGEligibilityWaitingPeriodExistingPolicyOptions']";
	public static final String Std__path = "";

	// $ Format

	public String _$formantcells(String Field, String columnno) {

		return ".//span[text()='" + Field + "']//following::td[" + columnno + "]";

	}

	public String _$formantbtn(String Field) {

		return ".//span[text()='" + Field + "']//following::a[1]";

	}

}
