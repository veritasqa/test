package com.Standard.OR;

public class WidgetOR extends SupportTickets {

	public static String Widgetname = "";
	public static final String Add_Widgets_Path = ".//h3[text()='Add Widgets']";

	// Order Lookup
	public static final String Orderlookup_Wid = "Order Lookup";

	public static final String Orderlookup_Path = ".//a[text()='Order Lookup']";
	public static final String Orderlookup_Orderno_Path = "_ctl00_rcbOrderNumber_Input']";
	public static final String Orderlookup_View_Path = "_ctl00_btnSubmit']";
	public static final String Orderlookup_Copy_Path = "_ctl00_btnCopy']";

	// My Recent Orders

	public static final String MyRecentOrders = "My Recent Orders";
	public static final String MyRecentOrders_Path = ".//a[text()='My Recent Orders']";

	public static String MyRecentOrdersview(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::a[2]";
	}

}
