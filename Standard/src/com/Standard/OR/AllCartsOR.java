package com.Standard.OR;

import com.Standard.Values.StandardValueRepository;

public class AllCartsOR extends StandardValueRepository {

	public static final String AC_Title_Path = ".//h1[contains(text(),'All Carts')]";
	public static final String AC_Legalname_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterTextBox_Organization']";
	public static final String AC_Legalnamefilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_Organization']";
	public static final String AC_Policyno_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterTextBox_PolicyNumber']";
	public static final String AC_Policynofilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_PolicyNumber']";
	public static final String AC_State_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterTextBox_StateOfSitus']";
	public static final String AC_Statefilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_StateOfSitus']";
	public static final String AC_Firstname_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterTextBox_FirstName']";
	public static final String AC_FirstnameFilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_FirstName']";
	public static final String AC_Lastname_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterTextBox_LastName']";
	public static final String AC_Lastnamefilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_LastName']";
	public static final String AC_Createdate_Path = "//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_RDIPFCreateDate_dateInput']";
	public static final String AC_Createdatefilter_Path = "//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_CreateDate']";
	public static final String AC_Description_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterTextBox_CartGroupDescription']";
	public static final String AC_Descriptionfilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_CartGroupDescription']";
	public static final String AC_Mycart_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_FilterCheckBox_IsUsers']";
	public static final String AC_Mycartfilter_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl02_ctl02_Filter_IsUsers']";

	public static final String AC_Expand1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_GECBtnExpandColumn']";
	public static final String AC_Proof1_Path = ".//td[text()='Veritas QA 1']//following::a[4]";
	public static final String AC_Legalname1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[2]";
	public static final String AC_Policynumber1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[3]";
	public static final String AC_State1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[4]";
	public static final String AC_Firstname1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[5]";
	public static final String AC_Lastname1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[6]";
	public static final String AC_Createdate1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[7]";
	public static final String AC_Description1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[8]";
	public static final String AC_Mycart1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_ctl00']";
	public static final String AC_Acquirebtn1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnSentEmail']";
	public static final String AC_Viewhistorybtn1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[10]/a[2]";

	public static final String AC_Firstpagebtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String AC_Previouspagebtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String AC_Nextpagebtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String AC_Lastpagebtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String AC_Pagesize_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl03_ctl01_PageSizeComboBox_Input']";
	public static final String AC_Itemsin_Path = "//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00PCN']";
	public static final String AC_20items_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__19']";



	// Load

	public static final String AC_Loadingpath_Path = ".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_divShoppingCart']";

}
