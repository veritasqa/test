package com.Standard.OR;

public class LandingPage extends InventoryPage {

	public static final String Std_LP_Announcement_Tab_ID = "ctl00_cphContent_rdAnnouncements_T";
	public static final String Std_LP_Username_ID = "ctl00_lbUserName";
	public static final String Std_LP_Logout_ID = "ctl00_lbnLogout";
	public static final String Std_LP_Bell_Path = ".//a[@title='There are carts pending approval.']";
	public static final String Std_FulfilmentSearchField_ID = "ctl00_txtQuickSearch";
	public static final String Std_FulfilmentSearchbtn_ID = "ctl00_btnQuickSearch";

	// Admin
	public static final String Std_LP_Home_path = ".//span[text()='Home']";
	public static final String Std_LP_Admin_path = ".//span[text()='Admin']";
	public static final String Std_LP_Specialproject_path = ".//span[text()='Special Projects']";
	public static final String Std_LP_ManageOrders_path = ".//span[text()='Manage Orders']";
	public static final String Std_LP_ManageUsers_path = ".//span[text()='Manage Users']";
	public static final String Std_LP_Support_path = ".//span[text()='Support']";
	public static final String Std_LP_Manageannouncements_path = ".//span[text()='Manage Announcements']";
	public static final String Std_LP_Allcarts_path = ".//span[text()='All Carts']";

	public String NavigationTab(String option) {

		return ".//span[text()='" + option + "']";

	}

	// Fulfillment Search region
	public static final String Std_LP_StartBtn_ID = "ctl00_lbStartNewOrder";

	// Widget

	public static final String Std_LP_AddWidget_Path = "//h3[text()='Add Widgets']";
	public static final String Std_LP_Widget_Orderlookup_ID = "ctl00_cphContent_btnAddWidgetOrderLookup";
	public static final String Std_LP_Widget_RecentOrders_ID = "ctl00_cphContent_btnAddWidgetMyOrders";
	public static final String Std_LP_Widget_RecentOrdersTitle_Path = "//em[text()='My Recent Orders']";
	public static final String Std_LP_Widget_OrderlookupTitle_Path = "//em[text()='Order Lookup']";
	public static final String Std_LP_MRO_CollapseBtn_Path = "//em[text()='My Recent Orders']//following::a[@title='Collapse']";
	public static final String Std_LP_MRO_CloseBtn_Path = "//em[text()='My Recent Orders']//following::a[@title='Close']";
	public static final String Std_LP_OL_CollapseBtn_Path = "//em[text()='Order Lookup']//following::a[@title='Collapse']";
	public static final String Std_LP_OL_CloseBtn_Path = "//em[text()='Order Lookup']//following::a[@title='Closee']";

	public static final String Std_LP_MyCart_ID = "aShoppingCart";

	// Orderlookup -OL
	public static final String Std_LP_OL_FromDateMonth_Path = "//*[contains(@id,'C_ctl00_rdpOrderSearchDateFrom_calendar_Title')]";
	public static final String Std_LP_OL_ToDateMonth_Path = "//*[contains(@id,'C_ctl00_rdpOrderSearchDateTo_calendar_Title')]";
	public static final String Std_LP_OL_CalenderOKBtn_ID = "rcMView_OK";

	public static final String Std_LP_OL_Searchbtn_Path = "//em[text()='Order Lookup']//following::a[text()='Search']";
	public static final String Std_LP_OL_FromDateicon_Path = "//em[text()='Order Lookup']//following::a[3]";
	public static final String Std_LP_OL_ToDateicon_Path = "//em[text()='Order Lookup']//following::a[50]";

	public static final String Std_LP_OL_OrderNumber_Path = "//em[text()='Order Lookup']//following::input[@value='Order Number']";
	public static final String Std_LP_OL_viewBtn_Path = "//em[text()='Order Lookup']//following::*[text()='View']";
	public static final String Std_LP_OL_CopyBtn_Path = "//em[text()='Order Lookup']//following::*[text()='Copy']";

	// *New* Decision Support Tool -DST
	public static final String Std_LP_DST_Path = "//em[text()='*New* Decision Support Tool']";
	public static final String Std_LP_DST_Collapse_Path = "//em[text()='*New* Decision Support Tool']//following::a[@title='Collapse']";
	public static final String Std_LP_SupportMessage_ID = "ctl00_cphContent_rdSupportTool_C_lblSupport";
	public static final String Std_LP_SupportMessage = "If you want a DST for this group, please go to the Enrollment Services request form within Salesforce.";
	public static final String Std_LP_DST_Expand_Path = "//em[text()='*New* Decision Support Tool']//following::a[@title='Expand']";

	// MyrecentOrders
	// examples
	// *[text()='1740409']//following::a[text()='View']
	// *[text()='1740409']//following::a[text()='Copy']
	public String OrderCopyORViewBtn(String Ordernumber, String ButtonText) {
		return "//*[text()='" + Ordernumber + "']//following::a[text()='" + ButtonText + "']";

	}

}
