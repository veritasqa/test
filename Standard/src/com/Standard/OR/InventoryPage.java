package com.Standard.OR;

public class InventoryPage extends GroupSearchPage {

	// inventory Site

	public static final String Inventory_Username_Path = ".//*[@id='Login']";
	public static final String Inventory_Passowrd_Path = ".//*[@id='Password']";
	public static final String Inventory_Submit_Btn_Path = ".//*[@id='Submit']";
	public static final String Inventory_Logout_Path = ".//*[@id='Table2']/tbody/tr[127]/td/a";

	public static final String Inventory_OrderSearch_Path = ".//a[text()='Edit/View Orders']";
	public static final String Inventory_Ordernumber_Path = ".//*[@id='OrderNumber']";
	public static final String Inventory_Search_Btn_Path = ".//*[@id='btnSearch']";
	public static final String Inventory_Edit_Btn_Path = ".//a[text()='Edit']";
	public static final String Inventory_Cancelorder_Btn_Path = ".//*[@id='CancelOrder']";
	public static final String Inventory_Order_Status = ".//*[@id='OrderStatus']";
	public static final String Inventory_logout_btn_Path = ".//a[text()='Logout']";
	

	public static final String Inventory_CustomerID_Path = ".//*[@id='CustomerID']";
	public static final String Inventory_SearchResult_Ordernumber = ".//*[@id='DataList']/tbody/tr[2]/td[1]";
	public static final String Inventory_SearchResult_Status_Path = ".//*[@id='DataList']/tbody/tr[2]/td[6]";

	public static final String Inventory_EditPage_Title_Path = ".//*[@id='ltitle']";
	public static final String Inventory_Status_Path = ".//*[@id='OrderStatus']";
	public static final String Inventory_CancelMessage_Path = ".//*[@id='message']";

}
