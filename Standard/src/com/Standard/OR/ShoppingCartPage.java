package com.Standard.OR;

public class ShoppingCartPage extends SearchResultsPage {

	public static final String Std_SCP_FirstQty_ID = "ctl00_cphContent_rgrdShoppingCart_ctl00_ctl05_rntxtQuantity";
	public static final String Std_SCP_EnvelopIcon_Path = "//i[@class='fa fa-envelope']";

	public static final String Std_SCP_SendBtn_Path = ".//a[@title='Send Cart']";
	public static final String Std_SCP_Send_Sendto_Path = ".//*[@id='tabForm']/p[1]/select";
	public static final String Std_SCP_Send_Action_Path = ".//*[@id='tabForm']/p[2]/select";
	public static final String Std_SCP_Send_PolicyType_Path = ".//*[@id='tabForm']/p[3]/select";

	public static final String Std_SCP_Send_Usersname_Path = ".//*[@id='tabForm']/p[3]/strong";
	public static final String Std_SCP_Send_Message_Path = ".//*[@id='tabForm']/p[4]/textarea";

	public static final String Std_SCP_ProofBtn_Path = ".//a[text()='Proof']";
	public static final String Std_SCP_ProofedBtn_Path = ".//a[text()='Proofed']";

	public static final String Std_SCP_ExpandAllCart_Path = ".//input[@name='ctl00$cphContent$rgrdShoppingCart$ctl00$ctl02$ctl00$ctl00']";
	public static final String Std_SCP_ExpandCart1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__2__0']";

	public static final String Std_SCP_ApproveBtn_Path = "//a[@title='Approve']";
	public static final String Std_SCP_ApprovedBtn_Path = "//a[@title='Approved']";
	public static final String Std_SCP_UpdateBtn_Path = "//a[@title='Update']";
	public static final String Std_SCP_CheckoutBtn_Path = ".//a[@title='Checkout']";
	public static final String Std_SCP_YesBtn_Path = "//a[@class='buttonAction right checkoutFlagYes']";
	public static final String Std_SCP_NoBtn_Path = "//a[@class='buttonAction right checkoutFlagNo']";
	public static final String Std_SCP_EditvariableYesBtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[5]/div[3]/div/p[2]/a[1]";
	public static final String Std_SCP_DeleteYesBtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tbody/tr[1]/td[2]/div[2]/div[2]/div[3]/div/p[2]/a[1]";

	public static final String Std_SCP_Pdfonly_path = ".//*[@id='ctl00_cphContent_rdemail']";
	public static final String Std_SCP_PrintandPdfonly_path = ".//*[@id='ctl00_cphContent_rdship']";
	public static final String Std_SCP_Deletepart_path = ".//b[text()='Cart(s)']//following::a[3][@title='Cancel Order']";

	public static final String Std_SCP_Logout_Path = ".//*[@id='ctl00_lbnLogout']";

	// Do you wanna continue with the order popup

	public static final String Std_SCP_variableYes_Path = "//a[@class='buttonAction right editVariablesYes']";

	// i hover icon
	public static final String Std_SCP_i_hovericon_path = "//td[@colspan='4']//following::img";
	public static final String Std_SCP_i_hovericon_Editbtn_path = "//a[text()='Edit']";
	public static final String Std_SCP_i_hovericon_Savebtn_path = "//a[text()='Save']";
	public static final String Std_SCP_i_hovericon_Yesbtn_path = "//a[text()='Yes']";
	public static final String Std_SCP_i_hovericon_Nobtn_path = "//a[text()='No']";
	public static final String Std_SCP_i_hovericon_Description_Path = "//*[text()='Description:']//following::input[1]";
	public static final String Std_SCP_i_hovericon_LegalName_Path = "//*[text()='Description:']//following::input[2]";
	public static final String Std_SCP_i_hovericon_PolicyNumber_Path = "//*[text()='Description:']//following::input[3]";

	// shipping information

	public static final String Std_SCP_EmailTxt_ID = "ctl00_cphContent_txtEmailShip";
	public static final String Std_SCP_Personalisation_Radio_ID = "ctl00_cphContent_rdPersonal";
	public static final String Std_SCP_Personal_Radio_ID = "ctl00_cphContent_rdPersonal";
	public static final String Std_SCP_Modification_Radio_ID = "ctl00_cphContent_rdModification";
	public static final String Std_SCP_None_Radio_ID = "ctl00_cphContent_rdNone";
	public static final String Std_SCP_300Pm_Radio_ID = "ctl00_cphContent_DeliveryTimeRbl_2";

	public static final String Std_SCP_ProductLineTxt_ID = "ctl00_cphContent_txtProductLine";
	public static final String Std_SCP_Printaccount_Txt_ID = "ctl00_cphContent_txtPrintAccount";
	public static final String Std_SCP_CostCenter_Txt_ID = "ctl00_cphContent_txtCostCenter";
	public static final String Std_SCP_Next_Btn_Path = "//a[text()='Next �']";
	public static final String Std_SCP_Editaddress_Btn_Path = "//a[text()='� Edit Address Information']";

	public static final String Std_SCP_Datepicker_ID = "ctl00_cphContent_rdpMailingDatePicker_popupButton";
	public static final String Std_SCP_Datepicker_Month_ID = "ctl00_cphContent_rdpMailingDatePicker_calendar_Title";
	public static final String Std_SCP_Datepicker_OKBtn_ID = "rcMView_OK";
	public static final String Std_SCP_MailingNote_ID = "ctl00_cphContent_mailingNote";
	public static final String Std_SCP_MailingNote = "Postcard direct mail orders can take 48-72 hours for production and mailing. Orders placed after 12 PM CST will be processed the next business day";

	public static final String Std_SCP_DeliveryDatepicker_ID = "ctl00_cphContent_rdtepkrDeliveryDate_popupButton";
	public static final String Std_SCP_DeliveryDatepicker_Month_ID = "ctl00_cphContent_rdtepkrDeliveryDate_calendar_Title";

	public static final String Std_SCP_Deliverymethod_ID = "ctl00_cphContent_lblDeliveryMethodMailing";
	public static final String Std_SCP_Deliverymethod_Path = "//*[@id='ctl00_cphContent_lblDeliveryMethod']";
	public static final String Std_SCP_ShippingAccountTxt_ID = "ctl00_cphContent_txtShippingAcountMailing";
	public static final String Std_SCP_Shippingcostcenter_ID = "ctl00_cphContent_ddlShippingCostCenter";
	public static final String Std_SCP_CostcenterTxt_ID = "ctl00_cphContent_txtMailingCostCenter";
	public static final String Std_SCP_MailingCOstOverridetxt_ID = "ctl00_cphContent_txtMailingCostCenterOverride";
	public static final String Std_SCP_EditAdressBtn_ID = "ctl00_cphContent_btnShowAddress";
	public static final String Std_SCP_Checkout_ID = "ctl00_cphContent_btnCheckout";

	public String Selectbuttonshoppingcartbytitle(String PieceName, String Title) {
		return "//*[text()='" + PieceName + "']//following::a[@title='" + Title + "']";
	}

	public String Selectbuttonshoppingcartbytext(String PieceName, String Text) {
		return "//*[text()='" + PieceName + "']//following::a[text()='" + Text + "']";
	}

	// Error Popup -EP

	public static final String Std_SCP_EP_ErrorMsg_path = ".//label[contains(text(),'Order has a mix of normal and maillist items')]//following::a[text()='Ok']";
	public static final String Std_SCP_EP_OKBtn_Path = ".//label[contains(text(),'Order has a mix of normal and maillist items')]";
	public static final String Std_SCP_EP_ItemremovedMsg_Path = "//*[text()='Item(s) have been removed from your cart.']";
	public static final String Std_SCP_EP_Ordersuccess_Msg_Path = "//*[text()='Order copied Successfully']";
	public static final String Std_SCP_EP_OrderQTYUpdateMSg_Path = "//*[text()='Quantity for POSTCARDAI0816 has been changed to 5 unit(s).']";

	// Postcard print and mail Shipping information-(SI)

	public static final String Std_SCP_SI_PrintandPdf_ID = "ctl00_cphContent_rdship";
	public static final String Std_SCP_SI_Pdfonly_ID = "ctl00_cphContent_rdemail";
	public static final String Std_SCP_SI_Name_ID = "ctl00_cphContent_txtName";
	public static final String Std_SCP_SI_Address_ID = "ctl00_cphContent_txtAddress1";
	public static final String Std_SCP_SI_City_ID = "ctl00_cphContent_txtCity";
	public static final String Std_SCP_SI_State_ID = "ctl00_cphContent_txtState";
	public static final String Std_SCP_SI_Zip_ID = "ctl00_cphContent_txtZip";
	public static final String Std_SCP_SI_Country_ID = "ctl00_cphContent_ddlCountry";
	public static final String Std_SCP_SI_Ship_Adres_override_ID = "ctl00_cphContent_txtShippingCostCenterOverride";

	// Confirmation page

	public static final String Std_CP_Status_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_lblStatus']/span";
	public static final String Std_CP_OrderNo_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']";
	public static final String Std_CP_Okbtn_Path = ".//a[text()='OK']";
	public static final String Std_CP_Submitcancelrequestbtn_Path = ".//a[text()='Submit Cancel Request']";
	public static final String Std_CP_Cancelreqsubmittedmesg_Path = ".//span[text()='An order cancel request has been submitted.']";
	public static final String Std_CP_Cancelreqsubmittedclose_Path = ".//a[text()='Close']";
	public static final String Std_CP_QuantityOKbtn_Path = ".//a[@class='buttonAction NoButton' and text()='OK']";
	public static final String Std_CP_Loading_Path = "//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_divShoppingCart']";

	// Methods

	public static String ClickExcludedbooklet(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//label[text()='Excluded']";

	}

	public static String ClickIncludebooklet(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//label[text()='Include']";

	}

	public static String Requiredck(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//input[contains(@id,'Required')]";

	}

	public static String In_Ex_checkbox(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//input[1]";

	}

	public static String ClickProof(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[text()='Proof']";

	}

	public static String ClickProofed(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[text()='Proofed']";

	}

	public static String Enterpartcustomtext(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::input[1]";

	}

	public static String ClickUpdatebtn(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[@title='Update']";

	}

	public static String ClickVariable(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[@title='Edit Variables']";

	}

	public static String ClickVariableNoteditable(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[@title='Variables Not Editable']";

	}

	public static String ClickVariable_Yes(String Partname) {

		return ".//tr//span[text()='" + Partname
				+ "']//following::a[contains(@class,'buttonAction right editVariablesYes')][1]";

	}

	public static String ClickExcludedbooklet_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//following::label[2][text()='Excluded']";

	}

	public static String ClickIncludebooklet_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//following::label[2][text()='Included']";

	}

	public static String ClickCopy_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//following::a[1]";

	}

	public static String Copyfromdd_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//span[contains(.,'Copy From')]//following::select[1]";

	}

	public static String Copyfromupdate_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet
				+ "')]//span[contains(.,'Copy From')]//following::select[1]//following::a[1]";

	}

	public static String Replacewithdd_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//span[contains(.,'Replace With')]//following::select[1]";

	}

	public static String Replacewithbtn_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet
				+ "')]//span[contains(.,'Replace With')]//following::select[1]//following::a[1]";

	}

	public static String ClickApprove(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[@title='Approve']";

	}

	public static String Lockicon(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//i[@title='This cart is locked']";

	}

	public static String Copybooklet_Details(String Booklet) {

		return ".//td[contains(.,'" + Booklet + "')]//following::a[1][text()='Copy']";

	}

	public static String Clickupdate(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[2]//a[@title='Update']";

	}

	public static String ClickQty(String Partname) {

		return ".//tr//span[text()='" + Partname + "']//following::td[1]//input[1][@type='text']";

	}

}
