package com.Standard.OR;

public class GroupSearchPage extends CustomizableOptionsPage {

	public static final String Std_GS_Groupname_ID = "ctl00_cphContent_txtOrganization";
	public static final String Std_GS_PolicyNumber_ID ="ctl00_cphContent_txtPolicyNumber";

	public static final String Std_GS_SearchButton_ID = "ctl00_cphContent_btnSearch";
		
	public static final String Std_GS_AddNewGroup_ID = "ctl00_cphContent_Variables_ctl00_ctl03_ctl01_InitInsertButton";
	public static final String Std_GS_Refreshbtn_ID = "ctl00_cphContent_Variables_ctl00_ctl03_ctl01_RefreshButton";
	public String Std_GroupSelect(String GroupName){
				
		return "//td[text()='"+GroupName+"']";
		 
	}
	
	public String Std_GroupEditOrDelete(String GroupName, String ButtonName){
		
		return "//td[text()='"+GroupName+"']//following::a[text()='"+ButtonName+"']";
		//td[text()='Veritas QA 1']//following::a[text()='Delete']
	}

	public String Std_GS_MandatoryStarPath(String ParentXpath ){
		return "//*[@id='"+ParentXpath+"']//following::sup";
				//*[@id="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtPolicyNumberNew"]//following::sup
	}
	

	 //GroupInformation of existing Contact- EC
	
	public static final String Std_GS_EC_Policynumber_ID= "ctl00_cphContent_Variables_ctl00_ctl05_txtPolicyNumberNew";
	public static final String Std_GS_EC_GroupleagalName_ID= "ctl00_cphContent_Variables_ctl00_ctl05_txtOrganizationNew";
	public static final String Std_GS_EC_OrgTypeArrow_ID= "ctl00_cphContent_Variables_ctl00_ctl05_ddlOrganizationTypeNew_Arrow";
	public static final String Std_GS_EC_AccounttypeArrow_ID="ctl00_cphContent_Variables_ctl00_ctl05_ddlOrganizationAccountType_Arrow";
	public static final String Std_GS_EC_CompanyArrow_ID="ctl00_cphContent_Variables_ctl00_ctl05_ddlCompany_Arrow";
	public static final String Std_GS_EC_StateofSitusArrow_ID="ctl00_cphContent_Variables_ctl00_ctl05_ddlStateOfSitus_Arrow";
	
	//Shipping information of existing Contact
	public static final String Std_GS_EC_ShipToName_ID="ctl00_cphContent_Variables_ctl00_ctl05_txtShipToName";
	public static final String Std_GS_EC_ShipAddress1_ID="ctl00_cphContent_Variables_ctl00_ctl05_txtShipToAddress1";
	public static final String Std_GS_EC_ShipAddress2_ID="ctl00_cphContent_Variables_ctl00_ctl05_txtShipToAddress2";
	public static final String Std_GS_EC_ShiptoCity_ID="ctl00_cphContent_Variables_ctl00_ctl05_txtShipToCity";
	public static final String Std_GS_EC_ShiptoStateArrow_ID="ctl00_cphContent_Variables_ctl00_ctl05_ddlShipToState_Arrow";
	public static final String Std_GS_EC_ShipZipArrow_ID="ctl00_cphContent_Variables_ctl00_ctl05_txtShipToZip";
	public static final String Std_GS_EC_OverrideAdresschangeCheckBox_ID="ctl00_cphContent_Variables_ctl00_ctl05_cbxAddressValid";
	public static final String Std_GS_EC_UpdateBtn_ID="ctl00_cphContent_Variables_ctl00_ctl05_btnUpdate";
	public static final String Std_GS_EC_ResetBtn_ID = "ctl00_cphContent_Variables_ctl00_ctl05_btnReset";
		
	 //GroupInformation of New Contact- NC
	
	public static final String Std_GS_NC_Policynumber_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtPolicyNumberNew";
	public static final String Std_GS_NC_GroupleagalName_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtOrganizationNew";
	public static final String Std_GS_NC_OrgTypeArrow_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_ddlOrganizationTypeNew_Arrow";
	public static final String Std_GS_NC_AccounttypeArrow_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_ddlOrganizationAccountType_Arrow";
	public static final String Std_GS_NC_CompanyArrow_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_ddlCompany_Arrow";
	
	public static final String Std_GS_NC_StateofSitusArrow_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_ddlStateOfSitus_Arrow";
	
	//Shipping information of New Contact
	public static final String Std_GS_NC_ShipToName_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtShipToName";
	public static final String Std_GS_NC_ShipAddress1_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtShipToAddress1";
	public static final String Std_GS_NC_ShipAddress2_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtShipToAddress2";
	public static final String Std_GS_NC_ShiptoCity_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtShipToCity";
	public static final String Std_GS_NC_ShiptoStateArrow_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_ddlShipToState_Arrow";
	public static final String Std_GS_NC_ShipZip_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_txtShipToZip";
	public static final String Std_GS_NC_OverrideAdresschangeCheckBox_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_cbxAddressValid";
	public static final String Std_GS_NC_UpdateBtn_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_btnUpdate";
	public static final String Std_GS_NC_ResetBtn_ID="ctl00_cphContent_Variables_ctl00_ctl02_ctl02_btnReset";

	
	public String Std_GS_inputXpath(String FieldName){
		return "//label[text()='"+FieldName+"']//following::input[1]";
		//label[text()='Policy Number']//following::input[1]

	}

	}


