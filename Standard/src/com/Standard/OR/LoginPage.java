package com.Standard.OR;


public  class LoginPage extends LandingPage{
	
	public static final String Std_Login_AdminloginBTN_Path = "//a[@class='AdminLink']";
	public static final String Std_Login_CloudloginBTN_Path = "//a[@href='https://cloudauth-test.standard.com/adfs/ls/IdPInitiatedSignOn.aspx?loginToRp=https://staging.veritas-solutions.net/thestandard/ ']";

	public static final String Std_Login_UserName_ID = "txtUserName";
	public static final String Std_Login_Password_ID = "txtPassword";
	public static final String Std_Login_LoginBtn_ID = "btnSubmit";
	
	//Error Message 
	public static final String Std_Login_Password_ErrorMsg_Path = "//li[text()='User Password is required.']";
	public static final String Std_Login_InvalidCredentials_Msg_path = "//li[text()='Invalid user name or password, please try again.']";
	public static final String Std_Login_Username_ErrorMsg_path = "//li[text()='UserName is required']";
	
	//SSO credentials
	public static final String SSOLogin_UserId_ID = "UserID";
	public static final String SSOLogin_Firstname_ID = "FirstName";
	public static final String SSOLogin_LastName_ID ="LastName";
	public static final String SSOLogin_Email_ID ="email";
	public static final String SSOLogin_Department_ID ="Department";
	public static final String SSOLogin_TargetURL_ID ="txtTargetURL";
	public static final String SSOLogin_MemberOF_ID ="MemberOf";
	public static final String SSOLogin_Submit_ID ="btnSubmit";
	public static final String SSOLogin_Debug_ID ="chkDebugMode";
	
	
}
