package com.Standard.OR;

public class OrderConfirmationPage extends ManageusersOR {

	public static final String Std_OC_OrderNumder_ID = "ctl00_ctl00_cphContent_cphSection_lblOrderNumber";
	public static final String Std_OC_OrderStatus_ID = "ctl00_ctl00_cphContent_cphSection_lblStatus";
	public static final String Std_OC_Orderdate_ID = "ctl00_ctl00_cphContent_cphSection_lblCreateDate";
	public static final String Std_OC_Printingcostcenter_ID = "ctl00_ctl00_cphContent_cphSection_lblBilledToCostCenter";
	public static final String Std_OC_ShippingCostCente_ID = "ctl00_ctl00_cphContent_cphSection_lblUpgradedShippingValue";

	public static final String Std_OC_ShippedTo_ID = "ctl00_ctl00_cphContent_cphSection_lnkMailList";

	public static final String Std_OC_OrderDetails_State_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[5]";
	public static final String Std_OC_Quantity_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[6]/span";

	public static final String Std_OC_OKBtn_ID = "ctl00_ctl00_cphContent_cphPageFooter_btnCopy";
	public static final String Std_OC_CopyBtn_ID = "ctl00_ctl00_cphContent_cphPageFooter_btnOk";

	public static final String Std_OC_SubmitCancelReqBtn_Path = "//a[text()='Submit Cancel Request']";
	public static final String Std_OC_CloseBtn_ID = "ctl00_ctl00_cphContent_cphSection2_btnClose";

}
