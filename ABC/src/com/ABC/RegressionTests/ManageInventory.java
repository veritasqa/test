package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class ManageInventory extends BaseTestABC {

	public static int Partno = 1;

	public void CreateNewpart(String PartName) throws InterruptedException {

		Type(Xpath, ABC_MI_SI_StockNo_Path, PartName);
		Click(Xpath, ABC_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Wait_ajax();
		while (Element_Is_Displayed(Xpath, ABC_MI_StockNumber1_Path)) {

			Partno = Partno + 1;
			CreatePart = PartName.substring(0, PartName.length() - 1) + Partno;

			Type(Xpath, ABC_MI_SI_StockNo_Path, CreatePart);
			Click(Xpath, ABC_MI_Searchbtn_Path);
			ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);

			Wait_ajax();
		}
		Partno = 1;
	}

	public void liValue_Isdisplayed(String livalue, String Dropdown_name) {

		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value(livalue)),
				livalue + "  is not displayed 'Create New Type' in " + Dropdown_name);

	}

	@Test(priority = 1, enabled = false)
	public void ABC_TC_2_5_3_1_1() throws InterruptedException {

		/*
		 * Page opens with create new item, search items, and search results grids and displays appropriate fields
		 * */

		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_New_CreateNewTypeTitle_Path),
				"'Create New Item' section on page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_SearchItemsTitle_Path),
				"'Search Item' section on page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_SearchResults_Path),
				"'Search Results' section on page is not displayed");
		softAssert.assertAll();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_New_CreateNewType_Path),
				"'Create New Type' drop-down is not displayd in 'Create New Item' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_New_CreateNewType_Path, "value").equalsIgnoreCase("- Any -"),
				"'Create New Type' drop-down has '- Any -' is not the default value");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_New_Add_Btn_Path),
				"'Add' button is not displayed near 'Create New Type' text field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_New_StockNoToCopy_Path),
				" 'Form Number to Copy' field is not displayed in 'Create New Item' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_New_Copy_Btn_Path),
				" 'Copy' button displays near 'Form Number To Copy' text field");
		Click(Xpath, ABC_MI_New_CreateNewType_Path);
		Wait_ajax();
		liValue_Isdisplayed("Book Builder", "Create New Type");
		liValue_Isdisplayed("Formsbook", "Create New Type");
		liValue_Isdisplayed("Kit - Custom Wizard", "Create New Type");
		liValue_Isdisplayed("Kit - On the Fly", "Create New Type");
		liValue_Isdisplayed("Kit - Prebuilt", "Create New Type");
		liValue_Isdisplayed("POD", "Create New Type");
		liValue_Isdisplayed("POD � Customizable", "Create New Type");
		liValue_Isdisplayed("Premium Item", "Create New Type");
		liValue_Isdisplayed("Print to Shelf", "Create New Type");
		liValue_Isdisplayed("Stock", "Create New Type");
		Click(Xpath, ABC_MI_New_CreateNewType_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_StockNo_Path),
				" 'Stock Number' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_Description_Path),
				" 'Description' field is not displayd in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_Keyword_Path),
				" 'Keyword' field is not displayd in 'Search Items' section");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("span", "*Searches on: Item ID, Item Description, Notes and Keyword Fields")),
				" '*Searches on: Item ID, Item Description, Notes and Keyword Fields' field is not displayd in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_InventoryType_Path),
				" 'inventory Type' drop-down is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in ' inventory Type' drop-down");
		Click(Xpath, ABC_MI_SI_InventoryType_Icon_Path);
		Wait_ajax();
		liValue_Isdisplayed("Book Builder", "inventory Type");
		liValue_Isdisplayed("Formsbook", "inventory Type");
		liValue_Isdisplayed("Kit - Custom Wizard", "inventory Type");
		liValue_Isdisplayed("Kit - On the Fly", "inventory Type");
		liValue_Isdisplayed("Kit - Prebuilt", "inventory Type");
		liValue_Isdisplayed("POD", "inventory Type");
		liValue_Isdisplayed("POD � Customizable", "inventory Type");
		liValue_Isdisplayed("Premium Item", "inventory Type");
		liValue_Isdisplayed("Print to Shelf", "inventory Type");
		liValue_Isdisplayed("Stock", "inventory Type");
		Click(Xpath, ABC_MI_SI_InventoryType_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_ProductType_Path),
				" 'Type Item' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_ProductType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'Type Item' drop-down");
		Click(Xpath, ABC_MI_SI_ProductType_Icon_Path);
		Wait_ajax();
		liValue_Isdisplayed("Bifold", "Product Type");
		liValue_Isdisplayed("Binder", "Product Type");
		liValue_Isdisplayed("Blogazine", "Product Type");
		liValue_Isdisplayed("Book", "Product Type");
		liValue_Isdisplayed("Book Builder", "Product Type");
		liValue_Isdisplayed("Box", "Product Type");
		liValue_Isdisplayed("Tool", "Product Type");
		liValue_Isdisplayed("Toolkit", "Product Type");
		liValue_Isdisplayed("White Paper", "Product Type");
		liValue_Isdisplayed("Wirebound", "Product Type");
		liValue_Isdisplayed("Workbook", "Product Type");
		Click(Xpath, ABC_MI_SI_ProductType_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_Program_Path),
				" 'Program' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Program_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'Program' drop-down");
		Click(Xpath, ABC_MI_SI_Programicon_Path);
		Wait_ajax();
		liValue_Isdisplayed("360 Assessment", "Program");
		liValue_Isdisplayed("ABI", "Program");
		liValue_Isdisplayed("Advisory Board Fellowship", "Program");
		liValue_Isdisplayed("EAB", "Program");
		liValue_Isdisplayed("Frontline Impact", "Program");
		liValue_Isdisplayed("Health Care", "Program");
		liValue_Isdisplayed("Leader Development", "Program");
		liValue_Isdisplayed("Or That", "Program");
		liValue_Isdisplayed("Other", "Program");
		liValue_Isdisplayed("PT", "Program");
		liValue_Isdisplayed("Supply Closet", "Program");
		Click(Xpath, ABC_MI_SI_Programicon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_Division_Path),
				" 'Division' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Division_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'Division' drop-down");
		Click(Xpath, ABC_MI_SI_Division_Icon_Path);
		Wait_ajax();
		liValue_Isdisplayed("EAB Research", "Division");
		liValue_Isdisplayed("Health Care Research", "Division");
		liValue_Isdisplayed("Health Care Technology", "Division");
		liValue_Isdisplayed("Health Care Consulting", "Division");
		liValue_Isdisplayed("EAB Technology", "Division");
		liValue_Isdisplayed("EAB Services", "Division");
		liValue_Isdisplayed("Student Success Collaborative (SSC)", "Division");
		liValue_Isdisplayed("Meetings Supply Boxes", "Division");
		liValue_Isdisplayed("Health Care Supplies", "Division");
		liValue_Isdisplayed("EAB Supplies", "Division");
		liValue_Isdisplayed("TABCo Supplies", "Division");
		Click(Xpath, ABC_MI_SI_Division_Icon_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_UserGroups_Path),
				" 'User Group' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_UserGroups_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'User Group' drop-down");
		Click(Xpath, ABC_MI_SI_UserGroups_Icon_Path);
		Wait_ajax();
		liValue_Isdisplayed("Administrators", "User Group");
		liValue_Isdisplayed("Pick_Pack", "User Group");
		liValue_Isdisplayed("Supply_Closet", "User Group");
		liValue_Isdisplayed("Talent_Development", "User Group");
		Click(Xpath, ABC_MI_SI_UserGroups_Icon_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_Active_Path),
				" 'Active' field is not displayed in 'Search Items' section ");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"  'Active' as default value in Active' field");
		Click(Xpath, ABC_MI_SI_Active_Icon_Path);
		Wait_ajax();
		liValue_Isdisplayed("Active", "Active");
		liValue_Isdisplayed("Inactive", "Active");
		liValue_Isdisplayed("Obsolete", "Active");
		Click(Xpath, ABC_MI_SI_Active_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_UnitsOnHand_Path),
				" 'Units on Hand' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' is not displayed by default in 'Units On Hand' dropdown");
		Click(Xpath, ABC_MI_SI_UnitsOnHand_Icon_Path);
		Wait_ajax();
		liValue_Isdisplayed("Low Stock", "Units on hand");
		liValue_Isdisplayed("All", "Units on hand");
		liValue_Isdisplayed("Out of Stock", "Units on hand");
		Click(Xpath, ABC_MI_SI_UnitsOnHand_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_SI_Topic_Path),
				" 'Topic' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Topic_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' is not displayed by default in 'Topic' dropdown");
		Click(Xpath, ABC_MI_SI_TopicIcon_Path);
		Wait_ajax();
		liValue_Isdisplayed("360 Results Delivery", "Topic");
		liValue_Isdisplayed("Accountability", "Topic");
		liValue_Isdisplayed("All Intensives", "Topic");
		liValue_Isdisplayed("All Talent Development", "Topic");
		liValue_Isdisplayed("Coaching Skills Workshop AND Unleashing Frontline Potential", "Topic");
		liValue_Isdisplayed("Volume Growth", "Topic");
		liValue_Isdisplayed("Web Applications", "Topic");
		liValue_Isdisplayed("Womens Services", "Topic");
		liValue_Isdisplayed("Workforce", "Topic");
		liValue_Isdisplayed("Workforce Planning", "Topic");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_Searchbtn_Path), " 'Search' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_Clearbtn_Path), " 'Clear' button is not displayed");
		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = false)
	public void ABC_TC_2_5_3_1_2() throws InterruptedException {

		/*
		 * Ability to select create new type and click add with appropriate page opening
		 * */

		CreateNewpart(QA_TESTNEWITEM_8);

		Click(Xpath, ABC_MI_New_CreateNewType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("POD"));
		Click(Xpath, ABC_MI_New_Add_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_Gen_NewItem_headerpart_Path),
				"'Manage Inventory Item: New Item' is not displayed");
		Type(Xpath, ABC_MI_Gen_Stockno_Path, CreatePart);
		Type(Xpath, ABC_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		Type(Xpath, ABC_MI_Gen_txtDescription_Path, CreatePart + "_Description");
		Type(Xpath, ABC_MI_Gen_txtKeyWords_Path, CreatePart + "_Key Words");
		Click(Xpath, ABC_MI_Gen_InventoryType_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("POD"));
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"1 is not Pre-Populated in the Units per Pack Field");
		Click(Xpath, ABC_MI_Gen_Producttype_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Bifold"));
		Click(Xpath, ABC_MI_Gen_Program_List_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "360 Assessment"));
		Click(Xpath, ABC_MI_Gen_Program_List_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, ABC_MI_Gen_Viewability_Input_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Not Viewable"));
		softAssert.assertTrue(Element_Is_selected(Xpath, ABC_MI_Gen_chkFulfillment_Path),
				"Fulfillment is not checked off");

		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Administrators")),
				"Administrators is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Supply_Closet")),
				"Supply_Closet is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Talent_Development")),
				"Talent_Development is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Pick_Pack")),
				"Pick_Pack is not checked off");
		softAssert.assertAll();
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);
		Click(Xpath, ABC_MI_RulesTab_Path);
		Click(Xpath, ABC_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(priority = 3, enabled = false)
	public void ABC_TC_2_5_3_1_3() throws InterruptedException {

		/*
		 * Validate that you can copy from the create new item grid
		 * */
		CreateNewpart(QA_TESTNEWITEM_8);

		Type(Xpath, ABC_MI_New_StockNoToCopy_Path, QA_TESTNEWITEM_X);
		Wait_ajax();
		Click(Xpath, li_value(QA_TESTNEWITEM_X));
		Click(Xpath, ABC_MI_New_Copy_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Text(Xpath, ABC_MI_Gen_NewItem_headerpart_Path).contains("Copy Of QA_TESTNEWITEM_"),
				"'Manage Inventory Item: <Copy Of QA_TESTNEWITEM_xxxx : QA TEST is not displayed");
		Type(Xpath, ABC_MI_Gen_Stockno_Path, CreatePart);
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtPredecessor_Path, "value").equalsIgnoreCase(QA_TESTNEWITEM_X),
				"'Predecessor' field is not pre-populated with <QA_TESTNEWITEM_X>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtDescription_Path, "value").trim()
						.equalsIgnoreCase("QA_TESTNEWITEM_X_Description"),
				"'Description' field is not pre-populated with <QA_TESTNEWITEM_X_Description>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtKeyWords_Path, "value").toUpperCase().trim()
						.equalsIgnoreCase("QA_TESTNEWITEM_X_Keyword"),
				"'Title' field is not pre-populated with <QA_TESTNEWITEM_X_Keyword>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_InventoryType_DropDown_Path, "value").equalsIgnoreCase("POD"),
				" 'Inventory Type' drop-down is not pre-populated with <POD> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_Gen_Producttype_Path, "value").equalsIgnoreCase("Bifold"),
				"'Product Type' drop-down is not pre-populated with<Bifold> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_Program_Input_Path, "value").equalsIgnoreCase("360 Assessment"),
				"'Program' drop-down is not pre-populated with <360 AssessmentJ> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_Viewability_Input_Path, "value").equalsIgnoreCase("Orderable"),
				"'Viewability' drop-down is pre-populated with <Not Viewable>from original part");
		softAssert.assertTrue(Element_Is_selected(Xpath, ABC_MI_Gen_chkFulfillment_Path),
				"Fulfillment is not checked off");

		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Administrators")),
				"Administrators is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Supply_Closet")),
				"Supply_Closet is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Talent_Development")),
				"Talent_Development is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Pick_Pack")),
				"Pick_Pack is not checked off");
		softAssert.assertAll();
		Type(Xpath, ABC_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);

		ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(CreatePart));
		Switch_New_Tab();
		Click(Xpath, ABC_MI_RulesTab_Path);
		Click(Xpath, ABC_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");
	}

	@Test(priority = 4, enabled = false)
	public void ABC_TC_2_5_3_1_4() throws InterruptedException {

		/*
		 * Validate that a part can be copied from the search results table
		 * */

		CreateNewpart(QA_Copy_SearchResults_1);

		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_Copy_SearchResults_X);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsCopy(QA_Copy_SearchResults_X));
		Click(Xpath, ABC_MI_CopyalertOK_Path);
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Text(Xpath, ABC_MI_Gen_NewItem_headerpart_Path).contains("Copy Of QA_Copy_SearchResults"),
				"'Manage Inventory Item: <Copy Of QA_Copy_SearchResults_Xxxx :  is not displayed");
		Type(Xpath, ABC_MI_Gen_Stockno_Path, CreatePart);
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtPredecessor_Path, "value").equalsIgnoreCase(QA_Copy_SearchResults_X),
				"'Predecessor' field is not pre-populated with <QA_Copy_SearchResults_X>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtDescription_Path, "value").trim()
						.equalsIgnoreCase("QA_Copy_SearchResults_Description"),
				"'Description' field is not pre-populated with <QA_Copy_SearchResults_Description>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtKeyWords_Path, "value").toUpperCase().trim()
						.equalsIgnoreCase("QA_Copy_SearchResults_keywords"),
				"'Title' field is not pre-populated with <QA_Copy_SearchResults_keywords>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_InventoryType_DropDown_Path, "value").equalsIgnoreCase("POD"),
				" 'Inventory Type' drop-down is not pre-populated with <POD> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_Gen_Producttype_Path, "value").equalsIgnoreCase("Bifold"),
				"'Product Type' drop-down is not pre-populated with<Bifold> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_Program_Input_Path, "value").equalsIgnoreCase("360 Assessment"),
				"'Program' drop-down is not pre-populated with <360 AssessmentJ> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_Viewability_Input_Path, "value").equalsIgnoreCase("Orderable"),
				"'Viewability' drop-down is pre-populated with <Not Viewable>from original part");
		softAssert.assertTrue(Element_Is_selected(Xpath, ABC_MI_Gen_chkFulfillment_Path),
				"Fulfillment is not checked off");

		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Administrators")),
				"Administrators is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Supply_Closet")),
				"Supply_Closet is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Talent_Development")),
				"Talent_Development is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Pick_Pack")),
				"Pick_Pack is not checked off");
		softAssert.assertAll();
		Type(Xpath, ABC_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);

		ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(CreatePart));
		Switch_New_Tab();
		Click(Xpath, ABC_MI_RulesTab_Path);
		Click(Xpath, ABC_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");
	}

	@Test(priority = 5, enabled = true)
	public void ABC_TC_2_5_3_1_5() throws InterruptedException {

		/*
		 * Ensure all the Search items fields are working and appropriate search results appear
		 * */

		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_Multifunction);
		Click(Xpath, ABC_MI_SI_Active_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("- Any -"));
		Thread.sleep(1000);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_MI_Searchresults(QA_Multifunction)),
				QA_Multifunction + " is not displayed in the 'Search Results' section");
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_StockNo_Path, "value").isEmpty(),
				"Stock No field is not blank after cleared");

		Type(Xpath, ABC_MI_SI_Description_Path, "QA_Multifunction_Description");
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Assert.assertTrue(
				Get_Text(Xpath, ABC_MI_Description1_Path).trim().equalsIgnoreCase("QA_Multifunction_Description"),
				"Record with 'QA_Multifunction_Description'  is not displayed in the 'Search Results' grid");
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Description_Path, "value").isEmpty(),
				"Description field is not blank after cleared");

		Type(Xpath, ABC_MI_SI_Keyword_Path, "QA_Multifunction_keywords");
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(
				Get_Attribute(Xpath, ABC_MI_Gen_txtKeyWords_Path, "value").trim()
						.equalsIgnoreCase("QA_Multifunction_keywords"),
				"'QA_Multifunction_keywords' is not displayed in the 'Keyword' field");
		Switch_Old_Tab();
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Keyword_Path, "value").isEmpty(),
				"Keyword field is not blank after cleared");

		Click(Xpath, ABC_MI_SI_InventoryType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("POD"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, ABC_MI_Type1_Path).contains("POD"),
				"Parts in 'Search Results' grid does not have 'POD' in 'Type' column");
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");

		Click(Xpath, ABC_MI_SI_ProductType_Path);
		Wait_ajax();
		Click(Xpath, li_value("Bifold"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_ProductType_Path, "value").equalsIgnoreCase("Bifold"),
				"'Bifold' is not displayed in the 'Product Type' field");
		Switch_Old_Tab();
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_ProductType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");

		Click(Xpath, ABC_MI_SI_Program_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Health Care"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Click(Xpath, ABC_MI_Gen_Program_Input_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_selected(Xpath, Dropdowncheckbox("Health Care")),
				"Health Care is not selected in  Channel dropdown");
		Switch_Old_Tab();
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Program_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Channel' drop-down after cleared");

		Click(Xpath, ABC_MI_SI_Division_Icon_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("EAB Research"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, DivsionCheckbox("EAB Research")),
				"'EAB Research' is not selected in the 'Category' field");
		Switch_Old_Tab();
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Division_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Category' drop-down after cleared");

		Click(Xpath, ABC_MI_SI_UserGroups_Path);
		Wait_ajax();
		Click(Xpath, li_value("Talent_Development"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Talent_Development")),
				"Talent_Developments is not checked off");
		Switch_Old_Tab();
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_UserGroups_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'User Group' drop-down after cleared");

		Assert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"'Active' is not pre papulated in the 'Active drop'down");
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Thread.sleep(1000);
		Assert.assertTrue(Get_Text(Xpath, ABC_MI_Status1_Path).equalsIgnoreCase("A"),
				"In 'Search Results' grid have 'A' in 'Status' Column is not displayed in the first row ");
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Searchloadingpanel_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"'Active' is not displayed in 'Active' field after clicked clear button");

		Click(Xpath, ABC_MI_SI_UnitsOnHand_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Out of Stock"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, ABC_MI_Onhand1_Path).equals("0"),
				"Results in 'Search Results' grid does not have '0' in 'On Hand' column");
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Units On Hand' field");

		Click(Xpath, ABC_MI_SI_TopicIcon_Path);
		Wait_ajax();
		Click(Xpath, li_value("360 Results Delivery"));
		Wait_ajax();
		Click(Xpath, ABC_MI_Searchbtn_Path);
		Thread.sleep(500);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, ABC_MI_Gen_Save_Path);
		Click(Xpath, ABC_MI_Gen_Topic_Arrow_Path);
		Thread.sleep(500);
		Assert.assertTrue(Element_Is_selected(Xpath, Textpath("label", "360 Results Delivery")),
				"360 Results Delivery is not selected in Topic Dropdown");
		Switch_Old_Tab();
		Click(Xpath, ABC_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_MI_SI_Topic_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Product Area' field");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		softAssert = new SoftAssert();
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		ManageInventoryNavigation();
		Closealltabs();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
