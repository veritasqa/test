package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class Fulfillment_User_Privilege extends BaseTestABC {

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[2]");
	}

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_8_8_1() throws InterruptedException {

		/*
		 * Verify the part is available with a same user group as the user logged in
		 */
		ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_usergroup);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(QA_usergroup));
		Switch_New_Tab();
		Assert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Talent_Development")),
				"Talent_Developments is not checked in MI page");
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		Type(Xpath, ABC_MU_UserName_Path, qaautoadmin);
		Click(Xpath, ABC_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Clickedituser(qaautoadmin);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Switch_New_Tab();
		Assert.assertTrue(Element_Is_selected(Xpath, UsergroupCheckbox("Talent_Development")),
				"Talent_Developments is not checked in manage users page");
		ClickHome();
		FulfilmentSearch(QA_usergroup);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsAddtoCartBtnXpath(QA_usergroup)),
				QA_usergroup + " is not displayed ");
	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "ABC_TC_2_8_8_1")
	public void ABC_TC_2_8_8_2() throws InterruptedException, IOException {

		/*
		 * Verifying the part is not available when no user group is selected for the part for any user
		 * */
		ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_usergroup);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(QA_usergroup));
		Switch_New_Tab();
		Click(Xpath, UserGroupsCheckbox("Talent_Development"));
		Click(Xpath, UserGroupsCheckbox("Supply_Closet"));
		Click(Xpath, UserGroupsCheckbox("Administrators"));
		Click(Xpath, UserGroupsCheckbox("Pick_Pack"));
		MI_Save();
		login(qaautoadmin, qaautoadmin);
		FulfilmentSearch(QA_usergroup);
		Assert.assertFalse(Element_Is_Displayed(Xpath, SearchResultsAddtoCartBtnXpath(QA_usergroup)),
				QA_usergroup + " is displayed ");
		Closealltabs();
		ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_usergroup);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(QA_usergroup));
		Switch_New_Tab();
		Click(Xpath, UserGroupsCheckbox("Talent_Development"));
		Click(Xpath, UserGroupsCheckbox("Supply_Closet"));
		Click(Xpath, UserGroupsCheckbox("Administrators"));
		Click(Xpath, UserGroupsCheckbox("Pick_Pack"));
		MI_Save();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Closealltabs();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
