package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class Materials extends BaseTestABC {

	public String Material_Edit(String Material) {

		return Textpath("td", Material) + "/following::a[2]";

	}

	public String Material_Activeck(String Material) {

		return ".//input[@value='" + Material + "']/following::input[4]";

	}

	@Test(priority = 1, enabled = false)
	public void ABC_TC_2_3_1_1() throws InterruptedException, IOException {

		/*
		 * Validate active category appears under materials dropdown
		 * */

		NavigateMenu(LP_Admin_path, LP_ManageCategory_path);

		if (Get_Attribute(Xpath, Textpath("td", "QA Test Materials"), "class").equalsIgnoreCase("Inactive")) {

			Click(Xpath, Material_Edit("QA Test Materials"));
			ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
			Click(Xpath, Material_Activeck("QA Test Materials"));
			Click(Xpath, Textpath("a", "Update"));
			ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);

		}

		ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_Multifunction);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(QA_Multifunction));
		Switch_New_Tab();
		if (Element_Is_selected(Xpath, DivsionCheckbox("QA Test Materials")) == false) {
			Click(Xpath, DivsionCheckbox("QA Test Materials"));
			MI_Save();
			login(qaautoadmin, qaautoadmin);
		}
		Closealltabs();
		MouseHover(Xpath, LP_Materials_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, LP_QAMaterials_path),
				"QA Test Materials is not displayed under materials");
	}

	@Test(priority = 2, enabled = true)
	public void ABC_TC_2_3_1_3() throws InterruptedException, IOException {

		/*
		 * Validate part appears in product search with appropriate header on Materials page
		 * */
		NavigateMenu(LP_Materials_path, LP_TripMaterials_path);
		Wait_ajax();
		Assert.assertTrue(Get_Title().toLowerCase().contains("search results"), "Search results page is not displayed");
		Assert.assertTrue(
				Get_Text(Xpath, ABC_SR_Matchingitems_Materials_Path).trim()
						.equalsIgnoreCase("Matching item(s) for: Division of 'Trip Prep', Ordering State of 'IL',"),
				"Matching item(s) for: Division of 'Trip Prep', Ordering State of 'IL',' is not displayed on the header bar");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsAddtoCartBtnXpath(QA_Multifunction)),
				QA_Multifunction + " is not displayed under Trip Prep ");
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Closealltabs();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
