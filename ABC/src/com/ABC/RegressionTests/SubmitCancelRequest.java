package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class SubmitCancelRequest extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_8_8_1() throws InterruptedException {

		/*
		 * Validate on 'confirmation' page submit a cancel request and verify a support ticket is created
		 */

		Click(Xpath, ABC_OC_Cancelreq_Btn_Path);
		Accept_Alert();
		Wait_ajax();
		Click(Xpath, Textpath("a", "Close"));
		Wait_ajax();
		Click(Xpath, LP_Support_path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, ABC_ST_VT_OrderNo1_Path).trim().equalsIgnoreCase(OrderNumber),
				"Order no is not displayed in the support Page");
		Supporttickenumber = Get_Text(Xpath, ABC_ST_VT_TicketNo1_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		System.out.println("Created Support ticket # is " + Supporttickenumber);
		Reporter.log(Supporttickenumber);

	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "ABC_TC_2_8_8_1")
	public void ABC_TC_2_8_8_2() throws InterruptedException {

		/*
		 * Validate cancel request ticket appears on support page
		 */
		softAssert.assertTrue(Get_Text(Xpath, ABC_ST_VT_SubCat1_Path).trim().equalsIgnoreCase("Cancel Order"),
				"Cancel Order is not displayed in the Sub Categoty column");
		softAssert.assertTrue(Get_Text(Xpath, ABC_ST_VT_Status1_Path).trim().equalsIgnoreCase("NEW"),
				"NEW is not displayed in the Sub Categoty column");
		softAssert.assertAll();

	}

	@Test(priority = 3, enabled = true, dependsOnMethods = "ABC_TC_2_8_8_2")
	public void ABC_TC_2_8_8_3() throws InterruptedException {

		/*
		 * Verify cancel request ticket works appropriately
		 */
		Click(Xpath, EditButtonOrderNumber(Supporttickenumber));
		Click(Xpath, ABC_STM_Addcommenticon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Type(Xpath, ABC_STM_Commenttext_Path, "QA Test");
		Click(Xpath, ABC_STM_Createcomment_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_STM_Closedstatus_Path);
		ExplicitWait_Element_Visible(Xpath, ABC_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_UpdateTicketmsg_Path),
				" The Ticket was successfully updated is not displayed - 1st");
		Click(Xpath, ABC_STM_UpdateTicketbtn_Path);
		ExplicitWait_Element_Visible(Xpath, ABC_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_UpdateTicketmsg_Path),
				" The Ticket was successfully updated is not displayed - 2nd");
		Click(Xpath, LP_Support_path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, ABC_ST_VT_TicketNo1_Path).trim().equalsIgnoreCase(Supporttickenumber),
				"Ticket no is not displayed in the support Page");
		Type(Xpath, ABC_ST_Statusfilter_Path, "Closed");
		Selectfilter(ABC_ST_Statusfilter_Path, "Contains");
		Wait_ajax();
		Assert.assertEquals("Closed", Get_Text(Xpath, ABC_ST_VT_Status1_Path),
				"Closed is the displayed in the stauts coulmn");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Wait_ajax();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();
		FF_Order();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
