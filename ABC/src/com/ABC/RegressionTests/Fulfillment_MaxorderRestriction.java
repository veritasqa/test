package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class Fulfillment_MaxorderRestriction extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_8_6_1() throws InterruptedException {

		/*
		 * Validate max order quantity default
		 * */

	ManageInventoryNavigation();
		Type(Xpath, ABC_MI_SI_StockNo_Path, QA_MaxOrder);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(QA_MaxOrder));
		Switch_New_Tab();
		Click(Xpath, ABC_MI_RulesTab_Path);
		Assert.assertTrue(Get_Text(Xpath, ABC_MI_Rules_MaxorderQuantity1_Path).equalsIgnoreCase("5"),
				"5 is not displayed in the max order quantity");

		Type(ID, ABC_LP_FulfilmentSearch_TxtBox_ID, QA_MaxOrder);
		Click(ID, ABC_LP_FulfilmentSearch_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_MaxOrder)),
				QA_MaxOrder + " is not displayed in the search results page");
		Type(Xpath, SearchResultsQty(QA_MaxOrder), "6");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_MaxOrder));
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("div", "Max Order Quantity is 5")),
				"Max Order Quantity is 5 is not displayed");
		Type(Xpath, SearchResultsQty(QA_MaxOrder), "3");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_MaxOrder));
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("div", "Successfully Added 3!")),
				"Successfully Added 3! is not displayed");
		Type(Xpath, SearchResultsQty(QA_MaxOrder), "5");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_MaxOrder));
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("div", "Max Order Quantity is 2")),
				"Max Order Quantity is 2 is not displayed");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
