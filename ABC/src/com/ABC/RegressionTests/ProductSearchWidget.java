package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class ProductSearchWidget extends BaseTestABC {

	@Test(priority = 1, enabled = false)
	public void ABC_TC_2_7_4_1_1() throws InterruptedException {

		/*
		 * Search by DERF#/Title/Program	
		 * */

		Type(Xpath, ABC_Productsearch_Materials_Textbox_Path, QA_Multifunction);
		Click(Xpath, ABC_Productsearch_SearchBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath(QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Clearcarts();

		Type(Xpath, ABC_Productsearch_Materials_Textbox_Path, "Health Care");
		Click(Xpath, ABC_Productsearch_SearchBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Clear(Xpath, ABC_SR_SearchMaterialsBy_TxtBox_Path);
		Type(Xpath, ABC_SR_SearchMaterialsBy_TxtBox_Path, QA_Multifunction);
		Click(Xpath, ABC_SR_Search_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath(QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Clearcarts();

		Type(Xpath, ABC_Productsearch_Materials_Textbox_Path, "QA_Multifunction_keywords");
		Click(Xpath, ABC_Productsearch_SearchBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath(QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page - Keyword Search");
		Clearcarts();

	}

	@Test(priority = 2, enabled = true)
	public void ABC_TC_2_7_4_2_1() throws InterruptedException {

		/*
		 * Verify adding part to cart from product search and clearing cart
		 * */

		Type(Xpath, ABC_Productsearch_Materials_Textbox_Path, QA_Multifunction);
		Click(Xpath, ABC_Productsearch_SearchBtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath(QA_Multifunction));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not displayed in the shopping cart Page");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
