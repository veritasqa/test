package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class LoginAndLogout extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_1_0_1_2() {

		// Verify Active user login

		Type(ID, ABC_Login_UserName_ID, qaautoadmin);
		Type(ID, ABC_Login_Password_ID, qaautoadmin);
		Click(ID, ABC_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_ViewAllAnnouncements_path),
				"Announcements is not displayed is not displayed");
	}

	@Test(priority = 2, enabled = true)
	public void ABC_TC_2_12_1_1() throws IOException, InterruptedException {

		/*
		 * Validates display of "Logout" on all pages of the site (Header)
		 */

		login(qaautoadmin, qaautoadmin);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_Logout_Path),
				"Log out link is not displayed in Home page");
		Click(Xpath, LP_Catalog_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_Logout_Path),
				"Log out link is not displayed in Catalog page");
		Click(Xpath, LP_Materials_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_Logout_Path),
				"Log out link is not displayed in Materials page");
		Click(Xpath, LP_Support_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_Logout_Path),
				"Log out link is not displayed in Support page");
		ManageInventoryNavigation();
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_Logout_Path), "Log out link is not displayed in MI page");
		Click(Xpath, LP_Specialproject_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_LP_Logout_Path),
				"Log out link is not displayed in Special Project page");

	}

	@Test(priority = 3, enabled = true)
	public void ABC_TC_2_12_1_3() throws IOException, InterruptedException {

		/*
		 * Validate "Logout" function and redirection back to the "Login page"
		 */
		Homepage();
		Click(ID, ABC_LP_Logout_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("h1", "Login")), "Login page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, ABC_Login_UserName_ID), "Username is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, ABC_Login_Password_ID), "Password is not displayed");
		softAssert.assertAll();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
