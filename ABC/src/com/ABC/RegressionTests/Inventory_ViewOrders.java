package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class Inventory_ViewOrders extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_VIP_1_1_1_1AndABC_TC_VIP_1_1_2_4() throws InterruptedException {

		/*
		 * Validate searching functionality for an order appears appropriately
		 * */
		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Select_DropDown_VisibleText(Xpath, Inventory_CustomerID_Path, "Advisory Board Company");
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);

		/*
		 * ABC_TC_VIP_1_1_2_4
		 * Validate that an order can be cancelled
		 * */

		Click(Xpath, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Select_DropDown_VisibleText(Xpath, Inventory_CustomerID_Path, "Advisory Board Company");
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)), "Order No is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "CANCELLED")), "CANCELLED is not displayed");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		// Clearcarts();
		// FF_Order();
		// logout();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
