package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class AdminSupport extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_4_3_1andABC_TC_2_4_3_2andABC_TC_2_4_3_3() throws InterruptedException {

		/*
		 * Add a new support ticket in Veritas section
		 * */

		Click(Xpath, ABC_ST_VT_Addticketicon_Path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, ABC_ST_CST_Categorydd_Path, "Other");
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, ABC_ST_CST_SubCategorydd_Path, "Other");
		Type(Xpath, ABC_ST_CST_Commentfield_Path, "QATest");
		Click(Xpath, ABC_ST_CST_Createbtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_Ticketno_Path), "Ticket # is not displayeds");
		Supporttickenumber = Get_Text(Xpath, ABC_STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		System.out.println("Created Support ticket # is " + Supporttickenumber);
		Reporter.log(Supporttickenumber);

		/*
		 * ABC_TC_2_4_3_2
		 * Validate user is able to update the Ticket on View/Edit Page
		 * */

		Click(Xpath, LP_Support_path);
		Wait_ajax();
		Click(Xpath, EditButtonOrderNumber(Supporttickenumber));
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_NewStatus_Path), " 'New' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_Inprogress_Path),
				" 'Inprogress' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_Hold_Path), " 'Hold' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_Closedstatus_Path),
				" 'Closed' button is not displayed");
		softAssert.assertAll();
		Assert.assertTrue(Get_Text(Xpath, ABC_STM_Ticketno_Path).trim().equalsIgnoreCase(Supporttickenumber),
				" the ticket is not opened and displayed with the <Ticket #> displayed on the top right side of the page ");
		Click(Xpath, ABC_STM_Inprogress_Path);
		ExplicitWait_Element_Visible(Xpath, ABC_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_UpdateTicketmsg_Path),
				" The Ticket was successfully updated is not displayed - 1st");
		Click(Xpath, ABC_STM_UpdateTicketbtn_Path);
		ExplicitWait_Element_Visible(Xpath, ABC_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_UpdateTicketmsg_Path),
				" The Ticket was successfully updated is not displayed - 2nd");

		/*
		 * ABC_TC_2_4_3_3
		 * Close the ticket and verify the ticket does not show up on the Support page
		 * */

		Click(Xpath, LP_Support_path);
		Wait_ajax();
		Type(Xpath, ABC_ST_TicketNotxt_Path, Supporttickenumber);
		Click(Xpath, ABC_ST_OrderNofilter_Path);
		Wait_ajax();
		Click(Xpath, EditButtonOrderNumber(Supporttickenumber));
		Click(Xpath, ABC_STM_Closedstatus_Path);
		ExplicitWait_Element_Visible(Xpath, ABC_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_UpdateTicketmsg_Path),
				" The Ticket was successfully updated is not displayed - 1st");
		Click(Xpath, ABC_STM_UpdateTicketbtn_Path);
		ExplicitWait_Element_Visible(Xpath, ABC_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_STM_UpdateTicketmsg_Path),
				" The Ticket was successfully updated is not displayed - 2nd");
		Click(Xpath, LP_Support_path);
		Wait_ajax();
		Type(Xpath, ABC_ST_TicketNotxt_Path, Supporttickenumber);
		Click(Xpath, ABC_ST_OrderNofilter_Path);
		Wait_ajax();
		Assert.assertEquals("Closed", Get_Text(Xpath, ABC_ST_VT_Status1_Path),
				"Closed is the displayed in the stauts coulmn");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Click(Xpath, LP_Support_path);
		Wait_ajax();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
