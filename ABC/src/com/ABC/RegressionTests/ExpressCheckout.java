package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class ExpressCheckout extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_7_4_2_1() throws InterruptedException {

		/*
		 * Verify adding parts to cart to create an order
		 * */

		Type(Xpath, ABC_LP_EC_ProductCode_Txtbox1_Path, QA_Multifunction);
		ExplicitWait_Element_Clickable(Xpath, Textpath("li", QA_Multifunction));
		Click(Xpath, Textpath("li", QA_Multifunction));
		Type(Xpath, ABC_LP_EC_Qty_Txtbox1_Path, "1");
		Click(Xpath, ABC_LP_EC_CheckoutBtn_path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, ABC_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, ABC_SCP_Next_Btn_ID);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Datepicker2(Xpath, ABC_SCP_DeliverydateCal_Path, Xpath, ABC_SCP_DeliverydateCalMonth_Path,
				Get_Futuredate("MMM"), Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Visible(Xpath, Textpath("span", "FEDEX_GROUND"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "FEDEX_GROUND")),
				"FEDEX_GROUND is not displayed");
		Click(Xpath, ABC_SCP_Checkout_path);
		Get_Orderno();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
