package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class ManageUsers extends BaseTestABC {

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[2]");
	}

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_5_10_1_3() throws InterruptedException {

		/*
		 * Validate Edit option opens on Search Result Row and allows you to edit the user
		 * */

		Type(Xpath, ABC_MU_UserName_Path, qaautoadmin);
		Click(Xpath, ABC_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Clickedituser(qaautoadmin);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, ABC_MU_Add_Address_Path, "913 Commerce Court 123");
		Click(Xpath, ABC_MU_Edit_Updatebtn_Path);

		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Clickedituser(qaautoadmin);
		Assert.assertTrue(
				Get_Attribute(Xpath, ABC_MU_Add_Address_Path, "value").equalsIgnoreCase("913 Commerce Court 123"),
				"'913 Commerce Court 123' is not displayed in 'Address' field");
		Type(Xpath, ABC_MU_Add_Address_Path, "913 Commerce Court");
		Click(Xpath, ABC_MU_Edit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
