package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class Add_Special_Projects extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_6_1_1() {

		/*
		 * Validate upon clicking "Add Special Projects" takes to General Special projects
		 * */
		MouseHover(Xpath, LP_Specialproject_path);
		Click(Xpath, LP_AddSpecialproject_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Generatab_Path), "General Tab is not displayed");

	}

	@Test(priority = 2, enabled = true)
	public void ABC_TC_2_6_1_2() throws InterruptedException {

		/*
		 * Create a special project without entering data and Verify the error messages
		 * */

		softAssert.assertTrue(Get_Attribute(Xpath, ABC_SP_Gen_JobTitle_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		softAssert.assertTrue(Get_DropDown(Xpath, ABC_SP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"- Select - is not displayed in the Job Type Dropdown");
		softAssert.assertTrue(
				Get_Text(Xpath, ABC_SP_Gen_Desctiption_Path).trim().equalsIgnoreCase("PO And Files Attached"),
				"PO And Files Attached is not displayed in the description");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_SP_Gen_CC_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_SplInst_Path).trim().isEmpty(),
				"\" \" is not displayed in the Special instruction");
		softAssert.assertFalse(Element_Is_selected(Xpath, ABC_SP_Gen_ISRush_Path), "Is Quote Needed is not unchecked");
		softAssert.assertFalse(Element_Is_selected(Xpath, ABC_SP_Gen_IsQuoteneeded_Path),
				"Is Quote Needed is not unchecked");
		softAssert.assertAll();
		Click(Xpath, ABC_SP_Gen_Create_Path);
		Wait_ajax();

		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("div", "You must enter a value in the following fields")),
				"You must enter a value in the following fields: is not displayed- 1st save");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Cost Center Is Required")),
				"Cost Center Is Required is required is not displayed - 1st save");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Job Title Is Required")),
				"Job Title is required is required is not displayed - 1st save");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Job Type Is Required")),
				"Job Type is required is required is not displayed- 1st save");
		softAssert.assertAll();
		Select_DropDown_VisibleText(Xpath, ABC_SP_Gen_JobType_Path, "Other");
		Click(Xpath, ABC_SP_Gen_Create_Path);
		Wait_ajax();
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("div", "You must enter a value in the following fields")),
				"You must enter a value in the following fields: is not displayed - 2nd Save");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Cost Center Is Required")),
				"Cost Center Is Required is required is not displayed - 2nd Save");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Job Title Is Required")),
				"Job Title is required is required is not displayed - 2nd Save");
		softAssert.assertAll();
		Type(Xpath, ABC_SP_Gen_JobTitle_Path, "QA");
		Click(Xpath, ABC_SP_Gen_Create_Path);
		Wait_ajax();
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("div", "You must enter a value in the following fields")),
				"You must enter a value in the following fields: is not displayed - 3rd Save");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Cost Center Is Required")),
				"Cost Center Is Required is required is not displayed - 3rd Save");
		softAssert.assertAll();

		Click(Xpath, ABC_SP_ProgramTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking Program Tab");
		Click(Xpath, ABC_SP_CostsTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking Costs Tab");
		Click(Xpath, ABC_SP_InventoryTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking Inventory Tab");
		Click(Xpath, ABC_SP_HistoryTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking History Tab");
		softAssert.assertAll();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

		if (SplprjTicketNumberlist.size() >= 1) {

			for (String SplprjTicketNo : SplprjTicketNumberlist) {

				Click(Xpath, LP_Specialproject_path);
				Wait_ajax();
				Type(Xpath, ABC_SP_TicketNo_Path, SplprjTicketNo);
				Click(Xpath, ABC_SP_JobtitleFilter_Path);
				ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
				Click(Xpath, ABC_SP_ViewEdit1_Path);
				Wait_ajax();
				if (Get_Text("Xpath", ABC_SP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled") == false) {
					Click(Xpath, ABC_SP_ProgramTab_Path);
					Wait_ajax();
					Click(Xpath, ABC_SP_Prog_Addcommenticon_Path);
					Wait_ajax();
					Type(Xpath, ABC_SP_Prog_AddComment_Path, "QA Test 2");
					Select_lidropdown(ABC_SP_Prog_AddType_Path, Xpath, "Cancel");
					Click(Xpath, ABC_SP_Prog_SubmitCommentbtn_Path);
					Wait_ajax();
					Click(Xpath, ABC_SP_Savebtn_Path);
					Wait_ajax();

				}

			}
		}
	}

}
