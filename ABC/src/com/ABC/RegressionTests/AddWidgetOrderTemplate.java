package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class AddWidgetOrderTemplate extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_7_7_2_2() throws InterruptedException {

		/*
		 * Add to Cart -Create order
		 * */

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath(QA_Multifunction));
		ExplicitWait_Element_Visible(Xpath, Textpath("div", "Successfully Added 1!"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1!")),
				"Successfully Added 1! is not displayed");
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();

		Click(Xpath, ABC_SCP_CreateOrderTemplate_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, ABC_SCP_COT_Name_Path, "QA Test");
		Type(Xpath, ABC_SCP_COT_Description_Path, "QA Order Template");
		Click(Xpath, ABC_SCP_COT_Createbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "QA Test order template has been successfully created.")),
				"QA Test order template has been successfully created. message is not displayed");
		Clearcarts();
		ExplicitWait_Element_Clickable(Xpath, ABC_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the order Template widgets");
		Click(Xpath, Wid_Xpath("Order Templates", Ordertemplate_Addtocart1_Path));
		Click(Xpath, ABC_SCP_Next_Btn_ID);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Datepicker2(Xpath, ABC_SCP_DeliverydateCal_Path, Xpath, ABC_SCP_DeliverydateCalMonth_Path,
				Get_Futuredate("MMM"), Get_Futuredate("d"), Get_Futuredate("YYYY"));
		ExplicitWait_Element_Visible(Xpath, Textpath("span", "FEDEX_GROUND"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "FEDEX_GROUND")),
				"FEDEX_GROUND is not displayed");
		Click(Xpath, ABC_SCP_Checkout_path);
		Get_Orderno();

	}

	@Test(priority = 2, enabled = true)
	public void ABC_TC_2_7_7_2_3() throws InterruptedException {

		/*
		 * Delete an Order Template
		 * */
		Clearcarts();
		Click(Xpath, Ordertemplate_View1_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Order Templates")),
				"Order Template page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + "  is not displayed Orde Template Page");
		softAssert.assertAll();
		Click(Xpath, Containspath("input", "value", "Delete"));
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("a", "QA Test")),
				"QA Test is displayed after delete");
		Homepage();
		Assert.assertFalse(Element_Is_Displayed(Xpath, Ordertemplate_Form1_Path),
				"QA Test is displayed after delete action in Order template widget");
		Reporter.log("Order Template has been deleted");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();
		if (Element_Is_Displayed(Xpath, Ordertemplate_View1_Path)) {
			Click(Xpath, Ordertemplate_View1_Path);
			Wait_ajax();
			while (Element_Is_Displayed(Xpath, Containspath("input", "value", "Delete"))) {

				Click(Xpath, Containspath("input", "value", "Delete"));
			}
		}

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
