package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class FulfillmentViewinMode extends BaseTestABC {

	public void Change_View(String Partname, String View) throws InterruptedException, IOException {

		Type(Xpath, ABC_MI_SI_StockNo_Path, Partname);
		Click(Xpath, ABC_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ABC_MI_Loading_Path);
		Click(Xpath, ABC_MI_SearchresultsEdit(CreatePart));
		Switch_New_Tab();
		Select_lidropdown(ABC_MI_Gen_Viewability_Input_Path, Xpath, View);
		MI_Save();
		login(qaautoadmin, qaautoadmin);

	}

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_8_5_1() throws InterruptedException, IOException {

		/*
		 * Validate that admin only part is viewable for an admin user
		 */
		Change_View(QA_adminonly, "Admin Only");
		FulfilmentSearch(QA_Viewability);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_adminonly)),
				QA_adminonly + " is not displayed in the search results page");

	}

	@Test(priority = 2, enabled = true)
	public void ABC_TC_2_8_5_4() throws InterruptedException, IOException {

		/*
		 * Validate when a part is set to viewable and not orderable that it"s viewable
		 */
		Change_View(QA_Viewability, "Viewable Not Orderable");
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Viewability)),
				QA_Viewability + " is not displayed in the search results page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Not Orderable")),
				"Not Orderable is not displayed in the search results page");

	}

	@Test(priority = 3, enabled = true)
	public void ABC_TC_2_8_5_5() throws InterruptedException, IOException {

		/*
		 * Validate when a part is set to not viewable that is not viewable
		 */

		Change_View(QA_Viewability, "Not Viewable");
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Viewability)),
				QA_Viewability + " is not displayed in the search results page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span",
				"Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active.")),
				"Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active. is not displayed");

	}

	@Test(priority = 4, enabled = true)
	public void ABC_TC_2_8_5_6() throws InterruptedException, IOException {

		/*
		 * Validate when the part is set to orderable that it is viewable and orderable
		 */

		Change_View(QA_Viewability, "Orderable");
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Viewability)),
				QA_Viewability + " is not displayed in the search results page");
		Assert.assertFalse(Element_Is_Displayed(Xpath, SearchResultsAddtoCartBtnXpath(QA_Viewability)),
				QA_Viewability + "Add to cart is not displayed in the search results page");
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Closealltabs();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
