package com.ABC.RegressionTests;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class SpecialProjects extends BaseTestABC {

	public void Creat_SplProject() throws InterruptedException {

		Click(Xpath, ABC_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, ABC_SP_Gen_JobTitle_Path, "QA Test 123");
		Select_DropDown_VisibleText(Xpath, ABC_SP_Gen_JobType_Path, "Other");
		Type(Xpath, ABC_SP_Gen_Desctiption_Path, "QA is Testing");
		Type(Xpath, ABC_SP_Gen_CC_Path, "9999");
		Type(Xpath, ABC_SP_Gen_SplInst_Path, "QA is testing");
		Click(Xpath, ABC_SP_Gen_ISRush_Path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Gen_IsQuoteneeded_Path);
		Click(Xpath, ABC_SP_Gen_Create_Path);
		Wait_ajax();
		SplprjTicketNumber = Get_Text(Xpath, ABC_SP_Gen_TicketNo_Path).trim();
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Reporter.log("Created Special project no is " + SplprjTicketNumber);
	}

	@Test(priority = 1, enabled = false)
	public void ABC_TC_2_6_1_3() {

		/*
		 * Verify "Add Ticket", "View/Edit", "Refresh" button displays on the Special Projects table.
		* */

		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Addticketicon_Path),
				"\"Add Ticket\" button is not displays on bottom left of 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Refreshicon_Path),
				"\"Refresh\" button is not displaysin the 'Projects' page");
		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = false)
	public void ABC_TC_2_6_1_4() throws InterruptedException {

		/*
		 * Validate "Add ticket button", Edit/View button takes to "General Special projects" page
		* */

		Click(Xpath, LP_Specialproject_path);
		Click(Xpath, ABC_SP_Addticketicon_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(Xpath, ABC_SP_Gen_JobTitle_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		Click(Xpath, ABC_SP_Gen_Cancel_Path);
		Click(Xpath, ABC_SP_ViewEdit1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Gen_JobTitle_Path), "Jobtitle field is not displayed");
		Click(Xpath, ABC_SP_Gen_Cancel_Path);

	}

	@Test(priority = 3, enabled = false)
	public void ABC_TC_2_6_1_5() throws InterruptedException {

		/*
		 * Add a new special project and verify new ticket appears on Special projects: view tickets table
		 * */

		Click(Xpath, ABC_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);

		Type(Xpath, ABC_SP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, ABC_SP_Gen_JobType_Path, "Other");
		Assert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_Desctiption_Path).trim().equalsIgnoreCase("PO And Files Attached"),
				"PO And Files Attached is not displayed in the description");
		Type(Xpath, ABC_SP_Gen_CC_Path, "9999");
		Click(Xpath, ABC_SP_Gen_Create_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed");
		SplprjTicketNumber = Get_Text(Xpath, ABC_SP_Gen_TicketNo_Path).trim();
		Reporter.log("Created Special project no is " + SplprjTicketNumber);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
		Type(Xpath, ABC_SP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, ABC_SP_JobtitleFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, ABC_SP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"Created Special Project no is not displayed first row");

	}

	@Test(priority = 4, enabled = false)
	public void ABC_TC_2_6_1_6() throws InterruptedException, IOException {

		/*
		 * Validate all the Fields/buttons of "General", Progress/Files, Costs are enabled in Special Projects is working fine
		 * */

		Click(Xpath, ABC_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		System.out.println(Get_Text(Xpath, ABC_SP_Gen_Status_Path).trim());
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Gen_JobTitleReq_Path), "Jobtitle * is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_SP_Gen_JobTitle_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		Type(Xpath, ABC_SP_Gen_JobTitle_Path, "QA Test 123");
		softAssert.assertTrue(Get_DropDown(Xpath, ABC_SP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"- Select - is not displayed in the Job Type Dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Gen_JobTypeReq_Path), "Job Type * is not displayed");
		Select_DropDown_VisibleText(Xpath, ABC_SP_Gen_JobType_Path, "Other");
		softAssert.assertTrue(
				Get_Text(Xpath, ABC_SP_Gen_Desctiption_Path).trim().equalsIgnoreCase("PO And Files Attached"),
				"PO And Filpores Attached is not displayed in the description");
		Type(Xpath, ABC_SP_Gen_Desctiption_Path, "QA is Testing");
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_Createdby_Path).trim().equalsIgnoreCase("qaauto"),
				"User name is not displayed Created by");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_SP_Gen_CC_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Gen_CCReq_Path), "Cost Center * is not displayed");
		Type(Xpath, ABC_SP_Gen_CC_Path, "9999");
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_SplInst_Path).trim().isEmpty(),
				"\\\" \\\" is not displayed in the Special instruction");
		Type(Xpath, ABC_SP_Gen_SplInst_Path, "QA Test");
		softAssert.assertFalse(Element_Is_selected(Xpath, ABC_SP_Gen_ISRush_Path), "Is Quote Needed is not unchecked");
		Click(Xpath, ABC_SP_Gen_ISRush_Path);
		Wait_ajax();
		softAssert.assertTrue(
				Get_Attribute(Xpath, ABC_SP_Gen_ISRushCal_Path, "value").equalsIgnoreCase(Get_Todaydate("M/dd/YYYY")),
				"Today's date is not displayed in the Is rush");
		softAssert.assertFalse(Element_Is_selected(Xpath, ABC_SP_Gen_IsQuoteneeded_Path),
				"Is Quote Needed is not unchecked");
		Click(Xpath, ABC_SP_Gen_IsQuoteneeded_Path);
		softAssert.assertAll();
		Click(Xpath, ABC_SP_Gen_Create_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed");
		SplprjTicketNumber = Get_Text(Xpath, ABC_SP_Gen_TicketNo_Path).trim();
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Reporter.log("Created Special project no is " + SplprjTicketNumber);

		Click(Xpath, ABC_SP_ProgramTab_Path);
		Click(Xpath, ABC_SP_Prog_Duedatecal_Path);
		Assert.assertTrue(Element_Is_Enabled(Xpath, Textpath("span", Get_Pastdate("d"))),
				"Past date is selectable in the Due date calender - Program tab");
		Click(Xpath, ABC_SP_Prog_Duedatecal_Path);
		Datepicker2(Xpath, ABC_SP_Prog_Duedatecal_Path, Xpath, ABC_SP_Prog_Duedate_monthcal_Path, Get_Todaydate("MMM"),
				Get_Todaydate("d"), Get_Todaydate("YYYY"));
		Click(Xpath, ABC_SP_Prog_Addcommenticon_Path);
		Wait_ajax();

		Type(Xpath, ABC_SP_Prog_AddComment_Path, "QA Test");
		softAssert.assertTrue(Get_Attribute(Xpath, ABC_SP_Prog_AddType_Path, "value").equalsIgnoreCase("Acknowledged"),
				"Acknowledged is not displayed in the Type  Type Dropdown");
		Click(Xpath, ABC_SP_Prog_Browse_Path);
		Thread.sleep(3000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\ABC\\Utils\\ABC_Upload.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\ABC\\Utils\\ABC_TC_2_6_1_6.xlsx");

		ExplicitWait_Element_Visible(Xpath, Containspath("input", "value", "Remove"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "ABC_TC_2_6_1_6.xlsx")),
				"Uploaded file is not displayed");
		Click(Xpath, ABC_SP_Prog_SubmitCommentbtn_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Prog_Createdate1_Path).contains(Get_Todaydate("M/d/YYYY")),
				"Today date is not displayed in the Craeted column");
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Prog_UserName1_Path).equalsIgnoreCase(qaautoadmin),
				"Username is not displayed in the  user name column");
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Prog_Comment1_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the  Comment column");
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Prog_Type1_Path).equalsIgnoreCase("Acknowledged"),
				"Acknowledged is not displayed in the  Type column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_Prog_Attachment1_Path),
				"file is not displayed Attachment coumn");
		softAssert.assertAll();
		Click(Xpath, ABC_SP_Prog_Attachment1_Path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed - Progress Tab");
		Click(Xpath, ABC_SP_CostsTab_Path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Cost_Billed_Path);
		Click(Xpath, ABC_SP_Cost_Addcosticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, ABC_SP_Cost_AddDescription_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, ABC_SP_Cost_AddType_Path, "Veritas Postage");
		Type(Xpath, ABC_SP_Cost_AddBilldate_Path, Get_Futuredate("MM/dd/yyyy"));
		Type(Xpath, ABC_SP_Cost_AddBillamount_Path, "0.010");
		Click(Xpath, ABC_SP_Cost_AddInsert_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed - Cost tab");
		Click(Xpath, ABC_SP_InventoryTab_Path);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, ABC_SP_Inventory_Jobtype_Path);
		Type(Xpath, ABC_SP_Inventory_StockNo_Path, QA_Multifunction);
		ExplicitWait_Element_Visible(Xpath, Textpath("span", QA_Multifunction));
		Click(Xpath, ABC_SP_Inventory_Addoprojbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, ABC_SP_Inventory_Stock1_Path).trim().equalsIgnoreCase(QA_Multifunction),
				QA_Multifunction + " is not displayed in the  Stock# column");
		Type(Xpath, ABC_SP_Inventory_Qty1_Path, "2");
		Click(Xpath, ABC_SP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed - Invnetory Tab");
		Click(Xpath, ABC_SP_HistoryTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_History_DateTime_Path),
				"Date/Time Column not displayed History coumn");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_History_User_Path),
				"User Column not displayed History coumn");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ABC_SP_History_Details_Path),
				"Details Column not displayed History coumn");
		softAssert.assertAll();
		Click(Xpath, ABC_SP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Prog_Addcommenticon_Path);
		Wait_ajax();
		Type(Xpath, ABC_SP_Prog_AddComment_Path, "QA Test 2");
		Select_lidropdown(ABC_SP_Prog_AddType_Path, Xpath, "Cancel");
		Click(Xpath, ABC_SP_Prog_SubmitCommentbtn_Path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"Cancelled is not displayed in the status Column");

	}

	@Test(priority = 5, enabled = true)
	public void ABC_TC_2_6_1_10AndABC_TC_2_6_1_11() throws InterruptedException, IOException {

		/*
		 * Verify changes are not saved on clicking Cancel button
		 * */
		Creat_SplProject();
		Type(Xpath, ABC_SP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, ABC_SP_JobtitleFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, ABC_SP_ViewEdit1_Path);
		Wait_ajax();
		Type(Xpath, ABC_SP_Gen_SplInst_Path, "QA Retesting");
		Click(Xpath, ABC_SP_Gen_Cancel_Path);
		Wait_ajax();
		Type(Xpath, ABC_SP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, ABC_SP_JobtitleFilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, ABC_SP_ViewEdit1_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_SplInst_Path).trim().equalsIgnoreCase("QA is testing"),
				"QA is testing is not displayed in the Special instruction");

		/*
		 * Validate the cancel functionality
		 */

		Click(Xpath, ABC_SP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Prog_Addcommenticon_Path);
		Wait_ajax();
		Type(Xpath, ABC_SP_Prog_AddComment_Path, "QA is validating cancel functionality");
		Select_lidropdown(ABC_SP_Prog_AddType_Path, Xpath, "Cancel");
		Click(Xpath, ABC_SP_Prog_SubmitCommentbtn_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, ABC_SP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"Cancelled is not displayed in the status Column");
		Click(Xpath, ABC_SP_Savebtn_Path);
		Wait_ajax();
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
		Click(Xpath, ABC_SP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "000453")), "Closed ticket is not displayed");
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

		if (SplprjTicketNumberlist.size() >= 1) {

			for (String SplprjTicketNo : SplprjTicketNumberlist) {

				Click(Xpath, LP_Specialproject_path);
				Wait_ajax();
				Type(Xpath, ABC_SP_TicketNo_Path, SplprjTicketNo);
				Click(Xpath, ABC_SP_JobtitleFilter_Path);
				ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
				Click(Xpath, ABC_SP_ViewEdit1_Path);
				Wait_ajax();
				if (Get_Text("Xpath", ABC_SP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled") == false) {
					Click(Xpath, ABC_SP_ProgramTab_Path);
					Wait_ajax();
					Click(Xpath, ABC_SP_Prog_Addcommenticon_Path);
					Wait_ajax();
					Type(Xpath, ABC_SP_Prog_AddComment_Path, "QA Test 2");
					Select_lidropdown(ABC_SP_Prog_AddType_Path, Xpath, "Cancel");
					Click(Xpath, ABC_SP_Prog_SubmitCommentbtn_Path);
					Wait_ajax();
					Click(Xpath, ABC_SP_Savebtn_Path);
					Wait_ajax();

				}

			}
		}
	}

}
