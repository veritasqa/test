package com.ABC.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.ABC.Base.BaseTestABC;

public class FullfillmentSearch extends BaseTestABC {

	@Test(priority = 1, enabled = true)
	public void ABC_TC_2_8_1_1() {

		/*
		 * Validate that parts can be searched on by using the fulfillment search field
		 * */

		Type(ID, ABC_LP_FulfilmentSearch_TxtBox_ID, QA_Multifunction);
		Click(ID, ABC_LP_FulfilmentSearch_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Multifunction)),
				QA_Multifunction + " is not displayed in the search results page");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Clearcarts();

	}

	@AfterClass(enabled = false)
	public void afterClass() throws InterruptedException {

	}

}
