package com.ABC.OR;

public class SpecialProjectsPage extends ManageUsersPage {

	public static final String ABC_SP_Title_Path = ".//h1[text()='Projects']";
	public static final String ABC_SP_ViewticketTitle_Path = ".//h1[contains(text(),'Special Projects : View Tickets')]";
	public static final String ABC_SP_TicketNo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_TicketNumber']";
	public static final String ABC_SP_TicketNofilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_TicketNumber']";
	public static final String ABC_SP_Jobtitle_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_JobTitle']";
	public static final String ABC_SP_JobtitleFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_JobTitle']";
	public static final String ABC_SP_Jobtype_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes_Input']";
	public static final String ABC_SP_Jobtypearrow_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes']/span/button";
	public static final String ABC_SP_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxStatus_Input']";
	public static final String ABC_SP_Statusarrow_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxStatus']/span/button";
	public static final String ABC_SP_Duedate_Path = ".//input[contains(@id,'FilterTextBox_DueDate')]";
	public static final String ABC_SP_DuedateFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_DueDate']";
	public static final String ABC_SP_Rushck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Rush']";
	public static final String ABC_SP_Rushckfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_Rush']";
	public static final String ABC_SP_Closedck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Closed']";
	public static final String ABC_SP_Closedckfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_Closed']";
	public static final String ABC_SP_TicketNo1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]/a";
	public static final String ABC_SP_Jobtitle1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]";
	public static final String ABC_SP_Jobtype1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[3]";
	public static final String ABC_SP_Status1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[4]";
	public static final String ABC_SP_Duedate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[5]";
	public static final String ABC_SP_Rushck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_ctl00']";
	public static final String ABC_SP_Closedck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_ctl01']";
	public static final String ABC_SP_ViewEdit1_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0\"]/td[8]/a";
	public static final String ABC_SP_Addticketicon_Path = ".//input[contains(@title,'Add Ticket')]";
	public static final String ABC_SP_Addticket_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String ABC_SP_Refreshicon_Path = ".//input[contains(@title,'Refresh')]";
	public static final String ABC_SP_Refresh_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String ABC_SP_Firstpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String ABC_SP_Previouspagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String ABC_SP_Nextpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String ABC_SP_Lastpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String ABC_SP_Pagenotext_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String ABC_SP_Pageof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String ABC_SP_Gobtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String ABC_SP_Pagesize_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String ABC_SP_Changebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String ABC_SP_Itemof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";

	// General Tab

	public static final String ABC_SP_Generatab_Path = ".//span[text()='General']";
	public static final String ABC_SP_Generatab_Job_Path = ".//*[@id='spanJobTitle']";
	public static final String ABC_SP_Generatab_Status_Path = ".//*[@id='spanStatus']";
	public static final String ABC_SP_Generatab_SP_Path = ".//*[@id='spanTicketNumber']";

	public static final String ABC_SP_Gen_TicketNo_Path = ".//span[contains(text(),'Ticket')]//following::span[1][@id='spanTicketNumber']";
	public static final String ABC_SP_Gen_Status_Path = "//*[@id=\"tabs\"]/div[1]/h1/span/span[2]";
	public static final String ABC_SP_Gen_SharepointReq_Path = ".//div[contains(text(),'SharePoint')]//following::div[1]/input[contains(@id,'ProjectID')]//following::span[1]";

	public static final String ABC_SP_Gen_JobTitle_Path = ".//div[contains(text(),'Job Title')]//following::div[1]/input[contains(@id,'JobTitle')]";
	public static final String ABC_SP_Gen_JobTitleReq_Path = ".//div[contains(text(),'Job Title')]//following::div[1]/input[contains(@id,'JobTitle')]//following::span[1]";
	public static final String ABC_SP_Gen_JobType_Path = ".//div[contains(text(),'Job Type')]//following::div[1]/div/select[contains(@id,'JobType')]";
	public static final String ABC_SP_Gen_JobTypeReq_Path = ".//div[contains(text(),'Job Type')]//following::div[1]/div/select[contains(@id,'JobType')]//following::span[1]";
	public static final String ABC_SP_Gen_Desctiption_Path = ".//*[@id='ctl00_cphContent_txtDescription']";

	public static final String ABC_SP_Gen_CC_Path = "//*[@id=\"ctl00_cphContent_txtCostCenter\"]";
	public static final String ABC_SP_Gen_CCReq_Path = "//*[@id=\"tabs-1\"]/div[1]/div[2]/div[1]/div[2]/span[1]";

	public static final String ABC_SP_Gen_SplInst_Path = "//*[@id=\"ctl00_cphContent_txtSpecialInstructions\"]";

	public static final String ABC_SP_Gen_ISRush_Path = "//*[@id=\"ctl00_cphContent_chkRush\"]";
	public static final String ABC_SP_Gen_ISRushCal_Path = "//*[@id=\"ctl00_cphContent_rdpRushDate_dateInput\"]";

	public static final String ABC_SP_Gen_IsQuoteneeded_Path = ".//*[@id='ctl00_cphContent_chkQuoteNeeded']";
	public static final String ABC_SP_Gen_Createdby_Path = ".//*[@id='ctl00_cphContent_lblCreateUser']";

	public static final String ABC_SP_Gen_Addnewicon_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[1]/a/span/span/span/img";
	public static final String ABC_SP_Gen_Addnew_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[1]/a/span/span/span/span";
	public static final String ABC_SP_Gen_Pencil_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[2]/a/span/span/span/img";
	public static final String ABC_SP_Gen_Editselected_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[2]/a/span/span/span/span";
	public static final String ABC_SP_Gen_Deleteicon_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[3]/a/span/span/span/img";
	public static final String ABC_SP_Gen_Deleteitem_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[3]/a/span/span/span/span";
	public static final String ABC_SP_Gen_Refreshicon_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[4]/a/span/span/span/img";
	public static final String ABC_SP_Gen_Refresh_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl00_RadToolBar1']/div/div/div/ul/li[4]/a/span/span/span/span";
	public static final String ABC_SP_Gen_Code_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00']/thead/tr[2]/th[2]";
	public static final String ABC_SP_Gen_Description_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00']/thead/tr[2]/th[3]";
	public static final String ABC_SP_Gen_Codetext_Path = "//*[@id=\"ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl02_TB_Code\"]";
	public static final String ABC_SP_Gen_Descriptiontext_Path = "//*[@id=\"ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl02_TB_Description\"]";
	public static final String ABC_SP_Gen_Codeedittext_Path = "//*[@id=\"ctl00_cphContent_gridMessageDefinitions_ctl00_ctl04_TB_Code\"]";
	public static final String ABC_SP_Gen_Descriptionedittext_Path = "//*[@id=\"ctl00_cphContent_gridMessageDefinitions_ctl00_ctl04_TB_Description\"]";
	public static final String ABC_SP_Gen_Tickicon_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl02_PerformInsertButton']/span";
	public static final String ABC_SP_Gen_Closeicon_Path = ".//*[@id='ctl00_cphContent_gridMessageDefinitions_ctl00_ctl02_ctl02_CancelButton']/span";
	public static final String ABC_SP_Gen_Pencilicon1_Path = "//*[@id=\"ctl00_cphContent_gridMessageDefinitions_ctl00_ctl04_EditButton\"]";
	public static final String ABC_SP_Gen_Tickicon1_Path = "//*[@id=\"ctl00_cphContent_gridMessageDefinitions_ctl00_ctl04_UpdateButton\"]/span";
	public static final String ABC_SP_usersTop_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[1]";
	public static final String ABC_SP_usersDown_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[2]";
	public static final String ABC_SP_usersAlltop_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[3]";
	public static final String ABC_SP_usersAlldown_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[4]";

	public String Teammemberlist(String Username) {

		return ".//div[contains(text(),'Team Members')]//following::li[1]//span[text()='" + Username + " - (Veritas)']";
	}

	public static final String ABC_SP_Gen_Cancel_Path = ".//input[@value='Cancel']";
	public static final String ABC_SP_Gen_Create_Path = ".//input[@value='Create']";
	public static final String ABC_SP_Gen_Downloadarch_Path = ".//input[@value='Download Archive']";

	public static final String ABC_SP_Gen_Save_Path = ".//input[@value='Save']";
	public static final String ABC_SP_Gen_Successmsg_Path = ".//span[text()='Data has been saved.']";
	public static final String ABC_SP_Gen_UnSuccessmsg_Path = ".//label[text()='Create the Special Project, before proceeding.']";

	// Program Tab

	public static final String ABC_SP_ProgramTab_Path = ".//span[text()='Progress / Files']";
	public static final String ABC_SP_Prog_Created_Path = ".//*[@id='ctl00_cphContent_lblCreateDate']";
	public static final String ABC_SP_Prog_Quoteaccepeted_Path = ".//*[@id='ctl00_cphContent_lblQuoteAcceptedDate']";
	public static final String ABC_SP_Prog_Proofcreated_Path = ".//*[@id='ctl00_cphContent_lblProofDate']";
	public static final String ABC_SP_Prog_Proofapproved_Path = ".//*[@id='ctl00_cphContent_lblProofApprovalDate']";
	public static final String ABC_SP_Prog_Duedate_Path = ".//*[@id='ctl00_cphContent_lblDueDate']";
	public static final String ABC_SP_Prog_Duedatecal_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_popupButton']";
	public static final String ABC_SP_Prog_Duedate_monthcal_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_calendar_Title']";
	public static final String ABC_SP_Prog_Completed_Path = ".//*[@id='ctl00_cphContent_lblCompleteDate']";
	public static final String ABC_SP_Prog_Cancelled_Path = ".//*[@id='ctl00_cphContent_lblCancelDate']";
	public static final String ABC_SP_Prog_Holdplaced_Path = ".//*[@id='ctl00_cphContent_lblHoldDate']";
	public static final String ABC_SP_Prog_ToggleArchivedComments_Path = ".//a[text()='Toggle Archived Comments']";
	public static final String ABC_SP_Prog_Addcommenticon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ABC_SP_Prog_Addcomment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String ABC_SP_Prog_Refreshicon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String ABC_SP_Prog_Refresh_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String ABC_SP_Prog_Type_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Input']";
	public static final String ABC_SP_Prog_Attachment_Path = ".//b[contains(text(),'Attachment:')]//following::input[1]";
	public static final String ABC_SP_Prog_Username_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_UserName\"]";
	public static final String ABC_SP_Prog_Topic_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_Topic\"]";
	public static final String ABC_SP_Prog_Status_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_Type\"]";
	public static final String ABC_SP_Prog_Comment_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_Comment\"]";

	public static final String ABC_SP_Prog_AddTopic_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_txtTopic']";
	public static final String ABC_SP_Prog_AddComment_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_txtComment\"]";
	public static final String ABC_SP_Prog_AddType_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Input\"]";
	public static final String ABC_SP_Prog_AddTypearrow_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Arrow\"]";
	public static final String ABC_SP_Prog_Browse_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00_ctl02_ctl02_uploadCommentAttachmentrow0\"]/span";
	public static final String ABC_SP_Prog_SubmitCommentbtn_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnSaveComment']";
	public static final String ABC_SP_Prog_CancelComment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnCancel']";



	public static final String ABC_SP_Prog_Submit_Path = ".//a[text()='Submit']";
	public static final String ABC_SP_Prog_Cancel_Path = ".//a[text()='Cancel' and contains(@id,'Comments')]";

	public static final String ABC_SP_Prog_Createdate1_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00__0\"]/td[1]";
	public static final String ABC_SP_Prog_UserName1_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00__0\"]/td[2]";
	public static final String ABC_SP_Prog_Comment1_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00__0\"]/td[3]";
	public static final String ABC_SP_Prog_Type1_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00__0\"]/td[4]";
	public static final String ABC_SP_Prog_Attachment1_Path = "//*[@id=\"ctl00_cphContent_Comments_ctl00__0\"]/td[5]/a";
	public static final String ABC_SP_Prog_NoneAttachment1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[7]";

	public static final String ABC_SP_Cancelbtn_Path = ".//input[@value='Cancel']";
	public static final String ABC_SP_Savebtn_Path = ".//input[@value='Save']";
	public static final String ABC_SP_DownloadArchive_Path = ".//input[@value='Download Archive']";

	public static final String ABC_SP_SuccessSavemsg_Path = ".//span[text()='Data has been saved.']";

	// Costs Tab

	public static final String ABC_SP_CostsTab_Path = ".//span[text()='Costs']";
	public static final String ABC_SP_Cost_QuoteCost_Path = ".//*[@id='ctl00_cphContent_txtQuoteCost']";
	public static final String ABC_SP_Cost_PrintCost_Path = ".//*[@id='ctl00_cphContent_txtPrintCost']";
	public static final String ABC_SP_Cost_VeritasPostage_Path = ".//*[@id='ctl00_cphContent_txtVeritasPostageCost']";
	public static final String ABC_SP_Cost_Fulfillment_Path = ".//*[@id='ctl00_cphContent_txtFulfillmentCost']";
	public static final String ABC_SP_Cost_ClientPostage_Path = ".//*[@id='ctl00_cphContent_txtClientPostageCost']";
	public static final String ABC_SP_Cost_Rush_Path = ".//*[@id='ctl00_cphContent_txtRushCost']";
	public static final String ABC_SP_Cost_Billed_Path = "//*[@id='ctl00_cphContent_ckBilled']";
	public static final String ABC_SP_Cost_Addcosticon_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ABC_SP_Cost_Addcostitem_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String ABC_SP_Cost_Refreshicon_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String ABC_SP_Cost_Refresh_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String ABC_SP_Cost_AddDescription_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_txtDescription']";
	public static final String ABC_SP_Cost_AddType_Path = "//*[@id=\"ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType\"]";
	public static final String ABC_SP_Cost_AddTypearrow_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType_Arrow']";
	public static final String ABC_SP_Cost_AddBilldate_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rdpBillDate_dateInput']";
	public static final String ABC_SP_Cost_AddBillamount_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rtxtBillAmount']";
	public static final String ABC_SP_Cost_UpdateBillamount_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl05_rtxtBillAmount']";

	public static final String ABC_SP_Cost_AddInsert_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_btnSave']";
	public static final String ABC_SP_Cost_UpdateInsert_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl05_btnSave']";

	public static final String ABC_SP_Cost_AddCancel_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_btnCancelCost']";
	public static final String ABC_SP_Cost_Description1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[1]";
	public static final String ABC_SP_Cost_Type1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[2]";
	public static final String ABC_SP_Cost_Billdate1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[3]";
	public static final String ABC_SP_Cost_Billamount1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[4]";
	public static final String ABC_SP_Cost_Edit1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl04_EditButton']";
	public static final String ABC_SP_Cost_Delete1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[6]/a";
	public static final String ABC_SP_Cost_Deletepop_Path = ".//div[contains(text(),'Delete Item?')]";
	public static final String ABC_SP_Cost_DeleteOK_Path = ".//span[text()='OK']";
	public static final String ABC_SP_Cost_DeleteCancel_Path = ".//span[text()='Cancel']";
	public static final String ABC_SP_Cost_Loading_Path = "//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_rgdCosts']";

	public static final String ABC_SP_Costs_Edit1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl04_EditButton']";
	public static final String ABC_SP_Costs_Delete1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[7]/a";

	public static final String ABC_SP_Costs_Insert_Path = ".//input[@value='Insert']";
	public static final String ABC_SP_Costs_Cancel_Path = ".//input[@value='Cancel' and contains(@id,'Cost')]";

	// Inventory Tab

	public static final String ABC_SP_InventoryTab_Path = ".//span[text()='Inventory']";
	public static final String ABC_SP_Inventory_StockNo_Path = "//*[@id=\"ctl00_cphContent_racbFormNumbers_Input\"]";
	public static final String ABC_SP_Inventory_Jobtype_Path = ".//*[@id='ctl00_cphContent_txtJobType']";
	public static final String ABC_SP_Inventory_Product_Code_Path = ".//*[@id='ctl00_cphContent_racbFormNumbers_Input']";
	public static final String ABC_SP_Inventory_Product_suggestion_Path = ".//td[text()='QA_Multifunction ']";
	public static final String ABC_SP_Inventory_Addoprojbtn_Path = ".//span[text()='Add to Project']";
	public static final String ABC_SP_Inventory_Refreshicon_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00_ctl03_ctl01_RefreshButton']";

	public static final String ABC_SP_Inventory_Stock1_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00__0']/td[2]";
	public static final String ABC_SP_Inventory_Qty1_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00_ctl04_txtQuantityWanted']";
	public static final String ABC_SP_Inventory_Delete1_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00__0']/td[8]/a";
	public static final String ABC_SP_Inventory_Refresh_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00_ctl03_ctl01_RebindGridButton']";

	// History Tab

	public static final String ABC_SP_HistoryTab_Path = ".//span[text()='History']";
	public static final String ABC_SP_History_DateTime_Path = ".//*[@id='ctl00_cphContent_History_ctl00']/thead/tr/th[1]/a";
	public static final String ABC_SP_History_User_Path = ".//*[@id='ctl00_cphContent_History_ctl00']/thead/tr/th[2]/a";
	public static final String ABC_SP_History_Details_Path = ".//*[@id='ctl00_cphContent_History_ctl00']/thead/tr/th[3]/a";
	public static final String ABC_SP_History_DateTime1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[1]";
	public static final String ABC_SP_History_User1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[2]";
	public static final String ABC_SP_History_Details1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";

	// Loading Path

	public static final String ABC_SP_Loading_Path = ".//*[@id='cphContent_cphSection_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects']";

	// Error messages

	public static final String ABC_SP_Error_Title_Path = ".//div[contains(text(),'You must enter a value in the following fields:')]";
	public static final String ABC_SP_Error_Jobtitle_Path = ".//li[text()='Job Title Is Required']";
	public static final String ABC_SP_Error_Jobtype_Path = ".//li[text()='Job Type Is Required']";
	public static final String ABC_SP_Error_OrderNo_Path = ".//li[text()='Order # Is Required']";
	public static final String ABC_SP_Errorfield_Jobtitle_Path = ".//span[text()='Job Title Is Required']";
	public static final String ABC_SP_Errorfield_Jobtype_Path = ".//span[text()='Job Type Is Required']";
	public static final String ABC_SP_ErrorCreate__Path = ".//label[text()='Create the Special Project, before proceeding.']";

	public static final String ABC_SP_Errorfield__Path = ".//span[text()='Job Title is required']";

	// Methods

	public static String View_Edit_button(String Ticketno) {

		return ".//a[text()='" + Ticketno + "']//following::a[text()='View/Edit'][1]";

	}

}
