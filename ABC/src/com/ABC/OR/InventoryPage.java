package com.ABC.OR;

public class InventoryPage extends InventoryManagementPage {

	// inventory Site

	public static final String Inventory_Username_Path = ".//*[@id='Login']";
	public static final String Inventory_Passowrd_Path = ".//*[@id='Password']";
	public static final String Inventory_Submit_Btn_Path = ".//*[@id='Submit']";
	public static final String Inventory_Logout_Path = ".//a[text()='Logout']";

	public static final String Inventory_EditViewInventory_Path = ".//a[text()='Edit/View Inventory Items']";
	public static final String Inventory_EV_Customerdd_Path = ".//*[@id='CustomerID']";
	public static final String Inventory_EV_Projectdd_Path = ".//*[@id='ProjectID']";
	public static final String Inventory_EV_Barcode_Path = ".//*[@id='Bar_Code']";

	public String Inventory_EV_EditView_Path(String Productname) {

		return ".//td[text()='" + Productname + "']/following::a[1][text()='Edit/View']";
	}

	public static final String Inventory_EV_UnitsonHand_Path = ".//*[@id='UnitsOnHand']";
	public static final String Inventory_EV_UnitsAvailable_Path = ".//*[@id='UnitsAvailable']";
	public static final String Inventory_EV_BackOrderQty_Path = ".//*[@id='BackOrderQty']";

	public static final String Inventory_OrderSearch_Path = ".//a[text()='Edit/View Orders']";
	public static final String Inventory_Ordernumber_Path = ".//*[@id='OrderNumber']";
	public static final String Inventory_Search_Btn_Path = ".//*[@id='btnSearch']";
	public static final String Inventory_Edit_Btn_Path = ".//a[text()='Edit']";
	public static final String Inventory_QuickShip_Path = ".//a[text()='Quick Ship']";

	public static final String Inventory_Cancelorder_Btn_Path = ".//*[@id='CancelOrder']";
	public static final String Inventory_Order_Status = ".//*[@id='OrderStatus']";
	public static final String Inventory_logout_btn_Path = ".//a[text()='Logout']";

	public static final String Inventory_ShippingInst_btn_Path = ".//textarea[@name='ShippingInstructions']";
	public static final String Inventory_Shipper_btn_Path = ".//select[@name='Shipper']";
	public static final String Inventory_Trackingno_btn_Path = ".//input[@name='ShipperTrackingCode']";
	public static final String Inventory_Shippingcharges_btn_Path = ".//input[@name='ShippingCharges']";
	public static final String Inventory_ShipNote_btn_Path = ".//a[text()='[Ship Note]']";

	public static final String Inventory_CustomerID_Path = ".//*[@id='CustomerID']";
	public static final String Inventory_SearchResult_Ordernumber = ".//*[@id='DataList']/tbody/tr[2]/td[1]";
	public static final String Inventory_SearchResult_Status_Path = ".//*[@id='DataList']/tbody/tr[2]/td[6]";

	public static final String Inventory_EditPage_Title_Path = ".//*[@id='ltitle']";
	public static final String Inventory_Status_Path = ".//*[@id='OrderStatus']";
	public static final String Inventory_CancelMessage_Path = ".//*[@id='message']";

	// PackSlip Page
	public static final String Inventory_PackSlipno_Path = ".//*[@id='Table1']/tbody/tr[1]/td[3]/font";

	// Quick Ship Path
	public static final String Inventory_QS_PickTicketBarcode_Path = "//*[@id='txtBarcode']";

	public static final String Inventory_QS_Shipper_Path = ".//*[@id='Shipper']";
	public static final String Inventory_QS_TrackingNo_Path = ".//*[@id='ShipperTrackingCode']";
	public static final String Inventory_QS_ShippingCharges_Path = ".//*[@id='ShippingCharges']";
	public static final String Inventory_QS_Perrecipient_Path = ".//*[@id='ShippingChargesCost_0']";
	public static final String Inventory_QS_Weight_Path = ".//*[@id='ShippingWeight']";
	public static final String Inventory_QS_Noofboxed_Path = ".//*[@id='Boxes']";
	public static final String Inventory_QS_ShipOrder_Path = ".//input[@value='Ship Order']";

}
