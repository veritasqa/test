package com.ABC.OR;

public class SearchResultsPage extends SpecialProjectsPage {

	// Search Results - SR

	// Search Results - SR

	public static final String ABC_SR_SearchMaterialsBy_TxtBox_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword\"]";

	public static final String ABC_SR_Search_Btn_ID = "cphContent_cphRightSideHeader_btnAdvancedSearch";
	public static final String ABC_SR_Search_Btn_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphRightSideHeader_btnAdvancedSearch\"]";

	public static final String ABC_SR_NoSearchresults_msg_Path = "Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active.";

	public String SearchResultsPiceNameXpath(String Piecename) {
		return ".//span[@class='productListFormNumber'][contains(.,'" + Piecename + "')]";
	}

	public String SearchResultsAddtoCartBtnXpath(String Piecename) {
		return "//span[@class='productListFormNumber'][contains(.,'" + Piecename
				+ "')]/following::a[text()='Add to Cart']";
	}

	public String SearchResultsQty(String Piecename) {
		return "//span[@class='productListFormNumber'][contains(.,'" + Piecename + "')]/following::input[1]";
	}

	public String SearchResultsAddtoDripBtnXpath(String Piecename) {
		return "//span[@class='productListFormNumber'][contains(.,'" + Piecename
				+ "')]/following::a[text()='Add to Drip']";
	}

	public String AddToCart_Title_Xpath(String PieceTitle) {
		return "//span[@class='description'][contains(.,'" + PieceTitle + "')]/following::a[text()='Add to Cart'][1]";
	}

	public String AddToCart_Name_Xpath(String PieceTitle) {
		return "//span[@class='productListFormNumber'][contains(.,'" + PieceTitle
				+ "')]/following::a[text()='Add to Cart'][1]";
	}

	public String SuccessfullyaddtoCartXpath(String PieceTitle) {
		return "//span[@class='description'][contains(.,'" + PieceTitle
				+ "')]/following::div[text()='Successfully Added 1!']";
	}

	public static final String ABC_SR_PieceAvailablity_Status_Path = "//div[@id='divAvailability']";
	public static final String ABC_SR_PieceNotAvailable_Status_Message = "Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active.";
	public static final String ABC_SR_Materials_Path = ".//h1[contains(text(),'Materials')]";
	public static final String ABC_SR_Matchingitems_Materials_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage\"]/div";
	public static final String ABC_SR_SearchbyMaterials_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSearchResults_txtKeyword']";
	public static final String ABC_SR_SearchbyMaterials_Searchbtn_Path = ".//a[text()='Search' and contains(@id,'AdvancedSearch')]";
	public static final String ABC_SR_Clear_Path = ".//a[text()='Clear']";
	public static final String ABC_SR_Loading_Path = ".//*[@id='ctl00_ctl00_cphContent_cphMain_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphRightSideContent_rgdProductList']";
	public static final String ABC_SR_AdvancedFilter_Path = ".//a[text()='Advanced Filter']";
	public static final String ABC_SR_ExcludeRegulatoryck_Path = ".//span[text()='Exclude Regulatory']/following::input[1][@type='checkbox']";
	public static final String ABC_SR_ExcludeUserKitsck_Path = ".//span[text()='Exclude User Kits']/following::input[1][@type='checkbox']";
	public static final String ABC_SR_CheckOut_Btn_ID = "cphContent_cphRightSideContent_btnSearchResultsCheckOut";
	public static final String ABC_SR_CheckOut_Btn_Path = ".//a[text()='Checkout' and contains(@id,'SearchResultsCheckOut')]";

}
