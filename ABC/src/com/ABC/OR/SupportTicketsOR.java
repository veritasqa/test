package com.ABC.OR;

public class SupportTicketsOR extends SearchResultsPage {

	// Support Veritas Ticket

	public static final String ABC_ST_Veritasticketitle_Path = ".//h1[text()='Support : Veritas Tickets']";
	public static final String ABC_ST_TicketNotxt_Path = "//*[@id=\"ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_CollaborationID\"]";
	public static final String ABC_ST_OrderNotxt_Path = "//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_OrderID']";
	public static final String ABC_ST_OrderNofilter_Path = "//*[@id=\"ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_OrderID\"]";
	public static final String ABC_ST_SubCattxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_SubCategory']";
	public static final String ABC_ST_SubCatfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_SubCategory']";
	public static final String ABC_ST_Productcodetxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_InventoryID']";
	public static final String ABC_ST_Productcodefilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_InventoryID']";
	public static final String ABC_ST_Submittedbytxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ReportedByUserName']";
	public static final String ABC_ST_Submittedbyfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ReportedByUserName']";
	public static final String ABC_ST_Updatedontxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_dateInput']";
	public static final String ABC_ST_Updatedoncalender_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_popupButton']";
	public static final String ABC_ST_Updatedonfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ModDate']";
	public static final String ABC_ST_Teamtxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_VeritasTeam']";
	public static final String ABC_ST_Teamfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_VeritasTeam']";
	public static final String ABC_ST_Statustxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']";
	public static final String ABC_ST_Statusfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']";
	public static final String ABC_ST_VT_OrderNo1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[2]";
	public static final String ABC_ST_VT_TicketNo1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[1]";
	public static final String ABC_ST_VT_Edit1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[10]/a";
	public static final String ABC_ST_VT_SubCat1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[3]";
	public static final String ABC_ST_VT_Stockno1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[4]";
	public static final String ABC_ST_VT_Submittedby1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[6]";
	public static final String ABC_ST_VT_Updatedon1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[6]";
	public static final String ABC_ST_VT_Team1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]";
	public static final String ABC_ST_VT_Status1_Path = "//*[@id=\"ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0\"]/td[8]";
	public static final String ABC_ST_VT_Addticketicon_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ABC_ST_VT_Addticketlabel_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String ABC_ST_VT_Refreshicon_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String ABC_ST_VT_Refreshlabel_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String ABC_ST_VT_Firstpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String ABC_ST_VT_Prevpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String ABC_ST_VT_Nextpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String ABC_ST_VT_Lastpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String ABC_ST_VT_Pagenotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String ABC_ST_VT_Gobtn_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String ABC_ST_VT_Pagesizetxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String ABC_ST_VT_Changtbn_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String ABC_ST_VT_Pagesizeof_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String ABC_ST_VT_Itemto_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";

	public static final String ABC_ST_VT_Secondrow_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__1']";

	// Client Support Ticket

	public static final String ABC_ST_Clientticketitle_Path = ".//h1[text()='Support : Client Tickets']";
	public static final String ABC_ST_CT_TicketNotxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_RNTBF_CollaborationID']";
	public static final String ABC_ST_CT_OrderNotxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_OrderID']";
	public static final String ABC_ST_CT_OrderNofilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_OrderID']";
	public static final String ABC_ST_CT_SubCattxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_SubCategory']";
	public static final String ABC_ST_CT_SubCatfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_SubCategory']";
	public static final String ABC_ST_CT_ProductCodetxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_InventoryID']";
	public static final String ABC_ST_CT_ProductCodefilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_InventoryID']";
	public static final String ABC_ST_CT_Submittedbytxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ReportedByUserName']";
	public static final String ABC_ST_CT_Submittedbyfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ReportedByUserName']";
	public static final String ABC_ST_CT_Updatedontxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_dateInput']";
	public static final String ABC_ST_CT_Updatedoncalender_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_popupButton']";
	public static final String ABC_ST_CT_Updatedonfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ModDate']";
	public static final String ABC_ST_CT_Teamtxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ClientTeam']";
	public static final String ABC_ST_CT_Teamfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ClientTeam']";
	public static final String ABC_ST_CT_Statustxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']";
	public static final String ABC_ST_CT_Statusfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']";
	public static final String ABC_ST_CT_TicketNo1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[1]";
	public static final String ABC_ST_CT_OrderNo1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[2]";
	public static final String STCT_Edit1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[9]/a";
	public static final String ABC_ST_CT_SubCat1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[3]";
	public static final String ABC_ST_CT_Stockno1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[4]";
	public static final String ABC_ST_CT_Submittedon1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[5]";
	public static final String ABC_ST_CT_Updatedon1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[6]";
	public static final String ABC_ST_CT_Team1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[7]";
	public static final String ABC_ST_CT_Status1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[8]";
	public static final String ABC_ST_CT_Addticketicon_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ABC_ST_CT_Refreshicon_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String ABC_ST_CT_Firstpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String ABC_ST_CT_Prevpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String ABC_ST_CT_Nextpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String ABC_ST_CT_Lastpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String ABC_ST_CT_Pagenotxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String ABC_ST_CT_Gobtn_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String ABC_ST_CT_Pagesizetxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String ABC_ST_CT_Changtbn_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String ABC_ST_CT_Pagesizeof_Path = "//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_PageOfLabel']";

	public static final String ABC_ST_CT_Secondrow_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__1']";

	// Create Support Ticket Popup

	public static final String ABC_ST_CST_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div";
	public static final String ABC_ST_CST_Title_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]";
	public static final String ABC_ST_CST_Categorydd_Path = ".//select[contains(@id,'ddlCategory')]";
	public static final String ABC_ST_CST_SubCategorydd_Path = ".//select[contains(@id,'ddlSubCategory')]";
	public static final String ABC_ST_CST_Ordernofield_Path = ".//input[contains(@id,'txtOrderNumber')]";
	public static final String ABC_ST_CST_Commentfield_Path = ".//textarea[contains(@id,'txtComment')]";
	public static final String ABC_ST_CST_Createbtn_Path = ".//a[contains(.,'Create')]";
	public static final String ABC_ST_CST_Cancelbtn_Path = ".//*[@id='btnHideCreateSupportTicketPopup']";

	// Support Ticket Manage

	public static final String ABC_STM_URL = "TheStandard/admin/clientcollaboration/SupportTicket_Manage.aspx?Ticket=";
	public static final String ABC_STM_NewStatus_Path = ".//a[text()='New']";
	public static final String ABC_STM_Inprogress_Path = ".//a[text()='In-Progress']";
	public static final String ABC_STM_Hold_Path = ".//a[text()='Hold']";
	public static final String ABC_STM_Closedstatus_Path = ".//a[text()='Closed']";
	public static final String ABC_STM_CancelOrder_Path = ".//a[text()='Cancel Order']";
	public static final String ABC_STM_Category_Path = ".//*[@id='ddlCategory']";
	public static final String ABC_STM_Assignedto_Path = ".//*[@id='ddlAssignedTo']";
	public static final String ABC_STM_SubCat_Path = ".//*[@id='ddlSubCategory']";
	public static final String ABC_STM_Veritasteam_Path = ".//*[@id='ddlVeritasTeam']";
	public static final String ABC_STM_Ordername_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rcmbxOrderNumber_Input']";
	public static final String ABC_STM_Clientteam_Path = ".//*[@id='ddlClientTeam']";
	public static final String ABC_STM_User1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[1]";
	public static final String ABC_STM_Createdate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[2]";
	public static final String ABC_STM_Comment1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]";
	public static final String ABC_STM_Attachment1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[4]/a";
	public static final String ABC_STM_Userrefreshicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String ABC_STM_Userrefreshlabel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String ABC_STM_Ticketno_Path = ".//*[@id='ticketNumberSpan']";
	public static final String ABC_STM_Addcommenticon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ABC_STM_Addcommentlabel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String ABC_STM_Commenttext_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_rtxtComment']";
	public static final String ABC_STM_Attachment_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_fupCommentsAttachmentfile0']";
	public static final String ABC_STM_CommentCancel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_btnCancel']";
	public static final String ABC_STM_Createcomment_Path = ".//a[text()='Create Comment']";
	public static final String ABC_STM_Addcommentloadingpanel_Path = ".//*[@id='RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid']";
	public static final String ABC_STM_TicketSuccefullyUpdatedmsg_Path = ".//*[@id='TicketSuccefullyUpdated']";
	public static final String ABC_STM_UpdateTicketbtn_Path = ".//input[@value='Update Ticket']";
	public static final String ABC_STM_UpdateTicketmsg_Path = ".//li[contains(text(),'The Ticket was successfully updated')]";

	//
	public static final String ABC_ST_Remove_Path = ".//input[@value='Remove']";
	public static final String ABC_ST_Loading_Path = ".//*[@id='RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid']";

	// Support page- SP

	// td[text()='1765665']/following::a[text()='Edit'][1]

	public String EditButtonOrderNumber(String OrderNumber) {

		return "//a[text()='" + OrderNumber + "']/following::a[text()='View/Edit'][1]";
	}

	public static final String ABC_SP_AddTicket_ID = "ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_InitInsertButton";

	public static final String ABC_SP_Status_TextBox_ID = "ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status";
	public static final String ABC_SP_Status_Filter_ID = "ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status";

	public static final String ABC_SP_Closed_ID = "btnClosedStatus"; // class="statusBtnSelected"
	public static final String ABC_SP_TicketUpdateMsg_ID = "TicketSuccefullyUpdated";

	public static final String ABC_SP_AddComment_Path = "//a[text()=' Add Comment']";

	public static final String ABC_SP_Comment_TextBox_ID = "ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_rtxtComment";

	public static final String ABC_SP_CreateComment_Btn_ID = "ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_btnSave";

}
