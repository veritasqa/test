package com.SecurityBenefit.OR;

public class ShoppingCart extends SearchResultsPage {

	public static final String SB_SCP_CostCenter_Dropdown_ID = "cphContent_ddlCostCenter";
	public static final String SB_SCP_Remove_btn_Path = ".//a[text()='Remove']";
	public static final String SB_SCP_EditVariables_btn_Path = ".//a[text()='Edit Variables']";
	public static final String SB_SCP_Proof_btn_Path = ".//a[text()='Proof']";
	public static final String SB_SCP_Proofed_btn_Path = ".//a[text()='Proofed']";
	public static final String SB_SCP_Qty_Path = ".//input[contains(@id,'txtQuantity') and @type='text']";
	public static final String SB_SCP_Update_Path = ".//a[text()='Update']";
	

	public static final String SB_SCP_Proof1_btn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[5]/a[4]";
	public static final String SB_SCP_Proof2_btn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__1']/td[5]/a[4]";

	// Shopping Cart Page - SCP

	public static final String SB_SCP_OrderConfirmation_Checkbox_ID = "cphContent_cbxOrderConfirmation";
	public static final String SB_SCP_ShippedConfirmation_Checkbox_ID = "cphContent_cbxShippedConfirmation";

	public static final String SB_SCP_CreateOrderTemplate_Btn_Path = ".//a[text()='Create Order Template']";
	public static final String SB_SCP_ClearCart_Btn_ID = "cphContent_btnClearCart";
	public static final String SB_SCP_Next_Btn_ID = "cphContent_btnShowShipping";
	public static final String SB_SCP_SubmitOrder_Btn_Path = ".//a[contains(@id,'Checkout') and text()='Submit Order']";

	// Shipping Method

	public static final String SB_SCP_ShippingLowCostMethod_Checkbox_ID = "cphContent_rdoShippingLowCostMethod";
	public static final String SB_SCP_DeliveryDate_CheckBox_ID = "cphContent_rdoDeliveryDate";
	public static final String SB_SCP_DeliveryDate_popupButton_ID = "ctl00_cphContent_rdtepkrDeliveryDate_popupButton";
	public static final String SB_SCP_Calendar_Title_ID = "ctl00_cphContent_rdtepkrDeliveryDate_calendar_Title";
	public static final String SB_SCP_Calendar_OK_Btn_ID = "rcMView_OK";
	public static final String SB_SCP_DeliveryMethod_Path = ".//span[text()='UPS Ground']";

	public static final String SB_SCP_RecipientNotes_ID = "cphContent_txtShippingInstructions";

	public static final String SB_SCP_Checkout_ID = "cphContent_btnCheckout";

	// Create Order Template Pop up
	public static final String SB_SCP_COT_Path = ".//div[@class='Box']";
	public static final String SB_SCP_COT_Title_Path = ".//div[contains(text(),'Create Order Template')]";
	public static final String SB_SCP_COT_Name_Path = ".//input[contains(@id,'txtOrderTemplateName') and @type='text']";
	public static final String SB_SCP_COT_Description_Path = ".//textarea[contains(@id,'txtOrderTemplateDescription')]";
	public static final String SB_SCP_COT_Createbtn_Path = ".//a[text()='Create']";
	public static final String SB_SCP_COT_Cancelbtn_Path = ".//a[text()='Cancel']";

	public static final String SB_SCP_Loading_Path = "//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_divShoppingCart']";
}
