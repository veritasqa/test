package com.SecurityBenefit.OR;

public class OrderConfirmationPage extends ManageUsersPage {

	// Order Confirmation- OC

	public static final String SB_OC_OrderNumber_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblOrderNumber";

	public static final String SB_OC_Printfile1_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10_ctl04_lblProofName']";

	public static final String SB_OC_Printfile2_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10_ctl10_hlProof']";

	public static final String SB_OC_Status_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblStatus";
	public static final String SB_OC_HoldForComplete_ID = "cphContent_cphSection_lblHoldForComplete";
	public static final String SB_OC_OrderDate_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblCreateDate";
	public static final String SB_OC_CostCenter_ID = "cphContent_cphSection_lblBilledToCostCenter";
	public static final String SB_OC_ShippedBy_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblShippedBy";
	public static final String SB_OC_TotalChargebackCost = "cphContent_cphSection_lblTotalChargeBackCost";
	public static final String SB_OC_Ticketno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[3]";
	public static final String SB_OC_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[4]/span";

	public static final String SB_OC_Stockno_Path = "//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[1]";
	public static final String SB_OC_Description_Path = "//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[2]";
	public static final String SB_OC_Qty_Path = "//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[4]";

	public static final String SB_OC_Shipbydate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[5]";
	public static final String SB_OC_Deliverdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[6]";
	public static final String SB_OC_Cancelorder_Path = ".//a[text()='Cancel Order']";

	public static final String SB_OC_Copy_Btn_Path = ".//a[text()='Copy']";
	public static final String SB_OC_OK_Btn_ID = "cphContent_cphPageFooter_btnOk";
	public static final String SB_OC_Home_Btn_Path = ".//a[text()='Home/New Order']";
}
