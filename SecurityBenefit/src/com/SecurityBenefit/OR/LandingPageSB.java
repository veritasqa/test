package com.SecurityBenefit.OR;

public class LandingPageSB extends InventoryPage {

	public static final String SB_LP_Logout_Path = ".//a[text()='Logout']";
	public static final String SB_LP_ViewAllAnnouncements_path = ".//a[text()='View All Announcements']";
	public static final String SB_LP_StartaMailing_path = ".//a[text()='Start a Mailing']";
	public static final String SB_LP_StartaBulkOrder_path = ".//a[text()='Start a Bulk Order']";

	public static final String SB_LP_Home_path = ".//span[text()='Home']";
	public static final String SB_LP_Selectedcontact_ID = "lblUserName";
	public static final String LP_Admin_path = ".//span[text()='Admin']";
	public static final String LP_Specialproject_path = ".//span[text()='Special Projects']";
	public static final String LP_Materials_path = ".//span[text()='Materials']";
	public static final String LP_Changereq_path = ".//span[text()='Change Requests']";

	public static final String LP_Support_path = ".//span[text()='Support']";
	public static final String LP_ManageUsers_path = ".//span[text()='Manage Users']";
	public static final String LP_ManageRoles_path = ".//span[text()='Manage Roles']";
	public static final String LP_ManageAnnouncements_path = ".//span[text()='Manage Announcements']";
	public static final String LP_Reports_path = ".//span[text()='Reports']";
	public static final String LP_ManageCategory_path = ".//span[text()='Manage Categories']";
	public static final String LP_ManageInventory_Path = ".//span[text()='Manage Inventory']";

	public static final String SB_LP_FulfilmentSearch_TxtBox_ID = "ctl00_txtQuickSearch";
	public static final String SB_LP_FulfilmentSearch_Btn_ID = "ctl00_btnQuickSearch";
	public static final String SB_LP_ClearCart_Path = ".//span[text()='Clear Cart']";
	public static final String SB_LP_ClearCart_disabled_ID = "Navigation_aCancelOrder";

	// Widgets

	public static final String Add_Shortcut_Path = ".//h3[text()='Add Widgets']";

	// Order Template
	public static final String Ordertemplate_Path = ".//a[text()='Order Templates']";
	public static final String Ordertemplate_Form1_Path = ".//*[@id='divOrderTemplates']/tbody/tr[1]/td[1]/div";
	public static final String Ordertemplate_Form2_Path = ".//*[@id='divOrderTemplates']/tbody/tr[2]/td[1]/div";
	public static final String Ordertemplate_Form3_Path = ".//*[@id='divOrderTemplates']/tbody/tr[3]/td[1]/div";
	public static final String Ordertemplate_View1_Path = ".//*[@id='divOrderTemplates']/tbody/tr[1]/td[2]/a[1]";
	public static final String Ordertemplate_View2_Path = ".//*[@id='divOrderTemplates']/tbody/tr[2]/td[2]/a[1]";
	public static final String Ordertemplate_View3_Path = ".//*[@id='divOrderTemplates']/tbody/tr[3]/td[2]/a[1]";
	public static final String Ordertemplate_Addtocart1_Path = "_ctl00_repOrderTemplates_ctl01_btnAddToCart']";
	public static final String Ordertemplate_Addtocart2_Path = "_ctl00_repOrderTemplates_ctl03_btnAddToCart']";
	public static final String Ordertemplate_Addtocart3_Path = "_ctl00_repOrderTemplates_ctl05_btnAddToCart']";
	public static final String Ordertemplate_selectrecipient = ".//em[text()='Order Templates']//following::a[text()='select a recipient'][1]";

	// New Forms and Materials

	public static final String NFMWidget_Selectrecipient_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String NFMWidget_Form1_Path = "_ctl00_lnkFormNumber1']";
	public static final String NFMWidget_Form2_Path = "_ctl00_lnkFormNumber2']";
	public static final String NFMWidget_Form3_Path = "_ctl00_lnkFormNumber3']";
	public static final String NFMWidget_Form4_Path = "_ctl00_lnkFormNumber4']";
	public static final String NFMWidget_Form5_Path = "_ctl00_lnkFormNumber5']";
	public static final String NFMWidget_Qty1_Path = "_ctl00_txtQuantity1']";
	public static final String NFMWidget_Qty2_Path = "_ctl00_txtQuantity2']";
	public static final String NFMWidget_Qty3_Path = "_ctl00_txtQuantity3']";
	public static final String NFMWidget_Qty4_Path = "_ctl00_txtQuantity4']";
	public static final String NFMWidget_Qty5_Path = "_ctl00_txtQuantity5']";
	public static final String NFMWidget_Addtocart_Path = "_ctl00_btnAddItem']";
	public static final String NFMWidget_Checkout_Path = "_ctl00_btnAddToCart']";

	// Product Search
	public static final String SB_Productsearch_Stocknum_Textbox_Path = "//em[text()='Product Search']/following::input[1][@value='Stock # / Description / Keyword']";
	public static final String SB_Productsearch_ProductCatagoryDD_Path = "//em[text()='Product Search']/following::label[text()='Product Category']/following::a[1]";
	public static final String SB_Productsearch_LiteraturetypeDD_Path = "//em[text()='Product Search']/following::label[text()='Literature Type']/following::a[1]";
	public static final String SB_Productsearch_SearchBtn_Path = "//em[text()='Product Search']/following::label[text()='Literature Type']/following::input[@value='Search']";

	// OrderLookup -OL

	public static final String SB_LP_OL_FromDate_TxtBox_path = "//em[text()='Order Lookup']/following::input[contains(@id,'ctl00_rdpOrderSearchDateFrom_dateInput')][1]";

	public static final String SB_LP_OL_ToDate_TxtBox_path = "//em[text()='Order Lookup']/following::input[contains(@id,'ctl00_rdpOrderSearchDateTo_dateInput')][1]";

	public static final String SB_LP_OL_Search_Btn_path = "//em[text()='Order Lookup']/following::a[contains(@id,'C_ctl00_btnSearch')][1]";

	public static final String SB_LP_OL_OrderNumber_TxtBox_path = "//em[text()='Order Lookup']/following::input[contains(@id,'ctl00_rcbOrderNumber_Input')][1]";

	public static final String SB_LP_OL_Copy_Btn_path = "//em[text()='Order Lookup']/following::a[contains(@id,'C_ctl00_btnCopy')][1]";

	public static final String SB_LP_OL_View_Btn_path = "//em[text()='Order Lookup']/following::a[contains(@id,'C_ctl00_btnSubmit')][1]";

	public static final String SB_OL_SearchBtn_Path = "//a[@id='ctl00_cphContent_btnSearch']";

	// Express Checkout-EC

	public static final String SB_LP_EC_SelectaRecipient_Path = "//em[text()='Express Checkout']/following::a[text()='select a recipient'][1]";
	public static final String SB_LP_EC_ProductCode_Txtbox1_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber1_Input')]";
	public static final String SB_LP_EC_ProductCode_Txtbox2_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber2_Input')]";
	public static final String SB_LP_EC_ProductCode_Txtbox3_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber3_Input')]";
	public static final String SB_LP_EC_ProductCode_Txtbox4_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber4_Input')]";
	public static final String SB_LP_EC_ProductCode_Txtbox5_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber5_Input')]";

	public static final String SB_LP_EC_Qty_Txtbox1_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity1')]";
	public static final String SB_LP_EC_Qty_Txtbox2_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity2')]";
	public static final String SB_LP_EC_Qty_Txtbox3_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity3')]";
	public static final String SB_LP_EC_Qty_Txtbox4_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity4')]";
	public static final String SB_LP_EC_Qty_Txtbox5_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity5')]";

	public static final String SB_LP_EC_CheckoutBtn_path = "//em[text()='Express Checkout']/following::a[text()='Checkout']";

	// RecentOrders -RO

	public static String RecentorderButtonpath(String OrderNumber, String ButtonName) {
		return "//td[text()='" + OrderNumber + "']/following::a[text()='" + ButtonName + "'][1]";
	}

	public static String RecentorderStatus(String OrderNumber) {
		return "//td[text()='" + OrderNumber + "']/following::td[text()='NEW'][1]";
	}

	// Quick find order
	public String SelectCatagoryradiobtnPath(String Catagory) {
		return "//input[@title='" + Catagory + "']";
	}

	public String SelectProductradiobtnPath(String Product) {
		return "//input[@value='" + Product + "']";
	}
	public static final String LP_Loading_Path = "//*[@id=\"divMstLoadingPanel\"]/div";
}
