package com.SecurityBenefit.OR;

public class ContactSearchPage extends ChangeRequestPage {

	// CSP
	
	public static final String SB_CSP_Firstname_Txtbox_ID = "ctl00_cphContent_txtFirstName";
	public static final String SB_CSP_LastName_Txtbox_ID = "ctl00_cphContent_txtLastName";
	public static final String SB_CSP_Firm_Txtbox_ID = "ctl00_cphContent_txtFirm";
	public static final String SB_CSP_StateProvince_Txtbox_ID = "ctl00_cphContent_txtStateSearch";
	public static final String SB_CSP_Search_Btn_ID = "ctl00_cphContent_btnSearch";
	public static final String SB_CSP_Clear_Btn_Path = "//a[text()='Clear']";
	public static final String SB_CSP_AddToCart_Btn_ID = "ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_InitInsertButton";
	public static final String SB_CSP_Refresh_Btn_ID = "ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_RebindGridButton";

	// List of created Contacts

	// Contact 1 - QanoFirm IL
}
