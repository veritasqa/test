package com.SecurityBenefit.OR;


public class SpecialProjectsPage extends ShoppingCart{

	public static final String SB_Title_Path = ".//h1[text()='Special Projects']";
	public static final String SB_ViewticketTitle_Path = ".//h1[contains(text(),'Special Projects : View Tickets')]";
	public static final String SB_TicketNo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_TicketNumber']";
	//public static final String SB_TicketNofilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_TicketNumber']";
	public static final String SB_Jobtitle_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_JobTitle']";
	public static final String SB_JobtitleFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_JobTitle']";
	public static final String SB_Jobtype_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes_Input']";
	public static final String SB_Jobtypearrow_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes']/span/button";
	public static final String SB_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxStatus_Input']";
	public static final String SB_Statusarrow_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxStatus']/span/button";
	public static final String SB_Duedate_Path = ".//input[contains(@id,'FilterTextBox_DueDate')]";
	public static final String SB_DuedateFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_DueDate']";
	public static final String SB_Rushck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Rush']";
	public static final String SB_Rushckfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_Rush']";
	public static final String SB_Closedck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Closed']";
	public static final String SB_Closedckfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_Closed']";
	public static final String SB_TicketNo1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]/a";
	public static final String SB_Jobtitle1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]";
	public static final String SB_Jobtype1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[3]";
	public static final String SB_Status1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[4]";
	public static final String SB_Duedate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[5]";
	public static final String SB_Rushck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_ctl00']";
	public static final String SB_Closedck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_ctl01']";
	public static final String SB_ViewEdit1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[9]/a";
	public static final String SB_Addticketicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_Addticket_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String SB_Refreshicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_Refresh_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String SB_Firstpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String SB_Previouspagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String SB_Nextpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String SB_Lastpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String SB_Pagenotext_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String SB_Pageof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String SB_Gobtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String SB_Pagesize_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String SB_Changebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String SB_Itemof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";

	// General Tab

	public static final String SB_Generatab_Path = ".//a[text()='General']";
	public static final String SB_Gen_TicketNo_Path = ".//label[contains(text(),'Ticket #:')]//following::label[1][@id='spanTicketNumber']";
	public static final String SB_Gen_Status_Path = ".//label[contains(text(),'Status:')]//following::label[1]";
	public static final String SB_Gen_JobTitle_Path = ".//label[contains(text(),'Job Title')]//following::input[1][@type='text']";
	public static final String SB_Gen_JobTitleReq_Path = ".//label[contains(text(),'Job Title:')]/following::span[text()='*']";
	public static final String SB_Gen_JobType_Path = ".//label[contains(text(),'Job Type')]//following::select[contains(@name,'JobType')]";
	public static final String SB_Gen_JobTypeReq_Path = ".//label[contains(text(),'Job Type:')]/following::span[text()='*']";
	public static final String SB_Gen_Description_Path = ".//label[contains(text(),'Description')]//following::textarea[1]";
	public static final String SB_Gen_Createdby_Path = ".//*[@id='ctl00_cphContent_lblCreateUser']";
	public static final String SB_Gen_Costcenter_Path = ".//label[contains(text(),'Cost Center')]//following::input[1][@type='text']";
	public static final String SB_Gen_Splinst_Path = ".//label[contains(text(),'Special Instructions')]//following::textarea";
	public static final String SB_Gen_Isrush_Path = ".//label[contains(text(),'Is Rush')]//following::input[1][@type='checkbox']";
	public static final String SB_Gen_IsQuoteneeded_Path = ".//label[contains(text(),'Is Quote Needed')]//following::input[1][@type='checkbox']";
	public static final String SB_Gen_Cancel_Path = ".//a[text()='Cancel']";
	public static final String SB_Gen_Create_Path = ".//a[text()='Create']";
	public static final String SB_Gen_Save_Path = ".//.//a[text()='Save']";
	public static final String SB_Gen_Successmsg_Path = ".//span[text()='Data has been saved.']";
	public static final String SB_Gen_UnSuccessmsg_Path = ".//label[text()='Create the Special Project, before proceeding.']";

	// Program Tab

	public static final String SB_ProgramTab_Path = ".//a[text()='Progress / Files']";
	public static final String SB_Prog_Created_Path = ".//*[@id='ctl00_cphContent_lblCreateDate']";
	public static final String SB_Prog_Quoteaccepeted_Path = ".//*[@id='cphContent_lblQuoteAcceptedDate']";
	public static final String SB_Prog_Proofcreated_Path = ".//*[@id='cphContent_lblProofDate']";
	public static final String SB_Prog_Proofapproved_Path = ".//*[@id='ctl00_cphContent_lblProofApprovalDate']";
	public static final String SB_Prog_Duedate_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_dateInput']";
	public static final String SB_Prog_Duedatecal_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_popupButton']";
	public static final String SB_Prog_Duedate_monthcal_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_calendar_Title']";
	public static final String SB_Prog_Completed_Path = ".//*[@id='cphContent_lblCompleteDate']";
	public static final String SB_Prog_Cancelled_Path = ".//*[@id='cphContent_lblCancelDate']";
	public static final String SB_Prog_Holdplaced_Path = ".//*[@id='cphContent_lblHoldDate']";
	public static final String SB_Prog_Addcommenticon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_Prog_Addcomment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String SB_Prog_Refreshicon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_Prog_Refresh_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String SB_Prog_Comment_Path = ".//label[contains(text(),'Comment')]//following::textarea";
	public static final String SB_Prog_Type_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Input']";
	public static final String SB_Prog_Attachment_Path = ".//b[contains(text(),'Attachment:')]//following::input[1]";

	public static final String SB_Prog_Createdate1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[1]";
	public static final String SB_Prog_UserName1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[2]";
	public static final String SB_Prog_Comment1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[3]";
	public static final String SB_Prog_Type1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[4]";
	public static final String SB_Prog_Attachment1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[5]/a";
	public static final String SB_Prog_Submit_Path = ".//a[text()='Submit']";
	public static final String SB_Prog_Cancel_Path = ".//a[text()='Cancel' and contains(@id,'Comments')]";

	// Costs Tab

	public static final String SB_CostsTab_Path = ".//a[text()='Costs']";
	public static final String SB_Costs_Quote_Path = ".//td[contains(text(),'Quote Cost')]//following::input[1][@type='text']";
	public static final String SB_Costs_Printcost_Path = ".//td[contains(text(),'Print Cost')]//following::input[1][@type='text']";
	public static final String SB_Costs_VeritasPost_Path = ".//td[contains(text(),'Veritas Postage')]//following::input[1][@type='text']";
	public static final String SB_Costs_FFCost_Path = ".//td[contains(text(),'Fulfillment Cost')]//following::input[1][@type='text']";
	public static final String SB_Costs_ClientPost_Path = ".//td[contains(text(),'Client Postage')]//following::input[1][@type='text']";
	public static final String SB_Costs_Rushcost_Path = ".//td[contains(text(),'Rush Cost')]//following::input[1][@type='text']";
	public static final String SB_Costs_Billedck_Path = ".//td[contains(text(),'Billed')]//following::input[1][@type='checkbox']";
	public static final String SB_Costs_Addcostitemicon_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_Costs_Addcostitem_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String SB_Costs_Refreshicon_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_Costs_Refresh_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String SB_Costs_Description_Path = ".//b[contains(text(),'Description')]//following::textarea";
	public static final String SB_Costs_Type_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType']";
	public static final String SB_Costs_Billdate_Path = ".//b[contains(text(),'Bill Date')]//following::input[1][@type='text']";
	public static final String SB_Costs_Billdatecal_Path = ".//b[contains(text(),'Bill Date')]//following::a[1]";
	public static final String SB_Costs_Billammount_Path = ".//b[contains(text(),'Bill Amount')]//following::input[1][@type='text']";
	public static final String SB_Costs_BilldateCal_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rdpBillDate_popupButton']";
	public static final String SB_Costs_BilldateCal_Month_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rdpBillDate_calendar_Title']";
	public static final String SB_Costs_Description1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[1]";
	public static final String SB_Costs_Productcode1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[2]";
	public static final String SB_Costs_Type1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[3]";
	public static final String SB_Costs_Billdate1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[4]";
	public static final String SB_Costs_Billamount1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[5]";

	public static final String SB_Costs_Edit1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl04_EditButton']";
	public static final String SB_Costs_Delete1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[7]/a";

	public static final String SB_Costs_Insert_Path = ".//input[@value='Insert']";
	public static final String SB_Costs_Cancel_Path = ".//input[@value='Cancel' and contains(@id,'Cost')]";

	// Inventory Tab

	public static final String SB_InventoryTab_Path = ".//a[text()='Inventory']";
	public static final String SB_Inventory_OrderNo_Path = ".//*[@id='ctl00_cphContent_lblOrderNumber']";
	public static final String SB_Inventory_Jobtype_Path = ".//*[@id='ctl00_cphContent_txtJobType']";
	public static final String SB_Inventory_Product_Code_Path = ".//*[@id='ctl00_cphContent_racbFormNumbers_Input']";
	public static final String SB_Inventory_Product_suggestion_Path = ".//td[text()='QA_Multifunction ']";
	public static final String SB_Inventory_Addoprojbtn_Path = ".//span[text()='Add to Project']";
	public static final String SB_Inventory_Refreshicon_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00_ctl03_ctl01_RefreshButton']";
	
	public static final String SB_Inventory_Productcode1_Path=".//*[@id='ctl00_cphContent_Parts_ctl00__0']/td[2]";
	public static final String SB_Inventory_Qty1_Path=".//*[@id='ctl00_cphContent_Parts_ctl00_ctl04_txtQuantityWanted']";
	public static final String SB_Inventory_Delete1_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00__0']/td[8]/a";
	public static final String SB_Inventory_Refresh_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00_ctl03_ctl01_RebindGridButton']";

	// History Tab

	public static final String SB_HistoryTab_Path = ".//a[text()='History']";
	public static final String SB_History_DateTime_Path = ".//*[@id='ctl00_cphContent_History_ctl00']/thead/tr/th[1]/a";
	public static final String SB_History_User_Path = ".//*[@id='ctl00_cphContent_History_ctl00']/thead/tr/th[2]/a";
	public static final String SB_History_Details_Path = ".//*[@id='ctl00_cphContent_History_ctl00']/thead/tr/th[3]/a";
	public static final String SB_History_DateTime1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[1]";
	public static final String SB_History_User1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[2]";
	public static final String SB_History_Details1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";

	// Loading Path

	public static final String SB_Loading_Path = ".//*[@id='cphContent_cphSection_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects']";


	// Error messages

	public static final String SB_Error_Title_Path = ".//div[contains(text(),'You must enter a value in the following fields:')]";
	public static final String SB_Error_Jobtitle_Path = ".//li[text()='Job Title Is Required']";
	public static final String SB_Error_Jobtype_Path = ".//li[text()='Job Type Is Required']";
	public static final String SB_Error_OrderNo_Path = ".//li[text()='Order # Is Required']";
	public static final String SB_Errorfield_Jobtitle_Path = ".//span[text()='Job Title Is Required']";
	public static final String SB_Errorfield_Jobtype_Path = ".//span[text()='Job Type Is Required']";
	public static final String SB_ErrorCreate__Path = ".//label[text()='Create the Special Project, before proceeding.']";

	public static final String SB_Errorfield__Path = ".//span[text()='Job Title is required']";

	// Methods

	public static String View_Edit_button(String Ticketno) {

		return ".//a[text()='" + Ticketno + "']//following::a[text()='View/Edit'][1]";

	}
	
	


}
