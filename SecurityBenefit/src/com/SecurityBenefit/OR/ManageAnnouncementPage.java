package com.SecurityBenefit.OR;

public class ManageAnnouncementPage extends Loginpage  {

	public static final String SB_MA_Title_Path = ".//h1[contains(text(),'Manage Announcements')]";
	public static final String SB_MA_Announcement_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']";
	public static final String SB_MA_Announcementfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']";
	public static final String SB_MA_Startdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFStartDate_dateInput']";
	public static final String SB_MA_Startdatefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_StartDate']";
	public static final String SB_MA_StartdateCalicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFStartDate_popupButton']";
	public static final String SB_MA_Inactivedate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFInactiveDate_dateInput']";
	public static final String SB_MA_Inactivedatefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_InactiveDate']";
	public static final String SB_MA_InactivedateCalicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RDIPFInactiveDate_popupButton']";
	public static final String SB_MA_Sort_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_RNTBF_Sort']";
	public static final String SB_MA_Sortfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Sort']";

	public static final String SB_MA_OnlyActiveck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterCheckBox_IsActive']";
	public static final String SB_MA_OnlyActivefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_IsActive']";
	public static final String SB_MA_Announcement1_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]";
	public static final String SB_MA_Startdate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[3]";
	public static final String SB_MA_Inactivedate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[4]";
	public static final String SB_MA_Sort1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[5]";
	public static final String SB_MA_Onlyactiveck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl04_ctl00']";
	public static final String SB_MA_Edit1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl04_EditButton']";
	public static final String SB_MA_Activate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a";
	public static final String SB_MA_Addannouncementbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_MA_Refreshbn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_MA_Firstpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String SB_MA_Previouspagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String SB_MA_Nextpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String SB_MA_Lastpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String SB_MA_Pagenotext_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String SB_MA_Pageof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String SB_MA_Pagegobtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String SB_MA_Pagesize_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String SB_MA_Pagechangebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String SB_MA_Itemof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";
	public static final String SB_MA_Row1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']";
	public static final String SB_MA_Row2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__1']";


	// Add Announcements

	public static final String SB_MA_Announcement_iFrame_ID = ".//iframe[contains(@id,'Iframe')]";
	public static final String SB_MA_Boldbtn_Path = ".//span[@class='Bold']";
	public static final String SB_MA_Underlinebtn_Path = ".//span[@class='Underline']";
	public static final String SB_MA_Italicbtn_Path = ".//span[@class='Italic']";
	public static final String SB_MA_Hyperlinkbtn_Path = ".//span[@class='LinkManager']";
	public static final String SB_MA_HyperlinkURL_Path = "//*[@id='LinkURL']";
	public static final String SB_MA_HyperlinkTarget_Path = "//*[@id='SkinnedLinkTargetCombo']";
	public static final String SB_MA_HyperlinkTooltip_Path = "//*[@id='LinkTooltip']";
	public static final String SB_MA_AddAnnouncement_Path = "/html/body";
	public static final String SB_MA_AddStartdate_Path = ".//label[text()='Start Date:']//following::input[1][@type='text']";
	public static final String SB_MA_AddStartdateCal_Path = ".//label[text()='Start Date:']//following::a[1]";
	public static final String SB_MA_AddStartdateCalmonth_Path = ".//td[contains(@id,'rdtpStartDate_calendar_Title')]";
	public static final String SB_MA_AddStartdateTime_Path = ".//a[contains(@id,'rdtpStartDate_timePopupLink')]";
	public static final String SB_MA_Addinactivedate_Path = ".//label[text()='Inactive Date:']//following::input[1][@type='text']";
	public static final String SB_MA_AddinactivedateCal_Path = ".//label[text()='Inactive Date:']//following::a[1]";
	public static final String SB_MA_AddinactivedateCalmonth_Path = ".//td[contains(@id,'rdtpInactiveDate_calendar_Title')]";
	public static final String SB_MA_AddinactivedateTime_Path = ".//a[contains(@id,'rdtpInactiveDate_timePopupLink')]";
	public static final String SB_MA_AddEMODUsersck_Path = ".//label[text()='User Group:']//following::input[1][@type='checkbox']";
	public static final String SB_MA_AddEnrollmentservices_Path = ".//label[text()='User Group:']//following::input[2][@type='checkbox']";
	public static final String SB_MA_AddSort_Path = ".//label[text()='Sort:']//following::input[1][@type='text']";
	public static final String SB_MA_Createannouncementbtn_Path = ".//a[text()='Create Announcement']";
	public static final String SB_MA_Updateammouncementbtn_Path = ".//a[text()='Update Announcement']";
	public static final String SB_MA_Cancelbtn_Path = ".//a[text()='Cancel']";

	public static String SB_MA_Createannouncement_Path(String Announcement) {

		return ".//em[text()='Announcements']//following::span[contains(text(),'" + Announcement + "')]";

	}

	public static String SB_MA_Editannouncement_Path(String Announcement) {

		return ".//em[text()='Announcements']//following::a[contains(text(),'" + Announcement + "')]";

	}

	public String UserGroup_Checkbox(String Checkboxoption) {

		return ".//label[text()='User Group:']/following::span[text()='" + Checkboxoption
				+ "']/preceding::input[1][@type='checkbox']";
	}

}
