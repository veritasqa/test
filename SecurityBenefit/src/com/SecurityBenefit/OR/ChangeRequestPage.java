package com.SecurityBenefit.OR;

import com.SecurityBenefit.Values.ValueRepositorySecurityBenefit;

public class ChangeRequestPage extends ValueRepositorySecurityBenefit  {

	// Support Veritas Ticket

	public static final String SB_Veritasticketitle_Path = ".//h1[text()='Change Request : Veritas Tickets']";
	public static final String SB_TicketNotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_CollaborationID']";
	public static final String SB_OrderNotxt_Path = "//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_OrderID']";
	public static final String SB_OrderNofilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_OrderID']";
	public static final String SB_SubCattxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_SubCategory']";
	public static final String SB_SubCatfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_SubCategory']";
	public static final String SB_Stocknotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_InventoryID']";
	public static final String SB_Stocknofilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_InventoryID']";
	public static final String SB_Submittedbytxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ReportedByUserName']";
	public static final String SB_Submittedbyfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ReportedByUserName']";
	public static final String SB_Updatedontxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_dateInput']";
	public static final String SB_Updatedoncalender_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_popupButton']";
	public static final String SB_Updatedonfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ModDate']";
	public static final String SB_Teamtxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ClientTeam']";
	public static final String SB_Teamfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ClientTeam']";
	public static final String SB_Statustxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']";
	public static final String SB_Statusfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']";
	public static final String SB_VT_OrderNo1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[2]";
	public static final String SB_VT_TicketNo1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[1]";
	public static final String SB_VT_Edit1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[9]/a";
	public static final String SB_VT_SubCat1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[3]";
	public static final String SB_VT_Stockno1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[4]";
	public static final String SB_VT_Submittedby1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]";
	public static final String SB_VT_Updatedon1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[6]";
	public static final String SB_VT_Team1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[7]";
	public static final String SB_VT_Status1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]";
	public static final String SB_VT_Addticketicon_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_VT_Addticketlabel_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String SB_VT_Refreshicon_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_VT_Refreshlabel_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String SB_VT_Firstpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String SB_VT_Prevpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String SB_VT_Nextpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String SB_VT_Lastpage_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String SB_VT_Pagenotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String SB_VT_Gobtn_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String SB_VT_Pagesizetxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String SB_VT_Changtbn_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String SB_VT_Pagesizeof_Path = "//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl02_PageOfLabel']";

	public static final String SB_VT_Secondrow_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__1']";

	// Client Support Ticket

	public static final String SB_Clientticketitle_Path = ".//h1[text()='Change Request : Client Tickets']";
	public static final String SB_CT_TicketNotxt_Path = "//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_CollaborationID']";
	public static final String SB_CT_OrderNotxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_OrderID']";
	public static final String SB_CT_OrderNofilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_OrderID']";
	public static final String SB_CT_SubCattxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_SubCategory']";
	public static final String SB_CT_SubCatfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_SubCategory']";
	public static final String SB_CT_Stocknotxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_InventoryID']";
	public static final String SB_CT_Stocknotxtfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_InventoryID']";
	public static final String SB_CT_Submittedbytxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ReportedByUserName']";
	public static final String SB_CT_Submittedbyfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ReportedByUserName']";
	public static final String SB_CT_Updatedontxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_dateInput']";
	public static final String SB_CT_Updatedoncalender_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_popupButton']";
	public static final String SB_CT_Updatedonfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ModDate']";
	public static final String SB_CT_Teamtxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ClientTeam']";
	public static final String SB_CT_Teamfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ClientTeam']";
	public static final String SB_CT_Statustxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']";
	public static final String SB_CT_Statusfilter_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']";
	public static final String SB_CT_TicketNo1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[1]";
	public static final String SB_CT_OrderNo1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[2]";
	public static final String STCT_Edit1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[9]/a";
	public static final String SB_CT_SubCat1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[3]";
	public static final String SB_CT_Stockno1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[4]";
	public static final String SB_CT_Submittedon1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[5]";
	public static final String SB_CT_Updatedon1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[6]";
	public static final String SB_CT_Team1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[7]";
	public static final String SB_CT_Status1_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[8]";
	public static final String SB_CT_Addticketicon_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_CT_Refreshicon_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_CT_Firstpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String SB_CT_Prevpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String SB_CT_Nextpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String SB_CT_Lastpage_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String SB_CT_Pagenotxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String SB_CT_Gobtn_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String SB_CT_Pagesizetxt_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String SB_CT_Changtbn_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String SB_CT_Pagesizeof_Path = "//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl02_PageOfLabel']";

	public static final String SB_CT_Secondrow_Path = ".//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__1']";

	// Create Support Ticket Popup

	public static final String SB_CST_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div";
	public static final String SB_CST_Title_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]";
	public static final String SB_CST_Categorydd_Path = "//*[@id='ctl00_PageModal_ddlCategory']";
	public static final String SB_CST_SubCategorydd_Path = "//*[@id='ddlSubCategory']";
	public static final String SB_CST_Ordernofield_Path = ".//*[@id='txtOrderNumber']";
	public static final String SB_CST_Commentfield_Path = "//*[@id='ctl00_PageModal_txtComment']";
	public static final String SB_CST_Createbtn_Path = ".//a[contains(.,'Create')]";
	public static final String SB_CST_Cancelbtn_Path = ".//*[@id='ctl00_PageModal_btnHideCreateSupportTicketPopup']";

	// Support Ticket Manage
	
	public static final String SB_STM_URL = "TheStandard/admin/clientcollaboration/SupportTicket_Manage.aspx?Ticket=";
	public static final String SB_STM_NewStatus_Path = ".//a[text()='New']";
	public static final String SB_STM_Inprogress_Path = ".//a[text()='In-Progress']";
	public static final String SB_STM_Hold_Path = ".//a[text()='Hold']";
	public static final String SB_STM_Closedstatus_Path = ".//a[text()='Closed']";
	public static final String SB_STM_CancelOrder_Path = ".//a[text()='Cancel Order']";
	public static final String SB_STM_Category_Path = ".//*[@id='ddlCategory']";
	public static final String SB_STM_Assignedto_Path = ".//*[@id='ddlAssignedTo']";
	public static final String SB_STM_SubCat_Path = ".//*[@id='ddlSubCategory']";
	public static final String SB_STM_Veritasteam_Path = ".//*[@id='ddlVeritasTeam']";
	public static final String SB_STM_Ordername_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rcmbxOrderNumber_Input']";
	public static final String SB_STM_Clientteam_Path = ".//*[@id='ddlClientTeam']";
	public static final String SB_STM_User1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[1]";
	public static final String SB_STM_Createdate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[2]";
	public static final String SB_STM_Comment1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]";
	public static final String SB_STM_Attachment1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[4]/a";
	public static final String SB_STM_Userrefreshicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SB_STM_Userrefreshlabel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String SB_STM_Ticketno_Path = ".//*[@id='ticketNumberSpan']";
	public static final String SB_STM_Addcommenticon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SB_STM_Addcommentlabel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String SB_STM_Commenttext_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_rtxtComment']";
	public static final String SB_STM_Attachment_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_fupCommentsAttachmentfile0']";
	public static final String SB_STM_CommentCancel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_btnCancel']";
	public static final String SB_STM_Createcomment_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_btnSave']";
	public static final String SB_STM_Addcommentloadingpanel_Path = ".//*[@id='RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid']";
	public static final String SB_STM_TicketSuccefullyUpdatedmsg_Path = ".//*[@id='TicketSuccefullyUpdated']";
	public static final String SB_STM_UpdateTicketbtn_Path = ".//input[@value='Update Ticket']";
	public static final String SB_STM_UpdateTicketmsg_Path = ".//p[contains(text(),'The Ticket was successfully updated')]";
	
	
	// 
	public static final String SB_Remove_Path = ".//input[@value='Remove']";
	public static final String SB_Loading_Path = ".//*[@id='RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid']";
	
	
	
}
