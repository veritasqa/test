package com.SecurityBenefit.Values;

import java.util.ArrayList;
import java.util.List;

import org.testng.asserts.SoftAssert;

public class ValueRepositorySecurityBenefit {

	public static final String Xpath = "xpath";
	public static final String ID = "id";
	public static final String Tag = "tag";
	public static final String Name = "name";
	public static final String CSSselector = "cssSelector";
	public static final String LinkText = "linkText";
	public static final String PartialLinkText = "partialLinkText";
	public static final String Classname = "classname";
	public static String SplprjTicketNumber = "";
	public static String SplprjTicketNumber2 = "";
	public static List<String> SplprjTicketNumberlist = new ArrayList<String>();
	public static List<String> OrderNumberlist = new ArrayList<String>();
	public static String CreatePart = "";
	public static List<String> Createdpartlist = new ArrayList<String>();

	public static String Browser = "chrome";
	public static SoftAssert softAssert;

	public static String Browserpath = System.getProperty("user.dir")
			+ "\\src\\com\\SecurityBenefit\\Driver\\chromedriver.exe";
	// URLs
	public static final String URL = "https://www.veritas-solutions.net/SecurityBenefit/login.aspx";
	public static final String Production_URL = "https://order.SB.com/login.aspx";

	public static final String Inventory_URL = "https://www.veritas-solutions.net/Inventory/login.aspx";

	public static final String Loading_Path = ".//*[contains(@id,'Load')]";

	// Pre prod: https://staging.veritas-solutions.net/SBpreprod/login.aspx
	// Stage: https://stageorder.SB.com/login.aspx
	// Prod: https://order.SB.com/login.aspx

	// Inven- Prod: https://www.veritas-solutions.net/Inventory/login.aspx
	// Inven- Stage: https://staging.veritas-solutions.net/Inventory/login.aspx

	// Credentials

	public static final String SBUsername = "qaauto";
	public static final String SBPassword = "qaauto";

	public static final String SBUsername1 = "qaauto1";
	public static final String SBPassword1 = "qaauto1";

	public static final String SBInactiveusername = "qainactive";
	public static final String SBInactivepassword = "qainactive";

	public static final String SBBasicUsername = "qaautobasic";
	public static final String SBBasicPassword = "qaautobasic";

	// User details

	public static final String SBUsername_Fname = "qaauto";
	public static final String SBPassword_Lname = "automation";

	public static final String SBUsername1_Fname1 = "qaauto1";
	public static final String SBPassword1_Lname1 = "automation";

	public static final String UserFName = "QAauto";
	public static final String UserLName = "Automation";

	public static final String Both_Fname1 = "qaauto";
	public static final String Both_Lname1 = "automation";

	public static final String Inventory_UserName = "yannamalai";
	public static final String Inventory_Password = "yannamalai590";

	// Shipping Details

	public static final String Email = "ver.qaauto@rrd.com";
	public static final String Address = "913 Commerce Ct";
	public static final String City = "Buffalo Grove";
	public static final String State = "Illinois";
	public static final String Zip = "60089";

	public static String OrderNumber = "5750331";
	public static String Supporttickenumber = "";
	public static String Clienttickenumber = "";
	public static List<String> ordernum = new ArrayList<String>();
	public static List<String> Supporttickenumberlist = new ArrayList<String>();
	public static List<String> Clienttickenumberlist = new ArrayList<String>();
	public static String[] OrderNumberArray;

	// Parts

	public static final String QA_Multifunction = "QA_Multifunction";
	public static final String QA_NewTestPart_X = "QA_NewTestPart_X";
	public static final String QA_TESTNEWITEM_1 = "QA_TESTNEWITEM_1";
	public static final String QA_NewItemAddTest_1 = "QA_NewItemAddTest_1";
	public static final String QA_TESTNEWITEM_X = "QA_TESTNEWITEM_X";
	public static final String QA_Copy_SearchResults_1 = "QA_Copy_SearchResults_1";
	public static final String QA_Copy_SearchResults_X = "QA_Copy_SearchResults_X";
	public static final String QA_COPY_NEWITEM_1 = "QA_COPY_NEWITEM_1";
	public static final String qa_kitonfly_4 = "qa_kitonfly_4";
	public static final String QA_Replace1 = "QA_Replace1";
	public static final String QA_REPLACE2 = "QA_REPLACE2";
	public static final String QA_Adminonly = "QA_Adminonly";
	public static final String QA_Flyer = "QA_Flyer";
	// public static final String QA_TESTNEW_1 = "QA_TESTNEW_1";
	public static String QA_TESTNEWITEM_ = "QA_TESTNEWITEM_";
	public static final String QA_Viewability = "QA_Viewability";
	public static final String QA_Obsolete = "QA_Obsolete";
	public static final String QA_FirmRestriction = "QA_FIRMRESTRICTION";

}
