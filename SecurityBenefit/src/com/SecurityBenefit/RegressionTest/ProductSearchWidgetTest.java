package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ProductSearchWidgetTest extends BaseTestSecurityBenefit {

	// Search by Stock#/Description/Keyword

	@Test(enabled = false)
	public void SB_TC_2_6_4_1_1() throws InterruptedException {
		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, QA_Multifunction);
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Multifunction")),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Click(Xpath, SB_SR_Clear_Path);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, "QA_MULTIFUNCTION_KEYWORDS");
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Multifunction")),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Click(Xpath, SB_SR_Clear_Path);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, "QA_MULTIFUNCTION_DESCRIPTION");
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Multifunction")),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Click(Xpath, SB_SR_Clear_Path);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Clearcarts();

	}

	// Search by Product Category
	@Test(enabled = false)
	public void SB_TC_2_6_4_1_2() throws InterruptedException {

		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, QA_NewTestPart_X);

		Select_lidropdown(SB_Productsearch_ProductCatagoryDD_Path, Xpath, "AdvanceDesigns");
		Wait_ajax();
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		ContactClick("qaauto", "automation");
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath(QA_NewTestPart_X)),
				" 'QA_NewTestPart_X' is not displays on 'Product Search' page");
		Click(Xpath, SB_SR_Clear_Path);
		Clearcarts();
	}

	// Search by Literature Type
	@Test(enabled = false)
	public void SB_TC_2_6_4_1_3() throws InterruptedException {

		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, QA_Multifunction);

		Select_lidropdown(SB_Productsearch_LiteraturetypeDD_Path, Xpath, "Ad");
		Wait_ajax();
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Multifunction")),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Click(Xpath, SB_SR_Clear_Path);
		Clearcarts();
	}

	// Search by Product catgory and Literature Type
	@Test
	public void SB_TC_2_6_4_1_4() throws InterruptedException {

		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, QA_Multifunction);

		Select_lidropdown(SB_Productsearch_ProductCatagoryDD_Path, Xpath, "AdvanceDesigns");
		Wait_ajax();
		Select_lidropdown(SB_Productsearch_LiteraturetypeDD_Path, Xpath, "Ad");
		Wait_ajax();
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Multifunction")),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Click(Xpath, SB_SR_Clear_Path);
		Clearcarts();
	}

	// Verify adding part to cart from product search and clearing cart
	@Test
	public void SB_TC_2_6_4_2_1() throws InterruptedException {

		Type(Xpath, SB_Productsearch_Stocknum_Textbox_Path, QA_Multifunction);
		Click(Xpath, SB_Productsearch_SearchBtn_Path);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath("QA_Multifunction"));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");

		Clearcarts();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
