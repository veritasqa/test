package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ValidateFullfilmentSearchTest extends BaseTestSecurityBenefit{
	
	//Validate that parts can be searched on by using the fulfillment search field
	@Test(priority = 1, enabled = true)
	public void SB_TC_2_7_1_1_1() throws InterruptedException {


		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Multifunction")),
				" 'QA_MULTIFUNCTION' is not displays on 'Product Search' page");
		Clearcarts();
	}
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
