package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class OrderLookupTest extends BaseTestSecurityBenefit{
	//Search by date selection
	@Test(priority = 2)
	public void  SB_TC_2_6_3_1_1() throws InterruptedException{
		
		Type(Xpath, SB_LP_OL_FromDate_TxtBox_path, Get_Pastdate("m/d/YYYY"));
		
		Type(Xpath, SB_LP_OL_FromDate_TxtBox_path, Get_Todaydate("m/d/YYYY"));
		
		Click(Xpath, SB_LP_OL_Search_Btn_path);
		
		Wait_ajax();
		
		ExplicitWait_Element_Clickable(Xpath, SB_OL_SearchBtn_Path);
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("td", OrderNumber)),"the (Order #) from Pre-condition is displayed in 'Order Search' grid");
		
	    Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");

	}
	
	
	//View by order number
	@Test(priority = 3)
	public void  SB_TC_2_6_3_1_3() throws InterruptedException{
		
	Type(Xpath, SB_LP_OL_OrderNumber_TxtBox_path, OrderNumber);
		
		Wait_ajax();
		
		ExplicitWait_Element_Visible(Xpath,Textpath("li", OrderNumber));

		Click(Xpath,Textpath("li", OrderNumber));
		
		
		Click(Xpath, SB_LP_OL_View_Btn_path);
		
		
		ExplicitWait_Element_Clickable(ID, SB_OC_OK_Btn_ID);
		    
		Assert.assertTrue(Get_Text(ID, SB_OC_OrderNumber_ID).equalsIgnoreCase(OrderNumber),"OrderNumber Mismatch");
		
		
	}
	
	
	//Copy by order number and able to place an order
	@Test(priority = 4)
	public void  SB_TC_2_5_3_1_2() throws InterruptedException{
		
		Type(Xpath, SB_LP_OL_OrderNumber_TxtBox_path, OrderNumber);
		
		Wait_ajax();
		
		ExplicitWait_Element_Visible(Xpath,Textpath("li", OrderNumber));

		Click(Xpath,Textpath("li", OrderNumber));
		
		Click(Xpath, SB_LP_OL_Copy_Btn_path);
		
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Get_Orderno();
	}
	
	
	@Test(priority = 1)
	public void OrderLookupPrecondition() throws InterruptedException {
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath("QA_Multifunction"));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Get_Orderno();
	}
	
	
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}


}
