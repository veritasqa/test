package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class FulfillmentPartview extends BaseTestSecurityBenefit {

	// Validate that admin only part is viewable for an admin user
	@Test(priority = 1, enabled = true)
	public void SB_TC_2_7_5_1() throws InterruptedException {

		FulfilmentSearch(QA_Adminonly);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Adminonly")),
				" 'QA_Adminonly' is not displays on 'Product Search' page");
		Clearcarts();
	}

	/* Viewability Option list
	1.  Viewable Not Orderable
	2.	Admin Only
	3.	Not Viewable
	4.	Orderable
		
	*/
	public void ChangeViewability(String ViewabilityOption) throws InterruptedException, IOException {
		NavigateMenu(LP_Admin_path, LP_ManageInventory_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Searchbtn_Path);
		Type(Xpath, SB_MI_SI_StockNo_Path, QA_Viewability);
		Click(Xpath, SB_MI_Searchbtn_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_Edit1_Path);
		Wait_ajax();
		Switch_New_Tab();

		Select_lidropdown(SB_MI_Gen_Viewability_Arrow_Path, Xpath, ViewabilityOption);
		Click(Xpath, SB_MI_Gen_Save_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_Savepopup_Ok_Path);
		Switch_Old_Tab();
		logout();

	}

	// Validate when a part is set to viewable and not orderable that it's
	// viewable
	@Test(priority = 2, enabled = true)
	public void SB_TC_2_7_5_2() throws InterruptedException, IOException {
		login(SBUsername, SBPassword);
		ChangeViewability("Viewable Not Orderable");
		login(SBUsername, SBPassword);
		FulfilmentSearch(QA_Viewability);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, "//div[contains(text(),'Not Orderable')]"),
				"Not orderable message is missing");
	}

	// Validate when a part is set to not viewable that is not viewable
	@Test(priority = 2, enabled = true)
	public void SB_TC_2_7_5_3() throws IOException, InterruptedException {
		login(SBUsername, SBPassword);
		ChangeViewability("Not Viewable");
		login(SBUsername, SBPassword);
		FulfilmentSearch(QA_Viewability);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", SB_SR_NoSearchresults_msg_Path)),
				SB_SR_NoSearchresults_msg_Path + " --Message not displayed");
	}

	// Validate when the part is set to orderable that it is viewable and
	// orderable

	@Test(priority = 2, enabled = true)
	public void SB_TC_2_7_5_4() throws IOException, InterruptedException {
		login(SBUsername, SBPassword);
		ChangeViewability("Orderable");
		login(SBUsername, SBPassword);
		FulfilmentSearch(QA_Viewability);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCart_Name_Xpath("QA_Viewability")),
				" 'QA_Viewability' is not displays on 'Product Search' page");
		Clearcarts();
	}

	// Validate that an admin only part is not viewable for a basic user
	@Test(priority = 2, enabled = true)
	public void SB_TC_2_7_5_5() throws IOException, InterruptedException {
		login(SBBasicUsername, SBBasicPassword);
		FulfilmentSearch(QA_Adminonly);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", SB_SR_NoSearchresults_msg_Path)),
				SB_SR_NoSearchresults_msg_Path + " --Message not displayed");

		Clearcarts();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
