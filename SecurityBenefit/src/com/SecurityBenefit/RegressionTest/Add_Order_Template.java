package com.SecurityBenefit.RegressionTest;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;

public class Add_Order_Template extends BaseTestSecurityBenefit {

	@Test(priority = 1, enabled = true)
	public void SB_TC_2_6_7_2_1() throws InterruptedException {

		// Add to Cart
		// -Create order

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath(QA_Multifunction));
		SearchbyMaterials(QA_FirmRestriction);
		Click(Xpath, AddToCart_Name_Xpath(QA_FirmRestriction));
		Click(Xpath, SB_SR_CheckOut_Btn_Path);
		Wait_ajax();
		Click(Xpath, SB_SCP_CreateOrderTemplate_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Type(Xpath, SB_SCP_COT_Name_Path, "QA Test");
		Type(Xpath, SB_SCP_COT_Description_Path, "QA Order Template");
		Click(Xpath, SB_SCP_COT_Createbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "QA Test order template has been successfully created.")),
				"QA Test order template has been successfully created. message is not displayed");
		Click(Xpath, SB_LP_Home_path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SB_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the order Template widgets");
		Click(Xpath, Wid_Xpath("Order Templates", Ordertemplate_Addtocart1_Path));
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "SUBMITTED")), " SUBMITTED is not displayed");
		Get_Orderno();

	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "SB_TC_2_6_7_2_1")
	public void SB_TC_2_6_7_2_2() throws InterruptedException {

		Click(Xpath, Ordertemplate_selectrecipient);
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_LP_ViewAllAnnouncements_path), "Home page is not displayed");
		Click(Xpath, Ordertemplate_View1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "Order Templates")),
				"Order Template page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + "  is not displayed Orde Template Page");
		Click(Xpath, Containspath("input", "value", "Delete"));
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("a", "QA Test")),
				"QA Test is displayed after delete");
		Homepage();
		Assert.assertFalse(Element_Is_Displayed(Xpath, Ordertemplate_Form1_Path),
				"QA Test is displayed after delete action in Order template widget");
		Reporter.log("Order Template has been deleted");
		Clearcarts();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Ordertemplate_selectrecipient),
				"Select Recipient link is not displayed");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		Clearcarts();
		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
		Click(Xpath, Ordertemplate_selectrecipient);
		ContactClick("qaauto", "automation");
		Wait_ajax();
		if (Element_Is_Displayed(Xpath, Ordertemplate_View1_Path)) {
			Click(Xpath, Ordertemplate_View1_Path);
			while (Element_Is_Displayed(Xpath, Containspath("input", "value", "Delete"))) {

				Click(Xpath, Containspath("input", "value", "Delete"));
			}
		}
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

}
