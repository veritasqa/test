package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class Login extends BaseTestSecurityBenefit {

	@Test(priority = 1, enabled = true)
	public void SB_TC_1_0_1_1() {

		// Validate login screen fields

		softAssert.assertTrue(Element_Is_Displayed(ID, SB_Login_UserName_ID), "Username field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, SB_Login_Password_ID), "Username field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, SB_Login_LoginBtn_ID), "Login button is not displayed");
		softAssert.assertAll();
	}

	@Test(priority = 2, enabled = true)
	public void SB_TC_1_0_1_2() {

		// Verify Active user login

		Type(ID, SB_Login_UserName_ID, SBUsername);
		Type(ID, SB_Login_Password_ID, SBPassword);
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_LP_ViewAllAnnouncements_path), "Not logged in");
		Click(Xpath, SB_LP_Logout_Path);

	}

	@Test(priority = 3, enabled = true)
	public void SB_TC_1_0_1_3() {

		// verify Inactive user login

		Type(ID, SB_Login_UserName_ID, SBInactiveusername);
		Type(ID, SB_Login_Password_ID, SBInactivepassword);
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("li", "Invalid user name or password, please try again.")),
				"Invalid user name or password, please try again. is not displayed");

	}

	@Test(priority = 4, enabled = true)
	public void SB_TC_1_0_1_4() {

		// Verify the error message is displayed upon "Invalid user name and
		// password" combination

		Type(ID, SB_Login_UserName_ID, "QA");
		Type(ID, SB_Login_Password_ID, "Veritas1234");
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("li", "Invalid user name or password, please try again.")),
				"Invalid user name or password, please try again. is not displayed");
		Type(ID, SB_Login_UserName_ID, "Veritas_QA");
		Type(ID, SB_Login_Password_ID, "Veritas123");
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("li", "Invalid user name or password, please try again.")),
				"Invalid user name or password, please try again. is not displayed");

	}

	@Test(priority = 5, enabled = true)
	public void SB_TC_1_0_1_5() {

		// Verify the error message when user login without username

		Type(ID, SB_Login_UserName_ID, "");
		Type(ID, SB_Login_Password_ID, SBPassword);
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "User Name is required")),
				"User Name is required. is not displayed");

	}

	@Test(priority = 6, enabled = true)
	public void SB_TC_1_0_1_6() {

		// Verify the error message when user login without password

		Type(ID, SB_Login_UserName_ID, SBUsername);
		Type(ID, SB_Login_Password_ID, "");
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "User Password is required.")),
				"User Password is required. is not displayed");

	}

	@Test(priority = 7, enabled = true)
	public void SB_TC_1_0_1_7() {

		// Verify error message when user name and password is not provided for
		// login

		Type(ID, SB_Login_UserName_ID, "");
		Type(ID, SB_Login_Password_ID, "");
		Click(ID, SB_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "User Name is required")),
				"User Name is required. is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "User Password is required.")),
				"User Password is required. is not displayed");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
