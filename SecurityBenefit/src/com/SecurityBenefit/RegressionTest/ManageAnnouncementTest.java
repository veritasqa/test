package com.SecurityBenefit.RegressionTest;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ManageAnnouncementTest extends BaseTestSecurityBenefit{

	public static String Announcement = "QA Test " + Get_Todaydate("MM/d/YYYY HH:mm:SS");
	public static String EditAnnouncement = "QA Test for Announcement " + Get_Todaydate("MM/d/YYYY HH:mm:SS");
	public static String HyperlinkURL_Stage = "https://staging.veritas-solutions.net/TheStandard/";
	public static String HyperlinkURL_Prod = "https://www.veritas-solutions.net/TheStandard/";
	public static String Itemof;

	public void CreateAnnouncement(String StartDate_d, String InactiveDate_d) throws InterruptedException {

		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);
		ExplicitWait_Element_Visible(Xpath, SB_MA_Title_Path);
		Click(Xpath, SB_MA_Addannouncementbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MA_Createannouncementbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, SB_MA_Announcement_iFrame_ID));
		Type(Xpath, SB_MA_AddAnnouncement_Path, Announcement);
		Switch_To_Default();
		Datepicker2(Xpath, SB_MA_AddStartdateCal_Path, Xpath, SB_MA_AddStartdateCalmonth_Path,
				Get_Todaydate("MMM"), StartDate_d, Get_Todaydate("YYYY"));
		Click(Xpath, SB_MA_AddStartdateTime_Path);
		Datepicker2(Xpath, SB_MA_AddinactivedateCal_Path, Xpath, SB_MA_AddinactivedateCalmonth_Path,
				Get_Futuredate("MMM"), InactiveDate_d, Get_Futuredate("YYYY"));
	//	Click(Xpath, SB_MA_AddinactivedateTime_Path);
		Click(Xpath, UserGroup_Checkbox("Administrators"));
		Click(Xpath, UserGroup_Checkbox("Advisors Excel"));	
		Click(Xpath, UserGroup_Checkbox("Appointed Reps"));
		Click(Xpath, UserGroup_Checkbox("Bank Appointed Reps"));
		Click(Xpath, UserGroup_Checkbox("EMO"));
		Click(Xpath, UserGroup_Checkbox("RIAs"));
		Click(Xpath, UserGroup_Checkbox("RPA"));
		
		Type(Xpath, SB_MA_AddSort_Path, "1");
		Click(Xpath, SB_MA_Createannouncementbtn_Path);
		Wait_ajax();
		Click(Xpath, SB_LP_Home_path);
		Wait_ajax();

	}

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[1]");
	}
	
	@Test(enabled = false, priority = 1)
	public void Pimco_TC_2_4_4_1_1() throws InterruptedException {

		// Validate page opens and appropriate fields/tables are displaying

		ExplicitWait_Element_Visible(Xpath, SB_MA_Title_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Title_Path),
				"'Manage Announcements' page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Announcement_Path),
				"'Announcement' search field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Announcementfilter_Path),
				" 'Filter' icon is not displayed by 'Announcement' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Startdate_Path),
				"'Start Date' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Startdatefilter_Path),
				" 'Filter' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_StartdateCalicon_Path),
				" 'Calendar' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Inactivedate_Path),
				" 'Inactive Date' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Inactivedatefilter_Path),
				" 'Filter' icon is not displayed by 'Inactive Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_InactivedateCalicon_Path),
				" 'Calendar' icon is not displayed by 'Inactive Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Sort_Path),
				" 'Sort' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Sortfilter_Path),
				"'Filter' icon is not displayed by 'Sort' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_OnlyActiveck_Path),
				" 'Only Active' checkbox is not displayed  on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_OnlyActivefilter_Path),
				" 'Filter' icon is not displayed by 'Only Active' checkbox");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Addannouncementbtn_Path),
				"'Add Announcement' button is not displayed on 'Manage Announcements' page at the bottom of the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Refreshbn_Path),
				" 'Refresh' button is not displayed on 'Manage Announcements' page at the bottom of the page");
		softAssert.assertAll();

	}
	
	@Test(enabled = true, priority = 2)
	public void Pimco_TC_2_4_4_1_2() throws InterruptedException, AWTException {

		// Add Announcement

		CreateAnnouncement(Get_Todaydate("d"), Get_Futuredate("d"));
		Click(Xpath, Textpath("a", "View All Announcements"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Test' is not displayed in 'View All Announcements' page ");

	}
	
	@Test(enabled = true, priority = 4)
	public void Pimco_TC_2_4_4_1_3() throws InterruptedException, AWTException {

		// Edit the Announcement

		Type(Xpath, SB_MA_Announcement_Path, Announcement);
		Selectfilter(SB_MA_Announcementfilter_Path, "EqualTo");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Announcement)),
				"Searched result 'QA Test' is not displayed under 'Announcement' column with 'Edit' link");
		Click(Xpath, SB_MA_Edit1_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, SB_MA_Announcement_iFrame_ID));
		Type(Xpath, SB_MA_AddAnnouncement_Path, EditAnnouncement);
		Switch_To_Default();
		Click(Xpath, SB_MA_Updateammouncementbtn_Path);
		Wait_ajax();
		Click(Xpath, SB_LP_Home_path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "View All Announcements"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", EditAnnouncement)),
				"'QA Test' is not displayed in 'View All Announcements'");

	}
	

	@Test(enabled = true, priority = 5)
	public void Pimco_TC_2_4_4_1_4() throws InterruptedException, AWTException {

		// Pre-Req

		CreateAnnouncement(Get_Future_Nth_Date("d", 2), Get_Futuredate("d"));
		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);

		// Activate/Deactivate Announcements

		Type(Xpath, SB_MA_Announcement_Path, Announcement);
		Selectfilter(SB_MA_Announcementfilter_Path, "EqualTo");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Announcement)),
				"Searched result 'QA Testing' is not displayed under 'Announcement' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("td", Get_Future_Nth_Date("M/d/YYYY", 2) + " 12:00:00 AM")),
				" <Future Date & Time- MM/DD/YYYY HH:MM:SS AM/PM> is  not displayed under 'Start Date' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("td", Get_Futuredate("M/d/YYYY") + " 12:00:00 AM")),
				" <Future Date & Time- MM/DD/YYYY HH:MM:SS AM/PM> is  not displayed under 'Inactive Date' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MA_Activate1_Path), "Activate is not displayed");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MA_Onlyactiveck1_Path),
				"'Only Active' checkbox is checked off for 'QA Testing'");
		softAssert.assertAll();
		Click(Xpath, SB_MA_Activate1_Path);
		Wait_ajax();
		Click(Xpath, SB_LP_Home_path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Testing' is not displayed in View All Announcements page");
		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);
		ExplicitWait_Element_Visible(Xpath, SB_MA_Title_Path);
		Type(Xpath, SB_MA_Announcement_Path, Announcement);
		Selectfilter(SB_MA_Announcementfilter_Path, "EqualTo");
		Click(Xpath, SB_MA_Activate1_Path);
		Click(Xpath, SB_LP_Home_path);
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Testing' is displayed in View All Announcements after deactivated");

	}
	
	@Test(enabled = true, priority = 3)
/*	public void Pimco_TC_2_4_4_1_7() throws InterruptedException, AWTException, IOException {

		// Validate User is able to view announcement by changing the audience

		// CreateAnnouncement(Get_Todaydate("d"), Get_Futuredate("d"));
		 Announcement = "QA Test for Announcement 08/1/2018 14:43:134";

		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);
		Type(Xpath, SB_MA_Announcement_Path, Announcement);
		Selectfilter(SB_MA_Announcementfilter_Path, "EqualTo");
		Click(Xpath, SB_MA_Edit1_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, UserGroup_Checkbox("Advisor Excel")),
				"Advisor Excel is not checked");
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBoth1Username);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBoth1Username)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Clickedituser(PIMCOBoth1Username);

		if (Element_Is_selected(Xpath, UsergroupCheckbox("Advisory/Brokerage"))) {

			Click(Xpath, UsergroupCheckbox("Advisory/Brokerage"));
			Click(Xpath, Pimco_MU_Edit_Updatebtn_Path);
		}
		Click(ID, Pimco_LP_Logout_ID);

		login(PIMCOBoth1Username, PIMCOBoth1Password);
		Click(Xpath, Textpath("a", "View All Announcements"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Test' is not displayed in 'View All Announcements'page ");
		Click(ID, Pimco_LP_Logout_ID);

		login(PIMCOBothUsername, PIMCOBothPassword);
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBoth1Username);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBoth1Username)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Clickedituser(PIMCOBoth1Username);
		Click(Xpath, UsergroupCheckbox("Advisory/Brokerage"));
		Click(Xpath, Pimco_MU_Edit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

	} */
 
	
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		// Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
