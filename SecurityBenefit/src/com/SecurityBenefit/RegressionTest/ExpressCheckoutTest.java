package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ExpressCheckoutTest extends BaseTestSecurityBenefit{
	
	
	@Test(enabled =true ,priority = 1)
	public void SB_TC_2_5_1_1_1() throws InterruptedException{
		Click(Xpath, SB_LP_EC_SelectaRecipient_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, SB_CSP_Search_Btn_ID);
		
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		
		ExplicitWait_Element_Clickable(Xpath, SB_LP_EC_CheckoutBtn_path);
		
		Type(Xpath, SB_LP_EC_ProductCode_Txtbox1_Path, QA_Multifunction);
		Wait_ajax();
		Type(Xpath, SB_LP_EC_Qty_Txtbox1_Path, "1");
		Wait_ajax();
		Click(Xpath, SB_LP_EC_CheckoutBtn_path);
		Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");

	    Clearcarts();
	   // Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (0)")),"Checkout link on top right side of the page is  Not grayed out");

		
	}
	
	@Test(enabled =true ,priority = 3)
	public void SB_TC_2_5_1_2_1() throws InterruptedException{
		Clearcarts();
		Click(Xpath, SB_LP_EC_SelectaRecipient_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, SB_CSP_Search_Btn_ID);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SB_LP_EC_CheckoutBtn_path);
		
		Type(Xpath, SB_LP_EC_ProductCode_Txtbox1_Path, QA_Multifunction);
		Wait_ajax();
		Type(Xpath, SB_LP_EC_Qty_Txtbox1_Path, "1");
		Wait_ajax();
		Click(Xpath, SB_LP_EC_CheckoutBtn_path);
		Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	  	Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Get_Orderno();
		Homepage();
		
			
		
	}
	
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);


	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		// Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}
}
