package com.SecurityBenefit.RegressionTest;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class SpecialProjectsAndADDSp extends BaseTestSecurityBenefit{
	
	@Test(enabled = true, priority = 4)
	public void SB_TS_2_4_10_1_4() throws InterruptedException, IOException {
		// Add a new special project and verify new ticket appears on Special
		// projects: view tickets table

		SpecialProjectsNav();
		Click(Xpath, SB_Addticketicon_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SB_Gen_Cancel_Path);
		Type(Xpath, SB_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SB_Gen_JobType_Path, "Other");
		Click(Xpath, SB_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_Gen_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_TicketNo_Path),
				"New <Ticket Number> is not displayed for the 'Ticket #' field");
		SplprjTicketNumber = Get_Text(Xpath, SB_Gen_TicketNo_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber); 
		System.out.println("Created ticket number is " + SplprjTicketNumber);
		Reporter.log("Created ticket number is " + SplprjTicketNumber);
		SpecialProjectsNav();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				"New <Ticket Number> is not displayed under the 'Ticket #' column on the 'Special Projects: View Tickets ' grid");
		
	}

	@Test(enabled = true, priority = 7)
	public void SB_TS_2_4_10_1_7() throws InterruptedException, IOException {

		// Validate the cancel functionality
	//	SplprjTicketNumber ="000476";
		SpecialProjectsNav();
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		Click(Xpath, SB_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_Prog_Duedatecal_Path);
		Click(Xpath, SB_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_Prog_Submit_Path);
		Type(Xpath, SB_Prog_Comment_Path, "'QA is validating cancel functionality");
		Select_lidropdown(SB_Prog_Type_Path, Xpath, "Cancel");
		Wait_ajax();
		Click(Xpath, SB_Prog_Submit_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, SB_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed by the 'Status' field on the header of the 'Progress/Files' tab");
		SpecialProjectsNav();
		Click(Xpath, SB_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				" <Ticket #> is not displayed under the 'Ticket #' column on the 'Special Projects : View Tickets' grid");
		Assert.assertTrue(Get_Text(Xpath, Status(SplprjTicketNumber)).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed under 'Status' column for the copied <Ticket #> on 'Special Projects : View Tickets' grid");
	}
	
	
	
	
	
	// ADD SP Test Cases

		@Test(enabled = true, priority = 9)
		public void SI_TC_2_3_11_1_1_1AndSI_TC_2_3_11_1_1_2() throws InterruptedException, AWTException {

			// Validate upon clicking "Add Special Projects" takes to General
			// Special projects

			MouseHover(Xpath, LP_Admin_path);
			Thread.sleep(3000);
			MouseHover(Xpath, Textpath("span", "Special Projects"));
			Thread.sleep(2000);
			Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Add Special Project")),
					"'Add Special Project' is not displayed"); 
			Click(Xpath, Textpath("span", "Add Special Project"));
			Assert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_JobTitle_Path),
					"'Job Title' field is not displayed  in 'General' tab on 'Special Projects' page");

			// SI_TC_2_3_11_1_1_2

			// Create a special project without entering data and Verify the error
			// messages

			softAssert.assertTrue(Get_Attribute(Xpath, SB_Gen_JobTitle_Path, "value").equalsIgnoreCase(""),
					"' ' is not displaying in 'Job Title' field");
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_JobTitleReq_Path),
					"A red asterisk * is not displayed by the 'Job Title' field on  'General' tab");
			softAssert.assertTrue(Get_DropDown(Xpath, SB_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
					"'-Select-'  is not displays in 'Job Type' drop down field");
		//	softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_JobTypeReq_Path),
		//			" a red asterisk * is not displayed by the 'Job Type' field on  'General' tab");
			softAssert.assertTrue(Get_Attribute(Xpath, SB_Gen_Description_Path, "value").equalsIgnoreCase(""),
					"' ' is not displaying in 'Description' field");
			
			softAssert.assertTrue(Get_Attribute(Xpath, SB_Gen_Costcenter_Path, "value").equalsIgnoreCase(""),
					"' ' is not displaying in 'Cost center' field");
			softAssert.assertTrue(Get_Attribute(Xpath, SB_Gen_Splinst_Path, "value").equalsIgnoreCase(""),
					"' ' is not displaying in 'Special Instructions' field");
			softAssert.assertFalse(Element_Is_selected(Xpath, SB_Gen_Isrush_Path), "'Is Rush?' checkbox is checked");
			softAssert.assertFalse(Element_Is_selected(Xpath, SB_Gen_IsQuoteneeded_Path),
					"'Is Quote needed?' checkbox is checked");
			softAssert.assertAll();
			Click(Xpath, SB_Gen_Create_Path);
			Wait_ajax();
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Title_Path),
					"You must enter a value in the following fields: is not displayed");
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Jobtitle_Path),
					"Job Title IS Required is not displayed");
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Jobtype_Path),
					"Job Type Is Required is not displayed");
			softAssert.assertAll();
			Select_DropDown_VisibleText(Xpath, SB_Gen_JobType_Path, "Fulfillment Order Request");
			Click(Xpath, SB_Gen_Create_Path);
			Wait_ajax();
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Title_Path),
					"You must enter a value in the following fields: is not displayed - 2");
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Jobtitle_Path),
					"Job Title IS Required is not displayed - 2");
			softAssert.assertAll();
			Thread.sleep(2000);
			Select_DropDown_VisibleText(Xpath, SB_Gen_JobType_Path, "- Select -");
			Thread.sleep(2000);
			Type(Xpath, SB_Gen_JobTitle_Path, "QA");
			Thread.sleep(2000);
			Click(Xpath, SB_Gen_Create_Path);
			Thread.sleep(2000);
			Wait_ajax();
			Click(Xpath, SB_Gen_Create_Path);
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Title_Path),
					"You must enter a value in the following fields: is not displayed - 3");
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Error_Jobtype_Path),
					"Job Type Is Required is not displayed - 3");
			softAssert.assertAll();
			Click(Xpath, SB_ProgramTab_Path);
			Wait_ajax();
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_UnSuccessmsg_Path),
					" 'Create the Special Project, before proceeding.' error message is not displayed - Program Tab");
			Click(Xpath, SB_CostsTab_Path);
			Wait_ajax();
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_UnSuccessmsg_Path),
					" 'Create the Special Project, before proceeding.' error message is not displayed - Costs Tab");
			Click(Xpath, SB_InventoryTab_Path);
			Wait_ajax();
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_UnSuccessmsg_Path),
					" 'Create the Special Project, before proceeding.' error message is not displayed - Inventory Tab");
			Click(Xpath, SB_HistoryTab_Path);
			Wait_ajax();
			softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Gen_UnSuccessmsg_Path),
					" 'Create the Special Project, before proceeding.' error message is not displayed - History Tab");
			softAssert.assertAll();
		}
		
		
	
	
	public String ClickViewEdit(String Ticketno) throws InterruptedException {

		return ".//a[text()='" + Ticketno + "']//following::a[1][text()='View/Edit']";
	}

	public String History(String Changehistory) {

		return ".//td[contains(text(),'" + Changehistory + "')]";
	}

	public String Status(String Ticketno) throws InterruptedException {

		return ".//a[text()='" + Ticketno + "']//following::td[3]";
	}
	
	public void SpecialProjectsNav() {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Special Projects"));
		ExplicitWait_Element_Clickable(Xpath, SB_Addticketicon_Path);

	}
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);

	}

	@BeforeMethod
	public void beforeMethod() {

		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

		for (String Cancelspecialproject : SplprjTicketNumberlist) {

			SpecialProjectsNav();
			Type(Xpath, SB_TicketNo_Path, Cancelspecialproject);
			Click(Xpath, SB_JobtitleFilter_Path);
			Click(Xpath, Textpath("span", "Contains"));

			if (Element_Is_Displayed(Xpath, Textpath("div", "No Special Projects Available"))) {

			} else {

				Click(Xpath, ClickViewEdit(Cancelspecialproject));
				Click(Xpath, SB_ProgramTab_Path);
				ExplicitWait_Element_Clickable(Xpath, SB_Prog_Duedatecal_Path);
				Click(Xpath, SB_Prog_Addcommenticon_Path);
				ExplicitWait_Element_Clickable(Xpath, SB_Prog_Submit_Path);
				Type(Xpath, SB_Prog_Comment_Path, "'QA cancel");
				Select_lidropdown(SB_Prog_Type_Path, Xpath, "Cancel");
				Wait_ajax();
				Click(Xpath, SB_Prog_Submit_Path);
				Wait_ajax();

			}

		}

	}

}
