package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ManageOrders extends BaseTestSecurityBenefit {

	public void ManageOrdersNav() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Orders"));
		ExplicitWait_Element_Clickable(Xpath, MO_Searchbtn_Path);
		Wait_ajax();

	}

	@Test(priority = 1, enabled = false)
	public void Prereq() throws InterruptedException {

		// Add a part to cart from the fulfillment search product page and
		// complete order

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath("QA_Multifunction"));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Get_Orderno();
	}

	@Test(priority = 3, enabled = false)
	public void SB_TC_2_4_6_1_2() throws InterruptedException {

		// Verify search order copy functionality

		ManageOrdersNav();

		Type(Xpath, MO_Ordernofield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, MO_Searchbtn_Path);
		Thread.sleep(3000);
		ExplicitWait_Element_Clickable(Xpath, MO_SearchresultsCopy(OrderNumber));

		Click(Xpath, MO_SearchresultsCopy(OrderNumber));
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("span", "Please select a contact to copy the order for.")),
				"Please select a contact to copy the order for. is not dispalyed in the contact search page");
		ContactClick("qaauto", "automation");
		ExplicitWait_Element_Clickable(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not dispalyed in the Shopping cart page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not dispalyed in the Shopping cart page");
		Click(Xpath, SB_SCP_Remove_btn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Item(s) have been removed from your cart.")),
				"Item(s) have been removed from your cart. is not dispalyed in the Shopping cart page");
	}

	@Test(priority = 4, enabled = true)
	public void SB_TC_2_4_6_1_3() throws InterruptedException {

		// Validate that select order - view functionality works appropriately

		ManageOrdersNav();
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Viewbtn_Path);

		ExplicitWait_Element_Clickable(Xpath, MO_Gen_OrderSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MO_GeneralTab_Path), "General Tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Ticketnumber1_Path).equalsIgnoreCase(OrderNumber),
				"(Order Number) is not displays in 'Manage Order' field on header bar");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Username1_Path).equalsIgnoreCase(Both_Fname1 + " " + Both_Lname1),
				" (First Name Last Name) not displays under 'User Name' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Orderdate1_Path).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"(Today's Date - MM/DD/YYYY) not displays under 'Order Date' column");
		softAssert.assertFalse(Get_Text(Xpath, MO_Gen_Shipdate1_Path).isEmpty(),
				"(Date - MM/DD/YYYY) not displays under 'Ship Date' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Closedate1_Path).trim().equalsIgnoreCase(""),
				"\"\" not displays under 'Close Date' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Canceldate1_Path).trim().equalsIgnoreCase(""),
				"\"\" not displays under 'Cancel Date' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_OrderType1_Path).equalsIgnoreCase("SECURITYBENEFIT_FULFILLMENT"),
				"SECURITYBENEFIT_FULFILLMENT not displays under 'Order Type' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_OnBackOrder1_Path).equalsIgnoreCase("False"),
				"False not displays under 'Back Order' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_OnHold1_Path).contains("False"),
				"False not displays under 'Hold For Complete' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_SelectOrder_Path),
				"'Select Order button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_EditStatus_Path),
				"'Edit Status button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_OrderSearchbtn_Path),
				"'Order Search button' is not displayed");
		softAssert.assertAll();

		Click(Xpath, MO_ItemsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Items_Nextbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Productcode1_Path).equalsIgnoreCase(QA_Multifunction),
				QA_Multifunction + "  not displays under 'Product Code' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Title1_Path).equalsIgnoreCase(QA_Multifunction + "_DESCRIPTION"),
				QA_Multifunction + "_Title not displays under 'Title' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Qty1_Path).equalsIgnoreCase("1"),
				"'1' not displays under 'Qty' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Shipped1_Path).equalsIgnoreCase("0"),
				"'0' not displays under 'Shipped' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Backorder1_Path).equalsIgnoreCase("0"),
				"'0' not displays under 'Backorder' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Chargeback1_Path).equalsIgnoreCase("$0.00"),
				"'$0.00' not displays under 'Chargeback' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Print1_Path).equalsIgnoreCase("$0.01"),
				"'$0.00' not displays under 'Print' column ");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Fulfillment1_Path).equalsIgnoreCase("$0.42"),
				" '$0.53' not displays under 'Fulfillment' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Items_Total1_Path).equalsIgnoreCase("$0.43"),
				"'$0.54' not displays under 'Total' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Proof_Path),
				"'Proof' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Updatebtn_Path),
				"'Proof' column is not displays in the items tab");
		softAssert.assertTrue(Get_Attribute(Xpath, MO_Items_Costcenterfield_Path, "value").equalsIgnoreCase("375"),
				"'375' not displays in 'Billing Cost Center' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Items_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side");
		softAssert.assertAll();

		Click(Xpath, MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Shipping_Nextbtn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "qaauto automation")),
				"User Name is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "913 Commerce Ct")),
				"Address is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "Buffalo Grove")),
				"City is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "IL")),
				"State is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "60089-2375")),
				"Phone no is not displayed under Address column");

		softAssert.assertTrue(Get_Text(Xpath, MO_Shipping_Shipmethod1_Path).equalsIgnoreCase("UPS Ground"),
				"UPS Ground is not displayed under Ship method column ");

		softAssert.assertTrue(Get_Text(Xpath, MO_Shipping_ShippingInformation1_Path).contains("Charges:$0.00"),
				"Charges:$0.00 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Shipping_ShippingInformation1_Path).contains("Weight:0"),
				"Weight:0 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Shipping_ShippingInformation1_Path).contains("Boxes:$0.00"),
				"Boxes:$0.00 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Shipping_ShippingInformation1_Path).contains("Skids:$0.00"),
				"Skids:$0.00 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Shipping_TrackingNumber1_Path).trim().equalsIgnoreCase(""),
				"'   ' not displays under 'Tracking Number' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side - Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipping_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the right side - Shipping tab");
		softAssert.assertAll();

		Click(Xpath, MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Ticketno1_Path).trim().equalsIgnoreCase(OrderNumber),
				"(Order Number) not displays under 'Ticket Number' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Firstname1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'First Name' column ");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_lastname1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'Last Name' column ");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Company1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'Company' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Address11_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'Address 1' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_City1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'City' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_State1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'State/Province' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Zip1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'Zip/Postal Code' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Phone1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'Phone' column");
		softAssert.assertTrue(Get_Text(Xpath, MO_Maillist_Trackeingno1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'Tracking Number' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Maillistfilebtn_Path),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		// Click(Xpath, MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertAll();

		Click(Xpath, MO_AttachmentTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Attachment_Savebtn_Path);

		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Type_Path),
				"'Attachment Type' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Createuser_Path),
				"'Create user' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Createdate_Path),
				"'Create Date' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Linktofile_Path),
				"'Link to File' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Refreshicon_Path),
				" 'Refresh' link is not displays on the right side of the page in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_AddAttachicon_Path),
				"'Add Attachment is not ' link displays in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Savebtn_Path),
				"'Save' button is not displays below 'Refresh' button in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Attachment_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Attachments Tab");
		softAssert.assertAll();

	}

	@Test(priority = 5, enabled = false)
	public void SB_TC_2_4_6_1_4() throws InterruptedException {

		// Validate that select order copy functionality works appropriately

		ManageOrdersNav();
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Copybtn_Path);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, SB_SCP_Next_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not dispalyed in the Shopping cart page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not dispalyed in the Shopping cart page");
		Click(Xpath, SB_SCP_Remove_btn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Item(s) have been removed from your cart.")),
				"Item(s) have been removed from your cart. is not dispalyed in the Shopping cart page");

	}

	@Test(priority = 7, enabled = true)
	public void SB_TC_2_4_6_1_5() throws InterruptedException {

		// Create Order Template

		ManageOrdersNav();
		Type(Xpath, MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, MO_Copybtn_Path);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, SB_SCP_Next_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not dispalyed in the Shopping cart page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not dispalyed in the Shopping cart page");
		Click(Xpath, SB_SCP_CreateOrderTemplate_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_SCP_COT_Title_Path),
				"'Create Order Template' button at the end of the page");
		Type(Xpath, SB_SCP_COT_Name_Path, "QA Template Test");
		Type(Xpath, SB_SCP_COT_Description_Path, "QA Test");
		Click(Xpath, SB_SCP_COT_Createbtn_Path);
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "QA Template Test order template has been successfully created.")),
				"'QA Template Test order template has been successfully created. is not displayed");
		Click(Xpath, SB_LP_Home_path);
		ExplicitWait_Element_Clickable(Xpath, SB_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Template Test"),
				"QA Template Test is not displayed in the Order template widget");

	}

	@Test(priority = 6, enabled = true)
	public void SB_TC_2_4_6_1_6() throws InterruptedException {

		// Verify that you can select an order and works appropriately
		ManageOrdersNav();
		Type(Xpath, MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MO_SR_Loadingpanel_Path);
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, MO_Gen_OrderSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MO_GeneralTab_Path), "General Tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, MO_Gen_Status_Path).equalsIgnoreCase("SUBMITTED"),
				"'Status New' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_SelectOrder_Path),
				"'Select Order button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Gen_EditStatus_Path),
				"'Edit Status button' is not displayed");
		softAssert.assertAll();

		Click(Xpath, MO_ItemsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Items_Nextbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, MO_Items_Productcode1_Path).equalsIgnoreCase(QA_Multifunction),
				QA_Multifunction + "  not displays under 'Product Code' column");

		Click(Xpath, MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Shipping_Nextbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, MO_Shipping_Shipmethod1_Path).equalsIgnoreCase("UPS Ground"),
				"This is a QA piece is not displayed under Recipient Notes column ");

		Click(Xpath, MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Maillist_Maillistfilebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MO_Maillist_Maillistfilebtn_Path),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");

		Click(Xpath, MO_AttachmentTab_Path);
		ExplicitWait_Element_Clickable(Xpath, MO_Attachment_Savebtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "There are no attachments associated with this order.")),
				"There are no attachments associated with this order. not displayed");
	}

	@Test(priority = 1, enabled = false)
	public void SB_TC_2_4_6_1_8() throws InterruptedException, IOException {

		// Ensure Select order, search order, and search results grids appear
		// appropriately

		ManageOrdersNav();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_SelectOrderfield_Path),
				"'Select Order' field is not displays in 'Select Order' section ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Viewbtn_Path), "'View' button is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Copybtn_Path), "'Copy' button is not displays");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Ordernofield_Path),
				"'Order Number' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Trackingnofield_Path),
				"'Tracking No' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Typedropdown_Path),
				"'Type' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Userfield_Path),
				"'User' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Shipperfield_Path),
				"'Shipper' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Statusdrop_Path),
				"'Status' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Startdatefield_Path),
				"'Start Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Startdatecalender_Path),
				"'Start Date' calender icon is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Enddatefield_Path),
				"'End Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Enddatecalender_Path),
				"'End Date' calender icon is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_RecipientName_Path),
				"'Recipient' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Addressfield_Path),
				"'Address' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Cityfield_Path),
				"'City' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Statefield_Path),
				"'State' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Zipfield_Path),
				"'Zip' field is not displays in 'Search order' section");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Searchbtn_Path),
				"'Search' button is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MO_Clearbtn_Path),
				"'Clear' button is not displays in 'Search order' section");

		softAssert.assertTrue(Get_Text(Xpath, MO_SR_OrderNo_Path).trim().equalsIgnoreCase("Order#"),
				" 'Order #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Orderedby_Path).trim().equalsIgnoreCase("Ordered by"),
				" 'Ordered by' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Trackingno_Path).trim().equalsIgnoreCase("Tracking#"),
				" 'Tracking #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Orderdate_Path).trim().equalsIgnoreCase("Order Date"),
				" 'Order Date' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Shipper_Path).trim().equalsIgnoreCase("Shipper"),
				" 'Shipper' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Status_Path).trim().equalsIgnoreCase("Status"),
				" 'Status' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_Recipient_Path).trim().equalsIgnoreCase("Recipient"),
				" 'Recipient' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_State_Path).trim().equalsIgnoreCase("State/Province"),
				" 'State/Province' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, MO_SR_OrderType_Path).trim().equalsIgnoreCase("OrderType"),
				" 'OrderType' column is not displays in 'Search Results' grid");
		softAssert.assertAll();

	}

	@BeforeClass

	public void beforeclass() throws IOException, InterruptedException {
		login(SBUsername, SBPassword);

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		ClickHome();

	}
}
