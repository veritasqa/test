package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ManageUserTest extends BaseTestSecurityBenefit {
	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[2]");
	}

	@Test(priority = 3, enabled = true)
	public void SB_TC_2_4_13_1_3() throws InterruptedException {

		/*
		 * Validate Edit option opens on Search Result Row and allows you to Edit the user
		 */

		Type(Xpath, SB_MU_UserName_Path, SBUsername);
		Click(Xpath, SB_MU_UserNamefilter_Path);
		// ExplicitWait_Element_Not_Visible(Xpath, SB_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", SBUsername)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		ExplicitWait_Element_Not_Visible(Xpath, SB_MU_Loading_Path);
		Wait_ajax();
		Clickedituser(SBUsername);
		Type(Xpath, SB_MU_Add_Address_Path, "913 Commerce Court-123");
		Click(Xpath, SB_MU_Edit_Updatebtn_Path);

		ExplicitWait_Element_Not_Visible(Xpath, SB_MU_Loading_Path);
		Clickedituser(SBUsername);
		Assert.assertTrue(
				Get_Attribute(Xpath, SB_MU_Add_Address_Path, "value").equalsIgnoreCase("913 Commerce Court-123"),
				"'913 Commerce Court-123' is not displayed in 'Address' field");
		Type(Xpath, SB_MU_Add_Address_Path, "913 Commerce Court");
		Click(Xpath, SB_MU_Edit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MU_Loading_Path);

	}

	@Test(priority = 4, enabled = true)
	public void SB_TC_2_4_13_1_4() throws InterruptedException, IOException {

		/*
		 * Validate Add User link opens "Add User mode" and user is allowed to enter values 
		 */

		Click(Xpath, SB_MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MU_Loading_Path);
		Type(Xpath, SB_MU_Add_Username_Path, "QAUSER");
		Type(Xpath, SB_MU_Add_Userpwd_Path, "Password");
		Type(Xpath, SB_MU_Add_Firstname_Path, "qaauto");
		Type(Xpath, SB_MU_Add_Lastname_Path, "SB_TC_2_4_13_1_4");
		Type(Xpath, SB_MU_Add_Email_Path, Email);
		Type(Xpath, SB_MU_Add_Address_Path, Address);
		Type(Xpath, SB_MU_Add_City_Path, City);
		Select_DropDown_VisibleText(Xpath, SB_MU_Add_State_Path, "Illinois");
		Type(Xpath, SB_MU_Add_Zip_Path, Zip);
		Select_DropDown_VisibleText(Xpath, SB_MU_Add_Costcenter_Path, "9999 - Veritas Cost Center");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MU_Add_Activeck_Path),
				" 'Active User' checkbox is not checked for 'Active' field");
		MoveandClick(Xpath, SB_MU_Add_ProfilePhoto_Path);
		Runtime.getRuntime()
				.exec(System.getProperty("user.dir")
						+ "\\src\\com\\Pimco\\Utils\\Upload_Files\\SB_TC_2_4_13_1_4_ProfilePhoto.exe" + " "
						+ System.getProperty("user.dir")
						+ "\\src\\com\\Pimco\\Utils\\Upload_Files\\SB_TC_2_4_13_1_4_ProfilePhoto.jpg");
		ExplicitWait_Element_Visible(Xpath, SB_MU_Add_ProfilePhoto_Remove_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MU_Add_ProfilePhoto_Remove_Path),
				"Profile Picture Removed is not displayed");
		Click(Xpath, UsergroupCheckbox("US"));
		Click(Xpath, UsergroupCheckbox("CA"));
		Click(Xpath, SecurityCheckbox("Veritas Admin"));
		Click(Xpath, SecurityCheckbox("Admin"));
		Click(Xpath, SecurityCheckbox("Order Approval Admin"));
		Click(Xpath, SecurityCheckbox("Super Admin"));
		Click(Xpath, SecurityCheckbox("Special Project"));
		Click(Xpath, SB_MU_Add_Cancelbtn_Path);
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		ExplicitWait_Element_Clickable(Xpath, SB_MU_Adduserbtn_Path);

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}
}
