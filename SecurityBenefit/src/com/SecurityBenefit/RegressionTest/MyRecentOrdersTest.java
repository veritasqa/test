package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class MyRecentOrdersTest extends BaseTestSecurityBenefit{

	//Create a new order
	@Test(enabled = true, priority = 1 )
	public void Pimco_TC_2_6_5_1_1() throws InterruptedException{
		

		FulfilmentSearch(QA_Flyer);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath("QA_Flyer"));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Flyer)),
				" 'QA_Flyer' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Get_Orderno();
	
	}
	
	//Copy order from widget
	@Test(enabled = true, priority = 2 )
	public void Pimco_TC_2_6_5_1_2() throws InterruptedException{
		
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,RecentorderStatus(OrderNumber)),"OrderNumber status is not New");
		
		Click(Xpath,RecentorderButtonpath(OrderNumber, "Copy"));
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, SB_CSP_Search_Btn_ID);
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", "Please select a contact to copy the order for.")),"Please select a contact to copy the order for. - Message not displayed");
		
		ContactClick("qaauto", "automation");
		Clearcarts();
		
				
	}
	
	
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
