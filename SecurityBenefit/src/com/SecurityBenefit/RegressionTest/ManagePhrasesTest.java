package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ManagePhrasesTest extends BaseTestSecurityBenefit{

	//Verify Search Grid is displayed with all appropriate fields
	@Test(priority =1)
	public void SB_TC_2_4_7_1_1() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Phrases"));
		
		ExplicitWait_Element_Visible(Xpath, Textpath("h1", "Manage Phrases"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Name")),"Name Coloumn is missing in manage phrases page ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Default Phrase")),"Default Phrase Coloumn is missing in manage phrases page ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Custom Phrase")),"Custom Phrase Coloumn is missing in manage phrases page ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Custom Active")),"Custom Active Coloumn is missing in manage phrases page ");
		
		
		
	}
	
	
	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
