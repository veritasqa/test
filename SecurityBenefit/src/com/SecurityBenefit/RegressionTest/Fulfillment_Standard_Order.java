package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class Fulfillment_Standard_Order extends BaseTestSecurityBenefit {

	@Test(priority = 1, enabled = true)
	public void SB_TC_2_7_1_1_1() throws InterruptedException {

		// Add a part to cart from the fulfillment search product page and
		// complete order

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath("QA_Multifunction"));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				" 'QA_MULTIFUNCTION' is not displays on 'Shopping Cart' page");
		Assert.assertTrue(Get_Attribute(Xpath, SB_SCP_Qty_Path, "value").equalsIgnoreCase("1"),
				" '1' is dispalyed in 'Quantity' field on 'Shopping Cart' page");
		Click(Xpath, SB_SCP_SubmitOrder_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("span", "Order Successfully Submitted")),
				"Order Successfully Submitted is not displayed");
		Get_Orderno();
	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "SB_TC_2_7_1_1_1")
	public void SB_TC_2_7_1_1_2() throws InterruptedException {

		// Validate that order confirmation page appears
		softAssert.assertTrue(Get_Text(ID, SB_OC_OrderDate_ID).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"(Today's Date: MM/DD/YYYY) appears for 'Order Date' field");
		softAssert.assertTrue(Get_Text(ID, SB_OC_OrderDate_ID).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"(Today's Date: MM/DD/YYYY) appears for 'Order Date' field");
		softAssert.assertTrue(Get_Text(ID, SB_OC_ShippedBy_ID).equalsIgnoreCase("UPS Ground"),
				"UPS Ground is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_OC_Ticketno_Path), "Ticket No is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, SB_OC_Status_Path).equalsIgnoreCase("Submitted"),
				"Submitted is not displayed under Order details grid");
		softAssert.assertTrue(Get_Text(Xpath, SB_OC_Shipbydate_Path).contains(Get_Todaydate("MM/dd/YYYY")),
				"(Today's Date: MM/DD/YYYY) is not displays by 'Ship By Date' field  in 'Order Details' grid");
		softAssert.assertFalse(Get_Text(Xpath, SB_OC_Deliverdate_Path).equalsIgnoreCase(Get_Todaydate("MM/dd/YYYY")),
				"(Future Date: MM/DD/YYYY) is not displays by 'Delievery By Date' field  in 'Order Details' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Cancel Order")),
				"'Cancel Order' button not appears in 'Order Details' grid");
		softAssert.assertTrue(Get_Text(Xpath, SB_OC_Stockno_Path).contains(QA_Multifunction),
				"QA_Multifunction is not displays in the stock no");
		softAssert.assertTrue(
				Get_Text(Xpath, SB_OC_Description_Path).toUpperCase().contains("QA_MULTIFUNCTION_DESCRIPTION"),
				"QA_MULTIFUNCTION_DESCRIPTION is not displays in the Description");
		softAssert.assertTrue(Get_Text(Xpath, SB_OC_Qty_Path).contains("1"),
				"1(1/0) is not displayed under Quantity");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Copy")), "'Copy' button not appears");
		softAssert.assertAll();
		Click(Xpath, SB_LP_Home_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_LP_ViewAllAnnouncements_path), "Home page is not displayed");

	}


	@Test(priority = 3, enabled = true)
	public void SB_TC_2_7_1_1_4() throws InterruptedException {

		// Validate an order template can be created

		Click(Xpath, SB_LP_ClearCart_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_LP_ViewAllAnnouncements_path), "Home page is not displayed");

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("qaauto", "automation");
		Wait_ajax();
		Click(Xpath, AddToCart_Name_Xpath(QA_Multifunction));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Click(Xpath, SB_SCP_CreateOrderTemplate_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_SCP_Loading_Path);
		Type(Xpath, SB_SCP_COT_Name_Path, "QA Template Test");
		Type(Xpath, SB_SCP_COT_Description_Path, "QA Test");
		Click(Xpath, SB_SCP_COT_Createbtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "QA Template Test order template has been successfully created.")),
				"QA Template Test order template has been successfully created. message is not displayed");
		Click(Xpath, SB_LP_Home_path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SB_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Template Test"),
				"QA Template Testis not displayed in the order Template widgets");
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
