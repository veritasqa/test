package com.SecurityBenefit.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class ManageInventory extends BaseTestSecurityBenefit {

	public static int Partno = 1;

	public void CreateNewpart(String PartName) throws InterruptedException {

		Type(Xpath, SB_MI_SI_StockNo_Path, PartName);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		while (Element_Is_Displayed(Xpath, SB_MI_StockNumber1_Path)) {

			Partno = Partno + 1;
			CreatePart = PartName.substring(0, PartName.length() - 1) + Partno;

			Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
			Click(Xpath, SB_MI_SI_Active_Icon_Path);
			Wait_ajax();
			Click(Xpath, li_value("- Any -"));
			Wait_ajax();
			Click(Xpath, SB_MI_Searchbtn_Path);
			ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);

			Wait_ajax();
		}
		Partno = 1;
	}

	public void MI_Save() {

		Click(Xpath, SB_MI_Gen_Save_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Savepopup_Ok_Path);
		Click(Xpath, SB_MI_Savepopup_Ok_Path);
	}

	@Test(priority = 2, enabled = true)
	public void SB_TC_2_4_3_1_1() throws InterruptedException {

		/* 
		 * Page opens with create new item, search items, and search results grids and displays appropriate fields
		 */

		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_New_CreateNewTypeTitle_Path),
				"'Create New Item' section on page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_SearchItemsTitle_Path),
				"'Search Item' section on page is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_New_CreateNewType_Path),
				"'Create New Type' drop-down is not displayd in 'Create New Item' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_New_CreateNewType_Path, "value").equalsIgnoreCase("- Any -"),
				"'Create New Type' drop-down has '- Any -' is not the default value");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_New_Add_Btn_Path),
				"'Add' button is not displayed near 'Create New Type' text field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_New_StockNoToCopy_Path),
				" 'Form Number to Copy' field is not displayed in 'Create New Item' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_New_Copy_Btn_Path),
				" 'Copy' button displays near 'Form Number To Copy' text field");
		Click(Xpath, SB_MI_New_CreateNewType_Path);
		// Wait_ajax();
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Formsbook")),
				" 'Formsbook' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Custom Wizard")),
				" 'Kit - Custom Wizard' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				" 'Kit-On the Fly' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Prebuilt")),
				" 'Kit-Prebuilt' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD � Customizable")),
				" 'POD-Customizable' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD")),
				" 'POD' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premium Item")),
				" 'Premium Item' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				" 'Stock' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Print to Shelf")),
				" 'Print to Shelf' is not displayed 'Create New Type' drop-down");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_StockNo_Path),
				" 'Form Number' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_Description_Path),
				" 'Description' field is not displayd in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_Keyword_Path),
				" 'Keyword' field is not displays in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_Madatoryfieldtext_Path),
				" the text '*Searches on: Item ID, Item Description, Notes and Keyword Fields' is not displayed next to the 'Keyword' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_InventoryType_Path),
				" 'inventory Type' drop-down is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in ' inventory Type' drop-down");

		Click(Xpath, SB_MI_SI_InventoryType_Path);
		// Wait_ajax();
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Formsbook")),
				" 'Formsbook' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				" 'Kit-On the Fly' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Prebuilt")),
				" 'Kit-Prebuilt' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD � Customizable")),
				" 'POD-Customizable' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premium Item")),
				" 'Premium Item' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD")),
				" 'POD' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Print to Shelf")),
				" 'Print to Shelf' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				" 'Stock' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Custom Wizard")),
				" 'Kit - Custom Wizard' is not displayed 'Inventory Type' drop-down");
		Click(Xpath, SB_MI_SI_InventoryType_Icon_Path);
		// Wait_ajax();
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_LiteratureType_Path),
				" 'Literature Type' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_LiteratureType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'Literature Type' drop-down");
		Click(Xpath, SB_MI_SI_LiteratureType_Path);
		Wait_ajax();
		// Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Ad")),
				" 'Ad' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Application")),
				" 'Application' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Article/Newsletter")),
				" 'Article/Newsletter' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Bio")),
				" 'Bio' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Brochure")),
				" 'Brochure' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Business Card")),
				" 'Business Card' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Buyers Guide")),
				" 'Buyers Guide' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CD/DVD")),
				" 'CD/DVD' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("E-Mail Correspondence")),
				" 'E-Mail Correspondence' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Envelope")),
				" 'Envelope' is not displayed in the 'Literature Type' drop-downdrop-down");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Fact Sheet")),
				" 'Fact Sheet' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Folder/Pocket Folder")),
				" 'Folder/Pocket Folder' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Guide")),
				" 'Guide' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit")),
				" 'Kit' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit Insert")),
				" 'Kit Insert' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Label")),
				" 'Label' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Letter")),
				" 'Letter' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Notecard")),
				" 'Notecard' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other")),
				" 'Other' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Performance")),
				" 'Performance' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Postcard")),
				" 'Postcard' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Poster")),
				" 'Poster' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Powerpoint Presentation")),
				" 'Powerpoint Presentation' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Prospectus")),
				" 'Prospectus' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("State Replacement Form")),
				" 'State Replacement Form' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Statement of Additional Information")),
				" 'Statement of Additional Information' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Video")),
				" 'Video' is not displayed in the 'Literature Type' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Sales Idea")),
				" 'Sales Idea' is not displayed in the 'Literature Type' drop-downdrop-down");

		Click(Xpath, SB_MI_SI_LiteratureType_Icon_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_Channel_Path),
				" 'Channel' drop-down is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Channel_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value displaying in 'Category' drop-down");
		Click(Xpath, SB_MI_SI_Channel_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Bank")),
				" 'Bank' is not displayed in the 'Channel' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate")),
				" 'Corporate' is not displayed in the 'Channel' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Defined Contribution")),
				" 'Defined Contribution' is not displayed in the 'Channel' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Education")),
				" 'Education' is not displayed in the 'Channel' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("EMO/IMO")),
				" 'EMO/IMO' is not displayed in the 'Channel' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("PRS")),
				" 'PRS' is not displayed in the 'Channel' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("RIA")),
				" 'RIA' is not displayed in the 'Channel' drop-downdrop-down");

		Click(Xpath, SB_MI_SI_Channel_Icon_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_Category_Path),
				" 'Category' drop-down is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Category_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value displaying in 'Category' drop-down");

		Click(Xpath, SB_MI_SI_Category_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Fixed Annuities")),
				" 'Fixed Annuities' is not displayed in the 'Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Fixed Indexed Annuities")),
				" 'Fixed Indexed Annuities' is not displayed in the 'Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Mutual Funds")),
				" 'Mutual Funds' is not displayed in the 'Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Variable Annuities")),
				" 'Variable Annuities' is not displayed in the 'Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other")),
				" 'Other' is not displayed in the 'Category' drop-downdrop-down");
		Click(Xpath, SB_MI_SI_Category_Icon_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_UserGroups_Path),
				" 'User Group' is not displayed  in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_UserGroups_Path, "value").equalsIgnoreCase("- Any -"),
				"  '-Any-' as default value in User Groups' field");
		Click(Xpath, SB_MI_SI_UserGroups_Path);
		// Wait_ajax();
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Administrators")),
				" 'Administrators' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Advisors Excel")),
				" 'Advisors Excel' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Appointed Reps")),
				" 'Appointed Reps' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Bank Appointed Reps")),
				" 'Bank Appointed Reps' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("EMO")),
				" 'EMO' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Internal Sales Users")),
				" 'Internal Sales Users' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Internal Users")),
				" 'Internal Users' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Law/Marketing")),
				" 'Law/Marketing' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("RIAs")),
				" 'RIAs' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("RPA")),
				" 'RPA' is not displayed in the 'User Group' drop-downdrop-down");
		Click(Xpath, SB_MI_SI_UserGroups_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_Active_Path),
				" 'Active' field is not displayed in 'Search Items' section ");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"  'Active' as default value in Active' field");
		Click(Xpath, SB_MI_SI_Active_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Active")),
				" 'Active' is not displayed in the  'Active' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Inactive")),
				" 'Inactive' is not displayed in the  'Active' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Obsolete")),
				" 'Obsolete' is not displayed in the  'Active' drop-downdrop-down");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_UnitsOnHand_Path),
				" 'Units on Hand' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' is not displayed by default in 'Units On Hand' dropdown");
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SI_UnitsOnHand_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("All")),
				" 'ALL' is not displayed in the  'Units On Hand' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Low Stock")),
				" 'Low Stock' is not displayed in the  'Units On Hand' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Out of Stock")),
				" 'Out of Stock' is not displayed in the  'Units On Hand' drop-downdrop-down");
		Click(Xpath, SB_MI_SI_UnitsOnHand_Icon_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_ProductCategory_Path),
				" 'Product Area' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_ProductCategory_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' is default value displaying in 'Product Area' dropdown");

		Click(Xpath, SB_MI_SI_ProductCategory_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Advanced Choice Annuity")),
				" 'Advanced Choice Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("AdvanceDesigns")),
				" 'AdvanceDesigns' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("AdvanceDesigns NY")),
				" 'AdvanceDesigns NY' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Advisor Mutual Fund Program")),
				" 'Advisor Mutual Fund Program' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Advisor Retirement Program")),
				" 'Advisor Retirement Program' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Advisor Variable Annuity")),
				" 'Advisor Variable Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("AdvisorDesigns")),
				" 'AdvisorDesigns' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("AdvisorDesigns NY")),
				" 'AdvisorDesigns NY' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("AEA Valuebuilder Mutual Fund Program")),
				" 'AEA Valuebuilder Mutual Fund Program' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("AEA Valuebuilder Variable Annuity")),
				" 'AEA Valuebuilder Variable Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Choice Annuity")),
				" 'Choice Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Classic Strategies")),
				" 'Classic Strategies' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Consumer � General Education")),
				" 'Consumer � General Education' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate")),
				" 'Corporate' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("EliteDesigns")),
				" 'EliteDesigns' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("EliteDesigns II")),
				" 'EliteDesigns II' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("EliteDesigns II NY")),
				" 'EliteDesigns II NY' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("EliteDesigns NY")),
				" 'EliteDesigns NY' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Flexible Spending Account")),
				" 'Flexible Spending Account' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Foundations Annuity")),
				" 'Foundations Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Group Retirement Program ")),
				" 'Group Retirement Program ' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("HRA")),
				" 'HRA' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Morningstar Services")),
				" 'Morningstar Services' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("NEA DirectInvest")),
				" 'NEA DirectInvest' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("NEA Retirement Program")),
				" 'NEA Retirement Program' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("NEA Valuebuilder HRA")),
				" 'NEA Valuebuilder HRA' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("NEA Valuebuilder Mutual Fund Program")),
				" 'NEA Valuebuilder Mutual Fund Program' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("NEA Valuebuilder Variable Annuity")),
				" 'NEA Valuebuilder Variable Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premier Choice")),
				" 'Premier Choice' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("RateTrack Annuity")),
				" 'RateTrack Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Reps � Guides and Reference Materials")),
				" 'Reps � Guides and Reference Materials' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Secure Income Annuity")),
				" 'Secure Income Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("SecureDesigns")),
				" 'SecureDesigns' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("SecureDesigns NY")),
				" 'SecureDesigns NY' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("SecurePoint Retirement")),
				" 'SecurePoint Retirement' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("SFR")),
				" 'SFR' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Total Interest Annuity")),
				" 'Total Interest Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Total Value Annuity")),
				" 'Total Value Annuity' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Variflex")),
				" 'Variflex' is not displayed in the  'Product Category' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Workplace Retirement Program Clean Shares")),
				" 'Workplace Retirement Program clean shares' is not displayed in the  'Product Category' drop-downdrop-down");

		Click(Xpath, SB_MI_SI_ProductCategory_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Searchbtn_Path), " 'Search' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Clearbtn_Path), " 'Clear' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SI_SearchResults_Path),
				"'Search Results' section on page is not displayed");

		softAssert.assertAll();

	}

	@Test(priority = 3, enabled = true)
	public void SB_TC_2_4_3_1_2() throws InterruptedException {

		/* Ability to select create new type and click add with appropriate page opening
		 * 
		 */
		CreateNewpart(QA_TESTNEWITEM_1);

		Click(Xpath, SB_MI_New_CreateNewType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("POD"));
		Click(Xpath, SB_MI_New_Add_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_NewItem_headerpart_Path),
				"'Manage Inventory Item: New Item' is not displayed");
		Type(Xpath, SB_MI_Gen_Stockno_Path, CreatePart);
		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		Type(Xpath, SB_MI_Gen_txtDescription_Path, "QA Test");
		Type(Xpath, SB_MI_Gen_txtKeyWords_Path, "QAKey");
		Click(Xpath, SB_MI_Gen_Literaturetype_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Ad"));
		Click(Xpath, SB_MI_Gen_ProductCategory_List_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "AdvisorDesigns"));
		Click(Xpath, SB_MI_Gen_Channel_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "Bank"));
		Click(Xpath, SB_MI_Gen_Viewability_Input_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Not Viewable"));
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Administrators")),
				"Administrators is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Advisors Excel")),
				"Advisors Excel is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Appointed Reps")),
				"Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Bank Appointed Reps")),
				"Bank Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("EMO")), "EMO is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Internal Sales Users")),
				"Internal Sales Users is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Internal Users")),
				"Internal Users is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Law/Marketing")),
				"Law/Marketing is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RIAs")), "RIAs is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RPA")), "RAP is not checked off");
		softAssert.assertAll();
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(priority = 4, enabled = true)
	public void SB_TC_2_4_3_1_3() throws InterruptedException {

		/*
		 * Validate that you can copy from the create new item grid 
		 	*/

		CreateNewpart(QA_TESTNEWITEM_1);

		Type(Xpath, SB_MI_New_StockNoToCopy_Path, QA_TESTNEWITEM_X);
		Wait_ajax();
		Click(Xpath, li_value(QA_TESTNEWITEM_X));
		Click(Xpath, SB_MI_New_Copy_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Text(Xpath, SB_MI_Gen_NewItem_headerpart_Path).contains("Copy Of QA_TESTNEWITEM_"),
				"'Manage Inventory Item: <Copy Of QA_TESTNEWITEM_xxxx : QA TEST is not displayed");
		Type(Xpath, SB_MI_Gen_Stockno_Path, CreatePart);
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtPredecessor_Path, "value").equalsIgnoreCase(QA_TESTNEWITEM_X),
				"'Predecessor' field is not pre-populated with <QA_TESTNEWITEM_X>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtDescription_Path, "value").toUpperCase().contains("QATEST"),
				"'Description' field is not pre-populated with <THIS IS A QA TEST>from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtKeyWords_Path, "value").toUpperCase().contains("QAKEY"),
				"'Title' field is not pre-populated with <QAKEY>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_InventoryType_DropDown_Path, "value").equalsIgnoreCase("POD"),
				" 'Inventory Type' drop-down is not pre-populated with <Print On-Demand> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Literaturetype_Path, "value").equalsIgnoreCase("Ad"),
				"'Lietrature Type' drop-down is not pre-populated with<Brochure> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_ProductCategory_List_Input_Path, "value")
						.equalsIgnoreCase("AdvanceDesigns"),
				"'Product Category' drop-down is not pre-populated with <AdvanceDesigns AN-EDJ> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Channel_Input_Path, "value").equalsIgnoreCase("Bank"),
				"'Channel' drop-down is not pre-populated with <Bank AN-EDJ> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_Viewability_Input_Path, "value").equalsIgnoreCase("Not Viewable"),
				"'Viewability' drop-down is pre-populated with <Not Viewable>from original part");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Gen_chkFulfillment_Path),
				"Fulfillment is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Appointed Reps")),
				"Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Bank Appointed Reps")),
				"Bank Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("EMO")), "EMO is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RIAs")), "RIAs is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RPA")), "RAP is not checked off");
		softAssert.assertAll();
		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(priority = 5, enabled = true)
	public void SB_TC_2_4_3_1_4() throws InterruptedException {

		/*
		 * Validate that a part can be copied from the Search Results table
		 		 */

		CreateNewpart(QA_Copy_SearchResults_1);
		Type(Xpath, SB_MI_SI_StockNo_Path, QA_Copy_SearchResults_X);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsCopy(QA_Copy_SearchResults_X));
		Click(Xpath, SB_MI_CopyalertOK_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Text(Xpath, SB_MI_Gen_NewItem_headerpart_Path).contains("Copy Of QA_Copy_SearchResults_"),
				"'Manage Inventory Item: <QA_Copy_SearchResults_xxxx : QA TEST is not displayed");
		Type(Xpath, SB_MI_Gen_Stockno_Path, CreatePart);
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtPredecessor_Path, "value").equalsIgnoreCase(QA_Copy_SearchResults_X),
				"'Predecessor' field is not pre-populated with <QA_Copy_SearchResults_X>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtDescription_Path, "value").toUpperCase().contains("QA Description"),
				"'Description' field is not pre-populated with <QA Description>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtKeyWords_Path, "value").toUpperCase().contains("QA Key"),
				"'Title' field is not pre-populated with <QA Key>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_InventoryType_DropDown_Path, "value").equalsIgnoreCase("POD"),
				" 'Inventory Type' drop-down is not pre-populated with <Print On-Demand> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Literaturetype_Path, "value").equalsIgnoreCase("Ad"),
				"'Lietrature Type' drop-down is not pre-populated with<Brochure> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_ProductCategory_List_Input_Path, "value")
						.equalsIgnoreCase("AdvanceDesigns"),
				"'Product Category' drop-down is not pre-populated with <AdvanceDesigns AN-EDJ> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_Viewability_Input_Path, "value").equalsIgnoreCase("Orderable"),
				"'Viewability' drop-down is pre-populated with <Orderable>from original part");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Gen_chkFulfillment_Path),
				"Fulfillment is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Appointed Reps")),
				"Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Bank Appointed Reps")),
				"Bank Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("EMO")), "EMO is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RIAs")), "RIAs is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RPA")), "RAP is not checked off");
		softAssert.assertAll();
		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(priority = 6, enabled = true)
	public void SB_TC_2_4_3_1_5() throws InterruptedException {

		/*
		 * Ensure all the Search items fields are working and appropriate search results appearf
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, QA_Multifunction);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("- Any -"));
		Thread.sleep(1000);
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Searchresults(QA_Multifunction)),
				QA_Multifunction + " is not displayed in the 'Search Results' section");
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_StockNo_Path, "value").isEmpty(),
				"Stock No field is not blank after cleared");

		Type(Xpath, SB_MI_SI_Description_Path, "QA");
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SB_MI_Description1_Path).contains("QA"),
				"Record with 'QA'  is not displayed in the 'Search Results' grid");
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Description_Path, "value").isEmpty(),
				"Description field is not blank after cleared");

		Type(Xpath, SB_MI_SI_Keyword_Path, "QA");
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Click(Xpath, SB_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtKeyWords_Path, "value").contains("QA"),
				"'QA' is not displayed in the 'Keyword' field");
		Switch_Old_Tab();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Keyword_Path, "value").isEmpty(),
				"Keyword field is not blank after cleared");

		Click(Xpath, SB_MI_SI_InventoryType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("POD"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SB_MI_Type1_Path).contains("POD"),
				"Parts in 'Search Results' grid does not have 'POD' in 'Type' column");
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");

		Click(Xpath, SB_MI_SI_LiteratureType_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("Application"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Click(Xpath, SB_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Literaturetype_Path, "value").equalsIgnoreCase("Application"),
				"'Application' is not displayed in the 'Collateral' field");
		Switch_Old_Tab();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_LiteratureType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");

		Click(Xpath, SB_MI_SI_Channel_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Bank"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Click(Xpath, SB_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Click(Xpath, SB_MI_Gen_Channel_Input_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_selected(Xpath, Dropdowncheckbox("Bank")),
				"Bank is not selected in  Channel dropdown");
		Switch_Old_Tab();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Channel_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Channel' drop-down after cleared");

		Click(Xpath, SB_MI_SI_Category_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Fixed Annuities"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Click(Xpath, SB_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, CatagoriesCheckbox("Fixed Annuities")),
				"'Fixed Annuities' is not selected in the 'Category' field");
		Switch_Old_Tab();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Category_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Category' drop-down after cleared");

		Click(Xpath, SB_MI_SI_UserGroups_Path);
		Wait_ajax();
		Click(Xpath, li_value("Appointed Reps"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Click(Xpath, SB_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Appointed Reps")),
				"Appointed Reps is not checked off");
		Switch_Old_Tab();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_UserGroups_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'User Group' drop-down after cleared");

		Assert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"'Active' is not pre papulated in the 'Active drop'down");
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Thread.sleep(1000);
		Assert.assertTrue(Get_Text(Xpath, SB_MI_Status1_Path).equalsIgnoreCase("A"),
				"In 'Search Results' grid have 'A' in 'Status' Column is not displayed in the first row ");
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Searchloadingpanel_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"'Active' is not displayed in 'Active' field after clicked clear button");

		Click(Xpath, SB_MI_SI_UnitsOnHand_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Out of Stock"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SB_MI_Onhand1_Path).equals("0"),
				"Results in 'Search Results' grid does not have '0' in 'On Hand' column");
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Units On Hand' field");

		Click(Xpath, SB_MI_SI_ProductCategory_Path);
		Wait_ajax();
		Click(Xpath, li_value("AdvanceDesigns"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		Thread.sleep(500);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Click(Xpath, SB_MI_Edit1_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Click(Xpath, SB_MI_Gen_ProductCategory_List_Input_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_selected(Xpath, SB_MI_Gen_ProductCategory_AdvanceDesigns_Path),
				"AdvanceDesigns is not selected");
		Switch_Old_Tab();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_ProductCategory_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Product Area' field");
		softAssert.assertAll();

	}

	@Test(priority = 7, enabled = true)
	public void SB_TC_2_4_3_1_6() throws InterruptedException {

		/*
		 * Ensure all the Search items clear button is working and appropriate search results appear
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, QA_Multifunction);
		Type(Xpath, SB_MI_SI_Description_Path, "QA Test");
		Type(Xpath, SB_MI_SI_Keyword_Path, "QA");
		Click(Xpath, SB_MI_SI_InventoryType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Formsbook"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_LiteratureType_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("Application"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_Channel_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Bank"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_Category_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Fixed Annuities"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_UserGroups_Path);
		Wait_ajax();
		Click(Xpath, li_value("Appointed Reps"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_UnitsOnHand_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Out of Stock"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_ProductCategory_Path);
		Wait_ajax();
		Click(Xpath, li_value("AdvanceDesigns"));
		Wait_ajax();
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("Active"));
		Wait_ajax();
		Click(Xpath, SB_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_StockNo_Path, "value").isEmpty(),
				"Stock No field is not blank after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Description_Path, "value").isEmpty(),
				"Description field is not blank after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Keyword_Path, "value").isEmpty(),
				"Keyword field is not blank after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_LiteratureType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Channel_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Channel' drop-down after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Category_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Category' drop-down after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_UserGroups_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'User Group' drop-down after cleared");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"'Active' is not displayed in 'Active' field after clicked clear button");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Units On Hand' field");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_SI_ProductCategory_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Product Area' field");
		softAssert.assertAll();

	}

	@Test(priority = 8, enabled = true)
	public void SB_TC_2_4_3_2_1() throws InterruptedException {

		/*
		 * Validate all fields that are not greyed out can be edited and saved
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, QA_NewTestPart_X);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(QA_NewTestPart_X));
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		Type(Xpath, SB_MI_Gen_txtDescription_Path, "QA_NewTestPart Description");
		Type(Xpath, SB_MI_Gen_txtLongDescription_Path, "QA_NewTestPart is a QA TEST Long");
		Type(Xpath, SB_MI_Gen_txtKeyWords_Path, "QA_NewTestPart Key Word");
		Select_lidropdown(SB_MI_Gen_InventoryType_Arrow_Path, Xpath, "POD");
		Type(Xpath, SB_MI_Gen_UnitsPerPack_Path, "2");

		Click(Xpath, SB_MI_Gen_Literaturetype_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Application"));
		Wait_ajax();
		Type(Xpath, SB_MI_Gen_FormCostCenter_Path, "5678");

		Click(Xpath, SB_MI_Gen_ProductCategory_List_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "AdvisorDesigns"));
		Click(Xpath, Textpath("label", "Advisor Mutual Fund Program"));
		Click(Xpath, SB_MI_Gen_Channel_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "Bank"));
		Click(Xpath, Textpath("label", "Corporate"));
		Type(Xpath, SB_MI_Gen_txtKeyWords_Path, Get_Futuredate("MM/dd/YYYY"));
		Click(Xpath, SB_MI_Gen_chkMarketing_Path);
		Click(Xpath, UserGroupsCheckbox("Appointed Reps"));
		Click(Xpath, UserGroupsCheckbox("Bank Appointed Reps"));
		Click(Xpath, UserGroupsCheckbox("EMO"));
		Click(Xpath, UserGroupsCheckbox("RIAs"));
		Click(Xpath, UserGroupsCheckbox("RPA"));
		Type(Xpath, SB_MI_Gen_txtNotes_Path, "Notes");
		Type(Xpath, SB_MI_Gen_txtSplmessage_Path, "Special Message");
		Datepicker2(Xpath, SB_MI_Gen_Splmessage_Startdate_Path, Xpath, SB_MI_Gen_Splmessage_Startdatemonth_Path,
				Get_Todaydate("MMM"), Get_Todaydate("d"), Get_Todaydate("YYYY"));
		Datepicker2(Xpath, SB_MI_Gen_Splmessage_Enddate_Path, Xpath, SB_MI_Gen_Splmessage_EnddateMonth_Path,
				Get_Futuredate("MMM"), Get_Futuredate("d"), Get_Futuredate("YYYY"));
		Click(Xpath, CatagoriesCheckbox("Fixed Annuities"));
		Type(Xpath, SB_MI_Gen_EffectiveDate_dateInput_Path, Get_Futuredate("MM/dd/YYYY"));
		Click(Xpath, SB_MI_Gen_Viewability_Input_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Admin Only"));
		MI_Save();

	}

	@Test(priority = 9, enabled = true)
	public void SB_TC_2_4_3_2_2() throws InterruptedException {

		/*
		 * Validate appropriate changes are recorded in change history: for edits
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, QA_NewTestPart_X);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(QA_NewTestPart_X));
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);

		Click(Xpath, SB_MI_ChangeHistoryTab_Path);
		Wait_ajax();
		Select_lidropdown(SB_MI_ChangeHistory_Pagesize_Action_Path, Xpath, "20");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td",
				"UserGroups Added: {-None-}; UserGroups Removed: {Appointed Reps, Bank Appointed Reps, EMO, RIAs, RPA}")),
				"UserGroups Added: {-None-}; UserGroups Removed: {Appointed Reps, Bank Appointed Reps, EMO, RIAs, RPA} is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "EffectiveDate changed from")),
				"EffectiveDate changed from '12:00:00 AM' to '(Future Date - MM/DD/YYYY) is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "RevisionDate changed from")),
				"RevisionDate changed from is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "UnitsPerPack changed from '1' to '2'")),
				"UnitsPerPack changed from '1' to '2' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						TextpathContains("td", "SpecialMessage changed from '' to 'Special Message'")),
				"SpecialMessage changed from '' to 'Special Message' is not displayed");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								TextpathContains("td",
										"KeyWords changed from 'QA_NewTestPart' to 'QA_NewTestPart Key Word'")),
						"KeyWords changed from 'QA_NewTestPart' to 'QA_NewTestPart Key Word' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "Notes changed from '' to 'NOTES'")),
				"Notes changed from '' to 'NOTES' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "IsMarketing changed from 'False' to 'True'")),
				"IsMarketing changed from 'False' to 'True' is not displayed");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								TextpathContains("td",
										"Viewability changed from 'ViewableAndOrderable' to 'AdminOnly'")),
						"Viewability changed from 'ViewableAndOrderable' to 'AdminOnly' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td",
				"LongDescription changed from 'QA_NewTestPart is a QA TEST' to 'QA_NewTestPart is a QA TEST Long'")),
				"LongDescription changed from 'QA_NewTestPart is a QA TEST' to 'QA_NewTestPart is a QA TEST Long' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						TextpathContains("td",
								"Description changed from 'QA_NewTestPart' to 'QA_NewTestPart Description'")),
				"Description changed from 'QA_NewTestPart' to 'QA_NewTestPart Description' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "InventoryTypeID changed from 'Stock' to 'POD'")),
				"InventoryTypeID changed from 'Stock' to 'POD' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td",
				"ProductCategory values 'removed {-None-}' 'added ( Advisor Mutual Fund Program, AdvisorDesigns)'")),
				"ProductCategory values 'removed {-None-}' 'added ( Advisor Mutual Fund Program, AdvisorDesigns)' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						TextpathContains("td", "Channel values 'removed (Bank)' 'added (Corporate)'")),
				"Channel values 'removed (Bank)' 'added (Corporate)' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "DocumentType changed from 'Ad' to 'Application'")),
				"DocumentType changed from 'Ad' to 'Application' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "FormCostCenter changed from '1234' to '5678'")),
				"FormCostCenter changed from '1234' to '5678' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "SpecialMessageDeactivationDate changed from ")),
				"SpecialMessageDeactivationDate changed from  is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "SpecialMessageActivationDate changed")),
				"SpecialMessageActivationDate changed is not displayed");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								TextpathContains("td",
										"Category Added: {Fixed Annuities}; Category Removed: {-None-}")),
						"Category Added: {Fixed Annuities}; Category Removed: {-None-} is not displayed");

	}

	@Test(priority = 10, enabled = true)
	public void SB_TC_2_4_3_2_3() throws InterruptedException {

		/*
		 * Validate all fields that are not greyed out can have the edits undone and saved
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, QA_NewTestPart_X);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(QA_NewTestPart_X));
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "003/08/2018");
		Type(Xpath, SB_MI_Gen_txtDescription_Path, "QA_NewTestPart");
		Type(Xpath, SB_MI_Gen_txtLongDescription_Path, "QA_NewTestPart is a QA TEST");
		Type(Xpath, SB_MI_Gen_txtKeyWords_Path, "QA_NewTestPart");
		Select_lidropdown(SB_MI_Gen_InventoryType_Arrow_Path, Xpath, "Stock");
		Type(Xpath, SB_MI_Gen_UnitsPerPack_Path, "1");

		Click(Xpath, SB_MI_Gen_Literaturetype_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Ad"));
		Wait_ajax();
		Type(Xpath, SB_MI_Gen_FormCostCenter_Path, "1234");

		Click(Xpath, SB_MI_Gen_ProductCategory_List_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "AdvisorDesigns"));
		Click(Xpath, Textpath("label", "Advisor Mutual Fund Program"));
		Click(Xpath, SB_MI_Gen_Channel_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "Bank"));
		Click(Xpath, Textpath("label", "Corporate"));
		Click(Xpath, SB_MI_Gen_chkMarketing_Path);
		Click(Xpath, UserGroupsCheckbox("Appointed Reps"));
		Click(Xpath, UserGroupsCheckbox("Bank Appointed Reps"));
		Click(Xpath, UserGroupsCheckbox("EMO"));
		Click(Xpath, UserGroupsCheckbox("RIAs"));
		Click(Xpath, UserGroupsCheckbox("RPA"));
		Type(Xpath, SB_MI_Gen_txtNotes_Path, "");
		Type(Xpath, SB_MI_Gen_txtSplmessage_Path, "");
		Click(Xpath, CatagoriesCheckbox("Fixed Annuities"));
		Type(Xpath, SB_MI_Gen_EffectiveDate_dateInput_Path, Get_Futuredate("MM/dd/YYYY"));
		Click(Xpath, SB_MI_Gen_Viewability_Input_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Orderable"));
		MI_Save();
	}

	@Test(priority = 11, enabled = true)
	public void SB_TC_2_4_3_2_4() throws InterruptedException {

		/*
		 * Validate appropriate changes are recorded in change history: for undo changes
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, QA_NewTestPart_X);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(QA_NewTestPart_X));
		Switch_New_Tab();
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);

		Click(Xpath, SB_MI_ChangeHistoryTab_Path);
		Wait_ajax();
		Select_lidropdown(SB_MI_ChangeHistory_Pagesize_Action_Path, Xpath, "20");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td",
				"UserGroups Added: {Appointed Reps, Bank Appointed Reps, EMO, RIAs, RPA}; UserGroups Removed: {-None-}")),
				"UserGroups Added: {Appointed Reps, Bank Appointed Reps, EMO, RIAs, RPA}; UserGroups Removed: {-None-} is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "RevisionDate changed from")),
				"RevisionDate changed from is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "UnitsPerPack changed from '2' to '1'")),
				"UnitsPerPack changed from '2' to '1' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						TextpathContains("td", "SpecialMessage changed from 'Special Message' to ''")),
				"SpecialMessage changed from 'Special Message' to '' is not displayed");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								TextpathContains("td",
										"KeyWords changed from 'QA_NewTestPart Key Word' to 'QA_NewTestPart'")),
						"KeyWords changed from 'QA_NewTestPart Key Word' to 'QA_NewTestPart' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "Notes changed from 'NOTES' to ''")),
				"Notes changed from 'NOTES' to '' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "IsMarketing changed from 'True' to 'False'")),
				"IsMarketing changed from 'True' to 'False' is not displayed");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								TextpathContains("td",
										"Viewability changed from 'NotViewable' to 'ViewableAndOrderable'")),
						"Viewability changed from 'NotViewable' to 'ViewableAndOrderable' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td",
				"LongDescription changed from 'QA_NewTestPart is a QA TEST Long' to 'QA_NewTestPart is a QA TEST'")),
				"LongDescription changed from 'QA_NewTestPart is a QA TEST Long' to 'QA_NewTestPart is a QA TEST' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						TextpathContains("td",
								"Description changed from 'QA_NewTestPart Description' to 'QA_NewTestPart'")),
				"Description changed from 'QA_NewTestPart Description' to 'QA_NewTestPart' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "InventoryTypeID changed from 'POD' to 'Stock'")),
				"InventoryTypeID changed from 'POD' to 'Stock'");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td",
				"ProductCategory values 'removed ( Advisor Mutual Fund Program, AdvisorDesigns)' 'added {-None-}'")),
				"ProductCategory values 'removed ( Advisor Mutual Fund Program, AdvisorDesigns)' 'added {-None-}' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						TextpathContains("td", "Channel values 'removed (Corporate)' 'added (Bank)'")),
				"Channel values 'removed (Corporate)' 'added (Bank)' is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "DocumentType changed from 'Application' to 'Ad'")),
				"DocumentType changed from 'Application' to 'Ad'");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "FormCostCenter changed from '5678' to '1234'")),
				"FormCostCenter changed from '5678' to '1234' is not displayed");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								TextpathContains("td",
										"Category Added: {-None-}; Category Removed: {Fixed Annuities}")),
						"Category Added: {-None-}; Category Removed: {Fixed Annuities} is not displayed");

	}

	@Test(priority = 12, enabled = true)
	public void SB_TC_2_4_3_3_1andSB_TC_2_4_3_3_2() throws InterruptedException {

		/*
		 * Validate all fields in general tab appear and are blank
		 */

		Click(Xpath, SB_MI_New_CreateNewType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Stock"));
		Click(Xpath, SB_MI_New_Add_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_NewItem_header_Path),
				"Manage Inventory Item : New Item' section is not displayed");

		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Stockno_Path, "value").isEmpty(),
				"'Form number' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_ProductCode_Path, "value").isEmpty(),
				"'Product Code' field is not blank");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtRevisionDate_Path, "value").equalsIgnoreCase("__/__/____"),
				"'' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtPredecessor_Path, "value").isEmpty(),
				"'Predecessor' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtDescription_Path, "value").isEmpty(),
				"'Description' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtLongDescription_Path, "value").isEmpty(),
				"'Long Description' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtKeyWords_Path, "value").isEmpty(),
				"'Keywords' field is not blank");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_InventoryType_DropDown_Path, "value").equalsIgnoreCase("Stock"),
				"'Stock' is not selected in 'Inventory Type' dropdown");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"'1' is not pre-populated in the Units per pack field");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_Literaturetype_Path, "value").equalsIgnoreCase("- Please Select -"),
				"'- Please Select -' is not selected in 'Lierature Type");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_FormCostCenter_Path, "value").isEmpty(),
				"'Form cost center' field is not blank");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_ProductCategory_List_Input_Path, "value")
						.equalsIgnoreCase("- Select Product Categorie(s) -"),
				"'- Select Product Categorie(s) -' is not displayed in Product Categorie(s) ");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_Channel_Input_Path, "value").equalsIgnoreCase("- Select Channel(s) -"),
				"'- Select -' is not displayed in Channel type");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_Viewability_Input_Path, "value").equalsIgnoreCase("- Please Select -"),
				"'- Please Select -' is not displayed in Viewability Drop down");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_EffectiveDate_dateInput_Path, "value").isEmpty(),
				"'Effective date' field is not blank");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Gen_chkFulfillment_Path),
				"User Kit Container Checkbox is not selected");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Gen_chkMarketing_Path),
				"User Kit Container Checkbox is not selected");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Gen_chkAppbuilder_Path),
				"App builder Checkbox is not selected");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_Usergrps_Path), "User Groups are not displayed");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Administrators")),
				"Administrators is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Advisors Excel")),
				"Advisors Excel is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Appointed Reps")),
				"Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Bank Appointed Reps")),
				"Bank Appointed Reps is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("EMO")), "EMO is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Internal Sales Users")),
				"Internal Sales Users is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Internal Users")),
				"Internal Users is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("Law/Marketing")),
				"Law/Marketing is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RIAs")), "RIAs is not checked off");
		softAssert.assertTrue(Element_Is_selected(Xpath, UserGroupsCheckbox("RPA")), "RAP is not checked off");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtNotes_Path, "value").isEmpty(),
				"'Notes' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_txtSplmessage_Path, "value").isEmpty(),
				"'Special message' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Splmessage_Startdatetxt_Path, "value").isEmpty(),
				"'Special message start date' field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Splmessage_Enddatetxt_Path, "value").isEmpty(),
				"'Special message End date' field is not blank");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h2", "Categories")),
				"Categories section is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h2", "Item Usage")),
				"Item Usage section is not displayed");
		softAssert.assertAll();

		/*
		 * SB_TC_2_4_3_3_2
		 * Validate all fields in general tab appear and are blank
		 */

		Click(Xpath, SB_MI_Gen_Save_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Invalid Stock #!")),
				"Invalid Stock #! error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Invalid Revision Date!")),
				"Invalid Revision Date! error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Description is required!")),
				"Description is required! error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Keywords are Required!")),
				"Keywords are Required! error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Literature type is required!")),
				"Literature type is required! error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Required")),
				"Product Category Required error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Viewability is required!")),
				"Viewability is required! error message is not displayed");
		softAssert.assertAll();
	}

	@Test(priority = 13, enabled = true)
	public void SB_TC_2_4_3_3_3andSB_TC_2_4_3_3_4andSB_TC_2_4_3_3_5andSB_TC_2_4_3_3_6() throws InterruptedException {

		/*
		 * Validate that filling out all required fields on general tab enable the other tabs
		 */

		CreateNewpart(QA_NewItemAddTest_1);
		ManageInventoryNavigation();
		Click(Xpath, SB_MI_New_CreateNewType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Stock"));
		Click(Xpath, SB_MI_New_Add_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_NewItem_headerpart_Path),
				"'Manage Inventory Item: New Item' is not displayed");
		Type(Xpath, SB_MI_Gen_Stockno_Path, CreatePart);
		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		Type(Xpath, SB_MI_Gen_txtDescription_Path, "QA Test");
		Type(Xpath, SB_MI_Gen_txtKeyWords_Path, "QA Key");
		Click(Xpath, SB_MI_Gen_Literaturetype_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Ad"));
		Click(Xpath, SB_MI_Gen_ProductCategory_List_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "AdvisorDesigns"));
		Click(Xpath, SB_MI_Gen_Channel_Arrow_Path);
		Thread.sleep(500);
		Click(Xpath, Textpath("label", "Bank"));
		Click(Xpath, SB_MI_Gen_Viewability_Input_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Not Viewable"));
		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Attachments")),
				"Attachments tab is not displayed");
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);
		Click(Xpath, SB_MI_Pricing_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Unit Price")),
				"Unit Price is not displayed");
		Click(Xpath, SB_MI_Pricing_FlatPricing_Path);
		Click(Xpath, SB_MI_Notification_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Notification Options")),
				"Notification Options is not displayed");
		Click(Xpath, SB_MI_RulesTab_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Rules")),
				"Rules Header is not displayed");
		Click(Xpath, SB_MI_MetricsTab_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Metrics")),
				"Metrics Header is not displayed");
		Click(Xpath, SB_MI_ChangeHistoryTab_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Change History")),
				"Change History Header is not displayed");
		Click(Xpath, SB_MI_PageflexTab_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Pageflex ")),
				"Pageflex  Header is not displayed");

		/*
		 * SB_TC_2_4_3_3_4
		 * Validate the all fields in the other tabs appear and most are blank
		 */

		Click(Xpath, SB_MI_AttachmentTab_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_PrdThumb_Path),
				"Product Thumbnail is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_PrdThumb_Upload_Path),
				"Upload - Product Thumbnail is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_Onlinesample_Path),
				"Online Sample is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_Onlinesampl_Upload_Path),
				"Upload - Online Sample is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_FINRAletter_Path),
				"Finra Letter is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_FINRAletter_Upload_Path),
				"Upload - Finra Letter is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_PrintReady_Path),
				"Print Ready PDF section is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_PrintReady_Browse_Path),
				"Print Ready PDF Browse is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_PrintReady_Password_Path),
				"Password field is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Attach_PODprintSpecs_Path),
				"POD Print specs is not displayed");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Colors_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Colors field");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Pagesize_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for page size ");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Stock_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Stock");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Coverstock_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Cover Stock");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Finish_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Finish");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Coating_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Coating");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Attach_Noofpages_Path, "value").equalsIgnoreCase("0"),
				"0 - is not displayed for No of pages");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Binding_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Binding");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_PrintMethod_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Print Method");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_Printype_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Print Type");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Attach_Flatsize_Path, "value").isEmpty(),
				"Flat Size field is not empty");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Attach_IMCOposition_Path, "value").isEmpty(),
				"'Imposition/Composition field is not empty");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Attach_Finishsize_Path, "value").isEmpty(),
				"Finish Size field is not empty");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Attach_PCSPrint_Path, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for PCS print");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Attach_ClickCost_Path, "value").equalsIgnoreCase("0.0000"),
				"0.0000 is not displayed for Click cost");

		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pricing_Path), "Pricing tab is not displayed");

		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Pricing_ChargebackCost_Path, "value").equalsIgnoreCase("0.0000"),
				"0.0000 is not populated in  'Chargeback Cost' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pricing_PricingDetails_Path),
				"Veritas Pricing Details is not displayed");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_PricingType_Flatck_Path),
				"Flat radio button is not selected");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Pricing_Tieredck_ChargebackCost_Path),
				"Tired radio button is selected");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Pricing_Approvedck_ChargeItem_Path),
				"Is Print Cost Approved: radio button is not selected");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pricing_FlatPricing_Path),
				"Flat Pricing is not displayed");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Pricing_FlatpricingCk_Path),
				"No Cost Item radio button is not selected");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Pricing_Prodcost_Path, "value").equalsIgnoreCase("0.0000"),
				"0.0000 is not populated in Production cost field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_Save_Path), "Save button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Back_Path), "Back button is not displayed");

		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Notifi_Title_Path), "Notification tab is not displayed");

		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Notifi_Notificationck_Path),
				" Send Reorder Point Email  radio button is selected");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Notifi_AtQuantityLevel_Path, "value").equalsIgnoreCase("0"),
				"0 - is not displayed for At Quantity level");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Notifi_ReOrderQuantity_Path, "value").equalsIgnoreCase("0"),
				"0 - is not displayed for Reorder Quantity");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Noticationaddress_Path),
				"Notification Addresses is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Securitybenefit@veritas-solutions.com")),
				"Securitybenefit@veritas-solutions.com email is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "fulfillmentadmin@securitybenefit.com")),
				"fulfillmentadmin@securitybenefit.com email is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Addemail_Path), "Add email button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Refresh_Path), "Refresh button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_Save_Path),
				"Save button is not displayed - Notifications tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Back_Path),
				"Back button is not displayed - Notifications tab");

		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Title_Path), "Rules tab is not displayed");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_RulesTab_Allowbackorder_Path),
				" Allow backorder radio button is selected");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_RulesTab_Removeduplicte_Path),
				"Can Remove Duplicates When Used As Child Item  Point Email  radio button is selected");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxorderTitle_Path),
				"Max Order Quantity header is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Rules_NoMax_Path).trim().contains("0 = No Max"),
				"0 = No Max is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_DefaultMaxOrderQty_Path, "value").equalsIgnoreCase("0"),
				"0 - is not displayed for Default Maximum Order Quantity");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Rules_DefaultMaxOrderQtyFrq_Path, "value").equalsIgnoreCase("Per Order"),
				"Per Order is not displayed in Default Maximum Order Quantity Frequency");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxOrderQtyPerRole_Field_Path),
				"Max Qty for Role is not displays in the 'Maximum Order Quantity Per Role' field");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Rules_MaxOrderQtyPerRole_Drop_Path, "value")
						.equalsIgnoreCase("Please select one"),
				"Please Select one is not displays in dropdown in same row as the 'Maximum Order Quantity Per Role' field ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxOrderQtyPerRole_Add_Path),
				"Add button is not displays - Rules tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Superseding_Path),
				"Superseding header is not displays");

		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ReplaceType_Path, "value").equalsIgnoreCase("None"),
				"None is not displays in replace type");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ReplaceWith_Field_Path, "value").isEmpty(),
				"Replace With Field is not empty");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ReplaceDate_Path, "value").isEmpty(),
				"Replace Date Field is not empty");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path),
				"Obsolete Now button is not displays");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ObsoleteDate_Path, "value").isEmpty(),
				"Obsolete Date Field is not empty");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_Reason_Field_Path, "value").isEmpty(),
				"Reason Field is not empty");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_RequireRelatedItems_Path),
				"Required/Related Items header is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Require_StockNo_Path),
				"Form Number column is not displays in the 'Required/Related Items' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Require_Quantity_Path),
				"Quantity column is not displays in the 'Required/Related Items' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Require_Contnentlocation_Path),
				"Current location column is not displays in the 'Required/Related Items' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Require_Addbtn_Path),
				"Add button is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Require_Refreshbtn_Path),
				"Refresh button is not displays");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_StateTitle_Path),
				"State Rules header is not displays");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Rules_StateModefield_Path).trim().contains("Mode"),
				"Mode field is not displayed in the State Rules");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Rules_StateMode_ExcRadio_Path),
				"Exclude radio button is selected in State Rules");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Rules_StateMode_IncRadio_Path),
				"Include radio button is not selected in State Rules");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_States_Drop_Path, "value").contains("Select"),
				"- Select - is not displays in the  'States' dropdown");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_FirmMode_Path),
				"Firm Rules header is not displays");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Rules_FirmModefield_Path).trim().contains("Mode"),
				"Mode field is not displayed in the Firm Rules");
		softAssert.assertFalse(Element_Is_selected(Xpath, SB_MI_Rules_FirmMode_ExcRadio_Path),
				"Exclude radio button is selected in Firm Rules");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Rules_FirmMode_IncRadio_Path),
				"Include radio button is not selected in Firm Rules");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_Firms_Drop_Path, "value").contains("Select Firm(s)"),
				"- Select Firm(s)- is not displays in the  'Firm' dropdown");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_Maxordertitle_Path),
				"Max Order Quantity details header is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxorderFirstpage_Path),
				"First page is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxorderPrevpage_Path),
				"Previous page is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxorderNextpage_Path),
				"Next page is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Rules_MaxorderLastpage_Path),
				"Last page is not displays");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_MaxorderPagesize_Path, "value").contains("10"),
				"10 is not displayed in the page size - Rules tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "1")), "1 is not  is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_Save_Path),
				"Save button is not displayed - Rules tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Back_Path), "Back button is not displayed - Rule tab");

		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_MetricsHeader_Path), "Metrics tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_UnitsonHand_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in  Units on Hand : ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_UnitsAvailable_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Units Available : ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_Allocated_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in  Units Allocated : ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_BackOrderQTY_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Back Order QTY :  ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_MTD_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in MTD Usage :  ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_YTD_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in YTD Usage :  ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_AverageMonthlyUsage_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Average Monthly Usage :  ");
		softAssert.assertTrue(Get_Text(Xpath, SB_MI_Metrics_AverageMonthlyDownloads_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Average Monthly Downloads :  ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Back_Path),
				"Back button is not displayed - Metrics tab");

		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistoryTitle_Path),
				"Change history tab is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_Date_Path),
				"Date Column is not displayed in the Change history tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_User_Path),
				"User Column is not displayed in the Change history tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_Action_Path),
				"Action Column is not displayed in the Change history tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_Firstpage_Path),
				"First page is not display - Change history tabs");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_Prevpage_Path),
				"Previous page is not displays - Change history tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_Nextpage_Path),
				"Next page is not displays - Change history tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_ChangeHistory_Lastpage_Path),
				"Last page is not displays - Change history tab");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_ChangeHistory_Pagesize_Action_Path, "value").contains("10"),
				"10 is not displayed in the page size - Change history tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "1")), "1 is not  is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Back_Path),
				"Back button is not displayed - Change history tab");

		Click(Xpath, SB_MI_PageflexTab_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflextitle_Path), "Pageflex is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Pageflex_Jobnamefield_Path, "value").isEmpty(),
				"Pageflex Job Name' field is not empty");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Copyinveoptions_Path),
				"Copy Inventpry option  header is not displays in pageflex");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Copyfrom_Path),
				"Copy From is not displays in pageflex");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_CopyFromInventory_Path),
				"Copy From Inventory is not displays in pageflex");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Copybtn_Path),
				"Copy button is not displays in pageflex");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Bulkdelete_Path),
				"Bulk delete is not displays in pageflex");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_PageflexInventory_Path),
				"Inventpry option  header is not displays in pageflex");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_PageflexOptiontype__Path),
				"Option Type column is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_FriendlyName_Path),
				"Friendly Namecolumn is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_FieldGroup_Path),
				"Field Group column is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_MaxLength_Path),
				"Max Length column is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Visible_Path),
				"	Visible column is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Required_Path),
				"Required column is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Active_Path),
				"Active column is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Addnewrecord_Path),
				"Add new Record is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pageflex_Refresh_Path),
				"Refresh is not displays in the 'Inventory Options' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Gen_Save_Path),
				"Save button is not displayed - Pageflex tab");
		softAssert.assertAll();

		/*
		 * SB_TC_2_4_3_3_5
		 * Validate that not filling out all required fields on Pricing Tab doesn't enable you to move to another tab
		 */

		Click(Xpath, SB_MI_Pricing_Path);
		Click(Xpath, SB_MI_Pricing_FlatpricingCk_Path);
		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Pricing_ProdcostError_Path),
				"Print Cost must be greater than zero! is not dispalyed");

		/*
		 * SB_TC_2_4_3_3_6
		 * Validate that filling out all required fields on Pricing tab enables you to move to another tab
		 */
		Type(Xpath, SB_MI_Pricing_Prodcost_Path, "0.01");
		Click(Xpath, SB_MI_Gen_Next_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Notifi_Title_Path),
				"Notification tab is not displayed - SB_TC_2_4_3_3_6");
		MI_Save();

	}

	@Test(priority = 14, enabled = true)
	public void SB_TC_2_4_3_3_7() throws InterruptedException, IOException {

		/*
		 * Validate that a part can be obsolete
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(CreatePart));
		Switch_New_Tab();
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Click(Xpath, SB_LP_Logout_Path);
		login(SBUsername, SBPassword);
		ManageInventoryNavigation();
		Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, SB_MI_SearchresultsEdit(CreatePart)),
				"Obsoleted part is displayed");
	}

	@Test(priority = 15, enabled = true)
	public void SB_TC_2_4_3_3_8andSB_TC_2_4_3_3_9() throws InterruptedException, IOException {

		/*
		 * Validate that a part can be unobsolete
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("Obsolete"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(CreatePart));
		Switch_New_Tab();
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		MI_Save();
		Click(Xpath, SB_LP_Logout_Path);
		login(SBUsername, SBPassword);
		ManageInventoryNavigation();
		Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_SearchresultsEdit(CreatePart)),
				"UnObsoleted part is not displayed");

		/*
		 * SB_TC_2_4_3_3_9
		 * Validate appropriate changes are recorded in Change History
		 */
		Closealltabs();
		Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("Obsolete"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(CreatePart));
		Switch_New_Tab();
		Click(Xpath, SB_MI_ChangeHistoryTab_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "Reactivating Part")),
				"Reactivating Part is not displayed");
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();

	}

	@Test(priority = 16, enabled = true)
	public void SB_TC_2_4_3_4_1andSB_TC_2_4_3_4_2andSB_TC_2_4_3_4_3() throws InterruptedException, IOException {

		/*
		 * Validate when copying a part the original part name appears in predecessor field and part number field is able to be edited
		 */

		CreateNewpart(QA_COPY_NEWITEM_1);
		ManageInventoryNavigation();
		Type(Xpath, SB_MI_SI_StockNo_Path, QA_COPY_NEWITEM_1);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsCopy(QA_COPY_NEWITEM_1));
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Copyalert_Path), "Copy Alert is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_CopyalertTitle_Path),
				"'Copy?' is not displayed in the Copy pop up");
		Click(Xpath, SB_MI_CopyalertOK_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Gen_Save_Path);
		Type(Xpath, SB_MI_Gen_Stockno_Path, CreatePart);
		Assert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtPredecessor_Path, "value").equalsIgnoreCase(QA_COPY_NEWITEM_1),
				"'Predecessor' field is not pre-populated with <QA_COPY_NEWITEM_1>from original part");

		Type(Xpath, SB_MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"));
		Select_lidropdown(SB_MI_Gen_Viewability_Arrow_Path, Xpath, "Orderable");
		MI_Save();
		Reporter.log(CreatePart + " is Created new piece");
		Createdpartlist.add(CreatePart);

		/*
		 * SB_TC_2_4_3_4_2
		 * Validate that appropriate fields copy over from previous part
		 */

		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtDescription_Path, "value").toUpperCase().contains("QA Test"),
				"'Description' field is not pre-populated with <QA Test>from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtKeyWords_Path, "value").toUpperCase().contains("THIS IS A QA TEST "),
				"'Title' field is not pre-populated with <THIS IS A QA TEST >from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_txtRevisionDate_Path, "value").contains(Get_Todaydate("MM/dd/YYYY")),
				"Today's date is not populated in the Revision fields");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_InventoryType_DropDown_Path, "value").equalsIgnoreCase("POD"),
				" 'Inventory Type' drop-down is not pre-populated with <Print On-Demand> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_UnitsPerPack_Path, "value").equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Literaturetype_Path, "value").equalsIgnoreCase("Ad"),
				"'Lietrature Type' drop-down is not pre-populated with<Brochure> from original part");
		softAssert.assertTrue(
				Get_Attribute(Xpath, SB_MI_Gen_ProductCategory_List_Input_Path, "value")
						.equalsIgnoreCase("AdvanceDesigns"),
				"'Product Category' drop-down is not pre-populated with <AdvanceDesigns> from original part");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Gen_Channel_Input_Path, "value").equalsIgnoreCase("Bank"),
				"'Channel' drop-down is not pre-populated with <Bank> from original part");
		softAssert.assertTrue(Element_Is_selected(Xpath, SB_MI_Gen_chkFulfillment_Path),
				"Fulfillment is not checked off");

		/*
		 * SB_TC_2_4_3_4_3
		 * Validate appropriate changes are recorded in change history
		 */

		Click(Xpath, SB_MI_ChangeHistoryTab_Path);
		Wait_ajax();
		Thread.sleep(3000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "FormNumber changed from")),
				"FormNumber changed from 'Copy Of QA_COPY_NEWITEM_1 to is not displayed in the change hisory tab");
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		MI_Save();
		Reporter.log(CreatePart + " is Obsoleted");
		Click(Xpath, SB_LP_Logout_Path);
		login(SBUsername, SBPassword);
		ManageInventoryNavigation();
		Type(Xpath, SB_MI_SI_StockNo_Path, CreatePart);
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, SB_MI_SearchresultsEdit(CreatePart)),
				"Obsoleted part is displayed");
		softAssert.assertAll();
	}

	@Test(priority = 1, enabled = false)
	public void SB_TC_2_4_3_4_4() throws InterruptedException, IOException {

		/*
		 * Validate a part can be replaced
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, qa_kitonfly_4);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(qa_kitonfly_4));
		Switch_New_Tab();
		Click(Xpath, SB_MI_Kitting_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Kitting_Additembtn_Path);
		Click(Xpath, SB_MI_Kittingitemviewdetail(QA_Replace1));
		Switch_New_Tab2();
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ReplaceType_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("Date"));
		Type(Xpath, SB_MI_Rules_ReplaceDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Type(Xpath, SB_MI_Rules_ReplaceWith_Field_Path, QA_REPLACE2);
		Type(Xpath, SB_MI_Rules_ReplaceDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Type(Xpath, SB_MI_Rules_ReplaceWith_Field_Path, QA_REPLACE2);
		MI_Save();
		Closealltabs();
		Click(Xpath, SB_LP_Logout_Path);

	}

	@Test(priority = 17, enabled = false)
	public void SB_TC_2_4_3_4_4_Continuation() throws InterruptedException, IOException {

		/*
		 * Validate a part can be replaced
		 */

		Type(Xpath, SB_MI_SI_StockNo_Path, qa_kitonfly_4);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(qa_kitonfly_4));
		Switch_New_Tab();
		Click(Xpath, SB_MI_Kitting_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Kitting_Additembtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Kittingitemlist(QA_REPLACE2)),
				"QA_Replace_2 is not displayed");
		Click(Xpath, SB_MI_KittingitemRemove(QA_REPLACE2));
		Closealltabs();
		ManageInventoryNavigation();
		Type(Xpath, SB_MI_SI_StockNo_Path, QA_Replace1);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(QA_Replace1));
		Switch_New_Tab();
		Click(Xpath, SB_MI_RulesTab_Path);
		Click(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		Click(Xpath, SB_MI_Rules_UnObsoleteNowbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_MI_Rules_ObsoleteNowbtn_Path);

		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ReplaceType_Path, "value").equalsIgnoreCase("None"),
				"None is not displays in replace type");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ReplaceWith_Field_Path, "value").isEmpty(),
				"Replace With Field is not empty");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ReplaceDate_Path, "value").isEmpty(),
				"Replace Date Field is not empty");
		softAssert.assertTrue(Get_Attribute(Xpath, SB_MI_Rules_ObsoleteDate_Path, "value").isEmpty(),
				"Obsolete Date Field is not empty");
		softAssert.assertAll();
		MI_Save();
		Closealltabs();
		Click(Xpath, SB_LP_Logout_Path);
		login(SBUsername, SBPassword);
		ManageInventoryNavigation();
		Type(Xpath, SB_MI_SI_StockNo_Path, qa_kitonfly_4);
		Click(Xpath, SB_MI_SI_Active_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, SB_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Wait_ajax();
		Click(Xpath, SB_MI_SearchresultsEdit(qa_kitonfly_4));
		Switch_New_Tab();
		Click(Xpath, SB_MI_Kitting_Path);
		Type(Xpath, SB_MI_Kitting_Stockno_Path, QA_Replace1);
		Select_lidropdown(SB_MI_Kitting_Type_Path, Xpath, "Component");
		Select_lidropdown(SB_MI_Kitting_Location_Path, Xpath, "Inside");
		Type(Xpath, SB_MI_Kitting_Qty_Path, "1");
		Click(Xpath, SB_MI_Kitting_Perkitck_Path);
		Type(Xpath, SB_MI_Kitting_Sortorder_Path, "2");
		Click(Xpath, SB_MI_Kitting_Additembtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_MI_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_MI_Kittingitemlist(QA_Replace1)),
				QA_Replace1 + " is not displayed");
		MI_Save();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException, IOException {

		Closealltabs();
		softAssert = new SoftAssert();
		login(SBUsername, SBPassword);
		ManageInventoryNavigation();

	}

	@AfterMethod
	public void afterMethod() {
	}

	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
		System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
	}

	@AfterClass
	public void afterClass() {

	}

	@BeforeSuite
	public void beforeSuite() {
	}

}
