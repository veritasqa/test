package com.SecurityBenefit.RegressionTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.SecurityBenefit.Base.BaseTestSecurityBenefit;

public class Change_Request extends BaseTestSecurityBenefit {

	@Test(priority = 3, enabled = true)
	public void SB_TC_2_3_1_1() throws InterruptedException {

		// Verify Change requests page opens with Veritas/Client tickets search
		// grid

		Click(Xpath, LP_Changereq_path);
		// NavigateMenu(LP_Admin_path, LP_Support_path);
		ExplicitWait_Element_Clickable(Xpath, SB_CT_Addticketicon_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Veritasticketitle_Path),
				"Support : Veritas Tickets' grid is not displayed on the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_TicketNotxt_Path), " 'Ticket #' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_OrderNotxt_Path), " 'order #'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_SubCattxt_Path), " 'Sub category'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Stocknotxt_Path),
				" 'Product code'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Submittedbytxt_Path),
				" 'Submitted by'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Updatedontxt_Path),
				" 'Updated on'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Teamtxt_Path), " 'Team'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Statustxt_Path), " 'Status'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_Clientticketitle_Path),
				"Support : Client Tickets' grid is not displayed on the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_TicketNotxt_Path),
				" 'Ticket #' column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_OrderNotxt_Path),
				" 'order #'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_SubCattxt_Path),
				" 'Sub category'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_Stocknotxt_Path),
				" 'Product code'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_Submittedbytxt_Path),
				" 'Submitted by'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_Updatedontxt_Path),
				" 'Updated on'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_Teamtxt_Path), " 'Team'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_CT_Statustxt_Path),
				" 'Status'column is not displayed in CT");
		softAssert.assertAll();

	}

	@Test(priority = 1, enabled = true)
	public void SB_TC_2_3_2_1() throws InterruptedException, AWTException {

		// Add a new Change Request ticket in Veritas section
		Click(Xpath, LP_Changereq_path);
		// NavigateMenu(LP_Admin_path, LP_Support_path);
		Click(Xpath, SB_VT_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_CST_Createbtn_Path);
		Select_DropDown_VisibleText(Xpath, SB_CST_Categorydd_Path, "Other");
		Thread.sleep(2000);
		Type(Xpath, SB_CST_Commentfield_Path, "Test");
		Select_DropDown_VisibleText(Xpath, SB_CST_SubCategorydd_Path, "Other");
		Click(Xpath, SB_CST_Createbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_UpdateTicketbtn_Path),
				"new support ticket page is not displayed");
		Supporttickenumber = Get_Text(Xpath, SB_STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		Reporter.log(Supporttickenumber);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_Ticketno_Path), "Ticket # is not displayed");
		Assert.assertTrue(Get_DropDown(Xpath, SB_STM_Category_Path).equalsIgnoreCase("Other"),
				"'Other' is not displayed for 'Category' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, SB_STM_SubCat_Path).equalsIgnoreCase("Other"),
				"'Other' is not displayed for 'Sub Category' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, SB_STM_Assignedto_Path).equalsIgnoreCase("Veritas"),
				"'Veritas' is not displayed for 'Assigned to' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, SB_STM_Veritasteam_Path).equalsIgnoreCase("Fulfillment"),
				"'Fulfillment' is not displayed for 'Veritas Team to' drop down");
		Assert.assertTrue(Get_DropDown(Xpath, SB_STM_Clientteam_Path).equalsIgnoreCase("Inventory Admin"),
				"'Inventory Admin' is not displayed for 'Client Team to' drop down");
		Assert.assertTrue(Get_Text(Xpath, SB_STM_Comment1_Path).equalsIgnoreCase("Test"),
				"'Test' is not displayed for 'Comment' column");

	}

	@Test(priority = 3, enabled = true, dependsOnMethods = "SB_TC_2_3_2_1")
	public void SI_TC_2_3_2_2ANDSI_TC_2_3_2_3() throws InterruptedException, AWTException {

		// Validate user is able to update the Ticket on Edit/View Page

		Robot robot = new Robot();
		Click(Xpath, LP_Changereq_path);
		// NavigateMenu(LP_Admin_path, LP_Support_path);
		Type(Xpath, SB_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Click(Xpath, SB_VT_Edit1_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, SB_STM_Ticketno_Path).trim().equalsIgnoreCase(Supporttickenumber),
				" the ticket is not opened and displayed with the <Ticket #> displayed on the top right side of the page ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_NewStatus_Path), " 'New' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_Inprogress_Path),
				" 'Inprogress' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_Hold_Path), " 'Hold' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_Closedstatus_Path),
				" 'Closed' button is not displayed");
		softAssert.assertAll();
		Click(Xpath, SB_STM_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_STM_Createcomment_Path);
		Type(Xpath, SB_STM_Commenttext_Path, "QA Test");
		// Type(Xpath, STM_Attachment_Path, SI_TC_2_3_1_3_2_file);

		// Click(Xpath, STM_Attachment_Path);
		// Fileupload(SI_TC_2_3_1_3_2_file);
		// ExplicitWait_Element_Visible(Xpath, ST_Remove_Path);
		Click(Xpath, SB_STM_Createcomment_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SB_Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, SB_STM_Comment1_Path).equalsIgnoreCase("QA Test"),
				"'QA Test' is not displayed for 'Comment' column");
		softAssert.assertTrue(
				Get_Text(Xpath, SB_STM_User1_Path).toLowerCase().equalsIgnoreCase(UserFName + " " + UserLName),
				"'QA Test' is not displayed for 'Comment' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_Createdate1_Path), "Create Date is not displayed");
		// softAssert.assertTrue(Element_Is_Displayed(Xpath,
		// STM_Attachment1_Path),
		// " 'File' is not displayed under 'Attachment' column");
		softAssert.assertAll();
		Click(Xpath, SB_STM_Inprogress_Path);
		ExplicitWait_Element_Visible(Xpath, SB_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

		// Close the ticket and verify the ticket does not show up on the
		// Support page
		Click(Xpath, LP_Changereq_path);
		// NavigateMenu(LP_Admin_path, LP_Support_path);
		Type(Xpath, SB_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Assert.assertEquals("In-Progress", Get_Text(Xpath, SB_VT_Status1_Path),
				"'In-Progress' is not displayed under 'Status' column");
		Click(Xpath, SB_VT_Edit1_Path);
		ExplicitWait_Element_Clickable(Xpath, SB_STM_UpdateTicketbtn_Path);
		Click(Xpath, SB_STM_Closedstatus_Path);
		
		ExplicitWait_Element_Visible(Xpath, SB_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SB_STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");
		Click(Xpath, LP_Changereq_path);
		// NavigateMenu(LP_Admin_path, LP_Support_path);
		Type(Xpath, SB_TicketNotxt_Path, Supporttickenumber);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Assert.assertEquals("Closed", Get_Text(Xpath, SB_VT_Status1_Path),
				"'Closed' is not displayed under 'Status' column");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(SBUsername, SBPassword);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		Click(Xpath, SB_LP_Home_path);

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
