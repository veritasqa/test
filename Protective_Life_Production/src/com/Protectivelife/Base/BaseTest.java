package com.Protectivelife.Base;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.Protectivelife.Methods.AssertiveMethods;

public class BaseTest extends AssertiveMethods {

	@BeforeSuite(enabled = true)
	public void openbrowser() throws IOException, InterruptedException {

		Open_Browser(Browser, Browserpath);
		Open_Url_Window_Max(Production_URL, driver);
		// Open_Url_Window_Max(Production_URL, driver);
		
		// login(UserName,Password);
		Implicit_Wait(driver);
	}

	@AfterSuite(enabled = false)
	public void afterClass() {

		Cancelallorders();

		Close_Browser(driver);
		Quit_Browser(driver);

	}

	public void login(String UserName, String Password) throws IOException, InterruptedException {

		if (Is_Element_Present(locatorType, Username_Path, driver)) {
			Type(locatorType, Username_Path, UserName, driver);
			Type(locatorType, Password_Path, Password, driver);
			Click(locatorType, Login_btn_Path, driver);

			Assert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));
			ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

			Implicit_Wait(driver);
		}

		else {
			logout();
			Type(locatorType, Username_Path, UserName, driver);
			Type(locatorType, Password_Path, Password, driver);
			Click(locatorType, Login_btn_Path, driver);
			Assert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));
			ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

			Implicit_Wait(driver);
		}

	}

	public void logout() throws IOException, InterruptedException {

		Click(locatorType, Home_btn_Path, driver);
		softAssert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));
		Click(locatorType, Logout_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Login_btn_Path, driver);

	}

	public void Clearcart() {
		Click(locatorType, Clear_Cart_Path, driver);
		Thread_Sleep(10000);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
	}

	public void ContactSearch(String firname, String Laname) {

		Type(locatorType, FirstName_Path, firname, driver);
		Type(locatorType, LastName_Path, Laname, driver);

		Click(locatorType, Search_btn_Path, driver);

		ExplicitWait_Element_Visible(locatorType, Records_Per_Page_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType, Contact_Click_Path,
		// driver).equalsIgnoreCase(firname + " " + Laname),
		// "Contact Name is Mismatching");
		ExplicitWait_Element_Clickable(locatorType, ".//td[text()='" + firname + " " + Laname + "']", driver);

		Click(locatorType, ".//td[text()='" + firname + " " + Laname + "']", driver);

	}

	public void SSOLogin(String Crm_Agent_Login) throws InterruptedException {

		Open_Url_Window_Max(SSO_URL, driver);
		Type(locatorType, xml_textfield_Path, Crm_Agent_Login, driver);
		Click(locatorType, Login_btn_Path, driver);
		Thread.sleep(2000);
		Click(locatorType, TokenId_path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
	}
	/*
	 * 
	
	* public void ContactSearch(String firname, String Laname) {
	 * 
	 * Type(locatorType, FirstName_Path, firname, driver); Type(locatorType,
	 * LastName_Path, Laname, driver);
	 * 
	 * Click(locatorType, Search_btn_Path, driver);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Edit_btn_Path, driver);
	 * Assert.assertTrue(Get_Text(locatorType, Contact_Click_Path,
	 * driver).equalsIgnoreCase( firname + " " + Laname),
	 * "Contact Name is Mismatching");
	 * ExplicitWait_Element_Clickable(locatorType, Contact_Click_Path, driver);
	 * 
	 * Click(locatorType, Contact_Click_Path, driver);
	 * 
	 * }
	 */

	public void PlaceanOrder(String methodname) {
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		if (Element_Isselected(locatorType, Order_Confirmation_Checkbox_Path, driver)) {

		} else {
			Click(locatorType, Order_Confirmation_Checkbox_Path, driver);

		}
		if (Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver)) {
			Assert.assertTrue(Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver));
		} else {
			Click(locatorType, Shipped_Confirmation_Checkbox_Path, driver);

		}
		Click(locatorType, Next_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, SC_Loadingpanel_Path, driver);
		Thread_Sleep(1500);
		Click(locatorType, SC_DeliverydateCalender_Path, driver);
		Thread_Sleep(1500);
		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);
		Click(locatorType, SC_DeliverydateCalender_Path, driver);
		if (Is_Element_Present(locatorType, SC_Costcenterck_Path, driver)) {
			Click(locatorType, SC_Costcenterck_Path, driver);
			DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		} else {
			DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		}
		Type(locatorType, SC_Shipcomments_Path, "This is a QA piece", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		// ExplicitWait_Element_Not_Visible(locatorType,
		// OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);

		System.out.println("Order Number placed in method  " + methodname + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + methodname + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);
	}

	public void Datepicker(String Month, String Ok_Btn) {

		Click(locatorType, Month, driver);
		Click(locatorType, ".//a[text()='" + Get_Futuredate("MMM") + "']", driver);
		Thread_Sleep(500);
		Click(locatorType, ".//a[text()='" + Get_Futuredate("YYYY") + "']", driver);
		Thread_Sleep(500);
		Click(locatorType, Ok_Btn, driver);
		Thread_Sleep(500);
		Click(locatorType, ".//a[text()='" + Get_Futuredate("d") + "']", driver);
	}
	
	public void Datepickercustom(String Month, String Ok_Btn, String Selectmonth,String SelectDate, String Selectyear ) {

		Click(locatorType, Month, driver);
		Click(locatorType, ".//a[text()='" + Selectmonth + "']", driver);
		Thread_Sleep(500);
		Click(locatorType, ".//a[text()='" + Selectyear + "']", driver);
		Thread_Sleep(500);
		Click(locatorType, Ok_Btn, driver);
		Thread_Sleep(500);
		Click(locatorType, ".//a[text()='" + SelectDate + "']", driver);
	}

	public void Cancelallorders() {

		// Open_Browser(Browser,Browserpath);
		Open_Url_Window_Max(Inventory_URL, driver);
		Type(locatorType, Inventory_Username_Path, Inventory_UserName, driver);
		Type(locatorType, Inventory_Passowrd_Path, Inventory_Password, driver);
		Click(locatorType, Inventory_Submit_Btn_Path, driver);

		for (String Onum : ordernum) {

			ExplicitWait_Element_Visible(locatorType, Inventory_OrderSearch_Path, driver);
			Click(locatorType, Inventory_OrderSearch_Path, driver);
			ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path, driver);
			Type(locatorType, Inventory_Ordernumber_Path, Onum, driver);
			Click(locatorType, Inventory_Search_Btn_Path, driver);
			Click(locatorType, Inventory_Edit_Btn_Path, driver);

			if (Get_Attribute(locatorType, Inventory_Order_Status, "Value", driver).equalsIgnoreCase("NEW")) {

				Click(locatorType, Inventory_Cancelorder_Btn_Path, driver);
				Alert_Accept(driver);

				Assert.assertTrue(Get_Attribute(locatorType, Inventory_Order_Status, "Value", driver)
						.equalsIgnoreCase("Cancelled"), "Order is not cancelled is not displayed");

			}
			Click(locatorType, Inventory_OrderSearch_Path, driver);
			ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path, driver);
		}

		Click(locatorType, Inventory_logout_btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, Inventory_Username_Path, driver);

	}

	public void Obsoleteparts() throws IOException, InterruptedException {

		login(UserName, Password);
		Hover(locatorType, Admin_btn_Path, driver);
		Click(locatorType, Manage_Inventory_btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Searchbtn_Path, driver);
		for (String Partlist : Createdpartlist) {
			Type(locatorType, MI_SI_FormNo_Path, Partlist, driver);
			Click(locatorType, MI_SI_Active_Icon_Path, driver);
			Thread_Sleep(1000);
			Click(locatorType, li_value("- Any -"), driver);
			Thread_Sleep(1000);
			Click(locatorType, MI_SI_Active_Icon_Path, driver);
			Thread_Sleep(1000);
			Click(locatorType, MI_Searchbtn_Path, driver);
			ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);

			if (Element_Is_Displayed(locatorType, MI_SearchresultsEdit(Partlist), driver)) {
				Click(locatorType, MI_SearchresultsEdit(Partlist), driver);
				Switch_New_Tab(driver);

				ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
				Click(locatorType, MI_RulesTab_Path, driver);
				ExplicitWait_Element_Clickable(locatorType, MI_Rules_MaxOrderQtyPerRole_Add_Path, driver);
				if (Element_Is_Displayed(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver)) {
					Click(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
					Click(locatorType, MI_Gen_Save_Path, driver);
					Click(locatorType, MI_Savepopup_Ok_Path, driver);
					Reporter.log(Partlist + " is Obsoleted");
					Closealltabs(driver);
					Hover(locatorType, Admin_btn_Path, driver);
					Click(locatorType, Manage_Inventory_btn_Path, driver);
					ExplicitWait_Element_Clickable(locatorType, MI_Searchbtn_Path, driver);

				}
			} else {
				Closealltabs(driver);
				Hover(locatorType, Admin_btn_Path, driver);
				Click(locatorType, Manage_Inventory_btn_Path, driver);
				ExplicitWait_Element_Clickable(locatorType, MI_Searchbtn_Path, driver);
			}

		}

	}

	public void AllstateAgentLogin(String Crm_Agent_Login) {

		Open_Url_Window_Max(SSO_URL, driver);
		Type(locatorType, xml_textfield_Path, Crm_Agent_Login, driver);
		Click(locatorType, Login_btn_Path, driver);
		Thread_Sleep(3000);
		Click(locatorType, TokenId_path, driver);
	}

}
