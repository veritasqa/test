package com.Protectivelife.OR;

public class InventoryManagement extends LandingPage {

	public String MI_SearchresultsCopy(String PartName) {

		return ".//*[text()='" + PartName + "']//following::td[7]/a";

	}

	public String MI_SearchresultsEdit(String PartName) {

		return ".//*[text()='" + PartName + "']//following::td[8]/a";

	}

	public String MI_Searchresults(String PartName) {

		return ".//*[text()='" + PartName + "']";

	}
	
	public String MI_Kittingitemviewdetail(String PartName) {

		return ".//*[text()='" + PartName + "']//following::td[15]/a";

	}

	public String MI_Kittingitemlist(String PartName) {

		return ".//*[text()='" + PartName + "']";

	}
	
	public String MI_KittingitemRemove(String PartName) {

		return ".//*[text()='" + PartName + "']//following::td[14]/a";

	}
	
	
	
	public static String OriginalWindow = "";
	
	// Create New Item Section

	public static final String MI_New_CreateNewTypeTitle_Path = ".//h1[text()='Create New Item']";
	public static final String MI_New_CreateNewType_Path = ".//*[@id='ctl00_cphContent_ddlNewType_Input']";
	public static final String MI_New_CreateNewType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlNewType_Arrow']";

	public static final String MI_New_POD_Path = "//*[@id='ctl00_cphContent_ddlNewType_DropDown']/div/ul/li[19]";

	public static final String MI_New_Add_Btn_Path = ".//a[text()='Add']";
	public static final String MI_New_FormNoToCopy_Path = ".//*[@id='ctl00_cphContent_ddlCopy_Input']";
	public static final String MI_New_Copy_Btn_Path = ".//a[text()='Copy']";

	// Search Items Section

	public static final String MI_SI_SearchItemsTitle_Path = ".//h1[text()='Search Items']";

	public static final String MI_SI_FormNo_Path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
	public static final String MI_SI_Title_Path = ".//*[@id='ctl00_cphContent_txtDescription']";
	public static final String MI_SI_Keyword_Path = ".//*[@id='ctl00_cphContent_txtKeyWord']";
	public static final String MI_SI_Madatoryfieldtext_Path = ".//*[@id='ctl00_cphContent_lblKeyWordInfo']";
	public static final String MI_SI_InventoryType_Path = ".//*[@id='ctl00_cphContent_ddlType_Input']";
	public static final String MI_SI_InventoryType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlType_Arrow']";
	public static final String MI_SI_CollateralType_Path = ".//*[@id='ctl00_cphContent_ddlDocumentType_Input']";
	public static final String MI_SI_CollateralType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlDocumentType_Arrow']";
	public static final String MI_SI_Category_Path = ".//*[@id='ctl00_cphContent_ddlCategory_Input']";
	public static final String MI_SI_Category_Icon_Path = ".//*[@id='ctl00_cphContent_ddlCategory_Arrow']";
	public static final String MI_SI_UserGroups_Path = ".//*[@id='ctl00_cphContent_ddlAudience_Input']";
	public static final String MI_SI_UserGroups_Icon_Path = ".//*[@id='ctl00_cphContent_ddlAudience_Arrow']";
	public static final String MI_SI_Active_Path = ".//*[@id='ctl00_cphContent_ddlActive_Input']";
	public static final String MI_SI_Active_Icon_Path = ".//*[@id='ctl00_cphContent_ddlActive_Arrow']";
	public static final String MI_SI_UnitsOnHand_Path = ".//*[@id='ctl00_cphContent_ddlUOH_Input']";
	public static final String MI_SI_UnitsOnHand_Icon_Path = ".//*[@id='ctl00_cphContent_ddlUOH_Arrow']";
	public static final String MI_SI_ProductArea_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_Input']";
	public static final String MI_SI_ProductArea_Icon_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_Arrow']";
	public static final String MI_SI_Programs_Path = ".//*[@id='ctl00_cphContent_ddlExtraFilter2_Input']";
	public static final String MI_SI_Programs_Icon_Path = ".//*[@id='ctl00_cphContent_ddlExtraFilter2_Arrow']";
	public static final String MI_Searchbtn_Path = ".//*[@id='ctl00_cphContent_btnSearch']";
	public static final String MI_Clearbtn_Path = ".//*[@id='ctl00_cphContent_btnClear']";
	public static final String MI_Copyalert_Path = ".//*[contains(@id,'RadWindowWrapper_confirm')]";
	public static final String MI_CopyalertOK_Path = "//span[@class='rwInnerSpan' and text()='OK']";
	public static final String MI_CopyalertCancel_Path = "//span[@class='rwInnerSpan' and text()='Cancel']";
	public static final String MI_CopyalertTitle_Path = "//em[contains(.,'Copy')]";
	public static final String MI_FormNumber1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[1]";
	public static final String MI_Status1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]";
	public static final String MI_Type1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[3]";
	public static final String MI_Title1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[4]";
	public static final String MI_Createdate1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[5]";
	public static final String MI_Onhand1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[6]";
	public static final String MI_Unitsavail1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[7]";
	public static final String MI_Edit1_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a";
	
	public static final String MI_Searchloadingpanel_Path =".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_Results']";
	public static final String MI_SI_FirstPage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String MI_SI_LastPage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String MI_SI_Nextpage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String MI_SI_Previouspage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String MI_SI_PageNavField_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[2]/a/span";
	public static final String MI_SI_PageSizeField_Path = ".//*[@id='ctl00_cphContent_Results_ctl00_ctl03_ctl02_PageSizeComboBox']/table/tbody/tr/td[1]";
	public static final String MI_SI_EmptySearchresult_Path = ".//div[text()='No records to display.']";
	

	// Edit Manage Inventory
	// General Tab

	public static final String MI_Gen_NewItem_header_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	public static final String MI_Gen_NewItem_headerpart_Path = ".//*[@id='ctl00_cphContent_lblHeader']";

	public static final String MI_Gen_General_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[1]/a/span/span/span";
	public static final String MI_Gen_Formnumber_Path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
	public static final String MI_Gen_Formnumber_Asterisk_Path = ".//*[@id='ctl00_cphContent_Label1']";
	public static final String MI_Gen_ProductCode_Path = ".//*[@id='ctl00_cphContent_txtAlias']";
	public static final String MI_Gen_txtRevisionDate_Path = ".//*[@id='ctl00_cphContent_txtRevisionDate']";
	public static final String MI_Gen_RevisionDate_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblRequiredRevisionDate']";
	public static final String MI_Gen_Obsolete_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_dateInput']";
	public static final String MI_Gen_ObsoleteCalender_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_popupButton']";
	public static final String MI_Gen_ObsoleteClock_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_timePopupLink']";
	public static final String MI_Gen_txtPredecessor_Path = ".//*[@id='ctl00_cphContent_txtPredecessor']";
	public static final String MI_Gen_txtTitle_Path = ".//*[@id='ctl00_cphContent_txtDescription']";
	public static final String MI_Gen_txtTitle_Asterisk_Path = ".//*[@id='ctl00_cphContent_Label17']";
	public static final String MI_Gen_txtDescription_Path = ".//*[@id='ctl00_cphContent_txtLongDescription']";
	public static final String MI_Gen_txtDescription_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblRequiredLongDescription']";
	public static final String MI_Gen_txtKeyWords_Path = ".//*[@id='ctl00_cphContent_txtKeyWords']";
	public static final String MI_Gen_txtKeyWords_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblRequiredKeyords']";
	public static final String MI_Gen_InventoryType_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlInventoryType_Arrow']";
	public static final String MI_Gen_InventoryType_DropDown_Path = ".//*[@id='ctl00_cphContent_ddlInventoryType_Input']";
	public static final String MI_Gen_InventoryType_Asterisk_Path = ".//*[@id='ctl00_cphContent_Label3']";
	public static final String MI_Gen_UnitsPerPack_Path = ".//*[@id='ctl00_cphContent_txtUnitsPerPack']";
	public static final String MI_Gen_UnitsPerPack_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblUnitsPerPackRequired']";
	public static final String MI_Gen_Collateraltype_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlDocumentType_Arrow']";
	public static final String MI_Gen_Collateraltype_Path = ".//*[@id='ctl00_cphContent_ddlDocumentType_Input']";
	public static final String MI_Gen_Collateraltype_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblRequiredDocumentType']";
	public static final String MI_Gen_FormCostCenter_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlFormCostCenter_Arrow']";
	public static final String MI_Gen_FormCostCenter_Path = ".//*[@id='ctl00_cphContent_ddlFormCostCenter_Input']";
	public static final String MI_Gen_FormCostCenter_Checkall_Path = ".//div[@id='ctl00_cphContent_ddlFormCostCenter_DropDown']//label[text()='Check All']//input[@type='checkbox']";
	public static final String MI_Gen_FormCostCenter_63208_AN_EDJ_Path = ".//*[@id='ctl00_cphContent_ddlFormCostCenter_DropDown']/div/ul/li[1]/label/input";
	public static final String MI_Gen_ProductLineType_List_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlExtraFilter1_Arrow']";
	public static final String MI_Gen_ProductLineType_List_Input_Path = ".//*[@id='ctl00_cphContent_ddlExtraFilter1_Input']";
	public static final String MI_Gen_ProductLineType_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblRequiredExtraFilter1']";
	public static final String MI_Gen_ProductLineType_Company_Path = ".//div[@id='ctl00_cphContent_ddlExtraFilter1_DropDown']//label[text()='Company']//input[@type='checkbox']";
	public static final String MI_Gen_ProductName_List_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_Arrow']";
	public static final String MI_Gen_ProductName_List_Input_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_Input']";
	public static final String MI_Gen_ProductName_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblRequiredProductCategory']";
	public static final String MI_Gen_ProductName_AdvantageChoice_Path = ".//div[@id='ctl00_cphContent_ddlProductCategory_DropDown']//label[text()='Advantage Choice UL']//input[@type='checkbox']";
	public static final String MI_Gen_ProductName_AssetBuildert_Path = ".//div[@id='ctl00_cphContent_ddlProductCategory_DropDown']//label[text()='Asset Builder']//input[@type='checkbox']";
	public static final String MI_Gen_Programs_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlExtraFilter2_Arrow']";
	public static final String MI_Gen_Programs_Input_Path = ".//*[@id='ctl00_cphContent_ddlExtraFilter2_Input']";
	public static final String MI_Gen_UserKitContainer_Path = ".//*[@id='ctl00_cphContent_chkIsUserKitContainer']";
	public static final String MI_Gen_BusinessLine_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_Arrow']";
	public static final String MI_Gen_BusinessLine_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_Input']";
	public static final String MI_Gen_BusinessLine_Asterisk_Path = ".//*[@id='ctl00_cphContent_lblFormOwnerAsterisk']";
	public static final String MI_Gen_Viewability_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Arrow']";
	public static final String MI_Gen_Viewability_Input_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Input']";
	public static final String MI_Gen_Viewability_Asterisk_Path = ".//*[@id='ctl00_cphContent_Label9']";
	public static final String MI_Gen_EffectiveDate_dateInput_Path = ".//*[@id='ctl00_cphContent_dtEffectiveDate_dateInput']";
	public static final String MI_Gen_EffectiveDate_Calender_Path = ".//*[@id='ctl00_cphContent_dtEffectiveDate_popupButton']";
	public static final String MI_Gen_EffectiveDate_Clock_Path = ".//*[@id='ctl00_cphContent_dtEffectiveDate_timePopupLink']";
	public static final String MI_Gen_ViewIn_Path = ".//*[@id='ctl00_cphContent_lblChkViewIn']";
	public static final String MI_Gen_chkFulfillment_Path = ".//*[@id='ctl00_cphContent_chkFulfillment']";
	public static final String MI_Gen_chkMarketing_Path = ".//*[@id='divContent clearfix']/div[5]/div[1]/table/tbody/tr[20]/td[2]/label[2]";
	public static final String MI_Gen_chkBulkOrder_Path = ".//*[@id='ctl00_cphContent_chkIsBulk']";
	public static final String MI_Gen_txtNotes_Path = ".//*[@id='ctl00_cphContent_txtNotes']";
	public static final String MI_Gen_Save_Path = ".//*[@id='ctl00_cphContent_btnSave']";
	public static final String MI_Gen_Cancel_Path = ".//a[text()='Cancel']";
	public static final String MI_Gen_Next_Path = ".//*[@id='ctl00_cphContent_btnNext']";
	public static final String MI_Gen_CategoryTitle_Path = ".//*[@id='divContent clearfix']/div[8]/h1";
	public static final String MI_Gen_Caegorysetion_Path = ".//*[@id='divContent clearfix']/div[8]/div[1]";
	public static final String MI_Gen_ItemUsagesection_Path = ".//*[@id='divContent clearfix']/div[11]/h1";
	
	public static final String MI_Gen_Category_Annuties_Path=".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[1]/div/label/input";
	public static final String MI_Gen_Usergrps_Path=".//span[text()='User Group']";
	public static final String MI_Gen_Usergrps_Allstate_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[1]/div/label/input";
	public static final String MI_Gen_Usergrps_BGACheckbox_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/div/label/input";
	public static final String MI_Gen_Usergrps_IndependentAgent_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[3]/div/label/input";
	public static final String MI_Gen_Usergrps_Advisor_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[4]/div/label/input";
	public static final String MI_Gen_Usergrps_DLC_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[5]/div/label/input";
	public static final String MI_Gen_Usergrps_RVP_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[6]/div/label/input";
	public static final String MI_Gen_Usergrps_RDADM_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[7]/div/label/input";
	public static final String MI_Gen_Usergrps_Hybrid_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[8]/div/label/input";
	public static final String MI_Gen_Usergrps_Salesdesk_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[9]/div/label/input";
	public static final String MI_Gen_Usergrps_Marketing_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[10]/div/label/input";
	public static final String MI_Gen_Usergrps_Sysadmin_Path=".//*[@id='ctl00_cphContent_rtvAudience']/ul/li[11]/div/label/input";
	public static final String MI_Gen_Usergrps_Checkbox_Path=".//*[@id='divContent clearfix']/div[5]/div[1]/table/tbody/tr[21]";
	
	// Error messages
	public static final String MI_Gen_Err_FormNumber_Path=".//*[@id='ctl00_cphContent_cfvFormNumber']";
	public static final String MI_Gen_Err_RevisionDate_Path=".//*[@id='ctl00_cphContent_cfvRevDate']";
	public static final String MI_Gen_Err_Title_Path=".//*[@id='ctl00_cphContent_cfvDescription']";
	public static final String MI_Gen_Err_Description_Path=".//*[@id='ctl00_cphContent_cfvLongDescription']";
	public static final String MI_Gen_Err_KeyWords_Path=".//*[@id='ctl00_cphContent_cfvKeyWords']";
	public static final String MI_Gen_Err_CollateralType_Path=".//*[@id='ctl00_cphContent_cfvDocumentType']";
	public static final String MI_Gen_Err_ProductName_Path=".//*[@id='ctl00_cphContent_cfvProductCategory']";
	public static final String MI_Gen_Err_ProductLine_Path=".//*[@id='ctl00_cphContent_cfvExtraFilter1']";
	public static final String MI_Gen_Err_BusinessLine_Path=".//*[@id='ctl00_cphContent_CustomValidator1']";
	public static final String MI_Gen_Err_Viewability_Path=".//*[@id='ctl00_cphContent_cfvViewability']";
	
	
	public static final String MI_Savepopup_Path = ".//*[@id='ctl00_cphContent_NavigationBar_divOverlay']/div/div/div[2]/div";
	public static final String MI_Savepopup_Message_Path = ".//*[@id='ctl00_cphContent_NavigationBar_lblDialog']";
	public static final String MI_Savepopup_Ok_Path = "//*[@id='ctl00_cphContent_NavigationBar_btnOk']";

	public static final String MI_ObsCalenderMonth_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_popupButton']";
	public static final String MI_ObsCalenderOK_Path = ".//*[@id='rcMView_OK']";

	// Attachments Tab
	public static final String MI_AttachmentTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[2]/a/span/span/span";
	public static final String MI_AttachmentTitle_Path = ".//*[@id='divContent clearfix']/div[3]/h1";
	public static final String MI_Attach_PrdThumb_Path = ".//legend[text()='Product Thumbnail']";
	public static final String MI_Attach_PrdThumb_Browse_Path = ".//*[@id='ctl00_cphContent_uplThumbnail']";
	public static final String MI_Attach_PrdThumb_Upload_Path = ".//*[@id='ctl00_cphContent_btnThumbnail']";
	public static final String MI_Attach_PrdThumb_Remove_Path = ".//*[@id='ctl00_cphContent_btnRemoveThumbnail']";
	public static final String MI_Attach_PrdThumb_ViewThumb_Path = ".//*[@id='ctl00_cphContent_lnkThumbnail']";
	public static final String MI_Attach_Onlinesample_Path = ".//legend[text()='Online sample']";
	public static final String MI_Attach_Onlinesample_Browse_Path = ".//*[@id='ctl00_cphContent_uplSample']";
	public static final String MI_Attach_Onlinesampl_Upload_Path = ".//*[@id='ctl00_cphContent_btnSample']";
	public static final String MI_Attach_FINRAletter_Path = ".//legend[text()='FINRA letter']";
	public static final String MI_Attach_FINRAletter_BrowsePath = ".//*[@id='ctl00_cphContent_uplFINRA']";
	public static final String MI_Attach_FINRAletter_Upload_Path = ".//*[@id='ctl00_cphContent_btnFINRA']";
	public static final String MI_Attach_PrintReady_Path = ".//h1[text()='Print Ready Pdf']";
	public static final String MI_Attach_PrintReady_Browse_Path = ".//*[@id='PreflightUpload_fuPdf']";
	public static final String MI_Attach_PrintReady_Upload_Path = ".//*[@id='PreflightUpload_btnUpload']";
	public static final String MI_Attach_PrintReady_View_Path = ".//*[@id='PreflightUpload_lnkViewPdf']";
	public static final String MI_Attach_PrintReady_Remove_Path = ".//*[@id='PreflightUpload_lnkRemovePdf']";
	public static final String MI_Attach_PrintReady_Password_Path = ".//*[@id='PreflightUpload_txtPassword']";
	
	public static final String MI_Attach_PODprintSpecs_Path = ".//h1[text()='POD Print Specs']";
	public static final String MI_Attach_Colors_Path = ".//*[@id='ctl00_cphContent_ddlColors_Input']";
	public static final String MI_Attach_Pagesize_Path = ".//*[@id='ctl00_cphContent_ddlPageSize_Input']";
	public static final String MI_Attach_Stock_Path = ".//*[@id='ctl00_cphContent_ddlStock_Input']";
	public static final String MI_Attach_Coverstock_Path = ".//*[@id='ctl00_cphContent_ddlCoverStock_Input']";
	public static final String MI_Attach_Finish_Path = ".//*[@id='ctl00_cphContent_ddlFinish_Input']";
	public static final String MI_Attach_Coating_Path = ".//*[@id='ctl00_cphContent_ddlCoating_Input']";
	public static final String MI_Attach_Noofpages_Path = ".//*[@id='ctl00_cphContent_txtImpressions']";
	public static final String MI_Attach_Binding_Path = ".//*[@id='ctl00_cphContent_ddlBinding_Input']";
	public static final String MI_Attach_PrintMethod_Path = ".//*[@id='ctl00_cphContent_ddlPrintMethod_Input']";
	public static final String MI_Attach_Printype_Path = ".//*[@id='ctl00_cphContent_ddlPrintType_Input']";
	public static final String MI_Attach_Flatsize_Path = ".//*[@id='ctl00_cphContent_txtSheetSize']";
	public static final String MI_Attach_IMCOposition_Path = ".//*[@id='ctl00_cphContent_txtComposition']";
	public static final String MI_Attach_Finishsize_Path = ".//*[@id='ctl00_cphContent_txtFinishSize']";
	public static final String MI_Attach_PCSPrint_Path = ".//*[@id='ctl00_cphContent_ddlPCSPrintType_Input']";
	public static final String MI_Attach_ClickCost_Path = ".//*[@id='ctl00_cphContent_txtClickCost']";
	public static final String MI_Attach_Nextbtn_Path = ".//*[@id='ctl00_cphContent_btnNext']";
	public static final String MI_Attach_backbtn_Path = ".//*[@id='ctl00_cphContent_btnBack']";

	// Pricing Tab

	public static final String MI_Pricing_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[3]/a/span/span/span";
	public static final String MI_Pricing_UnitPrice_Path = ".//*[@id='ctl00_cphContent_pnlChargeBackCost']/div[1]/h1";
	public static final String MI_Pricing_ChargebackCost_Path = ".//*[@id='ctl00_cphContent_txtChargeBackCost']";
	public static final String MI_Pricing_PricingDetails_Path = ".//*[@id='ctl00_cphContent_pnlPricingDetails']/div[1]/h1";
	public static final String MI_Pricing_Typeck_Path = ".//*[@id='ctl00_cphContent_optType_0']";
	public static final String MI_Pricing_Approvedck_ChargeItem_Path = ".//*[@id='ctl00_cphContent_chkPrintCostApproved']";
	public static final String MI_Pricing_Tieredck_ChargebackCost_Path = ".//*[@id='ctl00_cphContent_optType_1']";
	public static final String MI_Pricing_FlatPricing_Path = ".//*[@id='ctl00_cphContent_pnlFlat']/div[1]/h1";
	public static final String MI_Pricing_NoCostItemCk_Path = ".//*[@id='ctl00_cphContent_chkNoCostItem']";
	public static final String MI_Pricing_Prodcost_Path = ".//*[@id='ctl00_cphContent_txtPrintCost']";
	public static final String MI_Pricing_ProdcostError_Path = ".//span[text()='Print Cost must be greater than zero!']";
	
	// Notification Tab
	public static final String MI_Notification_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[4]/a/span/span/span";
	public static final String MI_Notifi_Title_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	public static final String MI_Notifi_Addresses_Path = ".//*[@id='divContent clearfix']/div[8]/h1";
	public static final String MI_Notifi_Notificationck_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	public static final String MI_Notifi_AtQuantityLevel_Path = ".//*[@id='ctl00_cphContent_txtQuantity']";
	public static final String MI_Notifi_ReOrderQuantity_Path = ".//*[@id='ctl00_cphContent_txtOrderQuantity']";
	public static final String MI_Notifi_ReceiptNotificationck_Path = ".//*[@id='ctl00_cphContent_chkEmailAutoReceipt']";
	public static final String MI_Notifi_Email_Path = ".//td[text()='ver.qa@rrd.com']";
	public static final String MI_Notifi_Email2_Path = ".//td[text()='ver.protectivelife@rrd.com']";

	// Rules Tab

	public static final String MI_RulesTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span";
	public static final String MI_RulesTab_Allowbackorder_Path = ".//*[@id='ctl00_cphContent_chkBackorder']";
	public static final String MI_RulesTab_Removeduplicte_Path = ".//*[@id='ctl00_cphContent_chkRemoveDuplicates']";
	public static final String MI_Rules_Title_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	public static final String MI_Rules_MaxorderTitle_Path = ".//h1[text()='Max Order Quantity']";
	public static final String MI_Rules_DefaultMaxOrderQty_Path = ".//*[@id='ctl00_cphContent_txtMaxOrderQty']";
	public static final String MI_Rules_NoMax_Path = "//td[contains(.,'0 = No Max')]";
	public static final String MI_Rules_DefaultMaxOrderQtyFrq_Path = ".//*[@id='ctl00_cphContent_ddlMaxOrderQuantityFrequency_Input']";
	public static final String MI_Rules_DefaultMaxOrderQtyFrq_icon_Path = ".//*[@id='ctl00_cphContent_ddlMaxOrderQuantityFrequency_Arrow']";
	public static final String MI_Rules_MaxOrderQtyPerRole_Field_Path = ".//*[@id='ctl00_cphContent_txtMaxOrderQtyRole']";
	public static final String MI_Rules_MaxOrderQtyPerRole_Drop_Path = ".//*[@id='ctl00_cphContent_cboMaxOrderQtyRole_Input']";
	public static final String MI_Rules_MaxOrderQtyPerRole_Add_Path = ".//*[@id='btnAddMaxOrderQty']";
	public static final String MI_Rules_AllowBackordersRadio_Path = ".//*[@id='ctl00_cphContent_chkBackorder']";
	public static final String MI_Rules_ChildItem_Path = ".//*[@id='ctl00_cphContent_chkBackorder']";
	public static final String MI_Rules_Superseding_Path = ".//h1[text()='Superseding']";
	public static final String MI_Rules_ReplaceType_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Input']";
	public static final String MI_Rules_ReplaceType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Arrow']";
	public static final String MI_Rules_ReplaceWith_Field_Path = ".//*[@id='ctl00_cphContent_ddlReplaceWith_Input']";
	public static final String MI_Rules_ReplaceDate_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_dateInput']";
	public static final String MI_Rules_ReplaceDateCalender_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_popupButton']";
	public static final String MI_Rules_ReplaceDateTime_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_timePopupLink']";
	public static final String MI_Rules_ObsoleteDate_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_dateInput']";
	public static final String MI_Rules_ObsoleteDateCaleder_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_popupButton']";
	public static final String MI_Rules_ObsoleteDateTime_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_timePopupLink']";
	public static final String MI_Rules_ObsoleteNowbtn_Path = ".//a[text()='Obsolete Now']";
	public static final String MI_Rules_UnObsoleteNowbtn_Path = ".//a[text()='Un-Obsolete']";
	public static final String MI_Rules_UnObsoleteNowmsg_Path = ".//span[text()='Item reactivated!']";
	public static final String MI_Rules_Reason_Field_Path = "//*[@id='ctl00_cphContent_txtReason']";
	public static final String MI_Rules_RequireRelatedItems_Path = ".//*[@id='divContent clearfix']/div[14]/h1";
	public static final String MI_Rules_Require_FormNo_Path = ".//th[text()='Form Number']";
	public static final String MI_Rules_Require_Quantity_Path = ".//a[text()='Quantity']";
	public static final String MI_Rules_Require_Currentlocation_Path = ".//a[text()='Content Location']";
	public static final String MI_Rules_Require_Type_Path = ".//a[text()='Type']";
	public static final String MI_Rules_StateMode_Path = ".//*[@id='ctl00_cphContent_divStateRules']/div[1]/h1";
	public static final String MI_Rules_StateModefield_Path = ".//*[@id='ctl00_cphContent_divStateRules']/div[1]/div[1]/div[1]/div[1]";
	public static final String MI_Rules_StateMode_ExcRadio_Path = ".//*[@id='ctl00_cphContent_rdbStateMode_0']";
	public static final String MI_Rules_StateMode_IncRadio_Path = ".//*[@id='ctl00_cphContent_rdbStateMode_1']";
	public static final String MI_Rules_States_Drop_Path = ".//*[@id='ctl00_cphContent_ddlIncludeExcludeStates_Input']";
	public static final String MI_Rules_FirmMode_Path = ".//*[@id='divContent clearfix']/div[18]/h1";
	public static final String MI_Rules_FirmModefield_Path = ".//*[@id='ctl00_cphContent_divFirms']/div[1]/div[1]";

	public static final String MI_Rules_FirmMode_ExcRadio_Path = ".//*[@id='ctl00_cphContent_rblFirmMode_0']";
	public static final String MI_Rules_FirmMode_IncRadio_Path = ".//*[@id='ctl00_cphContent_rblFirmMode_1']";
	public static final String MI_Rules_Firms_Drop_Path = ".//*[@id='ctl00_cphContent_ddlFirms_Input']";
	public static final String MI_Rules_Firms_back_Path = ".//*[@id='ctl00_cphContent_btnBack']]";
	public static final String MI_Rules_Maxordertitle_Path = ".//*[@id='divContent clearfix']/div[21]/h1";
	public static final String MI_Rules_MaxorderFrequency_Path = ".//a[text()='Frequency']";
	public static final String MI_Rules_MaxorderRolename_Path = ".//a[text()='Role Name']";
	public static final String MI_Rules_MaxorderQuantity_Path = ".//a[text()='Max Order Quantity']";

	
	// Metrics Tab
	
	public static final String MI_MetricsTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[6]/a/span/span/span";
	public static final String MI_MetricsHeader_Path = ".//*[@id='ctl00_cphContent_lblHeader']";
	public static final String MI_MetricsTable_Path = ".//*[@id='divContent clearfix']/div[5]/div/table";
	public static final String MI_Metrics_UnitsonHand_Path = ".//*[@id='ctl00_cphContent_lblUnitsOnHand_Value']";
	public static final String MI_Metrics_UnitsAvailable_Path = ".//*[@id='ctl00_cphContent_lblUnitsAvailable_Value']";
	public static final String MI_Metrics_Allocated_Path = ".//*[@id='ctl00_cphContent_lblUnitsAllocated_Value']";
	public static final String MI_Metrics_BackOrderQTY_Path = ".//*[@id='ctl00_cphContent_lblBackOrderQty_Value']";
	public static final String MI_Metrics_MTD_Path = ".//*[@id='ctl00_cphContent_lblMTDUsage_Value']";
	public static final String MI_Metrics_YTD_Path = ".//*[@id='ctl00_cphContent_lblYTDUsage_Value']";
	public static final String MI_Metrics_AverageMonthlyUsage_Path = ".//*[@id='ctl00_cphContent_lblAverageMonthlyUsage_Value']";
	public static final String MI_Metrics_AverageMonthlyDownloads_Path = ".//*[@id='ctl00_cphContent_lblAverageDownload_Value']";
	
	// Change History Tab
	
	public static final String MI_ChangeHistoryTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span";
	public static final String MI_ChangeHistoryTitle_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	
	public static final String MI_ChangeHistoryAction1_Path =".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";
	public static final String MI_ChangeHistoryAction2_Path =".//*[@id='ctl00_cphContent_History_ctl00__1']/td[3]";
	public static final String MI_ChangeHistoryAction3_Path =".//*[@id='ctl00_cphContent_History_ctl00__2']/td[3]";
	public static final String MI_ChangeHistoryAction4_Path =".//*[@id='ctl00_cphContent_History_ctl00__3']/td[3]";
	public static final String MI_ChangeHistoryAction5_Path =".//*[@id='ctl00_cphContent_History_ctl00__4']/td[3]";
	public static final String MI_ChangeHistoryAction6_Path =".//*[@id='ctl00_cphContent_History_ctl00__5']/td[3]";
	public static final String MI_ChangeHistory_Date_Path =".//a[text()='Date']";
	public static final String MI_ChangeHistory_User_Path =".//a[text()='User']";
	public static final String MI_ChangeHistory_Action_Path =".//a[text()='Action']";
	
	
	
	// Pageflex Tab
	
	public static final String MI_PageflexTab_Path =".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[8]/a/span/span/span";
	public static final String MI_Pageflextitle_Path =".//*[@id='divContent clearfix']/div[4]/h1";
	public static final String MI_Pageflex_Jobnamefield_Path =".//*[@id='ctl00_cphContent_txtJobName']";
	public static final String MI_PageflexInventory_Path =".//*[@id='divContent clearfix']/div[7]/h1";
	public static final String MI_PageflexOptiontype__Path =".//a[text()='Option Type']";
	public static final String MI_Pageflex_FriendlyName_Path =".//a[text()='Friendly Name']";
	public static final String MI_Pageflex_FieldGroup_Path =".//a[text()='Field Group']";
	public static final String MI_Pageflex_MaxLength_Path =".//a[text()='Max Length']";
	public static final String MI_Pageflex_Visible_Path =".//a[text()='Visible']";
	public static final String MI_Pageflex_Required_Path =".//a[text()='Required']";
	public static final String MI_Pageflex_Active_Path =".//a[text()='Active']";



	public static final String MI_Pageflex_Inventoryoption_Path =".//*[@id='divContent clearfix']/div[7]/h1";
	public static final String MI_Pageflex_Savebtn_Path =".//*[@id='ctl00_cphContent_btnSave']";
	
	// Kitting Tab
	
	public static final String MI_Kitting_Path =".//span[text()='Kitting']";
	public static final String MI_Kitting_Additembtn_Path =".//*[@id='ctl00_cphContent_btnKitItem']";
	public static final String MI_Kitting_RemovepopupOK_Path =".//span[text()='OK']";
	public static final String MI_Kitting_Formno_Path =".//*[@id='ctl00_cphContent_ddlKitFormNumber_Input']";
	public static final String MI_Kitting_Type_Path =".//*[@id='ctl00_cphContent_ddlKitType_Arrow']";
	public static final String MI_Kitting_Location_Path =".//*[@id='ctl00_cphContent_ddlKitLocation_Arrow']";
	public static final String MI_Kitting_Qty_Path =".//*[@id='ctl00_cphContent_txtKitQuantity']";
	public static final String MI_Kitting_Perkitck_Path =".//*[@id='ctl00_cphContent_rdbPerParent']";
	public static final String MI_Kitting_Perorderck_Path =".//*[@id='ctl00_cphContent_rdbPerOrder']";
	public static final String MI_Kitting_Optionalck_Path =".//*[@id='ctl00_cphContent_chkKitOptional']";
	public static final String MI_Kitting_Sortorder_Path =".//*[@id='ctl00_cphContent_txtKitSortOrder']";
	public static final String MI_Kitting_Additemloadingpanel_Path =".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_pnlKitItem']";
	
	//	
	
	public static final String MI_Viewable_not_orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[5]";
	public static final String MI_Not_Viewable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[3]";
	public static final String MI_Orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[4]";
}

