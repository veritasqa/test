package com.Protectivelife.OR;


public class SupportTickets extends Widgets {
	
	
	// Support Veritas Ticket
	
	public static final String ST_Veritasticktitle_Path = ".//h1[text()='Support : Veritas Tickets']";
	public static final String ST_TicketNotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_CollaborationID']";
	public static final String ST_OrderNotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[2]";
	public static final String ST_OrderNofilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_OrderID']";
	public static final String ST_SubCattxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_SubCategory']";
	public static final String ST_SubCatfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_SubCategory']";
	public static final String ST_Formnotxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_InventoryID']";
	public static final String ST_Formnofilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_InventoryID']";
	public static final String ST_Submittedbytxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ReportedByUserName']";
	public static final String ST_Submittedbyfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ReportedByUserName']";
	public static final String ST_Updatedontxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_dateInput']";
	public static final String ST_Updatedoncalender_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_RDIPFModDate_popupButton']";
	public static final String ST_Updatedonfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ModDate']";
	public static final String ST_Teamtxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_ClientTeam']";
	public static final String ST_Teamfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_ClientTeam']";
	public static final String ST_Statustxt_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']";
	public static final String ST_Statusfilter_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']";
	public static final String ST_VT_OrderNo1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[2]";
	public static final String ST_VT_TicketNo1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[1]/a";
	public static final String ST_VT_Edit1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[9]/a";
	public static final String ST_VT_SubCat1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[3]";
	public static final String ST_VT_Status1_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]";
	public static final String ST_VT_Addticketicon_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ST_VT_Addticketlabel_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String ST_VT_Refreshicon_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String ST_VT_Refreshlabel_Path = ".//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String ST_VT__Path = "";
	
	// Create Support Ticket Popup
	public static final String ST_CST_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div";
	public static final String ST_CST_Title_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]";
	public static final String ST_CST_Categorydd_Path = "//*[@id='ctl00_cphContent_ddlCategory']";
	public static final String ST_CST_SubCategorydd_Path = "//*[@id='ddlSubCategory']";
	public static final String ST_CST_Ordernofield_Path = ".//*[@id='ctl00_cphContent_txtOrderNumber']";
	public static final String ST_CST_Commentfield_Path = ".//*[@id='ctl00_cphContent_btnCreateSupportTicket']";
	public static final String ST_CST_Createbtn_Path = ".//a[contains(.,'Create')]";
	public static final String ST_CST_Cancelbtn_Path = ".//*[@id='ctl00_cphContent_btnHideCreateSupportTicketPopup']";
	
	// Support Ticket Manage
	
	public static final String STM_NewStatus_Path = ".//a[text()='New']";
	public static final String STM_Inprogress_Path = ".//a[text()='In-Progress']";
	public static final String STM_Hold_Path = ".//a[text()='Hold']";
	public static final String STM_Closedstatus_Path = ".//a[text()='Closed']";
	public static final String STM_CancelOrder_Path = ".//a[text()='Cancel Order']";
	public static final String STM_Category_Path = ".//*[@id='ddlCategory']";
	public static final String STM_Assignedto_Path = ".//*[@id='ddlAssignedTo']";
	public static final String STM_SubCat_Path = ".//*[@id='ddlSubCategory']";
	public static final String STM_Veritasteam_Path = ".//*[@id='ddlVeritasTeam']";
	public static final String STM_Ordername_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rcmbxOrderNumber_Input']";
	public static final String STM_Clientteam_Path = ".//*[@id='ddlClientTeam']";
	public static final String STM_User1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[1]";
	public static final String STM_Createdate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[2]";
	public static final String STM_Comment1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]";
	public static final String STM_Attachment1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[4]";
	public static final String STM_Userrefreshicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String STM_Userrefreshlabel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String STM_Ticketno_Path = ".//*[@id='ticketNumberSpan']";
	public static final String STM_Addcommenticon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String STM_Addcommentlabel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String STM_Commenttext_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_rtxtComment']";
	public static final String STM_Attachment_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_fupCommentsAttachmentrow0']";
	public static final String STM_CommentCancel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_btnCancel']";
	public static final String STM_Createcomment_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00_ctl02_ctl02_btnSave']";
	public static final String STM_Addcommentloadingpanel_Path = ".//*[@id='RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid']";
	public static final String STM_TicketSuccefullyUpdatedmsg_Path = ".//*[@id='TicketSuccefullyUpdated']";
	public static final String STM_UpdateTicketbtn_Path = ".//*[@id='SupportTicketCostsDiv']/div[2]/ul/li[2]/input";
	public static final String STM_UpdateTicketmsg_Path = ".//*[@id='TicketSuccefullyUpdated']";
	
	//Client Support Ticket 
	
	
	public static final String ST_CT_Addticketicon_Path =	"//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ST_CT_Edit1_Path = "//*[@id='ctl00_cphContent_ClientSupportTicketsRadGrid_ctl00__0']/td[9]/a";

}
