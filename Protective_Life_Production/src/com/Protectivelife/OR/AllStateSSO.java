package com.Protectivelife.OR;

import com.Protectivelife.Values.ValueRepository;

public class AllStateSSO extends ValueRepository {
	
	public static final String All_Homelogo_Path = ".//*[@id='divHeaderImage']/img";
	public static final String All_OrderPrintMaterials_Path = ".//h1[contains(text(),'Order Print Materials')]";
	public static final String All_Part1image_Path = ".//*[@id='ImgCapitalGains']";
	public static final String All_Part2image_Path = ".//*[@id='ImgtaxDeferral']";
	public static final String All_Part1name_Path = ".//*[@id='lblInventoryName1']";
	public static final String All_Part2name_Path = ".//*[@id='lblInventoryName2']";
	public static final String All_Part1qty_Path = ".//*[@id='txtInventoryQty1']";
	public static final String All_Part2qty_Path = ".//*[@id='txtInventoryQty2']";
	public static final String All_Shippinginfo_Path = ".//h1[contains(text(),'Shipping Information')]";
	public static final String All_Namefield_Path = ".//*[@id='txtShipName']";
	public static final String All_Address1_Path = ".//*[@id='txtShipAddress1']";
	public static final String All_Address2_Path = ".//*[@id='txtShipAddress2']";
	public static final String All_Address3_Path = ".//*[@id='txtShipAddress3']";
	public static final String All_City_Path = ".//*[@id='txtShipCity']";
	public static final String All_Statedd_Path = ".//*[@id='ddlShipState']";
	public static final String All_Zip_Path = ".//*[@id='txtShipZip']";
	public static final String All_Phone_Path = ".//*[@id='txtShipPhone']";
	public static final String All_Email_Path = ".//*[@id='txtShipEmail']";
	public static final String All_FinishOrder_Path = ".//*[@id='btnOrder']";
	public static final String All_Name_Err_Path = ".//*[@id='CustomValidator6']";
	public static final String All_Address1_Err_Path = ".//*[@id='CustomValidator7']";
	public static final String All_City_Err_Path = ".//*[@id='CustomValidator8']";
	public static final String All_State_Err_Path = ".//*[@id='CustomValidator3']";
	public static final String All_Zip_Err_Path = ".//*[@id='CustomValidator9']";
	public static final String All_Part2orderlimit_Err_Path = ".//*[@id='CustomValidator2']";
	public static final String All_Successfullorder_Msg_Path = ".//*[@id='lblErrorMessage']";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	public static final String All__Err_Path = "";
//	
}
