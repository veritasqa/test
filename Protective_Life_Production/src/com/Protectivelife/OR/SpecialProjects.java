package com.Protectivelife.OR;

public class SpecialProjects extends SupportTickets {
	
	
	public static final String Spl_save_Message ="Data has been saved.";
	public static final String Spl_Status_Message ="New";
	public static final String Spl_Details_History = "	Changed Description From '' To 'QA test'";
	
	public static final String Spl_Prgs_BrowseFiles1 = System.getProperty("user.dir")+"\\src\\com\\Protectivelife\\Util\\Quality.jpg";
	public static final String Spl_Prgs_BrowseFiles2 = System.getProperty("user.dir")+"\\src\\com\\Protectivelife\\Util\\Sample File Attachment.docx";

	
	public static final String Spl_AddTicket_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_AddNewRecordButton']";	
	public static final String Spl_Refresh_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RefreshButton']";		
	public static final String Spl_Viewedit_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[8]/a";
	
	// Search bar elements
	public static final String Spl_Ticketnum_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_TicketNumber']";
	public static final String Spl_jobtitle_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes']/span/button";
	public static final String Spl_Closed_checkbox_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Closed']";
	
	
	public static final String Spl_GeneralTab_jobtitle = ".//*[@id='tabs-1']/div[1]/div[1]/div[1]/div[1]";
	
	
	public static final String Spl_GeneralTab_Path=".//*[@id='ctl00_cphContent_lnkGeneral']/span";
	public static final String Spl_GeneralTab_jobtitle_Path= ".//*[@id='ctl00_cphContent_txtJobTitle']";
	public static final String Spl_JobType_Path =	".//*[@id='ctl00_cphContent_ddlJobType']";
	public static final String Spl_Description_Path =	".//*[@id='ctl00_cphContent_txtDescription']";
	
	public static final String Spl_QtyOfAddressee_Path =".//*[@id='ctl00_cphContent_txtQtyOfAddressee']";
	public static final String Spl_Shipping_Dropdown_Path =".//*[@id='ctl00_cphContent_ddlShipping']";
	public static final String Spl_DuedateInput_Path =".//*[@id='ctl00_cphContent_dtpDueDate_dateInput']";
	public static final String Spl_Createdby_Path =".//*[@id='ctl00_cphContent_lblCreateUser']";
	public static final String Spl_CostCenter_Arrow_Path =".//*[@id='ctl00_cphContent_ddlCostCenter_Arrow']";
	public static final String Spl_CostCenter_Input_Path =".//*[@id='ctl00_cphContent_ddlCostCenter_Input']";
	public static final String Spl_SubAccount_Arrow_Path =".//*[@id='ctl00_cphContent_ddlSubAccount_Arrow']";
	public static final String Spl_SubAccount_Input_Path =".//*[@id='ctl00_cphContent_ddlSubAccount_Input']";
	public static final String Spl_OnBehalfOf_Path =".//*[@id='ctl00_cphContent_txtOnBehalfOf']";
	public static final String Spl_SpecialInstructions_Path =".//*[@id='ctl00_cphContent_txtSpecialInstructions']";
	public static final String Spl_Rush_chkbox_Path =".//*[@id='ctl00_cphContent_chkRush']";
	public static final String Spl_Quote_chkbox_Path =".//*[@id='ctl00_cphContent_chkQuoteNeeded']";
	public static final String Spl_Note_path = ".//*[@id='tabs-1']/div[1]/div[2]/div[8]/div";
	public static final String Spl_GeneralTab_jobtitle_Star = ".//*[@id='tabs-1']/div[1]/div[1]/div[1]/div[2]/span";
	public static final String Spl_GeneralTab_jobtype_Star = ".//*[@id='tabs-1']/div[1]/div[1]/div[2]/div[2]/span[1]";
	public static final String Spl_GeneralTab_onbehalf_Star = ".//*[@id='tabs-1']/div[1]/div[2]/div[3]/div[2]/span";

	public static final String Spl_CostCenter_9999_Path =".//*[@id='ctl00_cphContent_ddlCostCenter_DropDown']/div/ul/li[28]";
	public static final String Spl_SubAccount_LI_NS_Path =".//*[@id='ctl00_cphContent_ddlSubAccount_DropDown']/div/ul/li[4]/label/input";

	
	public static final String Spl_ProgressFiles_Path=".//*[@id='ctl00_cphContent_lnkProgress']/span";
	public static final String Spl_Prgs_AddComment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String Spl_Prgs_Refresh_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String Spl_Prgs_Comment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_txtComment']";
	public static final String Spl_Prgs_Type_arrow = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Arrow']";
	public static final String Spl_Prgs_Type_cancel = ".//li[contains(.,'Cancel')]";
	public static final String Spl_Prgs_Type_Acknowledged = "//li[contains(.,'Acknowledged')]";
	public static final String Spl_Prgs_BrowseFiles_btn = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_uploadCommentAttachmentrow0']/span";

	public static final String Spl_Prgs_Submit_btn = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnSaveComment']";
	public static final String Spl_Prgs_cancel_btn = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnCancel']";
	
	
	public static final String Spl_Costs_Path=".//*[@id='ctl00_cphContent_lnkCosts']/span";
	public static final String Spl_Costs_QuoteCost_Path=".//*[@id='ctl00_cphContent_txtQuoteCost']";
	public static final String Spl_Costs_PrintCost_Path=".//*[@id='ctl00_cphContent_txtPrintCost']";
	public static final String Spl_Costs_PostageCost_Path=".//*[@id='ctl00_cphContent_txtVeritasPostageCost']";
	public static final String Spl_Costs_FulfillmentCost_Path=".//*[@id='ctl00_cphContent_txtFulfillmentCost']";
	public static final String Spl_Costs_Billed_Path=".//*[@id='ctl00_cphContent_ckBilled']";
	public static final String Spl_Costs_ClientPostageCost_Path=".//*[@id='ctl00_cphContent_txtClientPostageCost']";
	public static final String Spl_Costs_RushCost_Path=".//*[@id='ctl00_cphContent_txtRushCost']";

	
	public static final String Spl_Costs_AddNewRecordButton_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	
	public static final String Spl_Costs_Record_Description_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_txtDescription']";
	public static final String Spl_Costs_Record_Type_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType']";
	public static final String Spl_Costs_Record_dateInput_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rdpBillDate_dateInput']";
	public static final String Spl_Costs_Record_BillAmount_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rtxtBillAmount']";
	public static final String Spl_Costs_Record_Save_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_btnSave']";

	
	
	public static final String Spl_Costs_Description_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_txtDescription']";
	public static final String Spl_Costs_ddlType_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType']";

	public static final String Spl_Costs_BillDate_dateInpu_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rdpBillDate_dateInput']";
	public static final String Spl_Costs_tBillAmount_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rtxtBillAmount']";
	public static final String Spl_Costs_Save_Path=".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_btnSave']";
	
	
	
	public static final String Spl_Inventory_Path=".//*[@id='ctl00_cphContent_lnkEnclosures']/span";
	public static final String Spl_Inventory_AddprojectTxt_Path=".//*[@id='ctl00_cphContent_racbFormNumbers_Input']";
	public static final String Spl_Inventory_Addproject_Path=".//*[@id='ctl00_cphContent_btnAddFormNumbers']";
	public static final String Spl_Inventory_Multifunction_Path=".//*[@id='aspnetForm']/div[1]/div/ul/li[1]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";

	
	
	
	public static final String Spl_History_Path=".//*[@id='ctl00_cphContent_lnkHistory']/span";
	public static final String Spl_History_Description = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";
	public static final String Spl_History_Date_Time_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[1]";
	public static final String Spl_History_User_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[2]";
	public static final String Spl_History_Details_Path = ".//td[contains(.,'Created On')]";
	
	
	public static final String Spl_Cancel_Btn_Path = ".//*[@id='ctl00_cphContent_btnCancel']";
	public static final String Spl_create_Btn_Path = ".//*[@id='ctl00_cphContent_btnSave']";
	public static final String Spl_save_Message_Path=".//*[@id='ctl00_cphContent_lblMessage']";
	public static final String Spl_Ticketnum_path = ".//*[@id='spanTicketNumber']";
	public static final String Spl_Status_path= ".//*[@id='tabs']/div[1]/h1/span/span[2]";

	
	public String TicketnumberEdit(String Ticketnumber) {

		return ".//a[@title='" + Ticketnumber + "']//following::td[7]/a";

	}
	
	public String Ticketresultsticketnum(String Ticketnumber) {

		return ".//a[@title='" + Ticketnumber + "']";

	}
	
	
	
	
}

