package com.Protectivelife.Methods;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;

import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Protectivelife.OR.BulkInventoryUpdate;

public class CommonMethods extends BulkInventoryUpdate {

	public static WebDriver driver;
	public static String reason;
	public Alert alert = null;
	public WebElement element;
	public String today_date;
	public String Att = "";
	public String attrib = "";

	public static WebDriver Open_Browser(String browser, String path) {

		try {
			if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", path);
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase("chrome")) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				options.addArguments("--disable-web-security");
				options.addArguments("--no-proxy-server");
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				options.setExperimentalOption("prefs", prefs);
				System.setProperty("webdriver.chrome.driver", path);
				driver = new ChromeDriver(options);
			} else if (browser.equalsIgnoreCase("opera")) {
				driver = new OperaDriver();
			} else if (browser.equalsIgnoreCase("safari")) {
				driver = new SafariDriver();
			} else if (browser.equalsIgnoreCase("internetexplorer")) {
				driver = new InternetExplorerDriver();
			}

			else {
				reason = "Invalid Browser Input. Check Browser name";
			}

		} catch (Exception e) {

			reason = e.getMessage();

		}
		return driver;

	}

	public static void Open_Url_Window_Max(String url, WebDriver _driver)

	{
		driver = _driver;

		try {

			driver.manage().window().maximize();
			driver.get(url);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// Assert.assertTrue(false);
		}

	}

	public void Implicit_Wait(WebDriver _driver)

	{
		driver = _driver;
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
	}

	public void ExplicitWait_Element_Clickable(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			WebDriverWait wait = new WebDriverWait(driver, 25);
			element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
	}

	public void ExplicitWait_Element_Visible(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			WebDriverWait wait = new WebDriverWait(driver, 25);
			element = driver.findElement(locator);
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}
	}

	public void ExplicitWait_Element_Not_Visible(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			element = driver.findElement(locator);
			wait.until(ExpectedConditions.invisibilityOf(element));
		} catch (Exception e) {

			System.out.println(e.getMessage() + "Element is " + value);
			// Assert.assertTrue(false);

		}

	}

	public void Close_Browser(WebDriver _driver) {
		driver = _driver;

		try {

			driver.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}
	}

	public void Thread_Sleep(int Sleep_time) {

		try {
			Thread.sleep(Sleep_time);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}

	}

	public void Quit_Browser(WebDriver _driver)

	{
		driver = _driver;

		try {
			driver.quit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}

	}

	public By locatorValue(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		By by;
		switch (locatorType) {
		case "id":
			by = By.id(value);
			break;
		case "name":
			by = By.name(value);
			break;
		case "xpath":
			by = By.xpath(value);
			break;
		case "cssSelector":
			by = By.cssSelector(value);
			break;
		case "linkText":
			by = By.linkText(value);
			break;
		case "partialLinkText":
			by = By.partialLinkText(value);
			break;
		case "classname":
			by = By.className(value);
			break;
		default:
			by = null;
			break;
		}
		return by;
	}

	public boolean Click_On_Element(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Clickable(locatorType, value, driver);
			driver.findElement(locator).click();
			return true;

		} catch (Exception e) {
			// System.out.println("Element "+value+" not found");
			// Assert.assertTrue(false);
			return false;
		}
	}

	public boolean Element_IsEnabled(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			// ExplicitWait_Element_Clickable(locatorType, value, driver);
			return driver.findElement(locator).isEnabled();

		} catch (Exception e) {

			return false;
			// System.out.println("Element "+value+" not found");
			// Assert.assertTrue(false);
		}
	}

	public boolean Element_IsEnabled_Nowait(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			return driver.findElement(locator).isEnabled();

		} catch (Exception e) {

			return false;
			// System.out.println("Element "+value+" not found");
			// Assert.assertTrue(false);
		}
	}

	public void Element(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Clickable(locatorType, value, driver);
			driver.findElement(locator);

		} catch (Exception e) {
			// System.out.println("Element "+value+" not found");
			// Assert.assertTrue(false);
		}
	}

	public boolean Clear_Type_Charecters(String locatorType, String value, String parameter, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Visible(locatorType, value, driver);
			driver.findElement(locator).clear();
			driver.findElement(locator).sendKeys(parameter);
			return true;

		} catch (Exception e) {
			// Assert.assertTrue(false);
			return false;

			// System.out.println(e.getMessage());
		}

	}

	public String Get_Attribute(String locatorType, String value, String parameter, WebDriver _driver) {
		driver = _driver;

		try {

			By locator;
			locator = locatorValue(locatorType, value, driver);
			// driver.findElement(locator).clear();
			ExplicitWait_Element_Visible(locatorType, value, driver);
			attrib = driver.findElement(locator).getAttribute(parameter).trim();

		} catch (Exception e) {
			// result = "Fail";
			reason = e.getMessage();

			return e.getMessage();

		}
		return attrib;
	}

	public boolean Double_Click(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			Actions act = new Actions(driver);
			ExplicitWait_Element_Clickable(locatorType, value, driver);
			WebElement Admin = driver.findElement(locator);
			act.doubleClick(Admin).build().perform();
			return true;
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
			// Assert.assertTrue(false);
			return false;
		}

	}

	public boolean Hover_Over_Element(String locatorType, String value, WebDriver _driver) {

		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			Actions act = new Actions(driver);

			WebElement Admin = driver.findElement(locator);
			act.clickAndHold(Admin).build().perform();
			return true;

		} catch (Exception e) {

			System.out.println(e.getMessage());
			// Assert.assertTrue(false);
			return false;
		}
	}

	public void SwitchtoAlert(WebDriver _driver) {

		driver = _driver;

		try {

			// Alert popup = 
			driver.switchTo().alert();

		} catch (Exception e) {

			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}

	}

	public boolean Accept_Alert(WebDriver _driver) {

		driver = _driver;

		try {

			Alert popup = driver.switchTo().alert();

			popup.accept();
			driver.switchTo().defaultContent();
			return true;
		} catch (Exception e) {

			System.out.println(e.getMessage());
			// Assert.assertTrue(false);
			return false;
		}

	}

	public String Alert_Text(WebDriver _driver) {

		driver = _driver;

		try {

			Alert popup = driver.switchTo().alert();

			return popup.getText();
			// driver.switchTo().defaultContent();
		} catch (Exception e) {

			return e.getMessage();
			// Assert.assertTrue(false);

		}

	}

	public void Switch_New_Tab(WebDriver _driver) {
		driver = _driver;

		try {

			Window_Handle = driver.getWindowHandle();

			List<String> ID = new ArrayList<String>(driver.getWindowHandles());

			driver.switchTo().window(ID.get(1));

		} catch (Exception e) {

			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}
	}

	public void Switch_Old_Tab(WebDriver _driver) {
		driver = _driver;

		try {
			Close_Browser(driver);

			driver.switchTo().window(Window_Handle);

		} catch (Exception e) {

			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}
	}

	public Boolean Is_Element_Present(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			driver.findElement(locator);
			return true;
		} catch (Exception e) {
			reason = e.getMessage();

			return false;
		}
	}

	public boolean Element_Isselected(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		By locator;
		locator = locatorValue(locatorType, value, driver);
		return driver.findElement(locator).isSelected();
	}

	public boolean Element_Is_Displayed(String locatorType, String value, WebDriver _driver) {
		driver = _driver;
		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			return driver.findElement(locator).isDisplayed();
		} catch (Exception e) {

			reason = e.getMessage();
			return false;
		}
	}

	public String Get_Text(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Visible(locatorType, value, driver);

			String Text = driver.findElement(locator).getText();
			return Text;

		} catch (Exception e) {

			reason = e.getMessage();
			return reason;
		}
	}

	public String Get_Title(WebDriver _driver) {
		driver = _driver;

		try {

			return driver.getTitle();

		} catch (Exception e) {

			return e.getMessage();
		}
	}

	public String Get_Current_Url(WebDriver _driver) {
		driver = _driver;

		try {

			return driver.getCurrentUrl();

		} catch (Exception e) {

			return e.getMessage();
		}
	}

	public List<String> getexcel(String filepath) throws IOException {
		List<String> excellist = new ArrayList<String>();

		FileInputStream fis = new FileInputStream(filepath);

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Sheet1");

		XSSFRow row = sheet.getRow(1);
		row.getLastCellNum();

		for (int rownum = 1; rownum <= sheet.getLastRowNum(); rownum++) {

			XSSFRow row2 = sheet.getRow(rownum);
			excellist.add(row2.getCell(0).toString());

		}
		workbook.close();
		fis.close();
		return excellist;
	}

	public List<String> getexcelcol2(String filepath) throws IOException {
		List<String> excellist = new ArrayList<String>();

		FileInputStream fis = new FileInputStream(filepath);

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Sheet1");

		XSSFRow row = sheet.getRow(1);
		row.getLastCellNum();

		for (int rownum = 1; rownum <= sheet.getLastRowNum(); rownum++) {

			XSSFRow row2 = sheet.getRow(rownum);
			excellist.add(row2.getCell(1).toString());
		}
		workbook.close();
		fis.close();
		return excellist;
	}

	public void writeReport(String output, String result) throws IOException // Dev:Functionto
																				// write
																				// test
																				// report
																				// with
																				// time
																				// stamp
	{
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
		Date curDate = new Date();
		String strDate = sdf.format(curDate);

		String TestFile = output + "_" + strDate + ".txt";
		File FC = new File(TestFile);
		FC.createNewFile();

		FileWriter FW = new FileWriter(TestFile); // Writing In to file.
		BufferedWriter BW = new BufferedWriter(FW);
		BW.write(result);
		BW.newLine();
		BW.close();
	}

	public void Select_DropDown(String locatorType, String value, String option, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Visible(locatorType, value, driver);
			element = driver.findElement(locator);
			Select Select_drop = new Select(element);
			Select_drop.selectByValue(option);
			// return Text;

		} catch (Exception e) {
			// result = "Fail";
			reason = e.getMessage();
			// return reason;
		}
	}

	public boolean Select_DropDown_VisibleText(String locatorType, String value, String option, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			// ExplicitWait_Element_Visible(locatorType, value, driver);
			element = driver.findElement(locator);
			Select Select_drop = new Select(element);
			Select_drop.selectByVisibleText(option);
			return true;

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			// return reason;
			return false;
		}
	}

	public String Get_DropDown(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Visible(locatorType, value, driver);
			element = driver.findElement(locator);
			Select Select_drop = new Select(element);
			// Select_drop.selectByValue(option);
			return Select_drop.getFirstSelectedOption().getText();

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			return e.getMessage();
		}
	}

	public String Get_Todaydate(String dateformat) {
		// driver = _driver;

		try {
			// Create object of SimpleDateFormat class and decide the format
			DateFormat dateFormat = new SimpleDateFormat(dateformat);

			// get current date time with Date()
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.MINUTE, -630);

			// Now format the date
			return dateFormat.format(date);

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			return e.getMessage();
		}
	}

	public String Get_Futuredate(String dateform) {
		// driver = _driver;

		try {
			DateFormat dateFormat = new SimpleDateFormat(dateform);
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.DATE, 7);
			return dateFormat.format(c.getTime());

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			return e.getMessage();
		}
	}

	public String Get_Pastdate(String dateform) {
		// driver = _driver;

		try {
			DateFormat dateFormat = new SimpleDateFormat(dateform);
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.DATE, -7);
			return dateFormat.format(c.getTime());

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			return e.getMessage();
		}
	}

	public static String Get_FutureTime(String dateform) {
		// driver = _driver;

		try {
			DateFormat dateFormat = new SimpleDateFormat(dateform);
			Date date = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.MINUTE, -625);
			return dateFormat.format(c.getTime());

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			return e.getMessage();
		}
	}

	public boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} // try
		catch (NoAlertPresentException e) {
			return false;
		} // catch
	}

	public String Wid_Xpath(String Widget, String Path) {

		try {

			Att = ".//*[@id='" + Get_Attribute(locatorType,
					"//*[contains(@id,'RadDock')]//*[text()='" + Widget + "']//following::tr[1]//*[@class='rdContent']",
					"Id", driver) + Path;
		}

		catch (Exception e) {

			return e.getMessage();
		}

		return Att;
	}

	public String li_value(String option) {

		return ".//li[text()='" + option + "']";

	}

	public String Textpath(String option) {

		return ".//*[text()='" + option + "']";

	}

	public int RandomInt() {
		Random rand = new Random();
		return rand.nextInt(9999);

	}

	public void NavigateBack(WebDriver _driver) {
		driver = _driver;

		try {

			driver.navigate().back();

		} catch (Exception e) {

			reason = e.getMessage();
		}
	}

	public void Fileupload(String FilePath, WebDriver _driver) {
		driver = _driver;

		try {

			StringSelection ss = new StringSelection(FilePath);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();
			Wait_ajax();
			//Thread_Sleep(5000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

		} catch (Exception e) {

			reason = e.getMessage();
		}
	}

	public void Closealltabs(WebDriver _driver) {
		driver = _driver;
		OriginalWindow = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		for (String handle : ID) {
			// System.out.println(handle);

			if (!handle.equalsIgnoreCase(OriginalWindow)) {

				driver.switchTo().window(handle);
				driver.close();

			}

		}
		driver.switchTo().window(OriginalWindow);
	}

	public void Switch_New_Tab2(WebDriver _driver) {
		driver = _driver;

		try {

			Window_Handle = driver.getWindowHandle();

			List<String> ID = new ArrayList<String>(driver.getWindowHandles());

			driver.switchTo().window(ID.get(2));

		} catch (Exception e) {

			System.out.println(e.getMessage());
			// Assert.assertTrue(false);

		}
	}

	public static void Wait_ajax() throws InterruptedException {
		String a = null;
		for (int i = 0; i < 30; i++) {
			a = (String) ((JavascriptExecutor) driver)
					.executeScript("var data=window.$.active; return data.toString();");
			Thread.sleep(20);
			if (!a.equals("0")) {
				a = (String) ((JavascriptExecutor) driver)
						.executeScript("var data=window.$.active; return data.toString();");

				break;

			}
		}
		int z = 0;
		while (!a.equals("0")) {
			a = (String) ((JavascriptExecutor) driver)
					.executeScript("var data=window.$.active; return data.toString()");
			Thread.sleep(100);
			z++;
			if (z > 700) {
				break;
			}

		}

	}
	
	public String GetCSS_Backgroundcolor(String locatorType, String value, WebDriver _driver) {

		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			ExplicitWait_Element_Visible(locatorType, value, driver);
			return Color.fromString(driver.findElement(locator).getCssValue("background-color")).asHex();

		} catch (Exception e) {
			// Assert.assertTrue(false);
			return e.getMessage();

			// System.out.println(e.getMessage());
		}

	}

}