package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class ExpressCheckoutTest extends BaseTest {

	public void HoverandSelectWidget() {

	}

	@Test(enabled = true)
	public void PL_TC_2_6_1_1_3() {

		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_SelectRecipient_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(STATE_RESTRICTION_Name, STATE_RESTRICTION_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Expck_Form1_Path), "QA_STATERESTRICTION", driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), "1", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);

		//

		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).equalsIgnoreCase("QA_STATERESTRICTION"),
				"Part Name Mis-match");

		Clearcart();

		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_SelectRecipient_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(STATE_RESTRICTION_Name_NoIL, STATE_RESTRICTION_last_Name_NOIL);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Expck_Form1_Path), "QA_STATERESTRICTION", driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), "1", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);
		Thread_Sleep(3000);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);
		ExplicitWait_Element_Visible(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver);

		System.out.println(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver));
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver)
				.equalsIgnoreCase(Invalid_Form_Message), "Message not Displayed ");

	}

	@Test(enabled = false)
	public void PL_TC_2_6_1_1_4() {

		Clearcart();

		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_SelectRecipient_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(FIRMRESTRICTION_FirstName1, FIRMRESTRICTION_lastName1);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Expck_Form1_Path), "QA_FIRMRESTRICTION", driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), "1", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);

		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).equalsIgnoreCase("QA_FIRMRESTRICTION"),
				"Part Name MissMatch");

		Clearcart();

		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_SelectRecipient_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(FIRMRESTRICTION_FirstName2, FIRMRESTRICTION_lastName2);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Expck_Form1_Path), "QA_FIRMRESTRICTION", driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), "1", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);
		Thread_Sleep(3000);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);
		ExplicitWait_Element_Visible(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver);

		System.out.println(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver));
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver)
				.equalsIgnoreCase(Invalid_Form_Message), "Message not Displayed ");

	}

	/*
	 * @Test public void PL_TC_2_6_1_1_5(){
	 * 
	 * Hover_Over_Element(locatorType, Add_Shortcut_Path, driver); //
	 * Thread.sleep(500); Widgetname = Get_Text(locatorType,
	 * Express_Checkout_Path, driver);
	 * 
	 * Click_On_Element(locatorType, Express_Checkout_Path, driver);
	 * 
	 * Click_On_Element(locatorType, Wid_Xpath(Widgetname,
	 * Wid_UserKit_SelectRecipient_Path), driver);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
	 * 
	 * ContactSearch(STATEFIRMRESTRICTION_FirstName1,
	 * STATEFIRMRESTRICTION_lastName1);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path,
	 * driver);
	 * 
	 * Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname,
	 * Expck_Form1_Path), "QA_STATEFIRMRESTRICTION", driver);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname,
	 * Express_Form_Suggestion_Path), driver);
	 * 
	 * Click_On_Element(locatorType, Wid_Xpath(Widgetname,
	 * Express_Form_Suggestion_Path), driver);
	 * 
	 * Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname,
	 * Express_Form_Qty1_Path), "1", driver);
	 * 
	 * 
	 * Click_On_Element(locatorType, Wid_Xpath(Widgetname,
	 * Expck_Checkoutbtn_Path), driver);
	 * 
	 * //
	 * 
	 * Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path , driver)
	 * .equalsIgnoreCase("QA_STATEFIRMRESTRICTION"), "Part Name MissMatch");
	 * 
	 * 
	 * 
	 * Clearcart();
	 * 
	 * 
	 * Hover_Over_Element(locatorType, Add_Shortcut_Path, driver); //
	 * Thread.sleep(500); Widgetname = Get_Text(locatorType,
	 * Express_Checkout_Path, driver);
	 * 
	 * Click_On_Element(locatorType, Express_Checkout_Path, driver);
	 * 
	 * Click_On_Element(locatorType, Wid_Xpath(Widgetname,
	 * Wid_UserKit_SelectRecipient_Path), driver);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
	 * 
	 * ContactSearch(STATEFIRMRESTRICTION_FirstName2,
	 * STATEFIRMRESTRICTION_lastName2);
	 * 
	 * Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname,
	 * Expck_Form1_Path), "QA_STATEFIRMRESTRICTION", driver);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname,
	 * Express_Form_Suggestion_Path), driver);
	 * 
	 * Click_On_Element(locatorType, Wid_Xpath(Widgetname,
	 * Express_Form_Suggestion_Path), driver);
	 * 
	 * Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname,
	 * Express_Form_Qty1_Path), "1", driver);
	 * 
	 * 
	 * Click_On_Element(locatorType, Wid_Xpath(Widgetname,
	 * Expck_Checkoutbtn_Path), driver); Thread_Sleep(3000);
	 * 
	 * ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname,
	 * Expck_Checkoutbtn_Path), driver);
	 * ExplicitWait_Element_Visible(locatorType, Wid_Xpath(Widgetname,
	 * Express_ErrorMessage_Path), driver);
	 * 
	 * System.out.println(Get_Text(locatorType, Wid_Xpath(Widgetname,
	 * Express_ErrorMessage_Path), driver));
	 * Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname,
	 * Express_ErrorMessage_Path), driver)
	 * .equalsIgnoreCase(Invalid_Form_Message), "Message not Displayed ");
	 * 
	 * }
	 */

	@Test(enabled = false)
	public void PL_TC_2_6_1_2_1() {
		Clearcart();

		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_SelectRecipient_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(FIRMRESTRICTION_FirstName1, FIRMRESTRICTION_lastName1);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Expck_Form1_Path), "QA_MULTIFUNCTION", driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), "1", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Expck_Checkoutbtn_Path), driver);

		//

		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).equalsIgnoreCase("QA_MULTIFUNCTION"),
				"Part Name MissMatch");

		// PlaceanOrder("PL_TC_2_6_1_2_1");

		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		if (Element_Isselected(locatorType, Order_Confirmation_Checkbox_Path, driver)) {
			Assert.assertTrue(Element_Isselected(locatorType, Order_Confirmation_Checkbox_Path, driver));
		} else {
			Click_On_Element(locatorType, Order_Confirmation_Checkbox_Path, driver);

		}
		if (Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver)) {
			Assert.assertTrue(Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver));
		} else {
			Click_On_Element(locatorType, Shipped_Confirmation_Checkbox_Path, driver);

		}
		Click_On_Element(locatorType, Next_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, SC_Loadingpanel_Path, driver);
		Click_On_Element(locatorType, SC_DeliverydateCalender_Path, driver);

		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);
		Click_On_Element(locatorType, SC_DeliverydateCalender_Path, driver);
		if (Is_Element_Present(locatorType, SC_Costcenterck_Path, driver)) {
			Click_On_Element(locatorType, SC_Costcenterck_Path, driver);
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "9999", driver);
		} else {
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "9999", driver);
		}
		Clear_Type_Charecters(locatorType, SC_Shipcomments_Path, "This is a QA piece", driver);
		Click_On_Element(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);

		System.out.println("Order Number placed in method  " + "PL_TC_2_6_1_2_1" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + "PL_TC_2_6_1_2_1" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	@BeforeTest
	public void Ex_login() throws IOException, InterruptedException {
		login(UserName, Password);

	}

}
