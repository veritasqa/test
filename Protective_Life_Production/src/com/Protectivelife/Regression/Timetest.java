package com.Protectivelife.Regression;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Timetest {
	
	public static String Get_FutureTime(String dateform) {
		// driver = _driver;

		try {
		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MINUTE, -625);
		return dateFormat.format(c.getTime());

		} catch (Exception e) {
		// result = "Fail";
		// reason = e.getMessage();
		return e.getMessage();
		}
		}
	
	 public static void main(String[] args){
		 
		 System.out.println(Get_FutureTime("MM/dd/YYYY HH:mm:ss a"));
	}

}
