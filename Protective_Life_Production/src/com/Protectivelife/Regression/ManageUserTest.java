package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class ManageUserTest extends BaseTest{
	
	public static final String Manageuser_Username = "Pfnu";
	public static final String Manageuser_Address1 = "913 Commerce Ct";
	public static final String Manageuser_Address2 = "";

	
	 public void ManageUserNavigation() {
		 Hover_Over_Element(locatorType, Admin_btn_Path, driver);

		 Click_On_Element(locatorType, Manage_Users_btn_Path, driver);

		 ExplicitWait_Element_Clickable(locatorType, MU_AddUser_btn_Path, driver);
		 }
	
	@Test
	public void PL_TC_2_5_9_1_3 () throws InterruptedException{
		ManageUserNavigation();
		Clear_Type_Charecters(locatorType, MU_Username_Fld_Path, Manageuser_Username, driver);
		Thread.sleep(3000);

		Click_On_Element(locatorType, MU_Username_Filter_Path, driver);
		Thread.sleep(3000);

		//Click_On_Element(locatorType, Contains_Path, driver);
		Thread.sleep(3000);

	
		ExplicitWait_Element_Visible(locatorType, MU_Result_Username_Path, driver);
		Thread.sleep(3000);
		Assert.assertTrue(Get_Text(locatorType, MU_Result_Username_Path, driver).trim().equalsIgnoreCase(Manageuser_Username), Get_Text(locatorType, MU_Result_Username_Path, driver) +"UserNames not matching");
		Thread.sleep(3000);

		Click_On_Element(locatorType, MU_Result_Edit_btn_Path, driver);
		Thread.sleep(3000);
		
		Clear_Type_Charecters(locatorType, MU_Result_Address1_fld_Path, Manageuser_Address1, driver);
	//	Clear_Type_Charecters(locatorType, MU_Result_Address2_fld_Path, Manageuser_Address2, driver);
		
		Click_On_Element(locatorType, MU_Insert_Update_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MU_Result_Edit_btn_Path, driver);
		Click_On_Element(locatorType, MU_Result_Edit_btn_Path, driver);
		
		Assert.assertTrue(Get_Attribute(locatorType, MU_Result_Address1_fld_Path,"value", driver).trim().contains(Manageuser_Address1),Get_Text(locatorType, MU_Result_Address1_fld_Path, driver) +"Address line 1 is not matching");
		//Assert.assertTrue(Get_Text(locatorType, MU_Result_Address2_fld_Path, driver).equalsIgnoreCase(Manageuser_Address2), "Address line 2 is not matching");

		
	}
	
	@Test
	public void PL_TC_2_5_9_1_4 (){
		ManageUserNavigation();
		Click_On_Element(locatorType, MU_AddUser_btn_Path, driver);
		Clear_Type_Charecters(locatorType, MU_Username_Txt_Path, "QAUSER", driver);
		Clear_Type_Charecters(locatorType, MU_Password_Txt_Path, "QAUSER", driver);
		Clear_Type_Charecters(locatorType, MU_Firstname_Txt_Path, "QAFirstName", driver);
		Clear_Type_Charecters(locatorType, MU_Lastname_Txt_Path, "QALastName", driver);
		Clear_Type_Charecters(locatorType, MU_Email_Txt_Path, EmailId, driver);

		Clear_Type_Charecters(locatorType, MU_Address1_Txt_Path,Address1, driver);
	//	Clear_Type_Charecters(locatorType, MU_Address2_Txt_Path, "", driver);
		Clear_Type_Charecters(locatorType, MU_City_Txt_Path,City, driver);
		
		Select_DropDown_VisibleText(locatorType, MU_State_Dropdown_Txt_Path, "Illinois", driver);

		
		Clear_Type_Charecters(locatorType, MU_Zipcode_Txt_Path, Zipcode, driver);

		Select_DropDown_VisibleText(locatorType, MU_CostCenter_Dropdown_Path, "99999", driver);
		
		Click_On_Element(locatorType, MU_Cancel_Btn_Path, driver);

	}
	
	
	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login(UserName,Password);

	}
}
