package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Validate_POD extends BaseTest{
	
	@Test
	public void PL_TC_2_7_1_5_1() throws IOException, InterruptedException, AWTException {

		// Validate user is able to clear cart for mailist with 205 recipients
		// as Inventory Admin

		login(InUserName, InPassword);
		Clearcart();

		Click_On_Element(locatorType, Personalized_mailing_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Click_On_Element(locatorType, MLU_Uploadtemplate_Path, driver);
		Thread_Sleep(2000);
		Fileupload(Personilizedmailinglist, driver);
		Click_On_Element(locatorType, MLU_Uploadbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click_On_Element(locatorType, MLU_Ok_Path, driver);
		Clear_Type_Charecters(locatorType, Searchbymaterial_Path, Part17, driver);
		Click_On_Element(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part17),
				Part17 + " is not displayed in the materials page");
		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(550);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Minicart_Part1_Path, driver).contains(Part17),
				Part17 + " is not displayed in the Mini shopping cart");
		Click_On_Element(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Clear_Type_Charecters(locatorType, SCO_Name_Path, "Q Auto", driver);
		Clear_Type_Charecters(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Clear_Type_Charecters(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Clear_Type_Charecters(locatorType, SCO_Location_Path, "QA Test", driver);
		Clear_Type_Charecters(locatorType, SCO_Address_Path, Address1, driver);
		Clear_Type_Charecters(locatorType, SCO_City_Path, City, driver);
		Select_DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Clear_Type_Charecters(locatorType, SCO_Zip_Path, Zipcode, driver);
		Clear_Type_Charecters(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Clear_Type_Charecters(locatorType, SCO_RSVPName_Path, InUserName, driver);
		Clear_Type_Charecters(locatorType, SCO_RSVPPhone_Path, "(222) 222-2222", driver);
		Clear_Type_Charecters(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Clear_Type_Charecters(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click_On_Element(locatorType, SCO_Savebtn_Path, driver);
		
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part17),
				Part17 + " piece is not appeared in the 'Checkout' Page");
		Click_On_Element(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertTrue(Get_Text(locatorType, SC_Proofbtn_Path, driver).equalsIgnoreCase("Proofed"),
				"Proofed button is not displays ");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Assert.assertTrue(Get_DropDown(locatorType, SC_ShipMethoddrop_Path, driver).equalsIgnoreCase("USPS"),
				"'USPS' pre-populates in 'Ship Method' drop down");
		Clear_Type_Charecters(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click_On_Element(locatorType, Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_2_7_1_b_3'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_2_7_1_b_3 is '" + OrderNumber + "'");
		
	}
	
}
