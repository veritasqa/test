package com.Protectivelife.Regression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class CRM_SSO extends BaseTest {

	@Test (enabled = true, priority=1)
	public void PL_TC_CRM_SSO_1_3_3() throws InterruptedException {
		SSOLogin(Crm_Login);
		Thread.sleep(2000);
		// Landing page
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Inadmin"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");
		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part25, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(500);

		// Search results Page

		// ExplicitWait_Element_Visible(locatorType,
		// AddtoCart_Successfullmessage_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Click_On_Element(locatorType, Checkoutbtn_Path, driver);

		// Checkout Page

		Select_DropDown_VisibleText(locatorType, Order_On_Behalf_Of_Path, "1N/A - NOT APPLICABLE", driver);

		Clear_Type_Charecters(locatorType, SC_Name_Path, Part25, driver);
		Clear_Type_Charecters(locatorType, SC_Address1_Path, Address1, driver);
		Clear_Type_Charecters(locatorType, SC_City_Path, City, driver);
		Clear_Type_Charecters(locatorType, SC_State_Path, State, driver);
		Clear_Type_Charecters(locatorType, SC_Zip_Path, Zipcode, driver);
		Clear_Type_Charecters(locatorType, SC_Email_Path, EmailId, driver);

		Click_On_Element(locatorType, Order_Confirmation_Checkbox_Path, driver);
		Click_On_Element(locatorType, Shipped_Confirmation_Checkbox_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part25),
				Part25 + " piece is not appeared in the 'Checkout' Page");
		Click_On_Element(locatorType, Next_btn_Path, driver);

		Thread.sleep(5000);

		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Name_Path, driver), "Name field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Address1_Path, driver),
				"Address field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_City_Path, driver), "City field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_State_Path, driver), "State field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Zip_Path, driver), "Zip field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Country_Path, driver),
				"Country field is not disabled");
		Assert.assertTrue(Element_Isselected(locatorType, Order_Confirmation_Checkbox_Path, driver),
				"Order Confirmation is not checked off");
		Assert.assertTrue(Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver),
				"Shipped Confirmation is not checked off");

		Thread_Sleep(2000);
		Click_On_Element(locatorType, SC_DeliverydateCalender_Path, driver);
		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);

		/*
		 * Assert.assertTrue(Get_Text(locatorType, SC_DeliveryMethod_Path,
		 * driver).equalsIgnoreCase("UPS Ground"), Get_Text(locatorType,
		 * SC_DeliveryMethod_Path, driver) + "- Delivery Method is missing ");
		 */

		Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		// Assert.assertTrue(Get_Text(locatorType,
		// SC_SC_Costcenterdrop_Path_Path, driver).equalsIgnoreCase("99999"),
		// Get_Text(locatorType, SC_SC_Costcenterdrop_Path_Path, driver) +
		// "Costcentre DropDown ");

		if (Is_Element_Present(locatorType, SC_Costcenterck_Path, driver)) {
			Click_On_Element(locatorType, SC_Costcenterck_Path, driver);
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "63103", driver);
		} else {
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "63103", driver);
		}
		Click_On_Element(locatorType, SC_Checkout_Path, driver);

		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println(OrderNumber);

		Clearcart();

		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part25, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Thread_Sleep(500);

		Clear_Type_Charecters(locatorType, Searchbymaterial_Path, Part21, driver);
		Thread_Sleep(500);
		Click_On_Element(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		Thread_Sleep(500);

		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Thread_Sleep(500);
		Click_On_Element(locatorType, Checkoutbtn_Path, driver);

		// Shopping Cart
		Assert.assertTrue(Get_Text(locatorType, SC_ChargeBackCosttotal_Path, driver).trim().contains("$0.01"),
				"ChargeBack Cost Missmatch");

		// Checkout Page

		Select_DropDown_VisibleText(locatorType, Order_On_Behalf_Of_Path, "1N/A - NOT APPLICABLE", driver);

		Clear_Type_Charecters(locatorType, SC_Name_Path, Part25, driver);
		Clear_Type_Charecters(locatorType, SC_Address1_Path, Address1, driver);
		Clear_Type_Charecters(locatorType, SC_City_Path, City, driver);
		Clear_Type_Charecters(locatorType, SC_State_Path, State, driver);
		Clear_Type_Charecters(locatorType, SC_Zip_Path, Zipcode, driver);
		Clear_Type_Charecters(locatorType, SC_Email_Path, EmailId, driver);

		Click_On_Element(locatorType, Order_Confirmation_Checkbox_Path, driver);
		Click_On_Element(locatorType, Shipped_Confirmation_Checkbox_Path, driver);

		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);

		Assert.assertTrue(Get_Text(locatorType, SC_SC_Costcenterdrop_Path_Path, driver).trim().contains("99999"),
				"Cost Centre Missmatch ");

		Assert.assertTrue(Get_Text(locatorType, SC_DeliveryMethod_Path, driver).trim().contains("UPS Ground"),
				"UPS Ground is not displayed");

		Click_On_Element(locatorType, SC_Checkout_Path, driver);

		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println(OrderNumber);

		Clearcart();

	}

	@Test (enabled = true, priority=2)
	public void PL_TC_CRM_SSO_1_3_4() throws InterruptedException {

		SSOLogin(Crm_SalesDesk_Login);
		Thread.sleep(2000);

		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(
				Get_Text(locatorType, LP_Username_Path, driver)
						.equalsIgnoreCase("Supplies For Life Agent Services Guest"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");

		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part21, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);

		// ExplicitWait_Element_Visible(locatorType,
		// AddtoCart_Successfullmessage_Path, driver);
		//
		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Thread_Sleep(500);

		Click_On_Element(locatorType, Checkoutbtn_Path, driver);

		Select_DropDown_VisibleText(locatorType, Order_On_Behalf_Of_Path, "1N/A - NOT APPLICABLE", driver);

		Clear_Type_Charecters(locatorType, SC_Name_Path, Part25, driver);
		Clear_Type_Charecters(locatorType, SC_Address1_Path, Address1, driver);
		Clear_Type_Charecters(locatorType, SC_City_Path, City, driver);
		Clear_Type_Charecters(locatorType, SC_State_Path, State, driver);
		Clear_Type_Charecters(locatorType, SC_Zip_Path, Zipcode, driver);
		Clear_Type_Charecters(locatorType, SC_Email_Path, EmailId, driver);

		Click_On_Element(locatorType, Order_Confirmation_Checkbox_Path, driver);
		Click_On_Element(locatorType, Shipped_Confirmation_Checkbox_Path, driver);
		Click_On_Element(locatorType, Next_btn_Path, driver);

		// Datepicker(SC_CalenderMonth_Path,SC_CalenderOK_Path);

		Assert.assertTrue(Get_Text(locatorType, SC_SC_Costcenterdrop_Path_Path, driver).trim().contains("99999"),
				"Cost Centre Missmatch ");

		Assert.assertTrue(Get_Text(locatorType, SC_DeliveryMethod_Path, driver).trim().contains("UPS Ground"),
				"UPS Ground is not displayed");

		Click_On_Element(locatorType, SC_Checkout_Path, driver);
	}

	@Test (enabled = true, priority=3)
	public void PL_TC_CRM_SSO_1_3_6() throws InterruptedException {

		SSOLogin(Crm_firm);
		Thread.sleep(2000);
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Salesdesk"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");
		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part20, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);

	}

	@Test (enabled = true, priority=4)
	public void PL_TC_CRM_SSO_1_3_7() throws InterruptedException {

		SSOLogin(Crm_firm2);
		Thread.sleep(2000);
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Salesdesk"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");
		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part20, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);

	}
}
