package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Validate_AgentSSOPage extends BaseTest {

	@Test(enabled = true)
	public void PL_TC_Agent_SSO_1_2_3() throws IOException, InterruptedException, AWTException {

		// Verify security roles for Agent SSO user while placing an order for
		// piece having Chargeback cost
		SSOLogin(Crm_Agent_Login);
		Type(locatorType, Fullfilment_Search_Path, Part25, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(500);
		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);

		// Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path,
		// driver).contains(Part25),
		// Part25 + " piece is not appeared in the 'Checkout' Page");
		Click(locatorType, Next_btn_Path, driver);

		ExplicitWait_Element_Not_Visible(locatorType, SC_Loadingpanel_Path, driver);
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Name_Path, driver), "Name field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Address1_Path, driver), "Address field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_City_Path, driver), "City field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_State_Path, driver), "State field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Zip_Path, driver), "Zip field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Country_Path, driver), "Country field is not disabled");
		Assert.assertTrue(Element_Isselected(locatorType, SC_UPSGround_Check_Path, driver),
				"UPS Method is not greyed out");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Crdt_Num_Path, driver),
				"Credit Card details are not displayed");

		Type(locatorType, Crdt_Num_Path, "12345678923456", driver);
		Type(locatorType, Security_Code_Path, "123", driver);
		Type(locatorType, ExpirationMonth_Path, "12", driver);
		Type(locatorType, ExpirationYear_Path, "18", driver);
		Click(locatorType, Clear_Cart_Path, driver);

	}

	@Test(enabled = true)
	public void PL_TC_Agent_SSO_1_2_4() throws IOException, InterruptedException, AWTException {

		// Verify security roles for Agent SSO user while placing an order for
		// piece having Chargeback cost
		SSOLogin(Crm_Agent_Login);
		Type(locatorType, Fullfilment_Search_Path, Part21, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(500);
		// softAssert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);

		softAssert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part21),
				Part21 + " piece is not appeared in the 'Checkout' Page");
		Click(locatorType, Next_btn_Path, driver);

		Click(locatorType, Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_Agent_SSO_1_2_4'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_Agent_SSO_1_2_4 is '" + OrderNumber + "'");

		ExplicitWait_Element_Not_Visible(locatorType, SC_Loadingpanel_Path, driver);
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Name_Path, driver), "Name field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Address1_Path, driver), "Address field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_City_Path, driver), "City field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_State_Path, driver), "State field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Zip_Path, driver), "Zip field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Country_Path, driver), "Country field is not disabled");
		Assert.assertTrue(Element_Isselected(locatorType, SC_UPSGround_Check_Path, driver),
				"UPS Method is not greyed out");
		Type(locatorType, Crdt_Num_Path, "12345678923456", driver);
		Type(locatorType, Security_Code_Path, "123", driver);
		Type(locatorType, ExpirationMonth_Path, "12", driver);
		Type(locatorType, ExpirationYear_Path, "18", driver);
		Click(locatorType, Clear_Cart_Path, driver);

	}

	@Test(enabled = true)
	public void PL_TC_Agent_SSO_1_2_5() throws IOException, InterruptedException, AWTException {

		// Verify security roles for Agent SSO user while placing an order for
		// piece having Chargeback cost
		SSOLogin(Crm_Agent_Login);
		Type(locatorType, Fullfilment_Search_Path, Part25, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(500);
		softAssert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver).trim()
				.equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1! message is not not appeared");
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);

		softAssert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part25),
				Part25 + " piece is not appeared in the 'Checkout' Page");

		Click(locatorType, Next_btn_Path, driver);

		ExplicitWait_Element_Not_Visible(locatorType, SC_Loadingpanel_Path, driver);
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Name_Path, driver), "Name field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Address1_Path, driver), "Address field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_City_Path, driver), "City field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_State_Path, driver), "State field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Zip_Path, driver), "Zip field is not disabled");
		Assert.assertFalse(Element_IsEnabled(locatorType, SC_Country_Path, driver), "Country field is not disabled");
		Assert.assertTrue(Element_Isselected(locatorType, SC_UPSGround_Check_Path, driver),
				"UPS Method is not greyed out");
		Type(locatorType, Crdt_Num_Path, "12345678923456", driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Crdt_Num_Path, driver),
				"Credit Card details are not displayed");

		Type(locatorType, Security_Code_Path, "123", driver);
		Type(locatorType, ExpirationMonth_Path, "12", driver);
		Type(locatorType, ExpirationYear_Path, "18", driver);
		Click(locatorType, Clear_Cart_Path, driver);

	}

}
