package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class FF_PartViewMode extends BaseTest {

	@Test(enabled = false)
	public void PL_TC_2_7_5_1() throws IOException, InterruptedException {
		login(UserName, Password);
		Type(locatorType, Fullfilment_Search_Path, Part22, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname_withoutfirm, Lastname_withoutfirm);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).trim().toLowerCase().contains("qa_adminonly"),
				Part22 + " is not displayed");
		Click(locatorType, Logout_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Login_btn_Path, driver);

	}

	@Test(enabled = false)
	public void PL_TC_2_7_5_2() throws InterruptedException {

		// Validate that an Admin only part is not viewable for a basic user
		// (Agents)

		SSOLogin(Crm_Agent_Login);
		Type(locatorType, Fullfilment_Search_Path, Part22, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, NoRecords_found_path, driver).equalsIgnoreCase(NoRecord_Message),
				"Parts not available message not found");

	}

	@Test(enabled = false)
	public void PL_TC_2_7_5_3() throws InterruptedException {

		// Validate that an Admin only part is not viewable for an sso user

		SSOLogin(Crm_Login2);
		Type(locatorType, Fullfilment_Search_Path, Part22, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, NoRecords_found_path, driver).equalsIgnoreCase(NoRecord_Message),
				"Parts not available message not found");

	}

	@Test(enabled = false)
	public void PL_TC_2_7_5_4() throws IOException, InterruptedException {
		Open_Url_Window_Max(URL, driver);
		login(UserName, Password);
		// On main Page
		Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Manage_Inventory_btn_Path, driver);
		// On Inventory page
		Type(locatorType, MI_SI_FormNo_Path, Part24, driver);

		Click(locatorType, MI_Searchbtn_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		// String Activity = Get_Attribute(locatorType,
		// MI_Gen_Viewability_Input_Path, Attribute_Value, driver);
		// on new Tab
		Switch_New_Tab(driver);
		// String Activity = Get_Attribute(locatorType,
		// MI_Gen_Viewability_Input_Path, Attribute_Value, driver);
		// do {
		Click(locatorType, MI_Gen_Viewability_Arrow_Path, driver);

		Thread.sleep(500);

		Doubleclick(locatorType, MI_Viewable_not_orderable_Path, driver);

		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		newview = Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, Attribute_Value, driver);

		// } while (!Activity.equalsIgnoreCase(newview));

		Switch_Old_Tab(driver);
		logout();
		login(UserName, Password);

		Type(locatorType, Fullfilment_Search_Path, Part24, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname_withoutfirm, Lastname_withoutfirm);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part24), Part24 + " is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Availability_Status, driver).equalsIgnoreCase("Not Orderable"),
				"Parts Viewability is Mismatching");

	}

	@Test(enabled = false)
	public void PL_TC_2_7_5_5() throws IOException, InterruptedException {
		Open_Url_Window_Max(URL, driver);
		login(UserName, Password);
		// On main Page
		Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Manage_Inventory_btn_Path, driver);
		// On Inventory page
		Type(locatorType, MI_SI_FormNo_Path, Part24, driver);

		Click(locatorType, MI_Searchbtn_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		// on new Tab
		Switch_New_Tab(driver);
		// String Activity = Get_Attribute(locatorType,
		// MI_Gen_Viewability_Input_Path, Attribute_Value, driver);

		// do {
		Click(locatorType, MI_Gen_Viewability_Arrow_Path, driver);

		Thread.sleep(500);

		Doubleclick(locatorType, MI_Not_Viewable_Path, driver);

		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);

		newview = Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, Attribute_Value, driver);

		// } while (!Activity.equalsIgnoreCase(newview));

		Switch_Old_Tab(driver);
		logout();
		login(UserName, Password);

		Type(locatorType, Fullfilment_Search_Path, Part24, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname_withoutfirm, Lastname_withoutfirm);
		Assert.assertTrue(Get_Text(locatorType, NoRecords_found_path, driver).equalsIgnoreCase(NoRecord_Message),
				"Parts not available message not found");

	}

	@Test(enabled = false)
	public void PL_TC_2_7_5_6() throws IOException, InterruptedException {
		Open_Url_Window_Max(URL, driver);
		login(UserName, Password);
		// On main Page
		Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Manage_Inventory_btn_Path, driver);
		// On Inventory page
		Type(locatorType, MI_SI_FormNo_Path, Part24, driver);

		Click(locatorType, MI_Searchbtn_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		// on new Tab
		Switch_New_Tab(driver);
		// String Activity = Get_Attribute(locatorType,
		// MI_Gen_Viewability_Input_Path, Attribute_Value, driver);

		// do {
		Click(locatorType, MI_Gen_Viewability_Arrow_Path, driver);

		Thread.sleep(500);

		Doubleclick(locatorType, MI_Orderable_Path, driver);

		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		// } while (!Activity.equalsIgnoreCase(newview));

		Switch_Old_Tab(driver);
		logout();
		login(UserName, Password);

		Type(locatorType, Fullfilment_Search_Path, Part24, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname_withoutfirm, Lastname_withoutfirm);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part24), Part24 + " is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Add_To_Cart_btn_Path, driver), "Add to Cart is missing ");

	}

}
