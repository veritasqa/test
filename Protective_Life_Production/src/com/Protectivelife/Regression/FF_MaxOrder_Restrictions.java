package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class FF_MaxOrder_Restrictions extends BaseTest {

	@Test(enabled = true)
	public void PL_TC_2_7_6_1() throws IOException, InterruptedException, AWTException {

		// Validate max order quantity default

		login(UserName, Password);
		Type(locatorType, Fullfilment_Search_Path, Part34, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		//ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Type(locatorType, AddtoCart_Qty_Path, "6", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver).trim()
				.equalsIgnoreCase("Max Order Quantity is 5"), "Max Order Quantity is 5 message is not not appeared");
		softAssert.assertTrue(
				Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver).equalsIgnoreCase("Shopping cart is empty."),
				"MiniShopping cart is not empty");
		Type(locatorType, AddtoCart_Qty_Path, "3", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1200);
		softAssert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver).trim()
				.equalsIgnoreCase("Successfully Added 3!"), "Successfully Added 3! message is not not appeared");
		Type(locatorType, AddtoCart_Qty_Path, "5", driver);
		//Thread_Sleep(1200);
		Wait_ajax();
		softAssert.assertTrue(
				Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver).trim()
						.equalsIgnoreCase("Max Order Quantity is 5 (3 in cart)"),
				"Max Order Quantity is 5 (3 in cart) message is not not appeared");
		Click(locatorType, Clear_Cart_Path, driver);
		softAssert.assertAll();

	}

}
