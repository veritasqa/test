package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Admin_Manage_Orders extends BaseTest {

	public void ManageOrderNavigation() {
		Hover(locatorType, Admin_btn_Path, driver);

		Click(locatorType, Manage_Orders_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MO_Searchbtn_Path, driver);
	}

	public String MO_Searchresults(String PartName) {
 
		return ".//td[text()='" + PartName + "']";

	}

	public String MO_SearchresultsSelect(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a[1]";

	}

	public String MO_SearchresultsCopy(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a[2]";

	}

	public String MO_SearchresultsCollaboration(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a[3]";

	}

	public String MO_Gen_SelectOrder(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[9]/a";

	}

	public String MO_Gen_EditStatus(String Orderno) {

		return ".//td[text()='" + Orderno + "']//following::td[10]/a";

	}

	@BeforeClass (enabled=true)
	public void Pre_requisite() throws IOException, InterruptedException {
 
		login(UserName,Password);
		Type(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		//ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part1), Part1 + " is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		PlaceanOrder("For the 'Admin Manage Order' Pre-Requisite");
		logout();

	}

	@Test (enabled=true)
	public void PL_TC_2_5_6_1_1() throws IOException, InterruptedException {

		// Ensure Select order, search order, and search results grids appear
		// appropriately

		login(UserName,Password);
		ManageOrderNavigation();

		Assert.assertTrue(Get_Text(locatorType, MO_SelectOrdertitle_Path, driver).trim().contains("Select Order"),
				"'Select Order' section is not displays in header bar");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_SelectOrderfield_Path, driver),
				"'Select Order' field is not displays in 'Select Order' section ");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Viewbtn_Path, driver),
				"'View' button is not displays");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Copybtn_Path, driver),
				"'Copy' button is not displays");

		Assert.assertTrue(Get_Text(locatorType, MO_Searchordertitle_Path, driver).trim().contains("Search Order"),
				"'Search Order' section is not displays in header bar");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Ordernofield_Path, driver),
				"'Order Number' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Trackingnofield_Path, driver),
				"'Tracking No' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Typedropdown_Path, driver),
				"'Type' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Userfield_Path, driver),
				"'User' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipperfield_Path, driver),
				"'Shipper' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Statusdrop_Path, driver),
				"'Status' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Startdatefield_Path, driver),
				"'Start Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Enddatefield_Path, driver),
				"'End Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_RecipientName_Path, driver),
				"'Recipient' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Addressfield_Path, driver),
				"'Address' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Cityfield_Path, driver),
				"'City' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Statefield_Path, driver),
				"'State' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Zipfield_Path, driver),
				"'Zip' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchbtn_Path, driver),
				"'Search' button is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Clearbtn_Path, driver),
				"'Clear' button is not displays in 'Search order' section");

		Assert.assertTrue(Get_Text(locatorType, MO_SearchResultstitle_Path, driver).trim().contains("Search Results"),
				"'Search Results' section is not displays in header bar");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_OrderNo_Path, driver).trim().equalsIgnoreCase("Order#"),
				" 'Order #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Orderedby_Path, driver).trim().equalsIgnoreCase("Ordered by"),
				" 'Ordered by' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Trackingno_Path, driver).trim().equalsIgnoreCase("Tracking#"),
				" 'Tracking #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Orderdate_Path, driver).trim().equalsIgnoreCase("Order Date"),
				" 'Order Date' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Shipper_Path, driver).trim().equalsIgnoreCase("Shipper"),
				" 'Shipper' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Status_Path, driver).trim().equalsIgnoreCase("Status"),
				" 'Status' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Recipient_Path, driver).trim().equalsIgnoreCase("Recipient"),
				" 'Recipient' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_State_Path, driver).trim().equalsIgnoreCase("State/Province"),
				" 'State/Province' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_OrderType_Path, driver).trim().equalsIgnoreCase("OrderType"),
				" 'OrderType' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_Noresult_Path, driver).trim().equalsIgnoreCase("No Order Records Found."),
				" 'No Order Records Found.'is not displays in 'Search Results' grid");

		softAssert.assertAll();
	}

	@Test(enabled=true)
	public void PL_TC_2_5_6_1_2() throws IOException, InterruptedException {

		// Verify Search and Clear functionality in Search Order works

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_Ordernofield_Path, OrderNumber, driver);
		Click(locatorType, MO_Typedropdown_arrow_Path, driver);
//		Thread_Sleep(500);
		Wait_ajax();
		Click(locatorType, li_value("PROTECTIVELIFE_FULFILLMENT"), driver);
		Type(locatorType, MO_Userfield_Path, UserName, driver);
		Type(locatorType, MO_Shipperfield_Path, "UPS Ground", driver);
//		Click(locatorType, MO_Statusdrop_arrow_Path, driver);
//		Thread_Sleep(500);
//		Click(locatorType, li_value("NEW"), driver);
		Type(locatorType, MO_Startdatefield_Path, Get_Todaydate("M/d/YYYY"), driver);
		Type(locatorType, MO_Enddatefield_Path, Get_Todaydate("M/d/YYYY"), driver);
		Type(locatorType, MO_RecipientName_Path, Firstname + " " + Lastname, driver);
		Type(locatorType, MO_Addressfield_Path, "913 Commerce ct", driver);
		Type(locatorType, MO_Cityfield_Path, "Buffalo Grove", driver);
		Type(locatorType, MO_Statefield_Path, "IL", driver);
		Type(locatorType, MO_Zipfield_Path, "60089-2375", driver);
		Wait_ajax();
//		Thread_Sleep(2000);
		Click(locatorType, MO_Searchbtn_Path, driver);
		Wait_ajax();
		//Thread_Sleep(80000);
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(OrderNumber), driver),
				"<Order #> from Pre-requisite is not displays in 'Search Results' grid");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(Firstname + " " + Lastname), driver),
				"<First Name Last Name> is not displays under 'Ordered by' column in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(locatorType, MO_SR_OrderDate1_Path, driver).trim().contains(Get_Todaydate("M/d/YYYY")));
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults("UPS Ground"), driver),
				"'UPS Ground' is not  displasy under 'Shipper' field");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults("NEW"), driver),
				"'NEW' is not displays under 'Status' column");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(Firstname + " " + Lastname), driver),
				"<First Name Last Name> is not displays under 'Recipient' column ");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults("IL"), driver),
				"'IL' is not displays under 'State/Province' column");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults("PROTECTIVELIFE_FULFILLMENT"), driver),
				" 'PROTECTIVELIFE_FULFILLMENT' is not displays under 'OrderType' column");
		Click(locatorType, MO_Clearbtn_Path, driver);
		Wait_ajax();
//		Thread_Sleep(500);
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Ordernofield_Path, "value", driver).isEmpty(),
				"'Order #' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Statusdrop_Path, "value", driver).isEmpty(),
				"'Status' dropdown is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Typedropdown_Path, "value", driver).isEmpty(),
				"'Type' dropdown is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Userfield_Path, "value", driver).isEmpty(),
				"'User' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Shipperfield_Path, "value", driver).isEmpty(),
				"'Shipper' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_RecipientName_Path, "value", driver).isEmpty(),
				"'Recipient' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Startdatefield_Path, "value", driver).isEmpty(),
				"'Start date' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Enddatefield_Path, "value", driver).isEmpty(),
				"'End Date' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Addressfield_Path, "value", driver).isEmpty(),
				"'Address' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Cityfield_Path, "value", driver).isEmpty(),
				"'City' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Statefield_Path, "value", driver).isEmpty(),
				"'State/Province' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(locatorType, MO_Zipfield_Path, "value", driver).isEmpty(),
				"'Zip/Postal Code' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");

		softAssert.assertAll();

		// Click(locatorType, MO_SearchresultsSelect(OrderNumber),
		// driver);

	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_3() throws IOException, InterruptedException {

		// Verify search order copy functionality

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_Ordernofield_Path, OrderNumber, driver);
		Click(locatorType, MO_Searchbtn_Path, driver);
		Wait_ajax();
//		Thread_Sleep(20000);
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		Click(locatorType, MO_SearchresultsCopy(OrderNumber), driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, SC_Copypartmessage_Path, driver).trim()
						.equalsIgnoreCase("Successfully Added 1 Items to Cart"),
				" 'Successfully Added 1 Items to Cart' message is not displays above 'Thumbnail'");
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part1), Part1 + " is not displayed");
		Click(locatorType, SC_Removebtn_Path, driver);
		Wait_ajax();
//		Thread_Sleep(2000);
		Assert.assertTrue(
				Get_Text(locatorType, SC_Itemremovedmessage_Path, driver).trim()
						.equalsIgnoreCase("Item(s) have been removed from your cart."),
				" 'Item(s) have been removed from your cart.' message is not displays");
		Assert.assertTrue(
				Get_Text(locatorType, SC_Noitemmessage_Path, driver).equalsIgnoreCase("No items in the cart."),
				" 'No items in the cart.' message is not displays");

	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_4() throws IOException, InterruptedException {

		// Validate that select order - view functionality works appropriately

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_SelectOrderfield_Path, OrderNumber, driver);
		Wait_ajax();
//		Thread_Sleep(80000);
		ExplicitWait_Element_Not_Visible(locatorType, MO_SelectOrderloading_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		Click(locatorType, MO_Viewbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Gen_OrderSearchbtn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MO_GeneralTab_Path, driver), "General Tab is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, MO_Gen_ManageOrdernumber_Path, driver).equalsIgnoreCase(OrderNumber),
				"<Order Number> is not displays in 'Manage Order' field on header bar");
		softAssert.assertTrue(Get_Text(locatorType, MO_Gen_Ticketnumber1_Path, driver).equalsIgnoreCase(OrderNumber),
				"<Order Number> is not displays in 'Ticket Number' field");
		softAssert.assertTrue(Get_Text(locatorType, MO_Gen_Status_Path, driver).equalsIgnoreCase("NEW"),
				" 'New' is not displays in 'Status' field on header bar");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Gen_Selectbtn1_Path, driver),
				"'Select Order' button is not displays on right side of the page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Gen_OrderSearchbtn_Path, driver),
				"Order Search' button is not displays on left side of the page");
		softAssert.assertAll();

		Click(locatorType, MO_ItemsTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Items_Nextbtn_Path, driver);
		softAssert.assertTrue(Get_Text(locatorType, MO_Items_Formnumber1_Path, driver).trim().equalsIgnoreCase(Part1),
				"'QA_Multifunction' is not displays under 'Form Number' column");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Title_Path, driver),
				"'Title' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_State_Path, driver),
				"'State' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Qty_Path, driver),
				"'Qty' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Shipped_Path, driver),
				"'Shipped' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Backorder_Path, driver),
				"'Backorder' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Print_Path, driver),
				"'Print' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Fulfillment_Path, driver),
				"'Fulfillment' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Chargeback_Path, driver),
				"'Chargeback' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Shipping_Path, driver),
				"'Shipping' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Total_Path, driver),
				"'Total' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Proof_Path, driver),
				"'Proof' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Nextbtn_Path, driver),
				"'Next' button is not displays at the end of the page on the right side");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Backbtn_Path, driver),
				"'Back' button is not displays at the end of the page on the left side");
		softAssert.assertAll();

		Click(locatorType, MO_Shippingtab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Shipping_Nextbtn_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Address_Path, driver),
				"'Address' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Shippinginst_Path, driver),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Reqdelievery_Path, driver),
				"'Required Delivery' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Shipmethod_Path, driver),
				"'Ship Method' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Shippinginfo_Path, driver),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Trackingno_Path, driver),
				"'Tracking Number' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Upgradedshipping_Path, driver),
				"'Upgraded Shipping' column is not displays in the Shipping tab");
		softAssert.assertAll();

		Click(locatorType, MO_Maillisttab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Maillist_Maillistfilebtn_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Maillist_Nextbtn_Path, driver),
				"'Next' button is not displays at the end of the page on the right side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Maillist_Backbtn_Path, driver),
				"'Back' button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Maillist_Maillistfilebtn_Path, driver),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		Click(locatorType, MO_Maillist_Maillistfilebtn_Path, driver);
		softAssert.assertAll();

		Click(locatorType, MO_AttachmentTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Attachment_Savebtn_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Type_Path, driver),
				"'Attachment Type' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Createuser_Path, driver),
				"'Create user' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Createdate_Path, driver),
				"'Create Date' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Linktofile_Path, driver),
				"'Link to File' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Refreshicon_Path, driver),
				" 'Refresh' link is not displays on the right side of the page in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_AddAttachicon_Path, driver),
				"'Add Attachment is not ' link displays in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Savebtn_Path, driver),
				"'Save' button is not displays below 'Refresh' button in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Backbtn_Path, driver),
				"'Back' button is not displays at the end of the page on the left side in the Attachments Tab");
		softAssert.assertAll();

	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_5() throws IOException, InterruptedException {

		// Validate that select order copy functionality works appropriately

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_SelectOrderfield_Path, OrderNumber, driver);
//		Thread_Sleep(80000);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, MO_SelectOrderloading_Path, driver);
		Click(locatorType, MO_Copybtn_Path, driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, SC_Copypartmessage_Path, driver)
						.equalsIgnoreCase("Successfully Added 1 Items to Cart"),
				" 'Successfully Added 1 Items to Cart' message is not displays above 'Thumbnail'");
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part1), Part1 + " is not displayed");
		Click(locatorType, SC_Removebtn_Path, driver);
//		Thread_Sleep(2000);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, SC_Itemremovedmessage_Path, driver)
						.equalsIgnoreCase("Item(s) have been removed from your cart."),
				" 'Item(s) have been removed from your cart.' message is not displays");
		Assert.assertTrue(
				Get_Text(locatorType, SC_Noitemmessage_Path, driver).equalsIgnoreCase("No items in the cart."),
				" 'No items in the cart.' message is not displays");

	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_6() throws IOException, InterruptedException {

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_SelectOrderfield_Path, OrderNumber, driver);
//		Thread_Sleep(80000);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, MO_SelectOrderloading_Path, driver);
		Click(locatorType, MO_Copybtn_Path, driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, SC_Copypartmessage_Path, driver)
						.equalsIgnoreCase("Successfully Added 1 Items to Cart"),
				" 'Successfully Added 1 Items to Cart' message is not displays above 'Thumbnail'");
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part1),
				Part1 + " is not displayed in the Checkout");
		Click(locatorType, SC_CreateOrderTemplatebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_COT_Createbtn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, SC_COT_Path, driver));
		Type(locatorType, SC_COT_Name_Path, "QA Template Test", driver);
		Type(locatorType, SC_COT_Description_Path, "QA Test", driver);
		Click(locatorType, SC_COT_Createbtn_Path, driver);
//		Thread_Sleep(2000);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, SC_Createtemplatemessage_Path, driver)
						.equalsIgnoreCase("QA Template Test order template has been successfully created."),
				" 'QA Template Test order template has been successfully created.' message is not displayed "+Get_Text(locatorType, SC_Createtemplatemessage_Path, driver));
		Click(locatorType, Home_btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Manage_Announcements_btn_Path, driver);
		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		Click(locatorType, Ordertemplate_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Ordertemplate_Form1_Path, driver).equalsIgnoreCase("QA Template Test"),
				"'QA Template Test' Order Template is not displayed in the Order Template Widget");
		Click(locatorType, Clear_Cart_Path, driver);

	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_7() throws IOException, InterruptedException {

		// Validate Collaboration works appropriately

		login(UserName,Password);
		Click(locatorType, Support_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ST_VT_Addticketicon_Path, driver);
		Click(locatorType, ST_VT_Addticketicon_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ST_CST_Createbtn_Path, driver);
		DropDown_VisibleText(locatorType, ST_CST_Categorydd_Path, "Order Requests", driver);
		ExplicitWait_Element_Visible(locatorType, ST_CST_Ordernofield_Path, driver);
		Type(locatorType, ST_CST_Ordernofield_Path, OrderNumber, driver);
		DropDown_VisibleText(locatorType, ST_CST_SubCategorydd_Path, "Incorrect Order Received", driver);
//		Thread_Sleep(2000);
		Wait_ajax();
		Click(locatorType, ST_CST_Createbtn_Path, driver);
		ManageOrderNavigation  ();
		Type(locatorType, MO_Ordernofield_Path, OrderNumber, driver);
		Click(locatorType, MO_Searchbtn_Path, driver);
//		Thread_Sleep(80000);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(OrderNumber), driver),
				"<Order #> is not displays in 'Search Results' grid");
		Click(locatorType, MO_SearchresultsCollaboration(OrderNumber), driver);
		ExplicitWait_Element_Clickable(locatorType, STM_UpdateTicketbtn_Path, driver);
		Click(locatorType, STM_Addcommenticon_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, STM_Addcommentloadingpanel_Path, driver);
		Type(locatorType, STM_Commenttext_Path, "This is test", driver);
		Click(locatorType, STM_Createcomment_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, STM_Addcommentloadingpanel_Path, driver);
		Click(locatorType, STM_UpdateTicketbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, STM_UpdateTicketmsg_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, STM_UpdateTicketmsg_Path, driver).trim()
						.equalsIgnoreCase("The Ticket was successfully updated."),
				"'The Ticket was successfully updated' message is not appears");
		ManageOrderNavigation();
		Type(locatorType, MO_Ordernofield_Path, OrderNumber, driver);
		
		Click(locatorType, MO_Searchbtn_Path, driver);
//		Thread_Sleep(80000);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(OrderNumber), driver),
				"<Order #> is not displays in 'Search Results' grid");
		Click(locatorType, MO_SearchresultsCollaboration(OrderNumber), driver);
		ExplicitWait_Element_Clickable(locatorType, STM_UpdateTicketbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, STM_Comment1_Path, driver).trim().equalsIgnoreCase("This is test"),
				"'This is test' in 'Comment' field is not displayed");
	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_8() throws IOException, InterruptedException {

		// Verify that you can select an order and works appropriately

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_Ordernofield_Path, OrderNumber, driver);
		Click(locatorType, MO_Searchbtn_Path, driver);
//		Thread_Sleep(80000);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(OrderNumber), driver),
				"<Order #> is not displays in 'Search Results' grid");
		Click(locatorType, MO_SearchresultsSelect(OrderNumber), driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Gen_OrderSearchbtn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MO_GeneralTab_Path, driver), "General Tab is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, MO_Gen_ManageOrdernumber_Path, driver).equalsIgnoreCase(OrderNumber),
				"<Order Number> is not displays in 'Manage Order' field on header bar");
		softAssert.assertTrue(Get_Text(locatorType, MO_Gen_Ticketnumber1_Path, driver).equalsIgnoreCase(OrderNumber),
				"<Order Number> is not displays in 'Ticket Number' field");
		softAssert.assertTrue(Get_Text(locatorType, MO_Gen_Status_Path, driver).equalsIgnoreCase("NEW"),
				" 'New' is not displays in 'Status' field on header bar");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Gen_Selectbtn1_Path, driver),
				"'Select Order' button is not displays on right side of the page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Gen_OrderSearchbtn_Path, driver),
				"Order Search' button is not displays on left side of the page");
		softAssert.assertAll();

		Click(locatorType, MO_ItemsTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Items_Nextbtn_Path, driver);
		softAssert.assertTrue(Get_Text(locatorType, MO_Items_Formnumber1_Path, driver).trim().equalsIgnoreCase(Part1),
				"'QA_Multifunction' is not displays under 'Form Number' column");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Title_Path, driver),
				"'Title' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_State_Path, driver),
				"'State' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Qty_Path, driver),
				"'Qty' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Shipped_Path, driver),
				"'Shipped' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Backorder_Path, driver),
				"'Backorder' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Print_Path, driver),
				"'Print' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Fulfillment_Path, driver),
				"'Fulfillment' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Chargeback_Path, driver),
				"'Chargeback' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Shipping_Path, driver),
				"'Shipping' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Total_Path, driver),
				"'Total' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Proof_Path, driver),
				"'Proof' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Nextbtn_Path, driver),
				"'Next' button is not displays at the end of the page on the right side");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Items_Backbtn_Path, driver),
				"'Back' button is not displays at the end of the page on the left side");
		softAssert.assertAll();

		Click(locatorType, MO_Shippingtab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Shipping_Nextbtn_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Address_Path, driver),
				"'Address' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Shippinginst_Path, driver),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Reqdelievery_Path, driver),
				"'Required Delivery' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Shipmethod_Path, driver),
				"'Ship Method' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Shippinginfo_Path, driver),
				"'Shipping Information' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Trackingno_Path, driver),
				"'Tracking Number' column is not displays in the Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Shipping_Upgradedshipping_Path, driver),
				"'Upgraded Shipping' column is not displays in the Shipping tab");
		softAssert.assertAll();

		Click(locatorType, MO_Maillisttab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Maillist_Maillistfilebtn_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Maillist_Nextbtn_Path, driver),
				"'Next' button is not displays at the end of the page on the right side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Maillist_Backbtn_Path, driver),
				"'Back' button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Maillist_Maillistfilebtn_Path, driver),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		Click(locatorType, MO_Maillist_Maillistfilebtn_Path, driver);
		softAssert.assertAll();

		Click(locatorType, MO_AttachmentTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Attachment_Savebtn_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Type_Path, driver),
				"'Attachment Type' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Createuser_Path, driver),
				"'Create user' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Createdate_Path, driver),
				"'Create Date' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Linktofile_Path, driver),
				"'Link to File' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Refreshicon_Path, driver),
				" 'Refresh' link is not displays on the right side of the page in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_AddAttachicon_Path, driver),
				"'Add Attachment is not ' link displays in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Savebtn_Path, driver),
				"'Save' button is not displays below 'Refresh' button in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MO_Attachment_Backbtn_Path, driver),
				"'Back' button is not displays at the end of the page on the left side in the Attachments Tab");
		softAssert.assertAll();

	}
	
	@Test(enabled=true)
	public void PL_TC_2_5_6_1_9() throws IOException, InterruptedException {

		// Verify "Tracking link" takes user directly to site

		login(UserName,Password);
		ManageOrderNavigation();
		Type(locatorType, MO_Trackingnofield_Path, "1ZTest", driver);
		Click(locatorType, MO_Statusdrop_arrow_Path, driver);
//		Thread_Sleep(500);
		Wait_ajax();
		Click(locatorType, li_value("CLOSED"), driver);
		Click(locatorType, MO_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MO_SR_Loadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MO_Searchresults(OrderNumber), driver),
				OrderNumber+" <Order #> is not displays in 'Search Results' grid");
		Click(locatorType, MO_SearchresultsSelect(OrderNumber), driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Gen_OrderSearchbtn_Path, driver);
		Click(locatorType, MO_Shippingtab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MO_Shipping_Nextbtn_Path, driver);
		Click(locatorType, MO_Shipping_1ZTest_Path, driver);
		Assert.assertTrue(Get_Title(driver).contains("UPS"), " 'UPS' page is not displays");
		NavigateBack(driver);
		Assert.assertTrue(Get_Title(driver).contains("Shipping"),
				"User is not nvaigate back to Protective life 'Shipping' page from UPS site");

	}

}