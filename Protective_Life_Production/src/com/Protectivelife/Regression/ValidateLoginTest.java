package com.Protectivelife.Regression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class ValidateLoginTest extends BaseTest{
	
	@Test
	public void PI_TC_1_0_1_2() {
		
		 Clear_Type_Charecters(locatorType, Username_Path, UserName, driver);
	 		Clear_Type_Charecters(locatorType, Password_Path, Password, driver);
	 		Click_On_Element(locatorType, Login_btn_Path, driver);
	 		
	 		Assert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));
			ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		
	}

}
