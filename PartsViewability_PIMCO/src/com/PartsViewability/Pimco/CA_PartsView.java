
package com.PartsViewability.Pimco;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.PartsViewability.CommonMethods.CommonMethods;

public class CA_PartsView extends CommonMethods {

	@Test

	public void Partsview() throws InterruptedException, IOException {

		for (int i = 0; i < Total_parts; i++) {
			Implicit_Wait(driver);
			String Parts = Part.get(i).toString().trim();
			String Activity = Action.get(i).toString().trim();

			// On main Page
			Hover_Over_Element(locatorType, Admin_Btn_Path, driver);
			// Thread.sleep(500);
			Click_On_Element(locatorType, Manage_Inventory_Path, driver);
			// On Inventory page
			Clear_Type_Charecters(locatorType, Form_Number_Path, Parts, driver);

			Click_On_Element(locatorType, Active_Arrow_Path, driver);

			ExplicitWait_Element_Visible(Parts, Active_Any_Path, driver);
			Thread.sleep(1000);
			Click_On_Element(locatorType, Active_Any_Path, driver);

			Click_On_Element(locatorType, Search_Btn_Path, driver);

			ExplicitWait_Element_Clickable(locatorType, MI_SearchresultsEdit(Parts), driver);

			// to check If there is no part available
			if (Is_Element_Present(locatorType, MI_SearchresultsEdit(Parts), driver))

			{
				Click_On_Element(locatorType, MI_SearchresultsEdit(Parts), driver);

				if (Activity.equalsIgnoreCase("Skip")) {

					Results.add("Part " + Parts + " Is skipped");

				}

				else {
					// on new Tab
					// Switch_New_Tab(driver);

					// Excel reading logic for both viewable or orderable

					do {
						if (Activity.equalsIgnoreCase("Not Viewable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Not_Viewable_Path, driver);

						}

						else if (Activity.equalsIgnoreCase("Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Viewable Not Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Viewable_not_orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Admin Only"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Admin_Only_Path, driver);

						} else

						{
							Results.add("Part " + Parts + " Available But Take a LOOk at the Second Column in excel");
						}

						// Saving the settings
						Click_On_Element(locatorType, Save_Btn_Path, driver);
						Click_On_Element(locatorType, Ok_Btn_Path, driver);

						newview = Get_Attribute(locatorType, Viewability_input, Attribute_Value, driver);

					} while (!Activity.equalsIgnoreCase(newview));

					Results.add(newview);

					// Switch_Old_Tab(driver);
				}

			}

			else {
				Results.add("Not Available");
			}

		}

	}

	@BeforeSuite
	public void beforeClass() throws IOException, InterruptedException {

		Implicit_Wait(driver);
		Open_Browser(Browser, Browserpath);
		Open_Url_Window_Max(URL, driver);
		Clear_Type_Charecters(locatorType, UserName_Path, CA_ADmin_UserName, driver);
		Clear_Type_Charecters(locatorType, Password_Path, CA_Admin_Password, driver);
		Click_On_Element(locatorType, Login_Btn_Path, driver);
		// Thread.sleep(500);
		Part = getexcel(CA_Excel_File_Path);
		Action = getexcelcol2(CA_Excel_File_Path);
		Total_parts = Part.size();
		System.out.println(Total_parts);

	}

	@AfterSuite
	public void afterClass() throws IOException {
		Writeexcel(CA_Excel_File_Path, CA_Excel_OFile_Path);
		Close_Browser(driver);
		Quit_Browser(driver);

	}
}