package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC013 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC013: Most Popular Item");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   //Click on Select a recipient link for the Most Popular Items widget
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_lblFillOutContactInfo']/a")).click();
		   
		   ////Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(5000);
		   
		   
		   //Type 1 for Qty in the first box for Most Popular Items
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_txtQuantity1']")).sendKeys("1");
		   
		   //Checkout
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_btnAddToCart']")).click();
		   
		   //clear cart
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_btnCancelOrder']/span")).click();
		   
		   //Verify not production
			//Verify Not production display on the page
			 try {
			      assertEquals("Not Production", driver.findElement(By.xpath("//*[@id='ctl00_divNotProductionLabel']")).getText());
			      System.out.println("PASS: Not Production display on the page");
			    } catch (Error e) {
			     System.out.println("***FAIL: Not Production Does not display on the page***");
			      verificationErrors.append(e.toString());
			    }
			 System.out.println("Test Case Name TC013: Completed");  
		  }
		   
			  @After
			   	public void teardown() {
			   	//close current window
			 //  	driver.quit();
			   	//close the original window
			  	driver.quit(); 
		   
		  }
		  }


		   