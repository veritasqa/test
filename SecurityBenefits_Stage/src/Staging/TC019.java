package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
//import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
//import org.openqa.selenium.NoAlertPresentException;
//import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
/*import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.mustache.Value;
import static org.junit.Assert.*;*/

public class TC019 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
	//	 private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
		
			
			  
			  
		  System.out.println("Test Case TC019: Activate, Edit, and deactivate Announcement");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(000);
		   
		   
		   //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		  //Select Manage Announcements
		  driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[4]/a/span")).click();
		   
		 //In the Announcement filter field type QA Test
		  driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']")).sendKeys("QA TESTING");
		  
		 //In the Filter Dropdown select 'contains'
		  driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']")).click();
	      driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_rfltMenu_detached']/ul/li[2]/a/span")).click();
		  
		 //Verify that you see QA Test in the Announcement Column
	      if(driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]")).isDisplayed() == true){
	       System.out.println("PASS: QA Test in the Announcement Column");
			 } else {
		    System.out.println("***FAIL: QA Test is not in the Announcement Column***");	 
	         }  
		  
		 //Click edit button
	     driver.findElement(By.id("ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl04_EditButton")).click();
		 //Clear the text in the announcement field
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[5]/a/span")).click();

	     
		 //Click on the B in the bar above the announcement field
		   driver.findElement(By.cssSelector("span.Underline")).click();
		   
		 //Click on the U in the bar above the announcement field
		   driver.findElement(By.cssSelector("span.Bold")).click();
		 
		 //Type 'QA TESTING' In the Announcement Field
		
		 //Click Update Announcement Button
		 driver.findElement(By.id("ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl05_btnSaveAnnouncement")).click();
  
		 //Verify that you see 'QA TESTING' in the Announcement Column
		 if(driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]")).isDisplayed() == true){
		       System.out.println("PASS: QA Test in the Announcement Column");
				 } else {
			    System.out.println("***FAIL: QA Test is not in the Announcement Column***");	 
		         }  
			  
		 //Click the Activate Button
		 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a")).click();

		 //Click Home
		 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
 
		/* //Under the Announcements Widget Verify you see 'QA TESTING'
		 if(driver.findElement(By.xpath("//*[@id='divAnnouncements']/span[1]")).isDisplayed() == true){
		       System.out.println("PASS: Announcements Widget display");
				 } else {
			    System.out.println("***FAIL: Announcements Widget dont display ***");	 
		         }  
			 */
		 
		 //Hover Over Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		 //Select Manage Announcements
			  driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[4]/a/span")).click();
  
		 //In the Announcement filter field type QA Test
			  driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']")).sendKeys("QA TESTING");
	  
		 //In the Filter Dropdown select 'contains'
	    driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_rfltMenu_detached']/ul/li[2]/a/span")).click();

			  
		 //Verify that you see QA Test in the Announcement Column
		 if(driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]")).isDisplayed() == true){
		       System.out.println("PASS: QA Test in the Announcement Column");
				 } else {
			    System.out.println("***FAIL: QA Test is not in the Announcement Column***");	 
		
				 }  
		
		 Thread.sleep(2000);
		 //Verify that you see the Deactivate Link
		 try {
		      assertEquals("Deactivate", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a")).getText());
		      System.out.println("PASS: Deactivate Link display");
		    } catch (Error e) {
		     System.out.println("***FAIL: Deactivate Link does not display***");
		      verificationErrors.append(e.toString());
		    }
		 
		 //Click the deactivate link
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a")).click();
		 
		 //In the Announcement filter field type QA Test
		  driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']")).sendKeys("QA TESTING");

		 //In the Filter Dropdown select 'contains'
		    driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']")).click();
			driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_rfltMenu_detached']/ul/li[2]/a/span")).click();
 
		 //Verify that you see QA Test in the Announcement Column
			 if(driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]")).isDisplayed() == true){
			       System.out.println("PASS: QA Test in the Announcement Column");
					 } else {
				    System.out.println("***FAIL: QA Test is not in the Announcement Column***");	 
			         }  
		 //Verify that you see the Activate Link
			try {
			      assertEquals("Activate", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a")).getText());
			      System.out.println("PASS: Activate Link display");
			    } catch (Error e) {
			     System.out.println("***FAIL: Activate Link does not display***");
			      verificationErrors.append(e.toString());
			    }
			
		  //Click the Home button
	       driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]")).click();
	       
	       System.out.println("Test Case Name TC019: Completed");  
		  }
		  

		 
		
		@After
		
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		   	}
		  	
		   
		  
	      
		  }


	