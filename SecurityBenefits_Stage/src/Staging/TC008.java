
package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC008 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		}	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC008: Create Order Template.");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		  
		   
		   
		    
		    
		   
		  //process to navigate on the widget..
		   /*
		   Actions act = new Actions (driver);
		   WebElement AddWidgets = driver.findElement(By.id("addWidgetSection"));
		   act.moveToElement(AddWidgets).build().perform();
		  Thread.sleep(3000);
		  */
		   
		   
		  //Type QA_Stock in the fulfillment search bar and search
		   driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_Stock");
		   driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();
		   
		  
		   
		  /* 
		   
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnAddWidgetExpressCheckout']")).click();
		   Thread.sleep(3000);
		 //select and click on recipient link
		   driver.findElement(By.linkText("select a recipient")).click();
		   */
		   
		   //Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(5000);
		   
		  
		   //Vetify QA_stock display on search results page
		/*   try {
			   assertEquals("QA_Stock", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span")).getText());
			   System.out.println("PASS : QA_Stock display on the search results page");
			 } catch (Error e) {
			  System.out.println("fail: QA_Stock does not display on the search results page");
			   verificationErrors.append(e.toString());
			 }*/
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span")).isDisplayed() == true){
			      System.out.println("PASS: QA_Stock display on the search results page");
			    } else {
			     System.out.println("FAIL:  QA_Stock does not display on the search results page");
			     
			    }
		

		   
		   
		   //add to cart
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]")).click();
		   Thread.sleep(3000);
		   
		   //checkout
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_btnSearchResultsCheckOut']")).click();
		   Thread.sleep(3000);
		   
		   //Verify checkout has quantity amount  
		   
		   try {
			   assertEquals("Checkout (1)", driver.findElement(By.xpath("//*[@id='aShoppingCart']/span")).getText());
			   System.out.println("PASS : checkout has quantity amount of one");
			 } catch (Error e) {
			  System.out.println("fail: checkout does not have quantity amount of one");
			   verificationErrors.append(e.toString());
			 }
		   
		   
		   
		  //Clear cost center field and enter 9999
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");
		 
		 //Next
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();
		   
		 //click create order template
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowCreateOrderTemplatePopup']")).click();
		 
		 Thread.sleep(4000);
		 
		// Verify Item been removed
		  /* try {
			   assertEquals("                             Create Order Template", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]")).getText());
			   System.out.println("PASS : Item(s) have been removed from your cart.");
			 } catch (Error e) {
			  System.out.println("FAIL: Item(s) have been removed from your cart does not display");
			   verificationErrors.append(e.toString());
			 }*/
		   
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]")).isDisplayed() == true){
			      System.out.println("PASS:Item(s) have been removed from your cart.");
			    } else {
			     System.out.println("FAIL: Item(s) have been removed from your cart does not display");
			     
			    }
		 
		 //enter name and description
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtOrderTemplateName']")).sendKeys("QA Test");
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtOrderTemplateDescription']")).sendKeys("QA Test Creating Order Template");
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCreateOrderTemplate']")).click();  
		   
		
		 //
		   
		  /* 
		   try {
			   assertEquals("test order template has been successfully created.", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).getText());
			   System.out.println("PASS : QA TEST order template has been successfully created.");
			 } catch (Error e) {
			  System.out.println("FAIL: QA TEST order template has not been been successfully created.");
			   verificationErrors.append(e.toString());
			 } */
		   
		 
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']")).isDisplayed() == true){
			      System.out.println("PASS: QA TEST order template has been successfully created.");
			    } else {
			     System.out.println("FAIL: QA TEST order template has not been been successfully created.");
			     
			    }
		 
		   
		   
		   
		   //clear Cart
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click(); 
		   
		   
		   
		   //Home
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		   
		 /*  System.out.println("Removing the Widget will be done manually and " +
					"that's because it does not contain a property ");
			  */
		   
		   System.out.println("Test Case Name TC008: Completed");  
			  }
		  
		  
			  @After
			   	public void teardown() {
			   	//close current window
			 //  	driver.quit();
			   	//close the original window
			  	driver.quit();
		   
			   }
		   
		   
}
		   
