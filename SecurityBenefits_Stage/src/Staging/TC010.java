
package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC010 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
	//	  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		}	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC010: Order Lookup Widget");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		  
		   
		   
		    
		    
		   
		  //process to navigate on the widget..
		   /*
		   Actions act = new Actions (driver);
		   WebElement AddWidgets = driver.findElement(By.id("addWidgetSection"));
		   act.moveToElement(AddWidgets).build().perform();
		  Thread.sleep(3000);
		  */
		   
		   
		   
		  //In the Date from field type in the order look up widget and search
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rdpOrderSearchDateFrom_dateInput']")).sendKeys("1/1/2015");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rdpOrderSearchDateTo_dateInput']")).sendKeys("1/5/2015");
           driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_btnSearch']")).click();
           
		  //Click on page 1
         // driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[8]/a")).click();
          
          //verify same order number display
          try {
		  assertEquals("1697508", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[1]")).getText());
		  System.out.println("PASS : Order number display on the page");
		 } catch (Error e) {
		  System.out.println("fail:  Order number display on the page");
		   verificationErrors.append(e.toString());
		 }
         
        
          //Click view
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[8]/a")).click();
		   
		   
		 // Verify Cancelled
		   if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblStatus']/span")).isDisplayed() == true){
			      System.out.println("PASS: Cancelled display on the page");
			    } else {
			     System.out.println("FAIL: Cencelled does not display on the page");

			    }
		   
		 //Home
		 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		
		//Enter order in lookup 
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rcbOrderNumber_Input']")).sendKeys("1697508");
   
		Thread.sleep(4000);  
		
	    // view
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_btnSubmit']")).click();

	   
		// Order number  
		 try {
			  assertEquals("1697508", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']")).getText());
			  System.out.println("PASS : Order number display on the page");
			 } catch (Error e) {
			  System.out.println("fail:  Order number display on the page");
			   verificationErrors.append(e.toString());
			 }
	        
		  
		// Verify Cancelled
	 if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblStatus']/span")).isDisplayed() == true){
		  System.out.println("PASS: Cancelled display on the page");
		 } else {
		System.out.println("FAIL: Cencelled does not display on the page");
		    }
		   
	 //Home
	 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
	 
	 
	 //Enter another Order number
	//Enter order in lookup 
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rcbOrderNumber_Input']")).sendKeys("1695527");

	Thread.sleep(4000); 
		   
		   
	//Copy
	 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_btnCopy']")).click();
	 
	//Enter contact information
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
	   Thread.sleep(9000);
	   
	   
		   
	//Verify Successfully added 4 items to cart
	   
	   try {
			  assertEquals("Successfully Added 4 Items to Cart", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).getText());
			  System.out.println("PASS : Successfully Added 4 Items to Cart");
			 } catch (Error e) {
			  System.out.println("fail:  Did not Successfully Add 4 Items to Cart");
			   verificationErrors.append(e.toString());
			 }
		   
   //Verify that you see 'IM3830005-V20140901'
	   try {
			  assertEquals("IM3830005-V20140901", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl08_lblFormNumber']")).getText());
			  System.out.println("PASS : Successfully Added 4 Items to Cart");
			 } catch (Error e) {
			  System.out.println("fail:  Did not Successfully Add 4 Items to Cart");
			   verificationErrors.append(e.toString());
			 }
		
	   try {
			  assertEquals("IM3830005-V20140901", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl10_lblFormNumber']")).getText());
			  System.out.println("PASS : Successfully Added 4 Items to Cart");
			 } catch (Error e) {
			  System.out.println("fail:  Did not Successfully Add 4 Items to Cart");
			   verificationErrors.append(e.toString());
			 }
		
	   //Verify that you see 'How To Read Your Retirement Statement-Annuities (Consumer)'
	   try {
			  assertEquals("How To Read Your Retirement Statement-Annuities (Consumer)", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblDescription']")).getText());
			  System.out.println("PASS : Message display -that you see 'How To Read Your Retirement Statement-Annuities (Consumer");
			 } catch (Error e) {
			  System.out.println("fail:  Message does not display -Verify that you see 'How To Read Your Retirement Statement-Annuities (Consumer");
			   verificationErrors.append(e.toString());
			 }
	   
	   
	  //Verify that you see '221090100-V20130301'

	   try {
			  assertEquals("221090100-V20130301", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl06_lblFormNumber']")).getText());
			  System.out.println("PASS : 221090100-V20130301 display on the message");
			 } catch (Error e) {
			  System.out.println("fail:  221090100-V2013030 does not display");
			   verificationErrors.append(e.toString());
			 }
	   
	   //Clear cart
	   
	   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();
	   
	   
	    
		   
		   //Home
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		   
		 /*  System.out.println("Removing the Widget will be done manually and " +
					"that's because it does not contain a property ");
			  */
		   
		   System.out.println("Test Case Name TC010: Completed");  
			  }
		  
			  @After
			   	public void teardown() {
			   	//close current window
			 //  	driver.quit();
			   	//close the original window
			  	driver.quit();
		   
			   }
		   
		   
}
		   
