package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC014 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC014: Manage Inventory - Tab Verifications");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   
		   //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		   //Manage Inventory
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();
	   
		   //Verify that you see the search items table
		   try {
			      assertEquals("Search Items", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_pnlSearch']/div[1]/h1")).getText());
			      System.out.println("PASS: search items display on the page");
			    } catch (Error e) {
			     System.out.println("***FAIL: search items Does not display on the page***");
			      verificationErrors.append(e.toString());
			    }
		  
		   
		  //Type QATESTPOD in the Stock # field
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QATESTPOD");

		   
		 //search  
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   
         //edit
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		 
		//Switch to new window opened
		  for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
	Thread.sleep(2000);	  
		 
	   
	 //Verify that you see 'QATestPOD : QA Test'
		 try {
		      assertEquals("QATestPOD : QA Test", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblHeader']")).getText());
		      System.out.println("PASS: QATest display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: QATest Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }
	  
		 
	// Verify that you see the General Tab
		 
		 try {
		      assertEquals("General", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[1]/a/span/span/span")).getText());
		      System.out.println("PASS: General tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: General tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }
		 
	//	Verify that you see the Attachments Tab
		 try {
		      assertEquals("Attachments", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[2]/a/span/span/span")).getText());
		      System.out.println("PASS: Attachments tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Attachments tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }
		 
		 
	//Verify that you see the Pricing Tab
		 try {
		      assertEquals("Pricing", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[3]/a/span/span/span")).getText());
		      System.out.println("PASS: Pricing tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Pricing tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }	 
		 
	//Verify that you see the Notifications Tab
		 try {
		      assertEquals("Notifications", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[4]/a/span/span/span")).getText());
		      System.out.println("PASS: Notifications tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Notifications tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }	 
		 
	// Verify that you see the Rules Tab
		 try {
		      assertEquals("Rules", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span")).getText());
		      System.out.println("PASS: Rules tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Rules tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    } 
	//Verify that you see the Metrics Tab
		 try {
		      assertEquals("Metrics", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[6]/a/span/span/span")).getText());
		      System.out.println("PASS: Metrics tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Metrics tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }
	//Verify that you see the Change History Tab
		 try {
		      assertEquals("Change History", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span")).getText());
		      System.out.println("PASS: History tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: History tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }
    // Verify that you see the PageFlex Tab
		 try {
		      assertEquals("Pageflex", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[8]/a/span/span/span")).getText());
		      System.out.println("PASS: Pageflex tab display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Pageflex tab Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }

		 
		 
		//click on attachment
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[2]/a/span/span/span")).click();

		 
		 
		// Verify that you see 'Product Thumbnail'

		   try {
			      assertEquals("Product Thumbnail", driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[3]/div/fieldset[1]/legend")).getText());
			      System.out.println("PASS: Product Thumbnail display on the Attachment page");
			    } catch (Error e) {
			     System.out.println("***FAIL: Product Thumbnail does not display on the Attachment page***");
			      verificationErrors.append(e.toString());
			    }

		/* 
		//View Thumbnail
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lnkThumbnail']")).click();
	
		Thread.sleep(3000);
		driver.close();
		
		 
		   //Switch to new window opened
		   for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		   
		   
		 //Get back to the attachments tab for QATESTPOD		   
		   driver.switchTo().defaultContent();*/
		 
		 //Verify that you see 'Online sample'
		   if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[3]/div/fieldset[2]/legend")).isDisplayed() == true){
			      System.out.println("PASS: Online sample display on the Attachment page");
			    } else {
			     System.out.println("***FAIL: Online sample does not display on the Attachment page***");

			    }
		   
		   
		/* //Click on the View Sample Link
         driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lnkSample']")).click();
         
         Thread.sleep(3000);
 		 driver.close();
 		
         
         
       //Switch to new window opened
		   for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		   
		   
		 //Get back to the attachments tab for QATESTPOD		   
		   driver.switchTo().defaultContent();
         */
		 //Verify that you see 'FINRA letter'
	
		   if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[3]/div/fieldset[3]/legend")).isDisplayed() == true){
			      System.out.println("PASS: FINRA letter display on the Sample Link page");
			    } else {
			     System.out.println("***FAIL: FINRA letter does not display on the Sample Link page***");

			    }

		 //Verify that you can see 'POD Print Specs'
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_pnlPOD']/div[1]/h1")).isDisplayed() == true){
			      System.out.println("PASS: POD Print Specs display on the Sample Link page");
			    } else {
			     System.out.println("***FAIL: POD Print Specs does not display on the Sample Link page***");

			    }
		 
		   //Verify that you see the save button
		   
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSave']")).isDisplayed() == true){
	        	 System.out.println("PASS: Save button display on the Sample Link page");
			    } else {
			     System.out.println("***=FAIL: Save button does not display on the Sample Link page***");	 
	         }
	         
		   
		
		 //Verify that you see the back button

		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnBack']")).isDisplayed() == true){
	        	 System.out.println("PASS: back button display on the Sample Link page");
			    } else {
			     System.out.println("***=FAIL: back button does not display on the Sample Link page***");	 
	         }
	         
		   
		 //Verify that you see the next button
		
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnNext']")).isDisplayed() == true){
	        	 System.out.println("PASS: next button display on the Sample Link page");
			    } else {
			     System.out.println("***FAIL: next button does not display on the Sample Link page***");	 
	         }  
         
		 
		 //Click on the Pricing Tab
         driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[3]/a/span/span/span")).click();
		 
		 //Verify that you see 'Chargeback Cost'

         if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblChargeBackCost']")).isDisplayed() == true){
		      System.out.println("PASS: Chargeback Cost display on the Pricing page");
		    } else {
		     System.out.println("***FAIL: Chargeback Cost does not display on the pricing page***");

		    }
		 
		 //Verify that you see 'Pricing Type'

         if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblPricingType']")).isDisplayed() == true){
		      System.out.println("PASS: Pricing Type display on the Pricing page");
		    } else {
		     System.out.println("***FAIL: Pricing Type does not display on the Pricing page***");

		    }
        
		 //Verify that you see 'Flat'

         try {
		      assertEquals("Flat", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_optType']/tbody/tr/td[1]/label")).getText());
		      System.out.println("PASS: Flat display on the pricing page");
		    } catch (Error e) {
		     System.out.println("***FAIL: Flat does not display on the pricing page***");
		      verificationErrors.append(e.toString());
		    }
		 
		 // Verify that you see 'Tiered'

         try {
	      assertEquals("Tiered", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_optType']/tbody/tr/td[2]/label")).getText());
	      System.out.println("PASS: Tiered display on the pricing page");
	    } catch (Error e) {
	     System.out.println("***FAIL: Tiered does not display on the pricing page***");
	      verificationErrors.append(e.toString());
	    }
		 
  
		 
		 //Verify that you see 'Is Print Cost Approved'

         if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblPrintProductionCost']")).isDisplayed() == true){
		      System.out.println("PASS: Is Print Cost Approved: display on the pricing page");
		    } else {
		     System.out.println("***FAIL: Is Print Cost Approved: does not display on the pricing page***");

		    }
                  
		 //Verify that you see 'No Cost Item'

         if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_pnlFlat']/div[1]/div/table/tbody/tr[1]/td[1]")).isDisplayed() == true){
        	 System.out.println("PASS: No Cost Item: display on the pricing page");
		    } else {
		     System.out.println("***FAIL:  No Cost Item: does not display on the pricing page***");
		

        	 
         }
		 //Verify that you see 'Print Production Cost'
         if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblPrintProductionCost']")).isDisplayed() == true){
        	 System.out.println("PASS: Print Production Cost display on the pricing page");
		    } else {
		     System.out.println("***FAIL:  Print Production Cost does not display on the pricing page***");	 
         }
         
         
         //Verify that you see the save button
 
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSave']")).isDisplayed() == true){
	        	 System.out.println("PASS: Save button display on the pricing pagee");
			    } else {
			     System.out.println("***=FAIL: Save button does not display on the pricing page***");	 
	         }
	         
		   
		
		 //Verify that you see the back button

		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnBack']")).isDisplayed() == true){
	        	 System.out.println("PASS: back button display on the pricing pagee");
			    } else {
			     System.out.println("***=FAIL: back button does not display on the pricing page***");	 
	         }
	         
		   
		 //Verify that you see the next button
		
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnNext']")).isDisplayed() == true){
	        	 System.out.println("PASS: next button display on the pricing pagee");
			    } else {
			     System.out.println("***FAIL: next button does not display on the pricing page***");	 
	         }  
         
         
		  //Click on the Notification Tab
          driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[4]/a/span/span/span")).click();
		   
		    
         //Verify that you see 'Send Reorder Point Email' 
         if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/div[1]/table/tbody/tr[1]/td[2]/span/label")).isDisplayed() == true){
        	 System.out.println("PASS:Send Reorder Point Email display on the Notification page");
		    } else {
		     System.out.println("***FAIL: Send Reorder Point Email does not display on the Notification page***");	 
         }  
     
         
		 //Verify that you see 'At Quantity Level:'

         if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/div[1]/table/tbody/tr[2]/td[1]")).isDisplayed() == true){
		      System.out.println("PASS: At Quantity Level: display");
		    } else {
		     System.out.println("FAIL: At Quantity Level: does not display");

		    }
         
         
         //Verify that you see 'Re-Order Quantity'
         if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/div[1]/table/tbody/tr[3]/td[1]")).isDisplayed() == true){
		      System.out.println("PASS: Re-Order Quantity display");
		    } else {
		     System.out.println("FAIL: Re-Order Quantity does not display");

		    }

         
         //Verify that you see 'Notification Addresses'
         if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[8]/h1")).isDisplayed() == true){
		      System.out.println("PASS: Notification Addresses Level: display");
		    } else {
		     System.out.println("FAIL: Notification Addresses does not display");

		    }
         
         //Verify that you see the save button
         
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSave']")).isDisplayed() == true){
	        	 System.out.println("PASS: Save button display on the Notification pagee");
			    } else {
			     System.out.println("***=FAIL: Save button does not display on the Notification page***");	 
	         }
	         
		   
		
		 //Verify that you see the back button

		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnBack']")).isDisplayed() == true){
	        	 System.out.println("PASS: back button display on the Notification pagee");
			    } else {
			     System.out.println("***=FAIL: back button does not display on the Notification page***");	 
	         }
	         
		   
		 //Verify that you see the next button
		
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnNext']")).isDisplayed() == true){
	        	 System.out.println("PASS: next button display on the Notification pagee");
			    } else {
			     System.out.println("***FAIL: next button does not display on the Notification page***");	 
	         }  
       
		 //Click on the Rules Tab
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span")).click();   
		   
		 //Verify that you see 'Max Order Quantity'
	 
		 if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[8]/h1")).isDisplayed() == true){
        	 System.out.println("PASS: Max Order Quantity display on the Rules page");
		    } else {
		     System.out.println("***FAIL: Max Order Quantity does not display on the Rules page***");	 
         }  
   
		
		 //Verify that you see 'Superseding'
	
		 if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[11]/h1")).isDisplayed() == true){
        	 System.out.println("PASS:  Superseding display on the Rules page");
		    } else {
		     System.out.println("***FAIL: Superseding does not display on the Rules page***");	 
         }  
   
		 
		 //Verify that you see 'Required Items'
	
		 if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[14]/h1")).isDisplayed() == true){
        	 System.out.println("PASS:  Required Items display on the rules page");
		    } else {
		     System.out.println("***FAIL: Superseding does not display on the rules page***");	 
         } 
		 
		 //Verify that you see 'State Rules'
		 
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_divStateRules']/div[1]/h1")).isDisplayed() == true){
        	 System.out.println("PASS:  State Rules display on the rules page");
		    } else {
		     System.out.println("***FAIL: State Rules does not display on the rules page***");	 
         } 
		 
		 
		 //Verify that you see 'Firm Rules'
		
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblFirmRules']")).isDisplayed() == true){
        	 System.out.println("PASS:  Firm Rules display on the rules page");
		    } else {
		     System.out.println("***FAIL: Firm Rules does not display on the rules page***");	 
         }
		 
		 //Verify that you see 'Max Order Quantity Details'
		
		 if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[21]/h1")).isDisplayed() == true){
        	 System.out.println("PASS:  Max Order Quantity Details display on the rules page");
		    } else {
		     System.out.println("***FAIL: Max Order Quantity Details does not display on the rules page***");	 
         }
		 
	      //Verify that you see the save button
         
			   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSave']")).isDisplayed() == true){
		        	 System.out.println("PASS: Save button display on the Rules pagee");
				    } else {
				     System.out.println("***=FAIL: Save button does not display on the Rules page***");	 
		         }
		         
			   
			
			 //Verify that you see the back button

			   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnBack']")).isDisplayed() == true){
		        	 System.out.println("PASS: back button display on the Rules pagee");
				    } else {
				     System.out.println("***=FAIL: back button does not display on the Rules page***");	 
		         }
		         
			   
			 //Verify that you see the next button
			
			   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnNext']")).isDisplayed() == true){
		        	 System.out.println("PASS: next button display on the Rules pagee");
				    } else {
				     System.out.println("***FAIL: next button does not display on the Rules page***");	 
		         } 
     
		 //Click on the Metrics Tab
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[6]/a/span/span/span")).click();
		 
		 //Verify that you see 'Units on Hand'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblUnitsOnHand']")).isDisplayed() == true){
        	 System.out.println("PASS: Units on Hand : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: Units on Hand : does not display on the metrics page***");	 
         }
		 
		 //Verify that you see 'Units Available'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblUnitsAvailable']")).isDisplayed() == true){
        	 System.out.println("PASS: Units Available : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: Units Available : does not display on the metrics page***");	 
         }
		 
		 //Verify that you see 'Units Allocated'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblUnitsAllocated']")).isDisplayed() == true){
        	 System.out.println("PASS: Units Allocated : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: Units Allocated : does not display on the metrics page***");	 
         }
		 //Verify that you see 'Back Order QTY'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblBackOrderQty']")).isDisplayed() == true){
        	 System.out.println("PASS: Back Order QTY : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: Back Order QTY : does not display on the metrics page***");	 
         }
		 //Verify that you see 'MTD Usage'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblMTDUsage']")).isDisplayed() == true){
        	 System.out.println("PASS: MTD Usage : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: MTD Usage :  does not display on the metrics page***");	 
         }
		 
		 //Verify that you see 'YTD Usage'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblYTDUsage']")).isDisplayed() == true){
        	 System.out.println("PASS: YTD Usage : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: YTD Usage : does not display on the metrics page***");	 
         }
		 
		 //Verify that you see 'Average Monthly Usage'
		 
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblAverageMonthlyUsage']")).isDisplayed() == true){
        	 System.out.println("PASS: Average Monthly Usage : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: Average Monthly Usage : does not display on the metrics page***");	 
         }
		 
		 //Verify that you see 'Average Monthly Downloads'
	 
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblAverageDownload']")).isDisplayed() == true){
        	 System.out.println("PASS: Average Monthly Downloads : display on the metrics page");
		    } else {
		     System.out.println("***=FAIL: Average Monthly Downloads : does not display on the metrics page***");	 
         }
		 
		 
		 //Verify that you see the back button
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnBack']")).isDisplayed() == true){
	        	 System.out.println("PASS: back button display on the Metrics pagee");
			    } else {
			     System.out.println("***=FAIL: back button does not display on the Metrics page***");	 
	         }
	         
		   
		 //Verify that you see the next button
		
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnNext']")).isDisplayed() == true){
	        	 System.out.println("PASS: next button display on the Metrics pagee");
			    } else {
			     System.out.println("***FAIL: next button does not display on the Metrics page***");	 
	         } 
		   
		 //Click on the Change History Tab
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span")).click();
		 
		 //Verify that you see 'Date'
		
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00']/thead/tr[1]/th[1]/a")).isDisplayed() == true){
        	 System.out.println("PASS: Date display on the History page");
		    } else {
		     System.out.println("***FAIL:  Date does not display on the History page***");	 
         }
		 
		 //Verify that you see 'User'
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00']/thead/tr[1]/th[2]/a")).isDisplayed() == true){
        	 System.out.println("PASS: user display on the history page");
		    } else {
		     System.out.println("***FAIL:  user does not display on the history page***");	 
         }
		 
		 if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00']/thead/tr[1]/th[3]/a")).isDisplayed() == true){
        	 System.out.println("PASS: Action display on the history page");
		    } else {
		     System.out.println("***FAIL:  Action does not display on the history page***");	 
         }
		 
		 //Verify that you see 'Action'
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00']/thead/tr[1]/th[3]/a")).isDisplayed() == true){
	        	 System.out.println("PASS: Action display on the history page");
			    } else {
			     System.out.println("***FAIL:  Action does not display on the history page***");	 
	         }
		 
		    
			 //Verify that you see the back button

			  
			   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnBack']")).isDisplayed() == true){
		        	 System.out.println("PASS: back button display on the history pagee");
				    } else {
				     System.out.println("***FAIL: back button does not display on the history page***");	 
		         }
		
		 //Click on the Home Button
         driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
         
         System.out.println("Test Case Name TC014: Completed");  
         
		  } 
         
   	  @After
	   	public void teardown() {
	   	//close current window
	 //  	driver.quit();
	   	//close the original window
   		  driver.close();
	  	driver.quit(); 
 
         
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		   
		  }
}
