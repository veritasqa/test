package Staging;

//import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
//import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
//import org.openqa.selenium.NoAlertPresentException;
//import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
/*import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.mustache.Value;
import static org.junit.Assert.*;*/

public class TC021 {



		private WebDriver driver;
		  private String baseUrl;
	//	 private StringBuffer verificationErrors = new StringBuffer();
	//	 private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
		  
			  
		  System.out.println("Test Case TC021: Reports Page");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(000);
		   
		   
		   //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		   
		   //Reports
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[12]/a/span")).click();
		   
		 //Verify that you see 'Available Reports' section
		   if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/h1")).isDisplayed() == true){
		       System.out.println("PASS: Available reoports display");
				 } else {
			    System.out.println("***FAIL: Available reoports does not display***");	 
		         }    
		 //Verify that you see 'Report Name'
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00']/thead/tr/th[1]/a")).isDisplayed() == true){
		       System.out.println("PASS: Reports name display");
				 } else {
			    System.out.println("***FAIL: Reports name does not display***");	 
		         }   
		 //Verify that you see 'Description'
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00']/thead/tr/th[2]/a")).isDisplayed() == true){
		       System.out.println("PASS: Description display");
				 } else {
			    System.out.println("***FAIL:  Description does not display***");	 
		         } 
		   
		   
		   
		 //Verify that you see 'Report Scheduler' section
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblSchedule']")).isDisplayed() == true){
		       System.out.println("PASS: Report Scheduler' section display");
				 } else {
			    System.out.println("***FAIL: Report Scheduler' section does not display***");	 
		         }  
		   
		   
		 //Verify that you see the 'My Report Subscriptions'
		   if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[11]/h1")).isDisplayed() == true){
		       System.out.println("PASS: My Report Subscriptions display");
				 } else {
			    System.out.println("***FAIL: My Report Subscriptions does not display***");	 
		         } 
		   
		   
		 //Verify that you see '8WeekUsageReport'
		   if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00__1']/td[1]")).isDisplayed() == true){
		       System.out.println("PASS: 8WeekUsageReport display");
				 } else {
			    System.out.println("***FAIL: 8WeekUsageReport does not display***");	 
		         }   
		   
		   
		 //Click on the View Report Link for the 8 Week Usage Report
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00__1']/td[3]/a")).click();
		   
		/*   
		 //Verify that you see '8 Week Usage Report'
		   if(driver.findElement(By.xpath("//*[@id='P63959b0e6e3d4d3487d5e3a6b1ae97eaoReportCell']/table/tbody/tr[1]/td/div/table/tbody/tr[2]/td[4]/table/tbody/tr/td/div")).isDisplayed() == true){
			   System.out.println("PASS: 8 Week Usage Report display");
				 } else {
			    System.out.println("***FAIL: 8 Week Usage Report display***");	 
		         }  
		   
		 //Verify that you see Literature type
		   if(driver.findElement(By.xpath("//*[@id='ParametersGridrptTest_ctl00']/tbody/tr[1]/td[1]/span")).isDisplayed() == true){
		       System.out.println("PASS: Literature type display");
				 } else {
			    System.out.println("***FAIL: Literature type does not display***");	 
		         }  
		   
		 //Verify that you see Channel
		   if(driver.findElement(By.xpath("//*[@id='ParametersGridrptTest_ctl00']/tbody/tr[1]/td[4]/span")).isDisplayed() == true){
		       System.out.println("PASS:  Channel display");
				 } else {
			    System.out.println("***FAIL:  Channel does not display***");	 
		         }  
		   
		 //In the Literature type field select POD
		 driver.findElement(By.xpath("//*[@id='rptTest_ctl00_ctl03_ddValue']")).click();
		 driver.findElement(By.xpath("//*[@id='rptTest_ctl00_ctl03_ddValue']/option[4]")).click();
		 
		   
		 //Click View Report
		 driver.findElement(By.xpath("//*[@id='rptTest_ctl00_ctl00']")).click();
		   
		 
		 //Verify that you see Report Run Criteria:  Literature Type= 'POD'
		   if(driver.findElement(By.xpath("//*[@id='P63959b0e6e3d4d3487d5e3a6b1ae97eaoReportCell']/table/tbody/tr[1]/td/div/table/tbody/tr[4]/td[3]/table/tbody/tr/td/div")).isDisplayed() == true){
		       System.out.println("PASS: Literature type display");
				 } else {
			    System.out.println("***FAIL: Literature type does not display***");	 
		         }    
		         
		 //In the select a format dropdown choose Excel
			 driver.findElement(By.xpath("//*[@id='rptTest_ctl01_ctl05_ctl00']")).click();
			 driver.findElement(By.xpath("//*[@id='rptTest_ctl01_ctl05_ctl00']/option[6]")).click();
  
		   
		 //Verify that you see the link Export
		   if(driver.findElement(By.partialLinkText("Export")).isDisplayed() == true){
		       System.out.println("PASS: Export display");
				 } else {
			    System.out.println("***FAIL: Export does not display***");	 
		         }  
		   
		 //Close the Report window by clicking the x
		 driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_cphContent_ContentWindow']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		 */
		 
		 //navigation back
		 driver.navigate().back();
		 Thread.sleep(3000);
		 /*
		 //Verify that you see Business Owner: 
		   if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/div[1]/label")).isDisplayed() == true){
		       System.out.println("PASS: Business owner display");
				 } else {
			    System.out.println("***FAIL: Business owner display***");	 
		         }*/
		   
		 //Click Home
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		   
		   System.out.println("Test Case Name TC021: Completed");  
		  }
		  
		   @After
			
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		   	}
		  	
		   
		  
	      
		  }
