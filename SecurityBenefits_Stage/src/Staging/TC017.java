package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class TC017 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC017: Add/Close Change Request");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		 //Click on Change Requests
		 driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[4]/a/span")).click();
		   
		 //Verify that you see 'Change Request: Veritas Tickets'
		 if(driver.findElement(By.xpath("//*[@id='VeritasSection']/h1")).isDisplayed() == true){
        	 System.out.println("PASS:  Change request display");
		    } else {
		     System.out.println("***FAIL: Change request dont display***");	 
         }  
		 //Verify that you see 'Change Request: Client Tickets'
		 if(driver.findElement(By.xpath("//*[@id='CustomerSection']/h1")).isDisplayed() == true){
        	 System.out.println("PASS:   Client Tickets display");
		    } else {
		     System.out.println("***FAIL:  Client Tickets dont display***");	 
         }
		 //In the Veritas Section click 'add ticket'
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']")).click();
		 
		 //Select Other from the Category dropdown
		 Select myO = new Select(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCategory']")));						
		 myO.selectByVisibleText("Other");		
		 
	Thread.sleep(4000);
		 //Select Other from the Sub Category dropdown
		 Select my1 = new Select(driver.findElement(By.xpath("//*[@id='ddlSubCategory']")));						
		 my1.selectByVisibleText("Other");		
		 
		 
		 //Type 'QA Test' in the Comments section
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtComment']")).sendKeys("QA Test");
		 
		 //Click the Create Button
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCreateSupportTicket']")).click();

		 //Verify that you see New button
		 if(driver.findElement(By.xpath("//*[@id='btnNewStatus']")).isDisplayed() == true){
        	 System.out.println("PASS:  New Buttons display");
		    } else {
		     System.out.println("***FAIL: New button dont display***");	 
         } 
		 
		 //Verify that you see In-Progress Button
		 if(driver.findElement(By.xpath("//*[@id='btnInProgressStatus']")).isDisplayed() == true){
        	 System.out.println("PASS:  in-progress display");
		    } else {
		     System.out.println("***FAIL: in progress dont display***");	 
         } 
		 //Verify that you see the Hold Button
		 if(driver.findElement(By.xpath("//*[@id='btnHoldStatus']")).isDisplayed() == true){
        	 System.out.println("PASS:  hold Buttons display");
		    } else {
		     System.out.println("***FAIL: hold button dont display***");	 
         } 
		 //Verify that you see the Closed Button
		 if(driver.findElement(By.xpath("//*[@id='btnClosedStatus']")).isDisplayed() == true){
        	 System.out.println("PASS:  Closed Buttons display");
		    } else {
		     System.out.println("***FAIL: Closed button dont display***");	 
         } 
		 //Verify that you see 'QA Test' in the comments column
		 try {
		      assertEquals("QA Test", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]")).getText());
		      System.out.println("PASS: QA Test display");
		    } catch (Error e) {
		     System.out.println("***FAIL: QA Test dont display***");
		      verificationErrors.append(e.toString());
		    } 
		 
		 //Click on the In-Progress button
		 driver.findElement(By.xpath("//*[@id='btnInProgressStatus']")).click();
Thread.sleep(2000);
		 //Verify that you see 'The Ticket was successfully updated.'
		 if(driver.findElement(By.xpath("//*[@id='TicketSuccefullyUpdated']")).isDisplayed() == true){
        	 System.out.println("PASS:  The Ticket was successfully updated display");
		    } else {
		     System.out.println("***FAIL: The Ticket was successfully updated display***");	 
         } 
		 //Click on the change requests menu button
		 driver.findElement(By.xpath("//*[@id='divLiList']/li[4]/a/span")).click();

		 //Verify that you see 'Change Request: Veritas Tickets'
		 if(driver.findElement(By.xpath("//*[@id='VeritasSection']/h1")).isDisplayed() == true){
        	 System.out.println("PASS:  Change request display");
		    } else {
		     System.out.println("***FAIL: Change request dont display***");	 
         }  
		 //Verify that you see 'Change Request: Client Tickets'
		 if(driver.findElement(By.xpath("//*[@id='CustomerSection']/h1")).isDisplayed() == true){
        	 System.out.println("PASS:   Client Tickets display");
		    } else {
		     System.out.println("***FAIL:  Client Tickets dont display***");	 
         }
		 
		 //Verify that you see 'Qa Veritas' in the Submitted By column
		 try {
		      assertEquals("Qa Veritas", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]")).getText());
		      System.out.println("PASS: QA Veritas display");
		    } catch (Error e) {
		     System.out.println("***FAIL: QA Veritas dont display***");
		      verificationErrors.append(e.toString());
		    } 
		
		 //Verify that you see 'In-Progress' in the Status column
		 try {
		      assertEquals("In-Progress", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]")).getText());
		      System.out.println("PASS: QA Veritas display");
		    } catch (Error e) {
		     System.out.println("***FAIL: QA Veritas dont display***");
		      verificationErrors.append(e.toString());
		    } 
		 
		 //Click on the View/Edit Button
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[9]/a")).click();

		 //Click on the Closed Button
		 driver.findElement(By.xpath("//*[@id='btnClosedStatus']")).click();
Thread.sleep(2000);
		 //Verify that you see 'The Ticket was successfully updated.
		 if(driver.findElement(By.xpath("//*[@id='TicketSuccefullyUpdated']")).isDisplayed() == true){
        	 System.out.println("PASS:  The Ticket was successfully updated display");
		    } else {
		     System.out.println("***FAIL: The Ticket was successfully updated display***");	 
         } 
		 
		 //Click on Change Requests
		 driver.findElement(By.xpath("//*[@id='divLiList']/li[4]/a/span")).click();

		 //Type Closed in the Status Filter Field
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']")).sendKeys("Closed");
		 Thread.sleep(2000);
		 
		 //In the Filter Dropdown select 'contains'
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']")).click();
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_rfltMenu_detached']/ul/li[2]/a/span")).click();
Thread.sleep(3000);
		 
		 //Verify that you see 'Qa Veritas' in the Submitted By column
try {
    assertEquals("Qa Veritas", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]")).getText());
    System.out.println("PASS: QA Veritas display");
  } catch (Error e) {
   System.out.println("***FAIL: QA Veritas dont display***");
    verificationErrors.append(e.toString());
  } 

		 //Verify that you see 'Closed' in the Status column
try {
    assertEquals("Closed", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]")).getText());
    System.out.println("PASS: Closed display");
  } catch (Error e) {
   System.out.println("***FAIL: Closed dont display***");
    verificationErrors.append(e.toString());
  } 


		 //Click the Home button
		 driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		 
		 System.out.println("Test Case Name TC017: Completed");  
		  }

		 
		 @After
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		 
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
		  }
}

		   