package Staging;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC003 {

	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();
	// private boolean acceptNextAlert = true;

	//
	@BeforeClass
	public void setUp() throws Exception {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--disable-web-security");
		options.addArguments("--no-proxy-server");
		options.addArguments("disable-infobars");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		options.setExperimentalOption("prefs", prefs);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\driver\\chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Driver.manage().window().maximize();
		baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";

	}

	// if you want to print the link one on the page

	// to run in other browsers

	/*
	public static void main(String[] args) {  
	  String browser = "Chrome"; // select a browser to test on
	  WebDriver driver = null;
	  
	  if(browser.equals("Mozilla"))
	  	driver = new FirefoxDriver();
	  else if (browser.equals("Chrome")){
	  	System.setProperty("webdriver.chrome.driver",System)
	  	driver = new ChromeDriver();
	  }else if (browser.equals("IE")){
	  }
	*/

	@Test
	public void testUntitled() throws Exception {

		System.out.println("Test Case Name TC003: Placing a Fulfillment Order");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		driver.findElement(By.id("btnSubmitReal")).click();

		// make sure cart is clear
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();
		Thread.sleep(5000);

		String myxp = "//*[@id='ctl00_btnQuickSearch']";
		if ("earch".equals(driver.findElement(By.xpath(myxp)).getText())) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}

		// Search part
		driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_POD Test");
		driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();

		// Verify Part display under matching items.
		// get attribute will get you whats the name,type, id, and value of the
		// field.
		String Part = driver
				.findElement(By
						.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span"))
				.getAttribute("class");
		System.out.println("Pass: QA part display on the page " + Part);

		Thread.sleep(3000);
		// click add to cart and checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]"))
				.click();
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']"))
				.click();

		/*
		boolean vresults = driver.equals("one");
		if (vresults){
		 System.out.println("pass");
		} else {
		 System.out.println("fail");
		
		}
		*/

		// Verify shipping information
		// Verify order part display
		try {
			Assert.assertEquals("QA_POD TEST",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']"))
							.getText());
			System.out.println("PASS: Part QA_POD TEST display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Part QA_POD TEST does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify name
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtName']")).getText());
			System.out.println("PASS: Qa Veritas display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Qa Veritas does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify address 1
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtAddress1']")).getText());
			System.out.println("PASS: Address display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Address does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify city
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCity']")).getText());
			System.out.println("PASS: City display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: City does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify State
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtState']")).getText());
			System.out.println("PASS: State display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: State does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify Zip
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtZip']")).getText());
			System.out.println("PASS: Zip display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Zip does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify Country
		/*
		try {
		assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCountry']/option[232]")).getText());
		System.out.println("PASS: Country display on the shipping page");
		} catch (Error e) {
		System.out.println("FAIL: Country does not display on the shipping page");
		verificationErrors.append(e.toString());
		}
		*/
		// Verify Country
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCountry']/option[232]")).isDisplayed();
		System.out.println("PASS: Country display on the shipping page");

		// Verify email
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtEmail']")).getText());
			System.out.println("PASS: Email display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Email does not display on the shipping page");
			verificationErrors.append(e.toString());
		}
		// Verify email
		/*
		if 
		(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxOrderConfirmation']")).isDisplayed());
		System.out.println("PASS:check box is selected for order confirmation");
		*/

		// verify check box are selected for shipping

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxOrderConfirmation']")) != null)
			System.out.println("PASS:check box is selected for order confirmation");
		else
			System.out.println("FAIL:checkbox is not selected for order confirmation");

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxShippedConfirmation']")) != null)
			System.out.println("PASS:check box is selected for shipped confirmation");
		else
			System.out.println("FAIL:checkbox is not selected for shipped confirmation");

		// click next and checkout
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();

		// clear cost center field and enter cost center
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");

		// checkout
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCheckout']")).click();

		/*
	
		*/
		// copy the text number

		String OrderNumber = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']"))
				.getText();
		System.out.println("Order Number is**************************   " + OrderNumber);

		// click on OK
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnOk']")).click();

		// Verify Not production display on the page
		try {
			Assert.assertEquals("Not Production",
					driver.findElement(By.xpath("//*[@id='ctl00_divNotProductionLabel']")).getText());
			System.out.println("PASS: Not Production display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: Not Production Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		/// process of canceling the order
		driver.get("https://staging.veritas-solutions.net/inventory/login.aspx");
		driver.findElement(By.xpath("//*[@id='Login']")).sendKeys("ishuweikeh");
		driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("QAlogin123");
		driver.findElement(By.xpath("//*[@id='btnSubmitReal']")).click();
		driver.findElement(By.xpath("//*[@id='Table2']/tbody/tr[39]/td/a")).click();
		driver.findElement(By.xpath("//*[@id='OrderNumber']")).clear();
		driver.findElement(By.xpath("//*[@id='OrderNumber']")).sendKeys(OrderNumber);
		driver.findElement(By.xpath("//*[@id='btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='DataList']/tbody/tr[2]/td[8]/a")).click();
		// driver.findElement(By.xpath(".//*[@id='ProjectID']/option[8]")).click();
		// driver.findElement(By.xpath("//*[@id='submit2']")).click();
		driver.findElement(By.xpath("//*[@id='CancelOrder']")).click();
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();

		System.out.println("Test Case Name TC003: Completed");

	}

	@AfterClass(enabled = false)
	public void teardown() throws Exception {
		// close current window
		driver.quit();
		// close the original window
		driver.quit();

	}
}
