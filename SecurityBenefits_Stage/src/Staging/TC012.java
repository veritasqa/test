package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC012 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC012: My Recent Orders Widget");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   
		   //In My Recent Orders Widget click the view button on the first order listed
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock7bd5b260-2bfe-4cad-a0de-a699b67f11e4_C']/table/tbody/tr[1]/td[3]/a[2]")).click();
		   
		   
		   //Verify that you see 'Order Confirmation'
		   if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphMain_divHeaderImage']/h1")).isDisplayed() == true){
			      System.out.println("PASS: Order Confirmation display");
			    } else {
			     System.out.println("FAIL: Order Confirmation display");

			    }
		   
		   
		   //Verify that you see 'Order Information'
		   if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[2]/h1")).isDisplayed() == true){
			      System.out.println("PASS: Order information display");
			    } else {
			     System.out.println("FAIL: Order information display");

			    }
		   
		  //Verify that you see 'Order Details'
		   
		   if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[4]/h1")).isDisplayed() == true){
			      System.out.println("PASS: Order Details display");
			    } else {
			     System.out.println("FAIL: Order Details display");

			    }
		   

		 //Home
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		                             
		  //Click copy button on the first order listed in the my recent orders widget
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock7bd5b260-2bfe-4cad-a0de-a699b67f11e4_C_ctl00_repOrders_ctl01_btnCopy']")).click();
		   
		  
		   ////Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(9000);

		   
		   //Verify that you see Shipping Information
		   
		   try {
				  assertEquals("Shipping Information", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblShippingTitle']")).getText());
				  System.out.println("PASS : Shipping information display");
				 } catch (Error e) {
				  System.out.println("fail:  Shipping information does not display");
				   verificationErrors.append(e.toString());
				 } 
		   
		   

		   
		   //Click Clear Cart
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();
		   System.out.println("Test Case Name TC012: Completed");  
		  }
		   
		   @After
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit(); 
		  	
}
}