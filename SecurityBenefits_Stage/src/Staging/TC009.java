
package Staging;

//import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC009 {



		private WebDriver driver;
		  private String baseUrl;
	//	 private StringBuffer verificationErrors = new StringBuffer();
	//	  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		}	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC009: Order Templates Widget");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		  
		   
		   
		    
		    
		   
		  //process to navigate on the widget..
		   /*
		   Actions act = new Actions (driver);
		   WebElement AddWidgets = driver.findElement(By.id("addWidgetSection"));
		   act.moveToElement(AddWidgets).build().perform();
		  Thread.sleep(3000);
		  */
		   
		   
		  //click on the order templates select a recipient
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDockdc803715-d1f1-4105-9a57-c19d4be800f9_C_ctl00_lblWidgetOff']/a")).click();
		   
		  /*  
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnAddWidgetExpressCheckout']")).click();
		   Thread.sleep(3000);
		 //select and click on recipient link
		   driver.findElement(By.linkText("select a recipient")).click();
		   */
		   
		   
		   //Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(5000);
		   
		  
		   //Verify QA display on the order templates widget
		/*   try {
			   assertEquals("QA_Stock", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span")).getText());
			   System.out.println("PASS : QA_Stock display on the search results page");
			 } catch (Error e) {
			  System.out.println("fail: QA_Stock does not display on the search results page");
			   verificationErrors.append(e.toString());
			 }*/
		   
		   
		   if (driver.findElement(By.xpath("//*[@id='divOrderTemplates']/tbody/tr[1]/td[1]/div")).isDisplayed() == true){
			      System.out.println("PASS: QA Test display in the widget");
			    } else {
			     System.out.println("FAIL:  QA Test does not display in the widget");
			     
			    }
		
		    
		   //Click view
		   driver.findElement(By.xpath("//*[@id='divOrderTemplates']/tbody/tr[1]/td[2]/a[1]")).click();
		   
		   
		   //Order templates display
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_divHeaderImage']/h1")).isDisplayed() == true){
			      System.out.println("PASS: Order templates display");
			    } else {
			     System.out.println("FAIL:  Order templates does not display");
			     
			    }
		   
		   
		 //creating order template display
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_divHeader']/a/span")).isDisplayed() == true){
			      System.out.println("PASS: Creating Order templates display");
			    } else {
			     System.out.println("FAIL:  Creating Order templates does not display");
			     
			    }
		      
		   
          //QA stock display
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_rgdOrderTemplateInventory_ctl00__0']/td[2]/span[1]")).isDisplayed() == true){
			      System.out.println("PASS: QA stock display on the page");
			    } else {
			     System.out.println("FAIL:  QA stock does not display on the page");
			     
			    } 
     //Desert schools
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_rgdOrderTemplateInventory_ctl00__0']/td[2]/span[2]/b")).isDisplayed() == true){
			      System.out.println("PASS: Desert Schools display on the page");
			    } else {
			     System.out.println("FAIL: Desert schools does not display on the page");
			     
			    } 

		   
		   //add to cart
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_btnOrderTemplateAddToCart']")).click();	   
		   
		   
		  // Verify qa stock
		   
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']")).isDisplayed() == true){
			      System.out.println("PASS: QA stock display on the page");
			    } else {
			     System.out.println("FAIL: QA stock does not display on the page");
			     
			    } 

		   
		   //Remove
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']")).click();	 
		   
		   //ITem removed display
		   if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']")).isDisplayed() == true){
			      System.out.println("PASS: Item(s) have been removed from your cart. display on the page");
			    } else {
			     System.out.println("FAIL:Item(s) have been removed from your cart. does not display on the page");
			     
			    } 

		   
		   
		   
		   //Home
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		   
		 /*  System.out.println("Removing the Widget will be done manually and " +
					"that's because it does not contain a property ");
			  */
		   
		   System.out.println("Test Case Name TC009: Completed");  
			  }
		  
		  
		  
			  @After
			   	public void teardown() {
			   	//close current window
			 //  	driver.quit();
			   	//close the original window
			  	driver.quit();
		   
			   }
		   
		   
}
		   
