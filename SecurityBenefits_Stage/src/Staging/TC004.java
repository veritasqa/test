package Staging;

import static org.junit.Assert.assertEquals;

//import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;



public class TC004 {
	

	 private WebDriver driver;
	  private String baseUrl;
	  private StringBuffer verificationErrors =  new StringBuffer();

	
	  @Before
	  public void setUp() throws Exception {
	    driver = new FirefoxDriver();
	    baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    
	  } 
	
	  // if you want to print the link one on the page
	  
	  
	  
	  //to run in other browsers
	  
	  /*
	  public static void main(String[] args) {  
	    String browser = "Chrome"; // select a browser to test on
	    WebDriver driver = null;
	    
	    if(browser.equals("Mozilla"))
	    	driver = new FirefoxDriver();
	    else if (browser.equals("Chrome")){
	    	System.setProperty("webdriver.chrome.driver",System)
	    	driver = new ChromeDriver();
	    }else if (browser.equals("IE")){
	    }
	  */
	  
	  


@Test
public void testUntitled() throws Exception {
	
	System.out.println("Test Case Name 004: Update and Remove Part from Fulfillment Order");

	
//Login
 driver.get(baseUrl + "/SecurityBenefit/login.aspx");
 driver.findElement(By.id("txtUserName")).clear();
 driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
 driver.findElement(By.id("txtPassword")).clear();
 driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
 driver.findElement(By.id("btnSubmit")).click();
 

//make sure cart is clear
driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

 
//Search part
 driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_POD Test");
 driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();
 
 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
 
 //Verify Part display under matching items.
 // get attribute will get you whats the name,type, id, and value of the field.
 String Part = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span")).getAttribute("class"); 
 System.out.println("Pass: QA part display on the page " +   Part  );  
  
 
 Thread.sleep(3000);
 //click add to cart and checkout
 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]")).click();
 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']")).click();
 
 //clear quantity field
driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rntxtQuantity']")).clear();
driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rntxtQuantity']")).sendKeys("10");

//update
driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnUpdate']")).click();

//Verify part was updated to 10

/*
try {
    assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).isDisplayed());
    System.out.println("PASS: Quantity for Stock # QA_POD TEST has been changed to 10 unit(s).");
  } catch (Error e) {
   System.out.println("FAIL: Quantity for Stock # QA_POD TEST has NOT been changed to 10 unit(s).");
    verificationErrors.append(e.toString());
  }*/

Thread.sleep(2000);
//remove part
driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']")).click();

/*try {
    assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).getText());
    System.out.println("PASS: Part was removed");
  } catch (Error e) {
   System.out.println("FAIL: Part was not removed.");
    verificationErrors.append(e.toString());
  }
*/
//click home
driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();


try {
    assertEquals("Training Guidelines", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rdTrainingGuides_T']/em")).getText());
    System.out.println("PASS: Training Guidelines display on the home page");
  } catch (Error e) {
   System.out.println("FAIL: Training Guidelines does NOT display on the home page");
    verificationErrors.append(e.toString());
  }

System.out.println("Test Case Name TC004: Completed");  

}

//logout and close
@After
public void teardown() throws Exception {
driver.quit();

}
}
















 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 