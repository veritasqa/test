package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
//import org.openqa.selenium.NoAlertPresentException;
//import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
/*import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.mustache.Value;*/
import static org.junit.Assert.*;

public class TC018 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		 private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
		
			
			  
			  
		  System.out.println("Test Case TC018: Placing an Order and Submitting a Cancel Request");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   
		   
		 //make sure cart is clear
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();
		   
		    
		   //Search part
		    driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QATESTPOD");
		    driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();
		    
		    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		    
		    //Verify Text on the page
		    if(driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span")).isDisplayed() == true){
	        	 System.out.println("PASS: QA Test POD display");
			    } else {
			     System.out.println("***FAIL: QA Test POD dont display***");	 
	         }  
		    
		  //Click Add to cart
		    driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]")).click();

		  //Verify Text on the page
		    try {
			      assertEquals("QATestPOD (IL)", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_divFormNumber']/a")).getText());
			      System.out.println("PASS: QATestPOD (IL) display");
			    } catch (Error e) {
			     System.out.println("***FAIL: QATestPOD (IL) dont display***");
			      verificationErrors.append(e.toString());
			    } 
			
		  //Click Checkout
		    driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']")).click();

		  //Verify Text on the page name
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtName']")).isDisplayed() == true){
	        	 System.out.println("PASS: Name display display");
			    } else {
			     System.out.println("***FAIL:  Name dont display***");	 
	         }  
		     
		  
		  //Verify Text on the page address 1
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtAddress1']")).isDisplayed() == true){
	        	 System.out.println("PASS: address 1 display");
			    } else {
			     System.out.println("***FAIL: address 1 dont display***");	 
	         }  
		    
		  //Verify Text on the page city
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCity']")).isDisplayed() == true){
	        	 System.out.println("PASS: City display");
			    } else {
			     System.out.println("***FAIL: City dont display***");	 
	         }  
		    
		  //Verify Text on the page State
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtState']")).isDisplayed() == true){
	        	 System.out.println("PASS: State display");
			    } else {
			     System.out.println("***FAIL: State display***");	 
	         }  
		    
		  //Verify Text on the page Zip
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtZip']")).isDisplayed() == true){
	        	 System.out.println("PASS: Zip display");
			    } else {
			     System.out.println("***FAIL: Zip dont display***");	 
	         }  
		    
		  //Verify Text on the page country
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCountry']")).isDisplayed() == true){
	        	 System.out.println("PASS: State display");
			    } else {
			     System.out.println("***FAIL: State dont display***");	 
	         }  
		    
		  //Verify Text on the page email
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtEmail']")).isDisplayed() == true){
	        	 System.out.println("PASS: Email display");
			    } else {
			     System.out.println("***FAIL: Email dont display***");	 
	         }  
		    
		  //Verify Text on the page
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxOrderConfirmation']")).isSelected() == true){
	        	 System.out.println("PASS: Order Confirmation is checked");
			    } else {
			     System.out.println("***FAIL: Order Confirmation is notchecked***");	 
	         }  
		    
		  //Verify Text on the page
		    if(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxShippedConfirmation']")).isSelected() == true){
	        	 System.out.println("PASS: Shipped confirmation display");
			    } else {
			     System.out.println("***FAIL: Shipeed confirmation dont display***");	 
	         }  
		    
		  
		  //Clear Cost Center Field
		  //Type 9999 in the Cost Center Field
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");
		  
		  
		  //Click Next
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click(); 
 
		  //Click Checkout
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCheckout']")).click();

		  Thread.sleep(5000);
		  //Copy order number
		  String ordernumber = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']")).getText();
		                      System.out.println( "The order Number is"  +   ordernumber);
		                      
		   
		  Thread.sleep(2000);
		  //Click on Submit Cancel Request
		 // driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[7]/a")).click(); 
		  driver.findElement(By.linkText("Submit Cancel Request")).click();
		  //Click ok for Do you want to cancel this order? 
		  Thread.sleep(2000);
		  assertTrue(closeAlertAndGetItsText().matches("^Do you want to cancel this order[\\s\\S]$"));
		  Thread.sleep(3000);
		 
		 
		//  driver.findElement(By.linkText("OK")).click();
		  Thread.sleep(3000);
		 
		 //close popup
		  driver.findElement(By.id("ctl00_ctl00_cphContent_cphSection2_btnClose")).click();
		  Thread.sleep(3000);
		  //Click on Change Requests
		  driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[4]/a/span")).click(); 

		  
		  //Verify that you see 'Qa Veritas' in the Submitted By column
		  try {
		      assertEquals("Qa Veritas", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]")).getText());
		      System.out.println("PASS: Qa Veritas display");
		    } catch (Error e) {
		     System.out.println("***FAIL: Qa Veritas dont display***");
		      verificationErrors.append(e.toString());
		    }
		  
		  //Verify that you see 'New' in the Status column
		  try {
		      assertEquals("New", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]")).getText());
		      System.out.println("PASS: New display");
		    } catch (Error e) {
		     System.out.println("***FAIL: New dont display***");
		      verificationErrors.append(e.toString());
		    }
		  
		  
		  //Click on the View/Edit Button
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[9]/a")).click(); 

		  
		  //Verify that you see 'Cancel Order' in the comments column
		  try {
		      assertEquals("Cancel Order", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]")).getText());
		      System.out.println("PASS: Cancel Order display");
		    } catch (Error e) {
		     System.out.println("***FAIL: Cancel Order display***");
		      verificationErrors.append(e.toString());
		    }
		  
		  
		  //Click on the cancel order button
		  driver.findElement(By.xpath("//*[@id='btnCancelOrder']")).click(); 

		  
		
		  
		  //Click on Change Requests
		  driver.findElement(By.xpath("//*[@id='divLiList']/li[4]/a/span")).click(); 

		  
		  //Typed Closed in the Status Filter Field
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']")).sendKeys("Closed");
			 Thread.sleep(2000);
			 
			 //In the Filter Dropdown select 'contains'
			 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']")).click();
			 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_rfltMenu_detached']/ul/li[2]/a/span")).click();
	Thread.sleep(3000);
			 
		  //Verify that you see 'Cancel Order' in the Sub Category Section
	try {
	      assertEquals("Cancel Order", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__4']/td[3]")).getText());
	      System.out.println("PASS: Cancel Order was cancelled");
	    } catch (Error e) {
	     System.out.println("***FAIL: Cancel order didnt get cancelled***");
	      verificationErrors.append(e.toString());
	    }
	  
	
		  //Verify that you see 'Qa Veritas' in the Submitted By column
	try {
	    assertEquals("Qa Veritas", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]")).getText());
	    System.out.println("PASS: QA Veritas display");
	  } catch (Error e) {
	   System.out.println("***FAIL: QA Veritas dont display***");
	    verificationErrors.append(e.toString());
	  } 

		  //Verify that you see 'Closed' in the Status column
	try {
	    assertEquals("Closed", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]")).getText());
	    System.out.println("PASS: Closed display");
	  } catch (Error e) {
	   System.out.println("***FAIL: Closed dont display***");
	    verificationErrors.append(e.toString());
	  }
		  //Click the Home button
	       driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
	       
	       System.out.println("Test Case Name TC018: Completed");  
		  }

		 
		
		@After
		
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		   	}
		  	
		  	 private String closeAlertAndGetItsText() {
				    try {
				      Alert alert = driver.switchTo().alert();
				      String alertText = alert.getText();
				      if (acceptNextAlert) {
				        alert.accept();
				      } else {
				        alert.dismiss();
				      }
				      return alertText;
				    } finally {
				      acceptNextAlert = true;
				    }	
		  	
		  	
		  	
		 }
		 
} 	
		  	 
		  	