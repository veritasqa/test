package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
//import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
//import org.openqa.selenium.NoAlertPresentException;
//import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
/*import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.mustache.Value;
import static org.junit.Assert.*;*/

public class TC020 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
	//	 private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
		  
			  
		  System.out.println("Test Case TC020: Filtering in Manage Phrases");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(000);
		   
		   
		   //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		   //Select Manage Phrases
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[8]/a/span")).click();
		   
		   //Type Login in the Name Field
           driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterTextBox_DisplayName']")).sendKeys("Login");
           
		
           //In the Filter Dropdown select 'contains'
 		  driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_DisplayName']")).click();
 	      driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_rfltMenu_detached']/ul/li[2]/a/span")).click();
 	      
 	     //Verify that you see 'Login Bad User Name and/or Password' in the name column
 	     try {
		      assertEquals("Login Bad User Name and/or Password", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00__0']/td[1]")).getText());
		      System.out.println("PASS: Login Bad User Name and/or Password display");
		    } catch (Error e) {
		     System.out.println("***FAIL: Login Bad User Name and/or Password does not display***");
		      verificationErrors.append(e.toString());
		    }
 	     
 	     //home
 	     driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
 	     
 	    System.out.println("Test Case Name TC020: Completed");  
		  }
 	    @After
		
	   	public void teardown() {
	   	//close current window
	 //  	driver.quit();
	   	//close the original window
	  	driver.quit();  
	   	}
	  	
	   
	  
      
	  }
		  
