package Staging;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Regression {

	public static WebDriver driver;
	public static String reason;
	public Alert alert = null;
	public WebElement element;
	public String today_date;
	public String Att = "";
	private boolean acceptNextAlert = true;
	public String attrib = "";
	public static String OriginalWindow = "";
	public static Scanner Enterinput = new Scanner(System.in);
	public static String captchainput = "";
	private StringBuffer verificationErrors = new StringBuffer();
	private String baseUrl = "https://staging.veritas-solutions.net/SecurityBenefitPreProd/login.aspx";

	@BeforeSuite
	public void setUp() throws Exception {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--disable-web-security");
		options.addArguments("--no-proxy-server");
		options.addArguments("disable-infobars");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		options.setExperimentalOption("prefs", prefs);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\driver\\chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Driver.manage().window().maximize();
		baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
		// Driver.manage().window().maximize(); // to maximize window
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}

	}
	
	public void Closealltabs() {
		
		OriginalWindow = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		for (String handle : ID) {
			// System.out.println(handle);

			if (!handle.equalsIgnoreCase(OriginalWindow)) {

				driver.switchTo().window(handle);
				driver.close();

			}

		}
		driver.switchTo().window(OriginalWindow);
	}

	@Test(enabled = true)
	public void TC001() throws Exception {

		System.out.println("Test Case Name TC001: Login/Logout");

		// 1-Login and logout**************************************************
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();

		// Verify Not production display on the page
		try {
			Assert.assertEquals("Not Production",
					driver.findElement(By.xpath("//*[@id='ctl00_divNotProductionLabel']")).getText());
			System.out.println("PASS: Not Production display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: Not Production Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Logout
		driver.findElement(By.id("ctl00_lbnLogout")).click();
		driver.findElement(By.xpath("//*[@id='btnSubmitReal']")).click();
		Thread.sleep(5000);
		// Verify required user and password message display

		if (driver.findElement(By.xpath("//*[@id='valSummary']")).isDisplayed() == true) {
			System.out.println("PASS: User and Password message Required");
		} else {
			System.out.println("***FAIL: User and Password message Does not display on the page***");

		}

		// Verify User Password msg display.
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmitReal")).click();
		Thread.sleep(5000);

		try {
			Assert.assertEquals("User Password is required.",
					driver.findElement(By.xpath("//*[@id='valSummary']/ul/li")).getText());
			System.out.println("PASS: User Password message Required");
		} catch (Error e) {
			System.out.println("***FAIL: User Password message Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		Thread.sleep(2000);
		// Verify user name msg display
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmitReal")).click();

		try {
			Assert.assertEquals("User Name is required",
					driver.findElement(By.xpath("//*[@id='valSummary']/ul/li")).getText());
			System.out.println("PASS: User name msg Required Display");
		} catch (Error e) {
			System.out.println("***FAIL: User name msg Does not display on the page***");
			verificationErrors.append(e.toString());

		}
		System.out.println("Test Case Name TC001: Completed");
	}

	@Test(enabled = true)
	public void TC002() throws Exception {

		System.out.println("Test Case TC002: Unobsolete Test Pieces");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmitReal")).click();
		Thread.sleep(000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_POD Test in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_POD Test");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		Thread.sleep(5000);

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(5000);
		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Item obsoleted display");
		} else {
			System.out.println("***FAIL: Item obsoleted display does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[8]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		Thread.sleep(4000);

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC002: ***************P1-Completed*************");

		// Hover over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_Stock in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_Stock");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);
		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Item obsoleted display");
		} else {
			System.out.println("***FAIL: Item obsoleted display does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC002: ***************P2-Completed*************");

		// Hover over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_Stock in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_Stock");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC002: ***************p3-Completed*************");

		System.out.println("Test Case TC002: ***************Ended*************");

	}

	@Test(enabled = true)
	public void TC003() throws Exception {

		System.out.println("Test Case Name TC003: Placing a Fulfillment Order");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmitReal")).click();

		// make sure cart is clear
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();
		Thread.sleep(5000);

		String myxp = "//*[@id='ctl00_btnQuickSearch']";
		if ("Search".equals(driver.findElement(By.xpath(myxp)).getText())) {
			System.out.println("pass");
		} else {
			System.out.println("fail");
		}

		// Search part
		driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_POD Test");
		driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();

		// Verify Part display under matching items.
		// get attribute will get you whats the name,type, id, and value of the
		// field.
		String Part = driver
				.findElement(By
						.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span"))
				.getAttribute("class");
		System.out.println("Pass: QA part display on the page " + Part);

		Thread.sleep(3000);
		// click add to cart and checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]"))
				.click();
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']"))
				.click();

		// Verify shipping information
		// Verify order part display
		try {
			Assert.assertEquals("QA_POD TEST",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']"))
							.getText());
			System.out.println("PASS: Part QA_POD TEST display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Part QA_POD TEST does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify name
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtName']")).getText());
			System.out.println("PASS: Qa Veritas display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Qa Veritas does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify address 1
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtAddress1']")).getText());
			System.out.println("PASS: Address display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Address does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify city
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCity']")).getText());
			System.out.println("PASS: City display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: City does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify State
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtState']")).getText());
			System.out.println("PASS: State display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: State does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify Zip
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtZip']")).getText());
			System.out.println("PASS: Zip display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Zip does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify Country
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCountry']/option[232]")).isDisplayed();
		System.out.println("PASS: Country display on the shipping page");

		// Verify email
		try {
			Assert.assertEquals("", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtEmail']")).getText());
			System.out.println("PASS: Email display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Email does not display on the shipping page");
			verificationErrors.append(e.toString());
		}
		// Verify email
		/*
		if 
		(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxOrderConfirmation']")).isDisplayed());
		System.out.println("PASS:check box is selected for order confirmation");
		*/

		// verify check box are selected for shipping

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxOrderConfirmation']")) != null)
			System.out.println("PASS:check box is selected for order confirmation");
		else
			System.out.println("FAIL:checkbox is not selected for order confirmation");

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxShippedConfirmation']")) != null)
			System.out.println("PASS:check box is selected for shipped confirmation");
		else
			System.out.println("FAIL:checkbox is not selected for shipped confirmation");

		// click next and checkout
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();

		// clear cost center field and enter cost center
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");

		// checkout
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCheckout']")).click();

		/*
		
		*/
		// copy the text number

		String OrderNumber = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']"))
				.getText();
		System.out.println("Order Number is**************************   " + OrderNumber);

		// click on OK
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnOk']")).click();

		// Verify Not production display on the page
		try {
			Assert.assertEquals("Not Production",
					driver.findElement(By.xpath("//*[@id='ctl00_divNotProductionLabel']")).getText());
			System.out.println("PASS: Not Production display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: Not Production Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		/// process of canceling the order
		driver.get("https://staging.veritas-solutions.net/inventory/login.aspx");
		driver.findElement(By.xpath("//*[@id='Login']")).sendKeys("ishuweikeh");
		driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("QAlogin123");
		driver.findElement(By.xpath("//*[@id='btnSubmitReal']")).click();
		driver.findElement(By.xpath("//*[@id='Table2']/tbody/tr[39]/td/a")).click();
		driver.findElement(By.xpath("//*[@id='OrderNumber']")).clear();
		driver.findElement(By.xpath("//*[@id='OrderNumber']")).sendKeys(OrderNumber);
		driver.findElement(By.xpath("//*[@id='btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='DataList']/tbody/tr[2]/td[8]/a")).click();
		// driver.findElement(By.xpath(".//*[@id='ProjectID']/option[8]")).click();
		// driver.findElement(By.xpath("//*[@id='submit2']")).click();
		driver.findElement(By.xpath("//*[@id='CancelOrder']")).click();
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();

		System.out.println("Test Case Name TC003: Completed");

	}

	@Test(enabled = true)
	public void TC004() throws Exception {

		System.out.println("Test Case Name 004: Update and Remove Part from Fulfillment Order");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();

		// make sure cart is clear
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

		// Search part
		driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_POD Test");
		driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();

		// Verify Part display under matching items.
		// get attribute will get you whats the name,type, id, and value of the
		// field.
		String Part = driver
				.findElement(By
						.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span"))
				.getAttribute("class");
		System.out.println("Pass: QA part display on the page " + Part);

		Thread.sleep(3000);
		// click add to cart and checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]"))
				.click();
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']"))
				.click();

		// clear quantity field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rntxtQuantity']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rntxtQuantity']"))
				.sendKeys("10");

		// update
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnUpdate']")).click();

		// Verify part was updated to 10

		Thread.sleep(2000);
		// remove part
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']")).click();

		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		try {
			assertEquals("Training Guidelines",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rdTrainingGuides_T']/em")).getText());
			System.out.println("PASS: Training Guidelines display on the home page");
		} catch (Error e) {
			System.out.println("FAIL: Training Guidelines does NOT display on the home page");
			verificationErrors.append(e.toString());
		}

		System.out.println("Test Case Name TC004: Completed");

	}

	@Test(enabled = true)
	public void TC005() throws Exception {

		System.out.println("Test Case Name 005: Validate Backorder Message for Fulfillment Order");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();

		// make sure cart is clear
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

		// Search part
		driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_Stock");
		driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();

		Thread.sleep(3000);
		// click add to cart and checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]"))
				.click();

		try {
			Assert.assertEquals("QA_Stock (IL)",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_divFormNumber']/a"))
							.getText());
			System.out.println("PASS: QA_Stock (IL) display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: QA_Stock (IL) does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']"))
				.click();

		// Verify shipping information
		// Verify order part display
		try {
			Assert.assertEquals("QA_Stock",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']"))
							.getText());
			System.out.println("PASS: Part QA_Stock display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Part QA_Stock does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// Verify back order msg display
		try {
			Assert.assertEquals("Backordered: 1",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_divQuantity']/span"))
							.getText());
			System.out.println("PASS: Part QA_StockBackordered: 1 display on the shipping page");
		} catch (Error e) {
			System.out.println("FAIL: Part Backordered: 1 does not display on the shipping page");
			verificationErrors.append(e.toString());
		}

		// click next
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();

		// clear cost center field and enter cost center
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");

		// checkout
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCheckout']")).click();

		String OrderNumber = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']"))
				.getText();
		System.out.println("Order Number is**************************   " + OrderNumber);

		// click on OK
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnOk']")).click();

		/// process of canceling the order
		driver.get("https://staging.veritas-solutions.net/inventory/login.aspx");
		driver.findElement(By.xpath("//*[@id='Login']")).sendKeys("ishuweikeh");
		driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("QAlogin123");
		driver.findElement(By.xpath("//*[@id='Submit']")).click();
		driver.findElement(By.xpath("//*[@id='Table2']/tbody/tr[39]/td/a")).click();
		driver.findElement(By.xpath("//*[@id='OrderNumber']")).clear();
		driver.findElement(By.xpath("//*[@id='OrderNumber']")).sendKeys(OrderNumber);
		driver.findElement(By.xpath("//*[@id='btnSearch']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='DataList']/tbody/tr[2]/td[8]/a")).click();
		Thread.sleep(2000);
		// driver.findElement(By.xpath(".//*[@id='ProjectID']/option[8]")).click();
		// driver.findElement(By.xpath("//*[@id='submit2']")).click();
		driver.findElement(By.xpath("//*[@id='CancelOrder']")).click();
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		driver.switchTo().defaultContent();

		Thread.sleep(2000);
		System.out.println("Test Case Name TC005: Completed");

	}

	@Test(enabled = true)
	public void TC006() throws Exception {

		System.out.println("Test Case PTC006: Add/Remove New Forms and Materials Widget");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		Thread.sleep(10000);

		// navigate to add Widget "New Forms and Materials Widget"
		Actions act = new Actions(driver);
		WebElement AddWidgets = driver.findElement(By.id("addWidgetSection"));
		act.moveToElement(AddWidgets).build().perform();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnAddWidgetNewProducts']")).click();
		Thread.sleep(3000);
		// select and click on recipient link
		driver.findElement(By.linkText("select a recipient")).click();

		// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(5000);

		// Verify QA Veritas display on top of the page

		try {
			Assert.assertEquals("Ordering for: Qa Veritas",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_h3OrderingFor']/i")).getText());
			System.out.println("PASS : QA Veritas display");
		} catch (Error e) {
			System.out.println("fail: QA Veritas does not display");
			verificationErrors.append(e.toString());
		}

		// Verify new forms and materials display on the widget

		System.out.println(
				"Removing the Widget will be done manually and " + "that's because it does not contain a property ");
		System.out.println("Test Case Name PTC006: Completed");

	}

	@Test(enabled = true)
	public void TC007() throws Exception {

		System.out.println("Test Case TC007: Add/Remove Express Checkout Widget. This test case will need to "
				+ "to be excuted manually because of the of the dynamic widget that keep changing the xpath");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// navigate to add Widget "New Forms and Materials Widget"

		// click on select a recipient under express checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_lblFillOutContactInfo']/a"))
				.click();

		// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(5000);

		// Enter info under Express checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_rtxtFormNumber1_Input']"))
				.sendKeys("QA_POD TEST");
		Thread.sleep(5000);
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_txtQuantity1']"))
				.sendKeys("1");
		Thread.sleep(3000);
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_btnAddToCart']"))
				.click();

		try {
			Assert.assertEquals("QA_POD TEST",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']"))
							.getText());
			System.out.println("PASS : QA_POD TEST");
		} catch (Error e) {
			System.out.println("fail: QA_POD TEST");
			verificationErrors.append(e.toString());
		}

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']")).click();
		Thread.sleep(3000);

		// Verify Item been removed
		try {
			Assert.assertEquals("Item(s) have been removed from your cart.",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).getText());
			System.out.println("PASS : Item(s) have been removed from your cart.");
		} catch (Error e) {
			System.out.println("FAIL: Item(s) have been removed from your cart does not display");
			verificationErrors.append(e.toString());
		}

		// Verify shopping cart is empty
		try {
			Assert.assertEquals("Your Shopping Cart Is Empty",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tbody/tr/td/div/div"))
							.getText());
			System.out.println("PASS : Your Shopping Cart Is Empty.");
		} catch (Error e) {
			System.out.println("FAIL: Your Shopping Cart Is Empty does not display");
			verificationErrors.append(e.toString());
		}

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println(
				"Removing the Widget will be done manually and " + "that's because it does not contain a property ");

		System.out.println("Test Case Name TC007: Completed");

	}

	@Test(enabled = true)
	public void TC008() throws Exception {

		System.out.println("Test Case TC008: Create Order Template.");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// process to navigate on the widget..

		// Type QA_Stock in the fulfillment search bar and search
		driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_Stock");
		driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();

		// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(5000);

		// Vetify QA_stock display on search results page

		if (driver
				.findElement(By
						.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA_Stock display on the search results page");
		} else {
			System.out.println("FAIL:  QA_Stock does not display on the search results page");

		}

		// add to cart
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]"))
				.click();
		Thread.sleep(3000);

		// checkout
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_btnSearchResultsCheckOut']"))
				.click();
		Thread.sleep(3000);

		// Verify checkout has quantity amount

		try {
			Assert.assertEquals("Checkout (1)",
					driver.findElement(By.xpath("//*[@id='aShoppingCart']/span")).getText());
			System.out.println("PASS : checkout has quantity amount of one");
		} catch (Error e) {
			System.out.println("fail: checkout does not have quantity amount of one");
			verificationErrors.append(e.toString());
		}

		// Clear cost center field and enter 9999
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");

		// Next
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();

		// click create order template
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowCreateOrderTemplatePopup']")).click();

		Thread.sleep(4000);

		// Verify Item been removed

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]"))
				.isDisplayed() == true) {
			System.out.println("PASS:Item(s) have been removed from your cart.");
		} else {
			System.out.println("FAIL: Item(s) have been removed from your cart does not display");

		}

		// enter name and description
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtOrderTemplateName']")).sendKeys("QA Test");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtOrderTemplateDescription']"))
				.sendKeys("QA Test Creating Order Template");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCreateOrderTemplate']")).click();

		//

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']")).isDisplayed() == true) {
			System.out.println("PASS: QA TEST order template has been successfully created.");
		} else {
			System.out.println("FAIL: QA TEST order template has not been been successfully created.");

		}

		// clear Cart
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC008: Completed");

	}

	@Test(enabled = true)
	public void TC009() throws Exception {

		System.out.println("Test Case TC009: Order Templates Widget");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// process to navigate on the widget..

		// click on the order templates select a recipient
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockdc803715-d1f1-4105-9a57-c19d4be800f9_C_ctl00_lblWidgetOff']/a"))
				.click();

		// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(5000);

		// Verify QA display on the order templates widget

		if (driver.findElement(By.xpath("//*[@id='divOrderTemplates']/tbody/tr[1]/td[1]/div")).isDisplayed() == true) {
			System.out.println("PASS: QA Test display in the widget");
		} else {
			System.out.println("FAIL:  QA Test does not display in the widget");

		}

		// Click view
		driver.findElement(By.xpath("//*[@id='divOrderTemplates']/tbody/tr[1]/td[2]/a[1]")).click();

		// Order templates display

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_divHeaderImage']/h1")).isDisplayed() == true) {
			System.out.println("PASS: Order templates display");
		} else {
			System.out.println("FAIL:  Order templates does not display");

		}

		// creating order template display

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_divHeader']/a/span"))
				.isDisplayed() == true) {
			System.out.println("PASS: Creating Order templates display");
		} else {
			System.out.println("FAIL:  Creating Order templates does not display");

		}

		// QA stock display

		if (driver
				.findElement(By
						.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_rgdOrderTemplateInventory_ctl00__0']/td[2]/span[1]"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA stock display on the page");
		} else {
			System.out.println("FAIL:  QA stock does not display on the page");

		}
		// Desert schools

		if (driver
				.findElement(By
						.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_rgdOrderTemplateInventory_ctl00__0']/td[2]/span[2]/b"))
				.isDisplayed() == true) {
			System.out.println("PASS: Desert Schools display on the page");
		} else {
			System.out.println("FAIL: Desert schools does not display on the page");

		}

		// add to cart
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_btnOrderTemplateAddToCart']"))
				.click();

		// Verify qa stock

		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA stock display on the page");
		} else {
			System.out.println("FAIL: QA stock does not display on the page");

		}

		// Remove
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']")).click();

		// ITem removed display
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']")).isDisplayed() == true) {
			System.out.println("PASS: Item(s) have been removed from your cart. display on the page");
		} else {
			System.out.println("FAIL:Item(s) have been removed from your cart. does not display on the page");

		}

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC009: Completed");

	}

	@Test(enabled = true)
	public void TC010() throws Exception {

		System.out.println("Test Case TC010: Order Lookup Widget");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// process to navigate on the widget..

		// In the Date from field type in the order look up widget and search
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rdpOrderSearchDateFrom_dateInput']"))
				.sendKeys("1/1/2015");
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rdpOrderSearchDateTo_dateInput']"))
				.sendKeys("1/5/2015");
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_btnSearch']"))
				.click();

		// Click on page 1
		// driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[8]/a")).click();

		// verify same order number display
		try {
			Assert.assertEquals("1697508",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[1]")).getText());
			System.out.println("PASS : Order number display on the page");
		} catch (Error e) {
			System.out.println("fail:  Order number display on the page");
			verificationErrors.append(e.toString());
		}

		// Click view
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[8]/a")).click();

		// Verify Cancelled
		if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblStatus']/span"))
				.isDisplayed() == true) {
			System.out.println("PASS: Cancelled display on the page");
		} else {
			System.out.println("FAIL: Cencelled does not display on the page");

		}

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		// Enter order in lookup
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rcbOrderNumber_Input']"))
				.sendKeys("1697508");

		Thread.sleep(4000);

		// view
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_btnSubmit']"))
				.click();

		// Order number
		try {
			Assert.assertEquals("1697508", driver
					.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']")).getText());
			System.out.println("PASS : Order number display on the page");
		} catch (Error e) {
			System.out.println("fail:  Order number display on the page");
			verificationErrors.append(e.toString());
		}

		// Verify Cancelled
		if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblStatus']/span"))
				.isDisplayed() == true) {
			System.out.println("PASS: Cancelled display on the page");
		} else {
			System.out.println("FAIL: Cencelled does not display on the page");
		}

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		// Enter another Order number
		// Enter order in lookup
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_rcbOrderNumber_Input']"))
				.sendKeys("1695527");

		Thread.sleep(4000);

		// Copy
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_RadDockc35436f0-0349-4c22-b75b-660b2c293db4_C_ctl00_btnCopy']"))
				.click();

		// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(9000);

		// Verify Successfully added 4 items to cart

		try {
			Assert.assertEquals("Successfully Added 4 Items to Cart",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).getText());
			System.out.println("PASS : Successfully Added 4 Items to Cart");
		} catch (Error e) {
			System.out.println("fail:  Did not Successfully Add 4 Items to Cart");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'IM3830005-V20140901'
		try {
			Assert.assertEquals("IM3830005-V20140901",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl08_lblFormNumber']"))
							.getText());
			System.out.println("PASS : Successfully Added 4 Items to Cart");
		} catch (Error e) {
			System.out.println("fail:  Did not Successfully Add 4 Items to Cart");
			verificationErrors.append(e.toString());
		}

		try {
			Assert.assertEquals("IM3830005-V20140901",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl10_lblFormNumber']"))
							.getText());
			System.out.println("PASS : Successfully Added 4 Items to Cart");
		} catch (Error e) {
			System.out.println("fail:  Did not Successfully Add 4 Items to Cart");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'How To Read Your Retirement Statement-Annuities
		// (Consumer)'
		try {
			Assert.assertEquals("How To Read Your Retirement Statement-Annuities (Consumer)",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblDescription']"))
							.getText());
			System.out.println(
					"PASS : Message display -that you see 'How To Read Your Retirement Statement-Annuities (Consumer");
		} catch (Error e) {
			System.out.println(
					"fail:  Message does not display -Verify that you see 'How To Read Your Retirement Statement-Annuities (Consumer");
			verificationErrors.append(e.toString());
		}

		// Verify that you see '221090100-V20130301'

		try {
			Assert.assertEquals("221090100-V20130301",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl06_lblFormNumber']"))
							.getText());
			System.out.println("PASS : 221090100-V20130301 display on the message");
		} catch (Error e) {
			System.out.println("fail:  221090100-V2013030 does not display");
			verificationErrors.append(e.toString());
		}

		// Clear cart

		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		/*  System.out.println("Removing the Widget will be done manually and " +
				"that's because it does not contain a property ");
		  */

		System.out.println("Test Case Name TC010: Completed");

	}

	@Test(enabled = true)
	public void TC011() throws Exception {

		System.out.println("Test Case TC011: Product Search Widget");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// In Product Search, type 'QA' in the Search for Material By field
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_txtKeyword']"))
				.sendKeys("QA");

		// Click on the Product Category
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_Arrow']"))
				.click();

		// Select Advanced Choice Annuity
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_DropDown']/div/ul/li[2]"))
				.click();

		// search
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_btnProductSearch']"))
				.click();

		//// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(9000);

		// Verify that you see 'Matching item(s) for:'

		try {
			Assert.assertEquals("Matching item(s) for:",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[1]"))
							.getText());
			System.out.println("PASS : Matching item(s) for: display");
		} catch (Error e) {
			System.out.println("fail:  Matching item(s) for: does not display");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'QA'
		try {
			Assert.assertEquals("'QA'",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[2]"))
							.getText());
			System.out.println("PASS : QA display");
		} catch (Error e) {
			System.out.println("fail:  QA does not display");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'IL'

		try {
			Assert.assertEquals("'IL'",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[3]"))
							.getText());
			System.out.println("PASS : IL display");
		} catch (Error e) {
			System.out.println("fail:  IL does not display");
			verificationErrors.append(e.toString());
		}

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		// Click on the Product Category
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_Arrow']"))
				.click();

		// Select Advanced Choice Annuity
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_DropDown']/div/ul/li[2]"))
				.click();

		// Click Search
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_btnProductSearch']"))
				.click();

		/* ////Enter contact information
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		 Thread.sleep(9000);
		*/
		// Verify you see 'Advanced Choice Annuity

		// Verify you see 'IL'
		try {
			Assert.assertEquals("'IL'",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[2]"))
							.getText());
			System.out.println("PASS : IL display");
		} catch (Error e) {
			System.out.println("fail:  IL does not display");
			verificationErrors.append(e.toString());
		}

		// Click on the Home button
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		// Click on the Product Category
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_Arrow']"))
				.click();

		// Select Advanced Choice Annuity
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_DropDown']/div/ul/li[2]"))
				.click();

		// Select Brochure from "Literature type"
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlDocumentType_Arrow']"))
				.click();
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlDocumentType_DropDown']/div/ul/li[7]"))
				.click();

		// Click Search
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_btnProductSearch']"))
				.click();

		// Verify you see 'Brochure'
		try {
			Assert.assertEquals("'Brochure'",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[2]"))
							.getText());
			System.out.println("PASS : Brochure display");
		} catch (Error e) {
			System.out.println("fail:  Brochure does not display");
			verificationErrors.append(e.toString());
		}

		// Verify you see 'IL'
		try {
			Assert.assertEquals("'IL'",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[3]"))
							.getText());
			System.out.println("PASS : IL display");
		} catch (Error e) {
			System.out.println("fail:  IL does not display");
			verificationErrors.append(e.toString());
		}

		// Click on the Home button
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC011: Completed");

	}

	@Test(enabled = true)
	public void TC012() throws Exception {

		System.out.println("Test Case TC012: My Recent Orders Widget");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// In My Recent Orders Widget click the view button on the first order
		// listed
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock7bd5b260-2bfe-4cad-a0de-a699b67f11e4_C']/table/tbody/tr[1]/td[3]/a[2]"))
				.click();

		// Verify that you see 'Order Confirmation'
		if (driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphMain_divHeaderImage']/h1"))
				.isDisplayed() == true) {
			System.out.println("PASS: Order Confirmation display");
		} else {
			System.out.println("FAIL: Order Confirmation display");

		}

		// Verify that you see 'Order Information'
		if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[2]/h1")).isDisplayed() == true) {
			System.out.println("PASS: Order information display");
		} else {
			System.out.println("FAIL: Order information display");

		}

		// Verify that you see 'Order Details'

		if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[4]/h1")).isDisplayed() == true) {
			System.out.println("PASS: Order Details display");
		} else {
			System.out.println("FAIL: Order Details display");

		}

		// Home
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		// Click copy button on the first order listed in the my recent orders
		// widget
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDock7bd5b260-2bfe-4cad-a0de-a699b67f11e4_C_ctl00_repOrders_ctl01_btnCopy']"))
				.click();

		//// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		Thread.sleep(9000);

		// Verify that you see Shipping Information

		try {
			Assert.assertEquals("Shipping Information",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblShippingTitle']")).getText());
			System.out.println("PASS : Shipping information display");
		} catch (Error e) {
			System.out.println("fail:  Shipping information does not display");
			verificationErrors.append(e.toString());
		}

		// Click Clear Cart
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();
		System.out.println("Test Case Name TC012: Completed");

	}

	@Test(enabled = true)
	public void TC013() throws Exception {

		System.out.println("Test Case TC013: Most Popular Item");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// Click on Select a recipient link for the Most Popular Items widget
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_lblFillOutContactInfo']/a"))
				.click();

		//// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(5000);

		// Type 1 for Qty in the first box for Most Popular Items
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_txtQuantity1']"))
				.sendKeys("1");

		// Checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_btnAddToCart']"))
				.click();

		// clear cart
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_btnCancelOrder']/span")).click();

		// Verify not production
		// Verify Not production display on the page
		try {
			Assert.assertEquals("Not Production",
					driver.findElement(By.xpath("//*[@id='ctl00_divNotProductionLabel']")).getText());
			System.out.println("PASS: Not Production display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: Not Production Does not display on the page***");
			verificationErrors.append(e.toString());
		}
		System.out.println("Test Case Name TC013: Completed");

	}

	@Test(enabled = true)
	public void TC014() throws Exception {

		System.out.println("Test Case TC013: Most Popular Item");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// Click on Select a recipient link for the Most Popular Items widget
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_lblFillOutContactInfo']/a"))
				.click();

		//// Enter contact information
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		Thread.sleep(5000);

		// Type 1 for Qty in the first box for Most Popular Items
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_txtQuantity1']"))
				.sendKeys("1");

		// Checkout
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_RadDockd7fcce86-40db-4804-87a3-13a57806a9d8_C_ctl00_btnAddToCart']"))
				.click();

		// clear cart
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_btnCancelOrder']/span")).click();

		// Verify not production
		// Verify Not production display on the page
		try {
			Assert.assertEquals("Not Production",
					driver.findElement(By.xpath("//*[@id='ctl00_divNotProductionLabel']")).getText());
			System.out.println("PASS: Not Production display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: Not Production Does not display on the page***");
			verificationErrors.append(e.toString());
		}
		System.out.println("Test Case Name TC013: Completed");

	}

	@Test(enabled = true)
	public void TC015() throws Exception {

		System.out.println("Test Case TC015: Manage Inventory - Search Criteria");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Verify that you see the search items table
		try {
			Assert.assertEquals("Search Items",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_pnlSearch']/div[1]/h1")).getText());
			System.out.println("PASS: search items display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: search items Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Type test in the keyword test field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtKeyWord']")).sendKeys("test");

		// clear
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();
		Thread.sleep(2000);
		// In the User Group Field Select EMO
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlAudience_Arrow']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlAudience_DropDown']/div/ul/li[6]")).click();

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Verify that you see QATESTPOD as the 2nd Stock # record in the search
		// results table
		try {
			Assert.assertEquals("QATestPOD",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__1']/td[1]/a")).getText());
			System.out.println("PASS: QATESTPOD as the 2nd Stock display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: QATESTPOD as the 2nd Stock Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Click Clear Button
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();

		// Verify that you see 'No records to display' in the Search Results
		// table
		try {
			Assert.assertEquals("No records to display.", driver
					.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00']/tbody/tr/td/div")).getText());
			System.out.println("PASS: No records to display. display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: No records to display. Does not display on the page***");
			verificationErrors.append(e.toString());
		}
		// In the Stock # Field Type TESTJILLPOD
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("TESTJILLPOD");

		// In the Active Field Select Obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlActive_Arrow']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlActive_DropDown']/div/ul/li[4]")).click();

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Verify that you see 'TESTJILLPOD'
		try {
			Assert.assertEquals("TESTJILLPOD",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[1]/a")).getText());
			System.out.println("PASS: TESTJILLPOD display. display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: TESTJILLPOD Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see Status 'O'
		try {
			Assert.assertEquals("O",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]")).getText());
			System.out.println("PASS: O display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: O Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Click Clear Button
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();

		// Verify that you see 'No records to display' in the Search Results
		// table
		try {
			Assert.assertEquals("No records to display.", driver
					.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00']/tbody/tr/td/div")).getText());
			System.out.println("PASS: No records to display. display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: No records to display. Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// In the Stock # Field Type QA_ChildItem_3_Backorder
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_ChildItem_3_Backorder");

		// In Units on Hand Select 'Out of Stock'
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlUOH_Arrow']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlUOH_DropDown']/div/ul/li[4]")).click();

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Verify that you see 'QA_ChildItem_3_Backorder
		try {
			Assert.assertEquals("QA_ChildItem_3_Backorder",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[1]/a")).getText());
			System.out.println("PASS: QA_ChildItem_3_Backorder display. display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: QA_ChildItem_3_Backorder Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see '0' in the Units on Hand Column
		try {
			Assert.assertEquals("0",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[6]")).getText());
			System.out.println("PASS: 0 display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: 0 Does not display on the page***");
			verificationErrors.append(e.toString());
		}

		// Click Clear Button
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();

		// Verify that you see 'No records to display' in the Search Results
		// table
		try {
			Assert.assertEquals("No records to display.", driver
					.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00']/tbody/tr/td/div")).getText());
			System.out.println("PASS: No records to display. display on the page");
		} catch (Error e) {
			System.out.println("***FAIL: No records to display. Does not display on the page***");
			verificationErrors.append(e.toString());
		}
		// Click the home button

		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC015: Completed");

	}

	@Test(enabled = true)
	public void TC016() throws Exception {

		System.out.println("Test Case TC016: Creating and Closing Special Project");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Special Project
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[11]/a/span")).click();

		// Verify that you see ' Special Projects : View Tickets '
		if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[2]/h1")).isDisplayed() == true) {
			System.out.println("PASS:  Special Projects display");
		} else {
			System.out.println("***FAIL: Special Projects dont display***");
		}

		// Click add ticket
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_InitInsertButton']"))
				.click();

		// Type QA Test in the Job Title Field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtJobTitle']")).sendKeys("QA Test");

		// In the Job Type Field Select "Other'
		// driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlJobType']")).click();
		// Thread.sleep(3000);
		// driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlJobType']/option[5]")).click();

		Select myO = new Select(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlJobType']")));
		myO.selectByVisibleText("Other");

		// Type QA Test in the Desciption Field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtDescription']")).sendKeys("QA Test");

		// Click Create
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSave']")).click();

		// Verify that you see 'Data has been saved.'
		try {
			assertEquals("Data has been saved.",
					driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblMessage']")).getText());
			System.out.println("PASS: Data has been saved display");
		} catch (Error e) {
			System.out.println("***FAIL: Data has been saved. dont display***");
			verificationErrors.append(e.toString());
		}
		// Click on the Progress/Files Tab
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lnkProgress']/span")).click();
		Thread.sleep(2000);
		// admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Special Project
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[11]/a/span")).click();

		// Verify you see 'QA Test'
		/* try {
		      assertEquals("QA test", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]")).getText());
		      System.out.println("PASS: QA test display");
		    } catch (Error e) {
		     System.out.println("***FAIL: QA Test dont display***");
		      verificationErrors.append(e.toString());
		    }   */

		if (driver
				.findElement(
						By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA test display ");
		} else {
			System.out.println("***FAIL: QA test does not  display  ***");
		}

		// Click View/Edit for the Special Project created
		driver.findElement(
				By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[8]/a")).click();

		// Click on the Progress/Files Tab
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lnkProgress']/span")).click();

		// Click on Add Comment / Progress/ Files
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_InitInsertButton']")).click();

		// Type 'QA Test Special Project' in the comments section
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_txtComment']"))
				.sendKeys("QA Test Special Project");

		// Select Canceled from the Type field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Arrow']")).click();
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_DropDown']/div/ul/li[2]"))
				.click();

		// Click Submit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnSaveComment']")).click();

		// Verify that you see 'Cancelled'
		if (driver.findElement(By.xpath("//*[@id='tabs']/div[1]/h1/span/span[2]")).isDisplayed() == true) {
			System.out.println("PASS:  Cancelled display on the page");
		} else {
			System.out.println("***FAIL: Cancelled does not display on the page***");
		}

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Special Project
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[11]/a/span")).click();
		Thread.sleep(2000);

		// Click on the Closed Checkbox
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Closed']"))
				.click();
		Thread.sleep(3000);
		// Verify that you see 'QA Test'
		try {
			Assert.assertEquals("QA Test",
					driver.findElement(
							By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]"))
							.getText());
			System.out.println("PASS: QA Test display");
		} catch (Error e) {
			System.out.println("***FAIL: QA Test dont display***");
			verificationErrors.append(e.toString());
		}
		System.out.println("Test Case Name TC016: Completed");

	}

	@Test(enabled = true)
	public void TC017() throws Exception {

		System.out.println("Test Case TC017: Add/Close Change Request");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// Click on Change Requests
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[4]/a/span")).click();

		// Verify that you see 'Change Request: Veritas Tickets'
		if (driver.findElement(By.xpath("//*[@id='VeritasSection']/h1")).isDisplayed() == true) {
			System.out.println("PASS:  Change request display");
		} else {
			System.out.println("***FAIL: Change request dont display***");
		}
		// Verify that you see 'Change Request: Client Tickets'
		if (driver.findElement(By.xpath("//*[@id='CustomerSection']/h1")).isDisplayed() == true) {
			System.out.println("PASS:   Client Tickets display");
		} else {
			System.out.println("***FAIL:  Client Tickets dont display***");
		}
		// In the Veritas Section click 'add ticket'
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl03_ctl01_InitInsertButton']"))
				.click();

		// Select Other from the Category dropdown
		Select myO = new Select(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCategory']")));
		myO.selectByVisibleText("Other");

		Thread.sleep(4000);
		// Select Other from the Sub Category dropdown
		Select my1 = new Select(driver.findElement(By.xpath("//*[@id='ddlSubCategory']")));
		my1.selectByVisibleText("Other");

		// Type 'QA Test' in the Comments section
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtComment']")).sendKeys("QA Test");

		// Click the Create Button
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCreateSupportTicket']")).click();

		// Verify that you see New button
		if (driver.findElement(By.xpath("//*[@id='btnNewStatus']")).isDisplayed() == true) {
			System.out.println("PASS:  New Buttons display");
		} else {
			System.out.println("***FAIL: New button dont display***");
		}

		// Verify that you see In-Progress Button
		if (driver.findElement(By.xpath("//*[@id='btnInProgressStatus']")).isDisplayed() == true) {
			System.out.println("PASS:  in-progress display");
		} else {
			System.out.println("***FAIL: in progress dont display***");
		}
		// Verify that you see the Hold Button
		if (driver.findElement(By.xpath("//*[@id='btnHoldStatus']")).isDisplayed() == true) {
			System.out.println("PASS:  hold Buttons display");
		} else {
			System.out.println("***FAIL: hold button dont display***");
		}
		// Verify that you see the Closed Button
		if (driver.findElement(By.xpath("//*[@id='btnClosedStatus']")).isDisplayed() == true) {
			System.out.println("PASS:  Closed Buttons display");
		} else {
			System.out.println("***FAIL: Closed button dont display***");
		}
		// Verify that you see 'QA Test' in the comments column
		try {
			Assert.assertEquals("QA Test",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]"))
							.getText());
			System.out.println("PASS: QA Test display");
		} catch (Error e) {
			System.out.println("***FAIL: QA Test dont display***");
			verificationErrors.append(e.toString());
		}

		// Click on the In-Progress button
		driver.findElement(By.xpath("//*[@id='btnInProgressStatus']")).click();
		Thread.sleep(2000);
		// Verify that you see 'The Ticket was successfully updated.'
		if (driver.findElement(By.xpath("//*[@id='TicketSuccefullyUpdated']")).isDisplayed() == true) {
			System.out.println("PASS:  The Ticket was successfully updated display");
		} else {
			System.out.println("***FAIL: The Ticket was successfully updated display***");
		}
		// Click on the change requests menu button
		driver.findElement(By.xpath("//*[@id='divLiList']/li[4]/a/span")).click();

		// Verify that you see 'Change Request: Veritas Tickets'
		if (driver.findElement(By.xpath("//*[@id='VeritasSection']/h1")).isDisplayed() == true) {
			System.out.println("PASS:  Change request display");
		} else {
			System.out.println("***FAIL: Change request dont display***");
		}
		// Verify that you see 'Change Request: Client Tickets'
		if (driver.findElement(By.xpath("//*[@id='CustomerSection']/h1")).isDisplayed() == true) {
			System.out.println("PASS:   Client Tickets display");
		} else {
			System.out.println("***FAIL:  Client Tickets dont display***");
		}

		// Verify that you see 'Qa Veritas' in the Submitted By column
		try {
			Assert.assertEquals("Qa Veritas",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]"))
							.getText());
			System.out.println("PASS: QA Veritas display");
		} catch (Error e) {
			System.out.println("***FAIL: QA Veritas dont display***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'In-Progress' in the Status column
		try {
			Assert.assertEquals("In-Progress",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]"))
							.getText());
			System.out.println("PASS: QA Veritas display");
		} catch (Error e) {
			System.out.println("***FAIL: QA Veritas dont display***");
			verificationErrors.append(e.toString());
		}

		// Click on the View/Edit Button
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[9]/a"))
				.click();

		// Click on the Closed Button
		driver.findElement(By.xpath("//*[@id='btnClosedStatus']")).click();
		Thread.sleep(2000);
		// Verify that you see 'The Ticket was successfully updated.
		if (driver.findElement(By.xpath("//*[@id='TicketSuccefullyUpdated']")).isDisplayed() == true) {
			System.out.println("PASS:  The Ticket was successfully updated display");
		} else {
			System.out.println("***FAIL: The Ticket was successfully updated display***");
		}

		// Click on Change Requests
		driver.findElement(By.xpath("//*[@id='divLiList']/li[4]/a/span")).click();

		// Type Closed in the Status Filter Field
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']"))
				.sendKeys("Closed");
		Thread.sleep(2000);

		// In the Filter Dropdown select 'contains'
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']"))
				.click();
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_rfltMenu_detached']/ul/li[2]/a/span"))
				.click();
		Thread.sleep(3000);

		// Verify that you see 'Qa Veritas' in the Submitted By column
		try {
			Assert.assertEquals("Qa Veritas",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]"))
							.getText());
			System.out.println("PASS: QA Veritas display");
		} catch (Error e) {
			System.out.println("***FAIL: QA Veritas dont display***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'Closed' in the Status column
		try {
			Assert.assertEquals("Closed",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]"))
							.getText());
			System.out.println("PASS: Closed display");
		} catch (Error e) {
			System.out.println("***FAIL: Closed dont display***");
			verificationErrors.append(e.toString());
		}

		// Click the Home button
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC017: Completed");

	}

	@Test(enabled = true)
	public void TC018() throws Exception {

		System.out.println("Test Case TC018: Placing an Order and Submitting a Cancel Request");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(4000);

		// make sure cart is clear
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

		// Search part
		driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QATESTPOD");
		driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();

		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();

		// Verify Text on the page
		if (driver
				.findElement(By
						.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA Test POD display");
		} else {
			System.out.println("***FAIL: QA Test POD dont display***");
		}

		// Click Add to cart
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]"))
				.click();

		// Verify Text on the page
		try {
			Assert.assertEquals("QATestPOD (IL)",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_divFormNumber']/a"))
							.getText());
			System.out.println("PASS: QATestPOD (IL) display");
		} catch (Error e) {
			System.out.println("***FAIL: QATestPOD (IL) dont display***");
			verificationErrors.append(e.toString());
		}

		// Click Checkout
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']"))
				.click();

		// Verify Text on the page name
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtName']")).isDisplayed() == true) {
			System.out.println("PASS: Name display display");
		} else {
			System.out.println("***FAIL:  Name dont display***");
		}

		// Verify Text on the page address 1
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtAddress1']")).isDisplayed() == true) {
			System.out.println("PASS: address 1 display");
		} else {
			System.out.println("***FAIL: address 1 dont display***");
		}

		// Verify Text on the page city
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCity']")).isDisplayed() == true) {
			System.out.println("PASS: City display");
		} else {
			System.out.println("***FAIL: City dont display***");
		}

		// Verify Text on the page State
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtState']")).isDisplayed() == true) {
			System.out.println("PASS: State display");
		} else {
			System.out.println("***FAIL: State display***");
		}

		// Verify Text on the page Zip
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtZip']")).isDisplayed() == true) {
			System.out.println("PASS: Zip display");
		} else {
			System.out.println("***FAIL: Zip dont display***");
		}

		// Verify Text on the page country
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlCountry']")).isDisplayed() == true) {
			System.out.println("PASS: State display");
		} else {
			System.out.println("***FAIL: State dont display***");
		}

		// Verify Text on the page email
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtEmail']")).isDisplayed() == true) {
			System.out.println("PASS: Email display");
		} else {
			System.out.println("***FAIL: Email dont display***");
		}

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxOrderConfirmation']")).isSelected() == true) {
			System.out.println("PASS: Order Confirmation is checked");
		} else {
			System.out.println("***FAIL: Order Confirmation is notchecked***");
		}

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_cbxShippedConfirmation']")).isSelected() == true) {
			System.out.println("PASS: Shipped confirmation display");
		} else {
			System.out.println("***FAIL: Shipeed confirmation dont display***");
		}

		// Clear Cost Center Field
		// Type 9999 in the Cost Center Field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");

		// Click Next
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();

		// Click Checkout
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCheckout']")).click();

		Thread.sleep(5000);
		// Copy order number
		String ordernumber = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']"))
				.getText();
		System.out.println("The order Number is" + ordernumber);

		Thread.sleep(2000);
		// Click on Submit Cancel Request
		// driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[7]/a")).click();
		driver.findElement(By.linkText("Submit Cancel Request")).click();
		// Click ok for Do you want to cancel this order?
		Thread.sleep(2000);
		Assert.assertTrue(closeAlertAndGetItsText().matches("^Do you want to cancel this order[\\s\\S]$"));
		Thread.sleep(3000);

		// driver.findElement(By.linkText("OK")).click();
		Thread.sleep(3000);

		// close popup
		driver.findElement(By.id("ctl00_ctl00_cphContent_cphSection2_btnClose")).click();
		Thread.sleep(3000);
		// Click on Change Requests
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[4]/a/span")).click();

		// Verify that you see 'Qa Veritas' in the Submitted By column
		try {
			Assert.assertEquals("Qa Veritas",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]"))
							.getText());
			System.out.println("PASS: Qa Veritas display");
		} catch (Error e) {
			System.out.println("***FAIL: Qa Veritas dont display***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'New' in the Status column
		try {
			Assert.assertEquals("New",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]"))
							.getText());
			System.out.println("PASS: New display");
		} catch (Error e) {
			System.out.println("***FAIL: New dont display***");
			verificationErrors.append(e.toString());
		}

		// Click on the View/Edit Button
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[9]/a"))
				.click();

		// Verify that you see 'Cancel Order' in the comments column
		try {
			Assert.assertEquals("Cancel Order",
					driver.findElement(By
							.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_SupportTicketCommentsRadGrid_ctl00__0']/td[3]"))
							.getText());
			System.out.println("PASS: Cancel Order display");
		} catch (Error e) {
			System.out.println("***FAIL: Cancel Order display***");
			verificationErrors.append(e.toString());
		}

		// Click on the cancel order button
		driver.findElement(By.xpath("//*[@id='btnCancelOrder']")).click();

		// Click on Change Requests
		driver.findElement(By.xpath("//*[@id='divLiList']/li[4]/a/span")).click();

		// Typed Closed in the Status Filter Field
		driver.findElement(By
				.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_FilterTextBox_Status']"))
				.sendKeys("Closed");
		Thread.sleep(2000);

		// In the Filter Dropdown select 'contains'
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00_ctl02_ctl02_Filter_Status']"))
				.click();
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_rfltMenu_detached']/ul/li[2]/a/span"))
				.click();
		Thread.sleep(3000);

		// Verify that you see 'Cancel Order' in the Sub Category Section
		try {
			Assert.assertEquals("Cancel Order",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__4']/td[3]"))
							.getText());
			System.out.println("PASS: Cancel Order was cancelled");
		} catch (Error e) {
			System.out.println("***FAIL: Cancel order didnt get cancelled***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'Qa Veritas' in the Submitted By column
		try {
			Assert.assertEquals("Qa Veritas",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[5]"))
							.getText());
			System.out.println("PASS: QA Veritas display");
		} catch (Error e) {
			System.out.println("***FAIL: QA Veritas dont display***");
			verificationErrors.append(e.toString());
		}

		// Verify that you see 'Closed' in the Status column
		try {
			Assert.assertEquals("Closed",
					driver.findElement(
							By.xpath("//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_ctl00__0']/td[8]"))
							.getText());
			System.out.println("PASS: Closed display");
		} catch (Error e) {
			System.out.println("***FAIL: Closed dont display***");
			verificationErrors.append(e.toString());
		}
		// Click the Home button
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC018: Completed");

	}

	@Test(enabled = true)
	public void TC019() throws Exception {

		System.out.println("Test Case TC019: Activate, Edit, and deactivate Announcement");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Announcements
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[4]/a/span")).click();

		// In the Announcement filter field type QA Test
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']"))
				.sendKeys("QA TESTING");

		// In the Filter Dropdown select 'contains'
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']"))
				.click();
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_rfltMenu_detached']/ul/li[2]/a/span"))
				.click();

		// Verify that you see QA Test in the Announcement Column
		if (driver
				.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA Test in the Announcement Column");
		} else {
			System.out.println("***FAIL: QA Test is not in the Announcement Column***");
		}

		// Click edit button
		driver.findElement(By.id("ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl04_EditButton")).click();
		// Clear the text in the announcement field
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Click on the B in the bar above the announcement field
		driver.findElement(By.cssSelector("span.Underline")).click();

		// Click on the U in the bar above the announcement field
		driver.findElement(By.cssSelector("span.Bold")).click();

		// Type 'QA TESTING' In the Announcement Field

		// Click Update Announcement Button
		driver.findElement(By.id("ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl05_btnSaveAnnouncement"))
				.click();

		// Verify that you see 'QA TESTING' in the Announcement Column
		if (driver
				.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA Test in the Announcement Column");
		} else {
			System.out.println("***FAIL: QA Test is not in the Announcement Column***");
		}

		// Click the Activate Button
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a"))
				.click();

		// Click Home
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		/* //Under the Announcements Widget Verify you see 'QA TESTING'
		 if(driver.findElement(By.xpath("//*[@id='divAnnouncements']/span[1]")).isDisplayed() == true){
		       System.out.println("PASS: Announcements Widget display");
				 } else {
			    System.out.println("***FAIL: Announcements Widget dont display ***");	 
		         }  
			 */

		// Hover Over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Announcements
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[4]/a/span")).click();

		// In the Announcement filter field type QA Test
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']"))
				.sendKeys("QA TESTING");

		// In the Filter Dropdown select 'contains'
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']"))
				.click();
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_rfltMenu_detached']/ul/li[2]/a/span"))
				.click();

		// Verify that you see QA Test in the Announcement Column
		if (driver
				.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA Test in the Announcement Column");
		} else {
			System.out.println("***FAIL: QA Test is not in the Announcement Column***");

		}

		Thread.sleep(2000);
		// Verify that you see the Deactivate Link
		try {
			Assert.assertEquals("Deactivate",
					driver.findElement(
							By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a"))
							.getText());
			System.out.println("PASS: Deactivate Link display");
		} catch (Error e) {
			System.out.println("***FAIL: Deactivate Link does not display***");
			verificationErrors.append(e.toString());
		}

		// Click the deactivate link
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a"))
				.click();

		// In the Announcement filter field type QA Test
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_FilterTextBox_Content']"))
				.sendKeys("QA TESTING");

		// In the Filter Dropdown select 'contains'
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00_ctl02_ctl02_Filter_Content']"))
				.click();
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_rfltMenu_detached']/ul/li[2]/a/span"))
				.click();

		// Verify that you see QA Test in the Announcement Column
		if (driver
				.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]"))
				.isDisplayed() == true) {
			System.out.println("PASS: QA Test in the Announcement Column");
		} else {
			System.out.println("***FAIL: QA Test is not in the Announcement Column***");
		}
		// Verify that you see the Activate Link
		try {
			Assert.assertEquals("Activate",
					driver.findElement(
							By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[8]/a"))
							.getText());
			System.out.println("PASS: Activate Link display");
		} catch (Error e) {
			System.out.println("***FAIL: Activate Link does not display***");
			verificationErrors.append(e.toString());
		}

		// Click the Home button
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdAnnouncements_ctl00__0']/td[1]"))
				.click();

		System.out.println("Test Case Name TC019: Completed");

	}

	@Test(enabled = true)
	public void TC020() throws Exception {

		System.out.println("Test Case TC020: Filtering in Manage Phrases");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Phrases
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[8]/a/span")).click();

		// Type Login in the Name Field
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterTextBox_DisplayName']"))
				.sendKeys("Login");

		// In the Filter Dropdown select 'contains'
		driver.findElement(By
				.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_DisplayName']"))
				.click();
		driver.findElement(
				By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_rfltMenu_detached']/ul/li[2]/a/span"))
				.click();

		// Verify that you see 'Login Bad User Name and/or Password' in the name
		// column
		try {
			Assert.assertEquals("Login Bad User Name and/or Password",
					driver.findElement(
							By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00__0']/td[1]"))
							.getText());
			System.out.println("PASS: Login Bad User Name and/or Password display");
		} catch (Error e) {
			System.out.println("***FAIL: Login Bad User Name and/or Password does not display***");
			verificationErrors.append(e.toString());
		}

		// home
		driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC020: Completed");

	}

	@Test(enabled = true)
	public void TC021() throws Exception {

		System.out.println("Test Case TC021: Reports Page");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Reports
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[12]/a/span")).click();

		// Verify that you see 'Available Reports' section
		if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/h1")).isDisplayed() == true) {
			System.out.println("PASS: Available reoports display");
		} else {
			System.out.println("***FAIL: Available reoports does not display***");
		}
		// Verify that you see 'Report Name'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00']/thead/tr/th[1]/a"))
				.isDisplayed() == true) {
			System.out.println("PASS: Reports name display");
		} else {
			System.out.println("***FAIL: Reports name does not display***");
		}
		// Verify that you see 'Description'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00']/thead/tr/th[2]/a"))
				.isDisplayed() == true) {
			System.out.println("PASS: Description display");
		} else {
			System.out.println("***FAIL:  Description does not display***");
		}

		// Verify that you see 'Report Scheduler' section
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblSchedule']")).isDisplayed() == true) {
			System.out.println("PASS: Report Scheduler' section display");
		} else {
			System.out.println("***FAIL: Report Scheduler' section does not display***");
		}

		// Verify that you see the 'My Report Subscriptions'
		if (driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[11]/h1")).isDisplayed() == true) {
			System.out.println("PASS: My Report Subscriptions display");
		} else {
			System.out.println("***FAIL: My Report Subscriptions does not display***");
		}

		// Verify that you see '8WeekUsageReport'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00__1']/td[1]")).isDisplayed() == true) {
			System.out.println("PASS: 8WeekUsageReport display");
		} else {
			System.out.println("***FAIL: 8WeekUsageReport does not display***");
		}

		// Click on the View Report Link for the 8 Week Usage Report
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Reports_ctl00__1']/td[3]/a")).click();

		/*   
		 //Verify that you see '8 Week Usage Report'
		   if(driver.findElement(By.xpath("//*[@id='P63959b0e6e3d4d3487d5e3a6b1ae97eaoReportCell']/table/tbody/tr[1]/td/div/table/tbody/tr[2]/td[4]/table/tbody/tr/td/div")).isDisplayed() == true){
			   System.out.println("PASS: 8 Week Usage Report display");
				 } else {
			    System.out.println("***FAIL: 8 Week Usage Report display***");	 
		         }  
		   
		 //Verify that you see Literature type
		   if(driver.findElement(By.xpath("//*[@id='ParametersGridrptTest_ctl00']/tbody/tr[1]/td[1]/span")).isDisplayed() == true){
		       System.out.println("PASS: Literature type display");
				 } else {
			    System.out.println("***FAIL: Literature type does not display***");	 
		         }  
		   
		 //Verify that you see Channel
		   if(driver.findElement(By.xpath("//*[@id='ParametersGridrptTest_ctl00']/tbody/tr[1]/td[4]/span")).isDisplayed() == true){
		       System.out.println("PASS:  Channel display");
				 } else {
			    System.out.println("***FAIL:  Channel does not display***");	 
		         }  
		   
		 //In the Literature type field select POD
		 driver.findElement(By.xpath("//*[@id='rptTest_ctl00_ctl03_ddValue']")).click();
		 driver.findElement(By.xpath("//*[@id='rptTest_ctl00_ctl03_ddValue']/option[4]")).click();
		 
		   
		 //Click View Report
		 driver.findElement(By.xpath("//*[@id='rptTest_ctl00_ctl00']")).click();
		   
		 
		 //Verify that you see Report Run Criteria:  Literature Type= 'POD'
		   if(driver.findElement(By.xpath("//*[@id='P63959b0e6e3d4d3487d5e3a6b1ae97eaoReportCell']/table/tbody/tr[1]/td/div/table/tbody/tr[4]/td[3]/table/tbody/tr/td/div")).isDisplayed() == true){
		       System.out.println("PASS: Literature type display");
				 } else {
			    System.out.println("***FAIL: Literature type does not display***");	 
		         }    
		         
		 //In the select a format dropdown choose Excel
			 driver.findElement(By.xpath("//*[@id='rptTest_ctl01_ctl05_ctl00']")).click();
			 driver.findElement(By.xpath("//*[@id='rptTest_ctl01_ctl05_ctl00']/option[6]")).click();
		
		   
		 //Verify that you see the link Export
		   if(driver.findElement(By.partialLinkText("Export")).isDisplayed() == true){
		       System.out.println("PASS: Export display");
				 } else {
			    System.out.println("***FAIL: Export does not display***");	 
		         }  
		   
		 //Close the Report window by clicking the x
		 driver.findElement(By.xpath("//*[@id='RadWindowWrapper_ctl00_cphContent_ContentWindow']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li/a")).click();
		 */

		// navigation back
		driver.navigate().back();
		Thread.sleep(3000);
		/*
		//Verify that you see Business Owner: 
		  if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[5]/div[1]/label")).isDisplayed() == true){
		      System.out.println("PASS: Business owner display");
			 } else {
		    System.out.println("***FAIL: Business owner display***");	 
		        }*/

		// Click Home
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();

		System.out.println("Test Case Name TC021: Completed");

	}

	@Test(enabled = true)
	public void TC022() throws Exception {

		System.out.println("Test Case TC022: Obsolete Test Pieces");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("qaauto");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("qaauto");
		driver.findElement(By.id("btnSubmit")).click();
		Thread.sleep(000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_POD Test in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_POD Test");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		Thread.sleep(5000);

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(5000);
		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Item obsoleted display");
		} else {
			System.out.println("***FAIL: Item obsoleted display does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		Thread.sleep(4000);

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC022: ***************P1-Completed*************");

		// Hover over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_Stock in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_Stock");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);
		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Item obsoleted display");
		} else {
			System.out.println("***FAIL: Item obsoleted display does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[8]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC022: ***************P2-Completed*************");

		// Hover over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_Stock in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_Stock");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[8]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.click();
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC022: ***************p3-Completed*************");

		System.out.println("Test Case TC022: ***************Ended*************");

	}

	@AfterMethod
	public void AfterMethod() {

		Closealltabs();
		

	}

}