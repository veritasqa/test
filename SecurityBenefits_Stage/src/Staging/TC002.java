package Staging;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC002 {

	private WebDriver driver;
	private String baseUrl;
	// private StringBuffer verificationErrors = new StringBuffer();
	// private boolean acceptNextAlert = true;

	//
	@BeforeClass
	public void setUp() throws Exception {

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--disable-web-security");
		options.addArguments("--no-proxy-server");
		options.addArguments("disable-infobars");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enabled", false);
		options.setExperimentalOption("prefs", prefs);
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\src\\driver\\chromedriver.exe");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Driver.manage().window().maximize();
		baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";

	}

	@Test
	public void testUntitled() throws Exception {

		System.out.println("Test Case TC002: Unobsolete Test Pieces");

		// Login
		driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		driver.findElement(By.id("txtUserName")).clear();
		driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		driver.findElement(By.id("txtPassword")).clear();
		driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		driver.findElement(By.id("btnSubmitReal")).click();
		Thread.sleep(000);

		// Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_POD Test in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_POD Test");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		Thread.sleep(5000);

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		Thread.sleep(5000);
		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Item obsoleted display");
		} else {
			System.out.println("***FAIL: Item obsoleted display does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		Thread.sleep(4000);

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC002: ***************P1-Completed*************");

		// Hover over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_Stock in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_Stock");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);
		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Item obsoleted display");
		} else {
			System.out.println("***FAIL: Item obsoleted display does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC002: ***************P2-Completed*************");

		// Hover over Admin
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();

		// Select Manage Inventory
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();

		// Type QA_Stock in Stock # field
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_Stock");

		// Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		// Click Edit
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a")).click();
		Thread.sleep(3000);
		// Store the current window handle
		// String winHandleBefore = driver.getWindowHandle();

		// Switch to new window opened
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// Click on Obsolete Now
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnObsoleteNow']")).click();
		Thread.sleep(3000);

		// Verify Text on the page
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblErrorMessage']")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Change History Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span"))
				.click();

		// filter Obsolete Part Now!
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_FilterTextBox_Action']"))
				.sendKeys("Obsolete Part Now!");
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00_ctl02_ctl02_Filter_Action']")).click();

		// Verify Text on the page 'Obsolete Part Now!'
		if (driver.findElement(By.xpath("//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]")).isDisplayed() == true) {
			System.out.println("PASS: Obsolete Part Now! display");
		} else {
			System.out.println("***FAIL: Obsolete Part Now! does not display***");
		}

		// Click on Rules Tab
		driver.findElement(
				By.xpath("//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span"))
				.click();

		// click on Un-obsolete
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnUnObsolete']")).click();

		System.out.println("Test Case TC002: ***************p3-Completed*************");

		System.out.println("Test Case TC002: ***************Ended*************");

	}

	@AfterClass(enabled = false)

	public void teardown() {
		// close current window
		// driver.quit();
		// close the original window
		driver.quit();

	}
}
