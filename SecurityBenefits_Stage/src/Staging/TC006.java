package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class TC006 {


		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		 // private boolean acceptNextAlert = true;
		 
		  
		
//		  	
		  @Before
		  public void setUp() throws Exception {
		    driver = new FirefoxDriver();
		    baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    driver.manage().window().maximize();
		    
		  } 
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case PTC006: Add/Remove New Forms and Materials Widget");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		  
		   
		Thread.sleep(10000)  ; 
		   
		   //navigate to add Widget "New Forms and Materials Widget"
		   Actions act = new Actions (driver);
		   WebElement AddWidgets = driver.findElement(By.id("addWidgetSection"));
		   act.moveToElement(AddWidgets).build().perform();
		  Thread.sleep(3000);
		  
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnAddWidgetNewProducts']")).click();
		   Thread.sleep(3000);
		 //select and click on recipient link
		   driver.findElement(By.linkText("select a recipient")).click();
		   
		   //Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(5000);
		   
		   
		   
		   
		  //Verify QA Veritas display on top of the page 
		   
		   try {
			   assertEquals("Ordering for: Qa Veritas", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_h3OrderingFor']/i")).getText());
			   System.out.println("PASS : QA Veritas display");
			 } catch (Error e) {
			  System.out.println("fail: QA Veritas does not display");
			   verificationErrors.append(e.toString());
			 }
		   
		   // Verify new forms and materials display on the widget
		  /* 
		   try {
			   assertEquals("New Forms and Materials", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock5c11fe86-f1ea-4247-abd6-70939e25640b_T']/em")).getText());
			   System.out.println("PASS: New Forms and Materials display");
			 } catch (Error e) {
			  System.out.println("FAIL: New Forms and Materials does not display");
			   verificationErrors.append(e.toString());
			 }
		   */
		   
		   //click on the close button
		   //navigate to add Widget "New Forms and Materials Widget"
		   /*
		   Actions act = new Actions (driver);
		   WebElement Close = driver.findElement(By.id("addWidgetSection"));
		   act.moveToElement(Close).build().perform();
		  */
		   
		
		   
		   
		System.out.println("Removing the Widget will be done manually and " +
				"that's because it does not contain a property ");
		 System.out.println("Test Case Name PTC006: Completed");   
		
		  }
		   @After
		   	public void teardown() throws Exception {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		   	driver.quit();


		   
		   
		   
		   }
		   
		   
		  
	}
	