package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC015 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC015: Manage Inventory - Search Criteria");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   
		   
		   //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		   //Manage Inventory
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[3]/a/span")).click();
	   
		   //Verify that you see the search items table
		   try {
			      assertEquals("Search Items", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_pnlSearch']/div[1]/h1")).getText());
			      System.out.println("PASS: search items display on the page");
			    } catch (Error e) {
			     System.out.println("***FAIL: search items Does not display on the page***");
			      verificationErrors.append(e.toString());
			    }
		  
		   
		  //Type test in the keyword test field
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtKeyWord']")).sendKeys("test");

		   
		 //clear 
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();
		 Thread.sleep(2000);
		 //In the User Group Field Select EMO
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlAudience_Arrow']")).click();
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlAudience_DropDown']/div/ul/li[6]")).click();
		
		//Click Search
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();

		 
		//Verify that you see QATESTPOD as the 2nd Stock # record in the search results table
		  try {
		      assertEquals("QATestPOD", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__1']/td[1]/a")).getText());
		      System.out.println("PASS: QATESTPOD as the 2nd Stock display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: QATESTPOD as the 2nd Stock Does not display on the page***");
		      verificationErrors.append(e.toString());
		    } 
		 
		//Click Clear Button
	    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();
 
		  
		//Verify that you see 'No records to display' in the Search Results table
	    try {
		      assertEquals("No records to display.", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00']/tbody/tr/td/div")).getText());
		      System.out.println("PASS: No records to display. display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: No records to display. Does not display on the page***");
		      verificationErrors.append(e.toString());
		    } 
		//In the Stock # Field Type TESTJILLPOD
	    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("TESTJILLPOD");
	    
		//In the Active Field Select Obsolete
	    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlActive_Arrow']")).click();
	    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlActive_DropDown']/div/ul/li[4]")).click();
	    
		//Click Search
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		 
		//Verify that you see 'TESTJILLPOD'
		 try {
		      assertEquals("TESTJILLPOD", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[1]/a")).getText());
		      System.out.println("PASS: TESTJILLPOD display. display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: TESTJILLPOD Does not display on the page***");
		      verificationErrors.append(e.toString());
		    } 
		 
		 
		//Verify that you see Status 'O'
		 try {
		      assertEquals("O", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]")).getText());
		      System.out.println("PASS: O display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: O Does not display on the page***");
		      verificationErrors.append(e.toString());
		    } 
		 
		 
		//Click Clear Button
		    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();
 
		 
		//Verify that you see 'No records to display' in the Search Results table
		    try {
			      assertEquals("No records to display.", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00']/tbody/tr/td/div")).getText());
			      System.out.println("PASS: No records to display. display on the page");
			    } catch (Error e) {
			     System.out.println("***FAIL: No records to display. Does not display on the page***");
			      verificationErrors.append(e.toString());
			    }     
		    
		//In the Stock # Field Type QA_ChildItem_3_Backorder
		    driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFormNumber']")).sendKeys("QA_ChildItem_3_Backorder");    
		    
		//In Units on Hand Select 'Out of Stock'
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlUOH_Arrow']")).click();
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlUOH_DropDown']/div/ul/li[4]")).click();
		 
		//Click Search
		driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		 
		//Verify that you see 'QA_ChildItem_3_Backorder
		 try {
		      assertEquals("QA_ChildItem_3_Backorder", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[1]/a")).getText());
		      System.out.println("PASS: QA_ChildItem_3_Backorder display. display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: QA_ChildItem_3_Backorder Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }   
		
		 
		
		//Verify that you see '0' in the Units on Hand Column
		 try {
		      assertEquals("0", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00__0']/td[6]")).getText());
		      System.out.println("PASS: 0 display on the page");
		    } catch (Error e) {
		     System.out.println("***FAIL: 0 Does not display on the page***");
		      verificationErrors.append(e.toString());
		    }   
		
		//Click Clear Button
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnClear']")).click();
		 
		//Verify that you see 'No records to display' in the Search Results table
		    try {
			      assertEquals("No records to display.", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Results_ctl00']/tbody/tr/td/div")).getText());
			      System.out.println("PASS: No records to display. display on the page");
			    } catch (Error e) {
			     System.out.println("***FAIL: No records to display. Does not display on the page***");
			      verificationErrors.append(e.toString());
			    }    
		//Click the home button
		    
		driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		
		 System.out.println("Test Case Name TC015: Completed");  
		  }
		
		  @After
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		 
		   
		  }
}




