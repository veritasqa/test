package Staging;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class TC016 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC016: Creating and Closing Special Project");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   
		   
		   //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		   
		   
		   
		 //Select Special Project
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[11]/a/span")).click();  
		   
		 //Verify that you see '     Special Projects : View Tickets ' 
		   if(driver.findElement(By.xpath("//*[@id='divContent clearfix']/div[2]/h1")).isDisplayed() == true){
	        	 System.out.println("PASS:  Special Projects display");
			    } else {
			     System.out.println("***FAIL: Special Projects dont display***");	 
	         }  
	   
	    
		   
		 //Click add ticket
		 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_InitInsertButton']")).click();
		  
		 //Type QA Test in the Job Title Field
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtJobTitle']")).sendKeys("QA Test");
		 
		 //In the Job Type Field Select "Other'
	//	 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlJobType']")).click();
		// Thread.sleep(3000);
	//	 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlJobType']/option[5]")).click();
		 
		 Select myO = new Select(driver.findElement(By.xpath("//*[@id='ctl00_cphContent_ddlJobType']")));						
		 myO.selectByVisibleText("Other");						
		 
		 

		 
		 //Type QA Test in the Desciption Field
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtDescription']")).sendKeys("QA Test");
		 
		 //Click Create
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSave']")).click();
		 
		 //Verify that you see 'Data has been saved.'
		 try {
		      assertEquals("Data has been saved.", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblMessage']")).getText());
		      System.out.println("PASS: Data has been saved display");
		    } catch (Error e) {
		     System.out.println("***FAIL: Data has been saved. dont display***");
		      verificationErrors.append(e.toString());
		    }  
		 //Click on the Progress/Files Tab
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lnkProgress']/span")).click();
		 Thread.sleep(2000);
		 //admin
		 driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		     
		 //Select Special Project
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[11]/a/span")).click();
		   
		 
		 //Verify you see 'QA Test'
		  /* try {
			      assertEquals("QA test", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]")).getText());
			      System.out.println("PASS: QA test display");
			    } catch (Error e) {
			     System.out.println("***FAIL: QA Test dont display***");
			      verificationErrors.append(e.toString());
			    }   */
		 
		   if(driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]")).isDisplayed() == true){
	        	 System.out.println("PASS: QA test display ");
			    } else {
			     System.out.println("***FAIL: QA test does not  display  ***");	 
	         }  
	   
		   
		   
		   
		 //Click View/Edit for the Special Project created
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[8]/a")).click();
 
  
		 //Click on the Progress/Files Tab
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lnkProgress']/span")).click();
  
		   
		 //Click on Add Comment / Progress/ Files
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_InitInsertButton']")).click();
 
		   
		 //Type 'QA Test Special Project' in the comments section
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_txtComment']")).sendKeys("QA Test Special Project");
 
		   
		 //Select Canceled from the Type field
			 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Arrow']")).click();
			 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_DropDown']/div/ul/li[2]")).click();

		 
		 //Click Submit
	      driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnSaveComment']")).click();
	 
		   
		 //Verify that you see 'Cancelled'
	      if(driver.findElement(By.xpath("//*[@id='tabs']/div[1]/h1/span/span[2]")).isDisplayed() == true){
	        	 System.out.println("PASS:  Cancelled display on the page");
			    } else {
			     System.out.println("***FAIL: Cancelled does not display on the page***");	 
	         }  
	   
	    
	      //Admin
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/a/span")).click();
		  
		 //Select Special Project
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[5]/ul/li[11]/a/span")).click();  
		   Thread.sleep(2000);
		   
		 //Click on the Closed Checkbox
		 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Closed']")).click();
		  Thread.sleep(3000);
		 //Verify that you see 'QA Test'
		 try {
		      assertEquals("QA Test", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]")).getText());
		      System.out.println("PASS: QA Test display");
		    } catch (Error e) {
		     System.out.println("***FAIL: QA Test dont display***");
		      verificationErrors.append(e.toString());
		    }   
		 System.out.println("Test Case Name TC016: Completed");  
		  }
		 
		 @After
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		 
		   
		   
		   
		   
		  }
}
