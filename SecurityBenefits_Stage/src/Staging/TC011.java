package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC011 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
		//  private boolean acceptNextAlert = true;
		 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		    }	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC011: Product Search Widget");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		   
		   
		   //In Product Search, type 'QA' in the Search for Material By field
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_txtKeyword']")).sendKeys("QA");
		   
		   //Click on the Product Category 
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_Arrow']")).click();
		  
		  //Select Advanced Choice Annuity
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_DropDown']/div/ul/li[2]")).click();

		   
		   
		   //search
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_btnProductSearch']")).click();
		   

		   ////Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(9000);
		   
		   
			//Verify that you see 'Matching item(s) for:'
		   
		   try {
				  assertEquals("Matching item(s) for:", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[1]")).getText());
				  System.out.println("PASS : Matching item(s) for: display");
				 } catch (Error e) {
				  System.out.println("fail:  Matching item(s) for: does not display");
				   verificationErrors.append(e.toString());
				 }
		   
		   //Verify that you see 'QA'
		   try {
				  assertEquals("'QA'", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[2]")).getText());
				  System.out.println("PASS : QA display");
				 } catch (Error e) {
				  System.out.println("fail:  QA does not display");
				   verificationErrors.append(e.toString());
				 }
		   
		  
		   //Verify that you see 'IL'

		   try {
				  assertEquals("'IL'", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[3]")).getText());
				  System.out.println("PASS : IL display");
				 } catch (Error e) {
				  System.out.println("fail:  IL does not display");
				   verificationErrors.append(e.toString());
				 }
		   
		   //Home
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		   
		   //Click on the Product Category 
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_Arrow']")).click();
		  
		  //Select Advanced Choice Annuity
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_DropDown']/div/ul/li[2]")).click();

		  //Click Search
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_btnProductSearch']")).click();
 
		   
		   
		  /* ////Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(9000);
		  */
		  //Verify you see 'Advanced Choice Annuity

		  //Verify you see 'IL'
		   try {
				  assertEquals("'IL'", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[2]")).getText());
				  System.out.println("PASS : IL display");
				 } catch (Error e) {
				  System.out.println("fail:  IL does not display");
				   verificationErrors.append(e.toString());
				 } 
		   

		   //Click on the Home button
		   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		  
		   //Click on the Product Category 
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_Arrow']")).click();
		  
		  //Select Advanced Choice Annuity
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlProductCategory_DropDown']/div/ul/li[2]")).click();

		   //Select Brochure from "Literature type"
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlDocumentType_Arrow']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_ddlDocumentType_DropDown']/div/ul/li[7]")).click();
  
		 //Click Search
		 driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock87ecacdf-a671-4a43-83ca-33781754a252_C_ctl00_btnProductSearch']")).click();
 

		 //Verify you see 'Brochure'
		 try {
			  assertEquals("'Brochure'", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[2]")).getText());
			  System.out.println("PASS : Brochure display");
			 } catch (Error e) {
			  System.out.println("fail:  Brochure does not display");
			   verificationErrors.append(e.toString());
			 } 
	   
		 
		 
		 //Verify you see 'IL'
		   try {
				  assertEquals("'IL'", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideCommandBar_lblSearchMessage']/div/b[3]")).getText());
				  System.out.println("PASS : IL display");
				 } catch (Error e) {
				  System.out.println("fail:  IL does not display");
				   verificationErrors.append(e.toString());
				 } 
		   
		
		 //Click on the Home button
		 driver.findElement(By.xpath("//*[@id='ctl00_ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		 
		 System.out.println("Test Case Name TC011: Completed");  
		  }   

		 @After
		   	public void teardown() {
		   	//close current window
		 //  	driver.quit();
		   	//close the original window
		  	driver.quit();  
		   
		   
		   
		   
		   
		   
		   
		   
		  }
}

		  