



package Staging;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;

public class TC007 {



		private WebDriver driver;
		  private String baseUrl;
		 private StringBuffer verificationErrors = new StringBuffer();
	//	  private boolean acceptNextAlert = true;
	//	 
		  
		
//		 
		    @Before
			public void setUp() throws Exception  {
				
				driver = new FirefoxDriver();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();
				baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
				
		}	
		    
		    
		  
		  
		  @Test
		  public void testUntitled() throws Exception {
			  
		  System.out.println("Test Case TC007: Add/Remove Express Checkout Widget. This test case will need to " +
		  		"to be excuted manually because of the of the dynamic widget that keep changing the xpath");
		  
		  
		  //Login
		   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
		   driver.findElement(By.id("txtUserName")).clear();
		   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
		   driver.findElement(By.id("txtPassword")).clear();
		   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
		   driver.findElement(By.id("btnSubmit")).click();
		   Thread.sleep(4000);
		  
		   
		   
		    
		    
		   
		   //navigate to add Widget "New Forms and Materials Widget"
		   /*
		   Actions act = new Actions (driver);
		   WebElement AddWidgets = driver.findElement(By.id("addWidgetSection"));
		   act.moveToElement(AddWidgets).build().perform();
		  Thread.sleep(3000);
		  */
		  //click on select a recipient under express checkout
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_lblFillOutContactInfo']/a")).click();
		   
		  /* 
		   
		  driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnAddWidgetExpressCheckout']")).click();
		   Thread.sleep(3000);
		 //select and click on recipient link
		   driver.findElement(By.linkText("select a recipient")).click();
		   */
		   
		   //Enter contact information
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
		   Thread.sleep(5000);
		   
		   // Enter info under Express checkout
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_rtxtFormNumber1_Input']")).sendKeys("QA_POD TEST");
		   Thread.sleep(5000);
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_txtQuantity1']")).sendKeys("1");
		   Thread.sleep(3000);
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_RadDock242b1a9b-ac73-4c57-bcc9-99b1e3d5957c_C_ctl00_btnAddToCart']")).click();
		   
		   try {
			   assertEquals("QA_POD TEST", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']")).getText());
			   System.out.println("PASS : QA_POD TEST");
			 } catch (Error e) {
			  System.out.println("fail: QA_POD TEST");
			   verificationErrors.append(e.toString());
			 }
		   
		   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']")).click();
		   Thread.sleep(3000);
		   
		   // Verify Item been removed
		   try {
			   assertEquals("Item(s) have been removed from your cart.", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_lblCommandBar']/div")).getText());
			   System.out.println("PASS : Item(s) have been removed from your cart.");
			 } catch (Error e) {
			  System.out.println("FAIL: Item(s) have been removed from your cart does not display");
			   verificationErrors.append(e.toString());
			 }
		   
		   // Verify shopping cart is empty
		   try {
			   assertEquals("Your Shopping Cart Is Empty", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00']/tbody/tr/td/div/div")).getText());
			   System.out.println("PASS : Your Shopping Cart Is Empty.");
			 } catch (Error e) {
			  System.out.println("FAIL: Your Shopping Cart Is Empty does not display");
			   verificationErrors.append(e.toString());
			 }
		   
		   //Home
		   driver.findElement(By.xpath("//*[@id='ctl00_Navigation_divLiList']/li[1]/a/span")).click();
		   
		   System.out.println("Removing the Widget will be done manually and " +
					"that's because it does not contain a property ");
			  
			  
		  
		  System.out.println("Test Case Name TC007: Completed");  
		  
		  }
		  
			  @After
			   	public void teardown() {
			   	//close current window
			 //  	driver.quit();
			   	//close the original window
			  	driver.quit();
		   
			   }
		   
		   
}
		   

		   
		   
		   
		  
		   
		  
		  
		  
		  
	

