package Staging;

import static org.junit.Assert.assertEquals;

//import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TC005 {

	private WebDriver driver;
	  private String baseUrl;
	  private StringBuffer verificationErrors = new StringBuffer();
	 // private boolean acceptNextAlert = true;
	 
	  
	
//	  	
	  @Before
	  public void setUp() throws Exception {
	    driver = new FirefoxDriver();
	    baseUrl = "https://staging.veritas-solutions.net/SecurityBenefit/login.aspx";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    
	  } 
	  
	  @Test
	  public void testUntitled() throws Exception {
		  
	  System.out.println("Test Case Name 005: Validate Backorder Message for Fulfillment Order");
	  	
	  //Login
	   driver.get(baseUrl + "/SecurityBenefit/login.aspx");
	   driver.findElement(By.id("txtUserName")).clear();
	   driver.findElement(By.id("txtUserName")).sendKeys("Veritas_QA");
	   driver.findElement(By.id("txtPassword")).clear();
	   driver.findElement(By.id("txtPassword")).sendKeys("Veritas1234");
	   driver.findElement(By.id("btnSubmit")).click();
	   

	  //make sure cart is clear
	  driver.findElement(By.xpath("//*[@id='ctl00_Navigation_btnCancelOrder']/span")).click();

	   
	  //Search part
	   driver.findElement(By.xpath("//*[@id='ctl00_txtQuickSearch']")).sendKeys("QA_Stock");
	   driver.findElement(By.xpath("//*[@id='ctl00_btnQuickSearch']")).click();
	   
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtFirstName']")).sendKeys("QA");
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtLastName']")).sendKeys("Veritas");
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnSearch']")).click();
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[2]")).click();
	   
	   
	   Thread.sleep(3000);
	   //click add to cart and checkout
	   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[3]/div/div[2]/a[1]")).click();
	   
	   
	   try {
		     assertEquals("QA_Stock (IL)", driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_divFormNumber']/a")).getText());
		     System.out.println("PASS: QA_Stock (IL) display on the shipping page");
		   } catch (Error e) {
		    System.out.println("FAIL: QA_Stock (IL) does not display on the shipping page");
		     verificationErrors.append(e.toString());
		   }

	   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']")).click();
	   
	 //Verify shipping information
	   //Verify order part display
	   try {
	       assertEquals("QA_Stock", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']")).getText());
	       System.out.println("PASS: Part QA_Stock display on the shipping page");
	     } catch (Error e) {
	      System.out.println("FAIL: Part QA_Stock does not display on the shipping page");
	       verificationErrors.append(e.toString());
	     } 
	   
	 //Verify back order msg display  
	   try {
	       assertEquals("Backordered: 1", driver.findElement(By.xpath("//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_divQuantity']/span")).getText());
	       System.out.println("PASS: Part QA_StockBackordered: 1 display on the shipping page");
	     } catch (Error e) {
	      System.out.println("FAIL: Part Backordered: 1 does not display on the shipping page");
	       verificationErrors.append(e.toString());
	     } 
	   
	   //click next
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnShowShipping']")).click();
	   
	 //clear cost center field and enter cost center
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).clear();
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_txtCostCenter']")).sendKeys("9999");

	   //checkout
	   driver.findElement(By.xpath("//*[@id='ctl00_cphContent_btnCheckout']")).click();

	   String OrderNumber = driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']")).getText();
	   System.out.println("Order Number is**************************   "  +  OrderNumber  );


	   //click on OK 
	   driver.findElement(By.xpath("//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnOk']")).click();
	   
	   
	 ///process of canceling the order
	   driver.get("https://staging.veritas-solutions.net/inventory/login.aspx");
	   driver.findElement(By.xpath("//*[@id='Login']")).sendKeys("ishuweikeh");
	   driver.findElement(By.xpath("//*[@id='Password']")).sendKeys("QAlogin123");
	   driver.findElement(By.xpath("//*[@id='Submit']")).click();
	   driver.findElement(By.xpath("//*[@id='Table2']/tbody/tr[39]/td/a")).click();
	   driver.findElement(By.xpath("//*[@id='OrderNumber']")).clear();
	   driver.findElement(By.xpath("//*[@id='OrderNumber']")).sendKeys(OrderNumber);
	   driver.findElement(By.xpath("//*[@id='btnSearch']")).click();
	   Thread.sleep(2000);
	   driver.findElement(By.xpath("//*[@id='DataList']/tbody/tr[2]/td[8]/a")).click();
	   Thread.sleep(2000);
	   //driver.findElement(By.xpath(".//*[@id='ProjectID']/option[8]")).click();
	   //driver.findElement(By.xpath("//*[@id='submit2']")).click();
	   driver.findElement(By.xpath("//*[@id='CancelOrder']")).click();
	   Thread.sleep(2000);
	   driver.switchTo().alert().accept();
	   driver.switchTo().defaultContent();
	   
       Thread.sleep(2000);
       System.out.println("Test Case Name TC005: Completed");  
	  }
	  
	   @After
	   	public void teardown() throws Exception {
	   	//close current window
	 //  	driver.quit();
	   	//close the original window
	   	driver.quit();


	   }
}


	   
	   
	   
	   