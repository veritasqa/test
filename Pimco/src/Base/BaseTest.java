package Base;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Config.Constants;

public class BaseTest {
	public static WebDriver Driver;
	public static ExtentReports report;
	public static ExtentTest logger;
	public static String currentBrowser;

	@BeforeSuite
	@Parameters("browser")
	public void setUp(String browserName) {
		System.out.println(browserName);
		// String currentDateTime = getDateTime();
		// report = new ExtentReports(
		// // System.getProperty("user.dir") + "\\ExtentReports\\ExtentReports -
		// " + currentDateTime + ".html");
		// System.getProperty("user.dir") +
		// "\\ExtentReports\\ExtentReports.html");
		// report.addSystemInfo("Host Name", "Automation User");
		// report.addSystemInfo("Environment", "QA");
		// report.addSystemInfo("User Name", "Selenium User");
		// report.loadConfig(new File(System.getProperty("user.dir") +
		// "\\extent-config.xml"));

		currentBrowser = browserName;
		if (Driver == null) {
			if (browserName.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "\\drivers\\geckodriver.exe");
				Driver = new FirefoxDriver();
			} else if (browserName.equalsIgnoreCase("chrome")) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				options.addArguments("--disable-web-security");
				options.addArguments("--no-proxy-server");
				options.addArguments("disable-infobars");
				options.addArguments("--disable-download-notification");
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);
				prefs.put("profile.default_content_setting_values.plugins", 1);
				prefs.put("profile.content_settings.plugin_whitelist.adobe-flash-player", 1);
				prefs.put("profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player", 1);
				options.setExperimentalOption("prefs", prefs);
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
				Driver = new ChromeDriver();
			} else if (browserName.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "\\drivers\\IEDriverServer.exe");
				Driver = new InternetExplorerDriver();
			}
			Driver.manage().window().maximize();
			Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Driver.get(Constants.URL_PRD);
			Driver.manage().deleteAllCookies();

		}
	}

	@AfterMethod(enabled = false)
	public void generateReport(ITestResult result) throws Exception {
		if (ITestResult.FAILURE == result.getStatus()) {
			String screenPath = captureFailedScreenshot(result.getName());
			String image = logger.addScreenCapture(screenPath);
			logger.log(LogStatus.FAIL, result.getName(), image);
			logger.log(LogStatus.FAIL, result.getThrowable());
		}
		report.endTest(logger);
	}

	@AfterTest(enabled = false)
	public void reportClear() {
		report.flush();
		report.close();
		Driver.quit();
	}

	public String captureFailedScreenshot(String screenName) throws IOException {
		Random rand = new Random();
		int number = rand.nextInt(10000);
		TakesScreenshot ts = (TakesScreenshot) Driver;
		File srcFile = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "\\Screenshots\\" + screenName + number + ".png";
		File dest = new File(destination);
		FileUtils.copyFile(srcFile, dest);
		return destination;
	}

	public String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String currentDateTime = dateFormat.format(date);
		return currentDateTime;
	}

}
