package Util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import Base.BaseTest;


public class CommonMethods extends BaseTest {


	 
	public static void waitforElement(WebElement elementLocators) throws InterruptedException {	
		WebDriverWait wait = new WebDriverWait(Driver,60);
		wait.until(ExpectedConditions.visibilityOf(elementLocators));
		Thread.sleep(1000);
	}	
	
	public static void waitforclickelement(WebElement elementLocators) throws InterruptedException {	
		WebDriverWait wait = new WebDriverWait(Driver,60);
		wait.until(ExpectedConditions.elementToBeClickable(elementLocators));
		Thread.sleep(1000);
	}		
	public static void waitforalert(){			
		WebDriverWait wait = new WebDriverWait(Driver,60);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = Driver.switchTo().alert();
		alert.accept();
	}
	
	public static void waittodisappear(WebElement elementLocators)throws InterruptedException {	
		WebDriverWait wait = new WebDriverWait(Driver,60);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#ctl00_ctl00_cphContent_cphMain_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphRightSideContent_ProductList>div>img")));		
	}
	
	public static String GetText(WebElement element){
		return element.getText();
	}
	
	public static void AcceptAlert(){
		Driver.switchTo().alert().accept();
	}
	
	 public static void selectDropdownValue(WebElement dropdownLocator, List<WebElement> dropdownValuesLocator, String value){
		 dropdownLocator.click();
		 for (WebElement element: dropdownValuesLocator){ 
			if (element.getText().trim().equals(value))			 
			{				
			element.click();
			break;				
			}			
	        }	 
	        }
	 
     public static void selectDropdownValueUsingContains(WebElement dropdownLocator, List<WebElement> dropdownValuesLocator, String value){
		 
	 JavascriptExecutor executor = (JavascriptExecutor)Driver;
	 executor.executeScript("arguments[0].click();",  dropdownLocator);
	 for (WebElement element: dropdownValuesLocator){ 
	 if (element.getText().trim().contains(value))			 
	 {				
	 element.click();
	 break;				
	 }			
	 }
     }
 
	public static void SelectFromDropDown(WebElement dropdownLocator, String value){		 
	Select dropdown = new Select(dropdownLocator);   
	dropdown.selectByVisibleText(value);	 
 }
	
	public static void PrintTime(String Comments){
		DateFormat df = new SimpleDateFormat("MMM_dd_yyyy");
		Date d = new Date();
		String time =  df.format(d);
		Comments = "Automated QA Test " + time ;		
	}

 
 }

	
	
	
	     
		

