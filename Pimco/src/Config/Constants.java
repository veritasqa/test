package Config;

public class Constants {

	public static final String URL_PRD = "https://order.pimco.com/login.aspx";
	public static final String URL_Inventory = "https://www.veritas-solutions.net/inventory/login.aspx";
	public static final String URL_STG = "https://stageorder.pimco.com/login.aspx?ReturnUrl=%2fdefault.aspx";
	public static final String URL_SSO_STG = "https://stageorder.pimco.com/api/wcf/SecurityService/WCFTest.aspx";
	public static final String URL_SSO_PRD = "https://order.pimco.com/api/wcf/SecurityService/WCFTest.aspx";
	public static final String URL_PreProd_STG = "https://staging.veritas-solutions.net/PimcoPreprod/default.aspx";


	public static final String URL_PRD_Cancel = "https://order.pimco.com/Default.aspx";

	// CA production and staging Admin
	public static final String CA_Admin_User = "qaautoca";
	public static final String CA_Admin_PWD = "qaautoca";
	// GWM Admin prd and stg
	public static final String GWM_Admin_User = "qagwm";
	public static final String GWM_Admin_PWD = "qagwm";
	// Production-GWM Basic
	public static final String GWM_PRD_Basic_User = "qaautobasic";
	public static final String GWM_PRD_Basic_PWD = "qaautobasic";
	// Inventory Credentials
	public static final String Inven_User = "yannamalai";
	public static final String Inven_PWD = "yannamalai590";
	// Production-Administrator Credentials-CA
	public static final String CA_PRD_Admin_User = "qaautoca";
	public static final String CA_PRD_Admin_PWD = "qaautoca";
	// Production and staging-Basic Credentials-CA
	public static final String CA_Basic_User = "qaautobasicca";
	public static final String CA_Basic_PWD = "qaautobasicca";

	// PRD-SSO-Credentials
	public static final String SSO_CA_UserID = "qaauto_ssoca";
	public static final String SSO_CA_FirstName = "qa";
	public static final String SSO_CA_LastName = "auto";
	public static final String SSO_CA_CostCenter = "9999";
	public static final String SSO_Email = "Ver.qaauto@rrd.com";
	public static final String SSO_Phone = "555-555-5555";
	public static final String SSO_BankUser = "Canada Retail";
	public static final String SSO_GWM_BankUser = "Bank Trust";
	public static final String SSO_GWM_UserID = "qaauto_sso";
	public static final String SSO_GWM_FirstName = "qa";
	public static final String SSO_GWM_LastName = "auto";

	public static final String ContactFN = "QA";
	public static final String ContactLN = "Veritas";

	public static final String ContactFirstName = "qa";
	public static final String ContactLastName = "auto";
	public static final String ContactLastNameSSO = "auto CA";

	public static final String ContactFirm = "Mass";

	public static final String ContactFirstNamePartial = "qa";
	public static final String ContactLastNamePartial = "auto";

	public static final String Address1 = "913 Commerce CT";
	public static final String City = "Buffalo Grove";
	public static final String Zip = "60089";
	public static final String State = "Illinois";
	public static final String Country = "United States";

	public static final String ResrtictedFirm = "Restricted by Firm";
	public static final String ProductCodeTest = "Test";
	public static final String ManageDropDowntext = "UPS";
	public static final String CostCenterName = "9999-1234";
	public static final String MaxOrderMsg = "Max Order Quantity is 5!";
	public static final String NoProductFound = "No Products Found";
	public static final String ItemsAddedtoCart = "Item(s) were added to your cart.";
	public static final String ShoppingCartisEmpty = "Your Shopping Cart Is Empty";
	public static final String MaillistProcessedSuccessfully = "Mail list processed successfully!";
	public static final String ItemsHavebeenRemoved = "Item(s) have been removed from your cart.";
	public static final String PIMCOMailing = System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx";// "C:\\Users\\ihsan.shuweikeh\\Documents\\AutomatedTestCases\\PIMCO\\PIMCO_Maillist.xlsx";

	// GWM Admin
	public static final String Partnumber1 = "QA_Bulkinventoryupdate1";
	public static final String Partnumber2 = "QA_Bulkinventoryupdate2";
	public static final String Partnumber3 = "QA_Bulkinventoryupdate3";
	public static final String Partnumber4 = "QA_Bulkinventoryupdate4";
	public static final String Partnumber5 = "QA_Bulkinventoryupdate5";
	public static final String Partnumber6 = "QA_Bulkinventoryupdate6";

	// public static final String Partnumber7 = "QA_Piece1";
	// public static final String Partnumber8 = "QA_Piece2";
	// public static final String Partnumber9 = "QA_Piece3";
	// public static final String Partnumber10 = "QA_Piece4";

	public static final String Partnumber11 = "TestKitOnFly_01";
	public static final String Partnumber12 = "TestKitOnFly_02";
	public static final String Partnumber13 = "TestKitOnFly_03";

	public static final String Partnumber14 = "TestFactSheet_01";
	public static final String Partnumber15 = "TestFactSheet_02";
	public static final String Partnumber16 = "TestFactSheet_05";

	public static final String Partnumber17 = "TestFundCard_01";
	public static final String Partnumber18 = "TestFundCard_02";
	public static final String Partnumber19 = "TestFundCard_03";
	public static final String Partnumber20 = "TestFundCard_04";
	public static final String Partnumber21 = "TestFundCard_05";

	public static final String Partnumber22 = "TestSupp_01";
	public static final String Partnumber23 = "TestSupp_02";
	public static final String Partnumber24 = "TestSupp_03";
	public static final String Partnumber25 = "TestSupp_04";

	public static final String Partnumber26 = "TestFolder_01";
	public static final String Partnumber27 = "TestKitPrebuilt_01";

	public static final String Partnumber28 = "AJedraszczak_BookTest_022316";
	public static final String Partnumber29 = "QA_Marketingonly";
	public static final String Partnumber30 = "QA_FirmRestrictionTest";
	public static final String Partnumber31 = "QA_MultifunctionTestPart";
	public static final String Partnumber32 = "Test777_77777";
	public static final String Partnumber33 = "QA_Obsolete";
	public static final String Partnumber34 = "QA_Replace_Test_2";
	public static final String Partnumber35 = "QA_Test_Replace_0515";
	public static final String Partnumber36 = "QA_BookbuilderKit1";
	public static final String Partnumber37 = "QA_FulfillmentOnly";

	// CA Parts
	public static final String Partnumber38 = "QA_CABulkinventoryupdate1";
	public static final String Partnumber39 = "QA_CABulkinventoryupdate2";
	public static final String Partnumber40 = "QA_CABulkinventoryupdate3";
	public static final String Partnumber41 = "QA_CABulkinventoryupdate4";
	public static final String Partnumber42 = "QA_CABulkinventoryupdate5";
	public static final String Partnumber43 = "QA_CABulkinventoryupdate6";
	public static final String Partnumber44 = "QA_MultifunctionTest";

}
