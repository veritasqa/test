package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Base.BasePage;
import Util.CommonMethods;
//import junit.framework.Assert;

public class CA_Basic_Page extends BasePage {

	Actions act = new Actions(Driver);

	public CA_Basic_Page(WebDriver Driver) {

		super(Driver);
	}

	@FindBy(how = How.XPATH, using = "//*[text()='Admin']")
	public WebElement BasicAdmin;
	@FindBy(how = How.XPATH, using = ".//*[@id='ctl00_cphContent_RadDockc6db58d2-4dde-4ebb-9999-abd6df3bb8c2_C_ctl00_lblFillOutContactInfo']/a")
	public WebElement MostPopularItems;

	public void CABasicLogin(String browser) throws InterruptedException {
		ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		MI.Username.sendKeys(Config.Constants.CA_Basic_User);
		MI.Password.sendKeys(Config.Constants.CA_Basic_PWD);
		MI.SearchButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
	}

	public void FulfillmentOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearch();
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
		CAAP.CheckoutPageNextButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutButton);
		CAAP.ShippingMethod_LCSMButton.click();
		CAAP.CheckoutButton.click();
		String ON = CommonMethods.GetText(CAAP.Orderinfo_PartNumber);
		CAAP.OkButton.click();
		CommonMethods.waitforElement(CAAP.HomePageImage);

		if (Driver.getCurrentUrl().equals(Config.Constants.URL_PRD_Cancel)) {
			CAAP.InvenLogin();
			CAAP.Inven_OrderNoTextField.sendKeys(ON);
			CAAP.InvenSearchandCancel();
		}
	}

	public void BulkOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.StartABulkOrderButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		CAAP.MailListOkButton.click();
		CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
		CAAP.SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber44);
		CAAP.SearchForMaterialsButton.click();
		CommonMethods.waitforElement(CAAP.AddToCart);
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.BulkOrderClearCartButton);
		CAAP.BulkOrderClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ClearShoppingCartFulfillment(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearch();
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
		CAAP.CheckoutPageNextButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutButton);
		CAAP.ShippingMethod_LCSMButton.click();
		CAAP.ShoppingCartClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void PagingFunctionality(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.ProductCodeTest);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstNamePartial);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNamePartial);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CAAP.HomePage_ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void VerifyMailList(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.StartaBulkOrderButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		Assert.assertEquals(CAAP.MailListFalseField.getText().trim(), "False");
		CAAP.ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
	}

	public void TrainingGuides(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		String winHandleBefore = Driver.getWindowHandle();
		CAAP.TrainingGuide.click();
		for (String winHandle : Driver.getWindowHandles()) {
			Driver.switchTo().window(winHandle);
		}
		Driver.close();
		Driver.switchTo().window(winHandleBefore);
	}

	public void MostPopularItems(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		MostPopularItems.click();
		//CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CAAP.HomePage_ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}
}
