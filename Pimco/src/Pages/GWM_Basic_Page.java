package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Base.BasePage;
import Util.CommonMethods;

public class GWM_Basic_Page extends BasePage {

	Actions act = new Actions(Driver);

	public GWM_Basic_Page(WebDriver Driver) {

		super(Driver);
	}

	@FindBy(how = How.CSS, using = "#cphContent_txtAddress1")
	public WebElement Address1;
	@FindBy(how = How.CSS, using = "#cphContent_txtCity")
	public WebElement City;
	@FindBy(how = How.CSS, using = "#cphContent_ddlState")
	public WebElement State;
	@FindBy(how = How.CSS, using = "#cphContent_txtZip")
	public WebElement Zip;
	@FindBy(how = How.CSS, using = "#cphContent_ddlCountry")
	public WebElement Country;
	@FindBy(how = How.CSS, using = "#divAvailability")
	public WebElement RestrictedByFirm;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_txtFirm")
	public WebElement ContactSearchFirm_TextField;

	public void GWMBasicLogin(String browser) throws InterruptedException {
		ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		MI.Username.sendKeys(Config.Constants.GWM_PRD_Basic_User);
		MI.Password.sendKeys(Config.Constants.GWM_PRD_Basic_PWD);
		MI.SearchButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
	}

	public void FulfillmentOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartABulkOrderButton);
//		CAAP.HomeButton.click();
//		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber31);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearch();
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
		Address1.sendKeys(Config.Constants.Address1);
		City.sendKeys(Config.Constants.City);
		Util.CommonMethods.SelectFromDropDown(State, Config.Constants.State);
		Zip.sendKeys(Config.Constants.Zip);
		Util.CommonMethods.SelectFromDropDown(Country, Config.Constants.Country);
		CAAP.CheckoutPageNextButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutButton);
		CAAP.ShippingMethod_LCSMButton.click();
		CAAP.CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.OkButton);
		String ON = CommonMethods.GetText(CAAP.Orderinfo_PartNumber);
		CAAP.OkButton.click();
		CommonMethods.waitforElement(CAAP.HomePageImage);
		if (Driver.getCurrentUrl().equals(Config.Constants.URL_PRD_Cancel)) {
			CAAP.InvenLogin();
			CAAP.Inven_OrderNoTextField.sendKeys(ON);
			CAAP.InvenSearchandCancel();
		}
	}

	public void BulkOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.StartABulkOrderButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		CAAP.MailListOkButton.click();
		CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
		CAAP.SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber31);
		CAAP.SearchForMaterialsButton.click();
		CommonMethods.waitforElement(CAAP.AddToCart);
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.BulkOrderClearCartButton);
		CAAP.BulkOrderClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ClearShoppingCartFulfillment(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearch();
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
		Address1.sendKeys(Config.Constants.Address1);
		City.sendKeys(Config.Constants.City);
		Util.CommonMethods.SelectFromDropDown(State, Config.Constants.State);
		Zip.sendKeys(Config.Constants.Zip);
		Util.CommonMethods.SelectFromDropDown(Country, Config.Constants.Country);
		CAAP.CheckoutPageNextButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutButton);
		CAAP.ShippingMethod_LCSMButton.click();
		CAAP.ShoppingCartClearCartButton.click();
	}

	public void PagingFunctionality(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.ProductCodeTest);
		CAAP.FullfulmentSearchButton.click();
		// CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstNamePartial);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNamePartial);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CAAP.ClearCartButton.click();
	}

	public void VerifyMailList(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.StartaBulkOrderButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		Assert.assertEquals(CAAP.MailListFalseField.getText().trim(), "False");
		CAAP.ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
	}

	public void TrainingGuides(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		String winHandleBefore = Driver.getWindowHandle();
		CAAP.TrainingGuide.click();
		for (String winHandle : Driver.getWindowHandles()) {
			Driver.switchTo().window(winHandle);
		}
		Driver.close();
		Driver.switchTo().window(winHandleBefore);
	}

	public void MostPopularItems(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		CABP.MostPopularItems.click();
		CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CAAP.HomePage_ClearCartButton.click();
	}

	public void PartRestrictionByFirm(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber30);
		CAAP.FullfulmentSearchButton.click();
		CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		ContactSearchFirm_TextField.sendKeys(Config.Constants.ContactFirm);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
		Assert.assertEquals(RestrictedByFirm.getText().trim(), Config.Constants.ResrtictedFirm);
		CAAP.ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}
}
