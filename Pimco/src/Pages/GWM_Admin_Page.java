package Pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Base.BasePage;
import Util.CommonMethods;

public class GWM_Admin_Page extends BasePage {

	Actions act = new Actions(Driver);

	public GWM_Admin_Page(WebDriver Driver) {

		super(Driver);
	}

	@FindBy(how = How.CSS, using = "#cphContent_btnClearCart")
	public WebElement GWM_ClearCartButton;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Phrases']")
	public WebElement GWM_ManagePhrases;
	@FindBy(how = How.CSS, using = ".PageSection.full>h1")
	public WebElement GWM_ManagePhrasesHeader;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Users']")
	public WebElement GWM_ManageUsers;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_UserName")
	public WebElement GWM_ManagerUsersUsersNameTextField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_UserName")
	public WebElement GWM_ManagerUsersUsersNameIcon;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[2]")
	public WebElement GWM_ManageUsersUserNameResults;
	@FindBy(how = How.CSS, using = ".rmActive.rmVertical.rmGroup.rmLevel1")
	public WebElement GWM_ManagerUsersIconWindow;

	DateFormat df = new SimpleDateFormat("MMM_dd_yyyy");
	Date d = new Date();
	String time = df.format(d);
	String Comments = "Auto_QA_" + time;

	public void GWMLogin(String browser) throws InterruptedException {
		ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		MI.Username.sendKeys(Config.Constants.GWM_Admin_User);
		MI.Password.sendKeys(Config.Constants.GWM_Admin_PWD);
		MI.SearchButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
	}

	public void MarketingCampaignOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.StartaMailingButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		CAAP.MailListOkButton.click();
		CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
		CAAP.SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber31);
		CAAP.SearchForMaterialsButton.click();
		CommonMethods.waitforElement(CAAP.AddtoDripButton);
		CAAP.AddtoDripButton.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(GWM_ClearCartButton);
		GWM_ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaMailingButton);
	}

	public void FulfillmentOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber31);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearch();
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
		GWMB.Address1.sendKeys(Config.Constants.Address1);
		GWMB.City.sendKeys(Config.Constants.City);
		Util.CommonMethods.SelectFromDropDown(GWMB.State, Config.Constants.State);
		GWMB.Zip.sendKeys(Config.Constants.Zip);
		Util.CommonMethods.SelectFromDropDown(GWMB.Country, Config.Constants.Country);
		CAAP.CheckoutPageNextButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutButton);
		CAAP.ShippingMethod_LCSMButton.click();
		CAAP.CheckoutButton.click();
		String ON = CommonMethods.GetText(CAAP.Orderinfo_PartNumber);
		CAAP.OkButton.click();
		CommonMethods.waitforElement(CAAP.HomePageImage);
		if (Driver.getCurrentUrl().equals(Config.Constants.URL_PRD_Cancel)) {
			CAAP.InvenLogin();
			CAAP.Inven_OrderNoTextField.sendKeys(ON);
			CAAP.InvenSearchandCancel();
		}
	}

	public void BulkOrder(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.StartABulkOrderButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		CAAP.MailListOkButton.click();
		CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
		CAAP.SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber31);
		CAAP.SearchForMaterialsButton.click();
		CommonMethods.waitforElement(CAAP.AddToCart);
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.BulkOrderClearCartButton);
		CAAP.BulkOrderClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ClearShoppingCartFulfillment(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
		CAAP.FullfulmentSearchButton.click();
		CAAP.ContactSearch();
		CAAP.AddToCart.click();
		CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
		CAAP.ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
		GWMB.Address1.sendKeys(Config.Constants.Address1);
		GWMB.City.sendKeys(Config.Constants.City);
		Util.CommonMethods.SelectFromDropDown(GWMB.State, Config.Constants.State);
		GWMB.Zip.sendKeys(Config.Constants.Zip);
		Util.CommonMethods.SelectFromDropDown(GWMB.Country, Config.Constants.Country);
		CAAP.CheckoutPageNextButton.click();
		CommonMethods.waitforElement(CAAP.CheckoutButton);
		CAAP.ShippingMethod_LCSMButton.click();
		CAAP.ShoppingCartClearCartButton.click();
		Thread.sleep(5000);
	}

	public void PagingFunctionality(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.ProductCodeTest);
		CAAP.FullfulmentSearchButton.click();
		// CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstNamePartial);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNamePartial);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CAAP.HomePage_ClearCartButton.click();
	}

	public void VerifyMailList(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.StartaBulkOrderButton.click();
		CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
		CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		CAAP.MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(CAAP.MailListOkButton);
		Assert.assertEquals(CAAP.MailListFalseField.getText().trim(), "False");
		CAAP.ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
	}

	public void TrainingGuides(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		String winHandleBefore = Driver.getWindowHandle();
		CAAP.TrainingGuide.click();
		for (String winHandle : Driver.getWindowHandles()) {
			Driver.switchTo().window(winHandle);
		}
		Driver.close();
		Driver.switchTo().window(winHandleBefore);
	}

	public void PartRestrictionByFirm(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.HomePage_Logo);
		CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber30);
		CAAP.FullfulmentSearchButton.click();
		CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		GWMB.ContactSearchFirm_TextField.sendKeys(Config.Constants.ContactFirm);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
		Assert.assertEquals(GWMB.RestrictedByFirm.getText().trim(), Config.Constants.ResrtictedFirm);
		CAAP.ClearCartButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void Reports(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.Reports.click();
		CommonMethods.waitforElement(CAAP.ReportName_BillingMonthly);
		Assert.assertEquals(CAAP.ReportName_BillingMonthly.getText().trim(), "Billing_ Monthly");
	}

	public void ReportScheduler(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		CAAP.Reports.click();
		CommonMethods.waitforElement(CAAP.ReportName_BillingMonthly);
		CAAP.ReportName_BillingMonthly.click();
		CommonMethods.waittodisappear(CAAP.LoadingIconimg);
		CAAP.ScheduleArrowdown.click();
		CommonMethods.waitforElement(CAAP.ScheduleArrowdownWindow);
		CAAP.ScheduleSelect.click();
		CAAP.ReportTypeArrow.click();
		CommonMethods.waitforElement(CAAP.ReportTypeWindow);
		CAAP.ReportTypeselect_Excel.click();
		CAAP.ScheduleComments.sendKeys(Comments);
		CAAP.ScheduleSubscribeButton.click();
		CommonMethods.waitforElement(CAAP.Unsubscribe);
		CAAP.Unsubscribe.click();
		CommonMethods.waitforElement(CAAP.UnsubscribeOKAlert);
		CAAP.UnsubscribeOKAlert.click();
		CommonMethods.waitforElement(CAAP.NoSubscriptionsAvailable);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ResetPhrasesSetting(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(CAAP.Veritas).build().perform();
		CAAP.ResetPhrasesAndSetting.click();
		CommonMethods.waitforElement(CAAP.ResetPhareses_SettingAndVar);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ContactsSearch(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(CAAP.Veritas).build().perform();
		CAAP.ContactSearch.click();
		CommonMethods.waitforElement(CAAP.ContactSearchHeader);
		Thread.sleep(3000);
		CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		CAAP.ContactSearchButton.click();
		CommonMethods.waitforElement(CAAP.SelectContact);
		CAAP.SelectContact.click();
		CAAP.HomeButton.click();
	}

	public void ManageAnnouncements(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		CAAP.ManageAnnouncements.click();
		CommonMethods.waitforElement(CAAP.ManageAnnouncementsHeader);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void KitQualityControl(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(CAAP.Veritas).build().perform();
		CAAP.KitQualityControl.click();
		CommonMethods.waitforElement(CAAP.RecentKitChanes);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ManagePhrases(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		GWM_ManagePhrases.click();
		CommonMethods.waitforElement(GWM_ManagePhrasesHeader);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ManageUsers(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		GWM_ManageUsers.click();
		CommonMethods.waitforElement(GWM_ManagePhrasesHeader);
		GWM_ManagerUsersUsersNameTextField.sendKeys(Config.Constants.CA_Admin_User);
		GWM_ManagerUsersUsersNameIcon.click();
		CommonMethods.waittodisappear(CAAP.LoadingIconimg);
		Thread.sleep(5000);
		Assert.assertEquals(GWM_ManageUsersUserNameResults.getText().trim(), Config.Constants.CA_Admin_User);
		CAAP.HomeButton.click();
		CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
	}

	public void ManageDropdowns(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(CAAP.Veritas).build().perform();
		CAAP.ManageDropdowns.click();
		CommonMethods.waitforElement(CAAP.ManageDropdown_TextFilterfield);
		CAAP.ManageDropdown_TextFilterfield.sendKeys(Config.Constants.ManageDropDowntext);
		CAAP.ManageDropdown_TextFilterIcon.click();
		// CommonMethods.waitforElement(CAAP.ManageDropdown_FilterIconWindow);
		CommonMethods.waitforElement(CAAP.ManageDropdown_result);
	}

	public void ManageCategories(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		CAAP.ManageCategories.click();
		CommonMethods.waitforElement(CAAP.ManageCategory_CategoryHeader);
	}

	public void ManageRoles(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		CAAP.ManageRoles.click();
		CommonMethods.waitforElement(CAAP.ManageRoles_SecurityHeader);
	}

	public void ManageCostCenter(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		CAAP.ManageCostCenter.click();
		CommonMethods.waitforElement(CAAP.AddCostCenter);
		CAAP.AddCostCenter.click();
		CommonMethods.waitforElement(CAAP.CostCenterTextField);
		CAAP.CostCenterTextField.sendKeys(Config.Constants.CostCenterName);
		CAAP.CostCenterNameTextField.sendKeys(Comments);
		CAAP.CreateCostCenterButton.click();
		// CommonMethods.waitforElement(CAAP.CostCenterSearchField);
		// CAAP.CostCenterSearchField.sendKeys(Config.Constants.CostCenterName);
		// CAAP.CostCenterFilterIcon.click();
		// CommonMethods.waitforElement(CAAP.CostCenterName);
		// Assert.assertEquals(CAAP.CostCenterName.getText().trim(), Comments);
		// CAAP.CostCenterDeleteButton.click();
		// CommonMethods.waitforElement(CAAP.CostCenterDeletedMSG);
	}

	public void SpecialProject(String browser) throws InterruptedException {
		CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
		WebElement Admin = CAAP.Admindrop;
		act.moveToElement(Admin).build().perform();
		CommonMethods.waitforElement(CAAP.SpecialProjectDropdown);
		act.moveToElement(CAAP.SpecialProjectDropdown).build().perform();
		Thread.sleep(3000);
		CommonMethods.waitforElement(CAAP.ADDSpecialProject);
		CAAP.ADDSpecialProject.click();
		CommonMethods.waitforElement(CAAP.JobTitleTextField);
		CAAP.JobTitleTextField.sendKeys(Comments);
		CommonMethods.SelectFromDropDown(CAAP.JobTypeDropDown, "Other");
		CAAP.CreateSpecialProjectsButton.click();
		CommonMethods.waitforElement(CAAP.DataHasbeenSavedMSG);
		CAAP.ProgressFilesButton.click();
		CAAP.ProgressFilesAddCommentButton.click();
		CAAP.AddCommentTextField.click();
		CommonMethods.waitforElement(CAAP.AddCommentTextField);
		CAAP.AddCommentTextField.sendKeys(Comments);
		CAAP.TypeDropDownArrow.click();
		CommonMethods.waitforElement(CAAP.Type_Window);
		CAAP.Type_Cancel.click();
		CAAP.CommentSubmitButton.click();
		CommonMethods.waitforElement(CAAP.DataHasbeenSavedMSG);
		act.moveToElement(Admin).build().perform();
		CAAP.SpecialProjectDropdown.click();
		CommonMethods.waitforElement(CAAP.SpecialProject_ClosedCheckbox);
		CAAP.SpecialProject_ClosedCheckbox.click();
		Thread.sleep(10000);
		CAAP.JopTitleFilterBox.sendKeys(Comments);
		CommonMethods.waitforElement(CAAP.JopTitleFilterIcon);
		CAAP.JopTitleFilterIcon.click();
		CommonMethods.waitforElement(CAAP.SpecialProIconWindow);
		CAAP.JopTitleFilterContains.click();
		CommonMethods.waitforElement(CAAP.JobTitleCommentfield);
		Assert.assertEquals(CAAP.JobTitleCommentfield.getText().trim(), Comments);
	}
}
