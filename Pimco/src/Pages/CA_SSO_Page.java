 package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Base.BasePage;
import Util.CommonMethods;

public class CA_SSO_Page extends BasePage {

	Actions act = new Actions(Driver);
		
		public CA_SSO_Page(WebDriver Driver){
			
			super(Driver);
		}  
		
		@FindBy(how = How.CSS, using = "#txtUserId")
		public WebElement UserID;
		@FindBy(how = How.CSS, using = "#txtFirstName")
		public WebElement FirstName;
		@FindBy(how = How.CSS, using = "#txtLastName")
		public WebElement LastName;
		@FindBy(how = How.CSS, using = "#txtCostCenter")
		public WebElement CostCenter;
		@FindBy(how = How.CSS, using = "#txtEmail")
		public WebElement Email;
		@FindBy(how = How.CSS, using = "#txtPhone")
		public WebElement Phone;
		@FindBy(how = How.CSS, using = "#txtUserGroup1")
		public WebElement UserGroup;
		@FindBy(how = How.CSS, using = "#cbIncludeContact")
		public WebElement IncludeContactInfo;
		@FindBy(how = How.XPATH, using = ".//*[@id='btnAuthenticate']")
		public WebElement AuthenticateButton;
		@FindBy(how = How.CSS, using = "#btnRedirect")
		public WebElement RedirectButton;
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock6a86166c-ea52-41f5-a415-3b0d6b674f06_C_ctl00_lblFillOutContactInfo>a")
		public WebElement ExpressCheckout_SelectARecipient;
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock6a86166c-ea52-41f5-a415-3b0d6b674f06_C_ctl00_rtxtFormNumber1_Input")
		public WebElement ExpressCheckout_ProductCode;
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock6a86166c-ea52-41f5-a415-3b0d6b674f06_C_ctl00_txtQuantity1")
		public WebElement ExpressCheckout_ProductQTY;
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock6a86166c-ea52-41f5-a415-3b0d6b674f06_C_ctl00_btnAddToCart")
		public WebElement ExpressCheckout_chkoutButton;
		
		
		public void CASSOLogin(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			Driver.get(Config.Constants.URL_SSO_PRD);
			CommonMethods.waitforElement(UserID);
			UserID.clear();
			UserID.sendKeys(Config.Constants.SSO_CA_UserID);
			FirstName.clear();
			FirstName.sendKeys(Config.Constants.SSO_CA_FirstName);
			LastName.clear();
			LastName.sendKeys(Config.Constants.SSO_CA_LastName);
			CostCenter.clear();
			CostCenter.sendKeys(Config.Constants.SSO_CA_CostCenter);
			Email.clear();
			Email.sendKeys(Config.Constants.SSO_Email);
			Phone.clear();
			Phone.sendKeys(Config.Constants.SSO_Phone);
			UserGroup.clear();
			UserGroup.sendKeys(Config.Constants.SSO_BankUser);
			IncludeContactInfo.click();
			AuthenticateButton.click();
			RedirectButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
		}
		public void FulfillmentOrder(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
			CAAP.FullfulmentSearchButton.click();
			CAAP.ContactSearchSSO();
			CAAP.AddToCart.click();
			CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
			CAAP.ShoppingCart_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
			CAAP.CheckoutPageNextButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutButton);		
			CAAP.ShippingMethod_LCSMButton.click();
			CAAP.CheckoutButton.click();	
			String ON = CommonMethods.GetText(CAAP.Orderinfo_PartNumber);
			CAAP.OkButton.click();
			CommonMethods.waitforElement(CAAP.HomePageImage);
			if(Driver.getCurrentUrl().equals(Config.Constants.URL_PRD_Cancel)){
				CAAP.InvenLogin();
				CAAP.Inven_OrderNoTextField.sendKeys(ON);
				CAAP.InvenSearchandCancel();
		}
		}
		public void MyUserKitsWidgetS1(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
			CAAP.MyUserKits_EditViewMyUserKits.click();
			CommonMethods.waitforElement(CAAP.ManageUserKits_AddNewUserKit);
		}
		public void MyUserKitsWidgetS2(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
			CAAP.MyUserKits_SelectARecipient.click();
			CommonMethods.waitforElement(CAAP.ContactSearchHeader);
			CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
			CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNameSSO);
			CAAP.ContactSearchButton.click();
			CommonMethods.waitforElement(CAAP.SelectContact);
			CAAP.SelectContact.click();
			CommonMethods.waitforElement(CAAP.ClearCartButton);
			CAAP.ClearCartButton.click();
			CommonMethods.waitforElement(CAAP.MyUserKits_SelectARecipient);		
		}
		public void BulkOrder(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.StartABulkOrderButton.click();
			CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
			CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir")+"/MailList/PIMCO_Maillist.xlsx");
			CAAP.MailList_UploadMailListButton.click();
			CommonMethods.waitforElement(CAAP.MailListOkButton);
			CAAP.MailListOkButton.click();
			CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
			CAAP.SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber44);
			CAAP.SearchForMaterialsButton.click();
			CommonMethods.waitforElement(CAAP.AddToCart);
			CAAP.AddToCart.click();
			CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
			CAAP.ShoppingCart_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.BulkOrderClearCartButton);		
			CAAP.BulkOrderClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);		
		}
		public void ClearShoppingCartFulfillment(String browser) throws InterruptedException{	
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
			CAAP.FullfulmentSearchButton.click();
			CAAP.ContactSearchSSO();
			CAAP.AddToCart.click();
			CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
			CAAP.ShoppingCart_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
			CommonMethods.waitforElement(CAAP.CheckoutPageNextButton);
			CAAP.CheckoutPageNextButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutButton);		
			CAAP.ShippingMethod_LCSMButton.click();
			CAAP.ShoppingCartClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);		
		}
		public void PagingFunctionality(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.ProductCodeTest);
			CAAP.FullfulmentSearchButton.click();
			CommonMethods.waitforElement(CAAP.ContactSearchHeader);
			CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstNamePartial);
			CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNamePartial);
			CAAP.ContactSearchButton.click();
			CommonMethods.waitforElement(CAAP.SelectContact);
			CAAP.SelectContact.click();
			CAAP.HomePage_ClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
		    }
		public void VerifyMailList(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.StartaBulkOrderButton.click();
			CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
			CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir")+"/MailList/PIMCO_Maillist.xlsx");
			CAAP.MailList_UploadMailListButton.click();
			CommonMethods.waitforElement(CAAP.MailListOkButton);
			Assert.assertEquals(CAAP.MailListFalseField.getText().trim(), "False");
			CAAP.ClearCartButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
		}
		public void TrainingGuides(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);		
			String winHandleBefore = Driver.getWindowHandle();	
			CAAP.TrainingGuide.click();		
			for(String winHandle : Driver.getWindowHandles()){
			Driver.switchTo().window(winHandle);
			}		
			Driver.close();		
			Driver.switchTo().window(winHandleBefore);		
		}
		public void ExpressCheckoutWidget(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			ExpressCheckout_SelectARecipient.click();
			CommonMethods.waitforElement(CAAP.ContactSearchHeader);
			CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
			CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNameSSO);
			CAAP.ContactSearchButton.click();
			CommonMethods.waitforElement(CAAP.SelectContact);
			CAAP.SelectContact.click();
			ExpressCheckout_ProductCode.sendKeys(Config.Constants.Partnumber44);
			ExpressCheckout_ProductQTY.sendKeys("1");
			ExpressCheckout_chkoutButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutPageNextButton);
			CAAP.ShoppingCartClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);		
		}
}



