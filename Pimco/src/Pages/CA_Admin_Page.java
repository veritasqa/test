package Pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Base.BasePage;
import Util.CommonMethods;

public class CA_Admin_Page extends BasePage {

	Actions act = new Actions(Driver);

	public CA_Admin_Page(WebDriver Driver) {

		super(Driver);
	}

	@FindBy(how = How.XPATH, using = "//img[@src='/JNLFull/IMG/navHome.png']")
	public WebElement Home;
	@FindBy(how = How.XPATH, using = "//span[text()='Cancel Order']")
	public WebElement CancelOrder;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnOk']")
	public WebElement OK;
	@FindBy(how = How.XPATH, using = "//td[text()='QA']")
	public WebElement Shipping;
	@FindBy(how = How.XPATH, using = "//*[text()='Checkout']")
	public WebElement CartCheckout;
	@FindBy(how = How.XPATH, using = "//span[text()='Client Collaboration']")
	public WebElement ClientCol;
	@FindBy(how = How.XPATH, using = "//a[text()='shipping information']")
	public WebElement Shippinginformation;
	@FindBy(how = How.XPATH, using = "//span[text()='Campaigns']")
	public WebElement Campaings;
	@FindBy(how = How.XPATH, using = "//input[@ value='Create Your Own Campaign']")
	public WebElement CreateCampaign;
	@FindBy(how = How.CSS, using = ".PageSection.full>h1")
	public WebElement ShippingInformationHeader;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Contacts_ctl00__0>td:nth-child(2)")
	public WebElement ShippingAddressSelect_QA;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Contacts_ctl00__1>td:nth-child(2)")
	public WebElement ShippingAddressSelect_QA2;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00_ctl04_aThumbnail>img")
	public WebElement ProductIconImage;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00__0>td:nth-child(2) img")
	public WebElement ProductIconForPendingImage;
	@FindBy(how = How.LINK_TEXT, using = "FINRA")
	public WebElement FINRA_Link;
	@FindBy(how = How.LINK_TEXT, using = "MorganStanley")
	public WebElement MorganS_Link;
	@FindBy(how = How.LINK_TEXT, using = "BrainShark")
	public WebElement BrainShark_Link;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ExpressCheckout_lblFillOutContactInfo>a")
	public WebElement ExpressCheckout_ShippingInfoLink;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ExpressCheckout_rtxtFormNumber1 input[id='ctl00_cphContent_ExpressCheckout_rtxtFormNumber1_Input']")
	public WebElement ExpressCheckout_ShippingTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ExpressCheckout_txtQuantity1")
	public WebElement ExpressCheckout_ShippingNumberTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ExpressCheckout_btnAddToCartAndStay")
	public WebElement ExpressCheckout_AddToCart;
	@FindBy(how = How.XPATH, using = "//span[text()='Item(s) were added to your cart.']")
	public WebElement ExpressCheckout_Itemsmsg;
	@FindBy(how = How.XPATH, using = "//li[text()='QA_MULTIFUNCTION']")
	public WebElement ExpressCheckout_Dropdown;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00_ctl04_lblInfoMessage")
	public WebElement PendingFirmApproval;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideCommandBar_txtKeyword")
	public WebElement ProductSearchTextField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideCommandBar_btnSearch")
	public WebElement ProductSearchButton;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00_ctl04_btnAddToWishList")
	public WebElement ParkForLaterButton;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_ChangeStateFirmRestriction1_ddlState_Arrow")
	public WebElement ChangeState_ArrowDP;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_ChangeStateFirmRestriction1_ddlState_DropDown li:nth-child(16)")
	public WebElement ChangeState_Iowa;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_ChangeStateFirmRestriction1_btnUpdateState")
	public WebElement ChangeState_UpdateButton;
	@FindBy(how = How.XPATH, using = "//span[text()='Pending state & firm approval']")
	public WebElement PendingStateFirmapproval;
	@FindBy(how = How.XPATH, using = "//div[text()='No Products Found']")
	public WebElement NoProductsFound;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_btnCreateYourOwnCampaign")
	public WebElement CreateYourOwnCampaingButton;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideHeader_imgHeader")
	public WebElement DripCampaingsHeader;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl00_rdpDrip_popupButton")
	public WebElement ShoppingCartDateIcon_1;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl01_rdpDrip_popupButton")
	public WebElement ShoppingCartDateIcon_2;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl00_rdpDrip_calendar_NN")
	public WebElement ShoppingCartDateIcon1_NextMontharrow;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl00_rdpDrip_calendar_Top']/tbody/tr[3]/td[4]/a")
	public WebElement ShoppingCartDateIcon1_SelectCalendarDate;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl01_rdpDrip_calendar_NN']")
	public WebElement ShoppingCartDateIcon2_NextMontharrow;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl01_rdpDrip_calendar_Top']/tbody/tr[3]/td[4]/a")
	public WebElement ShoppingCartDateIcon2_SelectCalendarDate;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_lblErrorMessage")
	public WebElement ShoppingCart_MsgUpdate;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00__0']/td[3]/div[contains(text(),'QA_MARKETINGONLY')]")
	public WebElement Item_QA_MarketingOnly;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphLeftSide_MiniShoppingCart1_repMiniShoppingCart_ctl01_divFormNumber']")
	public WebElement ShoppingCart_2ndPartnumber;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_cbxCostCenter")
	public WebElement CostCenterCheckbox;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_lblOrderNumber")
	public WebElement OrderNumber;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00_ctl04_txtQuantity")
	public WebElement UpdateQuantityTextfield;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideContent_ProductList_ctl00_ctl04_lblErrorMessage>b")
	public WebElement MaxOrderMsg;
	@FindBy(how = How.XPATH, using = "//span[text()='Investment Management']")
	public WebElement InvestmentManagementButton;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_divMoneyManagers']/div[1]/h1")
	public WebElement MoneyManagerHeader;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_divAssetClasses']/div[1]/h1")
	public WebElement AssetClassesHeader;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_divAudience']/div[1]/h1")
	public WebElement AudienceHeader;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_divValueAdd']/div[1]/h1")
	public WebElement ValueAddHeader;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_divProductName']/div[1]/h1")
	public WebElement ProductsHeader;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_lblEmailRequired")
	public WebElement EmailOnlyRequiredField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_Wholesaler_Name1")
	public WebElement EmailOnly_WholesalerName;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_Wholesaler_Title1")
	public WebElement EmailOnly_WholesalerTitle;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_btnCreateProofs")
	public WebElement ProceedtoCheckout;
	@FindBy(how = How.XPATH, using = "//span[text()='User Favorites']")
	public WebElement UserFavoriteButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlMailList_Arrow")
	public WebElement MailList_Dropdownarrow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlMailList_DropDown>div>ul>li:nth-child(5)")
	public WebElement MailList_SelectCurian;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlMailList_DropDown")
	public WebElement MailList_dropdownwindow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdMailListContents_ctl00__0>td:nth-child(8)")
	public WebElement MailList_ContentTrueValue;
	@FindBy(how = How.XPATH, using = "//*[text()='Client Collaboration']")
	public WebElement ClientCollaboration;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdCollaboration_ctl00_ctl03_ctl01_AddNewRecordButton")
	public WebElement ClientCollaboration_AddJNLTicket;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_ddlCategory")
	public WebElement ClientCollaboration_SelectCategory;
	@FindBy(how = How.CSS, using = "option[value='Inventory Requests']")
	public WebElement ClientCollaboration_InventoryRequests;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_ddlSubCategory")
	public WebElement ClientCollaboration_SubCategory;
	@FindBy(how = How.CSS, using = "option[value='Other']")
	public WebElement ClientCollaboration_SubCategory_Other;
	@FindBy(how = How.CSS, using = "#rcmbxFormNumber_Input")
	public WebElement ClientCollaboration_FormNumber;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_ddlAssignedTo")
	public WebElement ClientCollaboration_AssignedTo;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_btnSubmit")
	public WebElement ClientCollaboration_AddNewTicketButton;
	@FindBy(how = How.CSS, using = ".Box")
	public WebElement ClientCollaboration_TicketBox;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphPageFooter_lblTicketID")
	public WebElement ClientCollaboration_TicketNumber;
	@FindBy(how = How.LINK_TEXT, using = "Logout")
	public WebElement Logout;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_CollaborationID")
	public WebElement ClientCollaboration_TicketNumberTextField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_UpdatedOn")
	public WebElement ClientCollaboration_FilterIcon;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdCollaboration_ctl00__0>td:nth-child(1) a")
	public WebElement ClientCollaboration_CCTicketSelect;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_ddlStatus")
	public WebElement ClientCollaboration_Status;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_btnSubmit")
	public WebElement ClientCollaboration_UpdateTicketButton;
	@FindBy(how = How.CSS, using = ".racList")
	public WebElement ClientCollaboration_Formnumberpopupwindow;
	@FindBy(how = How.CSS, using = "#loginLogo>img")
	public WebElement LoginPage_Logo;

	// Used Locators
	@FindBy(how = How.XPATH, using = "//a[text()='Start a Bulk Order']")
	public WebElement StartaBulkOrderButton;
	@FindBy(how = How.CSS, using = "#cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout")
	public WebElement ShoppingCart_CheckoutButton;
	@FindBy(how = How.CSS, using = "#cphContent_btnMailList")
	public WebElement MailList_UploadMailListButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_fupMailListfile0")
	public WebElement MailList_ListPath;
	@FindBy(how = How.XPATH, using = "//a[text()='Start a Mailing']")
	public WebElement StartaMailingButton;
	@FindBy(how = How.CSS, using = "#Login")
	public WebElement Inven_usernametextfield;
	@FindBy(how = How.CSS, using = "#Password")
	public WebElement Inven_passwordtextfield;
	@FindBy(how = How.CSS, using = "#Submit")
	public WebElement Inven_Submit;
	@FindBy(how = How.XPATH, using = "//a[text()='Edit/View Orders']")
	public WebElement Inven_EditOrders;
	@FindBy(how = How.CSS, using = "#OrderNumber")
	public WebElement Inven_OrderNoTextField;
	@FindBy(how = How.CSS, using = "#btnSearch")
	public WebElement Inven_SearchButton;
	@FindBy(how = How.CSS, using = "#DataList>tbody>tr:nth-child(2)>td:nth-child(8) a")
	public WebElement Inven_EditButton;
	@FindBy(how = How.CSS, using = "#CancelOrder")
	public WebElement Inven_CancelOrderButton;
	@FindBy(how = How.CSS, using = "input[value='CANCELLED']")
	public WebElement Inven_StatusCanceled;
	@FindBy(how = How.CSS, using = "#imgLogo")
	public WebElement HomePage_Logo;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphMain_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphRightSideContent_ProductList>div>img")
	public WebElement LoadingIconimg;
	@FindBy(how = How.XPATH, using = "//h1[text()='Mail List Contents']")
	public WebElement MailListContentsHeader;
	@FindBy(how = How.CSS, using = "#cphContent_btnBack")
	public WebElement MailListOkButton;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword")
	public WebElement SearchByMaterialTextField;
	@FindBy(how = How.CSS, using = "#cphContent_cphRightSideHeader_btnAdvancedSearch")
	public WebElement SearchForMaterialsButton;
	@FindBy(how = How.CSS, using = "#cphContent_btnRemoveExludedContacts")
	public WebElement RemoveRestrictedRecipients;
	@FindBy(how = How.CSS, using = "#cphContent_lblCommandBar>div")
	public WebElement RemovedRestrictedRecipientsMSG;
	@FindBy(how = How.CSS, using = "#cphContent_btnCheckout")
	public WebElement CheckoutButton;
	@FindBy(how = How.XPATH, using = "//span[text()='Clear Cart']")
	public WebElement ClearCartButton;
	@FindBy(how = How.XPATH, using = ".//*[@id='cphContent_btnClearCart']")
	public WebElement ShoppingCartClearCartButton;
	@FindBy(how = How.CSS, using = "#cphContent_btnClearCart")
	public WebElement BulkOrderClearCartButton;
	@FindBy(how = How.CSS, using = "#Navigation_btnCancelOrder")
	public WebElement HomePage_ClearCartButton;
	@FindBy(how = How.XPATH, using = "//*[@id='LandingMainSection']/img")
	public WebElement HomePageImage;
	@FindBy(how = How.CSS, using = "#cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout")
	public WebElement ShoppingCartPage_CheckoutButton;
	@FindBy(how = How.XPATH, using = "//a[text()='Add to Drip']")
	public WebElement AddtoDripButton;
	@FindBy(how = How.XPATH, using = "//*[@id='aShoppingCart']/span")
	public WebElement Checkout;
	@FindBy(how = How.XPATH, using = "//*[@id='Navigation_divLiList']/li[4]/a/span")
	public WebElement Admindrop;
	@FindBy(how = How.XPATH, using = "//*[@id='Navigation_divLiList']/li[3]/a/span")
	public WebElement Reports;
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'txtQuickSearch')]")
	public WebElement FullfulmentSearchTextField;
	@FindBy(how = How.CSS, using = "#btnQuickSearch")
	public WebElement FullfulmentSearchButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_txtFirstName")
	public WebElement ContactSearchFirstName_textField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_txtLastName")
	public WebElement ContactSearchLastName_textField;
	@FindBy(how = How.CSS, using = "#cphContent_divHeaderImage>h1")
	public WebElement ContactSearchHeader;
	@FindBy(how = How.CSS, using = "#cphContent_btnSearch")
	public WebElement ContactSearchButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Contacts_ctl00__0>td:nth-child(2)")
	public WebElement SelectContact;
	@FindBy(how = How.XPATH, using = "//a[text()='Add to Cart']")
	public WebElement AddToCart;
	@FindBy(how = How.XPATH, using = "//*[@id='cphContent_divHeaderImage']/img")
	public WebElement CheckoutPageHeader;
	@FindBy(how = How.CSS, using = "#cphContent_btnShowShipping")
	public WebElement CheckoutPageNextButton;
	@FindBy(how = How.CSS, using = "#cphContent_rdoShippingLowCostMethod")
	public WebElement ShippingMethod_LCSMButton;
	@FindBy(how = How.CSS, using = "#cphContent_cphSection_lblOrderNumber")
	public WebElement Orderinfo_PartNumber;
	@FindBy(how = How.CSS, using = "#cphContent_cphPageFooter_btnOk")
	public WebElement OkButton;
	@FindBy(how = How.CSS, using = "#cphContent_lnkBulkOrder")
	public WebElement StartABulkOrderButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Results_ctl00>tfoot>tr:nth-child(2)>td>table>tbody>tr>td>div:nth-child(2)>a:nth-child(2)>span")
	public WebElement SearchResults2page;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdMailListContents_ctl00__0>td:nth-child(8)")
	public WebElement MailListFalseField;
	@FindBy(how = How.XPATH, using = "//*[@id='Navigation_divLiList']/li[1]/a/span")
	public WebElement HomeButton;
	@FindBy(how = How.XPATH, using = "//span[text()='Special Projects']")
	public WebElement SpecialProjectDropdown;
	@FindBy(how = How.XPATH, using = "//span[text()='Add Special Project']")
	public WebElement ADDSpecialProject;
	@FindBy(how = How.CSS, using = "#cphContent_txtJobTitle")
	public WebElement JobTitleTextField;
	@FindBy(how = How.CSS, using = "#cphContent_ddlJobType")
	public WebElement JobTypeDropDown;
	@FindBy(how = How.CSS, using = "#cphContent_btnSave")
	public WebElement CreateSpecialProjectsButton;
	@FindBy(how = How.CSS, using = "#cphContent_lblMessage")
	public WebElement DataHasbeenSavedMSG;
	@FindBy(how = How.CSS, using = "#cphContent_lnkProgress>span")
	public WebElement ProgressFilesButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Comments_ctl00_ctl03_ctl01_AddNewRecordButton")
	public WebElement ProgressFilesAddCommentButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Comments_ctl00_ctl02_ctl02_txtComment")
	public WebElement AddCommentTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Arrow")
	public WebElement TypeDropDownArrow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnSaveComment")
	public WebElement CommentSubmitButton;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Closed")
	public WebElement SpecialProject_ClosedCheckbox;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_JobTitle")
	public WebElement JopTitleFilterBox;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_JobTitle")
	public WebElement JopTitleFilterIcon;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached>ul>li:nth-child(2)>a")
	public WebElement JopTitleFilterContains;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]")
	public WebElement JobTitleCommentfield;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_DropDown>div>ul>li:nth-child(2)")
	public WebElement Type_Cancel;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_DropDown")
	public WebElement Type_Window;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached")
	public WebElement SpecialProIconWindow;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Phrases']")
	public WebElement ManagePhrases;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterTextBox_DefaultPhrase']")
	public WebElement DefaultPhrasesTextField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_DefaultPhrase")
	public WebElement DefaultPhrasesIcon;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdPhrases_rfltMenu_detached")
	public WebElement DefaultPhrasesIconWindow;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdPhrases_rfltMenu_detached>ul>li:nth-child(2)>a")
	public WebElement DefaultPhrasesIconContains;
	@FindBy(how = How.XPATH, using = "//*[text()='Please select a valid date range']")
	public WebElement DefaultPhrasesPhrase;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Users']")
	public WebElement ManageUsers;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Roles']")
	public WebElement ManageRoles;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Cost Centers']")
	public WebElement ManageCostCenter;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Categories']")
	public WebElement ManageCategories;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Dropdowns']")
	public WebElement ManageDropdowns;
	@FindBy(how = How.XPATH, using = "//*[text()='Reset Phrases & Settings']")
	public WebElement ResetPhrasesAndSetting;
	@FindBy(how = How.XPATH, using = "//*[text()='Kit Quality Control']")
	public WebElement KitQualityControl;
	@FindBy(how = How.XPATH, using = "//span[text()='Veritas']")
	public WebElement Veritas;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_FirstName")
	public WebElement FirstNameTextField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_FirstName")
	public WebElement FirstNameIcon;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0>td:nth-child(4)")
	public WebElement FirstNameResult;
	@FindBy(how = How.XPATH, using = "//*[text()='Billing_ Monthly']")
	public WebElement ReportName_BillingMonthly;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlSchedule_Arrow")
	public WebElement ScheduleArrowdown;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlSchedule_DropDown")
	public WebElement ScheduleArrowdownWindow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlSchedule_DropDown>div>ul>li:nth-child(2)")
	public WebElement ScheduleSelect;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlType_Arrow")
	public WebElement ReportTypeArrow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlType_DropDown")
	public WebElement ReportTypeWindow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlType_DropDown>div>ul>li:nth-child(3)")
	public WebElement ReportTypeselect_Excel;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_txtComments")
	public WebElement ScheduleComments;
	@FindBy(how = How.CSS, using = "#cphContent_btnAdd")
	public WebElement ScheduleSubscribeButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Subscriptions_ctl00__0>td:nth-child(3)>a")
	public WebElement Unsubscribe;
	@FindBy(how = How.XPATH, using = "//div[text()='No Subscriptions Available']")
	public WebElement NoSubscriptionsAvailable;
	@FindBy(how = How.XPATH, using = "//span[text()='OK']")
	public WebElement UnsubscribeOKAlert;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rdTrainingGuides_C_lnkTrainingGuide")
	public WebElement TrainingGuide;
	@FindBy(how = How.CSS, using = "#pageContainer1")
	public WebElement TrainingGuide_PopupWindow;
	@FindBy(how = How.XPATH, using = "//span[text()='Security']")
	public WebElement ManageRoles_SecurityHeader;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00_ctl03_ctl01_AddNewRecordButton")
	public WebElement AddCostCenter;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_rtxtCostCenter")
	public WebElement CostCenterTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_rtxtName")
	public WebElement CostCenterNameTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_btnSaveRole")
	public WebElement CreateCostCenterButton;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_FilterTextBox_CostCenter")
	public WebElement CostCenterSearchField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_Filter_CostCenter")
	public WebElement CostCenterFilterIcon;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdCostCenters_ctl00__0>td:nth-child(2)")
	public WebElement CostCenterName;
	@FindBy(how = How.XPATH, using = "//a[text()='Delete']")
	public WebElement CostCenterDeleteButton;
	@FindBy(how = How.XPATH, using = "//div[text()='No Cost Centers Available']")
	public WebElement CostCenterDeletedMSG;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00>thead>tr>th:nth-child(2)")
	public WebElement ManageCategory_CategoryHeader;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgDropdownLists_ctl00_ctl02_ctl02_FilterTextBox_Text")
	public WebElement ManageDropdown_TextFilterfield;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgDropdownLists_ctl00_ctl02_ctl02_Filter_Text")
	public WebElement ManageDropdown_TextFilterIcon;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgDropdownLists_rfltMenu_detached")
	public WebElement ManageDropdown_FilterIconWindow;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgDropdownLists_rfltMenu_detached>ul>li:nth-child(2) a span")
	public WebElement ManageDropdown_FilterIconContains;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgDropdownLists_ctl00__0")
	public WebElement ManageDropdown_result;
	@FindBy(how = How.CSS, using = "#divContentWrapper")
	public WebElement ResetPhareses_SettingAndVar;
	@FindBy(how = How.XPATH, using = "//*[text()='Recent Kit Changes']")
	public WebElement RecentKitChanes;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdRecentlyChangedKits_ctl00_ctl02_ctl02_FilterTextBox_FormNumber")
	public WebElement RecentKitChanges_FormNumberTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_rgrdRecentlyChangedKits_ctl00_ctl02_ctl02_Filter_FormNumber")
	public WebElement RecentKitChanges_FormNumberFilterIcon;
	@FindBy(how = How.XPATH, using = "//*[text()='Contains']")
	public WebElement FilterIcon_Contains;
	@FindBy(how = How.XPATH, using = "//*[text()='Contact Search']")
	public WebElement ContactSearch;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Announcements']")
	public WebElement ManageAnnouncements;
	@FindBy(how = How.CSS, using = ".PageSection.full>h1")
	public WebElement ManageAnnouncementsHeader;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage User Kits']")
	public WebElement ManageUserKits;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl03_ctl01_InitInsertButton")
	public WebElement ManageUserKits_AddNewUserKit;
	@FindBy(how = How.CSS, using = "#cphContent_txtFormNumber")
	public WebElement ManageUserKits_ProductCodeTextField;
	@FindBy(how = How.CSS, using = "#cphContent_txtDescription")
	public WebElement ManageUserKits_TitleTextField;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_cbKitContainer_Arrow")
	public WebElement ManageUserKits_KitContainerArrowButton;
	@FindBy(how = How.CSS, using = ".rcbHovered.rcbTemplate>table>tbody>tr>td:nth-child(1)>table>tbody>tr:nth-child(1)")
	public WebElement ManageUserKits_KitsContainerFirstoption;
	@FindBy(how = How.CSS, using = "#cphContent_btnSave")
	public WebElement ManageUserKits_SaveUserKit;
	@FindBy(how = How.CSS, using = "#cphContent_btnAddToCart")
	public WebElement ManageUserKits_AddToCartButton;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_Description")
	public WebElement ManageUserKits_TitleSearchField;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_Description")
	public WebElement ManageUserKits_TitleIcon;
	@FindBy(how = How.CSS, using = "#ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached>ul>li:nth-child(2)>a")
	public WebElement ManageUserKits_TitleContains;
	@FindBy(how = How.XPATH, using = "//*[text()='Delete']")
	public WebElement ManageUserKits_Delete;
	@FindBy(how = How.XPATH, using = "//*[text()='No User Kits Available']")
	public WebElement ManageUserKits_NoUserKitsAvailable;
	@FindBy(how = How.XPATH, using = "//*[text()='Edit/View My User Kits']")
	public WebElement MyUserKits_EditViewMyUserKits;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_RadDock3d6d0797-e656-4b4d-afe7-f36de7a0eae3_C_ctl00_lblFillOutContactInfo']/a")
	public WebElement MyUserKits_SelectARecipient;

	DateFormat df = new SimpleDateFormat("MMM_dd_yyyy");
	Date d = new Date();
	String time = df.format(d);

	String Comments = "Auto_QA_May_08_2017";
	// String Comments = "Auto_QA_" + time ;

	public void InvenLogin() throws InterruptedException {
		Driver.get(Config.Constants.URL_Inventory);
		Inven_usernametextfield.sendKeys(Config.Constants.Inven_User);
		Inven_passwordtextfield.sendKeys(Config.Constants.Inven_PWD);
		Inven_Submit.click();
		CommonMethods.waitforElement(Inven_EditOrders);
		Inven_EditOrders.click();
		CommonMethods.waitforElement(Inven_OrderNoTextField);
	}

	public void InvenSearchandCancel() throws InterruptedException {
		Inven_SearchButton.click();
		Inven_EditButton.click();
		CommonMethods.waitforElement(Inven_CancelOrderButton);
		Inven_CancelOrderButton.click();
		CommonMethods.AcceptAlert();
		CommonMethods.waitforElement(Inven_StatusCanceled);

	}

	public void ContactSearch() throws InterruptedException {
		// CommonMethods.waitforElement(ContactSearchHeader);
		ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		ContactSearchButton.click();
		CommonMethods.waitforElement(SelectContact);
		SelectContact.click();
		CommonMethods.waitforElement(SearchByMaterialTextField);
	}

	public void ContactSearchSSO() throws InterruptedException {
		CommonMethods.waitforElement(ContactSearchHeader);
		ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNameSSO);
		ContactSearchButton.click();
		CommonMethods.waitforElement(SelectContact);
		SelectContact.click();
		CommonMethods.waitforElement(SearchByMaterialTextField);
	}

	public void CALogin(String browser) throws InterruptedException {
		ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
		MI.Username.sendKeys(Config.Constants.CA_Admin_User);
		MI.Password.sendKeys(Config.Constants.CA_Admin_PWD);
		MI.SearchButton.click();
		CommonMethods.waitforElement(HomePage_Logo);
	}

	public void MarketingCampaignOrder(String browser) throws InterruptedException {
		StartaMailingButton.click();
		CommonMethods.waitforElement(MailList_UploadMailListButton);
		MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(MailListOkButton);
		MailListOkButton.click();
		CommonMethods.waitforElement(SearchByMaterialTextField);
		SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber44);
		SearchForMaterialsButton.click();
		CommonMethods.waitforElement(AddtoDripButton);
		AddtoDripButton.click();
		CommonMethods.waitforElement(ShoppingCart_CheckoutButton);
		ShoppingCart_CheckoutButton.click();
		// CommonMethods.waitforElement(RemoveRestrictedRecipients);
		// RemoveRestrictedRecipients.click();
		// CommonMethods.waitforElement(RemovedRestrictedRecipientsMSG);
		Thread.sleep(2000);
		CommonMethods.waitforclickelement(ClearCartButton);
		ClearCartButton.click();
		CommonMethods.waitforElement(HomePage_Logo);
	}

	public void FulfillmentOrder(String browser) throws InterruptedException {
		HomeButton.click();

		CommonMethods.waitforElement(HomePage_Logo);
		FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber44);
		FullfulmentSearchButton.click();
		ContactSearch();
		AddToCart.click();
		CommonMethods.waitforElement(ShoppingCart_CheckoutButton);
		ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(CheckoutPageHeader);
		CheckoutPageNextButton.click();
		ShippingMethod_LCSMButton.click();
		CheckoutButton.click();
		Thread.sleep(7000);
		String ON = CommonMethods.GetText(Orderinfo_PartNumber);
		OkButton.click();
		CommonMethods.waitforElement(HomePageImage);
		if (Driver.getCurrentUrl().equals(Config.Constants.URL_PRD_Cancel)) {
			InvenLogin();
			Inven_OrderNoTextField.sendKeys(ON);
			InvenSearchandCancel();

		}
	}

	public void BulkOrder(String browser) throws InterruptedException {
		Thread.sleep(2000);
		HomeButton.click();
		CommonMethods.waitforElement(HomePage_Logo);
		StartABulkOrderButton.click();
		CommonMethods.waitforElement(MailList_UploadMailListButton);
		MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(MailListOkButton);
		MailListOkButton.click();
		CommonMethods.waitforElement(SearchByMaterialTextField);
		SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber44);
		SearchForMaterialsButton.click();
		CommonMethods.waitforElement(AddToCart);
		AddToCart.click();
		CommonMethods.waitforElement(ShoppingCart_CheckoutButton);
		ShoppingCart_CheckoutButton.click();
		CommonMethods.waitforElement(BulkOrderClearCartButton);
		BulkOrderClearCartButton.click();
	}

	public void PagingFunctionality(String browser) throws InterruptedException {
		ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		MI.ManageInventory.click();
		CommonMethods.waitforElement(MI.ProductCode);
		MI.ProductCode.sendKeys(Config.Constants.ProductCodeTest);
		ContactSearchButton.click();
		CommonMethods.waitforElement(SearchResults2page);
		Assert.assertEquals(SearchResults2page.getText().trim(), "2");
	}

	public void VerifyMailList(String browser) throws InterruptedException {
		HomeButton.click();
		CommonMethods.waitforElement(HomePage_Logo);
		StartaMailingButton.click();
		CommonMethods.waitforElement(MailList_UploadMailListButton);
		MailList_ListPath.sendKeys(System.getProperty("user.dir") + "/MailList/PIMCO_Maillist.xlsx");
		MailList_UploadMailListButton.click();
		CommonMethods.waitforElement(MailListOkButton);
		Assert.assertEquals(MailListFalseField.getText().trim(), "False");
		ClearCartButton.click();
		CommonMethods.waitforElement(HomePage_Logo);
	}

	public void SpecialProject(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		CommonMethods.waitforElement(SpecialProjectDropdown);
		act.moveToElement(SpecialProjectDropdown).build().perform();
		CommonMethods.waitforElement(ADDSpecialProject);
		ADDSpecialProject.click();
		CommonMethods.waitforElement(JobTitleTextField);
		JobTitleTextField.sendKeys(Comments);
		CommonMethods.SelectFromDropDown(JobTypeDropDown, "Other");
		CreateSpecialProjectsButton.click();
		CommonMethods.waitforElement(DataHasbeenSavedMSG);
		ProgressFilesButton.click();
		ProgressFilesAddCommentButton.click();
		AddCommentTextField.click();
		CommonMethods.waitforElement(AddCommentTextField);
		AddCommentTextField.sendKeys(Comments);
		TypeDropDownArrow.click();
		CommonMethods.waitforElement(Type_Window);
		Type_Cancel.click();
		CommentSubmitButton.click();
		CommonMethods.waitforElement(DataHasbeenSavedMSG);
		act.moveToElement(Admin).build().perform();
		SpecialProjectDropdown.click();
		 CommonMethods.waitforElement(SpecialProject_ClosedCheckbox);
		 SpecialProject_ClosedCheckbox.click();
		 Thread.sleep(10000);
		 JopTitleFilterBox.sendKeys(Comments);
		 CommonMethods.waitforElement(JopTitleFilterIcon);
		 JopTitleFilterIcon.click();
		 CommonMethods.waitforElement(SpecialProIconWindow);
		 JopTitleFilterContains.click();
		 CommonMethods.waitforElement(JobTitleCommentfield);
		 Assert.assertEquals(JobTitleCommentfield.getText().trim(), Comments);
	}

	public void ManagePhrases(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManagePhrases.click();
		CommonMethods.waitforElement(DefaultPhrasesTextField);
		DefaultPhrasesTextField.sendKeys("Please select a valid date range");
		DefaultPhrasesIcon.click();
		CommonMethods.waitforElement(DefaultPhrasesIconWindow);
		DefaultPhrasesIconContains.click();
		CommonMethods.waitforElement(DefaultPhrasesPhrase);

	}

	public void ManageUsers(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManageUsers.click();
		FirstNameTextField.sendKeys("Ihsan");
		FirstNameIcon.click();
		// Thread.sleep(5000);
		// CommonMethods.waitforElement(FirstNameResult);
		// Assert.assertTrue(FirstNameResult.getText().trim().contains("Ihsan"));
	}

	public void Reports(String browser) throws InterruptedException {
		Reports.click();
		CommonMethods.waitforElement(ReportName_BillingMonthly);
		Assert.assertEquals(ReportName_BillingMonthly.getText().trim(), "Billing_ Monthly");
	}

	public void ReportScheduler(String browser) throws InterruptedException {
		Reports.click();
		CommonMethods.waitforElement(ReportName_BillingMonthly);
		ReportName_BillingMonthly.click();
		CommonMethods.waittodisappear(LoadingIconimg);
		ScheduleArrowdown.click();
		CommonMethods.waitforElement(ScheduleArrowdownWindow);
		ScheduleSelect.click();
		ReportTypeArrow.click();
		CommonMethods.waitforElement(ReportTypeWindow);
		ReportTypeselect_Excel.click();
		ScheduleComments.sendKeys(Comments);
		ScheduleSubscribeButton.click();
		CommonMethods.waitforElement(Unsubscribe);
		Unsubscribe.click();
		CommonMethods.waitforElement(UnsubscribeOKAlert);
		UnsubscribeOKAlert.click();
		CommonMethods.waitforElement(NoSubscriptionsAvailable);
	}

	public void TrainingGuides(String browser) throws InterruptedException {
		HomeButton.click();
		CommonMethods.waitforElement(HomePage_Logo);
		String winHandleBefore = Driver.getWindowHandle();
		TrainingGuide.click();
		for (String winHandle : Driver.getWindowHandles()) {
			Driver.switchTo().window(winHandle);
		}
		Driver.close();
		Driver.switchTo().window(winHandleBefore);
	}

	public void ManageRoles(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManageRoles.click();
		CommonMethods.waitforElement(ManageRoles_SecurityHeader);
	}

	public void ManageCostCenter(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManageCostCenter.click();
		CommonMethods.waitforElement(AddCostCenter);
		AddCostCenter.click();
		CommonMethods.waitforElement(CostCenterTextField);
		CostCenterTextField.sendKeys(Config.Constants.CostCenterName);
		CostCenterNameTextField.sendKeys(Comments);
		CreateCostCenterButton.click();
		 CommonMethods.waitforElement(CostCenterSearchField);
		 CostCenterSearchField.sendKeys(Config.Constants.CostCenterName);
		 CostCenterFilterIcon.click();
		 CommonMethods.waitforElement(CostCenterName);
		 Assert.assertEquals(CostCenterName.getText().trim(), Comments);
		 CostCenterDeleteButton.click();
		 CommonMethods.waitforElement(CostCenterDeletedMSG);
	}

	public void ManageCategories(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManageCategories.click();
		// CommonMethods.waitforElement(ManageCategory_CategoryHeader);
	}

	public void ManageDropdowns(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(Veritas).build().perform();
		ManageDropdowns.click();
		CommonMethods.waitforElement(ManageDropdown_TextFilterfield);
		ManageDropdown_TextFilterfield.sendKeys(Config.Constants.ManageDropDowntext);
		ManageDropdown_TextFilterIcon.click();
		CommonMethods.waitforElement(ManageDropdown_FilterIconWindow);
		CommonMethods.waitforElement(ManageDropdown_result);
	}

	public void ResetPhrasesSetting(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(Veritas).build().perform();
		ResetPhrasesAndSetting.click();
		CommonMethods.waitforElement(ResetPhareses_SettingAndVar);
	}

	public void KitQualityControl(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(Veritas).build().perform();
		KitQualityControl.click();
		CommonMethods.waitforElement(RecentKitChanes);
	}

	public void ContactsSearch(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		act.moveToElement(Veritas).build().perform();
		ContactSearch.click();
		// CommonMethods.waitforElement(ContactSearchHeader);
		ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		ContactSearchButton.click();
		CommonMethods.waitforElement(SelectContact);
		SelectContact.click();
	}

	public void ManageAnnouncements(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManageAnnouncements.click();
		CommonMethods.waitforElement(ManageAnnouncementsHeader);
	}

	public void ManageUserKits(String browser) throws InterruptedException {
		WebElement Admin = Admindrop;
		act.moveToElement(Admin).build().perform();
		ManageUserKits.click();
		CommonMethods.waitforElement(ManageUserKits_AddNewUserKit);
		ManageUserKits_AddNewUserKit.click();
		CommonMethods.waitforElement(ManageUserKits_ProductCodeTextField);
		ManageUserKits_ProductCodeTextField.sendKeys(Config.Constants.ProductCodeTest);
		ManageUserKits_TitleTextField.sendKeys(Comments);
		ManageUserKits_KitContainerArrowButton.click();
		CommonMethods.waitforElement(ManageUserKits_KitsContainerFirstoption);
		ManageUserKits_KitsContainerFirstoption.click();
		ManageUserKits_SaveUserKit.click();
		CommonMethods.waitforElement(ManageUserKits_AddToCartButton);
		act.moveToElement(Admin).build().perform();
		ManageUserKits.click();
		CommonMethods.waitforElement(ManageUserKits_AddNewUserKit);
		ManageUserKits_TitleSearchField.sendKeys(Comments);
		ManageUserKits_TitleIcon.click();
		CommonMethods.waitforElement(ManageUserKits_TitleIcon);
		ManageUserKits_TitleContains.click();
		CommonMethods.waittodisappear(LoadingIconimg);
		ManageUserKits_Delete.click();
		Util.CommonMethods.AcceptAlert();
		CommonMethods.waitforElement(ManageUserKits_NoUserKitsAvailable);
	}

	public void MyUserKitsWidgetS1(String browser) throws InterruptedException {
		HomeButton.click();
		CommonMethods.waitforElement(StartaBulkOrderButton);
		MyUserKits_EditViewMyUserKits.click();
		CommonMethods.waitforElement(ManageUserKits_AddNewUserKit);
	}

	public void MyUserKitsWidgetS2(String browser) throws InterruptedException {
		HomeButton.click();
		Thread.sleep(2000);
		CommonMethods.waitforElement(StartaBulkOrderButton);
		MyUserKits_SelectARecipient.click();
		// CommonMethods.waitforElement(ContactSearchHeader);
		ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
		ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
		ContactSearchButton.click();
		CommonMethods.waitforElement(SelectContact);
		SelectContact.click();
		CommonMethods.waitforElement(ClearCartButton);
		ClearCartButton.click();
		CommonMethods.waitforElement(MyUserKits_SelectARecipient);
	}
}
