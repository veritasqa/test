 package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Base.BasePage;
import Util.CommonMethods;

public class ManageInventory_Page extends BasePage {

Actions act = new Actions(Driver);
	
	public ManageInventory_Page(WebDriver Driver){
		
		super(Driver);
	} 

	@FindBy(how = How.XPATH, using = "//*[@id='Navigation_divLiList']/li[4]/a/span")
	public WebElement AdminPage;
	@FindBy(how = How.XPATH, using = "//*[text()='Manage Inventory']")
	public WebElement ManageInventory;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_txtFormNumber")
	public WebElement ProductCode ;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_ddlActive_Input']")
	public WebElement StatusAny ;
	@FindBy(how = How.XPATH, using = ".//*[@id='cphContent_btnSearch']")
	public WebElement Search ;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Results_ctl00_ctl04_lnkEdit")
	public WebElement PartEdit ;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_Results_ctl00_ctl06_lnkEdit")
	public WebElement PartEdit2 ;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_ddlViewability_Arrow']")
	public WebElement Viewability ;
	@FindBy(how = How.XPATH, using = "//*[text()='Save']")
	public WebElement Save ;
	@FindBy(how = How.XPATH, using = "//input[@value='OK']")
	public WebElement PopOK ;
	@FindBy(how = How.XPATH, using = "//*[text()='Orderable']")
	public WebElement Orderable ;
	@FindBy(how = How.XPATH, using = "//*[text()='Not Viewable']")
	public WebElement NotViewable ;
	@FindBy(how = How.XPATH, using = "//*[@id='txtUserName']")
	public WebElement Username ;
	@FindBy(how = How.XPATH, using = "//*[@id='txtPassword']")
	public WebElement Password ;
	@FindBy(how = How.XPATH, using = "//*[@id='btnSubmit']")
	public WebElement SearchButton ;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul")
	public WebElement Viewability_PopupWindow ;
	@FindBy(how = How.CSS, using = "#lbnLogout")
	public WebElement Logout ;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlActive_Arrow")
	public WebElement Status_Arrow;
	@FindBy(how = How.CSS, using = "#ctl00_cphContent_ddlActive_DropDown")
	public WebElement Status_PopupWindow;
	@FindBy(how = How.XPATH, using = ".//*[@id='ctl00_cphContent_ddlActive_DropDown']/div/ul/li[1]")
	public WebElement Status_Any;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_ddlViewability_Input']")
		public WebElement ViewabilityInput;
	@FindBy(how = How.XPATH, using = "//*[@id='ctl00_cphContent_txtFormNumber']")
	public WebElement Manageinventory_Formnumber;
	
	
	public void Login() throws InterruptedException{
		Username.sendKeys(Config.Constants.GWM_Admin_User);
		Password.sendKeys(Config.Constants.GWM_Admin_PWD);
		SearchButton.click();
		CommonMethods.waitforElement(AdminPage);
		}
	public void CALogin() throws InterruptedException{
		Username.sendKeys(Config.Constants.CA_Admin_User);
		Password.sendKeys(Config.Constants.CA_Admin_PWD);
		SearchButton.click();
		CommonMethods.waitforElement(AdminPage);
		}
	public void ViewableAndOrderable() throws InterruptedException{
		Viewability.click();
		CommonMethods.waitforElement(Viewability_PopupWindow);
		Orderable.click();
		Save.click();
		PopOK.click();
		Thread.sleep(1000);
		System.out.println(Manageinventory_Formnumber.getAttribute("value")+" "+ViewabilityInput.getAttribute("value"));
		}
	public void SearchParts() throws InterruptedException{
		WebElement Admin = AdminPage;
		act.moveToElement(Admin).build().perform();	
		ManageInventory.click();
		CommonMethods.waitforElement(ProductCode);
		}
	public void NotViewable() throws InterruptedException{
		Viewability.click();
		CommonMethods.waitforElement(Viewability_PopupWindow);
		NotViewable.click();
		Save.click();
		PopOK.click();
		Thread.sleep(1000);
		System.out.println(Manageinventory_Formnumber.getAttribute("value")+" "+ViewabilityInput.getAttribute("value"));
		}
	public void MakePartsViewable(String browser) throws InterruptedException {
			Login();
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber1);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber2);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber3);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber4);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber5);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber6);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			/*SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber7);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber8);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber9);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber10);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	*/
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber11);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber12);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Thread.sleep(3000);

			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber13);
		   CommonMethods.waitforElement(Status_Arrow);
	     	Status_Arrow.click();
	    	CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Thread.sleep(3000);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber14);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber15);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber16);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber17);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber18);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber19);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber20);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber21);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber22);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber23);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber24);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber25);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber26);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber27);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber28);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber29);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber30);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber31);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber32);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber33);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();		
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber34);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber35);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber36);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber37);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			ViewableAndOrderable();		
			
			Logout.click();
					
		}
	
	
	
	public void MakePartsNotViewable(String currentBrowser) throws InterruptedException {
			Login();
		
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber1);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber2);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber3);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber4);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber5);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber6);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			/*SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber7);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber8);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber9);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber10);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	*/
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber11);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber12);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber13);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Thread.sleep(2000);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber14);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber15);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber16);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber17);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber18);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber19);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber20);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber21);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber22);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber23);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber24);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber25);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber26);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Thread.sleep(2000);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber27);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber28);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber29);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber30);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber31);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber32);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber33);
			CommonMethods.waitforElement(Status_Arrow);
			Status_Arrow.click();
			CommonMethods.waitforElement(Status_PopupWindow);
			Status_Any.click();
			Thread.sleep(2000);

			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber34);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber35);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber36);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			SearchParts();
			ProductCode.sendKeys(Config.Constants.Partnumber37);
			Search.click();
			CommonMethods.waitforElement(PartEdit);
			PartEdit.click();
			NotViewable();	
			
			Logout.click();
}
	public void MakeCAPartsViewable(String browser) throws InterruptedException {
		CALogin();
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber38);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber39);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber40);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber41);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber42);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber43);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber44);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		ViewableAndOrderable();	
		
		Logout.click();
	}	
	public void MakeCAPartsNotViewable(String currentBrowser) throws InterruptedException {
		CALogin();
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber38);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber39);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber40);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber41);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();	
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber42);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber43);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();
		
		SearchParts();
		ProductCode.sendKeys(Config.Constants.Partnumber44);
		Search.click();
		CommonMethods.waitforElement(PartEdit);
		PartEdit.click();
		NotViewable();	
		
		Logout.click();
	}
	}
	