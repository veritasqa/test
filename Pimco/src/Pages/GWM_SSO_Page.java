 package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Base.BasePage;
import Util.CommonMethods;

public class GWM_SSO_Page extends BasePage {

	Actions act = new Actions(Driver);
		
		public GWM_SSO_Page(WebDriver Driver){
			
			super(Driver);
		} 
		
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock95898111-8245-418e-ab77-16ba8c061118_C_ctl00_lblFillOutContactInfo>a")
		public WebElement NewFormsAndMaterials;
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock95898111-8245-418e-ab77-16ba8c061118_C_ctl00_txtQuantity1")
		public WebElement NewFormsAndMaterials_quantity;
		@FindBy(how = How.CSS, using = "#ctl00_cphContent_RadDock95898111-8245-418e-ab77-16ba8c061118_C_ctl00_btnAddToCart")
		public WebElement NewFormsAndMaterials_CheckoutButton;
		
		public void GWMSSOLogin(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
			Driver.get(Config.Constants.URL_SSO_PRD);
			CommonMethods.waitforElement(CASO.UserID);
			CASO.UserID.clear();
			CASO.UserID.sendKeys(Config.Constants.SSO_GWM_UserID);
			CASO.FirstName.clear();
			//Thread.sleep(5000);
			CASO.FirstName.sendKeys(Config.Constants.SSO_GWM_FirstName);
			CASO.LastName.clear();
			//Thread.sleep(5000);
			CASO.LastName.sendKeys(Config.Constants.SSO_GWM_LastName);
			CASO.CostCenter.clear();
			//Thread.sleep(5000);
			CASO.CostCenter.sendKeys(Config.Constants.SSO_CA_CostCenter);
			CASO.Email.clear();
			//Thread.sleep(5000);
			CASO.Email.sendKeys(Config.Constants.SSO_Email);
			CASO.Phone.clear();
		    //Thread.sleep(5000);
			CASO.Phone.sendKeys(Config.Constants.SSO_Phone);
			CASO.UserGroup.clear();
			//Thread.sleep(5000);

			CASO.UserGroup.sendKeys(Config.Constants.SSO_GWM_BankUser);
			CASO.IncludeContactInfo.click();
			Thread.sleep(5000);

			CASO.AuthenticateButton.click();
			Thread.sleep(7000);

			CASO.RedirectButton.click();
			CommonMethods.waitforElement(CAAP.StartABulkOrderButton);
		}
		public void FulfillmentOrder(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber31);
			CAAP.FullfulmentSearchButton.click();
			CAAP.ContactSearch();
			CAAP.AddToCart.click();
			CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
			CAAP.ShoppingCart_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
			GWMB.Address1.sendKeys(Config.Constants.Address1);
			GWMB.City.sendKeys(Config.Constants.City);
			Util.CommonMethods.SelectFromDropDown(GWMB.State, Config.Constants.State);
			GWMB.Zip.sendKeys(Config.Constants.Zip);
			Util.CommonMethods.SelectFromDropDown(GWMB.Country, Config.Constants.Country);
			CAAP.CheckoutPageNextButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutButton);		
			CAAP.ShippingMethod_LCSMButton.click();
			CAAP.CheckoutButton.click();	
			String ON = CommonMethods.GetText(CAAP.Orderinfo_PartNumber);
			CAAP.OkButton.click();
			CommonMethods.waitforElement(CAAP.HomePageImage);
			if(Driver.getCurrentUrl().equals(Config.Constants.URL_PRD_Cancel)){
				CAAP.InvenLogin();
				CAAP.Inven_OrderNoTextField.sendKeys(ON);
				CAAP.InvenSearchandCancel();
		}
		}
		public void BulkOrder(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.StartABulkOrderButton.click();
			CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
			CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir")+"/MailList/PIMCO_Maillist.xlsx");
			CAAP.MailList_UploadMailListButton.click();
			CommonMethods.waitforElement(CAAP.MailListOkButton);
			CAAP.MailListOkButton.click();
			CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
			CAAP.SearchByMaterialTextField.sendKeys(Config.Constants.Partnumber31);
			CAAP.SearchForMaterialsButton.click();
			CommonMethods.waitforElement(CAAP.AddToCart);
			CAAP.AddToCart.click();
			CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
			CAAP.ShoppingCart_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.BulkOrderClearCartButton);		
			CAAP.BulkOrderClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);		
		}
		public void ClearShoppingCartFulfillment(String browser) throws InterruptedException{	
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber31);
			CAAP.FullfulmentSearchButton.click();
			CAAP.ContactSearch();
			CAAP.AddToCart.click();
			CommonMethods.waitforElement(CAAP.ShoppingCart_CheckoutButton);
			CAAP.ShoppingCart_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutPageHeader);
			GWMB.Address1.sendKeys(Config.Constants.Address1);
			GWMB.City.sendKeys(Config.Constants.City);
			Util.CommonMethods.SelectFromDropDown(GWMB.State, Config.Constants.State);
			GWMB.Zip.sendKeys(Config.Constants.Zip);
			Util.CommonMethods.SelectFromDropDown(GWMB.Country, Config.Constants.Country);
			CAAP.CheckoutPageNextButton.click();
			CommonMethods.waitforElement(CAAP.CheckoutButton);		
			CAAP.ShippingMethod_LCSMButton.click();
			CAAP.ShoppingCartClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);		
		}
		public void PagingFunctionality(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.ProductCodeTest);
			CAAP.FullfulmentSearchButton.click();
			CommonMethods.waitforElement(CAAP.ContactSearchHeader);
			CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstNamePartial);
			CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastNamePartial);
			CAAP.ContactSearchButton.click();
			CommonMethods.waitforElement(CAAP.SelectContact);
			CAAP.SelectContact.click();
			CAAP.HomePage_ClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
		    }
		public void VerifyMailList(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.StartaBulkOrderButton.click();
			CommonMethods.waitforElement(CAAP.MailList_UploadMailListButton);
			CAAP.MailList_ListPath.sendKeys(System.getProperty("user.dir")+"/MailList/PIMCO_Maillist.xlsx");
			CAAP.MailList_UploadMailListButton.click();
			CommonMethods.waitforElement(CAAP.MailListOkButton);
			Assert.assertEquals(CAAP.MailListFalseField.getText().trim(), "False");
			CAAP.ClearCartButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
		}
		public void TrainingGuides(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);	
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);		
			String winHandleBefore = Driver.getWindowHandle();	
			CAAP.TrainingGuide.click();		
			for(String winHandle : Driver.getWindowHandles()){
			Driver.switchTo().window(winHandle);
			}		
			Driver.close();		
			Driver.switchTo().window(winHandleBefore);		
		}
		public void PartRestrictionByFirm(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);
			CAAP.HomeButton.click();
			CommonMethods.waitforElement(CAAP.HomePage_Logo);
			CAAP.FullfulmentSearchTextField.sendKeys(Config.Constants.Partnumber30);
			CAAP.FullfulmentSearchButton.click();
			CommonMethods.waitforElement(CAAP.ContactSearchHeader);
			CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
			CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
			GWMB.ContactSearchFirm_TextField.sendKeys(Config.Constants.ContactFirm);
			CAAP.ContactSearchButton.click();
			CommonMethods.waitforElement(CAAP.SelectContact);
			CAAP.SelectContact.click();
			CommonMethods.waitforElement(CAAP.SearchByMaterialTextField);
			Assert.assertEquals(GWMB.RestrictedByFirm.getText().trim(), Config.Constants.ResrtictedFirm);
			CAAP.ClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);
		}
		public void NewFormsandMaterials(String browser) throws InterruptedException{
			CA_Admin_Page CAAP = PageFactory.initElements(Driver, CA_Admin_Page.class);
			NewFormsAndMaterials.click();
			CommonMethods.waitforElement(CAAP.ContactSearchHeader);
			CAAP.ContactSearchFirstName_textField.sendKeys(Config.Constants.ContactFirstName);
			CAAP.ContactSearchLastName_textField.sendKeys(Config.Constants.ContactLastName);
			CAAP.ContactSearchButton.click();
			CommonMethods.waitforElement(CAAP.SelectContact);
			CAAP.SelectContact.click();
			CommonMethods.waitforElement(NewFormsAndMaterials_quantity);
			NewFormsAndMaterials_quantity.click();
			NewFormsAndMaterials_quantity.sendKeys("1");
			NewFormsAndMaterials_CheckoutButton.click();
			CommonMethods.waitforElement(CAAP.BulkOrderClearCartButton);		
			CAAP.BulkOrderClearCartButton.click();
			CommonMethods.waitforElement(CAAP.StartaBulkOrderButton);		
		}
		}

