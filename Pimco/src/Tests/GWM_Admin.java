package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import Base.BaseTest;
import Pages.GWM_Admin_Page;

public class GWM_Admin extends BaseTest {

	@Test(priority = 1)
	public void GWM_Login() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.GWMLogin(currentBrowser);

	}

	@Test(priority = 2, enabled = false)
	public void MarketingCampaignOrder() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.MarketingCampaignOrder(currentBrowser);

	}

	@Test(priority = 100, enabled = true)
	public void FulfillmentOrder() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.FulfillmentOrder(currentBrowser);
//		Driver.get(Constants.URL_STG);
//		Driver.manage().deleteAllCookies();
//		CAAP.GWMLogin(currentBrowser);

	}

	@Test(priority = 4, enabled = false)
	public void BulkOrder() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.BulkOrder(currentBrowser);

	}

	@Test(priority = 5, enabled = false)
	public void ClearShoppingCartFulfillment() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ClearShoppingCartFulfillment(currentBrowser);

	}

	@Test(priority = 6, enabled = false)
	public void PagingFunctionality() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.PagingFunctionality(currentBrowser);

	}

	@Test(priority = 7, enabled = false)
	public void VerifyMailList() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.VerifyMailList(currentBrowser);

	}

	@Test(priority = 8, enabled = false)
	public void TrainingGuides() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.TrainingGuides(currentBrowser);

	}

	@Test(priority = 9, enabled = false)
	public void PartRestrictionByFirm() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.PartRestrictionByFirm(currentBrowser);

	}

	@Test(priority = 10, enabled = false)
	public void Reports() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.Reports(currentBrowser);

	}

	@Test(priority = 11, enabled = false)
	public void ReportScheduler() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ReportScheduler(currentBrowser);

	}

	@Test(priority = 12, enabled = false)
	public void ResetPhrasesSetting() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ResetPhrasesSetting(currentBrowser);

	}

	@Test(priority = 13, enabled = false)
	public void ContactsSearch() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ContactsSearch(currentBrowser);

	}

	@Test(priority = 14, enabled = false)
	public void ManageAnnouncements() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManageAnnouncements(currentBrowser);

	}

	@Test(priority = 15, enabled = false)
	public void KitQualityControl() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.KitQualityControl(currentBrowser);

	}

	@Test(priority = 16, enabled = false)
	public void ManagePhrases() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManagePhrases(currentBrowser);

	}

	@Test(priority = 17, enabled = false)
	public void ManageUsers() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManageUsers(currentBrowser);

	}

	@Test(priority = 18, enabled = false)
	public void ManageDropdowns() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManageDropdowns(currentBrowser);

	}

	@Test(priority = 19, enabled = false)
	public void ManageCategories() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManageCategories(currentBrowser);

	}

	@Test(priority = 20, enabled = false)
	public void ManageRoles() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManageRoles(currentBrowser);

	}

	@Test(priority = 21, enabled = false)
	public void ManageCostCenter() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.ManageCostCenter(currentBrowser);

	}

	@Test(priority = 22, enabled = false)
	public void SpecialProject() throws InterruptedException {

		GWM_Admin_Page CAAP = PageFactory.initElements(Driver, GWM_Admin_Page.class);

		CAAP.SpecialProject(currentBrowser);

	}
}
