package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import Base.BaseTest;
import Pages.ManageInventory_Page;

public class ManageParts extends BaseTest {


 
@Test (priority = 1, enabled = true)
public void MakePartsViewable() throws InterruptedException{
	
	ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
	
	MI.MakePartsViewable(currentBrowser);

}		

@Test (priority = 3, enabled = false)
public void MakeCAPartsViewable() throws InterruptedException{
			

	ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
				
	MI.MakeCAPartsViewable(currentBrowser);

}


@Test (priority = 2, enabled = false)
public void MakePartsNotViewable() throws InterruptedException{
			

	ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
			
			
	MI.MakePartsNotViewable(currentBrowser);
			

}

@Test (priority = 4, enabled = false)
public void MakeCAPartsNotViewable() throws InterruptedException{
			

	ManageInventory_Page MI = PageFactory.initElements(Driver, ManageInventory_Page.class);
			
			
	MI.MakeCAPartsNotViewable(currentBrowser);

}	
}
