 package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import Base.BaseTest;
import Pages.GWM_SSO_Page;

public class GWM_SSO extends BaseTest  {

	
	@Test (priority = 1, enabled = true)
	public void GWMSSOLogin() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.GWMSSOLogin(currentBrowser);
			
		logger.log(LogStatus.INFO,"GWMSSOLogin");
		logger.log(LogStatus.PASS, "GWMSSOLogin");	
	}	
	@Test (priority = 10, enabled = true)
	public void FulfillmentOrder() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.FulfillmentOrder(currentBrowser);
		GWSO.GWMSSOLogin(currentBrowser);	
		
		logger.log(LogStatus.INFO,"FulfillmentOrder");
		logger.log(LogStatus.PASS, "FulfillmentOrder");	
	}	
	@Test (priority = 3, enabled = false)
	public void BulkOrder() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.BulkOrder(currentBrowser);
			
		logger.log(LogStatus.INFO,"BulkOrder");
		logger.log(LogStatus.PASS, "BulkOrder");	
	}	
	@Test (priority = 4, enabled = false)
	public void ClearShoppingCartFulfillment() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.ClearShoppingCartFulfillment(currentBrowser);
			
		logger.log(LogStatus.INFO,"ClearShoppingCartFulfillment");
		logger.log(LogStatus.PASS, "ClearShoppingCartFulfillment");	
	}	
	@Test (priority = 5, enabled = false)
	public void PagingFunctionality() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.PagingFunctionality(currentBrowser);
			
		logger.log(LogStatus.INFO,"PagingFunctionality");
		logger.log(LogStatus.PASS, "PagingFunctionality");	
	}	
	@Test (priority = 6, enabled = false)
	public void VerifyMailList() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.VerifyMailList(currentBrowser);
			
		logger.log(LogStatus.INFO,"VerifyMailList");
		logger.log(LogStatus.PASS, "VerifyMailList");	
	}	
	@Test (priority = 7, enabled = false)
	public void TrainingGuides() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.TrainingGuides(currentBrowser);
			
		logger.log(LogStatus.INFO,"TrainingGuides");
		logger.log(LogStatus.PASS, "TrainingGuides");	
	}	
	@Test (priority = 8, enabled = true)
	public void PartRestrictionByFirm() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.PartRestrictionByFirm(currentBrowser);
			
		logger.log(LogStatus.INFO,"PartRestrictionByFirm");
		logger.log(LogStatus.PASS, "PartRestrictionByFirm");	
	}	
	@Test (priority = 9, enabled = true)
	public void NewFormsandMaterials() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		GWM_SSO_Page GWSO = PageFactory.initElements(Driver, GWM_SSO_Page.class);
		
		GWSO.NewFormsandMaterials(currentBrowser);
			
		logger.log(LogStatus.INFO,"NewFormsandMaterials");
		logger.log(LogStatus.PASS, "NewFormsandMaterials");	
	}	
}
