package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import Base.BaseTest;
import Pages.GWM_Basic_Page;

public class GWM_Basic extends BaseTest {

	@Test(priority = 1)
	public void GWMBasicLogin() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.GWMBasicLogin(currentBrowser);

	}

	@Test(priority = 100, enabled = true)
	public void FulfillmentOrder() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.FulfillmentOrder(currentBrowser);
		//Driver.get(Constants.URL_PRD);
		//Driver.manage().deleteAllCookies();
		//GWMB.GWMBasicLogin(currentBrowser);

	}

	@Test(priority = 3, enabled = false)
	public void BulkOrder() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.BulkOrder(currentBrowser);

	}

	@Test(priority = 4, enabled = false)
	public void ClearShoppingCartFulfillment() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.ClearShoppingCartFulfillment(currentBrowser);

	}

	@Test(priority = 5, enabled = false)
	public void PagingFunctionality() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.PagingFunctionality(currentBrowser);

	}

	@Test(priority = 6, enabled = false)
	public void VerifyMailList() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.VerifyMailList(currentBrowser);

	}

	@Test(priority = 7, enabled = false)
	public void TrainingGuides() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.TrainingGuides(currentBrowser);

	}

	@Test(priority = 8, enabled = false)
	public void MostPopularItems() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.MostPopularItems(currentBrowser);

	}

	@Test(priority = 9, enabled = false)
	public void PartRestrictionByFirm() throws InterruptedException {

		GWM_Basic_Page GWMB = PageFactory.initElements(Driver, GWM_Basic_Page.class);

		GWMB.PartRestrictionByFirm(currentBrowser);

	}

}
