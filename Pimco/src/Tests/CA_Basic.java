 package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import Base.BaseTest;
import Pages.CA_Basic_Page;

public class CA_Basic extends BaseTest {

	@Test (priority = 1)
	public void CABasicLogin() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.CABasicLogin(currentBrowser);
			
	
	}	
	@Test (priority = 10, enabled = true)
	public void FulfillmentOrder() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.FulfillmentOrder(currentBrowser);

	}	
	@Test (priority = 3, enabled = false)
	public void BulkOrder() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.BulkOrder(currentBrowser);
			
	
	}	
	@Test (priority = 4, enabled = false)
	public void ClearShoppingCartFulfillment() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.ClearShoppingCartFulfillment(currentBrowser);

	}	
	@Test (priority = 5, enabled = false)
	public void PagingFunctionality() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.PagingFunctionality(currentBrowser);

	}	
	@Test (priority = 6, enabled = false)
	public void VerifyMailList() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.VerifyMailList(currentBrowser);
			

	}	
	@Test (priority = 7, enabled = false)
	public void TrainingGuides() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.TrainingGuides(currentBrowser);
	
	}	
	@Test (priority = 8, enabled = false)
	public void MostPopularItems() throws InterruptedException{
		
		CA_Basic_Page CABP = PageFactory.initElements(Driver, CA_Basic_Page.class);
		
		CABP.MostPopularItems(currentBrowser);
			

	}	
}
