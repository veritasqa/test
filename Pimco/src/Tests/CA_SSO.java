 package Tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import Base.BaseTest;
import Pages.CA_SSO_Page;

public class CA_SSO  extends BaseTest {

	
	@Test (priority = 1, enabled = true)
	public void CASSOLogin() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.CASSOLogin(currentBrowser);
			
		logger.log(LogStatus.INFO,"CASSOLogin");
		logger.log(LogStatus.PASS, "CASSOLogin");	
	}	
	@Test (priority = 20, enabled = true)
	public void FulfillmentOrder() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.FulfillmentOrder(currentBrowser);
			
		logger.log(LogStatus.INFO,"FulfillmentOrder");
		logger.log(LogStatus.PASS, "FulfillmentOrder");	
	}	
	@Test (priority = 3, enabled = true)
	public void MyUserKitsWidgetS1() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.MyUserKitsWidgetS1(currentBrowser);
			
		logger.log(LogStatus.INFO,"MyUserKitsWidgetS1");
		logger.log(LogStatus.PASS, "MyUserKitsWidgetS1");	
	}	
	@Test (priority = 4, enabled = true)
	public void MyUserKitsWidgetS2() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.MyUserKitsWidgetS2(currentBrowser);
			
		logger.log(LogStatus.INFO,"MyUserKitsWidgetS2");
		logger.log(LogStatus.PASS, "MyUserKitsWidgetS2");	
	}	
	@Test (priority = 5, enabled = true)
	public void BulkOrder() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.BulkOrder(currentBrowser);
			
		logger.log(LogStatus.INFO,"BulkOrder");
		logger.log(LogStatus.PASS, "BulkOrder");	
	}	
	@Test (priority = 6, enabled = true)
	public void ClearShoppingCartFulfillment() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.ClearShoppingCartFulfillment(currentBrowser);
			
		logger.log(LogStatus.INFO,"ClearShoppingCartFulfillment");
		logger.log(LogStatus.PASS, "ClearShoppingCartFulfillment");	
	}	
	@Test (priority = 7, enabled = true)
	public void PagingFunctionality() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.PagingFunctionality(currentBrowser);
			
		logger.log(LogStatus.INFO,"PagingFunctionality");
		logger.log(LogStatus.PASS, "PagingFunctionality");	
	}	
	@Test (priority = 8, enabled = true)
	public void VerifyMailList() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.VerifyMailList(currentBrowser);
			
		logger.log(LogStatus.INFO,"VerifyMailList");
		logger.log(LogStatus.PASS, "VerifyMailList");	
	}
	@Test (priority = 9, enabled = true)
	public void TrainingGuides() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.TrainingGuides(currentBrowser);
			
		logger.log(LogStatus.INFO,"TrainingGuides");
		logger.log(LogStatus.PASS, "TrainingGuides");	
	}
	@Test (priority = 10, enabled = true)
	public void ExpressCheckoutWidget() throws InterruptedException{
		
		logger = report.startTest(Thread.currentThread().getStackTrace()[1].getMethodName());
		CA_SSO_Page CASO = PageFactory.initElements(Driver, CA_SSO_Page.class);
		
		CASO.ExpressCheckoutWidget(currentBrowser);
			
		logger.log(LogStatus.INFO,"ExpressCheckoutWidget");
		logger.log(LogStatus.PASS, "ExpressCheckoutWidget");	
	}
}
