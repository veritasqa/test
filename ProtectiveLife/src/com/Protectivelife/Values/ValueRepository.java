package com.Protectivelife.Values;

import java.util.ArrayList;
import java.util.List;

public class ValueRepository {

	public static List<String> ordernum = new ArrayList<String>();
	public static List<String> splprojects = new ArrayList<String>();
	public static List<String> Createdpartlist = new ArrayList<String>();

	public static String Browser = "chrome";
	// File path for Chrome Driver
	public static String Browserpath = System.getProperty("user.dir")
			+ "\\src\\com\\Protectivelife\\Driver\\chromedriver.exe";
	// File Path GroupA and GroupA1 ExcelList Files
	// public static String Excel_File_Path =
	// "C:\\Users\\rr249046\\Desktop\\A.xlsx";

	public static final String URL = "https://www.veritas-solutions.net/ProtectiveLife/login.aspx";
	public static final String Production_URL = "https://www.veritas-solutions.net/ProtectiveLife/login.aspx";

	// public static final String Default_URL =
	// "https://www.veritas-solutions.net/ProtectiveLife/default.aspx";

	public static final String Default_URL = "https://www.veritas-solutions.net/ProtectiveLife/default.aspx";
	public static final String Inventory_URL = "https://www.veritas-solutions.net/Inventory/login.aspx";
	// public static final String Inventory_URL =
	// "https://veritas-solutions.net/inventory/login.aspx";
	// public static final String SSO_URL =
	// "https://www.veritas-solutions.net/ProtectiveLife/API/SSO/XMLSSOTEST.aspx";
	public static final String SSO_URL = "https://www.veritas-solutions.net/ProtectiveLife/API/SSO/XMLSSOTEST.aspx";

	public static final String UserName = "qaauto";
	public static final String Password = "qaauto";
	public static final String EmailId = "ver.qaauto@rrd.com";

	public static final String InUserName = "qaautoinadmin";
	public static final String InPassword = "qaautoinadmin";
	public static final String InEmailId = "ver.qaauto@rrd.com";

	public static final String SalesdeskUserName = "qaautosalesdesk";
	public static final String SalesdeskPassword = "qaautosalesdesk";
	public static final String SalesdeskEmailId = "ver.qaauto@rrd.com";

	public static final String MarketingkUserName = "qaautomarketing";
	public static final String MarketingPassword = "qaautomarketing";
	public static final String MarketingEmailId = "ver.qaauto@rrd.com";

	public static final String Inventory_UserName = "arajasekaran";
	public static final String Inventory_Password = "arajasekaran780";

	public static final String Firstname = "qa";
	public static final String Lastname = "auto";
	public static final String Form_lastname = "auto";

	// contact without firm
	public static final String Firstname_withoutfirm = "qaauto";
	public static final String Lastname_withoutfirm = "automation";

	public static String ContactName = Firstname + " " + Lastname;

	public static final String STATE_RESTRICTION_Name = "Qastate";
	public static final String STATE_RESTRICTION_last_Name = "Positive";

	public static final String STATE_RESTRICTION_Name_WithIL = "Priyanka";
	public static final String STATE_RESTRICTION_last_Name_WithIL = "fnu";

	public static final String Firm_Restriction_Name = "QA Automation";
	public static final String Firm_Restriction_last_Name = "Auto";

	public static final String STATE_FIRM_RESTRICTION_Name = "QASTATEFIRMRESTRICTION ";
	public static final String STATE_FIRM_RESTRICTION_last_Name = "Firm Test";

	public static final String FIRMRESTRICTION_FirstName1 = "Qa";
	public static final String FIRMRESTRICTION_lastName1 = "Firm";

	public static final String FIRMRESTRICTION_FirstName2 = "Qafirm";
	public static final String FIRMRESTRICTION_lastName2 = "rest";

	public static final String STATEFIRMRESTRICTION_FirstName1 = "";
	public static final String STATEFIRMRESTRICTION_lastName1 = "";

	public static final String STATEFIRMRESTRICTION_FirstName2 = "";
	public static final String STATEFIRMRESTRICTION_lastName2 = "";

	public static final String Address1 = "913 Commerce ct";
	public static final String City = "Buffalo Grove";
	public static final String State = "IL";
	public static final String Zipcode = "60089";
	public static final String Shipper = "UPS Ground";

	public String Parent_Window_ID = "";
	public String Dropdown_Clear = "- Please Select -";

	public static String OutPut_File_Path = "C:\\Users\\rr249046\\Desktop\\Veritas\\Protective\\PartsViewability"; // Output
																													// File
																													// for
																													// Group
																													// A
	public String newview = "";
	public String partvalue = "";
	public static final String Attribute_Value = "Value";

	public static final String Part1 = "QA_MULTIFUNCTION";
	public static final String Part2 = "QA_FirmRestriction";
	public static final String Part3 = "QA_STATERESTRICTION";
	public static final String Part4 = "QA_StateFirmRestriction";
	public static final String Part5 = "QA_keyword";
	public static final String Part6 = "QA_Copy_NewItem_";
	public static final String Part7 = "QA_TESTNEWITEM_X";
	public static final String Part8 = "QA_TESTNEWITEM_";
	public static final String Part9 = "QA_TESTNEWITEM_1";
	public static final String Part10 = "QA_InventorySearch";
	public static final String Part11 = "QA_Copy_SearchResults_X";
	public static final String Part12 = "QA_NewTestPart";
	public static final String Part13 = "QA_PartEdit";
	public static final String Part14 = "QA_TESTNEWITEM_1";
	public static final String Part15 = "QA ChangeHistory";
	public static final String Part16 = "QA_Bookcover";
	public static final String Part17 = "CAC.1059.09.12";
	public static final String Part18 = "QA_Stockpart";
	public static final String Part19 = "QA_Formsbook1";
	public static final String Part20 = "QA_Kitonfly_2";
	public static final String Part21 = "QA_Flyer";
	public static final String Part22 = "QA_Adminonly";
	public static final String Part24 = "QA_Viewability";
	public static final String Part25 = "QA_Bookcover";
	// public static final String Part26 = "QA_Flyer";
	public static final String Part26 = "QA_Folder";
	public static final String Part27 = "QA_Folder2";
	public static final String Part28 = "QA_Folder3";
	// public static final String Part29 = "QA_Kit";
	// public static final String Part30 = "QA_Marketingonly";

	public static final String Part29 = "QA_KITONFLY_4";
	public static final String Part30 = "QA_REPLACE_1";

	public static final String Part31 = "QA_Replace_2";
	public static final String Part32 = "QA_TESTNEWITEM_4";
	public static final String Part33 = "QA_Copy_NewItem_1";

	public static final String Part34 = "QA_Maxorder";

	public static final String Part35 = "QA_Marketingonly";

	public List<String> Orders = new ArrayList<String>();

	public static String CreatePart = "";

	public static final String Qty = "1";

	public static String OrderNumber = "5701661";
	public static String TicketNumber = "";

	public static String SplprjTicketNumber = "";

	public static final String CreateTemplate_Message = "QA Test order template has been successfully created.";

	public static final String Invalid_Form_Message = "Invalid Form Number";

	public static final String outstock_Msg1 = "Out of Stock";
	public static final String outstock_Msg2 = "On Hand = 12";

	// Spl Projects
	public static final String SPPErrorTilte = "You must enter a value in the following fields:";
	public static final String SPPErrorJobTilte = "Job Title Is Required";
	public static final String SPPErrorJobType = "Job Type Is Required";
	public static final String SPPErrorCC = "Cost Center Is Required";

	// public static final String Crm_Agent_Login =
	// "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>qaautoagent1</FirstName><LastName>automation</LastName><UserEmail>Ver.QAAUTO@rrd.com</UserEmail><UserName>qaautoagent1</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Agent</Audience><CostCenter>9999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>qaautoagent1</ShipFirstName><ShipLastName>automation</ShipLastName><ShipCompany></ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913
	// Commerce
	// Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo
	// Grove</ShipCity><ShipStateProvince>iL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United
	// States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>Ver.QAAUTO@rrd.com</ShipEmail></Recipients></NewDataSet>";
	public static final String Crm_Agent_Login = "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>qaautoagent1</FirstName><LastName>automation</LastName><UserEmail>Ver.QAAUTO@rrd.com</UserEmail><UserName>Pfnuinadmin</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Agent</Audience><CostCenter>9999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>qaautoagent1</ShipFirstName><ShipLastName>automation</ShipLastName><ShipCompany></ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913 Commerce Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo Grove</ShipCity><ShipStateProvince>iL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>Ver.QAAUTO@rrd.com</ShipEmail></Recipients></NewDataSet>";

	public static final String Crm_Login = "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>qaautoagent1</FirstName><LastName>automation</LastName><UserEmail>Ver.QAAUTO@rrd.com</UserEmail><UserName>Pfnuinadmin</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Agent</Audience><CostCenter>9999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>qaautoagent1</ShipFirstName><ShipLastName>automation</ShipLastName><ShipCompany></ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913 Commerce Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo Grove</ShipCity><ShipStateProvince>iL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>Ver.QAAUTO@rrd.com</ShipEmail></Recipients></NewDataSet>";
	public static final String Crm_SalesDesk_Login = "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>Supplies For</FirstName><LastName>Life Agent Services Guest</LastName><UserEmail>priyanka@Veritas-solutions.com</UserEmail><UserName>Priyanka Allstate</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Agent</Audience><CostCenter>9999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>qaautoagent1</ShipFirstName><ShipLastName>automation</ShipLastName><ShipCompany></ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913 Commerce Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo Grove</ShipCity><ShipStateProvince>iL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>Ver.QAAUTO@rrd.com</ShipEmail></Recipients></NewDataSet>";
	public static final String Crm_firm = "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>priyanka</FirstName><LastName>Salesdesk</LastName><UserEmail>priyanka@Veritas-solutions.com</UserEmail><UserName>pfnusalesdesk</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Agent</Audience><CostCenter>9999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>qaautoagent1</ShipFirstName><ShipLastName>automation</ShipLastName><ShipCompany>Advisor Group</ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913 Commerce Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo Grove</ShipCity><ShipStateProvince>iL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>Ver.QAAUTO@rrd.com</ShipEmail></Recipients></NewDataSet>";
	public static final String Crm_firm2 = "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>priyanka</FirstName><LastName>Salesdesk</LastName><UserEmail>ver.qa@rrd.com</UserEmail><UserName>pfnusalesdesk</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Agent</Audience><CostCenter>9999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>qaautoagent1</ShipFirstName><ShipLastName>automation</ShipLastName><ShipCompany>AIG RETIREMENT ADVISORS, INC</ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913 Commerce Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo Grove</ShipCity><ShipStateProvince>iL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>Ver.QAAUTO@rrd.com</ShipEmail></Recipients></NewDataSet>";
	public static final String Crm__Allstateagent_Login = "<NewDataSet><Customer><CustomerID>PLC</CustomerID><FirstName>Priyanka FNU</FirstName><LastName>Allstate</LastName><UserEmail>qaveritas@gmail.com</UserEmail><UserName>Priyanka Fnu Allsate</UserName><AuthLevel>15</AuthLevel><Theme>PLC</Theme><Channel>PLB</Channel><CustomerType>FLDMGR</CustomerType><Audience>Allstate</Audience><CostCenter>99999</CostCenter></Customer><Recipients><PLCClientID></PLCClientID><ShipFirstName>QA</ShipFirstName><ShipLastName>Automation</ShipLastName><ShipCompany></ShipCompany><ShipCompanyDescription></ShipCompanyDescription><ShipAddress>913 Commerce Ct</ShipAddress><ShipAddress2></ShipAddress2><ShipAddress3></ShipAddress3><ShipCity>Buffalo Grove</ShipCity><ShipStateProvince>IL</ShipStateProvince><ShipZipPostal>60089</ShipZipPostal><ShipCountry>United States</ShipCountry><ShipPhone></ShipPhone><ShipEmail>yashwanthraj.annamalai@rrd.com</ShipEmail></Recipients></NewDataSet>";

	// Colours
	public static final String Orangecolor = "#f7b820";
	public static final String Bluecolor = "#00a9e0";

}
