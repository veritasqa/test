package com.Protectivelife.OR;

public class CommonXpaths extends Confirmation {
	
	// Filter Options- UserKits

		public static final String NoFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[1]/a/span";
		
		public static final String Contains_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[2]/a/span";
		public static final String DoesntContains_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[3]/a/span";
		public static final String StartsWith_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[4]/a/span";
		public static final String EndsWith_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[5]/a/span";
		public static final String EqualTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[6]/a/span";
		public static final String NotEqualTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[7]/a/span";
		public static final String GreaterThan_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[8]/a/span";
		public static final String LessThan_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[9]/a/span";
		public static final String GreatthanorEqualto_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[10]/a/span";
		public static final String LessthanOrEqual_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[11]/a/span";
		public static final String Between_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[12]/a/span";
		public static final String NotBetween_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[13]/a/span";
		public static final String IsEmpty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[14]/a/span";
		public static final String NotIsEmpty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[15]/a/span";
		public static final String IsNull_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[16]/a/span";
		public static final String NotisNull_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[17]/a/span";


// Ticket Page Filter options
		
		public static final String Support_Contains_Path  = 	"//*[@id='ctl00_cphContent_VeritasSupportTicketsRadGrid_rfltMenu_detached']/ul/li[2]/a/span";
}
