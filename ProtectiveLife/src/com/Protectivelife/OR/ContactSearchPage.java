package com.Protectivelife.OR;

public class ContactSearchPage extends CustomizableOptions {

	// Contact Details

	public static final String Firstname = "Priyanka";
	public static final String Lastname = "fnu";
	public static final String Firstname_Firmtest = "QA Automation";
	public static final String Lastname_Firmtest = "Firm Test";
	public static final String Form_lastname = "auto";
	public static String ContactName = Firstname + " " + Lastname;
	public static String ContactName_Firmtest = Firstname_Firmtest + " " + Lastname_Firmtest;

	// Contact Search
	public static final String ContactSearch_Title_Path = ".//h1[contains(text(),'Contact Search')]";

	public static final String FirstName_Path = ".//*[@id='ctl00_cphContent_txtFirstName']";
	public static final String LastName_Path = ".//*[@id='ctl00_cphContent_txtLastName']";
	public static final String FirmName_Path = ".//*[@id='ctl00_cphContent_txtFirmSearch']";
	public static final String Search_btn_Path = ".//a[text()='Search']";
	public static final String Clear_btn_Path = ".//a[text()='Clear']";
	public static final String Contact_Click_Path = ".//td[text()='" + ContactName + "']";

	public static final String Edit_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl04_EditButton']";
	// public static final String Delete_btn_Path =
	// ".//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[18]/a";
	public static final String Add_Contact_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String Add_Contact_label_Path = ".//a[text()='Add Contact']";
	public static final String Refresh_label_Path = ".//a[text()='Refresh']";
	public static final String Refresh_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String Records_Per_Page_Path = ".//span[text()='Records Per Page']";

	// Edit Contact
	public static final String ContactEdit_Firm = ".//*[@id='ddlFirm_Input']";
	public static final String ContactEdit_Firm_notlisted_Path = ".//*[@id='txtFirmName']";
	public static final String ContactEdit_Updatebtn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl05_btnUpdate']";
	public static final String ContactEdit_Loadingpanel_Path = ".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_Contacts']";

}
