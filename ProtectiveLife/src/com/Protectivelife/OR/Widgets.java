package com.Protectivelife.OR;

import com.Protectivelife.Values.ValueRepository;

public class Widgets extends ValueRepository { 

	public static String Widgetname = "";
	
	// Linked Form Fields

	public static final String Wid_Form1_Path = "_ctl00_lnkFormNumber1']";
	public static final String Wid_Form2_Path = "_ctl00_lnkFormNumber2']";
	public static final String Wid_Form3_Path = "_ctl00_lnkFormNumber3']";
	public static final String Wid_Form4_Path = "_ctl00_lnkFormNumber4']";
	public static final String Wid_Form5_Path = "_ctl00_lnkFormNumber5']";

	// Quantity Text field

	public static final String Wid_Qty1_Path = "_ctl00_txtQuantity1']";
	public static final String Wid_Qty2_Path = "_ctl00_txtQuantity2']";
	public static final String Wid_Qty3_Path = "_ctl00_txtQuantity3']";
	public static final String Wid_Qty4_Path = "_ctl00_txtQuantity4']";
	public static final String Wid_Qty5_Path = "_ctl00_txtQuantity5']";

	// Add to Cart

	public static final String Wid_Checkoutbtn_Path = "_ctl00_btnAddToCart']";

	// Select Contact

	public static final String Wid_Selectrecipient_Path = "_ctl00_lblFillOutContactInfo']/a";

	// Product Search

	public static final String ProductSearch_Materialsearch_Path = "_ctl00_txtKeyword']";
	public static final String ProductSearch_Product_Path = "_ctl00_ddlProductCategory_Input']";
	public static final String ProductSearch_Product_Icon_Path = "_ctl00_ddlProductCategory_Arrow']";
	public static final String ProductSearch_Programs_Path = "_ctl00_ddlExtraFilter2_Input']";
	public static final String ProductSearch_Programs_Icon_Path = "_ctl00_ddlExtraFilter2_Arrow']";
	public static final String ProductSearch_Searchbtn_Path = "_ctl00_btnProductSearch']";

	// Express Checkout

	public static final String Expck_Form1_Path = "_ctl00_rtxtFormNumber1_Input']";
	public static final String Expck_Form2_Path = "_ctl00_rtxtFormNumber2_Input']";
	public static final String Expck_Form3_Path = "_ctl00_rtxtFormNumber3_Input']";
	public static final String Expck_Form4_Path = "_ctl00_rtxtFormNumber4_Input']";
	public static final String Expck_Form5_Path = "_ctl00_rtxtFormNumber5_Input']";
	public static final String Expck_Checkoutbtn_Path = "_ctl00_btnAddToCart']";

	// Order Lookup
	public static final String OrderLookup_Path = ".//a[text()='Order Lookup']";
	public static final String OrderLookup_Widget_Path = ".//em[text()='Order Lookup']";
	public static final String OrderLookup_Fromdate_Path = "_ctl00_rdpOrderSearchDateFrom_dateInput']";
	public static final String OrderLookup_Todate_Path = "_ctl00_rdpOrderSearchDateTo_dateInput']";
	public static final String OrderLookup_Search_Path = "_ctl00_btnSearch']";
	public static final String OrderLookup_Searchbylink_Path = "_ctl00_UpdatePanel']/div[1]/a[2]";
	public static final String OrderLookup_Orderno_Path = "_ctl00_rcbOrderNumber_Input']";
	public static final String OrderLookup_Copy_Path = "_ctl00_btnCopy']";
	public static final String OrderLookup_View_Path = "_ctl00_btnSubmit']";
	
	// Order Lookup

	
		

	// My Recent Orders

	public static final String MyRecent_OrderNo_Path = "']/table/thead/tr/td[1]/b";
	public static final String MyRecent_Status_Path = "']/table/thead/tr/td[2]/b";
	public static final String MyRecent_Options_Path = "']/table/thead/tr/td[3]/b";
	public static final String MyRecent_OrderNo1_Path = "']/table/tbody/tr[1]/td[1]";
	public static final String MyRecent_OrderNo2_Path = "']/table/tbody/tr[2]/td[1]";
	public static final String MyRecent_OrderNo3_Path = "']/table/tbody/tr[3]/td[1]";
	public static final String MyRecent_OrderNo4_Path = "']/table/tbody/tr[4]/td[1]";
	public static final String MyRecent_OrderNo5_Path = "']/table/tbody/tr[5]/td[1]";
	public static final String MyRecent_Status1_Path = "']/table/tbody/tr[1]/td[2]/span";
	public static final String MyRecent_Status2_Path = "']/table/tbody/tr[2]/td[2]/span";
	public static final String MyRecent_Status3_Path = "']/table/tbody/tr[3]/td[2]/span";
	public static final String MyRecent_Status4_Path = "']/table/tbody/tr[4]/td[2]/span";
	public static final String MyRecent_Status5_Path = "']/table/tbody/tr[5]/td[2]/span";
	public static final String MyRecent_Copy1_Path = "_ctl00_repOrders_ctl01_btnCopy']";
	public static final String MyRecent_Copy2_Path = "_ctl00_repOrders_ctl03_btnCopy']";
	public static final String MyRecent_Copy3_Path = "_ctl00_repOrders_ctl05_btnCopy']";
	public static final String MyRecent_Copy4_Path = "_ctl00_repOrders_ctl07_btnCopy']";
	public static final String MyRecent_Copy5_Path = "_ctl00_repOrders_ctl09_btnCopy']";
	public static final String MyRecent_View1_Path = "']/table/tbody/tr[1]/td[3]/a[2]";
	public static final String MyRecent_View2_Path = "']/table/tbody/tr[2]/td[3]/a[2]";
	public static final String MyRecent_View3_Path = "']/table/tbody/tr[3]/td[3]/a[2]";
	public static final String MyRecent_View4_Path = "']/table/tbody/tr[4]/td[3]/a[2]";
	public static final String MyRecent_View5_Path = "']/table/tbody/tr[5]/td[3]/a[2]";

	// Order Template
	public static final String Ordertemplate_Path = ".//a[text()='Order Templates']";
	public static final String Ordertemplate_Form1_Path = ".//*[@id='divOrderTemplates']/tbody/tr[1]/td[1]/div";
	public static final String Ordertemplate_Form2_Path = ".//*[@id='divOrderTemplates']/tbody/tr[2]/td[1]/div";
	public static final String Ordertemplate_Form3_Path = ".//*[@id='divOrderTemplates']/tbody/tr[3]/td[1]/div";
	public static final String Ordertemplate_View1_Path = ".//*[@id='divOrderTemplates']/tbody/tr[1]/td[2]/a[1]";
	public static final String Ordertemplate_View2_Path = ".//*[@id='divOrderTemplates']/tbody/tr[2]/td[2]/a[1]";
	public static final String Ordertemplate_View3_Path = ".//*[@id='divOrderTemplates']/tbody/tr[3]/td[2]/a[1]";
	public static final String Ordertemplate_Addtocart1_Path = "_ctl00_repOrderTemplates_ctl01_btnAddToCart']";
	public static final String Ordertemplate_Addtocart2_Path = "_ctl00_repOrderTemplates_ctl03_btnAddToCart']";
	public static final String Ordertemplate_Addtocart3_Path = "_ctl00_repOrderTemplates_ctl05_btnAddToCart']";

	// New Materials

	public static final String Newmaterials_Form1pdf_Path = "_ctl00_lnkPDF1']/img";
	public static final String Newmaterials_Form2pdf_Path = "_ctl00_lnkPDF2']/img";
	public static final String Newmaterials_Form3pdf_Path = "_ctl00_lnkPDF3']/img";
	public static final String Newmaterials_Form4pdf_Path = "_ctl00_lnkPDF4']/img";
	public static final String Newmaterials_Form5pdf_Path = "_ctl00_lnkPDF5']/img";

	// Announcements

	public static final String View_Announcementsbtn_Path = ".//*[@id='ctl00_cphContent_rdAnnouncements_C']/a";

   // UserKit Widget
	
	public static final String Wid_UserKit_Select_Path = "//a[contains(@id,'btnAddWidgetUserKits')]";
		public static final String Wid_UserKit_Recepient_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String Wid_UserKit_formnumber = "_ctl00_lnkFormNumber1']/i";
		public static final String Editmysuerkits = "_ctl00_UpdatePanel']/div[2]/a";
	 public static final String Wid_UserKitEmpty ="_ctl00_lblFillOutContactInfo']";
	
	// Express checkout 
	
	public static final String Express_Checkout_Path = ".//*[@id='ctl00_cphContent_btnAddWidgetExpressCheckout']";
	public static final String Wid_UserKit_SelectRecipient_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String Express_Form_Suggestion_Path = "_ctl00_rtxtFormNumber1_DropDown']/div/ul/li[1]";
	public static final String Express_Form_Qty1_Path = "_ctl00_txtQuantity1']";
	public static final String Express_ErrorMessage_Path= "_ctl00_lblErrorMessage1']";
 
	
	

}
