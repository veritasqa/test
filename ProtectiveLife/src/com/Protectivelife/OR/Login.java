package com.Protectivelife.OR;

import org.testng.asserts.SoftAssert;

public class Login extends MailListUpload{
	public static final String locatorType = "xpath";
	
	public static final String locatorType_id ="id";
	public static final String locatorType_name="name";
	public static final String locatorType_cssSelector="cssSelector";
	public static final String locatorType_linkText="linkText";
	public static final String locatorType_partialLinkText="partialLinkText";
	public static final String locatorType_classname="classname";
	public String Window_Handle = "";
	// public String Att="";
	public SoftAssert softAssert = new SoftAssert();

 

	public static final String Username_Path = ".//*[@id='txtUserName']";
	public static final String Password_Path = ".//*[@id='txtPassword']";
	public static final String Login_btn_Path = ".//*[@id='btnSubmit']";
	
	//SSO
	
	public static final String TokenId_path =".//*[@id='TokenID']";
	public static final String xml_textfield_Path = ".//*[@id='XMlText']";

}
