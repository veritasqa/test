package com.Protectivelife.OR;

public class SearchResults extends SearchUserKits {

	public static final String Searchbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideHeader_btnAdvancedSearch']";
	public static final String Searchbymaterial_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword']";
	public static final String Searchbymaterial_Searchbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideHeader_btnAdvancedSearch']";
	public static final String Add_To_Cart_btn_Path = ".//*[@id='AddToCart']";
	public static final String Add_To_Drip_btn_Path = ".//*[@id='AddToDrip']";
	
	public static final String AddtoCart_Qty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_txtQuantity']";
	public static final String AddtoCart_Calenderfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_rdpDripDate_dateInput']";
	public static final String AddtoCart_Calender_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_rdpDripDate_popupButton']";
	public static final String AddtoCart_Calendermonth_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_rdpDripDate_calendar_Title']";
	public static final String AddtoCart_CalenderOk_Path = ".//*[@id='rcMView_OK']";
	public static final String Unitsonhand_Path = ".//*[@id='divUnitsAvailIncKit']";
	public static final String POD_Path = ".//*[@id='divIsPOD']";
	public static final String AddtoCart_maxQtymessage_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_divErrorMessageCart']";
	public static final String AddtoCart_Successfullmessage_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_divErrorMessageCart']";
	
	public static final String Stock_Availability = "//*[@id='divAvailability']";
	public static final String units_Availability = "//*[@id='divUnitsAvailIncKit']";
	
	public static final String MiniCart_Message = "//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_divBottomCommandBar']";
	
	public static final String Part_Name_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/div/span[1]";
	public static final String Checkoutbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_btnSearchResultsCheckOut']";
	public static final String Minicart_Loadingpanel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_rapMiniShoppingCart']";
	public static final String Minicart_Checkoutbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']";
	public static final String Minicart_Part1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_divFormNumber']/a";
	public static final String Minicart_Datefield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_rdpDrip_dateInput']";
	public static final String Minicart_Datefield2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl01_rdpDrip_dateInput']";
	
	public static final String Searchloadingpanel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphMain_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphRightSideContent_rgdProductList']";
	
	public static final String Availability_Status = ".//*[@id='divAvailability']";
	 public static final String Minicart_Emptycart_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_lblEmptyCart']s";

	 public static final String NoRecords_found_path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_NoRecordsDiv']/span";

	 public static final String NoRecord_Message= "Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active.";

	// Order Search Page
		
		public static final String Ordersearch_RecipientFirstName_Path = ".//input[@id='ctl00_cphContent_txtFirstName']";
		public static final String Ordersearch_RecipientLastName_Path = ".//input[@id='ctl00_cphContent_txtLastName']";
		public static final String Ordersearch_CreatedUserFirstName_Path = ".//input[@id='ctl00_cphContent_txtUserFirstName']";
		public static final String Ordersearch_CreatedUserLastName_Path = ".//input[@id='ctl00_cphContent_txtUserLastName']";
		public static final String Ordersearch_Searchbtn_Path = "//a[@class='buttonAction buttonBig']";
		public static final String Ordersearch_Loadingpanel_Path = ".//div[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_rgdOrders']";
		public static final String Ordersearch_Searchgrid_Path = ".//*[@id='divContent clearfix']/div[3]/h1";
		public static final String Ordersearch_Recipient1_Path = ".//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[4]";
		public static final String Ordersearch_Recipient2_Path = ".//*[@id='ctl00_cphContent_rgdOrders_ctl00__0']/td[5]";
		public static final String Ordersearch_OrderNumber = "//*[@id='ctl00_cphContent_txtOrderNumber']";
		public static final String Ordersearch_Stocknumber = ".//*[@id='ctl00_cphContent_txtFormNumber']";
		public static final String Ordersearch_StartDate = ".//*[@id='ctl00_cphContent_RadStartDate_popupButton']";
		public static final String Ordersearch_StartDateMonth = ".//*[@id='ctl00_cphContent_RadStartDate_calendar_Title']";
		public static final String Ordersearch_StartDateOk = ".//*[@id='rcMView_OK']";
		public static final String Ordersearch_Enddate = ".//*[@id='ctl00_cphContent_RadEndDate_popupButton']";
		public static final String Ordersearch_Enddatemonth = ".//*[@id='ctl00_cphContent_RadEndDate_calendar_Title']";
		public static final String Ordersearch_EnddateOk = ".//*[@id='rcMView_OK']";

}
