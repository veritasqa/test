package com.Protectivelife.OR;

public class Confirmation extends ContactSearchPage {
	
	public static final String Order_Conf_Title_Path = ".//*[@id='ctl00_ctl00_cphContent_cphMain_divHeaderImage']/h1";
	public static final String OrderConf_OrderinfoTitle_Path = ".//*[@id='divContent clearfix']/div[2]/h1";
	public static final String OrderNumber_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_lblOrderNumber']";
	public static final String OrderConf_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_lblStatus']/span";
	public static final String OrderConf_Orderdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_lblCreateDate']";
	public static final String OrderConf_ShippedTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_divAddress']";
	public static final String OrderConf_Shippedby_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_lblShippedBy']";
	public static final String OrderConf_ShipingInst_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_lblShippingInstructions']";
	public static final String OrderConf_Orderdettitle_Path = ".//*[@id='divContent clearfix']/div[4]/h1";
	public static final String OrderConf_TicNo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[3]";
	public static final String OrderConf_Status2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[4]/span";
	public static final String OrderConf_Shipbydate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[5]";
	public static final String OrderConf_Deliverdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[6]";
	public static final String OrderConf_Cancelreqbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[7]/a";
	public static final String OrderConf_FormNo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[1]";
	public static final String OrderConf_FormTitle_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[2]";
	public static final String OrderConf_FormNo1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[1]";
	public static final String OrderConf_State_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[3]";
	public static final String OrderConf_Quantity_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[4]";
	public static final String OrderConf_Copy_Path = ".//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnCopy']";
	public static final String OrderConf_OK_Path = ".//*[@id='ctl00_ctl00_cphContent_cphPageFooter_btnOk']";
	public static final String ShipbyDate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[5]";
	public static final String ShipbyDate2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__1']/td[5]";
	
	// Cancel pop up
	public static final String OrderCancel_Popup_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_divOverlay']/div/div/div[2]/div";
	public static final String OrderCancel_Popupmsg_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_lblOverlayMessage']";
	public static final String OrderCancel_Popup_Close_Path = ".//a[text()='Close']";

	
	
	

}

