package com.Protectivelife.OR;

public class SearchUserKits extends SettingsVariables{
	
	public static final String UserKits_Header_Path = ".//*[@id='ctl00_ctl00_cphContent_cphMain_divHeaderImage']/h1";
		
	public static final String UserKits_Formnum_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_FormNumber']";
	
	public static final String UserKits_Formnum_Icon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_FormNumber']";

	public static final String UserKits_Title_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_Description']";
	
	public static final String UserKits_Title_Icon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_Description']";
	
	public static final String UserKits_LastUpdated_path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_LastUpdated']";

	public static final String UserKits_LastUpdated_Icon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_LastUpdated']";
	
	public static final String UserKits_Edit_Header_Path = "//th[contains(.,'Edit')]";
	
	public static final String UserKits_AddNew_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl03_ctl01_AddNewRecordButton']";

	public static final String UserKits_Refesh_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl03_ctl01_RefreshButton']";
	
	public static final String AddUserKits_Title_Path = ".//*[@id='ctl00_cphContent_pnlUserKitDetail']/div/h1";
	
	public static final String AddUserKitsOwner_Title_Path = ".//*[@id='ctl00_cphContent_pnlKitOwner']/div/h1";

	
	public static final String UserKits_Form_Textbox_path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
	
	public static final String UserKits_Title_Textbox_Path = ".//*[@id='ctl00_cphContent_txtDescription']";
	
	
	public static final String UserKits_Kit_Arrow_Path =".//*[@id='ctl00_cphContent_cbKitContainer_Arrow']";
	
	public static final String UserKits_Currentowner_Path = ".//*[@id='ctl00_cphContent_lblCurrentOwnerName']";
	
	
	public static final String UserKits_Newowner_Path= ".//*[@id='ctl00_cphContent_rtxtSearchOwner_Input']";
	
	
	public static final String UserKits_SaveUserKit_Path = ".//*[@id='ctl00_cphContent_btnSave']";
	
	public static final String UserKits_UpdateOwner_Path = ".//*[@id='ctl00_cphContent_lnkUpdateOwner']";

	
	public static final String UserKits_Opt1_Edit_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00__0']/td[4]/a";

	public static final String UserKits_Opt2_Edit_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00__1']/td[4]/a";

	public static final String UserKits_Delete1_btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00__0']/td[5]/a";
	public static final String UserKits_Delete2_btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00__1']/td[5]/a";


	 //Kit Container Options
	public static final String UserKits_PMTestTG_Path =".//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li[1]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
	public static final String UserKits_PMTestFolder_Path =".//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
	public static final String UserKits_QAFolder_Path =".//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li[3]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
	public static final String UserKits_QAFolder2_Path =".//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li[4]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
	public static final String UserKits_QAFolder3_Path =".//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li[5]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
	public static final String UserKits_Partedit_Path =".//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li[6]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
	public static final String UserKit_Saved_Message_Path =".//*[@id='ctl00_cphContent_lblErrorMessage']";
	

	//Edit Kit build

	public static final String EditKit_Formnumber_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[1]";
	public static final String EditKit_Formnumber1_Path = 	"//*[@id='ctl00_cphContent_grdKitBuild_ctl00__1']/td[1]";
	public static final String EditKit_Description_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[2]";
	public static final String EditKit_Quantity_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[3]";
	public static final String EditKit_Type_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[4]";
	public static final String EditKit_Location_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[5]";
	public static final String EditKit_Option_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00_ctl04_ctl00']";
	public static final String EditKit_Sort_Path = ".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[7]";
	
	 //Add From Inventory

	public static final String Kit_FormNumber_Path ="//*[@id='ctl00_cphContent_acbAddFormNumber_Input']";
	public static final String Kit_Quantity_Path =".//*[@id='ctl00_cphContent_txtQuantity']";
	public static final String Kit_SortOrder_Path =".//*[@id='ctl00_cphContent_txtSortOrder']";
	public static final String Kit_AddtoKit_Path =".//*[@id='ctl00_cphContent_btnAddToKit']";
	public static final String Kit_Location_Path = "//*[@id='ctl00_cphContent_ddlLocation_Arrow']";

	
	
	
	//Filter Options- UserKits
	
	

			public static final String KitFilter_NoFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[1]/a/span";
			public static final String KitFilter_Contains_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[2]/a/span";
			public static final String KitFilter_DoesntContains_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[3]/a/span";
			public static final String KitFilter_StartsWith_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[4]/a/span";
			public static final String KitFilter_EndsWith_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[5]/a/span";
			public static final String KitFilter_EqualTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[6]/a/span";
			public static final String KitFilter_NotEqualTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[7]/a/span";
			public static final String KitFilter_GreaterThan_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[8]/a/span";
			public static final String KitFilter_LessThan_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[9]/a/span";
			public static final String KitFilter_GreatthanorEqualto_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[10]/a/span";
			public static final String KitFilter_LessthanOrEqual_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[11]/a/span";
			public static final String KitFilter_Between_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[12]/a/span";
			public static final String KitFilter_NotBetween_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[13]/a/span";
			public static final String KitFilter_IsEmpty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[14]/a/span";
			public static final String KitFilter_NotIsEmpty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[15]/a/span";
			public static final String KitFilter_IsNull_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[16]/a/span";
			public static final String KitFilter_NotisNull_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_rfltMenu_detached']/ul/li[17]/a/span";

}
