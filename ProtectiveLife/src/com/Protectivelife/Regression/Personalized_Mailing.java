package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Personalized_Mailing extends BaseTest {

	@Test(enabled = true, priority = 1)
	public void InadminMaillistupload() throws InterruptedException, IOException {

		login(InUserName, InPassword);
		// Clearcart();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);

		// upload Nofirm
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, Maillist_Nofirm_Name, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Personilizedmailinglist_Nofirm_Path, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);

		Wait_ajax();

		// upload nofirm 205
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, Maillist_Nofirm205_Name, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Personilizedmailinglist_Nofirm205_Path, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);

		Click(locatorType, Logout_Path, driver);

	}

	// Validate Marketing piece is not displaying in Fulfillment mode - Admin
	@Test(enabled = false, priority = 2)
	public void PL_T_2_7_1_b_1_1() throws IOException, InterruptedException, AWTException {

		login(InUserName, InPassword);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, Part35, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, NoRecords_found_path, driver).equalsIgnoreCase(NoRecord_Message),
				" No records Found message is not displayed");
	}

	@Test(enabled = false, priority = 3, dependsOnMethods = "InadminMaillistupload")
	public void PL_T_2_7_1_b_1_3() throws IOException, InterruptedException, AWTException {

		// Validate user is able to clear cart for mailist with 205 recipients
		// as Inventory Admin

		login(InUserName, InPassword);
		Clearcart();

		Click(locatorType, Personalized_mailing_path, driver);

		Click(locatorType, MLU_Chooseasavedmaillist_Path, driver);
		Wait_ajax();

		Click(locatorType, li_value(Maillist_Nofirm205_Name), driver);
		Wait_ajax();

		Click(locatorType, MLU_Uploadbtn_Path, driver);
		Wait_ajax();

		Click(locatorType, MLU_Ok_Path, driver);
		Wait_ajax();

		Type(locatorType, Searchbymaterial_Path, Part17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);

		/*ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver); 
		Type(locatorType, Searchbymaterial_Path, Part17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);*/
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part17),
				Part17 + " is not displayed in the materials page");
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Minicart_Part1_Path, driver).contains(Part17),
				Part17 + " is not displayed in the Mini shopping cart");
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Type(locatorType, SCO_Name_Path, "QA Auto", driver);
		Type(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Type(locatorType, SCO_Location_Path, "QA Test", driver);
		Type(locatorType, SCO_Address_Path, Address1, driver);
		Type(locatorType, SCO_City_Path, City, driver);
		Select_DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Type(locatorType, SCO_Zip_Path, Zipcode, driver);
		Type(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_RSVPName_Path, InUserName, driver);
		Type(locatorType, SCO_RSVPPhone_Path, "222.222.2222", driver);
		Type(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Type(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part17),
				Part17 + " piece is not appeared in the 'Checkout' Page");
		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);

		Assert.assertTrue(Is_Element_Present(locatorType, SC_ProofedBtn, driver), "Proofed Button Not Displayed");
		Click(locatorType, Clear_Cart_Path, driver);

	}

	@Test(enabled = false, priority = 4)
	public void DeleteInadminUploadmaillist() throws InterruptedException, IOException {

		login(InUserName, InPassword);
		// Click(locatorType, Home_btn_Path, driver);
		Wait_ajax();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);
		Click(locatorType, MML_NOFIRM_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);

		// Deleting Second List
		Click(locatorType, MML_NOFIRM205_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);

		logout();

	}

	@Test(enabled = false, priority = 5)
	public void SalesdeskUploadmaillist() throws InterruptedException, IOException {

		login(SalesdeskUserName, SalesdeskPassword);
		// Clearcart();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);

		// upload Nofirm
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, Maillist_Nofirm_Name, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Personilizedmailinglist_Nofirm_Path, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);

		Wait_ajax();

		// Upload No firm 205
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, Maillist_Nofirm205_Name, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Personilizedmailinglist_Nofirm205_Path, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = false, priority = 6, dependsOnMethods = "SalesdeskUploadmaillist")
	public void PL_TC_2_7_1_b_1_4() throws IOException, InterruptedException, AWTException {

		// Validate user is able to place a Marketing order as Salesdesk

		login(SalesdeskUserName, SalesdeskPassword);
		Clearcart();
		Click(locatorType, Personalized_mailing_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		// Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		// Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Chooseasavedmaillist_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value(Maillist_Nofirm_Name), driver);
		Wait_ajax();
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part17),
				Part17 + " is not displayed in the materials page");
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Minicart_Part1_Path, driver).contains(Part17),
				Part17 + " is not displayed in the Mini shopping cart");
		Click(locatorType, Minicart_Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Type(locatorType, SCO_Name_Path, "Q Auto", driver);
		Type(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Type(locatorType, SCO_Location_Path, "QA Test", driver);
		Type(locatorType, SCO_Address_Path, Address1, driver);
		Type(locatorType, SCO_City_Path, City, driver);
		Select_DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Type(locatorType, SCO_Zip_Path, Zipcode, driver);
		Type(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_RSVPName_Path, SalesdeskUserName, driver);
		Type(locatorType, SCO_RSVPPhone_Path, "222.222.2222", driver);
		Type(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Type(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part17),
				Part17 + " piece is not appeared in the 'Checkout' Page");
		Assert.assertEquals(GetCSS_Backgroundcolor(locatorType, SC_Proofbtn_Path, driver), Orangecolor,
				"Proof Button is not displayed in Orange Color");
		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertEquals(GetCSS_Backgroundcolor(locatorType, SC_ProofedBtn, driver), Bluecolor,
				"Proof Button is not displayed in blue Color after clicked Proof");
		Assert.assertTrue(Element_Is_Displayed(locatorType, SC_ProofedBtn, driver), "Proofed Button is not displayed");
		Click(locatorType, SC_EditVariable, driver);
		Wait_ajax();
		Type(locatorType, SCO_Location_Path, "QA Test 123", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part17),
				Part17 + " piece is not appeared in the 'Checkout' Page after Edit variable");
		Assert.assertEquals(GetCSS_Backgroundcolor(locatorType, SC_Proofbtn_Path, driver), Orangecolor,
				"Proof Button is not displayed in Orange Color after Edit variable");
		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertEquals(GetCSS_Backgroundcolor(locatorType, SC_ProofedBtn, driver), Bluecolor,
				"Proof Button is not displayed in blue Color after clicked Proof");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Assert.assertTrue(Get_DropDown(locatorType, SC_ShipMethoddrop_Path, driver).equalsIgnoreCase("USPS"),
				"'USPS' pre-populates in 'Ship Method' drop down");
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_2_7_1_b_3'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_2_7_1_b_3 is '" + OrderNumber + "'");

	}

	@Test(enabled = false, priority = 7)
	public void DeleteSalesdesk_Uploadmaillist() throws InterruptedException, IOException {

		login(SalesdeskUserName, SalesdeskPassword);
		// Click(locatorType, Home_btn_Path, driver);
		Wait_ajax();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);
		Click(locatorType, MML_NOFIRM_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);

		// Deleting Second List
		Click(locatorType, MML_NOFIRM205_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);
		Click(locatorType, Logout_Path, driver);
	}

	// Validate user is able to clear a cart with multiple drips as crmsso
	@Test(enabled = false, priority = 8)
	public void PL_TC_2_7_1_b_1_6() throws InterruptedException, IOException {

		SSOLogin(Crm_Login);
		Wait_ajax();
		// Landing page
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + " Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Inadmin"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");

		Clearcart();
		Click(locatorType, Personalized_mailing_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part17),
				Part17 + " is not displayed in the materials page");
		Assert.assertFalse(Get_Attribute(locatorType, SC_PartDatefiled_Path, "value", driver).isEmpty(),
				"Current available Date is not displayed in the Shopping cart");

		// Assert.assertTrue(Is_Element_Present(locatorType,
		// AddtoCart_Calenderfield_Path, driver),"Current Date is not displayed
		// in the Material search page for the Part");
		/*Assert.assertTrue(
				Get_Attribute(locatorType, AddtoCart_Calenderfield_Path, "value", driver)
						.equalsIgnoreCase(Get_Todaydate("M/dd/YYYY")),
						Get_Attribute(locatorType, AddtoCart_Calenderfield_Path, "value", driver) +	Get_Todaydate("MM/dd/YYYY")+"Current Date is not displayed in the Material search page for the Part");*/
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Wait_ajax();
		Click(locatorType, AddtoCart_Calender_Path, driver);
		Datepicker(AddtoCart_Calendermonth_Path, AddtoCart_CalenderOk_Path);
		Wait_ajax();
		Thread.sleep(2000);
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Assert.assertTrue(
				Get_Attribute(locatorType, Minicart_Datefield2_Path, "value", driver)
						.equalsIgnoreCase(Get_Futuredate("M/d/YYYY")),
				Get_Attribute(locatorType, Minicart_Datefield2_Path, "value", driver) + Get_Futuredate("MM/dd/YYYY")
						+ " CAC_1059_09_12 <Future Date:MM/DD/YYYY> is not appears in 'Mini shopping Cart'");
		Click(locatorType, Minicart_Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Type(locatorType, SCO_Name_Path, "QA Auto", driver);
		Type(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Type(locatorType, SCO_Location_Path, "QA Test", driver);
		Type(locatorType, SCO_Address_Path, Address1, driver);
		Type(locatorType, SCO_City_Path, City, driver);
		Select_DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Type(locatorType, SCO_Zip_Path, Zipcode, driver);
		Type(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_RSVPName_Path, SalesdeskUserName, driver);
		Type(locatorType, SCO_RSVPPhone_Path, "(222) 222-2222", driver);
		Type(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Type(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path,
		// driver).contains(Part17),
		// Part17 + " piece is not appeared in the 'Checkout' Page");
		// Assert.assertTrue(Is_Element_Present(locatorType,
		// AddtoCart_Calenderfield_Path, driver),"Current Date is not displayed
		// in the Material search page for the Part");

		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertTrue(Get_Text(locatorType, SC_Proofbtn_Path, driver).equalsIgnoreCase("Proofed"),
				"Proofed button is not displays ");

		Assert.assertTrue(Get_Attribute(locatorType, SC_PartDatefiled2_Path, "value", driver)
				.equalsIgnoreCase(Get_Futuredate("M/d/YYYY")), "Future Date is not displayed in the Shopping cart");

		Click(locatorType, SC_Proofbtn2_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertTrue(Get_Text(locatorType, SC_Proofbtn2_Path, driver).equalsIgnoreCase("Proofed"),
				"Proofed button is not displays ");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Assert.assertTrue(Get_DropDown(locatorType, SC_ShipMethoddrop_Path, driver).equalsIgnoreCase("USPS"),
				"'USPS' pre-populates in 'Ship Method' drop down");
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, Checkout_Path, driver);

		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_2_7_1_b_3'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_2_7_1_b_3 is '" + OrderNumber + "'");
		softAssert.assertTrue(Get_Text(locatorType, ShipbyDate1_Path, driver).contains(Get_Todaydate("M/d/YYYY")),
				"'CAC_1059_09_12' with <Today's Date MM/DD/YYYY> is not displayed in 'Ship By Date' field");
		softAssert.assertTrue(Get_Text(locatorType, ShipbyDate2_Path, driver).contains(Get_Futuredate("M/d/YYYY")),
				"'CAC_1059_09_12' with <Futures's Date MM/DD/YYYY> is not displayed in 'Ship By Date' field");
		// softAssert.assertAll();
	}

	@Test(enabled = false, priority = 4)
	public void PL_TC_2_7_1_b_1_5() throws IOException, InterruptedException, AWTException {

		// Validate user is able to place an order with multiple drips as
		// salesdesk

		login(SalesdeskUserName, SalesdeskPassword);
		Clearcart();

		Click(locatorType, Personalized_mailing_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part17),
				Part17 + " is not displayed in the materials page");
		Assert.assertFalse(Get_Attribute(locatorType, SC_PartDatefiled_Path, "value", driver).isEmpty(),
				"Current available Date is not displayed in the Shopping cart");

		// Assert.assertTrue(Is_Element_Present(locatorType,
		// AddtoCart_Calenderfield_Path, driver),"Current Date is not displayed
		// in the Material search page for the Part");
		/*Assert.assertTrue(
				Get_Attribute(locatorType, AddtoCart_Calenderfield_Path, "value", driver)
						.equalsIgnoreCase(Get_Todaydate("M/dd/YYYY")),
						Get_Attribute(locatorType, AddtoCart_Calenderfield_Path, "value", driver) +	Get_Todaydate("MM/dd/YYYY")+"Current Date is not displayed in the Material search page for the Part");*/
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Wait_ajax();
		Click(locatorType, AddtoCart_Calender_Path, driver);
		Datepicker(AddtoCart_Calendermonth_Path, AddtoCart_CalenderOk_Path);
		Wait_ajax();
		Thread.sleep(2000);
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)
				.equalsIgnoreCase("Successfully Added!"), "'Successfully Added!' message is not displayed");
		Assert.assertTrue(
				Get_Attribute(locatorType, Minicart_Datefield2_Path, "value", driver)
						.equalsIgnoreCase(Get_Futuredate("M/d/YYYY")),
				Get_Attribute(locatorType, Minicart_Datefield2_Path, "value", driver) + Get_Futuredate("MM/dd/YYYY")
						+ " CAC_1059_09_12 <Future Date:MM/DD/YYYY> is not appears in 'Mini shopping Cart'");
		Click(locatorType, Minicart_Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Type(locatorType, SCO_Name_Path, "QA Auto", driver);
		Type(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Type(locatorType, SCO_Location_Path, "QA Test", driver);
		Type(locatorType, SCO_Address_Path, Address1, driver);
		Type(locatorType, SCO_City_Path, City, driver);
		Select_DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Type(locatorType, SCO_Zip_Path, Zipcode, driver);
		Type(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_RSVPName_Path, SalesdeskUserName, driver);
		Type(locatorType, SCO_RSVPPhone_Path, "(222) 222-2222", driver);
		Type(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Type(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path,
		// driver).contains(Part17),
		// Part17 + " piece is not appeared in the 'Checkout' Page");
		// Assert.assertTrue(Is_Element_Present(locatorType,
		// AddtoCart_Calenderfield_Path, driver),"Current Date is not displayed
		// in the Material search page for the Part");

		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertTrue(Get_Text(locatorType, SC_Proofbtn_Path, driver).equalsIgnoreCase("Proofed"),
				"Proofed button is not displays ");

		Assert.assertTrue(Get_Attribute(locatorType, SC_PartDatefiled2_Path, "value", driver)
				.equalsIgnoreCase(Get_Futuredate("M/d/YYYY")), "Future Date is not displayed in the Shopping cart");

		Click(locatorType, SC_Proofbtn2_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertTrue(Get_Text(locatorType, SC_Proofbtn2_Path, driver).equalsIgnoreCase("Proofed"),
				"Proofed button is not displays ");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Assert.assertTrue(Get_DropDown(locatorType, SC_ShipMethoddrop_Path, driver).equalsIgnoreCase("USPS"),
				"'USPS' pre-populates in 'Ship Method' drop down");
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_2_7_1_b_3'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_2_7_1_b_3 is '" + OrderNumber + "'");

	}

}
