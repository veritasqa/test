package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class ProductSearchWidgetTest extends BaseTest {

	public void Hover_Over_widget() throws InterruptedException {
		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Product_Search_Path, driver);

		Click_On_Element(locatorType, Product_Search_Path, driver);
	}

	@Test
	public void PL_TC_2_6_4_1_1() throws InterruptedException {

		Hover_Over_widget();

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, ProductSearch_Searchbtn_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, ProductSearch_Materialsearch_Path), "QA_Multifunction",
				driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, ProductSearch_Searchbtn_Path), driver);

		ContactSearch(STATE_RESTRICTION_Name, STATE_RESTRICTION_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1), Part1 + " is not displayed");

		Clearcart();

		Click_On_Element(locatorType, Home_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, ProductSearch_Materialsearch_Path),
				"QA_MULTIFUNCTION_KEYWORDS", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, ProductSearch_Searchbtn_Path), driver);

		ContactSearch(STATE_RESTRICTION_Name, STATE_RESTRICTION_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1), Part1 + " is not displayed");

		Clearcart();

		Click_On_Element(locatorType, Home_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, ProductSearch_Materialsearch_Path),
				"QA_MULTIFUNCTION_TITLE", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, ProductSearch_Searchbtn_Path), driver);

		ContactSearch(STATE_RESTRICTION_Name, STATE_RESTRICTION_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1), Part1 + " is not displayed");

		Clearcart();

		Click_On_Element(locatorType, Home_btn_Path, driver);

	}

	@Test

	public void PL_TC_2_6_4_2_1() {

		Click_On_Element(locatorType, Home_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, ProductSearch_Materialsearch_Path),
				"QA_MULTIFUNCTION_TITLE", driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, ProductSearch_Searchbtn_Path), driver);

		ContactSearch(STATE_RESTRICTION_Name, STATE_RESTRICTION_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1), Part1 + " is not displayed");

		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);

		ExplicitWait_Element_Visible(locatorType, AddtoCart_Successfullmessage_Path, driver);

		Click_On_Element(locatorType, Checkoutbtn_Path, driver);

		Clearcart();
	}

	@BeforeTest
	public void beforetest() throws IOException, InterruptedException {
		login(UserName, Password);

	}
}
