package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Validate_POD extends BaseTest {
	
	@Test
	public void PL_TC_2_7_1_5_1ANDPL_TC_2_7_1_5_2ANDPL_TC_2_7_1_5_3()
			throws IOException, InterruptedException, AWTException {

		// Validate user is able to clear cart for mailist with 205 recipients
		// as Inventory Admin

		login(InUserName, InPassword);
		Clearcart();

		Click(locatorType, Personalized_mailing_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Thread_Sleep(2000);
		Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part17),
				Part17 + " is not displayed in the materials page");
		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Thread_Sleep(550);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver)
		// .equalsIgnoreCase("Successfully Added!"), "'Successfully Added!'
		// message is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Minicart_Part1_Path, driver).contains(Part17),
				Part17 + " is not displayed in the Mini shopping cart");
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Type(locatorType, SCO_Name_Path, "Q Auto", driver);
		Type(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Type(locatorType, SCO_Location_Path, "QA Test", driver);
		Type(locatorType, SCO_Address_Path, Address1, driver);
		Type(locatorType, SCO_City_Path, City, driver);
		DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Type(locatorType, SCO_Zip_Path, Zipcode, driver);
		Type(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_RSVPName_Path, InUserName, driver);
		Type(locatorType, SCO_RSVPPhone_Path, "(222) 222-2222", driver);
		Type(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Type(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part17),
				Part17 + " piece is not appeared in the 'Checkout' Page");
		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);
		Assert.assertTrue(Get_Text(locatorType, SC_Proofbtn_Path, driver).equalsIgnoreCase("Proofed"),
				"Proofed button is not displays ");
		DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Wait_ajax();
		Assert.assertTrue(Get_DropDown(locatorType, SC_ShipMethoddrop_Path, driver).equalsIgnoreCase("USPS"),
				"'USPS' pre-populates in 'Ship Method' drop down");
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Wait_ajax();
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_2_7_1_b_3'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_2_7_1_b_3 is '" + OrderNumber + "'");

	}

}
