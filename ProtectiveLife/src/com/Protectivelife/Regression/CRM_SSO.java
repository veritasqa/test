package com.Protectivelife.Regression;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class CRM_SSO extends BaseTest {

	// Verify security roles for Inadmin CRM SSO user while placing
	// an order for piece having Chargeback cost,
	// Non - Chargebackcost and Chargeback and non - chargeback

	@Test
	public void PL_TC_CRM_SSO_1_3_3() throws InterruptedException {
		SSOLogin(Crm_Login);
		Wait_ajax();
		// Landing page
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Inadmin"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");
		Type(locatorType, Fullfilment_Search_Path, Part25, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(500);

		// Search results Page

		ExplicitWait_Element_Visible(locatorType, AddtoCart_Successfullmessage_Path, driver);

		Click(locatorType, Checkoutbtn_Path, driver);

		// Checkout Page

		Select_DropDown_VisibleText(locatorType, Order_On_Behalf_Of_Path, "1N/A - NOT APPLICABLE", driver);

		Type(locatorType, SC_Name_Path, Part25, driver);
		Type(locatorType, SC_Address1_Path, Address1, driver);
		Type(locatorType, SC_City_Path, City, driver);
		Type(locatorType, SC_State_Path, State, driver);
		Type(locatorType, SC_Zip_Path, Zipcode, driver);
		Type(locatorType, SC_Email_Path, EmailId, driver);

		Click(locatorType, Order_Confirmation_Checkbox_Path, driver);
		Click(locatorType, Shipped_Confirmation_Checkbox_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part25),
				Part25 + " piece is not appeared in the 'Checkout' Page");
		Click(locatorType, Next_btn_Path, driver);

		Thread.sleep(5000);
		Wait_ajax();

		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Name_Path, driver), "Name field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Address1_Path, driver),
				"Address field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_City_Path, driver), "City field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_State_Path, driver), "State field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Zip_Path, driver), "Zip field is not disabled");
		Assert.assertFalse(Element_IsEnabled_Nowait(locatorType, SC_Country_Path, driver),
				"Country field is not disabled");
		Assert.assertTrue(Element_Isselected(locatorType, Order_Confirmation_Checkbox_Path, driver),
				"Order Confirmation is not checked off");
		Assert.assertTrue(Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver),
				"Shipped Confirmation is not checked off");

		Thread_Sleep(2000);
		Click(locatorType, SC_DeliverydateCalender_Path, driver);
		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);

		Thread.sleep(3000);
		//Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);

		if (Is_Element_Present(locatorType, SC_Costcenterck_Path, driver)) {
			Click(locatorType, SC_Costcenterck_Path, driver);
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		} else {
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		}
		Click(locatorType, SC_Checkout_Path, driver);

		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);

		System.out.println("Order Number placed in method  " + "PL_TC_CRM_SSO_1_3_3" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + " PL_TC_CRM_SSO_1_3_3" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, Part25, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);

		Wait_ajax();
		Type(locatorType, Searchbymaterial_Path, Part21, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		Wait_ajax();
		Thread_Sleep(5000);
		Click(locatorType, Add_To_Cart_btn_Path, driver);

		Thread_Sleep(500);

		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);

		Select_DropDown_VisibleText(locatorType, Order_On_Behalf_Of_Path, "1N/A - NOT APPLICABLE", driver);

		Click(locatorType, Order_Confirmation_Checkbox_Path, driver);
		Click(locatorType, Shipped_Confirmation_Checkbox_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, SC_PartName_Path, driver).contains(Part25),
				Part25 + " piece is not appeared in the 'Checkout' Page");
		Click(locatorType, Next_btn_Path, driver);

		Thread.sleep(3000);
		Wait_ajax();

		// Checkout Page
		
		Thread_Sleep(2000);
		Click(locatorType, SC_DeliverydateCalender_Path, driver);
		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);

		Thread.sleep(3000);
		//Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);

		if (Is_Element_Present(locatorType, SC_Costcenterck_Path, driver)) {
			Click(locatorType, SC_Costcenterck_Path, driver);
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		} else {
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		}
		Click(locatorType, SC_Checkout_Path, driver);

		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);

		System.out.println("Order Number placed in method  " + "PL_TC_CRM_SSO_1_3_3" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + " PL_TC_CRM_SSO_1_3_3" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

		Clearcart();

	}

	@Test
	public void PL_TC_CRM_SSO_1_3_4() throws InterruptedException {

		SSOLogin(Crm_SalesDesk_Login);
		Thread.sleep(2000);

		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(
				Get_Text(locatorType, LP_Username_Path, driver)
						.equalsIgnoreCase("Supplies For Life Agent Services Guest"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");

		Type(locatorType, Fullfilment_Search_Path, Part21, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);

		ExplicitWait_Element_Visible(locatorType, AddtoCart_Successfullmessage_Path, driver);

		// Assert.assertTrue(Get_Text(locatorType,
		// AddtoCart_Successfullmessage_Path, driver).trim()
		// .equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1!
		// message is not not appeared");
		Thread_Sleep(500);

		Click(locatorType, Checkoutbtn_Path, driver);

		Select_DropDown_VisibleText(locatorType, Order_On_Behalf_Of_Path, "1N/A - NOT APPLICABLE", driver);

		Type(locatorType, SC_Name_Path, Part25, driver);
		Type(locatorType, SC_Address1_Path, Address1, driver);
		Type(locatorType, SC_City_Path, City, driver);
		Type(locatorType, SC_State_Path, State, driver);
		Type(locatorType, SC_Zip_Path, Zipcode, driver);
		Type(locatorType, SC_Email_Path, EmailId, driver);
		Assert.assertTrue(Element_Isselected(locatorType, Order_Confirmation_Checkbox_Path, driver),
				"Order Confirmation is not checked off");
		Assert.assertTrue(Element_Isselected(locatorType, Shipped_Confirmation_Checkbox_Path, driver),
				"Shipped Confirmation is not checked off");


		Click(locatorType, Next_btn_Path, driver);

		Thread.sleep(3000);
		Wait_ajax();
		Thread_Sleep(2000);
		Click(locatorType, SC_DeliverydateCalender_Path, driver);
		Datepicker(SC_CalenderMonth_Path, SC_CalenderOK_Path);

		Thread.sleep(3000);
	//	Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
	

		if (Is_Element_Present(locatorType, SC_Costcenterck_Path, driver)) {
			Click(locatorType, SC_Costcenterck_Path, driver);
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		} else {
			Select_DropDown_VisibleText(locatorType, SC_SC_Costcenterdrop_Path_Path, "99999", driver);
		}
		Click(locatorType, SC_Checkout_Path, driver);

		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);

		System.out.println("Order Number placed in method  " + "PL_TC_CRM_SSO_1_3_3" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + " PL_TC_CRM_SSO_1_3_3" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);
		Clearcart();

	}

	@Test
	public void PL_TC_CRM_SSO_1_3_6() throws InterruptedException {

		SSOLogin(Crm_firm);
		Thread.sleep(2000);
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Salesdesk"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");
		Type(locatorType, Fullfilment_Search_Path, Part20, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);

	}

	@Test
	public void PL_TC_CRM_SSO_1_3_7() throws InterruptedException {

		SSOLogin(Crm_firm2);
		Thread.sleep(2000);
		Assert.assertTrue(Get_Current_Url(driver).equalsIgnoreCase(Default_URL),
				Get_Current_Url(driver) + "Url iS not Matching");
		Assert.assertTrue(Get_Text(locatorType, LP_Username_Path, driver).equalsIgnoreCase("Priyanka Salesdesk"),
				Get_Text(locatorType, LP_Username_Path, driver) + "- Username Mismatch");
		Type(locatorType, Fullfilment_Search_Path, Part20, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);

	}
}
