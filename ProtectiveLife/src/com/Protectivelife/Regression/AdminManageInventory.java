package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Protectivelife.Base.BaseTest;

public class AdminManageInventory extends BaseTest {

	public SoftAssert softAssert1;

	public void ManageInventoryNavigation() {
		Hover(locatorType, Admin_btn_Path, driver);

		Click(locatorType, Manage_Inventory_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MI_Searchbtn_Path, driver);
	}

	public String History(String Changehistory) {

		return ".//td[contains(text(),'" + Changehistory + "')]";
	}

	@Test(enabled = false, priority = 5)
	public void PL_TC_2_5_2_1_1() throws IOException, InterruptedException {

		// Page opens with create new item, search items, and search results
		// grids and displays appropriate fields

		login(UserName, Password);
		ManageInventoryNavigation();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_New_CreateNewTypeTitle_Path, driver),
				"'Create New Item' section on page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_SI_SearchItemsTitle_Path, driver),
				"'Search Item' section on page is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_New_CreateNewType_Path, driver),
				"'Create New Type' drop-down is not displayd in 'Create New Item' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_New_CreateNewType_Path, "value", driver).equalsIgnoreCase("- Any -"),
				"'Create New Type' drop-down has '- Any -' is not the default value");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_New_Add_Btn_Path, driver),
				"'Add' button is not displayed near 'Create New Type' text field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_New_FormNoToCopy_Path, driver),
				" 'Form Number to Copy' field is not displayed in 'Create New Item' section");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_New_Copy_Btn_Path, driver),
				" 'Copy' button displays near 'Form Number To Copy' text field");
		Click(locatorType, MI_New_CreateNewType_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Electronic Only"), driver),
				" 'Electronic Only' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Formsbook"), driver),
				" 'Formsbook' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Kit - On the Fly"), driver),
				" 'Kit-On the Fly' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Kit - Prebuilt"), driver),
				" 'Kit-Prebuilt' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("POD � Customizable"), driver),
				" 'POD-Customizable' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Premium Item"), driver),
				" 'Premium Item' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Print On-Demand"), driver),
				" 'Print On-Demand' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Stock"), driver),
				" 'Stock' is not displayed 'Create New Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("User Kit"), driver),
				" 'User Kit' is not displayed 'Create New Type' drop-down");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_FormNo_Path, driver),
				" 'Form Number' field is not displayed in 'Search Items' section");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_Title_Path, driver),
				" 'Title' field is not displayd in 'Search Items' section");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_Keyword_Path, driver),
				" 'Keyword' field is not displays in 'Search Items' section");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_Madatoryfieldtext_Path, driver),
				" the text '*Searches on: Item ID, Item Description, Notes and Keyword Fields' is not displayed next to the 'Keyword' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_InventoryType_Path, driver),
				" 'inventory Type' drop-down is not displayed in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_InventoryType_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in ' inventory Type' drop-down");

		Click(locatorType, MI_SI_InventoryType_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Electronic Only"), driver),
				" 'Electronic Only' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Formsbook"), driver),
				" 'Formsbook' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Kit - On the Fly"), driver),
				" 'Kit-On the Fly' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Kit - Prebuilt"), driver),
				" 'Kit-Prebuilt' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("POD � Customizable"), driver),
				" 'POD-Customizable' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Premium Item"), driver),
				" 'Premium Item' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Print On-Demand"), driver),
				" 'Print On-Demand' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Print to Shelf"), driver),
				" 'Print to Shelf' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Stock"), driver),
				" 'Stock' is not displayed 'Inventory Type' drop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("User Kit"), driver),
				" 'User Kit' is not displayed 'Inventory Type' drop-down");
		Click(locatorType, MI_SI_InventoryType_Icon_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_CollateralType_Path, driver),
				" 'Collateral Type' is not displayed in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_CollateralType_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'Collateral Type' drop-down");
		Click(locatorType, MI_SI_CollateralType_Path, driver);
		Wait_ajax();
		// Thread_Sleep(2000);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Book"), driver),
				" 'Book' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Box"), driver),
				" 'Box' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Brochure"), driver),
				" 'Brochure' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Business Card"), driver),
				" 'Business Card' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Display"), driver),
				" 'Display' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Book"), driver),
				" 'Book' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Box"), driver),
				" 'Box' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Brochure"), driver),
				" 'Brochure' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Business Card"), driver),
				" 'Business Card' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Display"), driver),
				" 'Display' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Envelope"), driver),
				" 'Envelope' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Flyer"), driver),
				" 'Flyer' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Email"), driver),
				" 'Email' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Folder"), driver),
				" 'Folder' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Form"), driver),
				" 'Form' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Kit"), driver),
				" 'Kit' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Mailer"), driver),
				" 'Mailer' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Multimedia"), driver),
				" 'Multimedia' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Promotional"), driver),
				" 'Promotional' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Prospectus"), driver),
				" 'Prospectus' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Sticker"), driver),
				" 'Sticker' is not displayed in the 'Collateral Type' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Supplement"), driver),
				" 'Supplement' is not displayed in the 'Collateral Type' drop-downdrop-down");
		Click(locatorType, MI_SI_CollateralType_Icon_Path, driver);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_Category_Path, driver),
				" 'Category' drop-down is not displayed in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_Category_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" '- Any -' is default value displaying in 'Category' drop-down");
		Click(locatorType, MI_SI_Category_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Annuities"), driver),
				" 'Annuities' is not displayed in the 'Category' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Life Insurance"), driver),
				" 'Life Insurance' is not displayed in the 'Category' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Concepts"), driver),
				" 'Concepts' is not displayed in the 'Category' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Other"), driver),
				" 'Other' is not displayed in the 'Category' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Programs"), driver),
				" 'Programs' is not displayed in the 'Category' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Sales Concepts"), driver),
				" 'Sales Concepts' is not displayed in the 'Category' drop-downdrop-down");
		Click(locatorType, MI_SI_Category_Icon_Path, driver);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_UserGroups_Path, driver),
				" 'User Group' is not displayed  in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_UserGroups_Path, "value", driver).equalsIgnoreCase("- Any -"),
				"  '-Any-' as default value in User Groups' field");
		Click(locatorType, MI_SI_UserGroups_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Advisor"), driver),
				" 'Advisor' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Allstate"), driver),
				" 'Allstate' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("RD, ADM"), driver),
				" 'RD, ADM' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("BGA"), driver),
				" 'BGA' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Hybrid Wholesaler"), driver),
				" 'Hybrid Wholesaler' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Independent Agent"), driver),
				" 'Independent Agent' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("RVP"), driver),
				" 'RVP' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Marketing"), driver),
				" 'Marketing' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("DLC"), driver),
				" 'DLC' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Sales Desk"), driver),
				" 'Sales Desk' is not displayed in the 'User Group' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("System Admin"), driver),
				" 'System Admin' is not displayed in the 'User Group' drop-downdrop-down");
		Click(locatorType, MI_SI_UserGroups_Icon_Path, driver);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_Active_Path, driver),
				" 'Active' field is not displayed in 'Search Items' section ");
		Click(locatorType, MI_SI_Active_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Active"), driver),
				" 'Active' is not displayed in the  'Active' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Inactive"), driver),
				" 'Inactive' is not displayed in the  'Active' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Obsolete"), driver),
				" 'Obsolete' is not displayed in the  'Active' drop-downdrop-down");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_UnitsOnHand_Path, driver),
				" 'Units on Hand' field is not displayed in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_UnitsOnHand_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" 'Any' is not displayed by default in 'Units On Hand' dropdown");
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Wait_ajax();
		Click(locatorType, MI_SI_UnitsOnHand_Path, driver);
		// Thread_Sleep(1500);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("All"), driver),
				" 'ALL' is not displayed in the  'Units On Hand' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Low Stock"), driver),
				" 'Low Stock' is not displayed in the  'Units On Hand' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Out of Stock"), driver),
				" 'Out of Stock' is not displayed in the  'Units On Hand' drop-downdrop-down");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_ProductArea_Path, driver),
				" 'Product Area' is not displayed in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_ProductArea_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" 'Any' is default value displaying in 'Product Area' dropdown");
		Click(locatorType, MI_SI_UnitsOnHand_Icon_Path, driver);
		Wait_ajax();
		Click(locatorType, MI_SI_ProductArea_Path, driver);
		// Thread_Sleep(800);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Advantage Choice UL"), driver),
				" 'Advantage Choice UL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Asset Builder"), driver),
				" 'Asset Builder' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Critical Answer"), driver),
				" 'Critical Answer' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Custom Choice UL"), driver),
				" 'Custom Choice UL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Dimensions II"), driver),
				" 'Dimensions II' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Fixed Annuity - General"), driver),
				" 'Fixed Annuity-General' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("FutureSaver II"), driver),
				" 'FutureSaver II' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Income Builder"), driver),
				" 'Income Builder' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Indexed Annuity II"), driver),
				" 'Indexed Annuity II' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Indexed Annuity II MVA"), driver),
				" 'Indexed Annuity II MVA' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Indexed Annuity NY"), driver),
				" 'Indexed Annuity NY' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Indexed Choice UL"), driver),
				" 'Indexed Choice UL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Investors Choice VUL"), driver),
				" 'Investors Choice VUL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Investors Choice VUL"), driver),
				" 'Investors Choice VUL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Life - General"), driver),
				" 'Life General' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Non-Par Whole Life"), driver),
				" 'Non-Par Whole Life' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Non-Par Whole Life"), driver),
				" 'Non-Par Whole Life' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Optional UL Riders & Endorsements"), driver),
				" 'Optional UL Riders & Endrosements' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Premiere III"), driver),
				" 'Premiere III' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Preserver II"), driver),
				" 'Preserver II' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProClassic UL"), driver),
				" 'ProClassic UL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProClassic UL NY"), driver),
				" 'ProClassic UL NY' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProPayer Income Annuity"), driver),
				" 'Propayer Income Annuity' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProPayer Income Annuity NY"), driver),
				" 'Propayer Income Annuity NY' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Platinum"), driver),
				" 'ProSaver Platinum' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Platinum Plus"), driver),
				" 'ProSaver Platinum Plus' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Platinum Plus NY"), driver),
				" 'ProSaver Platinum NY' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Plus II"), driver),
				" 'ProSaver Plus II' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver RateBuilder"), driver),
				" 'ProSaver  RateBuilder' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Secure II"), driver),
				" 'ProSaver Secure II' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Secure R"), driver),
				" 'ProSaver Secure R' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Select"), driver),
				" 'ProSaver Select' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("ProSaver Select R"), driver),
				" 'ProSaver Select R' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Protected Lifetime Income Options"), driver),
				" 'Protected Lifetime Income Options' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Protective VA Investors Series"), driver),
				" 'Protective VA Investors Series' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(
				Element_Is_Displayed(locatorType, li_value("Protective Variable Annuity II B Series"), driver),
				" 'Protective Variable Annuity II B Series' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Strategic Objectives VUL"), driver),
				" 'Strategic Objectives VUL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Survivor UL"), driver),
				" 'Survivor UL' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Survivorship Term"), driver),
				" 'Survivorship Term' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Variable Annuity - General"), driver),
				" 'Variable Annuity-General' is not displayed in the  'Product Area' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("General"), driver),
				" 'General' is not displayed in the  'Product Area' drop-downdrop-down");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_SI_Programs_Path, driver),
				" 'Sales Concepts/Programs' field is not displayed in 'Search Items' section");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_Programs_Path, "value", driver).equalsIgnoreCase("- Please Select -"),
				" '-Please Select-' is not displayed by default in 'USales Concepts/Programs' dropdown");
		Click(locatorType, MI_SI_ProductArea_Icon_Path, driver);
		Wait_ajax();
		Click(locatorType, MI_SI_Programs_Path, driver);
		// Wait_ajax();
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("About Protective Life"), driver),
				" 'About Protective Life' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Annuity Review"), driver),
				" 'Annuity Review' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Consumer Seminars"), driver),
				" 'Consumer Seminars' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Defined Objectives"), driver),
				" 'Defined Objectives' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("EDJ Regional Training"), driver),
				" 'EDJ Regional Training' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Estate Planning Basics"), driver),
				" 'Estate Planning Basics' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Financial Planning for Women"), driver),
				" 'Financial Planning for women' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("IRA Planning"), driver),
				" 'IRA Planning' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Life Check Up"), driver),
				" 'Life Check Up' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Markets & Investing"), driver),
				" 'Markets & Investing' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Premium Financing"), driver),
				" 'Premium Financing' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Protecting Your Retirement"), driver),
				" 'Protecting Your Retirement' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Retirement Income (VISTA)"), driver),
				" 'Retirement Income(VISTA)' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Tax Information"), driver),
				" 'Tax Information' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Understanding Social Security"), driver),
				" 'Understanding Social Security' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Wealth Transfer"), driver),
				" 'Wealth Transfer' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Working With Couples"), driver),
				" 'Working With Couples' is not displayeod in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Working With Farmers"), driver),
				" 'Working With Farmers' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, li_value("Working With Widows"), driver),
				" 'Working With Widows' is not displayed in the 'Sales Concepts/Programs' drop-downdrop-down");
		Click(locatorType, MI_SI_Programs_Icon_Path, driver);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Searchbtn_Path, driver),
				" 'Search' button is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Clearbtn_Path, driver),
				" 'Clear' button is not displayed");
		softAssert1.assertFalse(Element_Is_Displayed(locatorType, MI_SI_EmptySearchresult_Path, driver),
				"'Search Results' section is not blank");
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 3)
	public void PL_TC_2_5_2_1_2() throws IOException, InterruptedException {

		// Ability to select create new type and click add with appropriate page
		// opening

		login(UserName, Password);
		ManageInventoryNavigation();
		CreatePart = Part8 + RandomInt();
		Createdpartlist.add(CreatePart);
		Click(locatorType, MI_New_CreateNewType_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Print On-Demand"), driver);
		Click(locatorType, MI_New_Add_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_NewItem_header_Path, driver),
				"'Manage Inventory Item: New Item' is not displayed");
		Type(locatorType, MI_Gen_Formnumber_Path, CreatePart, driver);
		Type(locatorType, MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"), driver);
		// Type(locatorType, MI_Gen_txtPredecessor_Path, Part8 + "1", driver);
		Type(locatorType, MI_Gen_txtTitle_Path, "QA TEST", driver);
		Type(locatorType, MI_Gen_txtDescription_Path, "THIS IS A QA TEST", driver);
		Type(locatorType, MI_Gen_txtKeyWords_Path, "TEST", driver);
		Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Print On-Demand"), driver);
		Assert.assertTrue(Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equals("1"),
				"The number '1' is not pre-populated in 'Units Per Pack' field ");
		Click(locatorType, MI_Gen_Collateraltype_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Folder"), driver);
		Assert.assertTrue(Get_Attribute(locatorType, MI_Gen_FormCostCenter_Path, "value", driver).equals("- Select  -"),
				" 'Select' in  'Form Cost Center' drop down is not displayed.");
		Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_FormCostCenter_63208_AN_EDJ_Path, driver);
		Click(locatorType, MI_Gen_FormCostCenter_Arrow_Path, driver);
		Click(locatorType, MI_Gen_ProductName_List_Input_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_ProductName_AdvantageChoice_Path, driver);
		Click(locatorType, MI_Gen_ProductName_AssetBuildert_Path, driver);
		Click(locatorType, MI_Gen_ProductName_List_Arrow_Path, driver);
		Thread_Sleep(1000);

		Click(locatorType, MI_Gen_ProductLineType_List_Input_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_ProductLineType_Company_Path, driver);
		Click(locatorType, MI_Gen_ProductLineType_List_Arrow_Path, driver);
		Click(locatorType, MI_Gen_UserKitContainer_Path, driver);
		Thread_Sleep(1000);

		Click(locatorType, MI_Gen_BusinessLine_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Annuity"), driver);

		Type(locatorType, MI_Gen_EffectiveDate_dateInput_Path, Get_Futuredate("MM/dd/YYYY"), driver);

		Click(locatorType, MI_Gen_Viewability_Input_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Not Viewable"), driver);

		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Createdpartlist.add(CreatePart);
		Reporter.log("Created New Part Name is '" + CreatePart + "'");
		Thread_Sleep(2000);
		Click(locatorType, MI_RulesTab_Path, driver);
		Type(locatorType, MI_Rules_ObsoleteDate_Path, Get_Todaydate("MM/dd/YYYY HH:mm:ss"), driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Savepopup_Message_Path, driver),
				" 'Successfully Saved!' message is not displayed in pop-up window");
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(enabled = false, priority = 4)
	public void PL_TC_2_5_2_1_3() throws IOException, InterruptedException {

		// Validate that you can copy from the create new item grid

		login(UserName, Password);
		CreatePart = Part6 + "_" + RandomInt();
		Createdpartlist.add(CreatePart);
		ManageInventoryNavigation();
		Type(locatorType, MI_New_FormNoToCopy_Path, Part32, driver);
		Thread_Sleep(2000);
		Click(locatorType, MI_New_Copy_Btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, MI_Gen_NewItem_headerpart_Path, driver).contains("Copy Of QA_TESTNEWITEM_4"),
				"'Manage Inventory Item: <Copy Of QA_TESTNEWITEM_4xxx : QA TEST is not displayed");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtPredecessor_Path, driver),
				" 'Predecessor' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtPredecessor_Path, "value", driver)
						.equalsIgnoreCase("QA_TESTNEWITEM_4"),
				"'Predecessor' field is not pre-populated with <QA_TESTNEWITEM_4>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtTitle_Path, driver),
				" 'Predecessor' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtTitle_Path, "value", driver).toUpperCase().contains("QA TEST"),
				"'Title' field is not pre-populated with <QA TEST>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtDescription_Path, driver),
				" 'Description' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtDescription_Path, "value", driver).toUpperCase()
						.contains("THIS IS A QA TEST"),
				"'Description' field is not pre-populated with <THIS IS A QA TEST>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtKeyWords_Path, driver),
				" 'KeyWord' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).toUpperCase().contains("TEST"),
				"'Keywords' field is not pre-populated with <TEST>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_InventoryType_DropDown_Path, driver),
				"'Inventory Type' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver)
						.equalsIgnoreCase("Print On-Demand"),
				" 'Inventory Type' drop-down is not pre-populated with <Print On-Demand> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_UnitsPerPack_Path, driver),
				"  'Units Per Pack' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Collateraltype_Path, driver),
				" 'Collateral Type' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver).equalsIgnoreCase("Brochure"),
				"'Collateral Type' drop-down is not pre-populated with<Brochure> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_FormCostCenter_Path, driver),
				" 'Form Cost Center' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_FormCostCenter_Path, "value", driver)
						.equalsIgnoreCase("63208 AN-EDJ"),
				"'Form Cost Center' drop-down is not pre-populated with <63208 AN-EDJ> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ProductName_List_Input_Path, driver),
				"'Product Name' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
						.equalsIgnoreCase("Advantage Choice UL, Asset Builder"),
				" 'Product Name' drop-down is not pre-populated with <Advantage Choice UL, Asset Builder>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_UserKitContainer_Path, driver),
				" 'User Kit Container' field is not displayed");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_BusinessLine_Path, driver),
				" 'Business Line' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_BusinessLine_Path, "value", driver).equalsIgnoreCase("Annuity"),
				"'Business Line' drop-down is pre-populated with <Annuity>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Viewability_Input_Path, driver),
				" 'Viewability'drop-down is not displayed ");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, "value", driver)
						.equalsIgnoreCase("Not Viewable"),
				"'Viewability' drop-down is pre-populated with <Not Viewable>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"'Fulfillment' check box is not displayed in view In");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkMarketing_Path, driver),
				"'Marketing' check box is not displayed view In");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkBulkOrder_Path, driver),
				"'Bulk Order' check box is not displayed view In");

		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"'Fulfillment' check box is not pre Checked in view In ");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkBulkOrder_Path, driver),
				"'Bulk Order' check box is not pre Checked in view In ");
		Type(locatorType, MI_Gen_Formnumber_Path, CreatePart, driver);
		Type(locatorType, MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"), driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Savepopup_Message_Path, driver),
				" 'Successfully Saved!' message is not displayed in pop-up window");
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Reporter.log("Created New Part Name is '" + CreatePart + "'");
		// softAssert1.assertAll();();

		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(CreatePart), driver),
				CreatePart + " is not displayed in the 'Search Results' section");
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		// Type(locatorType, MI_Rules_ObsoleteDate_Path,
		// Get_Futuredate("MM/dd/YYYY"), driver);
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(enabled = false, priority = 1)
	public void PL_TC_2_5_2_1_4() throws IOException, InterruptedException {

		// Validate that a part can be copied from the search results table

		login(UserName, Password);
		CreatePart = Part6 + "_" + RandomInt();
		Createdpartlist.add(CreatePart);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part11, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(Part11), driver),
				Part11 + " is not displayed in the 'Search Results' section");
		Click(locatorType, MI_SearchresultsCopy(Part11), driver);
		// Assert.assertTrue(isAlertPresent(), "Copy pop up is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Copyalert_Path, driver), "Copy Alert is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_CopyalertTitle_Path, driver),
				"'Copy?' is not displayed in the Copy pop up");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_CopyalertOK_Path, driver),
				"OK button is not displayed in the Copy alert");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_CopyalertCancel_Path, driver),
				"Cancel button is not displayed in the Copy alert");
		Click(locatorType, MI_CopyalertOK_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Gen_NewItem_headerpart_Path, driver).contains("Copy Of"),
				" 'General' tab contains text: 'Manage Inventory Item: <Copy Of QA_Copy_SearchResults_XXX : QA TEST>");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtPredecessor_Path, driver),
				" 'Predecessor' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtPredecessor_Path, "value", driver)
						.equalsIgnoreCase("QA_Copy_SearchResults_X"),
				"'Predecessor' field is not pre-populated with QA_Copy_SearchResults_X from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtTitle_Path, driver),
				" 'Predecessor' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtTitle_Path, "value", driver).equalsIgnoreCase("QA TEST"),
				"'Title' field is not pre-populated with <QA TEST>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtDescription_Path, driver),
				" 'Description' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtDescription_Path, "value", driver)
						.equalsIgnoreCase("THIS IS A QA TEST"),
				"'Description' field is not pre-populated with <THIS IS A QA TEST>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtKeyWords_Path, driver),
				" 'KeyWord' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).equalsIgnoreCase("TEST"),
				"'Keywords' field is not pre-populated with <TEST>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_InventoryType_DropDown_Path, driver),
				"'Inventory Type' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver)
						.equalsIgnoreCase("Print On-Demand"),
				" 'Inventory Type' drop-down is not pre-populated with <Print On-Demand> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_UnitsPerPack_Path, driver),
				"  'Units Per Pack' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equalsIgnoreCase("1"),
				"'Units Per Pack' field is not pre-populated with <1> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Collateraltype_Path, driver),
				" 'Collateral Type' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver).equalsIgnoreCase("Brochure"),
				"'Collateral Type' drop-down is not pre-populated with<Brochure> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_FormCostCenter_Path, driver),
				" 'Form Cost Center' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_FormCostCenter_Path, "value", driver).equalsIgnoreCase("- Select  -"),
				"'Form Cost Center' drop-down is not pre-populated with <-Select-> from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ProductName_List_Input_Path, driver),
				"'Product Name' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
						.equalsIgnoreCase("Advantage Choice UL, Asset Builder"),
				" 'Product Name' drop-down is not pre-populated with <Advantage Choice UL, Asset Builder>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_UserKitContainer_Path, driver),
				" 'User Kit Container' field is not displayed");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_BusinessLine_Path, driver),
				" 'Business Line' drop-down is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_BusinessLine_Path, "value", driver).equalsIgnoreCase("Annuity"),
				"'Business Line' drop-down is pre-populated with <Annuity>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Viewability_Input_Path, driver),
				" 'Viewability'drop-down is not displayed ");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, "value", driver)
						.equalsIgnoreCase("Not Viewable"),
				"'Viewability' drop-down is pre-populated with <Not Viewable>from original part");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"'Fulfillment' check box is not displayed in view In");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkMarketing_Path, driver),
				"'Marketing' check box is not displayed view In");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkBulkOrder_Path, driver),
				"'Bulk Order' check box is not displayed view In");

		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"'Fulfillment' check box is not pre Checked in view In ");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkBulkOrder_Path, driver),
				"'Bulk Order' check box is not pre Checked in view In ");

		Type(locatorType, MI_Gen_Formnumber_Path, CreatePart, driver);
		Type(locatorType, MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"), driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Savepopup_Message_Path, driver),
				" 'Successfully Saved!' message is not displayed in pop-up window");
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Reporter.log("Created New Part Name is '" + CreatePart + "'");
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(CreatePart), driver),
				CreatePart + " is not displayed in the 'Search Results' section");
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Reporter.log(CreatePart + " is Obsoleted");

	}

	@Test(enabled = false, priority = 6)
	public void PL_TC_2_5_2_1_5() throws IOException, InterruptedException {

		// Ensure all the Search items fields are working and appropriate search
		// results appear

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part1, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(Part1), driver),
				Part1 + " is not displayed in the 'Search Results' section");
		Click(locatorType, MI_Clearbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, MI_SI_FormNo_Path, "value", driver).isEmpty(),
				"Form No field is not blank after cleared");

		Type(locatorType, MI_SI_Title_Path, "QA TEST", driver);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Title1_Path, driver).toLowerCase().contains("qa test"),
				"Record with 'QA TEST'  is not displayed in the 'Search Results' grid");
		Click(locatorType, MI_Clearbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, MI_SI_Title_Path, "value", driver).isEmpty(),
				"Title field is not blank after cleared");

		Type(locatorType, MI_SI_Keyword_Path, "QA", driver);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).toLowerCase().contains("qa"),
				"'QA' is not displayed in the 'Keyword' field");
		Switch_Old_Tab(driver);

		ManageInventoryNavigation();

		Click(locatorType, MI_SI_InventoryType_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Print On-Demand"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Type1_Path, driver).contains("Print On-Demand"),
				"Parts in 'Search Results' grid does not have 'Print On-Demand' in 'Type' column");
		Click(locatorType, MI_Clearbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(
				Get_Attribute(locatorType, MI_SI_InventoryType_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");

		Click(locatorType, MI_SI_CollateralType_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value("Book"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver).equalsIgnoreCase("Book"),
				"'Book' is not displayed in the 'Collateral' field");
		Switch_Old_Tab(driver);
		ManageInventoryNavigation();

		Click(locatorType, MI_SI_Category_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Annuities"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Isselected(locatorType, MI_Gen_Category_Annuties_Path, driver),
				"'Annuities' is not selected in the 'Category' field");
		Switch_Old_Tab(driver);
		ManageInventoryNavigation();

		Click(locatorType, MI_SI_UserGroups_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value("Advisor"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Advisor_Path, driver),
				"'Advisor' check-box is not selected as 'User Group'");
		Switch_Old_Tab(driver);
		ManageInventoryNavigation();

		Assert.assertTrue(Get_Attribute(locatorType, MI_SI_Active_Path, "value", driver).equalsIgnoreCase("Active"),
				"'Active' is pre papulated in the 'Active drop'down");
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Thread_Sleep(1000);
		Assert.assertTrue(Get_Text(locatorType, MI_Status1_Path, driver).equalsIgnoreCase("A"),
				"In 'Search Results' grid have 'A' in 'Status' Column is not displayed in the first row ");
		Click(locatorType, MI_Clearbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_Active_Path, "value", driver).equalsIgnoreCase("Active"),
				"'Active' is not displayed in 'Active' field after clicked clear button");

		Click(locatorType, MI_SI_UnitsOnHand_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Out of Stock"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Onhand1_Path, driver).equals("0"),
				"Results in 'Search Results' grid does not have '0' in 'On Hand' column");
		Click(locatorType, MI_Clearbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_UnitsOnHand_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Units On Hand' field");

		Click(locatorType, MI_SI_ProductArea_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value("Advantage Choice UL"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		Thread_Sleep(500);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		// System.out.println((Get_Attribute(locatorType,
		// MI_Gen_ProductName_List_Input_Path, "value", driver)));
		Assert.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
				.contains("Advantage Choice UL"), " 'Advantage Choice UL' displays in 'Product Area' drop-down");
		Switch_Old_Tab(driver);
		ManageInventoryNavigation();

		Click(locatorType, MI_SI_Programs_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("About Protective Life"), driver);
		Wait_ajax();
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(
				Get_Attribute(locatorType, MI_SI_Programs_Path, "value", driver).contains("About Protective Life"),
				" 'About Protective Life' displays in 'Product Area' drop-down");
		Switch_Old_Tab(driver);
		ManageInventoryNavigation();

		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 7)
	public void PL_TC_2_5_2_1_6() throws IOException, InterruptedException {

		// Ensure all the Search items clear button is working and appropriate
		// search results appear

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, "QA_Multifunction", driver);
		Type(locatorType, MI_SI_Title_Path, "QA TEST", driver);
		Type(locatorType, MI_SI_Keyword_Path, "QA", driver);

		Click(locatorType, MI_SI_InventoryType_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Electronic Only"), driver);
		Click(locatorType, MI_SI_InventoryType_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_CollateralType_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Book"), driver);
		Click(locatorType, MI_SI_CollateralType_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_Category_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Annuities"), driver);
		Click(locatorType, MI_SI_Category_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_UserGroups_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Advisor"), driver);
		Click(locatorType, MI_SI_UserGroups_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_Active_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Active"), driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_UnitsOnHand_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Out of Stock"), driver);
		Click(locatorType, MI_SI_UnitsOnHand_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_ProductArea_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Advantage Choice UL"), driver);
		Click(locatorType, MI_SI_ProductArea_Icon_Path, driver);
		Thread_Sleep(500);

		Click(locatorType, MI_SI_Programs_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("About Protective Life"), driver);
		Click(locatorType, MI_SI_Programs_Icon_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Clearbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_SI_FormNo_Path, "value", driver).isEmpty(),
				" 'Form No field is not  blank after cleared");

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_SI_Title_Path, "value", driver).isEmpty(),
				"'Title field is not  blank after cleared");

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_SI_Keyword_Path, "value", driver).isEmpty(),
				"'Keyword field is not blank after cleared");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_InventoryType_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" 'Inventory Type' drop-down's is not set default value is '- Any -'");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_CollateralType_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" 'Collateral Type' drop-down's is not set default value is '- Any -'");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_Category_Path, "value", driver).equalsIgnoreCase("- Any -"),
				" 'Category' drop-down's is not set default value is '- Any -'");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_UserGroups_Path, "value", driver).equalsIgnoreCase("- Any -"),
				"'User Groups' drop-down's is not set default value is '- Any -'");

		// softAssert1.assertTrue(Get_Attribute(locatorType, MI_SI_Active_Path,
		// "value", driver).equalsIgnoreCase("Active"),
		// "'Status' drop-down's is not set default value is 'Active'");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_UnitsOnHand_Path, "value", driver).equalsIgnoreCase("- Any -"),
				"'Units On Hand' drop-down's is not set default value is 'Any'");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_ProductArea_Path, "value", driver).equalsIgnoreCase("- Any -"),
				"'Product Area' drop-down's is not set default value is '- Any -'");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_SI_Programs_Path, "value", driver).equalsIgnoreCase("- Please Select -"),
				"'Sales Concepts/ Programs' drop-down's is not set default value is '- Please Select -'");
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 8)
	public void PL_TC_2_5_2_1_7() throws IOException, InterruptedException {

		// Ability to Click the edit button

		login(UserName, Password);
		ManageInventoryNavigation();

		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		String Emptysearchpartname = Get_Text(locatorType, MI_FormNumber1_Path, driver);
		System.out.println(Emptysearchpartname);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, MI_Gen_Formnumber_Path, "value", driver)
				.equalsIgnoreCase(Emptysearchpartname), "'General' tab is not displays for selected part");
		Switch_Old_Tab(driver);

	}

	@Test(enabled = false, priority = 9)
	public void PL_TC_2_5_2_2_1() throws IOException, InterruptedException {

		// Validate all fields in general tab appear and have all tabs enabled

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part12, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_Edit1_Path, driver);
		Switch_New_Tab(driver);
		Assert.assertTrue(Get_Attribute(locatorType, MI_Gen_Formnumber_Path, "value", driver).equalsIgnoreCase(Part12),
				"'General' tab is not displays for selected part");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductCode_Path, "value", driver).equalsIgnoreCase(Part12),
				"'QA_NewTestPart' is not displayed in 'Product Code' field");
		softAssert1.assertFalse(Get_Attribute(locatorType, MI_Gen_txtRevisionDate_Path, "value", driver).isEmpty(),
				"<Date:MM/DD/YYYY> is not displayed in 'Revision Date'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Obsolete_Path, driver),
				"'Obsolete Date' field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ObsoleteCalender_Path, driver),
				"'Calendar Icon' is not displayed next to 'Obsolete Date' field ");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ObsoleteClock_Path, driver),
				"'Clock Icon' is not displayed next to 'Obsolete Date' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtPredecessor_Path, driver),
				" 'Predecessor' field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtTitle_Path, "value", driver).equalsIgnoreCase(Part12),
				"'QA_NewTestPart' is not displayed in 'Tilte' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtTitle_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Title' field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtDescription_Path, "value", driver)
						.equalsIgnoreCase("QA_NewTestPart  IS A QA TEST"),
				" 'QA_NewTestPart  IS A QA TEST' is not displayed in 'Description' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtDescription_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Description' field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).equalsIgnoreCase(Part12),
				" 'QA_NewTestPart' is not displayed in 'Key words' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtKeyWords_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Keywords' field");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver)
				.equalsIgnoreCase("Stock"), " 'Stock' is not pre-selected in 'Inventory Type' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_InventoryType_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Inventory Type' field");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equalsIgnoreCase("1"),
				" '1' is not pre-populated in 'Units Per Pack' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_UnitsPerPack_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'UnitsPerPack' field");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver).equalsIgnoreCase("Form"),
				" 'Form' is not pre-selected in 'Collateral Type' dropdown");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Collateraltype_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Collateral type' field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_FormCostCenter_Path, "value", driver).equalsIgnoreCase("- Select  -"),
				" 'Select' is not displayed in 'Form Cost Center' dropdown");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
						.equalsIgnoreCase("Advantage Choice UL, Asset Builder"),
				" 'Advantage Choice UL, Asset Builder' is not pre-selected in 'Product Name' dropdown");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ProductName_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Product Name' field");

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductLineType_List_Input_Path, "value", driver)
				.equalsIgnoreCase("Company"), " 'Product Line' dropdown is not displayed  ");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ProductLineType_Asterisk_Path, driver),
				"Red asterisk is not displayed for 'Product Line Type' field");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Programs_Input_Path, "value", driver)
						.equalsIgnoreCase("- Please Select -"),
				" '- Please Select -' is not displayed in 'Sales Concepts/Programs' dropdown");

		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Gen_UserKitContainer_Path, driver),
				" 'User Kit Container' checkbox is checked");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_BusinessLine_Path, "value", driver).equalsIgnoreCase("Annuity"),
				" 'Annuity' is not pre-selected in 'Business Line' dropdown");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_BusinessLine_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Business Line' field");

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, "value", driver)
				.equalsIgnoreCase("Orderable"), "'Orderable' is not pre-selected in 'Viewability' dropdown");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Viewability_Asterisk_Path, driver),
				" Red asterisk is not displayed for 'Viewability' field");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_EffectiveDate_dateInput_Path, driver),
				"'Effective Date' field is not displayed ");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_EffectiveDate_Calender_Path, driver),
				" 'Calendar' icon is not displayed near 'Effective Date' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_EffectiveDate_Clock_Path, driver),
				"'Time' icon is not displayed near 'Effective Date' field");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ViewIn_Path, driver),
				"'View In' field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkFulfillment_Path, driver),
				" 'Fulfillment' checkbox is not displayed for 'View In' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkMarketing_Path, driver),
				"'Marketing Campaigns' checkbox is not displayed for 'View In' field ");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_chkBulkOrder_Path, driver),
				"'Bulk Order' checkbox is not displayed for 'View In' field ");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Usergrps_Checkbox_Path, driver),
				"'User Groups' checkbox is not displayed for 'View In' field");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtNotes_Path, driver),
				"'Notes' field is not displayed");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_CategoryTitle_Path, driver),
				"'Categories' section title is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Caegorysetion_Path, driver),
				"'Categories' section is not displayed");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ItemUsagesection_Path, driver),
				"'Item Usage' section is not displayed");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Cancel_Path, driver),
				"Cancel button is not displays on the bottom right side of the page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Next_Path, driver),
				"Next button is not displays on the bottom right side of the page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Save_Path, driver),
				"Save button is not displays on the bottom left side of the page");

		Click(locatorType, MI_AttachmentTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Attach_Onlinesample_Browse_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_AttachmentTitle_Path, driver),
				"Attachment Tab is not displayed");

		Click(locatorType, MI_Pricing_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_Pricing_UnitPrice_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_UnitPrice_Path, driver),
				"Unit Price is not displays on the 'Pricing' page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_PricingDetails_Path, driver),
				"Pricing Details Tab is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_FlatPricing_Path, driver),
				"'Flat Pricing' is not displayed on the 'Pricing' page");

		Click(locatorType, MI_Notification_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Notifi_Notificationck_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Notifi_Title_Path, driver),
				"Notification Header is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Notifi_Addresses_Path, driver),
				"Notification Addresses is not displayed");

		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Title_Path, driver),
				" 'Rules' section is not displayed on the Rules tab page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_DefaultMaxOrderQty_Path, driver),
				" 'Default Max Order Qty' section is not displayed on the Rules tab page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_RequireRelatedItems_Path, driver),
				" 'Require Related Items' section is not displayed on the Rules tab page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_StateMode_Path, driver),
				" 'State Mode' section is not displayed on the Rules tab page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_FirmMode_Path, driver),
				" 'Firm Mode' section is not displayed on the Rules tab page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Maxordertitle_Path, driver),
				" 'Max order title' section is not displayed on the Rules tab page");

		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_MetricsHeader_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_MetricsTable_Path, driver),
				"'Metrics' on the page is not displayed");

		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_ChangeHistoryAction1_Path, driver),
				" 'Change History' is not displayed on the page");

		Click(locatorType, MI_PageflexTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Pageflex_Savebtn_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_Jobnamefield_Path, driver),
				"'Pageflex Job Name' is not displayed on the page");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_Inventoryoption_Path, driver),
				"'Inventory Options' is not displayed on the page");
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 10)

	public void PL_TC_2_5_2_2_2() throws IOException, InterruptedException {

		// Validate all fields that are not greyed out can be edited and saved

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part13, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part13), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductCode_Path, "value", driver).equalsIgnoreCase(Part14),
				"'QA_NewTestPart_1' is not displayed in 'Product Code' field");

		softAssert1.assertFalse(Get_Attribute(locatorType, MI_Gen_txtRevisionDate_Path, "value", driver).isEmpty(),
				"<Date:MM/DD/YYYY> is not displayed in 'Revision Date'");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtPredecessor_Path, "value", driver).trim().equalsIgnoreCase(Part14),
				Part14 + " is not displayed in 'Predecessor Field'");

		Type(locatorType, MI_Gen_txtPredecessor_Path, Part14 + " QA TEST", driver);

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtTitle_Path, "value", driver).contains("QA TEST"),
				"'QA TEST' is not displayed in 'Tilte' field ");
		Type(locatorType, MI_Gen_txtTitle_Path, "QA TEST 123", driver);

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).contains("TEST"),
				"'TEST' is not displayed in 'Keyword' field ");
		Type(locatorType, MI_Gen_txtKeyWords_Path, "TEST QA", driver);

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver)
				.equalsIgnoreCase("Stock"), "'Stock' is not pre-selected in 'Inventory Type' field");
		Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Print On-Demand"), driver);
		Thread_Sleep(500);

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equalsIgnoreCase("1"),
				" '1' is not pre-populated in 'Units Per Pack' field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver).equalsIgnoreCase("Form"),
				" 'Form' is not pre-selected in 'Collateral Type' dropdown");
		Click(locatorType, MI_Gen_Collateraltype_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Folder"), driver);

		/*
		 * softAssert1.assertTrue( Get_Attribute(locatorType,
		 * MI_Gen_FormCostCenter_Path, "value",
		 * driver).equalsIgnoreCase("- Select  -"),
		 * " 'select' is not displayed in 'Form Cost Center' dropdown");
		 * Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		 * Thread_Sleep(500); Click(locatorType,
		 * MI_Gen_FormCostCenter_Checkall_Path, driver);
		 */

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
						.equalsIgnoreCase("Advantage Choice UL, Asset Builder"),
				" 'Advantage Choice UL, Asset Builder' is not pre-selected in 'Product Name' dropdown");

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductLineType_List_Input_Path, "value", driver)
				.equalsIgnoreCase("Company"), " 'Product Line' dropdown is not displayed  ");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Programs_Input_Path, "value", driver)
						.equalsIgnoreCase("About Protective Life"),
				" 'About Protective Life' is not displayed in 'Sales Concepts/Programs' dropdown");

		Click(locatorType, MI_Gen_UserKitContainer_Path, driver);
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_BusinessLine_Path, "value", driver).equalsIgnoreCase("Annuity"),
				" 'Annuity' is not pre-selected in 'Business Line' dropdown");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, "value", driver)
				.equalsIgnoreCase("Orderable"), "'Orderable' is pre-selected in 'Viewability' dropdown");
		Type(locatorType, MI_Gen_EffectiveDate_dateInput_Path, Get_Futuredate("MM/dd/yyy"), driver);

		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"Fullfilment checkbox is not checked in View In");

		if (Element_Isselected(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver) == true) {
			Click(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver);

		}
		softAssert1.assertAll();
		Type(locatorType, MI_Gen_txtNotes_Path, "This is the Notes", driver);
		softAssert1.assertAll();
		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Type(locatorType, MI_Gen_txtPredecessor_Path, Part14, driver);
		Type(locatorType, MI_Gen_txtTitle_Path, "QA TEST", driver);
		Type(locatorType, MI_Gen_txtKeyWords_Path, "TEST", driver);
		Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Stock"), driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_Collateraltype_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Form"), driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Switch_Old_Tab(driver);

	}

	@Test(enabled = false, priority = 11)
	public void PL_TC_2_5_2_2_3() throws IOException, InterruptedException {

		// Validate all fields that are not greyed out can have the edits undone
		// and saved

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part13, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part13), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);

		Type(locatorType, MI_Gen_txtPredecessor_Path, Part14, driver);

		Type(locatorType, MI_Gen_txtTitle_Path, "QA TEST", driver);
		Type(locatorType, MI_Gen_txtDescription_Path, "THIS IS A QA TEST DESCRIPTION", driver);

		Type(locatorType, MI_Gen_txtKeyWords_Path, "TEST", driver);

		Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Stock"), driver);

		Click(locatorType, MI_Gen_Collateraltype_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Form"), driver);
		/*
		 * Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		 * Thread_Sleep(500); Click(locatorType,
		 * MI_Gen_FormCostCenter_Checkall_Path, driver); Click(locatorType,
		 * MI_Gen_FormCostCenter_Arrow_Path, driver); Thread_Sleep(500);
		 */

		if (Element_Isselected(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver) == false) {
			Click(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver);

		}

		Type(locatorType, MI_Gen_txtNotes_Path, "QA Test", driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Savepopup_Message_Path, driver),
				" 'Successfully Saved!' message is not displayed in pop-up window");
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Switch_Old_Tab(driver);

	}

	@Test(enabled = false, priority = 12)
	public void PL_TC_2_5_2_2_4() throws IOException, InterruptedException {

		// Validate appropriate changes are recorded in change history: for
		// edits

		login(UserName, Password);
		ManageInventoryNavigation();
		String Changehistory = "";
		String Inputtext = "";

		Type(locatorType, MI_SI_FormNo_Path, Part14, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(800);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(800);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part14), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_txtPredecessor_Path, "value", driver);
		Inputtext = "QA_NewTestPart QA Test" + RandomInt();
		Type(locatorType, MI_Gen_txtPredecessor_Path, Inputtext, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
						.equalsIgnoreCase("Predecessor changed from '" + Changehistory + "' to '" + Inputtext + "'"),
				"Predecessor changed from '" + Changehistory + "' to '" + Inputtext
						+ "' is not displayed in the change history tab");
		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_txtDescription_Path, "value", driver);
		Inputtext = "QA TEST" + RandomInt();
		Type(locatorType, MI_Gen_txtDescription_Path, Inputtext, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver).equalsIgnoreCase(
						"LongDescription changed from '" + Changehistory + "' to '" + Inputtext + "'"),
				"LongDescription changed from '" + Changehistory + "' to '" + Inputtext
						+ "' is not displayed in the change history tab");

		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver);
		Inputtext = "TEST" + RandomInt();
		Type(locatorType, MI_Gen_txtKeyWords_Path, Inputtext, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
						.equalsIgnoreCase("KeyWords changed from '" + Changehistory + "' to '" + Inputtext + "'"),
				"KeyWords changed from '" + Changehistory + "' to '" + Inputtext
						+ "' is not displayed in the change history tab");

		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver);
		if (Changehistory.contains("Stock")) {
			Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Print On-Demand";
			Click(locatorType, li_value(Inputtext), driver);
			Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
			softAssert1.assertTrue(
					Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver).equalsIgnoreCase(
							"InventoryTypeID changed from '" + Changehistory + "' to '" + Inputtext + "'"),
					"InventoryTypeID changed from '" + Changehistory + "' to '" + Inputtext
							+ "' is not displayed in the change history tab");
		} else {
			Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Stock";
			Click(locatorType, li_value(Inputtext), driver);
			Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
			softAssert1.assertTrue(
					Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver).equalsIgnoreCase(
							"InventoryTypeID changed from '" + Changehistory + "' to '" + Inputtext + "'"),
					"InventoryTypeID changed from '" + Changehistory + "' to '" + Inputtext
							+ "' is not displayed in the change history tab");

		}
		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver);
		System.out.println(Changehistory);
		if (Changehistory.equalsIgnoreCase("Form")) {
			Click(locatorType, MI_Gen_Collateraltype_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Folder";
			Click(locatorType, li_value(Inputtext), driver);
			Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
			System.out.println("DocumentType changed from '" + Changehistory + "' to '" + Inputtext + "'");
			System.out.println(Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver));
			softAssert1.assertTrue(
					Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver).equalsIgnoreCase(
							"DocumentType changed from '" + Changehistory + "' to '" + Inputtext + "'"),
					"DocumentType changed from '" + Changehistory + "' to '" + Inputtext
							+ "' is not displayed in the change history tab");
		} else {
			Click(locatorType, MI_Gen_Collateraltype_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Form";
			Click(locatorType, li_value(Inputtext), driver);
			Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
			System.out.println("DocumentType changed from '" + Changehistory + "' to '" + Inputtext + "'");
			softAssert1.assertTrue(
					Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver).equalsIgnoreCase(
							"DocumentType changed from '" + Changehistory + "' to '" + Inputtext + "'"),
					"DocumentType changed from '" + Changehistory + "' to '" + Inputtext
							+ "' is not displayed in the change history tab");

		}
		softAssert1.assertAll();

		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_EffectiveDate_dateInput_Path, "value", driver);
		if (Changehistory.isEmpty()) {
			Changehistory = "12:00:00 AM";

		}
		System.out.println(Changehistory);
		Inputtext = Get_Futuredate("M/d/YYYY");
		Type(locatorType, MI_Gen_EffectiveDate_dateInput_Path, Inputtext + " 12:00 AM", driver);
		Thread_Sleep(2000);
		Click(locatorType, MI_Gen_Collateraltype_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, MI_Gen_Collateraltype_Arrow_Path, driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
		// driver).contains("EffectiveDate changed from"),
		// "EffectiveDate changed from '" + Changehistory + "' to '" + Inputtext
		// + "' is not displayed in the change history tab");
		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_txtNotes_Path, "value", driver);
		System.out.println(Changehistory);
		Inputtext = "QA TEST" + RandomInt();
		Type(locatorType, MI_Gen_txtNotes_Path, Inputtext, driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
						.equalsIgnoreCase("Notes changed from '" + Changehistory + "' to '" + Inputtext + "'"),
				"Notes changed from '" + Changehistory + "' to '" + Inputtext
						+ "' is not displayed in the change history tab");

		Click(locatorType, MI_Gen_General_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		/*
		 * Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		 * Thread_Sleep(500);
		 * 
		 * if (Element_Isselected(locatorType,
		 * MI_Gen_FormCostCenter_Checkall_Path, driver)) {
		 * 
		 * Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		 * Click(locatorType, MI_Gen_FormCostCenter_63208_AN_EDJ_Path, driver);
		 * } else {
		 * 
		 * Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		 * Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		 * Click(locatorType, MI_Gen_FormCostCenter_63208_AN_EDJ_Path, driver);
		 * 
		 * }
		 * 
		 * Click(locatorType, MI_Gen_Save_Path, driver);
		 * ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path,
		 * driver); Click(locatorType, MI_Savepopup_Ok_Path, driver);
		 * 
		 * Changehistory = Get_Attribute(locatorType,
		 * MI_Gen_FormCostCenter_Path, "value", driver);
		 * System.out.println(Changehistory); Inputtext =
		 * "63208 AN-EDJ, 63208 AN-INS, 63208 LI-IND, 63208 LI-PD, 63208-LI-INS, 63209 AN-EDJ, 63210 AN-EDJ, 63211 AN-EDJ, 63212 AN-EDJ, 63213 AN-EDJ, 63214, 63216 AN-EDJ, 63217 AN-EDJ, 63218 AN-EDJ"
		 * ; Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		 * Thread_Sleep(500); Click(locatorType,
		 * MI_Gen_FormCostCenter_Checkall_Path, driver); Click(locatorType,
		 * MI_Gen_Save_Path, driver);
		 * ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path,
		 * driver); Click(locatorType, MI_Savepopup_Ok_Path, driver);
		 * Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		 * ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		 * driver); softAssert1.assertTrue( Get_Text(locatorType,
		 * MI_ChangeHistoryAction1_Path, driver)
		 * .equalsIgnoreCase("FormCostCenter changed from '" + Changehistory +
		 * "' to '" + Inputtext + "'"), "FormCostCenter changed from '" +
		 * Changehistory + "' to '" + Inputtext +
		 * "' is not displayed in the change history tab");
		 * 
		 * Click(locatorType, MI_Gen_General_Path, driver);
		 * ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		 * driver);
		 */

		if (Element_Is_Displayed(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver)) {

		} else {

			Click(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver);
			Click(locatorType, MI_Gen_Save_Path, driver);
			ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
			Click(locatorType, MI_Savepopup_Ok_Path, driver);

		}

		Click(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
						.equalsIgnoreCase("UserGroups Added: {-None-}; UserGroups Removed: {BGA}"),
				"UserGroups Added: {-None-}; UserGroups Removed: {BGA} is not displayed in the change history tab");
		Switch_Old_Tab(driver);
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 13)
	public void PL_TC_2_5_2_2_5() throws IOException, InterruptedException {

		// Validate appropriate changes are recorded in change history: for undo
		// changes

		login(UserName, Password);
		ManageInventoryNavigation();

		Type(locatorType, MI_SI_FormNo_Path, Part14, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part14), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);

		String Changehistory = "";
		String Inputtext = "QA_NewTestPart QA Test" + RandomInt();
		Type(locatorType, MI_Gen_txtPredecessor_Path, Inputtext, driver);
		// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
		// .equalsIgnoreCase("Predecessor changed from '" + Changehistory + "'
		// to '" + Inputtext + "'"),
		// "Predecessor changed from '" + Changehistory + "' to '" + Inputtext
		// + "' is not displayed in the change history tab");
		//
		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		// Changehistory = Get_Attribute(locatorType,
		// MI_Gen_txtDescription_Path, "value", driver);
		Inputtext = "QA TEST" + RandomInt();
		Type(locatorType, MI_Gen_txtDescription_Path, Inputtext, driver);
		// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
		// driver).equalsIgnoreCase(
		// "LongDescription changed from '" + Changehistory + "' to '" +
		// Inputtext + "'"),
		// "LongDescription changed from '" + Changehistory + "' to '" +
		// Inputtext
		// + "' is not displayed in the change history tab");
		//
		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		// Changehistory = Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path,
		// "value", driver);
		Inputtext = "TEST" + RandomInt();
		Type(locatorType, MI_Gen_txtKeyWords_Path, Inputtext, driver);
		// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
		// .equalsIgnoreCase("KeyWords changed from '" + Changehistory + "' to
		// '" + Inputtext + "'"),
		// "KeyWords changed from '" + Changehistory + "' to '" + Inputtext
		// + "' is not displayed in the change history tab");
		//
		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver);
		if (Changehistory == "Stock") {
			Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Print On-Demand";
			Click(locatorType, li_value(Inputtext), driver);
			// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			// ExplicitWait_Element_Visible(locatorType,
			// MI_ChangeHistoryTitle_Path, driver);
			// softAssert1.assertTrue(
			// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
			// driver).equalsIgnoreCase(
			// "InventoryTypeID changed from '" + Changehistory + "' to '" +
			// Inputtext + "'"),
			// "InventoryTypeID changed from '" + Changehistory + "' to '" +
			// Inputtext
			// + "' is not displayed in the change history tab");
		} else {
			Click(locatorType, MI_Gen_InventoryType_DropDown_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Stock";
			Click(locatorType, li_value(Inputtext), driver);
			// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			// ExplicitWait_Element_Visible(locatorType,
			// MI_ChangeHistoryTitle_Path, driver);
			// softAssert1.assertTrue(
			// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
			// driver).equalsIgnoreCase(
			// "InventoryTypeID changed from '" + Changehistory + "' to '" +
			// Inputtext + "'"),
			// "InventoryTypeID changed from '" + Changehistory + "' to '" +
			// Inputtext
			// + "' is not displayed in the change history tab");

		}
		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver);
		if (Changehistory == "Form") {
			Click(locatorType, MI_Gen_Collateraltype_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Folder";
			Click(locatorType, li_value(Inputtext), driver);
			// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			// ExplicitWait_Element_Visible(locatorType,
			// MI_ChangeHistoryTitle_Path, driver);
			// softAssert1.assertTrue(
			// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
			// driver).equalsIgnoreCase(
			// "DocumentType changed from '" + Changehistory + "' to '" +
			// Inputtext + "'"),
			// "DocumentType changed from '" + Changehistory + "' to '" +
			// Inputtext
			// + "' is not displayed in the change history tab");
		} else {
			Click(locatorType, MI_Gen_Collateraltype_Path, driver);
			Thread_Sleep(500);
			Inputtext = "Form";
			Click(locatorType, li_value(Inputtext), driver);
			// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			// ExplicitWait_Element_Visible(locatorType,
			// MI_ChangeHistoryTitle_Path, driver);
			// softAssert1.assertTrue(
			// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
			// driver).equalsIgnoreCase(
			// "DocumentType changed from '" + Changehistory + "' to '" +
			// Inputtext + "'"),
			// "DocumentType changed from '" + Changehistory + "' to '" +
			// Inputtext
			// + "' is not displayed in the change history tab");

		}

		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		if (Element_Isselected(locatorType, MI_Gen_UserKitContainer_Path, driver)) {
			Click(locatorType, MI_Gen_UserKitContainer_Path, driver);
			Changehistory = "true";
			Inputtext = "false";
			// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			// ExplicitWait_Element_Visible(locatorType,
			// MI_ChangeHistoryTitle_Path, driver);
			// softAssert1.assertTrue(
			// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
			// driver).equalsIgnoreCase(
			// "IsUserKitContainer changed from '" + Changehistory + "' to '" +
			// Inputtext + "'"),
			// "IsUserKitContainer changed from '" + Changehistory + "' to '" +
			// Inputtext
			// + "' is not displayed in the change history tab");

		} else {
			Click(locatorType, MI_Gen_UserKitContainer_Path, driver);
			Changehistory = "false";
			Inputtext = "true";
			// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
			// ExplicitWait_Element_Visible(locatorType,
			// MI_ChangeHistoryTitle_Path, driver);
			// softAssert1.assertTrue(
			// Get_Text(locatorType, MI_ChangeHistoryAction1_Path,
			// driver).equalsIgnoreCase(
			// "IsUserKitContainer changed from '" + Changehistory + "' to '" +
			// Inputtext + "'"),
			// "IsUserKitContainer changed from '" + Changehistory + "' to '" +
			// Inputtext
			// + "' is not displayed in the change history tab");

		}

		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_EffectiveDate_dateInput_Path, "value", driver);
		Inputtext = Get_Futuredate("d/MM/yyy") + " 12:00 AM";
		Type(locatorType, MI_Gen_txtKeyWords_Path, Inputtext, driver);
		// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
		// .equalsIgnoreCase("EffectiveDate changed from '" + Changehistory + "'
		// to '" + Inputtext + "'"),
		// "EffectiveDate changed from '" + Changehistory + "' to '" + Inputtext
		// + "' is not displayed in the change history tab");

		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		Changehistory = Get_Attribute(locatorType, MI_Gen_txtNotes_Path, "value", driver);
		Inputtext = "QA TEST" + RandomInt();
		Type(locatorType, MI_Gen_txtNotes_Path, Inputtext, driver);
		// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
		// .equalsIgnoreCase("Notes changed from '" + Changehistory + "' to '" +
		// Inputtext + "'"),
		// "Notes changed from '" + Changehistory + "' to '" + Inputtext
		// + "' is not displayed in the change history tab");

		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		// Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		// Thread_Sleep(500);
		//
		// if (Element_Is_Displayed(locatorType,
		// MI_Gen_FormCostCenter_Checkall_Path, driver)) {
		//
		// Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		// Click(locatorType, MI_Gen_FormCostCenter_63208_AN_EDJ_Path, driver);
		// } else {
		//
		// Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		// Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		// Click(locatorType, MI_Gen_FormCostCenter_63208_AN_EDJ_Path, driver);
		//
		// }

		// Click(locatorType, MI_Gen_Save_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path,
		// driver);
		// Click(locatorType, MI_Savepopup_Ok_Path, driver);

		// Changehistory = Get_Attribute(locatorType,
		// MI_Gen_FormCostCenter_Path, "value", driver);
		// Inputtext = "63208 AN-EDJ, 63208 AN-INS, 63208 LI-IND, 63208 LI-PD,
		// 63208-LI-INS, 63209 AN-EDJ, 63210 AN-EDJ, 63211 AN-EDJ, 63212 AN-EDJ,
		// 63213 AN-EDJ, 63214, 63216 AN-EDJ, 63217 AN-EDJ, 63218 AN-EDJ";
		// Click(locatorType, MI_Gen_FormCostCenter_Path, driver);
		// Thread_Sleep(500);
		// Click(locatorType, MI_Gen_FormCostCenter_Checkall_Path, driver);
		// // Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
		// .equalsIgnoreCase("FormCostCenter changed from '" + Changehistory +
		// "' to '" + Inputtext + "'"),
		// "FormCostCenter changed from '" + Changehistory + "' to '" +
		// Inputtext
		// + "' is not displayed in the change history tab");

		// Click(locatorType, MI_Gen_General_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);

		if (Element_Is_Displayed(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver)) {

		} else {

			Click(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver);
			// Click(locatorType, MI_Gen_Save_Path, driver);
			// ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path,
			// driver);
			// Click(locatorType, MI_Savepopup_Ok_Path, driver);

		}

		Click(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver);
		// Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path,
		// driver);
		// softAssert1.assertTrue(
		// Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
		// .equalsIgnoreCase("UserGroups Added: {-None-}; UserGroups Removed:
		// {BGA}"),
		// "UserGroups Added: {-None-}; UserGroups Removed: {BGA} is not
		// displayed in the change history tab");
		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 14)
	public void PL_TC_2_5_2_3_1andPL_TC_2_5_2_3_2andPL_TC_2_5_2_3_3() throws IOException, InterruptedException {

		// Validate all fields in general tab appear and almost all are blank

		login(UserName, Password);
		ManageInventoryNavigation();
		CreatePart = Part12 + "_" + RandomInt();
		Createdpartlist.add(CreatePart);
		System.out.println(CreatePart);
		Click(locatorType, MI_New_CreateNewType_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Print On-Demand"), driver);
		Click(locatorType, MI_New_Add_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_NewItem_header_Path, driver),
				"Manage Inventory Item : New Item' section is not displayed");
		// CreatePart = Part8 + RandomInt();

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Formnumber_Path, "value", driver).isEmpty(),
				"'Form number' field is not blank");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductCode_Path, "value", driver).isEmpty(),
				"'Product Code' field is not blank");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtRevisionDate_Path, "value", driver).equalsIgnoreCase("__/__/____"),
				"'' field is not blank");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Obsolete_Path, "value", driver).isEmpty(),
				"Obsolete'' field is not blank");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtPredecessor_Path, "value", driver).isEmpty(),
				"'Predecessor' field is not blank");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtTitle_Path, "value", driver).isEmpty(),
				"'Title' field is not blank");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtDescription_Path, "value", driver).isEmpty(),
				"'Description' field is not blank");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).isEmpty(),
				"'Keywords' field is not blank");
		// softAssert1.assertTrue(Get_Attribute(locatorType,
		// MI_Gen_Formnumber_Path, "value", driver).isEmpty()," is not
		// displaying for '' field");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver)
				.equalsIgnoreCase("Print On-Demand"), "'Print On-Demand' is not selected in 'Inventory Type' dropdown");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equalsIgnoreCase("1"),
				"'1' is not pre-populated in the Units per pack field");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "'- Please Select -' is not selected in 'Collateral Type");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_FormCostCenter_Path, "value", driver).equalsIgnoreCase("- Select  -"),
				"'- Select -' is not displayed in Form cost center");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
				.equalsIgnoreCase("- Select Product Name(s) -"), "'- Select Product Name(s) -' is not displayed in ");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductLineType_List_Input_Path, "value", driver)
				.equalsIgnoreCase("- Select -"), "'- Select -' is not displayed in Producty line type");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Programs_Input_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "'- Select -' is not displayed in  Programs field");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Gen_UserKitContainer_Path, driver),
				"User Kit Container Checkbox is not selected");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_BusinessLine_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "'- Please Select -' is not displayed in bussiness line");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_Viewability_Input_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"),
				"'- Please Select -' is not displayed in Viewability Drop down");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_EffectiveDate_dateInput_Path, driver),
				"Effective date field is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_EffectiveDate_dateInput_Path, "value", driver).isEmpty(),
				"'Effective date' field is not blank");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"User Kit Container Checkbox is not selected");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Gen_chkMarketing_Path, driver),
				"User Kit Container Checkbox is not selected");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Gen_chkBulkOrder_Path, driver),
				"User Kit Container Checkbox is not selected");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Usergrps_Checkbox_Path, driver),
				"User Groups field are not displayed");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Allstate_Path, driver),
				"All state Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_BGACheckbox_Path, driver),
				"BGA Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_IndependentAgent_Path, driver),
				"Independent Agent Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Advisor_Path, driver),
				"Adivsor Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_DLC_Path, driver),
				"DLC Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_RVP_Path, driver),
				"RVP Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_RDADM_Path, driver),
				"RA, ADM Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Hybrid_Path, driver),
				"Hydrid Wholesaler Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Salesdesk_Path, driver),
				"Salesdesk Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Marketing_Path, driver),
				"Marketing Checkbox is not selected");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_Usergrps_Sysadmin_Path, driver),
				"System Admin Checkbox is not selected");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtNotes_Path, "value", driver).isEmpty(),
				"Notes field is not empty");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Usergrps_Checkbox_Path, driver),
				"User Groups field are not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_Caegorysetion_Path, driver),
				"Category section is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_ItemUsagesection_Path, driver),
				"Item Usage section is not displayed");

		softAssert1.assertAll();

		// Verify that you see error message "* Invalid Form Number!" near the
		// 'Form Number' field

		Click(locatorType, MI_Gen_Next_Path, driver);
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_Formnumber_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_FormNumber_Path, driver)).equalsIgnoreCase("* Invalid FormNumber!"),
				"Form Number error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_RevisionDate_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_RevisionDate_Path, driver))
						.equalsIgnoreCase("* Invalid Revision Date!"),
				"Revision Date error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_txtTitle_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_Title_Path, driver)).equalsIgnoreCase(" * Title is required!"),
				"Title error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_txtDescription_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_Description_Path, driver))
						.equalsIgnoreCase("* Description is Required!"),
				"Description error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_txtKeyWords_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_KeyWords_Path, driver)).equalsIgnoreCase("* Keywords are Required!"),
				"Keywords error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_Collateraltype_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_CollateralType_Path, driver)).equalsIgnoreCase(
						" * Document Type is required!"),
				"Collateral error message is not displayed");
		softAssert1.assertTrue(
				(Get_Text(locatorType, MI_Gen_ProductName_Asterisk_Path, driver)
						+ Get_Text(locatorType, MI_Gen_Err_ProductName_Path, driver)).equalsIgnoreCase("* Required"),
				"Product name error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_ProductLineType_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_ProductLine_Path, driver)).equalsIgnoreCase(
						"* Product Line is required!"),
				"Product line error message is not displayed");
		softAssert1.assertTrue(
				(Get_Text(locatorType, MI_Gen_BusinessLine_Asterisk_Path, driver)
						+ Get_Text(locatorType, MI_Gen_Err_BusinessLine_Path, driver)).equalsIgnoreCase("* Required"),
				"Busoness line error message is not displayed");
		softAssert1.assertTrue((Get_Text(locatorType, MI_Gen_Viewability_Asterisk_Path, driver)
				+ Get_Text(locatorType, MI_Gen_Err_Viewability_Path, driver))
						.equalsIgnoreCase("* Viewability is required!"),
				"Viewability error message is not displayed");
		Type(locatorType, MI_Gen_Formnumber_Path, CreatePart, driver);

		// Validate that filling out all required fields on general tab enable
		// the other tabs

		Type(locatorType, MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MMddYYYY"), driver);
		Type(locatorType, MI_Gen_txtTitle_Path, "QA", driver);
		Type(locatorType, MI_Gen_txtDescription_Path, "QA is Testing", driver);
		Type(locatorType, MI_Gen_txtKeyWords_Path, "QA Test", driver);
		Type(locatorType, MI_Gen_UnitsPerPack_Path, "1", driver);
		Click(locatorType, MI_Gen_Collateraltype_Arrow_Path, driver);
		Thread_Sleep(800);
		Click(locatorType, li_value("Book"), driver);
		Click(locatorType, MI_Gen_ProductName_List_Arrow_Path, driver);
		Thread_Sleep(800);
		Click(locatorType, MI_Gen_ProductName_AdvantageChoice_Path, driver);
		Click(locatorType, MI_Gen_ProductLineType_List_Arrow_Path, driver);
		Thread_Sleep(800);
		Click(locatorType, MI_Gen_ProductLineType_Company_Path, driver);
		Click(locatorType, MI_Gen_BusinessLine_Arrow_Path, driver);
		Thread_Sleep(800);
		Click(locatorType, li_value("Annuity"), driver);
		Click(locatorType, MI_Gen_Viewability_Arrow_Path, driver);
		Thread_Sleep(800);
		Click(locatorType, li_value("Not Viewable"), driver);
		Click(locatorType, MI_Gen_Next_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_AttachmentTitle_Path, driver),
				"Attachments Tab is not displayed");
		Click(locatorType, MI_Pricing_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_UnitPrice_Path, driver),
				"Unit Price header is not displayed");
		Click(locatorType, MI_Pricing_NoCostItemCk_Path, driver);
		Click(locatorType, MI_Notification_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Notifi_Title_Path, driver),
				"Notification Options header  is not displayed");
		Click(locatorType, MI_RulesTab_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Title_Path, driver),
				"Rules header  is not displayed");
		Click(locatorType, MI_MetricsTab_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_MetricsHeader_Path, driver),
				"Metrics header  is not displayed");
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_ChangeHistoryTitle_Path, driver),
				"Change history header  is not displayed");
		Click(locatorType, MI_PageflexTab_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_Jobnamefield_Path, driver),
				"Pageflex Job name  is not displayed");
		// Type(locatorType, value, parameter, driver);
		// Click(locatorType, value, driver);

	}

	@Test(enabled = false, priority = 15, dependsOnMethods = "PL_TC_2_5_2_3_1andPL_TC_2_5_2_3_2andPL_TC_2_5_2_3_3")
	public void PL_TC_2_5_2_3_5() throws IOException, InterruptedException {

		// Validate the all fields in the other tabs appear and most are blank.

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Gen_Next_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Attach_PrintReady_Browse_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PrdThumb_Path, driver),
				"'Product Thumbnail' text is not displayed in the 'Attachments' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PrdThumb_Browse_Path, driver),
				"Browse button is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PrdThumb_Upload_Path, driver),
				"Upload button is not displayed for Product Thumbnail ");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_Onlinesample_Path, driver),
				"'Online Sample' text is not displayed in the 'Attachments' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_Onlinesample_Browse_Path, driver),
				"Browse button is not displayed for the Online Sample ");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_Onlinesampl_Upload_Path, driver),
				"Upload button is not displayed for Online Sample");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_FINRAletter_Path, driver),
				"'Finra letter' text is not displayed in the 'Attachments' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_FINRAletter_BrowsePath, driver),
				"Browse button is not displayed for Finra letter");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_FINRAletter_Upload_Path, driver),
				"Upload button is not displayed for Finra letter");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PrintReady_Path, driver),
				"Print Ready Pdf is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PrintReady_Password_Path, driver),
				"Password field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PrintReady_Browse_Path, driver),
				" Print Ready browse is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Attach_PODprintSpecs_Path, driver),
				"POD Print specs is not displayed");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Colors_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Colors field");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Pagesize_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for page size ");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Attach_Stock_Path, "value", driver).equalsIgnoreCase("- Please Select -"),
				"- Please Select - is not displayed for Stock");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Coverstock_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Cover Stock");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Finish_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Finish");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Coating_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Coating");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Attach_Noofpages_Path, "value", driver).equalsIgnoreCase("0"),
				"0 - is not displayed for No of pages");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Binding_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Binding");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_PrintMethod_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Print Method");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Printype_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for Print Type");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Flatsize_Path, "value", driver).isEmpty(),
				"Flat Size field is not empty");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_IMCOposition_Path, "value", driver).isEmpty(),
				"'Imposition/Composition field is not empty");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_Finishsize_Path, "value", driver).isEmpty(),
				"Finish Size field is not empty");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Attach_PCSPrint_Path, "value", driver)
				.equalsIgnoreCase("- Please Select -"), "- Please Select - is not displayed for PCS print");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Attach_ClickCost_Path, "value", driver).equalsIgnoreCase("0.0000"),
				"0.0000 is not displayed for Click cost");

		Click(locatorType, MI_Pricing_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_UnitPrice_Path, driver),
				"Unit Price header is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Pricing_ChargebackCost_Path, "value", driver).equalsIgnoreCase("0.0000"),
				"0.0000 is not populated in  'Chargeback Cost' field");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_PricingDetails_Path, driver),
				"Veritas Pricing Details is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_Typeck_Path, driver),
				"Pricing Type is not displayed");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Pricing_Typeck_Path, driver),
				"Flat radio button is not selected");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Pricing_Tieredck_ChargebackCost_Path, driver),
				"Tired radio button is selected");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Pricing_Approvedck_ChargeItem_Path, driver),
				"Is Print Cost Approved: radio button is not selected");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_FlatPricing_Path, driver),
				"Flat Pricing is not displayed");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Pricing_NoCostItemCk_Path, driver),
				"No Cost Item radio button is not selected");
		// softAssert1.assertTrue(
		// Get_Attribute(locatorType, MI_Pricing_Prodcost_Path, "value",
		// driver).equalsIgnoreCase("0.0100"),
		// "0.0100 is not populated in Production cost field");

		Click(locatorType, MI_Notification_Path, driver);
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Notifi_Notificationck_Path, driver),
				" Send Reorder Point Email  radio button is selected");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Notifi_AtQuantityLevel_Path, "value", driver).equalsIgnoreCase("0"),
				"0 - is not displayed for At Quantity level");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Notifi_ReOrderQuantity_Path, "value", driver).equalsIgnoreCase("0"),
				"0 - is not displayed for Reorder Quantity");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Notifi_ReceiptNotificationck_Path, driver),
				" Send Inventory Receipt Email radio button is selected");
		Thread_Sleep(1500);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_Notifi_Email2_Path, driver).equalsIgnoreCase("ver.protectivelife@rrd.com"),
				"ver.protectivelife@rrd.com is not displayed for Notification Quantity");

		Click(locatorType, MI_RulesTab_Path, driver);

		softAssert1.assertTrue(Element_Isselected(locatorType, MI_RulesTab_Allowbackorder_Path, driver),
				" Allow backorder radio button is selected");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_RulesTab_Removeduplicte_Path, driver),
				"Can Remove Duplicates When Used As Child Item  Point Email  radio button is selected");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_MaxorderTitle_Path, driver),
				"Max Order Quantity header is not displayed");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Rules_NoMax_Path, driver).trim().contains("0 = No Max"),
				"0 = No Max is not displayed");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Rules_DefaultMaxOrderQty_Path, "value", driver).equalsIgnoreCase("0"),
				"0 - is not displayed for Default Maximum Order Quantity");
		softAssert1
				.assertTrue(
						Get_Attribute(locatorType, MI_Rules_DefaultMaxOrderQtyFrq_Path, "value", driver)
								.equalsIgnoreCase("Per Order"),
						"Per Order is not displayed in Default Maximum Order Quantity Frequency");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_MaxOrderQtyPerRole_Field_Path, driver),
				"Max Qty for Role is not displays in the 'Maximum Order Quantity Per Role' field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Rules_MaxOrderQtyPerRole_Drop_Path, "value", driver)
						.equalsIgnoreCase("Please select one"),
				"Please Select one is not displays in dropdown in same row as the 'Maximum Order Quantity Per Role' field ");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Superseding_Path, driver),
				"Superseding header is not displays");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Rules_ReplaceType_Path, "value", driver).equalsIgnoreCase("None"),
				"None is not displays in replace type");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Rules_ReplaceWith_Field_Path, "value", driver).isEmpty(),
				"Replace With Field is not empty");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Rules_ReplaceDate_Path, "value", driver).isEmpty(),
				"Replace Date Field is not empty");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Rules_ObsoleteDate_Path, "value", driver).isEmpty(),
				"Obsolete Date Field is not empty");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Rules_Reason_Field_Path, "value", driver).isEmpty(),
				"Reason Field is not empty");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_RequireRelatedItems_Path, driver),
				"Required/Related Items header is not displays");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Require_FormNo_Path, driver),
				"Form Number column is not displays in the 'Required/Related Items' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Require_Quantity_Path, driver),
				"Quantity column is not displays in the 'Required/Related Items' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Require_Currentlocation_Path, driver),
				"Current location column is not displays in the 'Required/Related Items' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Require_Type_Path, driver),
				"Type column is not displays in the 'Required/Related Items' grid");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_StateMode_Path, driver),
				"State Rules header is not displays");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Rules_StateModefield_Path, driver).trim().contains("Mode"),
				"Mode field is not displayed in the State Rules");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Rules_StateMode_ExcRadio_Path, driver),
				"Exclude radio button is selected in State Rules");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Rules_StateMode_IncRadio_Path, driver),
				"Include radio button is not selected in State Rules");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Rules_States_Drop_Path, "value", driver).contains("Select"),
				"- Select - is not displays in the  'States' dropdown");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_FirmMode_Path, driver),
				"Firm Rules header is not displays");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Rules_FirmModefield_Path, driver).trim().contains("Mode"),
				"Mode field is not displayed in the Firm Rules");
		softAssert1.assertFalse(Element_Isselected(locatorType, MI_Rules_FirmMode_ExcRadio_Path, driver),
				"Exclude radio button is selected in Firm Rules");
		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Rules_FirmMode_IncRadio_Path, driver),
				"Include radio button is not selected in Firm Rules");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Rules_Firms_Drop_Path, "value", driver).contains("Select Firm(s)"),
				"- Select Firm(s)- is not displays in the  'Firm' dropdown");

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_Maxordertitle_Path, driver),
				"Max Order Quantity details header is not displays");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_MaxorderFrequency_Path, driver),
				"Frequency is not displays");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_MaxorderRolename_Path, driver),
				"Role Name is not displays");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_MaxorderQuantity_Path, driver),
				"Max Order Quantity is not displays");

		Click(locatorType, MI_MetricsTab_Path, driver);
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_Metrics_UnitsonHand_Path, driver).trim().equalsIgnoreCase("999999"),
				"999999 is not displayed in  Units on Hand : ");
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_Metrics_UnitsAvailable_Path, driver).trim().equalsIgnoreCase("999999"),
				"999999 is not displayed in Units Available : ");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Metrics_Allocated_Path, driver).trim().equalsIgnoreCase("0"),
				"0 is not displayed in  Units Allocated : ");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Metrics_BackOrderQTY_Path, driver).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Back Order QTY :  ");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Metrics_MTD_Path, driver).trim().equalsIgnoreCase("0"),
				"0 is not displayed in MTD Usage :  ");
		softAssert1.assertTrue(Get_Text(locatorType, MI_Metrics_YTD_Path, driver).trim().equalsIgnoreCase("0"),
				"0 is not displayed in YTD Usage :  ");
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_Metrics_AverageMonthlyUsage_Path, driver).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Average Monthly Usage :  ");
		softAssert1.assertTrue(
				Get_Text(locatorType, MI_Metrics_AverageMonthlyDownloads_Path, driver).trim().equalsIgnoreCase("0"),
				"0 is not displayed in Average Monthly Downloads :  ");

		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_ChangeHistory_Date_Path, driver),
				"Date Column is not displayed in the Change history tab");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_ChangeHistory_User_Path, driver),
				"User Column is not displayed in the Change history tab");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_ChangeHistory_Action_Path, driver),
				"Action Column is not displayed in the Change history tab");

		Click(locatorType, MI_PageflexTab_Path, driver);
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Pageflex_Jobnamefield_Path, "value", driver).isEmpty(),
				"Pageflex Job Name' field is not empty");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_PageflexInventory_Path, driver),
				"Inventpry option  header is not displays in pageflex");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_PageflexOptiontype__Path, driver),
				"Option Type column is not displays in the 'Inventory Options' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_FriendlyName_Path, driver),
				"Friendly Namecolumn is not displays in the 'Inventory Options' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_FieldGroup_Path, driver),
				"Field Group column is not displays in the 'Inventory Options' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_MaxLength_Path, driver),
				"Max Length column is not displays in the 'Inventory Options' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_Visible_Path, driver),
				"	Visible column is not displays in the 'Inventory Options' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_Required_Path, driver),
				"Required column is not displays in the 'Inventory Options' grid");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, MI_Pageflex_Active_Path, driver),
				"Active column is not displays in the 'Inventory Options' grid");
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 16, dependsOnMethods = "PL_TC_2_5_2_3_1andPL_TC_2_5_2_3_2andPL_TC_2_5_2_3_3")
	public void PL_TC_2_5_2_3_4() throws IOException, InterruptedException {

		// Validate that not filling out all required fields on Pricing Tab
		// doesn't enable you to move to another tab

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Pricing_Path, driver);
		if (Element_Isselected(locatorType, MI_Pricing_NoCostItemCk_Path, driver)) {

			Click(locatorType, MI_Pricing_NoCostItemCk_Path, driver);

		}
		Type(locatorType, MI_Pricing_Prodcost_Path, "0", driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_Pricing_ProdcostError_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Pricing_ProdcostError_Path, driver),
				"'Print Cost must be greater than zero!' message is not displays");
	}

	@Test(enabled = false, priority = 17, dependsOnMethods = "PL_TC_2_5_2_3_1andPL_TC_2_5_2_3_2andPL_TC_2_5_2_3_3")

	public void PL_TC_2_5_2_3_6() throws IOException, InterruptedException {

		// Validate that not filling out all required fields on Pricing Tab
		// doesn't enable you to move to another tab

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Pricing_Path, driver);
		if (Element_Isselected(locatorType, MI_Pricing_NoCostItemCk_Path, driver)) {

		} else {
			Click(locatorType, MI_Pricing_NoCostItemCk_Path, driver);
		}
		Type(locatorType, MI_Pricing_Prodcost_Path, "0.01", driver);
		Click(locatorType, MI_Attach_Nextbtn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Notifi_Title_Path, driver),
				"Notification option header is not displayed");
		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);

	}

	@Test(enabled = false, priority = 18, dependsOnMethods = "PL_TC_2_5_2_3_1andPL_TC_2_5_2_3_2andPL_TC_2_5_2_3_3")
	public void PL_TC_2_5_2_3_8() throws IOException, InterruptedException {

		// Validate that a part can be obsolete

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		String todaydate = Get_FutureTime("M/d/YYYY HH:mm:ss a");
		Type(locatorType, MI_Rules_ObsoleteDate_Path, todaydate, driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Savepopup_Message_Path, driver),
				"'Successfully Saved!' message is not displayed in pop-up window");
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Reporter.log(CreatePart + " is Obsoleted");
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, MI_ChangeHistoryAction1_Path, driver)
						.contains("ObsoleteDate changed from '12:00:00 AM' to '" + Get_Todaydate("M/d/YYYY")),
				"Obosleted change history is not/incorrectly shown");

	}

	@Test(enabled = false, priority = 19)
	public void PL_TC_2_5_2_3_9() throws IOException, InterruptedException {

		// Validate that a part unobsolete

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_UnObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_UnObsoleteNowbtn_Path, driver);
		Thread_Sleep(800);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Rules_UnObsoleteNowmsg_Path, driver),
				"Item Reactivcated message is not shown");
		Assert.assertTrue(Get_Attribute(locatorType, MI_Rules_ObsoleteDate_Path, "value", driver).isEmpty(),
				"Obsolete date field is not empty after part has un-obsoleted");
		Click(locatorType, MI_Gen_Save_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Savepopup_Message_Path, driver),
				"'Successfully Saved!' message is not displayed in pop-up window");
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Reporter.log(Part12 + " is Obsoleted");
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, History("Reactivating Part"), driver),
				"Reactivating Part is not shown in change history");
		Click(locatorType, Logout_Path, driver);
		login(UserName, Password);
		Type(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part1), Part1 + " is not displayed");
	}

	@Test(enabled = false, priority = 19)
	public void PL_TC_2_5_2_3_10() throws IOException, InterruptedException {

		// Validate appropriate changes are recorded in change history

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part12, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part12), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Pricing_Path, driver);
		if (Element_Isselected(locatorType, MI_Pricing_NoCostItemCk_Path, driver)) {

			Click(locatorType, MI_Pricing_NoCostItemCk_Path, driver);

		}
		Type(locatorType, MI_Pricing_Prodcost_Path, "0.1", driver);
		Click(locatorType, MI_Attach_Nextbtn_Path, driver);
		Click(locatorType, MI_Pricing_Path, driver);
		Click(locatorType, MI_Pricing_NoCostItemCk_Path, driver);
		Type(locatorType, MI_Pricing_Prodcost_Path, "0.1", driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);

	}

	@Test(enabled = false, priority = 20)
	public void PL_TC_2_5_2_4_1() throws IOException, InterruptedException {

		// Validate when copying a part the original part name appears in
		// predecessor field and part number field is able to be edited

		login(UserName, Password);
		CreatePart = Part33 + "_" + RandomInt();
		Createdpartlist.add(CreatePart);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part33, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(Part33), driver),
				Part33 + " is not displayed");
		Click(locatorType, MI_SearchresultsCopy(Part33), driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Copyalert_Path, driver), "Copy Alert is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_CopyalertTitle_Path, driver),
				"'Copy?' is not displayed in the Copy pop up");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_CopyalertOK_Path, driver),
				"OK button is not displayed in the Copy alert");
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_CopyalertCancel_Path, driver),
				"Cancel button is not displayed in the Copy alert");
		Click(locatorType, MI_CopyalertOK_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		softAssert.assertTrue(Element_Is_Displayed(locatorType, MI_Gen_txtPredecessor_Path, driver),
				"Predecessor field is not displayed in the Copy alert");
		Assert.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtPredecessor_Path, "value", driver).trim().equalsIgnoreCase(Part33),
				Part33 + " is not displayed in Predecessor field");
		Type(locatorType, MI_Gen_Formnumber_Path, CreatePart, driver);
		Type(locatorType, MI_Gen_txtRevisionDate_Path, "0" + Get_Todaydate("MM/dd/YYYY"), driver);
		Click(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Reporter.log("Created New Part Name is '" + CreatePart + "'");

	}

	@Test(enabled = false, priority = 21, dependsOnMethods = "PL_TC_2_5_2_4_1")
	public void PL_TC_2_5_2_4_2() throws IOException, InterruptedException {

		// Validate that appropriate fields copy over from previous part
		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(CreatePart), driver), "");
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtTitle_Path, "value", driver).toUpperCase().contains("QA TEST"),
				"QA Test is not displayed in the Title field");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtDescription_Path, "value", driver).toUpperCase()
				.contains("THIS IS A QA TEST"), "THIS IS A QA TEST is not displayed in the Descriprion field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_txtKeyWords_Path, "value", driver).toUpperCase().contains("TEST"),
				"Test is not displayed in the Keyword field");
		softAssert1
				.assertTrue(
						Get_Attribute(locatorType, MI_Gen_InventoryType_DropDown_Path, "value", driver)
								.equalsIgnoreCase("Print On-Demand"),
						"Print On-Demand is not displayed in the Inventory field dropdown field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_UnitsPerPack_Path, "value", driver).equalsIgnoreCase("1"),
				"1 is not displayed in the Units Per Pack field");
		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_Collateraltype_Path, "value", driver).equalsIgnoreCase("Brochure"),
				"Brochure is not displayed in the Inventory Collateral dropdown field");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_ProductName_List_Input_Path, "value", driver)
						.equalsIgnoreCase("Advantage Choice UL, Asset Builder"),
				"'Advantage Choice UL, Asset Builder' is not pre-selected in 'Product Name' dropdown");

		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_ProductLineType_List_Input_Path, "value", driver)
				.equalsIgnoreCase("Company"), " 'Product Line' dropdown is not displayed  ");

		softAssert1.assertTrue(
				Get_Attribute(locatorType, MI_Gen_BusinessLine_Path, "value", driver).equalsIgnoreCase("Annuity"),
				"'Annuity' is not pre-selected in 'Business Line' dropdown");

		softAssert1.assertTrue(Element_Isselected(locatorType, MI_Gen_chkFulfillment_Path, driver),
				"Fullfilment checkbox is not checked in View In");
		softAssert1.assertTrue(Get_Attribute(locatorType, MI_Gen_txtNotes_Path, "value", driver).isEmpty(),
				"Notes field is not empty");
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Reporter.log(CreatePart + " is Obsoleted");
		softAssert1.assertAll();

	}

	@Test(enabled = false, priority = 22, dependsOnMethods = "PL_TC_2_5_2_4_1")
	public void PL_TC_2_5_2_4_3() throws IOException, InterruptedException {

		// Validate appropriate changes are recorded in change history

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, CreatePart, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Searchresults(CreatePart), driver), "");
		Click(locatorType, MI_SearchresultsEdit(CreatePart), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_ChangeHistoryTab_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MI_ChangeHistoryTitle_Path, driver);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, History("FormNumber changed from"), driver),
				"FormNumber changed from 'Copy Of QA_Copy_NewItem is not displayed in the Change history");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, History("Obsolete Part Now!"), driver),
				"Obsolete Part Now! is not displayed in the Change history");
		softAssert1.assertAll();
	}

	@Test(enabled = false, priority = 1)
	public void PL_TC_2_5_2_4_7() throws IOException, InterruptedException {

		// Validate a part can be replaced

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part29, driver);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part29), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Kitting_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Kitting_Additembtn_Path, driver);
		Click(locatorType, MI_Kittingitemviewdetail(Part30.toLowerCase()), driver);

		// Click(locatorType, MI_Kittingitemviewdetail(Part30.toUpperCase()),
		// driver);
		Switch_New_Tab2(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_ReplaceType_Icon_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value("Date"), driver);
		Type(locatorType, MI_Rules_ReplaceDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"), driver);
		Type(locatorType, MI_Rules_ReplaceWith_Field_Path, Part31, driver);
		Type(locatorType, MI_Rules_ReplaceDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"), driver);
		Type(locatorType, MI_Rules_ReplaceWith_Field_Path, Part31, driver);
		Wait_ajax();
		Click(locatorType, MI_Gen_Save_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path, driver);
		Click(locatorType, MI_Savepopup_Ok_Path, driver);
		Closealltabs(driver);
	}

	@Test(enabled = false, priority = 23)
	public void PL_TC_2_5_2_4_7_Continuation() throws IOException, InterruptedException {

		// Validate a part can be replaced Continuation

		login(UserName, Password);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part29, driver);
		Click(locatorType, MI_SI_Active_Icon_Path, driver);
		Wait_ajax();
		// Thread_Sleep(1000);
		Click(locatorType, li_value("- Any -"), driver);
		Wait_ajax();
		// Thread_Sleep(1000);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part29), driver);
		Switch_New_Tab(driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path, driver);
		Click(locatorType, MI_Kitting_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Kitting_Additembtn_Path, driver);
		// Click(locatorType, MI_Kittingitemviewdetail(Part30), driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, MI_Kittingitemlist(Part31), driver),
				"QA_Replace_2 is not displayed");
		Click(locatorType, MI_KittingitemRemove(Part31), driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Kitting_RemovepopupOK_Path, driver);
		Click(locatorType, MI_Kitting_RemovepopupOK_Path, driver);
		ManageInventoryNavigation();
		Type(locatorType, MI_SI_FormNo_Path, Part30, driver);
		Click(locatorType, MI_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, MI_Searchloadingpanel_Path, driver);
		Click(locatorType, MI_SearchresultsEdit(Part30.toLowerCase()), driver);
		// Click(locatorType, MI_SearchresultsEdit(Part30.toUpperCase()),
		// driver);
		Switch_New_Tab2(driver);
		Click(locatorType, MI_RulesTab_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_ObsoleteNowbtn_Path, driver);
		Wait_ajax();
		// Thread_Sleep(800);
		ExplicitWait_Element_Clickable(locatorType, MI_Rules_UnObsoleteNowbtn_Path, driver);
		Click(locatorType, MI_Rules_UnObsoleteNowbtn_Path, driver);
		Closealltabs(driver);

		// Set up piece to back
		// login(UserName, Password);
		// ManageInventoryNavigation();
		// Type(locatorType, MI_SI_FormNo_Path, Part29, driver);
		// Click(locatorType, MI_SI_Active_Icon_Path, driver);
		// Wait_ajax();
		//
		// Click(locatorType, li_value("- Any -"), driver);
		// Wait_ajax();
		//
		// Click(locatorType, MI_Searchbtn_Path, driver);
		// ExplicitWait_Element_Not_Visible(locatorType,
		// MI_Searchloadingpanel_Path, driver);
		// Click(locatorType, MI_SearchresultsEdit(Part29), driver);
		// Switch_New_Tab(driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Gen_Save_Path,
		// driver);
		// Click(locatorType, MI_Kitting_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType,
		// MI_Kitting_Additembtn_Path, driver);
		// Type(locatorType, MI_Kitting_Formno_Path, Part30, driver);
		// Wait_ajax();
		//
		// Click(locatorType, MI_Kitting_Type_Path, driver);
		// Wait_ajax();
		//
		// Click(locatorType, li_value("Component"), driver);
		// Wait_ajax();
		//
		// Click(locatorType, MI_Kitting_Location_Path, driver);
		// Wait_ajax();
		//
		// Click(locatorType, li_value("Affix Back Bottom Center"), driver);
		// Wait_ajax();
		//
		// Click(locatorType, MI_Kitting_Perkitck_Path, driver);
		// Type(locatorType, MI_Kitting_Sortorder_Path, "1", driver);
		// Click(locatorType, MI_Kitting_Additembtn_Path, driver);
		// ExplicitWait_Element_Not_Visible(locatorType,
		// MI_Kitting_Additemloadingpanel_Path, driver);
		// Assert.assertTrue(Element_Is_Displayed(locatorType,
		// MI_Kittingitemlist(Part30), driver),
		// "QA_Replace_1 is not displayed");
		// Click(locatorType, MI_Gen_Save_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, MI_Savepopup_Ok_Path,
		// driver);
		// Click(locatorType, MI_Savepopup_Ok_Path, driver);
		// Click(locatorType, Logout_Path, driver);

	}

	@BeforeMethod
	public void BefMethod() {
		softAssert1 = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void AfteMethod() throws IOException, InterruptedException {

		Closealltabs(driver);
		Click(locatorType, Logout_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Login_btn_Path, driver);

	}

	@AfterTest(enabled = false)
	public void Afterclass() throws IOException, InterruptedException {
		Obsoleteparts();
	}

}