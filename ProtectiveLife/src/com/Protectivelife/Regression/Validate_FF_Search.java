package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Validate_FF_Search extends BaseTest{
	
	
	@Test
	public void PL_TC_2_7_1_1() throws IOException, InterruptedException {
		
		login(UserName, Password);
		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1), Part1 + " is not displayed");
		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click_On_Element(locatorType, Checkoutbtn_Path, driver);
		
	}
}
