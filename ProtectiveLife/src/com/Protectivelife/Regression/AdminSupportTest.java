package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class AdminSupportTest extends BaseTest {

	public static final String Supportticket_Commment = "QATest";
	public static String Support_Ticket_Number = "";

	// Add a new support ticket in section

	@Test(enabled = true, priority = 1)
	public void PL_TC_2_4_3_1() throws InterruptedException {
		Click_On_Element(locatorType, Support_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ST_CT_Addticketicon_Path, driver);

		Assert.assertTrue(Is_Element_Present(locatorType, ST_CT_Addticketicon_Path, driver),
				"Page not navigating to Support");

		Click_On_Element(locatorType, ST_CT_Addticketicon_Path, driver);

		Select_DropDown_VisibleText(locatorType, ST_CST_Categorydd_Path, "Other", driver);
		Thread.sleep(2000);
		Select_DropDown_VisibleText(locatorType, ST_CST_SubCategorydd_Path, "Other", driver);

		Clear_Type_Charecters(locatorType, ST_CST_Commentfield_Path, Supportticket_Commment, driver);

		Click_On_Element(locatorType, ST_CST_Createbtn_Path, driver);

		ExplicitWait_Element_Visible(locatorType, STM_Ticketno_Path, driver);

		Assert.assertTrue(Is_Element_Present(locatorType, STM_Ticketno_Path, driver), "Tickets not created");

		Support_Ticket_Number = Get_Text(locatorType, STM_Ticketno_Path, driver);

		System.out.println(Support_Ticket_Number);

	}

	// Validate user is able to update the Ticket on Edit/View Page

	@Test(enabled = true, priority = 2)
	public void PL_TC_2_4_3_2() throws InterruptedException {

		Click_On_Element(locatorType, Support_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ST_CT_Addticketicon_Path, driver);

		Click_On_Element(locatorType, ST_CT_Edit1_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, STM_Ticketno_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, STM_Ticketno_Path, driver).equalsIgnoreCase(Support_Ticket_Number),
				"Wrong ticketnumber");

		Assert.assertTrue(Is_Element_Present(locatorType, STM_NewStatus_Path, driver), "Status button New is missing ");

		Assert.assertTrue(Is_Element_Present(locatorType, STM_Inprogress_Path, driver),
				"Status button Inprogress is missing ");

		Assert.assertTrue(Is_Element_Present(locatorType, STM_Hold_Path, driver), "Status button Hold is missing ");

		Assert.assertTrue(Is_Element_Present(locatorType, STM_Closedstatus_Path, driver),
				"Status button Closed is missing ");

		Click_On_Element(locatorType, STM_Inprogress_Path, driver);

		Click_On_Element(locatorType, STM_UpdateTicketbtn_Path, driver);

		// Click_On_Element(locatorType, STM_UpdateTicketbtn_Path, driver);
		ExplicitWait_Element_Visible(Supportticket_Commment, STM_UpdateTicketmsg_Path, driver);
		Thread.sleep(2000);

		Assert.assertTrue(Element_Is_Displayed(locatorType, STM_UpdateTicketmsg_Path, driver),
				"Status Message is missing ");

	}

	@BeforeTest
	public void Aspl_ogin() throws IOException, InterruptedException {
		login(UserName, Password);

	}

	@AfterTest(enabled = true)
	public void Afterclass() throws IOException, InterruptedException {
		logout();
	}

}
