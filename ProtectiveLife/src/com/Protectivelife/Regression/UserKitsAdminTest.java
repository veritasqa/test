package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class UserKitsAdminTest extends BaseTest {

	public static String AddKitName = "";

	public void ManageUserKitsNavigation() {
		Hover_Over_Element(locatorType, Admin_btn_Path, driver);

		Click(locatorType, Manage_User_Kits_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, UserKits_AddNew_Path, driver);
	}

	@Test(enabled = true, priority = 1)
	public void PL_TC_2_5_5_1_1_V1_1() {

		ManageUserKitsNavigation();

		Assert.assertTrue(Get_Title(driver).contains("Search User Kits"), "Manage User Kit page is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_Formnum_Path, driver),
				"Form Number field Icon Button is Missing");
		// 2nd half
		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_Title_Path, driver),
				"Title field Icon Button is Missing");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_Title_Icon_Path, driver),
				"Title Filter  Icon Button is Missing");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_LastUpdated_path, driver),
				"Last Updated field Icon Button is Missing");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_LastUpdated_Icon_Path, driver),
				"Last Updated Filter Icon Button is Missing");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_Edit_Header_Path, driver),
				"Edit Button is Missing");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_AddNew_Path, driver),
				"AddNew kit  is Missing");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, UserKits_Refesh_Path, driver),
				"Refresh Button is Missing");

		softAssert.assertAll();

		/*//PL_TC_2_5_5_1_1_V1_2
		
		
		
		Click(locatorType, UserKits_AddNew_Path, driver);
		Type(locatorType, UserKits_Form_Textbox_path, Part29, driver);
		
		Type(locatorType, UserKits_Title_Textbox_Path, "Admin Test" , driver);
		
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		
		Click(locatorType, UserKits_QAFolder_Path, driver);
		
		*/

	}

	@Test(enabled = true, priority = 2)

	public void PL_TC_2_5_5_1_1_V1_2() {

		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Admin Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase(UserName + "_QA_Kit_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Add 2nd User KIt
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit2", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Admin Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase(UserName + "_QA_Kit2_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

	}

	@Test(enabled = true, priority = 3)
	public void PL_TC_2_5_5_1_1_V1_3() {

		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Opt2_Edit_Path, driver);

		Click(locatorType, UserKits_Kit_Arrow_Path, driver);

		Click(locatorType, UserKits_Partedit_Path, driver);

		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Thread_Sleep(5000);
		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");

	}

	@Test(enabled = true, priority = 4)

	public void PL_TC_2_5_5_1_1_V1_4() {

		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Alert_Accept(driver);

		Click(locatorType, Logout_Path, driver);
	}

	public void Hover_UserKit_widget() throws InterruptedException {
		Hover_Over_Element(locatorType, Add_Shortcut_Path, driver);
		Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Wid_UserKit_Select_Path, driver);

		Click(locatorType, Wid_UserKit_Select_Path, driver);
	}

	@Test(enabled = true, priority = 5)
	public void PL_TC_2_6_8_1_V1_1() throws IOException, InterruptedException {

		login(UserName, Password);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		 Assert.assertTrue(
		 Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber),
		 driver).trim()
		 .equalsIgnoreCase(AddKitName), "Name In UserKit Widget Missmatch expected-" + AddKitName + "Butgot--"
		 + Get_Text(locatorType, Wid_Xpath(Widgetname,
		 Wid_UserKit_formnumber), driver));

	}

	@Test(enabled = true, priority = 6)
	public void PL_TC_2_6_8_1_V1_2() throws InterruptedException {

		// Validate Edit/View My user Kits are working properly (Admin)
		Hover_UserKit_widget();
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Opt1_Edit_Path, driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Click(locatorType, UserKits_Partedit_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
				.equalsIgnoreCase(AddKitName), "User Kit name is not displayed ");
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Accept_Alert(driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKitEmpty), driver).trim()
				.contains("No Items Available"), "No Items Available is not displayed");
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 7)
	public void PL_TC_2_5_5_1_1_V1_11ANDPL_TC_2_6_8_1_V1_5ANDPL_TC_2_6_8_1_V1_6()
			throws IOException, InterruptedException {

		login(MarketingkUserName, MarketingPassword);
		// PL_TC_2_5_5_1_1_V1_11
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase("PMarketing" + "_QA_Kit_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Add 2nd User KIt
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit2", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase("PMarketing" + "_QA_Kit2_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Modify the Kit
		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Opt2_Edit_Path, driver);

		Click(locatorType, UserKits_Kit_Arrow_Path, driver);

		Click(locatorType, UserKits_Partedit_Path, driver);

		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Thread_Sleep(5000);
		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		// Click(locatorType, Logout_Path, driver);

		// Delete the KIt

		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Alert_Accept(driver);

		Click(locatorType, Logout_Path, driver);

		// User Kit Widget

		login(MarketingkUserName, MarketingPassword);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		Assert.assertTrue(
				Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
						.equalsIgnoreCase(AddKitName),
				"Name In UserKit Widget Missmatch expected-" + AddKitName + "But got--"
						+ Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver));
		Click(locatorType, Logout_Path, driver);

		// PL_TC_2_6_8_1_V1_5

		login(MarketingkUserName, MarketingPassword);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		Assert.assertTrue(
				Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
						.equalsIgnoreCase(AddKitName),
				"Name In UserKit Widget Missmatch expected-" + AddKitName + "But got--"
						+ Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver));

		// PL_TC_2_6_8_1_V1_6
		// Validate Edit/View My user Kits are working properly (Admin)
		Hover_UserKit_widget();
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Opt1_Edit_Path, driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Click(locatorType, UserKits_Partedit_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
				.equalsIgnoreCase(AddKitName), "User Kit name is not displayed ");
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Accept_Alert(driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKitEmpty), driver).trim()
				.contains("No Items Available"), "No Items Available is not displayed");
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 8)
	public void PL_TC_2_5_5_1_1_V1_12ANDPL_TC_2_6_8_1_V1_3ANDPL_TC_2_6_8_1_V1_4()
			throws IOException, InterruptedException {

		login(InUserName, InPassword);
		// PL.TC.2.5.5.1.1.V1.12
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase("PInadmin" + "_QA_Kit_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Add 2nd User KIt
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit2", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase("PInadmin" + "_QA_Kit2_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Modify the Kit
		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Opt2_Edit_Path, driver);

		Click(locatorType, UserKits_Kit_Arrow_Path, driver);

		Click(locatorType, UserKits_Partedit_Path, driver);

		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Thread_Sleep(5000);
		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		// Click(locatorType, Logout_Path, driver);

		// Delete the KIt

		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Alert_Accept(driver);

		Click(locatorType, Logout_Path, driver);

		// User Kit Widget

		login(InUserName, InPassword);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		Assert.assertTrue(
				Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
						.equalsIgnoreCase(AddKitName),
				"Name In UserKit Widget Missmatch expected-" + AddKitName + "But got--"
						+ Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver));
		Click(locatorType, Logout_Path, driver);

		// PL.TC.2.6.8.1.V1.3
		// Validate Select a recipient link is working properly (Inadmin User)

		login(InUserName, InPassword);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		Assert.assertTrue(
				Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
						.equalsIgnoreCase(AddKitName),
				"Name In UserKit Widget Missmatch expected-" + AddKitName + "But got--"
						+ Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver));

		// PL.TC.2.6.8.1.V1.4
		// Validate Edit/View My user Kits are working properly (In Admin)
		Hover_UserKit_widget();
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Opt1_Edit_Path, driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Click(locatorType, UserKits_Partedit_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
				.equalsIgnoreCase(AddKitName), "User Kit name is not displayed ");
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Accept_Alert(driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKitEmpty), driver).trim()
				.contains("No Items Available"), "No Items Available is not displayed");
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 9)
	public void PL_TC_2_5_5_1_1_V1_13ANDPL_TC_2_6_8_1_V1_7ANDPL_TC_2_6_8_1_V1_8()
			throws IOException, InterruptedException {

		login(SalesdeskUserName, SalesdeskPassword);
		// PL_TC_2_5_5_1_1_V1_13
		// Validate Manage User kit is working fine for Sales desk user
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase("PSalesdesk" + "_QA_Kit_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Add 2nd User KIt
		ManageUserKitsNavigation();
		Click(locatorType, UserKits_AddNew_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKits_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Assert.assertTrue(Element_Is_Displayed(locatorType, AddUserKitsOwner_Title_Path, driver),
				"'User Kits Section is not displayed'");

		Type(locatorType, UserKits_Form_Textbox_path, "QA_Kit2", driver);
		Type(locatorType, UserKits_Title_Textbox_Path, "Test", driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, UserKits_QAFolder2_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);

		Assert.assertTrue(AddKitName.equalsIgnoreCase("PSalesdesk" + "_QA_Kit2_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");
		Type(locatorType, Kit_FormNumber_Path, "QA_Folder2", driver);
		Thread_Sleep(500);
		Click(locatorType, Kit_Location_Path, driver);
		Thread_Sleep(500);
		Click(locatorType, li_value("Affix Back"), driver);
		Click(locatorType, Kit_AddtoKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Kit_FormNumber_Path, driver),
				"'QA_Folder2 is not displayed in the 'Edit Kit Build' Section'");

		ManageUserKitsNavigation();

		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath(AddKitName), driver),
				AddKitName + "is not displayed under 'User Kits''");

		// Modify the Kit
		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Opt2_Edit_Path, driver);

		Click(locatorType, UserKits_Kit_Arrow_Path, driver);

		Click(locatorType, UserKits_Partedit_Path, driver);

		Click(locatorType, UserKits_SaveUserKit_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Thread_Sleep(5000);
		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		// Click(locatorType, Logout_Path, driver);

		// Delete the KIt

		ManageUserKitsNavigation();

		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Alert_Accept(driver);

		Click(locatorType, Logout_Path, driver);

		// User Kit Widget

		login(SalesdeskUserName, SalesdeskPassword);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		Assert.assertTrue(
				Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
						.equalsIgnoreCase(AddKitName),
				"Name In UserKit Widget Missmatch expected-" + AddKitName + "But got--"
						+ Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver));
		Click(locatorType, Logout_Path, driver);

		// PL_TC_2_6_8_1_V1_7
		// Validate Select a recipient link is working properly (Sales Desk
		// User)

		login(SalesdeskUserName, SalesdeskPassword);
		Hover_UserKit_widget();

		Click(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_Recepient_Path), driver);
		ContactSearch(Firstname, Lastname);

		Assert.assertTrue(
				Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
						.equalsIgnoreCase(AddKitName),
				"Name In UserKit Widget Missmatch expected-" + AddKitName + "But got--"
						+ Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver));

		// PL_TC_2_6_8_1_V1_8
		// Validate Edit/View My user Kits are working properly (Sales Desk)
		Hover_UserKit_widget();
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Opt1_Edit_Path, driver);
		Click(locatorType, UserKits_Kit_Arrow_Path, driver);
		Click(locatorType, UserKits_Partedit_Path, driver);
		Click(locatorType, UserKits_SaveUserKit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UserKits_SaveUserKit_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, UserKit_Saved_Message_Path, driver),
				"Saved Message is Missing");

		Assert.assertTrue(Get_Text(locatorType, EditKit_Formnumber1_Path, driver).trim().equalsIgnoreCase("QA_Folder3"),
				"Edit kit data Mismatch");
		AddKitName = Get_Attribute(locatorType, UserKits_Form_Textbox_path, "value", driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKit_formnumber), driver).trim()
				.equalsIgnoreCase(AddKitName), "User Kit name is not displayed ");
		Click(locatorType, Wid_Xpath(Widgetname, Editmysuerkits), driver);
		Click(locatorType, UserKits_Delete1_btn_Path, driver);

		Accept_Alert(driver);
		Click(locatorType, Home_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Wid_UserKitEmpty), driver).trim()
				.contains("No Items Available"), "No Items Available is not displayed");
		Click(locatorType, Logout_Path, driver);

	}

	@BeforeTest
	public void WidgetLogin() throws IOException, InterruptedException {
		login(UserName, Password);

	}

	@AfterTest(enabled = false)

	public void Outlog() throws IOException, InterruptedException {
		logout();
	}

}
