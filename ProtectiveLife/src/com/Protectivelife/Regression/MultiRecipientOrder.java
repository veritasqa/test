
package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class MultiRecipientOrder extends BaseTest {

	// Regular User

	@Test(enabled = true, priority = 1)
	public void RegularUploadMultimaillist() throws InterruptedException, IOException {

		login(UserName, Password);
		// Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);

		// Upload Nofirm
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, MultiMaillist_Swapna, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Multimaillist_SwapnaList, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 2, dependsOnMethods = "RegularUploadMultimaillist")
	public void PL_TC_2_7_1_a_1() throws IOException, InterruptedException, AWTException {

		// Validate user is able to place an Bulk order as regular Admin

		login(UserName, Password);
		Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		// Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		// Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Chooseasavedmaillist_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value(MultiMaillist_Swapna), driver);
		Wait_ajax();
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part19),
				Part19 + " is not displayed in the materials page");
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'Checkout(1)' is not displaying by 'Checkout' button");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Select_DropDown_VisibleText(locatorType, SC_ShipMethoddrop_Path, "UPS Ground", driver);
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		Wait_ajax();
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Order_Conf_Title_Path, driver),
				"Order Confirmation Page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderConf_OrderinfoTitle_Path, driver),
				"Order Information grid is not displayed");

		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		System.out.println("Order Number placed in method  " + "PL_TC_2_7_1_a_1" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + "PL_TC_2_7_1_a_1" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	@Test(enabled = true, priority = 3)
	public void RegularDel_SalesdeskMultimaillist() throws InterruptedException, IOException {

		login(UserName, Password);
		// Click(locatorType, Home_btn_Path, driver);
		Wait_ajax();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);
		Click(locatorType, MML_MultiMaillist_Swapna_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);
		Click(locatorType, Logout_Path, driver);

	}

	// Inventory Admin

	@Test(enabled = true, priority = 4)
	public void InadminUploadMultimaillist() throws InterruptedException, IOException {

		login(InUserName, InPassword);
		// Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);

		// Upload Nofirm
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, MultiMaillist_Swapna, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Multimaillist_SwapnaList, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 5, dependsOnMethods = "InadminUploadMultimaillist")
	public void PL_TC_2_7_1_a_2() throws IOException, InterruptedException, AWTException {

		// Validate user is able to place an Bulk order as Inventory Admin

		login(InUserName, InPassword);
		Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		// Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		// Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Chooseasavedmaillist_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value(MultiMaillist_Swapna), driver);
		Wait_ajax();
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part20, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Wait_ajax();
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'Checkout(1)' is not displaying by 'Checkout' button");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Select_DropDown_VisibleText(locatorType, SC_ShipMethoddrop_Path, "UPS Ground", driver);
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		Wait_ajax();
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Order_Conf_Title_Path, driver),
				"Order Confirmation Page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderConf_OrderinfoTitle_Path, driver),
				"Order Information grid is not displayed");
		//
		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part20),
				Part20 + " not appears under 'Order Details' grid ");
		System.out.println("Order Number placed in method  " + "PL_TC_2_7_1_a_2" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + "PL_TC_2_7_1_a_2" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	@Test(enabled = true, priority = 6)
	public void Inadmindel_SalesdeskMultimaillist() throws InterruptedException, IOException {

		login(InUserName, InPassword);
		// Click(locatorType, Home_btn_Path, driver);
		Wait_ajax();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);
		Click(locatorType, MML_MultiMaillist_Swapna_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);
		Click(locatorType, Logout_Path, driver);

	}

	// Marketing Admin
	@Test(enabled = true, priority = 7)
	public void MarkadminUploadMultimaillist() throws InterruptedException, IOException {

		login(MarketingkUserName, MarketingPassword);
		// Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);

		// Upload Nofirm
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, MultiMaillist_Swapna, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Multimaillist_SwapnaList, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 8, dependsOnMethods = "MarkadminUploadMultimaillist")
	public void PL_TC_2_7_1_a_3() throws IOException, InterruptedException, AWTException {

		// Validate user is able to place an Bulk order as Marketing Admin

		login(MarketingkUserName, MarketingPassword);
		Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		// Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		// Fileupload(Personilizedmailinglist, driver);
		Click(locatorType, MLU_Chooseasavedmaillist_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value(MultiMaillist_Swapna), driver);
		Wait_ajax();
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		// ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Wait_ajax();
		// Assert.assertTrue(Get_Text(locatorType, Part_Name_Path,
		// driver).contains(Part19),
		// Part19 + " is not displayed in the materials page");
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'Checkout(1)' is not displaying by 'Checkout' button");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Select_DropDown_VisibleText(locatorType, SC_ShipMethoddrop_Path, "UPS Ground", driver);
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		Wait_ajax();
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Order_Conf_Title_Path, driver),
				"Order Confirmation Page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderConf_OrderinfoTitle_Path, driver),
				"Order Information grid is not displayed");

		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		System.out.println("Order Number placed in method  " + "PL_TC_2_7_1_a_3" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + "PL_TC_2_7_1_a_3" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	@Test(enabled = true, priority = 9)
	public void Markadmindel_SalesdeskMultimaillist() throws InterruptedException, IOException {

		login(MarketingkUserName, MarketingPassword);
		Wait_ajax();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);
		Click(locatorType, MML_MultiMaillist_Swapna_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);
		Click(locatorType, Logout_Path, driver);

	}

	// SalesDesk User
	@Test(enabled = true, priority = 10)
	public void SalesdeskUploadMultimaillist() throws InterruptedException, IOException {

		login(SalesdeskUserName, SalesdeskPassword);
		Click(locatorType, MultiRecipientOrder_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);

		// Upload Nofirm
		Click(locatorType, MML_Addmaillistbtn_Path, driver);
		Type(locatorType, MML_MailListName_Path, MultiMaillist_Swapna, driver);
		Click(locatorType, MML_Choosefilepath_Path, driver);
		Fileupload(MML_Multimaillist_SwapnaList, driver);
		Click(locatorType, MML_Insertbtn_Path, driver);
		Click(locatorType, Logout_Path, driver);

	}

	@Test(enabled = true, priority = 11, dependsOnMethods = "SalesdeskUploadMultimaillist")
	public void PL_TC_2_7_1_a_4() throws IOException, InterruptedException, AWTException {

		// Validate user is able to place an Bulk order as Salesdesk Admin

		login(SalesdeskUserName, SalesdeskPassword);
		Clearcart();
		Click(locatorType, MultiRecipientOrder_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Chooseasavedmaillist_Path, driver);
		Wait_ajax();
		Click(locatorType, li_value(MultiMaillist_Swapna), driver);
		Wait_ajax();
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Wait_ajax();
		// Assert.assertTrue(Get_Text(locatorType, Part_Name_Path,
		// driver).contains(Part19),
		// Part19 + " is not displayed in the materials page");
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'Checkout(1)' is not displaying by 'Checkout' button");
		Select_DropDown_VisibleText(locatorType, SC_Behalfof_drop_Path, "1N/A - NOT APPLICABLE", driver);
		Select_DropDown_VisibleText(locatorType, SC_ShipMethoddrop_Path, "UPS Ground", driver);
		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		Wait_ajax();
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Order_Conf_Title_Path, driver),
				"Order Confirmation Page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderConf_OrderinfoTitle_Path, driver),
				"Order Information grid is not displayed");

		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		System.out.println("Order Number placed in method  " + "PL_TC_2_7_1_a_4" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + "PL_TC_2_7_1_a_4" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	@Test(enabled = true, priority = 12)
	public void Delete_SalesdeskMultimaillist() throws InterruptedException, IOException {

		login(SalesdeskUserName, SalesdeskPassword);
		Wait_ajax();
		Click(locatorType, Personalized_mailing_path, driver);
		Wait_ajax();
		Click(locatorType, MLU_Managelistsbtn_Path, driver);
		Click(locatorType, MML_MultiMaillist_Swapna_AutomationDeletebtn_Path, driver);
		// Wait_ajax();
		Alert_Accept(driver);
		Click(locatorType, Logout_Path, driver);

	}

}
