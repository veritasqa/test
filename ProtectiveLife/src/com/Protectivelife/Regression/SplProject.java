package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class SplProject extends BaseTest {

	public static String Ticketnumber = "";
	public static final String Supportticket_Commment = "QA Test";

	public void ManageSplPrjNav() {
		Hover(locatorType, Admin_btn_Path, driver);

		Click(locatorType, Special_Projects_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, Spl_Viewedit_Path, driver);
	}

	@Test(enabled = false, priority = 1)
	public void PL_TC_2_10_1_3() {
		// Verify "Add Ticket", "View/Edit", "Refresh" button displays on the
		// Special Projects table.
		ManageSplPrjNav();

		softAssert.assertTrue(Element_Is_Displayed(locatorType, Spl_AddTicket_Path, driver),
				"Addticket Button is Missing");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Spl_Refresh_Path, driver), "Refresh Button is Missing");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Spl_Viewedit_Path, driver),
				"View/Edit Button is Missing");

		softAssert.assertAll();

	}

	@Test(enabled = false, priority = 2)
	public void PL_TC_2_10_1_4() {

		// Validate "Add ticket button",Edit/View button takes to 'General' page

		ManageSplPrjNav();
		// Assert.assertTrue(Click(locatorType, Spl_AddTicket_Path, driver),
		// "Element Not Clickable");

		Click(locatorType, Spl_AddTicket_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");
		Click(locatorType, Spl_Cancel_Btn_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, Spl_AddTicket_Path,
		// driver);

		Click(locatorType, Spl_Viewedit_Path, driver);
		// ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path,
		// driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");
		Click(locatorType, Spl_Cancel_Btn_Path, driver);

	}

	@Test(enabled = false, priority = 3)

	public void PL_TC_2_10_1_5andPL_TC_2_10_1_8AndPL_TC_2_10_1_10AndPL_TC_2_10_1_11() throws InterruptedException {

		// Add a new special project and verify new ticket appears on Special
		// projects: view tickets table

		ManageSplPrjNav();
		Click(locatorType, Spl_AddTicket_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");
		Type(locatorType, Spl_GeneralTab_jobtitle_Path, Supportticket_Commment, driver);
		DropDown_VisibleText(locatorType, Spl_JobType_Path, "Other", driver);

		System.out.println(Get_Text(locatorType, Spl_Note_path, driver));
		Type(locatorType, Spl_OnBehalfOf_Path, Supportticket_Commment, driver);

		Assert.assertTrue(
				Get_Text(locatorType, Spl_Note_path, driver).trim().equalsIgnoreCase(
						"NOTE: Chargebacks apply to all special requests. Request a quote and inform your external of any charges that will be applied to their cost center."),
				Get_Text(locatorType, Spl_Note_path, driver) + " -Note Statement Doesnot match ");

		Click(locatorType, Spl_CostCenter_Arrow_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("99999"), driver);
		Thread_Sleep(1000);

		Assert.assertTrue(Get_Attribute(locatorType, Spl_SubAccount_Input_Path, "value", driver).contains("Select"),
				"Select is not displayed in the Sub Account field");

		Click(locatorType, Spl_SubAccount_Arrow_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Spl_SubAccount_LI_NS_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Spl_SubAccount_Arrow_Path, driver);
		Thread_Sleep(750);

		Type(locatorType, Spl_OnBehalfOf_Path, Supportticket_Commment, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_Description_Path, driver).isEmpty(),
				"Special Instruction field is not blank");

		Click(locatorType, Spl_create_Btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase(Spl_save_Message),
				"Data Saved message Doesnt match");

		Ticketnumber = Get_Text(locatorType, Spl_Ticketnum_path, driver).trim();

		Assert.assertTrue(Get_Text(locatorType, Spl_Status_path, driver).equalsIgnoreCase(Spl_Status_Message),
				Get_Text(locatorType, Spl_Status_path, driver) + "Status Doesnt match");

		ManageSplPrjNav();

		Type(locatorType, Spl_Ticketnum_Path, Ticketnumber, driver);

		Click(locatorType, Spl_jobtitle_Path, driver);

		Assert.assertTrue(
				Get_Text(locatorType, Ticketresultsticketnum(Ticketnumber), driver).equalsIgnoreCase(Ticketnumber),
				"Ticketnumber Doesnt match");

		// PL_TC_2_10_1_8

		// Validate change history updates made

		// Ticketnumber="000468";
		ManageSplPrjNav();

		Type(locatorType, Spl_Ticketnum_Path, Ticketnumber, driver);

		Click(locatorType, Spl_jobtitle_Path, driver);

		Assert.assertTrue(
				Get_Text(locatorType, Ticketresultsticketnum(Ticketnumber), driver).equalsIgnoreCase(Ticketnumber),
				"Ticketnumber Doesnt match");
		System.out.println(TicketnumberEdit(Ticketnumber));
		ExplicitWait_Element_Clickable(locatorType, TicketnumberEdit(Ticketnumber), driver);
		Thread.sleep(2000);
		Click(locatorType, TicketnumberEdit(Ticketnumber), driver);

		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);

		Type(locatorType, Spl_Description_Path, Supportticket_Commment, driver);

		Click(locatorType, Spl_create_Btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase(Spl_save_Message),
				"Data Saved message Doesnt match");

		Click(locatorType, Spl_History_Path, driver);

		Assert.assertTrue(
				Get_Text(locatorType, Spl_History_Description, driver)
						.equalsIgnoreCase("Changed Description From '' To 'QA Test'"),
				Get_Text(locatorType, Spl_History_Description, driver) + "Ticketnumber Doesnt match");

		// PL_TC_2_10_1_10
		// Verify changes are not saved on clicking Cancel button

		Click(locatorType, Spl_GeneralTab_Path, driver);

		Type(locatorType, Spl_Description_Path, "Test Special Instructions", driver);

		Thread.sleep(2000);

		Click(locatorType, Spl_Cancel_Btn_Path, driver);

		Thread.sleep(2000);

		Type(locatorType, Spl_Ticketnum_Path, Ticketnumber, driver);

		Click(locatorType, Spl_jobtitle_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Ticketresultsticketnum(Ticketnumber), driver).trim()
				.equalsIgnoreCase(Ticketnumber), "Ticketnumber Doesnt match");

		ExplicitWait_Element_Clickable(locatorType, TicketnumberEdit(Ticketnumber), driver);
		Thread.sleep(2000);

		Click(locatorType, TicketnumberEdit(Ticketnumber), driver);

		Thread.sleep(2000);

		Assert.assertTrue(
				Get_Text(locatorType, Spl_Description_Path, driver).trim().equalsIgnoreCase(Supportticket_Commment),
				"Special instructions field is not empty");

		// PL_TC_2_10_1_11
		// Validate the cancel functionality

		Click(locatorType, Spl_ProgressFiles_Path, driver);

		Click(locatorType, Spl_Prgs_AddComment_Path, driver);

		Type(locatorType, Spl_Prgs_Comment_Path, Supportticket_Commment, driver);
		Thread.sleep(2000);

		Click(locatorType, Spl_Prgs_Type_arrow, driver);
		Thread.sleep(2000);

		Click(locatorType, Spl_Prgs_Type_cancel, driver);
		Thread.sleep(2000);

		Click(locatorType, Spl_Prgs_Submit_btn, driver);
		Thread.sleep(2000);

		Click(locatorType, Spl_create_Btn_Path, driver);

		ManageSplPrjNav();
		Thread.sleep(2000);

		Click(locatorType, Spl_Closed_checkbox_Path, driver);
		Thread.sleep(2000);

		Assert.assertTrue(Get_Text(locatorType,
				"//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]/a", driver).trim()
						.equalsIgnoreCase(Ticketnumber),
				"Ticket is not Showing up in Closed ");

	}

	@Test(enabled = false, priority = 4)
	public void PL_TC_2_10_1_1_1() {
		// PL.TC.2.10.1.1.1
		// Validate upon clicking "Add Special Projects" takes to General
		// Special projects

		Hover(locatorType, Admin_btn_Path, driver);
		Thread_Sleep(2000);
		Hover(locatorType, Special_Projects_btn_Path, driver);
		Thread_Sleep(2000);

		Click(locatorType, Add_Special_Projects_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");
		Click(locatorType, Spl_Cancel_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Spl_Viewedit_Path, driver);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_AddTicket_Path, driver), "Addticket Button is Missing");

		Click(locatorType, Spl_AddTicket_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");

	}

	@Test(enabled = true, priority = 5)
	public void PL_TC_2_10_1_2() throws InterruptedException, IOException {

		// Validate the combination of filter functionality

		ManageSplPrjNav();
		Thread_Sleep(2000);

		Type(locatorType, Spl_Ticketnum_Path, "000564", driver);

		// Click(locatorType, Spl_jobtitle_Path, driver);

		Type(locatorType, Spl_jobtitle_Path, "QA", driver);

		Thread_Sleep(2000);
		Click(locatorType, Spl_jobtitle_Filter_Path, driver);
		Thread_Sleep(2000);

		Type(locatorType, Spl_DueDate_Path, "02/03/2018", driver);
		Thread_Sleep(2000);
		Click(locatorType, Spl_DueDate_Filter_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, Spl_Results_TicketNum, driver).contains("000564"),
				"Ticketnumber Doesnt match");

		ManageSplPrjNav();
		Type(locatorType, Spl_Ticketnum_Path, "000564", driver);

		// Click(locatorType, Spl_jobtitle_Path, driver);
		Thread_Sleep(2000);
		Click(locatorType, Spl_Rush_Path, driver);
		Thread_Sleep(2000);
		Click(locatorType, Spl_Rush_Filter_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, Spl_Results_TicketNum, driver).contains("0005"),
				"Ticketnumber Doesnt match");

	}

	@Test(enabled = false, priority = 6)
	public void PL_TC_2_10_1_6() throws InterruptedException, IOException {

		// Validate all the Fields/Buttons of 'General', Progress/Files, Costs
		// are enabled in Special Projects is working fine

		ManageSplPrjNav();
		Click(locatorType, Spl_AddTicket_Path, driver);

		// Type(locatorType, , UserName, driver);
		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");
		Thread_Sleep(2000);

		Assert.assertTrue(Get_Text(locatorType, Spl_GeneralTab_jobtitle, driver).contains("Job Title:"),
				"Job Title: field is Abscent");
		Assert.assertTrue(Get_Text(locatorType, Spl_GeneralTab_jobtitle_Star, driver).equalsIgnoreCase("*"),
				"'*' is not displays by 'Job Title' field");

		Type(locatorType, Spl_GeneralTab_jobtitle_Path, Supportticket_Commment, driver);

		Assert.assertTrue(Get_DropDown(locatorType, Spl_JobType_Path, driver).equalsIgnoreCase("- Select -"),
				"- Select - is not displays in 'Job Type' dropdown field");
		Assert.assertTrue(Get_Text(locatorType, Spl_GeneralTab_jobtype_Star, driver).equalsIgnoreCase("*"),
				"'*' is not displays by 'Job type' field");

		DropDown_VisibleText(locatorType, Spl_JobType_Path, "Other", driver);
		Assert.assertTrue(Get_Text(locatorType, Spl_Description_Path, driver).isEmpty(),
				"Description field is not blank");

		Type(locatorType, Spl_Description_Path, "QA is Testing", driver);

		Type(locatorType, Spl_QtyOfAddressee_Path, "1", driver);
		Assert.assertTrue(
				Get_DropDown(locatorType, Spl_Shipping_Dropdown_Path, driver).equalsIgnoreCase("- Please Select -"),
				"- Select - is not displays in 'Shipping/Post Service' dropdown field");

		DropDown_VisibleText(locatorType, Spl_Shipping_Dropdown_Path, "UPS Ground", driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_Createdby_Path, driver).contains("Priyanka Fnu"),
				"Created By  field is Mismatching");

		Assert.assertTrue(Get_Attribute(locatorType, Spl_CostCenter_Input_Path, "value", driver).contains("Select"),
				"Select is not displayed in the Cost center field");
		Click(locatorType, Spl_CostCenter_Arrow_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("99999"), driver);
		Thread_Sleep(1000);

		Assert.assertTrue(Get_Attribute(locatorType, Spl_SubAccount_Input_Path, "value", driver).contains("Select"),
				"Select is not displayed in the Sub Account field");
		Click(locatorType, Spl_SubAccount_Arrow_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Spl_SubAccount_LI_NS_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Spl_SubAccount_Arrow_Path, driver);
		Thread_Sleep(750);

		Type(locatorType, Spl_OnBehalfOf_Path, Supportticket_Commment, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_Description_Path, driver).isEmpty(),
				"Special Instruction field is not blank");
		Type(locatorType, Spl_SpecialInstructions_Path, Supportticket_Commment, driver);
		Assert.assertFalse(Element_Isselected(locatorType, Spl_Rush_chkbox_Path, driver),
				"Is Rush checkbox is checked");
		Click(locatorType, Spl_Rush_chkbox_Path, driver);

		Assert.assertTrue(Element_Isselected(locatorType, Spl_Quote_chkbox_Path, driver),
				"Is Quote needed checkbox is not checked");

		// Click(locatorType, SplProjects_Quoteneeded_Path, driver);
		Click(locatorType, Spl_create_Btn_Path, driver);
		Thread_Sleep(2000);

		ExplicitWait_Element_Visible(locatorType, Spl_save_Message_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed-1");

		// ProgresFiles

		Click(locatorType, Spl_ProgressFiles_Path, driver);
		Thread_Sleep(2000);

		ExplicitWait_Element_Clickable(locatorType, Spl_Prgs_AddComment_Path, driver);

		Click(locatorType, Spl_Prgs_AddComment_Path, driver);
		Thread_Sleep(2000);

		ExplicitWait_Element_Clickable(locatorType, Spl_Prgs_Submit_btn, driver);

		Type(locatorType, Spl_Prgs_Comment_Path, Supportticket_Commment, driver);

		Click(locatorType, Spl_Prgs_Type_arrow, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("Acknowledged"), driver);
		Thread_Sleep(1000);

		// Click(locatorType, Spl_Prgs_BrowseFiles_btn, driver);
		// Thread_Sleep(1500);

		// Fileupload(Spl_Prgs_BrowseFiles1, driver);
		// Thread_Sleep(1500);

		Click(locatorType, Spl_Prgs_Submit_btn, driver);
		Thread_Sleep(1500);

		Click(locatorType, Spl_Prgs_Save, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed-2");

		// Cost Tab
		Click(locatorType, Spl_Costs_Path, driver);
		Thread_Sleep(1500);

		ExplicitWait_Element_Clickable(locatorType, Spl_Costs_AddNewRecordButton_Path, driver);

		Type(locatorType, Spl_Costs_QuoteCost_Path, "0.1", driver);
		Type(locatorType, Spl_Costs_PrintCost_Path, "0.1", driver);
		Type(locatorType, Spl_Costs_PostageCost_Path, "0.1", driver);
		Type(locatorType, Spl_Costs_FulfillmentCost_Path, "0.1", driver);
		Click(locatorType, Spl_Costs_Billed_Path, driver);
		Thread_Sleep(800);

		Type(locatorType, Spl_Costs_ClientPostageCost_Path, "0.1", driver);
		Type(locatorType, Spl_Costs_RushCost_Path, "0.1", driver);

		Click(locatorType, Spl_Costs_AddNewRecordButton_Path, driver);

		Thread_Sleep(2000);

		Type(locatorType, Spl_Costs_Record_Description_Path, "QA Test", driver);
		DropDown_VisibleText(locatorType, Spl_Costs_Record_Type_Path, "Veritas Postage", driver);

		Type(locatorType, Spl_Costs_Record_dateInput_Path, Get_Futuredate("M/d/YYYY"), driver);
		Type(locatorType, Spl_Costs_Record_BillAmount_Path, "0.00", driver);

		Click(locatorType, Spl_Costs_Record_Save_Path, driver);
		Click(locatorType, Spl_Costs_Save_Path, driver);

		Thread_Sleep(2000);
		ExplicitWait_Element_Visible(locatorType, Spl_save_Message_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed-3");

		// Inventory
		Click(locatorType, Spl_Inventory_Path, driver);
		Thread_Sleep(1000);
		ExplicitWait_Element_Clickable(locatorType, Spl_Inventory_AddprojectTxt_Path, driver);

		// ExplicitWait_Element_Clickable(locatorType, Spl_Prgs_AddComment_Path,
		// driver);

		Type(locatorType, Spl_Inventory_AddprojectTxt_Path, "QA_MULTI", driver);
		Thread_Sleep(1500);

		Click(locatorType, Spl_Inventory_Multifunction_Path, driver);
		Thread_Sleep(750);

		Click(locatorType, Spl_Inventory_Addproject_Path, driver);
		Thread_Sleep(2000);

		Click(locatorType, Spl_create_Btn_Path, driver);
		Thread_Sleep(2000);

		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed-4");
		Thread_Sleep(2000);

		Ticketnumber = Get_Text(locatorType, Spl_Ticketnum_path, driver);
		Reporter.log("Ticket Number is " + Ticketnumber);

		Click(locatorType, Spl_History_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, Spl_History_Date_Time_Path, driver).contains(Get_Todaydate("M/d/YYYY")),
				"<Today's Date MM/DD/YY  HH:MM:SS AM/PM> is not displays under 'Date/Time'");
		Assert.assertTrue(Get_Text(locatorType, Spl_History_User_Path, driver).equalsIgnoreCase("Priyanka fnu"),
				"<First Name Last Name> is not displays under 'User' column ");
		Assert.assertTrue(
				Get_Text(locatorType, Spl_History_Details_Path, driver)
						.contains("Created On " + Get_Todaydate("M/d/YYYY")),
				"<Created On MM/DD/YY  HH:MM:SS AM/PM> is not displays under 'Details'  column "
						+ Get_Text(locatorType, Spl_History_Details_Path, driver));

		// to cancel the Ticket
		Click(locatorType, Spl_ProgressFiles_Path, driver);

		Click(locatorType, Spl_Prgs_AddComment_Path, driver);

		Type(locatorType, Spl_Prgs_Comment_Path, Supportticket_Commment, driver);
		Click(locatorType, Spl_Prgs_Type_arrow, driver);
		Thread.sleep(1000);

		Click(locatorType, Spl_Prgs_Type_cancel, driver);
		Thread.sleep(1000);

		Click(locatorType, Spl_Prgs_Submit_btn, driver);
		Thread.sleep(2000);

		Click(locatorType, Spl_create_Btn_Path, driver);

		ManageSplPrjNav();
		Thread.sleep(2000);

		Click(locatorType, Spl_Closed_checkbox_Path, driver);
		Thread.sleep(2000);

		Assert.assertTrue(Get_Text(locatorType,
				"//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]/a", driver).trim()
						.equalsIgnoreCase(Ticketnumber),
				"Ticket is not Showing up in Closed ");

		logout();
	}

	// PL.TC.2.10.1.13

	@Test(enabled = false, priority = 7)
	public void PL_TC_2_10_1_13() throws InterruptedException, IOException {

		login(SalesdeskUserName, SalesdeskPassword);
		ManageSplPrjNav();
		Click(locatorType, Spl_AddTicket_Path, driver);

		// Type(locatorType, , UserName, driver);
		ExplicitWait_Element_Clickable(locatorType, Spl_Cancel_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Spl_GeneralTab_jobtitle_Path, driver),
				"General Tab Not Opened");
		Thread_Sleep(2000);

		Assert.assertTrue(Get_Text(locatorType, Spl_GeneralTab_jobtitle, driver).contains("Job Title:"),
				"Job Title: field is Abscent");
		Assert.assertTrue(Get_Text(locatorType, Spl_GeneralTab_jobtitle_Star, driver).equalsIgnoreCase("*"),
				"'*' is not displays by 'Job Title' field");

		Type(locatorType, Spl_GeneralTab_jobtitle_Path, Supportticket_Commment, driver);

		Assert.assertTrue(Get_DropDown(locatorType, Spl_JobType_Path, driver).equalsIgnoreCase("- Select -"),
				"- Select - is not displays in 'Job Type' dropdown field");
		Assert.assertTrue(Get_Text(locatorType, Spl_GeneralTab_jobtype_Star, driver).equalsIgnoreCase("*"),
				"'*' is not displays by 'Job type' field");

		DropDown_VisibleText(locatorType, Spl_JobType_Path, "Other", driver);
		Assert.assertTrue(Get_Text(locatorType, Spl_Description_Path, driver).isEmpty(),
				"Description field is not blank");

		Type(locatorType, Spl_Description_Path, "QA is Testing", driver);

		Type(locatorType, Spl_QtyOfAddressee_Path, "1", driver);
		Assert.assertTrue(
				Get_DropDown(locatorType, Spl_Shipping_Dropdown_Path, driver).equalsIgnoreCase("- Please Select -"),
				"- Select - is not displays in 'Shipping/Post Service' dropdown field");

		DropDown_VisibleText(locatorType, Spl_Shipping_Dropdown_Path, "UPS Ground", driver);

		// Assert.assertTrue(Get_Text(locatorType, Spl_Createdby_Path,
		// driver).contains("Priyanka Fnu"),
		// "Created By field is Mismatching");

		Assert.assertTrue(Get_Attribute(locatorType, Spl_CostCenter_Input_Path, "value", driver).contains("Select"),
				"Select is not displayed in the Cost center field");
		Click(locatorType, Spl_CostCenter_Arrow_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("99999"), driver);
		Thread_Sleep(1000);

		Assert.assertTrue(Get_Attribute(locatorType, Spl_SubAccount_Input_Path, "value", driver).contains("Select"),
				"Select is not displayed in the Sub Account field");
		Click(locatorType, Spl_SubAccount_Arrow_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Spl_SubAccount_LI_NS_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Spl_SubAccount_Arrow_Path, driver);
		Thread_Sleep(750);

		Type(locatorType, Spl_OnBehalfOf_Path, Supportticket_Commment, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_Description_Path, driver).isEmpty(),
				"Special Instruction field is not blank");
		Type(locatorType, Spl_SpecialInstructions_Path, Supportticket_Commment, driver);
		Assert.assertFalse(Element_Isselected(locatorType, Spl_Rush_chkbox_Path, driver),
				"Is Rush checkbox is checked");
		Click(locatorType, Spl_Rush_chkbox_Path, driver);

		Assert.assertTrue(Element_Isselected(locatorType, Spl_Quote_chkbox_Path, driver),
				"Is Quote needed checkbox is not checked");

		// Click(locatorType, SplProjects_Quoteneeded_Path, driver);
		Click(locatorType, Spl_create_Btn_Path, driver);
		Thread_Sleep(2000);

		ExplicitWait_Element_Visible(locatorType, Spl_save_Message_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed -1");

		// ProgresFiles

		Click(locatorType, Spl_ProgressFiles_Path, driver);
		Thread_Sleep(2000);

		ExplicitWait_Element_Clickable(locatorType, Spl_Prgs_AddComment_Path, driver);

		Click(locatorType, Spl_Prgs_AddComment_Path, driver);
		Thread_Sleep(2000);

		ExplicitWait_Element_Clickable(locatorType, Spl_Prgs_Submit_btn, driver);

		Type(locatorType, Spl_Prgs_Comment_Path, Supportticket_Commment, driver);

		Click(locatorType, Spl_Prgs_Type_arrow, driver);
		Thread_Sleep(1000);
		Click(locatorType, li_value("Acknowledged"), driver);
		Thread_Sleep(1000);

		// Click(locatorType, Spl_Prgs_BrowseFiles_btn, driver);
		Thread_Sleep(1500);

		// Fileupload(Spl_Prgs_BrowseFiles1, driver);
		Thread_Sleep(1500);

		Click(locatorType, Spl_Prgs_Submit_btn, driver);
		Thread_Sleep(1500);
		Click(locatorType, Spl_Costs_Save_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed-2");

		// Cost Tab
		/*	Click(locatorType, Spl_Costs_Path, driver);
			Thread_Sleep(1500);
		
			ExplicitWait_Element_Clickable(locatorType, Spl_Costs_AddNewRecordButton_Path, driver);
		
			Type(locatorType, Spl_Costs_QuoteCost_Path, "0.1", driver);
			Type(locatorType, Spl_Costs_PrintCost_Path, "0.1", driver);
			Type(locatorType, Spl_Costs_PostageCost_Path, "0.1", driver);
			Type(locatorType, Spl_Costs_FulfillmentCost_Path, "0.1", driver);
			Click(locatorType, Spl_Costs_Billed_Path, driver);
			Thread_Sleep(800);
		
			Type(locatorType, Spl_Costs_ClientPostageCost_Path, "0.1", driver);
			Type(locatorType, Spl_Costs_RushCost_Path, "0.1", driver);
		
		
			Click(locatorType, Spl_Costs_AddNewRecordButton_Path, driver);
		
			Thread_Sleep(2000);
		
			Type(locatorType, Spl_Costs_Record_Description_Path, "QA Test", driver);
			DropDown_VisibleText(locatorType, Spl_Costs_Record_Type_Path, "Veritas Postage", driver);
		
			Type(locatorType, Spl_Costs_Record_dateInput_Path, Get_Futuredate("M/d/YYYY"), driver);
			Type(locatorType, Spl_Costs_Record_BillAmount_Path, "0.00", driver);
		
			Click(locatorType, Spl_Costs_Record_Save_Path, driver);
			Thread_Sleep(2000);
			ExplicitWait_Element_Visible(locatorType, Spl_save_Message_Path, driver);
			Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
					"'Data has been saved' message is not displayed"); 
					*/

		// Inventory
		Click(locatorType, Spl_Inventory_Path, driver);
		Thread_Sleep(1000);
		ExplicitWait_Element_Clickable(locatorType, Spl_Inventory_AddprojectTxt_Path, driver);

		// ExplicitWait_Element_Clickable(locatorType, Spl_Prgs_AddComment_Path,
		// driver);

		Type(locatorType, Spl_Inventory_AddprojectTxt_Path, "QA_MULTI", driver);
		Thread_Sleep(1500);

		Click(locatorType, Spl_Inventory_Multifunction_Path, driver);
		Thread_Sleep(750);

		Click(locatorType, Spl_Inventory_Addproject_Path, driver);
		Thread_Sleep(2000);

		Click(locatorType, Spl_create_Btn_Path, driver);
		Thread_Sleep(2000);

		Assert.assertTrue(Get_Text(locatorType, Spl_save_Message_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed-3");
		Thread_Sleep(2000);

		Ticketnumber = Get_Text(locatorType, Spl_Ticketnum_path, driver);
		Reporter.log("Ticket Number is " + Ticketnumber);

		Click(locatorType, Spl_History_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, Spl_History_Date_Time_Path, driver).contains(Get_Todaydate("M/d/YYYY")),
				"<Today's Date MM/DD/YY  HH:MM:SS AM/PM> is not displays under 'Date/Time'");
		Assert.assertTrue(Get_Text(locatorType, Spl_History_User_Path, driver).equalsIgnoreCase("Priyanka Salesdesk"),
				"<First Name Last Name> is not displays under 'User' column ");
		Assert.assertTrue(
				Get_Text(locatorType, Spl_History_Details_Path, driver)
						.contains("Created On " + Get_Todaydate("M/d/YYYY")),
				"<Created On MM/DD/YY  HH:MM:SS AM/PM> is not displays under 'Details'  column "
						+ Get_Text(locatorType, Spl_History_Details_Path, driver));

		// to cancel the Ticket
		Click(locatorType, Spl_ProgressFiles_Path, driver);

		Click(locatorType, Spl_Prgs_AddComment_Path, driver);

		Type(locatorType, Spl_Prgs_Comment_Path, Supportticket_Commment, driver);
		Click(locatorType, Spl_Prgs_Type_arrow, driver);
		Thread.sleep(1000);

		Click(locatorType, Spl_Prgs_Type_cancel, driver);
		Thread.sleep(1000);

		Click(locatorType, Spl_Prgs_Submit_btn, driver);
		Thread.sleep(2000);

		Click(locatorType, Spl_create_Btn_Path, driver);

		ManageSplPrjNav();
		Thread.sleep(2000);

		Click(locatorType, Spl_Closed_checkbox_Path, driver);
		Thread.sleep(2000);

		Assert.assertTrue(Get_Text(locatorType,
				"//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]/a", driver).trim()
						.equalsIgnoreCase(Ticketnumber),
				"Ticket is not Showing up in Closed ");
	}

	@BeforeTest
	public void Aspl_ogin() throws IOException, InterruptedException {
		login(UserName, Password);

	}

	@AfterTest(enabled = false)
	public void Outlog() throws IOException, InterruptedException {
		logout();
	}
}
