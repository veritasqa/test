package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class FF_SubmitCanelRequest extends BaseTest {

	@Test(enabled = true)
	public void PL_TC_2_7_1_2_1() throws IOException, InterruptedException {
		System.out.println(Get_Futuredate("MMM"));
		System.out.println(Get_Futuredate("d"));
		login(UserName, Password);
		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click_On_Element(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1),
				Part1 + " is not displayed");
		Click_On_Element(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click_On_Element(locatorType, Checkoutbtn_Path, driver);
		PlaceanOrder("PL_TC_2_7_1_1_1");

	}

	@Test(enabled = true, dependsOnMethods = "PL_TC_2_7_1_2_1")
	public void PL_TC_2_7_1_2_2() {
		Click_On_Element(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		Assert.assertTrue(isAlertPresent(), "Pop is not displayed upon clicking 'Submit Cancel Request'");
		Assert.assertTrue(Alert_Text(driver).equalsIgnoreCase("Do you want to cancel this order?"),
				"'Do you want to cancel this order?' message is not displayed in the popup");
		Accept_Alert(driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderCancel_Popup_Path, driver),
				"Order cancel Popup Message is not displayed");
		Assert.assertTrue(
				Get_Text(locatorType, OrderCancel_Popupmsg_Path, driver)
						.equalsIgnoreCase("An order cancel request has been submitted."),
				"An order cancel request has been submitted. message is not displayed in the pop up");
		Click_On_Element(locatorType, OrderCancel_Popup_Close_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Announcement_view_btn_Path, driver),
				"Home Page is not Displayed");
		Click_On_Element(locatorType, Support_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, ST_VT_OrderNo1_Path, driver).equalsIgnoreCase(OrderNumber),
				"Cancelled Order No is not displayed in the Support Ticket");
		Assert.assertTrue(Element_Is_Displayed(locatorType, ST_VT_TicketNo1_Path, driver),
				"Ticket Number is not displayed");
		TicketNumber = Get_Text(locatorType, ST_VT_TicketNo1_Path, driver);
		Reporter.log("Ticket Number is " + TicketNumber);

	}

	@Test(enabled = true, dependsOnMethods = "PL_TC_2_7_1_2_2")
	public void PL_TC_2_7_1_2_3() {

		Assert.assertTrue(Get_Text(locatorType, ST_VT_OrderNo1_Path, driver).equalsIgnoreCase(OrderNumber),
				"Canceled Order No is not displayed in the Support Ticket");
		Assert.assertTrue(Element_Is_Displayed(locatorType, ST_VT_TicketNo1_Path, driver),
				"Ticket Number is not displayed");
		Assert.assertTrue(Get_Text(locatorType, ST_VT_SubCat1_Path, driver).equalsIgnoreCase("Cancel Order"),
				"Cancel order is not displayed under Sub Category for the cancelled orderin the Support Ticket");
		Assert.assertTrue(Get_Text(locatorType, ST_VT_Status1_Path, driver).equalsIgnoreCase("New"),
				"Status New is not displayed under Status for the cancelled orderin the Support Ticket");
	}

	@Test(enabled = true, dependsOnMethods = "PL_TC_2_7_1_2_3")
	public void PL_TC_2_7_1_2_4() {

		Click_On_Element(locatorType, ST_VT_Edit1_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, STM_UpdateTicketbtn_Path, driver);
		Click_On_Element(locatorType, STM_Addcommenticon_Path, driver);
		Clear_Type_Charecters(locatorType, Fullfilment_Search_Path, Part1, driver);
		ExplicitWait_Element_Not_Visible(locatorType, STM_Addcommentloadingpanel_Path, driver);
		Clear_Type_Charecters(locatorType, STM_Commenttext_Path, "QA Test", driver);
		Click_On_Element(locatorType, STM_Createcomment_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, STM_Addcommentloadingpanel_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, STM_Comment1_Path, driver).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Comment section");
		Click_On_Element(locatorType, STM_Closedstatus_Path, driver);
		Thread_Sleep(1000);
		Assert.assertTrue(Element_Is_Displayed(locatorType, STM_TicketSuccefullyUpdatedmsg_Path, driver),
				"Ticket Succefully Updated message is not displayed");

		Click_On_Element(locatorType, Support_Path, driver);

		Assert.assertFalse(Get_Text(locatorType, ST_VT_TicketNo1_Path, driver).equalsIgnoreCase(TicketNumber),
				"Ticket Number is displayed");
		Clear_Type_Charecters(locatorType, ST_Statustxt_Path, "Closed", driver);
		Click_On_Element(locatorType, ST_Statusfilter_Path, driver);
		Thread_Sleep(1000);

		// Need to add filter options here

		Click_On_Element(locatorType, Support_Contains_Path, driver);
		Thread_Sleep(1000);
		Assert.assertTrue(Get_Text(locatorType, ST_VT_TicketNo1_Path, driver).equalsIgnoreCase(TicketNumber),
				"Ticket Number is not displayed after the filter");

	}

}