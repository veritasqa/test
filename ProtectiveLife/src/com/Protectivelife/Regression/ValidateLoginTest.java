package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;


public class ValidateLoginTest extends BaseTest{
	
	@Test
	public void PI_TC_1_0_1_2() throws IOException, InterruptedException {
		
		
		
		/* Clear_Type_Charecters(locatorType, Username_Path, UserName, driver);
	 		Clear_Type_Charecters(locatorType, Password_Path, Password, driver);
	 		Click(locatorType, Login_btn_Path, driver);
	 		
	 		Assert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));
			ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);*/

	 		//Implicit_Wait(driver);
		login(UserName,Password);

 		Assert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));

		
	}
	
	/* @BeforeTest
	 public void Aspl_ogin() throws IOException, InterruptedException{
	 	login(UserName,Password);

	 } */
	 
		@AfterTest(enabled = true)
		public void Afterclass() throws IOException, InterruptedException {
			logout();
		}


}
