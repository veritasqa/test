package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class FF_BackOrder extends BaseTest {

	@Test
	public void PL_TC_2_7_7_3() {
		Clearcart();
		Type(locatorType, Fullfilment_Search_Path, "QA_Backorder", driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains("QA_Backorder"),
			"QA_Backorder" + " is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Stock_Availability, driver).contains(outstock_Msg1),
				outstock_Msg1 + " is not displayed");
		Assert.assertTrue(Get_Text(locatorType, units_Availability, driver).contains(outstock_Msg2),
				outstock_Msg2 + " is not displayed");

	}

	@BeforeTest
	public void Inlg() throws IOException, InterruptedException {
		login(UserName, Password);

	}

	@AfterTest(enabled = false)
	public void Outog() throws IOException, InterruptedException {
		logout();
	}

}
