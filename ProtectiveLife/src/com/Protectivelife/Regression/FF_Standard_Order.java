package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class FF_Standard_Order extends BaseTest {

	@Test(enabled = false, priority = 1)
	public void PL_TC_2_7_1_1_1() throws IOException, InterruptedException {

		// Add a part to cart from the fulfillment search product page and
		// complete order - Admin Cost Center

		login(UserName, Password);
		Clearcart();
		Type(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		// ExplicitWait_Element_Clickable(locatorType,
		// Searchbymaterial_Searchbtn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).toUpperCase().contains(Part1),
				Part1 + " is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		PlaceanOrder("PL_TC_2_7_1_1_1");
		logout();

	}

	@Test(enabled = false, priority = 2)
	public void PL_TC_2_7_1_1_15() throws IOException, InterruptedException {

		// Validate child items are not appearing on Order Confirmation page and
		// in order confirmation email for (Inadmin)

		login(InUserName, InPassword);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, "QA_", driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		// ExplicitWait_Element_Clickable(locatorType,
		// Searchbymaterial_Searchbtn_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part18, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field");
		// Assert.assertTrue(Get_Text(locatorType, Unitsonhand_Path,
		// driver).contains("On Hand = 100"),
		// "'On Hand = 100' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field for the part " + Part19);
		Assert.assertTrue(Get_Text(locatorType, POD_Path, driver).contains("POD"), "'POD' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part20, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'(1)' is not displaying by 'Checkout' button");
		PlaceanOrder("PL_TC_2_7_1_1_15");
		ExplicitWait_Element_Clickable(locatorType, OrderConf_OK_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part20),
				"'QA_Kitonfly_2'is not appears under 'Order Details' grid ");
		logout();

	}

	@Test(enabled = false, priority = 3)
	public void PL_TC_2_7_1_1_16() throws IOException, InterruptedException {

		// Validate child items are not appearing on Order Confirmation page and
		// in order confirmation email for (mktadmin)

		login(MarketingkUserName, MarketingPassword);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, "QA_", driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		// ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part18, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field");
		Wait_ajax();
		// Assert.assertTrue(Get_Text(locatorType, Unitsonhand_Path,
		// driver).contains("On Hand = 100"),
		// "'On Hand = 100' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part21, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field for the part " + Part19);
		Assert.assertTrue(Get_Text(locatorType, POD_Path, driver).contains("POD"), "'POD' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'(1)' is not displaying by 'Checkout' button");
		PlaceanOrder("PL_TC_2_7_1_1_16");
		ExplicitWait_Element_Clickable(locatorType, OrderConf_OK_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		logout();

	}

	@Test(enabled = false, priority = 4)
	public void PL_TC_2_7_1_1_17() throws IOException, InterruptedException {

		// Validate child items are not appearing on Order Confirmation page and
		// in order confirmation email for (salesdesk admin)

		login(SalesdeskUserName, SalesdeskPassword);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, "QA_", driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		// ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part18, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field");
		// Assert.assertTrue(Get_Text(locatorType, Unitsonhand_Path,
		// driver).contains("On Hand = 100"),
		// "'On Hand = 100' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part21, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field for the part " + Part19);
		Assert.assertTrue(Get_Text(locatorType, POD_Path, driver).contains("POD"), "'POD' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'(1)' is not displaying by 'Checkout' button");
		PlaceanOrder("PL_TC_2_7_1_1_17");
		ExplicitWait_Element_Clickable(locatorType, OrderConf_OK_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		logout();
	}

	@Test(enabled = false, priority = 5)
	public void PL_TC_2_7_1_1_20() throws IOException, InterruptedException {

		// Verify max order quantity and Firm field on contact page (Admin)

		login(UserName, Password);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, Part16, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		Type(locatorType, FirstName_Path, Firstname_Firmtest, driver);
		Type(locatorType, LastName_Path, Lastname_Firmtest, driver);
		Click(locatorType, Search_btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, Records_Per_Page_Path, driver);
		Click(locatorType, Edit_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("AIG RETIREMENT ADVISORS, INC."), driver);
		// softAssert.assertFalse(Element_IsEnabled(locatorType,
		// ContactEdit_Firm_notlisted_Path, driver),
		// "'Firm(If not listed in drop down)' is not disabled after the firm is
		// selecte from the firm dropdown");
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("- - - Select a Firm - - -"), driver);
		Type(locatorType, ContactEdit_Firm_notlisted_Path, "Test Firm", driver);
		Click(locatorType, ContactEdit_Updatebtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ".//td[text()='" + ContactName_Firmtest + "']", driver);
		// ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part16), Part16 + " is not displayed");
		Type(locatorType, AddtoCart_Qty_Path, "501", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(800);
		// softAssert.assertTrue(Get_Text(locatorType,
		// AddtoCart_maxQtymessage_Path, driver).trim().equalsIgnoreCase(
		// "Max Order Quantity is 500"), "Max Order Quantity is 500 message is
		// not not appeared");
		Type(locatorType, AddtoCart_Qty_Path, "1", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1000);
		softAssert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver).trim()
				.equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1 message is not appears");
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		// Assert.assertTrue(Get_Attribute(locatorType, SC_Company_Path,
		// "value", driver).contains("Test"),
		// "'Test' is not displays in 'Company' field On 'Shipping Information'
		// Page");
		PlaceanOrder("PL_TC_2_7_1_1_20");
		// Assert.assertTrue(Get_Text(locatorType, OrderConf_ShippedTo_Path,
		// driver).trim().contains("Test Firm"),
		// "'Test' is not displayed in 'Shipped Address'");
		logout();
		// softAssert.assertAll();

	}

	@Test(enabled = false, priority = 6)
	public void PL_TC_2_7_1_1_21() throws IOException, InterruptedException {

		// Verify max order quantity and Firm field on contact page (InAdmin)

		login(InUserName, InPassword);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, Part26, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		Type(locatorType, FirstName_Path, Firstname_Firmtest, driver);
		Type(locatorType, LastName_Path, Lastname_Firmtest, driver);
		Click(locatorType, Search_btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, Records_Per_Page_Path, driver);
		Click(locatorType, Edit_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("AIG RETIREMENT ADVISORS, INC."), driver);
		// softAssert.assertFalse(Element_IsEnabled(locatorType,
		// ContactEdit_Firm_notlisted_Path, driver),
		// "'Firm(If not listed in drop down)' is not disabled after the firm is
		// selecte from the firm dropdown");
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("- - - Select a Firm - - -"), driver);
		Type(locatorType, ContactEdit_Firm_notlisted_Path, "Test Firm1", driver);
		Click(locatorType, ContactEdit_Updatebtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ".//td[text()='" + ContactName_Firmtest + "']", driver);
		Click(locatorType, ".//td[text()='" + ContactName_Firmtest + "']", driver);
		// ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part26), Part26 + " is not displayed");
		Type(locatorType, AddtoCart_Qty_Path, "501", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(800);
		// softAssert.assertTrue(Get_Text(locatorType,
		// AddtoCart_maxQtymessage_Path, driver).trim().equalsIgnoreCase(
		// "Max Order Quantity is 500"), "Max Order Quantity is 500 message is
		// not not appeared");
		Type(locatorType, AddtoCart_Qty_Path, "1", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1000);
		softAssert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver).trim()
				.equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1 message is not appears");
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		// Assert.assertTrue(Get_Attribute(locatorType, SC_Company_Path,
		// "value", driver).contains("Test"),
		// "'Test' is not displays in 'Company' field On 'Shipping Information'
		// Page");
		PlaceanOrder("PL_TC_2_7_1_1_21");
		// Assert.assertTrue(Get_Text(locatorType, OrderConf_ShippedTo_Path,
		// driver).trim().contains("Test Firm1"),
		// "'Test' is not displayed in 'Shipped Address'");
		logout();
		// softAssert.assertAll();
	}

	@Test(enabled = false, priority = 7)
	public void PL_TC_2_7_1_1_22() throws IOException, InterruptedException {

		// Verify max order quantity and Firm field on contact page (mktAdmin)

		login(MarketingkUserName, MarketingPassword);
		Clearcart();

		Type(locatorType, Fullfilment_Search_Path, Part27, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		Type(locatorType, FirstName_Path, Firstname_Firmtest, driver);
		Type(locatorType, LastName_Path, Lastname_Firmtest, driver);
		Click(locatorType, Search_btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, Records_Per_Page_Path, driver);
		Click(locatorType, Edit_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("AIG RETIREMENT ADVISORS, INC."), driver);
		// softAssert.assertFalse(Element_IsEnabled(locatorType,
		// ContactEdit_Firm_notlisted_Path, driver),
		// "'Firm(If not listed in drop down)' is not disabled after the firm is
		// selecte from the firm dropdown");
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("- - - Select a Firm - - -"), driver);
		Type(locatorType, ContactEdit_Firm_notlisted_Path, "Test Firm2", driver);
		Click(locatorType, ContactEdit_Updatebtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ".//td[text()='" + ContactName_Firmtest + "']", driver);
		// ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part27), Part27 + " is not displayed");
		Type(locatorType, AddtoCart_Qty_Path, "501", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1000);
		// softAssert.assertTrue(Get_Text(locatorType,
		// AddtoCart_maxQtymessage_Path, driver).trim()
		// .equalsIgnoreCase("Max Order Quantity is 500"), "Max Order Quantity
		// is 500 message is not appeared");
		Type(locatorType, AddtoCart_Qty_Path, "1", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1000);
		softAssert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver).trim()
				.equalsIgnoreCase("Successfully Added 1!"), "Successfully Added 1 message is not appears");
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		// Assert.assertTrue(Get_Attribute(locatorType, SC_Company_Path,
		// "value", driver).contains("Test"),
		// "'Test' is not displays in 'Company' field On 'Shipping Information'
		// Page");
		PlaceanOrder("PL_TC_2_7_1_1_22");
		// Assert.assertTrue(Get_Text(locatorType, OrderConf_ShippedTo_Path,
		// driver).trim().contains("Test Firm2"),
		// "'Test' is not displayed in 'Shipped Address'");
		logout();
		// softAssert.assertAll();

	}

	@Test(enabled = false, priority = 8)
	public void PL_TC_2_7_1_1_23() throws IOException, InterruptedException {

		// Verify max order quantity and Firm field on contact page (salesdesk)

		login(SalesdeskUserName, SalesdeskPassword);
		Clearcart();
		Type(locatorType, Fullfilment_Search_Path, Part28, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		Type(locatorType, FirstName_Path, Firstname_Firmtest, driver);
		Type(locatorType, LastName_Path, Lastname_Firmtest, driver);
		Click(locatorType, Search_btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, Records_Per_Page_Path, driver);
		Click(locatorType, Edit_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("AIG RETIREMENT ADVISORS, INC."), driver);
		// softAssert.assertFalse(Element_IsEnabled(locatorType,
		// ContactEdit_Firm_notlisted_Path, driver),
		// "'Firm(If not listed in drop down)' is not disabled after the firm is
		// selecte from the firm dropdown");
		Click(locatorType, ContactEdit_Firm, driver);
		Doubleclick(locatorType, li_value("- - - Select a Firm - - -"), driver);
		Type(locatorType, ContactEdit_Firm_notlisted_Path, "Test Firm3", driver);
		Click(locatorType, ContactEdit_Updatebtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, ContactEdit_Loadingpanel_Path, driver);
		Click(locatorType, ".//td[text()='" + ContactName_Firmtest + "']", driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part28), Part28 + " is not displayed");
		Type(locatorType, AddtoCart_Qty_Path, "501", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(800);
		Type(locatorType, AddtoCart_Qty_Path, "1", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1200);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		PlaceanOrder("PL_TC_2_7_1_1_22");
		logout();

	}

	@Test(enabled = false, priority = 9)
	public void PL_TC_2_7_1_1_18() throws IOException, InterruptedException {

		// Validate child items are not appearing on Order Confirmation page and
		// in order confirmation email for (Hotlink for Agent)
		// Usertype="SSO";
		SSOLogin(Crm_Agent_Login);
		Assert.assertTrue(Element_Is_Displayed(locatorType, HomepageLogo_Path, driver),
				"Protective life homepage logo is not displayed");
		Type(locatorType, Fullfilment_Search_Path, "QA_", driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part18, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field");
		// Assert.assertTrue(Get_Text(locatorType, Unitsonhand_Path,
		// driver).contains("On Hand = 100"),
		// "'On Hand = 100' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part21, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field for the part " + Part21);
		Assert.assertTrue(Get_Text(locatorType, POD_Path, driver).contains("POD"), "'POD' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'Checkout (1)' is not displaying by 'Checkout' button");
		PlaceanOrder("PL_TC_2_7_1_1_18");
		ExplicitWait_Element_Clickable(locatorType, OrderConf_OK_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		logout();

	}

	@Test(enabled = false, priority = 10)
	public void PL_TC_2_7_1_1_19() throws IOException, InterruptedException {

		// Validate child items are not appearing on Order Confirmation page and
		// in order confirmation email for (Hotlink for Agent)
		// Usertype="SSO";
		SSOLogin(Crm_Login);
		Assert.assertTrue(Element_Is_Displayed(locatorType, HomepageLogo_Path, driver),
				"Protective life homepage logo is not displayed");
		Type(locatorType, Fullfilment_Search_Path, "QA_", driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		Type(locatorType, Searchbymaterial_Path, Part18, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field");
		// Assert.assertTrue(Get_Text(locatorType, Unitsonhand_Path,
		// driver).contains("On Hand = 100"),
		// "'On Hand = 100' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part21, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Assert.assertTrue(Get_Attribute(locatorType, AddtoCart_Qty_Path, "value", driver).equals("1"),
				"'1' is not displays in 'Qty' field for the part " + Part21);
		Assert.assertTrue(Get_Text(locatorType, POD_Path, driver).contains("POD"), "'POD' is not displays");
		Type(locatorType, Searchbymaterial_Path, Part19, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"'Checkout (1)' is not displaying by 'Checkout' button");
		PlaceanOrder("PL_TC_2_7_1_1_19");
		ExplicitWait_Element_Clickable(locatorType, OrderConf_OK_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, OrderConf_FormNo1_Path, driver).trim().equalsIgnoreCase(Part19),
				"'QA_Formsbook1'is not appears under 'Order Details' grid ");
		logout();

	}

	@Test(enabled = true, priority = 11)
	public void PL_TC_2_7_1_1_24() throws IOException, InterruptedException {

		// Verify max order quantity (CRMSSO)
		// Usertype="SSO";
		SSOLogin(Crm_Login);
		Assert.assertTrue(Element_Is_Displayed(locatorType, HomepageLogo_Path, driver),
				"Protective life homepage logo is not displayed");
		Type(locatorType, Fullfilment_Search_Path, Part28, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part28), Part28 + " is not displayed");
		Type(locatorType, AddtoCart_Qty_Path, "501", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1200);

	}

	@Test(enabled = false, priority = 12)
	public void PL_TC_2_7_1_1_25() throws IOException, InterruptedException {

		// Verify max order quantity (CRMSSO)
		// Usertype="SSO";

		SSOLogin(Crm_Agent_Login);
		Assert.assertTrue(Element_Is_Displayed(locatorType, HomepageLogo_Path, driver),
				"Protective life homepage logo is not displayed");
		Assert.assertTrue(
				Get_Text(locatorType, Homepage_Username_Path, driver).equalsIgnoreCase("Qaautoagent1 Automation"),
				" <Agent> is not displays by 'Contact Us' on 'Home' page");
		Type(locatorType, Fullfilment_Search_Path, Part24, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part24), Part28 + " is not displayed");
		Type(locatorType, AddtoCart_Qty_Path, "501", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1200);
	}

	@AfterMethod(enabled = false)
	public void Aftermethod() throws IOException, InterruptedException {
		// if(!Usertype.equals("SSO")){
		// logout();

	}

}
