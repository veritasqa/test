package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class Order_Lookup extends BaseTest {

	@Test
	public void PL_TC_2_6_3_1_4() throws IOException, InterruptedException {

		// Verify Recipient and Created User field is working appropraitely on
		// Order look up page (Admin, inadmin, mkt,salesdesk)

		Type(locatorType, Fullfilment_Search_Path, Part21, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Searchbtn_Path, driver);
		// System.out.println(Get_Text(locatorType, Part_Name_Path, driver));

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).contains(Part21), Part21 + " is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		Click(locatorType, Checkoutbtn_Path, driver);
		PlaceanOrder("PL_TC_2_6_3_1_4");
		Wait_ajax();
		Click(locatorType, Home_btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
		Hover_Over_Element(locatorType, Add_Widget_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, OrderLookup_Path, driver);
		Click(locatorType, OrderLookup_Path, driver);
		// Type(locatorType, Wid_Xpath(Widgetname, OrderLookup_Fromdate_Path),
		// Get_Todaydate("M/d/YYYY"),
		// driver);
		// Type(locatorType, Wid_Xpath(Widgetname, OrderLookup_Todate_Path),
		// Get_Todaydate("M/d/YYYY"),
		// driver);
		//Assert.assertTrue(false, "Order search page is not displayed");
		Click(locatorType, Wid_Xpath(Widgetname, OrderLookup_Searchbylink_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Ordersearch_Searchbtn_Path, driver);
		Assert.assertTrue(Get_Title(driver).trim().equalsIgnoreCase("Order Search | ProtectiveLife"),
				"Order Search Page is not displayed");

		Type(locatorType, Ordersearch_OrderNumber, OrderNumber, driver);

		Type(locatorType, Ordersearch_RecipientFirstName_Path, Firstname, driver);
		Type(locatorType, Ordersearch_RecipientLastName_Path, Lastname, driver);
		Type(locatorType, Ordersearch_CreatedUserFirstName_Path, Firstname, driver);
		Type(locatorType, Ordersearch_CreatedUserLastName_Path, Lastname, driver);
		Click(locatorType, Ordersearch_StartDate, driver);
		Wait_ajax();
		Datepickercustom(Ordersearch_StartDateMonth, Ordersearch_StartDateOk, "Jan", "30", "2018");
		Wait_ajax();
		Click(locatorType, Ordersearch_Enddate, driver);
		Wait_ajax();
		Datepickercustom(Ordersearch_Enddatemonth, Ordersearch_StartDateOk, "Jan", "30", "2018");
		Wait_ajax();
		Click(locatorType, Ordersearch_Searchbtn_Path, driver);
//		ExplicitWait_Element_Not_Visible(locatorType, Ordersearch_Loadingpanel_Path, driver);
//		Assert.assertTrue(Element_Is_Displayed(locatorType, Ordersearch_Searchgrid_Path, driver),
//				"Order Seatch Grid is not displayed");
//		Assert.assertTrue(
//				Get_Text(locatorType, Ordersearch_Recipient1_Path, driver).equalsIgnoreCase(Firstname + " " + Lastname),
//				"Recipient name is not displayed");
//		Assert.assertTrue(
//				Get_Text(locatorType, Ordersearch_Recipient2_Path, driver).equalsIgnoreCase(Firstname + " " + Lastname),
//				"Creater User name is not displayed");

	}

	@BeforeTest
	public void Inlog() throws IOException, InterruptedException {
		login(UserName, Password);

	}

	@AfterTest
	public void Outlog() throws IOException, InterruptedException {
		logout();
	}
}
