package com.Protectivelife.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class AllStateAgent extends BaseTest {

	@Test(enabled = true)
	public void PL_TC_Allstate_SSO_1_1_1() throws IOException, InterruptedException {

		// Verify Allstate SSO landing page 

		AllstateAgentLogin(Crm__Allstateagent_Login);
		ExplicitWait_Element_Clickable(locatorType, All_FinishOrder_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, All_Homelogo_Path, driver),
				"Protective life logo is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, All_OrderPrintMaterials_Path, driver),
				"Protective life logo is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, All_Part1image_Path, driver),
				"Part1 image is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, All_Part2image_Path, driver),
				"Part2 image is not displayed");
		Click(locatorType, All_Part1image_Path, driver);
		if (Get_Title(driver).contains("Download")) {
			softAssert.assertTrue(false, "Part1 image is not downloaded");
			NavigateBack(driver);
			ExplicitWait_Element_Clickable(locatorType, All_FinishOrder_Path, driver);

		}
		Click(locatorType, All_Part2image_Path, driver);
		if (Get_Title(driver).contains("Download")) {
			softAssert.assertTrue(false, "Part2 image is not downloaded");
			NavigateBack(driver);
			ExplicitWait_Element_Clickable(locatorType, All_FinishOrder_Path, driver);

		}
		// softAssert.assertAll();
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_Part1qty_Path, driver),
				"Part1 qty field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_Part2qty_Path, driver),
				"Part2 qty field is not displayed");
		softAssert.assertTrue(Get_Attribute(locatorType, All_Part1qty_Path, "value", driver).equalsIgnoreCase("0"),
				"0 is not displayed in Part1 qty field");
		softAssert.assertTrue(Get_Attribute(locatorType, All_Part2qty_Path, "value", driver).equalsIgnoreCase("0"),
				"0 is not displayed in Part2 qty field");
		// softAssert.assertAll();
		Assert.assertTrue(Element_Is_Displayed(locatorType, All_Shippinginfo_Path, driver),
				"Shipping info header is not displayed");
		softAssert.assertTrue(Get_Text(locatorType, All_Namefield_Path, driver).isEmpty(), "Name field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_Address1_Path, driver).isEmpty(),
				"Address1 field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_Address2_Path, driver).isEmpty(),
				"Address2 field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_Address3_Path, driver).isEmpty(),
				"Address3 field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_City_Path, driver).isEmpty(), "City field is not blank");
		softAssert.assertTrue(Get_Attribute(locatorType, All_Statedd_Path,"value", driver).equalsIgnoreCase("- Select State -"),
				"State dropdown field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_Zip_Path, driver).isEmpty(), "Zip field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_Phone_Path, driver).isEmpty(), "Phone field is not blank");
		softAssert.assertTrue(Get_Text(locatorType, All_Email_Path, driver).isEmpty(), "Email field is not blank");
		Type(locatorType, All_Part2qty_Path, "120", driver);
		Click(locatorType, All_FinishOrder_Path, driver);
		Thread_Sleep(2000);
		softAssert.assertTrue(
				Get_Text(locatorType, All_Part2orderlimit_Err_Path, driver)
						.contains("Please enter numeric values, less than 100."),
				"Please enter numeric values, less than 100. is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_Name_Err_Path, driver),
				"Required field error message is not displayed for Name field");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_Address1_Err_Path, driver),
				"Required field error message is not displayed for Address 1 field");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_City_Err_Path, driver),
				"Required field error message is not displayed for City field");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_State_Err_Path, driver),
				"Required field error message is not displayed for State field");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, All_Zip_Err_Path, driver),
				"Required field error message is not displayed for Zip field");
		Type(locatorType, All_Part2qty_Path, "", driver);
		Click(locatorType, All_FinishOrder_Path, driver);
		Thread_Sleep(2000);
		softAssert.assertTrue(
				Get_Text(locatorType, All_Part2orderlimit_Err_Path, driver)
						.contains("Please enter numeric values, less than 100."),
				"Please enter numeric values, less than 100. is not displayed");
		softAssert.assertAll();
	}

	@Test(enabled = true)
	public void PL_TC_Allstate_SSO_1_1_2() throws IOException, InterruptedException {

		// Verify user is able to place an order through Allstate Page

		AllstateAgentLogin(Crm__Allstateagent_Login);
		ExplicitWait_Element_Clickable(locatorType, All_FinishOrder_Path, driver);
		Type(locatorType, All_Part2qty_Path, "20", driver);
		Type(locatorType, All_Namefield_Path, "Veritas", driver);
		Type(locatorType, All_Address1_Path, Address1, driver);
		Type(locatorType, All_City_Path, City, driver);
		DropDown_VisibleText(locatorType, All_Statedd_Path, State, driver);
		Type(locatorType, All_Zip_Path, Zipcode, driver);
		Type(locatorType, All_Phone_Path, "1111111111", driver);
		Type(locatorType, All_Email_Path, EmailId, driver);
		Click(locatorType, All_FinishOrder_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Element_Is_Displayed(locatorType, All_Successfullorder_Msg_Path, driver),
				"Successfull order message is not displayed");
		if (Get_Text(locatorType, All_Successfullorder_Msg_Path, driver).contains("Your order number is")) {
			String Ordermessage = (Get_Text(locatorType, All_Successfullorder_Msg_Path, driver));
			System.out.println(Ordermessage);
			char[] Getordernumber = new char[10];
			Ordermessage.getChars(Ordermessage.indexOf("is") + 3, Ordermessage.length() - 1, Getordernumber, 0);
			String AllstateOrderNumber = new String(Getordernumber);
			Open_Url_Window_Max(Inventory_URL, driver);
			Type(locatorType, Inventory_Username_Path, Inventory_UserName, driver);
			Type(locatorType, Inventory_Passowrd_Path, Inventory_Password, driver);
			Click(locatorType, Inventory_Submit_Btn_Path, driver);
			ExplicitWait_Element_Visible(locatorType, Inventory_OrderSearch_Path, driver);
			Click(locatorType, Inventory_OrderSearch_Path, driver);
			ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path, driver);
			Type(locatorType, Inventory_Ordernumber_Path, AllstateOrderNumber, driver);
			Click(locatorType, Inventory_Search_Btn_Path, driver);
			Click(locatorType, Inventory_Edit_Btn_Path, driver);
			Click(locatorType, Inventory_Cancelorder_Btn_Path, driver);
			Thread_Sleep(2000);
			Accept_Alert(driver);
			Assert.assertTrue(
					Get_Attribute(locatorType, Inventory_Order_Status, "Value", driver).equalsIgnoreCase("Cancelled"),
					"Order is not cancelled is not displayed");
			Reporter.log("Order Placed Successfully");
			Reporter.log("Order Number placed in method " + "PL_TC_Allstate_SSO_1_1_2" + " is '" + AllstateOrderNumber
					+ "'");
			Reporter.log(AllstateOrderNumber + " has been cancelled successfully");

		}

		else {

			Assert.assertTrue(false, "Order number is not generated");
		}

		// System.out.println(AllstateOrderNumber);
	}

	
}