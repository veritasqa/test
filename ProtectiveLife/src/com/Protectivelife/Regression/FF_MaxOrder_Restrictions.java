package com.Protectivelife.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Protectivelife.Base.BaseTest;

public class FF_MaxOrder_Restrictions extends BaseTest {

	@Test(enabled = true)
	public void PL_TC_2_7_6_1() throws IOException, InterruptedException, AWTException {

		// Validate max order quantity default
		
		Clearcart();
		Type(locatorType, Fullfilment_Search_Path, Part34, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);
		ContactSearch(Firstname, Lastname);
		Thread_Sleep(1200);
		Type(locatorType, AddtoCart_Qty_Path, "6", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(1200);	
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver).trim()
				.equalsIgnoreCase("Max Order Quantity is 5"), Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver)+
				"--Max Order Quantity is 5 message is not not appeared");
		Assert.assertTrue(
				Get_Text(locatorType, MiniCart_Message, driver).equalsIgnoreCase("Shopping cart is empty."),
				Get_Text(locatorType, MiniCart_Message, driver) +"--MiniShopping cart is not empty");
		Thread_Sleep(1200);
		Type(locatorType, AddtoCart_Qty_Path, "3", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		
		ExplicitWait_Element_Visible(locatorType, AddtoCart_Successfullmessage_Path, driver);

		Thread_Sleep(1200);
	//	Thread_Sleep(1200);
		Assert.assertTrue(Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver).trim()
				.equalsIgnoreCase("Successfully Added 3!"), Get_Text(locatorType, AddtoCart_Successfullmessage_Path, driver)+
				"--Successfully Added 3! message is not not appeared");
		
		Thread_Sleep(1200);
		Type(locatorType, AddtoCart_Qty_Path, "5", driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, AddtoCart_maxQtymessage_Path, driver);
		Thread_Sleep(1200);
		Assert.assertTrue(
				Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver).trim()
						.equalsIgnoreCase("Max Order Quantity is 5 (3 in cart)"),
						Get_Text(locatorType, AddtoCart_maxQtymessage_Path, driver)+
						"--Max Order Quantity is 5 (3 in cart) message is not not appeared");
		
		Click(locatorType, Clear_Cart_Path, driver);
	}
	

	@BeforeTest
	public void Inlog() throws IOException, InterruptedException {
	login(SalesdeskUserName,SalesdeskUserName);

	}

	@AfterTest(enabled=false)
	public void Outlog() throws IOException, InterruptedException{
		logout();
	}

}
