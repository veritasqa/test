package com.Protectivelife.Methods;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;


public class AssertiveMethods extends CommonMethods{
	
	public void Click(String locType, String Path, WebDriver  _driver){
		 Assert.assertTrue(Click_On_Element(locType, Path, _driver), "Element  -"+ Path +" - Not Clickable");

	}
	
	public void Type(String locType, String path, String parameter, WebDriver _driver ){
		Assert.assertTrue(Clear_Type_Charecters(locType, path, parameter, _driver),"Could not Type on the  - "+path +"- Element ");
	}
	
	
	public void Doubleclick(String locType, String Path, WebDriver  _driver){
		 Assert.assertTrue(Double_Click(locType, Path, _driver), "Couldnt double click  -"+ Path +" - Element");

	}
	
    public void Hover(String locType, String Path, WebDriver  _driver){
		 Assert.assertTrue(Hover_Over_Element(locType, Path, _driver), "Couldnt Hover Over -"+ Path +" - Element");

    }
    
    
    public void DropDown_VisibleText(String locType, String Path, String option, WebDriver _driver){
		 Assert.assertTrue(Select_DropDown_VisibleText(locType, Path, option, _driver), "Couldnt Hover Over -"+ Path +" - Element");

   }
	
    public void Alert_Accept(WebDriver _driver){
		 Assert.assertTrue(Accept_Alert(_driver),"Alert is missing");

  }
	
}
