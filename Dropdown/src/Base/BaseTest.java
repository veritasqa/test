package Base;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import Methods.CommonMethods;

public class BaseTest extends CommonMethods {

	@BeforeSuite
	public void openbrowser() throws IOException, InterruptedException {

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		Implicit_Wait();
	}

	@AfterSuite(enabled = false)
	public void Aftersuite() throws InterruptedException {

	}

	public void Selectfilter(String Filter, String Filteroption) throws InterruptedException {

		Click(Xpath, Filter);
		Wait_ajax();
		Click(Xpath, ".//span[text()='" + Filteroption + "']");
		Wait_ajax();

	}

	public void Clear() throws InterruptedException {

		if (Element_Is_Displayed(Xpath, Textpath("a", "Clear"))) {
			Click(Xpath, Textpath("a", "Clear"));
			Wait_ajax();
		}

	}

}
