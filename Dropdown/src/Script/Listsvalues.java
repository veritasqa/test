package Script;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.BaseTest;

public class Listsvalues extends BaseTest {
	
	//Senthil is testing

	@Test
	public void Listvalues() throws InterruptedException {

		MouseHover(Xpath, ".//span[text()='Admin']");
		Click(Xpath, ".//span[text()='Manage Inventory']");
		ExplicitWait_Element_Clickable(Xpath, ".//*[@id='ctl00_cphContent_btnSearch']");
		Click(Xpath, ".//*[@id='ctl00_cphContent_ddlDocumentType_Arrow']");
		Thread.sleep(2000);
		Listvalues("//*[@id=\"ctl00_cphContent_ddlDocumentType_DropDown\"]/div/ul/li");

	}

	@BeforeTest
	public void BeforeTest() {

		Type(ID, "txtUserName", "qaauto");
		Type(ID, "txtPassword", "qaauto");
		Click(ID, "btnSubmit");

	}

}
