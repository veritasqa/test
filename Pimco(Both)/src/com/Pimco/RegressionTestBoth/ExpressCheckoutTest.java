package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class ExpressCheckoutTest extends BaseTestPimco {
	
	@Test(enabled =true ,priority = 1)
	public void Pimco_TC_2_5_1_1_1() throws InterruptedException{
		Click(Xpath, Pimco_LP_EC_SelectaRecipient_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_CSP_Search_Btn_ID);
		ContactClick("QanoFirm", "IL");
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		
		Type(Xpath, Pimco_LP_EC_ProductCode_Txtbox1_Path, QA_Multifunction);
		Wait_ajax();
		Type(Xpath, Pimco_LP_EC_Qty_Txtbox1_Path, "1");
		Wait_ajax();
		Click(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");

	    Clearcarts();
	   // Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");

		
	}
	
	@Test(enabled =true ,priority = 2)
	public void Pimco_TC_2_5_1_1_2() throws InterruptedException{
	    Clearcarts();
		Click(Xpath, Pimco_LP_EC_SelectaRecipient_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_CSP_Search_Btn_ID);
		ContactClick("QAamripri", "IL");
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		 
		Type(Xpath, Pimco_LP_EC_ProductCode_Txtbox1_Path, QA_FirmRestriction);
		Wait_ajax();
		Type(Xpath, Pimco_LP_EC_Qty_Txtbox1_Path, "1");
		Wait_ajax();
		Click(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Clearcarts();

	    Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");
       
		
		Click(Xpath, Pimco_LP_EC_SelectaRecipient_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_CSP_Search_Btn_ID);
		ContactClick("QAaxa", "IL");
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		
		Type(Xpath, Pimco_LP_EC_ProductCode_Txtbox1_Path, QA_FirmRestriction);
		Wait_ajax();
		Type(Xpath, Pimco_LP_EC_Qty_Txtbox1_Path, "1");
		Wait_ajax();
		Click(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		Wait_ajax();
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Restricted by Firm")),"Restricted by Firm message is missing");

		Clearcarts();

		//Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");
			
	}
	
	
	@Test(enabled =true ,priority = 3)
	public void Pimco_TC_2_5_1_2_1() throws InterruptedException{
		Clearcarts();
		Click(Xpath, Pimco_LP_EC_SelectaRecipient_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_CSP_Search_Btn_ID);
		ContactClick("QanoFirm", "IL");
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		
		Type(Xpath, Pimco_LP_EC_ProductCode_Txtbox1_Path, QA_Multifunction);
		Wait_ajax();
		Type(Xpath, Pimco_LP_EC_Qty_Txtbox1_Path, "1");
		Wait_ajax();
		Click(Xpath, Pimco_LP_EC_CheckoutBtn_path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	  
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
			
		
	}
	
	
	
	
	@BeforeClass
	public void Widgetverify() throws InterruptedException{
	if(!Element_Is_Displayed(Xpath, WidgetNamePath("Express Checkout"))){
		Hover(Xpath, Textpath("h3", "Add Widgets"));
		Wait_ajax();
		Click(Xpath, Textpath("a","Express Checkout"));
	}
	}

	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}

}
