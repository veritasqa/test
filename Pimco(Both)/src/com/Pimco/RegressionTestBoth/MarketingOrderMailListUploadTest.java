package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingOrderMailListUploadTest  extends BaseTestPimco{

	
	//Upload a mail list with all records properly filled out
	@Test(enabled = true)
	public void Pimco_TC_2_6_1_1_1() throws InterruptedException, AWTException{
		
		   ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
			Click(ID, Pimco_LP_StartaMailing_ID);
			ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
		
			Thread.sleep(6000);
			Click(ID, Pimco_MLU_Select_File_ID);
			
			Fileupload(PIMCO_Maillist_Allfields_Path);
			Wait_ajax();
			Click(ID,Pimco_MLU_UploadMailList_ID);
			Wait_ajax();
			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "David")),"First name is missing in the first name coloumn");
			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Barr")),"Last name is missing in the last name coloumn");
			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "2730 W Tyvola Rd")),"Address1 is missing in the Adress1 coloumn");
			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Charlotte")),"City is missing in the City coloumn");
			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "NC")),"State/Province is missing in the State/Province coloumn");
			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "28217-4527")),"Zip/Postal Code is missing in the Zip/Postal Code coloumn");

			 Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "test@test.com")),"Email is missing in the Email coloumn");
			 
			 Clearcarts();
		 
	}
	
	
	//Upload a mail list with records missing information
	@Test(enabled = true)
	public void Pimco_TC_2_6_1_1_2(){
		
	}
	
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
}
