package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class SummaryProspectusTest extends BaseTestPimco {

	
	
	//Place order from fulfillment Search and verify it is displayed in Summary Prospectus widget 
	@Test(enabled = true,priority =1)
	public void Pimco_TC_2_5_6_A_V1_1_1() throws InterruptedException{
		    ManageinventoryNav();
		    Wait_ajax();
	        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Summaryprospectus);
	        
	    	Wait_ajax();
			Click(Xpath, li_value("- Any -"));
		   	Wait_ajax();
	        Click(ID, Pimco_IM_Search_Btn_ID);
	        Wait_ajax();
	        
	        Click(Xpath, ClickButtononSearchresultspath(QA_Summaryprospectus, "Edit"));
	        Wait_ajax();
		    
	        if(!Element_Is_selected(Xpath, CheckBoxXpath("Summary Prospectus"))){
	        	
	        	Click(Xpath, CheckBoxXpath("Summary Prospectus"));
	        	Wait_ajax();
		    	Click(ID, Pimco_IM_Save_Btn_ID);
				Wait_ajax();
				Click(ID, Pimco_IM_PMSG_OK_Button_ID);
				Wait_ajax();
	        }
	        
	        
		    
	        
	        
		     
	}
	
	public void ManageinventoryNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		

	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
		
	//	Clearcarts();
	}
	
}
