package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingPieceSearchTest extends BaseTestPimco{

	
	//Validate that parts with marketing mode only appear in marketing campaign search results
	@Test(enabled = true)
	public void Pimco_TC_2_6_1_1ANDPimco_TC_2_6_1_2ANDPimco_TC_2_6_1_3() throws InterruptedException, AWTException{
	
		Clearcarts();
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Marketingonly);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsAddtoDripBtnXpath(QA_Marketingonly)),"QA_Marketingonly doesnot appears on 'Materials' page");

		
		//Validate that parts with fulfillment only do not appear in marketing campaign search results
		Click(ID, Pimco_SR_Clear_Btn_ID);
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_FulfillmentOnly);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");

    	//Validate that parts with marketing and fulfillment appear in marketing campaign search results
    	Click(ID, Pimco_SR_Clear_Btn_ID);
    	Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_Multifunction)),"QA_Multifunction displays in 'Materials' page");
		Clearcarts();

	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
}
