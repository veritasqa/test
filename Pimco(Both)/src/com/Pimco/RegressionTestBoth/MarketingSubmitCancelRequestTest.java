package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingSubmitCancelRequestTest extends BaseTestPimco {

	//Validate an order is placed
	@Test
	public void PIMCO_TC_2_6_1_3_1ANDPIMCO_TC_2_6_1_3_2ANDPIMCO_TC_2_6_1_3_3ANDPIMCO_TC_2_6_1_3_4() throws InterruptedException, AWTException{
		
	Click(ID, Pimco_LP_StartaMailing_ID);
	ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);

	Thread.sleep(6000);
	Click(ID, Pimco_MLU_Select_File_ID);
	
	Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
	Wait_ajax();
	Click(ID,Pimco_MLU_UploadMailList_ID);
	Wait_ajax();
	Click(ID,Pimco_MLU_Ok_Btn_ID);
	
	Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Marketingonly);
	Click(ID, Pimco_SR_Search_Btn_ID);
  	Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Marketingonly));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath(" Marketing piece"));

	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    
        Wait_ajax();
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	   // ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		
		
		//Pimco.TC.2.6.1.3.2
		
		//Validate on confirmation page submit a cancel request and verify a support ticket is created
		
		
		
	Click(ID, Pimco_OC_SubmitCancelRequest_Btn);
		
		Thread.sleep(2000);
		Accept_Alert();
		
		Click(ID, Pimco_OC_Close_btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		//Pimco.TC.2.6.1.3.3
		
		//Validate cancel request ticket appears on support page
		
		//Pimco.TC.2.6.1.3.4
		
		//Verify cancel request ticket works appropriatel
		
		Hover(Xpath, Textpath("span","Admin"));
		Wait_ajax();
		
		Click(Xpath, Textpath("span","Support"));
		
	    ExplicitWait_Element_Clickable(ID, Pimco_SP_AddTicket_ID);

	    Click(Xpath, EditButtonOrderNumber(OrderNumber));
	    
	    Click(Xpath, Pimco_SP_AddComment_Path);
	    ExplicitWait_Element_Clickable(ID, Pimco_SP_CreateComment_Btn_ID);
	    Type(ID, Pimco_SP_Comment_TextBox_ID, "QA Test");
	    Click(ID, Pimco_SP_CreateComment_Btn_ID);
	    
	    Click(ID, Pimco_SP_Closed_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SP_TicketUpdateMsg_ID);

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_SP_TicketUpdateMsg_ID),"Ticket Succefully Updated Message NOT displays");

		Assert.assertTrue(Get_Attribute(ID, Pimco_SP_Closed_ID, "class").equalsIgnoreCase("statusBtnSelected"),"Closed is displayed under 'Status' column"); 
		
		Hover(Xpath, Textpath("span","Admin"));
		Wait_ajax();
		
		Click(Xpath, Textpath("span","Support"));
		
	    ExplicitWait_Element_Clickable(ID, Pimco_SP_AddTicket_ID);
		
	    Type(ID, Pimco_SP_Status_TextBox_ID, "Closed");
	    Click(ID, Pimco_SP_Status_Filter_ID);
	    
		ExplicitWait_Element_Visible(Xpath,Textpath("span", "Contains"));

	    Click(Xpath,Textpath("span", "Contains"));

	    
		Assert.assertTrue(Element_Is_Displayed(Xpath, EditButtonOrderNumber(OrderNumber)),"Ticket Not listed under Closed");

	    		
	
	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();
	}
	
}
