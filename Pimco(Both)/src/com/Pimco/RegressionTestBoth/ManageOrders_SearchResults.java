package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageOrders_SearchResults extends BaseTestPimco {

	public void ManageOrdersNav() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Orders"));
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Searchbtn_Path);
		Wait_ajax();

	}

	public static String PackslipNo;

	@Test(enabled = false, priority = 1)
	public void Pimco_TC_2_4_10_2_1() throws InterruptedException {

		/*
		 * Manage Order - Item limit not to exceed more than 500 in Search Results
		 * */

		ManageOrdersNav();
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Assert.assertTrue(Integer.parseInt(Get_Text(Xpath, Pimco_MO_SR_NoofItems_Path).trim()) <= 500,
				"At the end of the page on the right side items are showing above 500");
	}

	@Test(enabled = false, priority = 2)
	public void Pimco_TC_2_4_10_2_2() throws InterruptedException, IOException {

		/*
		 * Validate the ability to search Orders using addresses of Marketing order recipients 
		 */

		// Pre - Req
		Clearcarts();
		Click(Xpath, Pimco_LP_StartaMailing_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Maillist5_NoRestrictions);
		Click(ID, Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_MarketingOnly);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_MarketingOnly));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();

		// TC - Starts here

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Type(Xpath, Pimco_MO_Addressfield_Path, "2730 W Tyvola Rd");
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderNo1_Path).equalsIgnoreCase(OrderNumber),
				OrderNumber + " (Order #) from Pre-Condition is not displays in 'Search Results' grid ");
		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderType1_Path).equalsIgnoreCase("PIMCO_GWM_MARKETING"),
				"'PIMCO_GWM_MARKETING' not displays under 'Order Type' column");
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Gen_OrderSearchbtn_Path);
		Click(Xpath, Pimco_MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "2730 W Tyvola Rd")),
				"2730 W Tyvola Rd is not displayed in the mail list tab");
	}

	@Test(enabled = true, priority = 3)
	public void Pimco_TC_2_4_10_2_5() throws InterruptedException, IOException, AWTException {

		/*
		 * Verify Hyperlink tracking on Maillist tab when placed Marketing Order
		 * */

		/* Notes: Pimco (Both) Bulk Order no #: 4285502 in producion
		 *
		 */

		Robot robot = new Robot();
		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Containspath("input", "value", "PackSlip"));
		Thread.sleep(4000);
		Switch_New_Tab();
		PackslipNo = Get_Text(Xpath, Inventory_PackSlipno_Path).substring(1, 16);
		System.out.println(PackslipNo);
		Switch_Old_Tab();
		Click(Xpath, Inventory_QuickShip_Path);
		Type(Xpath, Inventory_QS_PickTicketBarcode_Path, PackslipNo);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Clickable(Xpath, Inventory_QS_ShipOrder_Path);

		Select_DropDown_VisibleText(Xpath, Inventory_QS_Shipper_Path, "USPS");
		Type(Xpath, Inventory_QS_TrackingNo_Path, "Test 1");
		Type(Xpath, Inventory_QS_ShippingCharges_Path, "0.0");
		Click(Xpath, Inventory_QS_Perrecipient_Path);
		Type(Xpath, Inventory_QS_Weight_Path, "0.0");
		Type(Xpath, Inventory_QS_Noofboxed_Path, "1");
		Click(Xpath, Inventory_QS_ShipOrder_Path);
		ExplicitWait_Element_Visible(Xpath, Containstextpath("div", "Record Updated"));

		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Containspath("input", "value", "PackSlip"));
		Thread.sleep(4000);
		Switch_New_Tab();
		PackslipNo = Get_Text(Xpath, Inventory_PackSlipno_Path).substring(1, 16);
		System.out.println(PackslipNo);
		Switch_Old_Tab();

		Click(Xpath, Inventory_QuickShip_Path);
		Type(Xpath, Inventory_QS_PickTicketBarcode_Path, PackslipNo);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Clickable(Xpath, Inventory_QS_ShipOrder_Path);
		Select_DropDown_VisibleText(Xpath, Inventory_QS_Shipper_Path, "USPS");
		Type(Xpath, Inventory_QS_TrackingNo_Path, "Test 2");
		Type(Xpath, Inventory_QS_ShippingCharges_Path, "0.0");
		Click(Xpath, Inventory_QS_Perrecipient_Path);
		Type(Xpath, Inventory_QS_Weight_Path, "0.0");
		Type(Xpath, Inventory_QS_Noofboxed_Path, "1");
		Click(Xpath, Inventory_QS_ShipOrder_Path);
		ExplicitWait_Element_Visible(Xpath, Containstextpath("div", "Record Updated"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Record Updated")),
				"Record Updated is not displayed");

		Get_URL(URL);
		login(PIMCOBoth1Username, PIMCOBoth1Password);
		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		Click(Xpath, Pimco_MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Test 1")),
				"Test 1 is not displayed in the mail list tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Test 2")),
				"Test 2 is not displayed in the mail list tab");
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Test 1"));
		Assert.assertTrue(Get_Title().toLowerCase().contains("usps.com"), "USPS.com page is not displayed - Test 1");
		NavigateBack();
		Click(Xpath, Textpath("a", "Test 2"));
		Assert.assertTrue(Get_Title().toLowerCase().contains("usps.com"), "USPS.com page is not displayed - Test 2");
		NavigateBack();
	}

	@Test(enabled = false, priority = 4)
	public void Pimco_TC_2_4_10_2_3() throws InterruptedException, IOException {

		/*
		 * Ability to search Orders using addresses of Bulk order recipients
		 * */

		// Pre - Req

		Clearcarts();
		Click(Xpath, Pimco_LP_StartaBulkOrder_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Mailist6_NoRestrictions);
		Click(ID, Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_FulfillmentMarketingBulk);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_FulfillmentMarketingBulk));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();

		// TC- Starts Here

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Type(Xpath, Pimco_MO_Addressfield_Path, "35 W Wacker Dr Ste 4500");
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderNo1_Path).equalsIgnoreCase(OrderNumber),
				OrderNumber + " (Order #) from Pre-Condition is not displays in 'Search Results' grid ");
		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderType1_Path).equalsIgnoreCase("PIMCO_GWM_BULKORDER"),
				"'PIMCO_GWM_BULKORDER' not displays under 'Order Type' column");
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Gen_OrderSearchbtn_Path);
		Click(Xpath, Pimco_MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "35 W Wacker Dr Ste 4500")),
				"35 W Wacker Dr Ste 4500 is not displayed in the mail list tab");

	}

	@Test(enabled = true, priority = 5)
	public void Pimco_TC_2_4_10_2_4() throws InterruptedException, IOException, AWTException {

		/*
		 * Verify Hyperlink tracking on Maillist tab when placed Bulk Order
		 * */

		/* Notes: Pimco (Both) Bulk Order no #: 5175843 in producion
		 *
		 */

		/*Click(Xpath, Pimco_LP_StartaBulkOrder_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Mailist6_NoRestrictions);
		Click(ID, Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_FulfillmentMarketingBulk);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_FulfillmentMarketingBulk));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();*/

		Robot robot = new Robot();
		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Containspath("input", "value", "PackSlip"));
		Thread.sleep(4000);
		Switch_New_Tab();
		PackslipNo = Get_Text(Xpath, Inventory_PackSlipno_Path).substring(1, 16);
		System.out.println(PackslipNo);
		Switch_Old_Tab();
		Click(Xpath, Inventory_QuickShip_Path);
		Type(Xpath, Inventory_QS_PickTicketBarcode_Path, PackslipNo);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Clickable(Xpath, Inventory_QS_ShipOrder_Path);

		Select_DropDown_VisibleText(Xpath, Inventory_QS_Shipper_Path, "USPS");
		Type(Xpath, Inventory_QS_TrackingNo_Path, "Test 1");
		Type(Xpath, Inventory_QS_ShippingCharges_Path, "0.0");
		Click(Xpath, Inventory_QS_Perrecipient_Path);
		Type(Xpath, Inventory_QS_Weight_Path, "0.0");
		Type(Xpath, Inventory_QS_Noofboxed_Path, "1");
		Click(Xpath, Inventory_QS_ShipOrder_Path);
		ExplicitWait_Element_Visible(Xpath, Containstextpath("div", "Record Updated"));

		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Inventory_Edit_Btn_Path);
		Click(Xpath, Containspath("input", "value", "PackSlip"));
		Thread.sleep(4000);
		Switch_New_Tab();
		PackslipNo = Get_Text(Xpath, Inventory_PackSlipno_Path).substring(1, 16);
		System.out.println(PackslipNo);
		Switch_Old_Tab();

		Click(Xpath, Inventory_QuickShip_Path);
		Type(Xpath, Inventory_QS_PickTicketBarcode_Path, PackslipNo);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		ExplicitWait_Element_Clickable(Xpath, Inventory_QS_ShipOrder_Path);
		Select_DropDown_VisibleText(Xpath, Inventory_QS_Shipper_Path, "USPS");
		Type(Xpath, Inventory_QS_TrackingNo_Path, "Test 2");
		Type(Xpath, Inventory_QS_ShippingCharges_Path, "0.0");
		Click(Xpath, Inventory_QS_Perrecipient_Path);
		Type(Xpath, Inventory_QS_Weight_Path, "0.0");
		Type(Xpath, Inventory_QS_Noofboxed_Path, "1");
		Click(Xpath, Inventory_QS_ShipOrder_Path);
		ExplicitWait_Element_Visible(Xpath, Containstextpath("div", "Record Updated"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Record Updated")),
				"Record Updated is not displayed");

		Get_URL(URL);
		login(PIMCOBoth1Username, PIMCOBoth1Password);
		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		Click(Xpath, Pimco_MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Test 1")),
				"Test 1 is not displayed in the mail list tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Test 2")),
				"Test 2 is not displayed in the mail list tab");
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Test 1"));
		Assert.assertTrue(Get_Title().toLowerCase().contains("usps.com"), "USPS.com page is not displayed - Test 1");
		NavigateBack();
		Click(Xpath, Textpath("a", "Test 2"));
		Assert.assertTrue(Get_Title().toLowerCase().contains("usps.com"), "USPS.com page is not displayed - Test 2");
		NavigateBack();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}
}
