package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class BulkOrder extends BaseTestPimco {

	public void Click_BookContent_Expand(String ProductCode) throws InterruptedException {

		Click(Xpath, ".//div[contains(text(),'" + ProductCode + "')]/preceding::input[1]");
		Wait_ajax();
		Thread.sleep(3000);
	}

	public String BookContent_Expanded(String ProductCode, String Expanded_Content) throws InterruptedException {

		return ".//div[contains(text(),'" + ProductCode + "')]/following::tr//td[text()='" + Expanded_Content + "']";

	}

	public String Inventory_Authentication_Stage(String Inven_Username, String Inven_Password, String Orderno) {

		return "http://" + Inven_Username + ":" + Inven_Password
				+ "@ver-sqlnew.cgx.net/ReportServer/Pages/ReportViewer.aspx?/Veritas/Staging/PickTickets/PickTicketPIMCO&rs:Command=Render&OrderID="
				+ Orderno;

	}

	@Test(priority = 1, enabled = false)
	public void Pimco_TC_2_7_1_1() throws InterruptedException, IOException {

		/*
		 * Verify user is able to place an order in bulk mode
		 * */

		Clearcarts();
		Click(Xpath, Pimco_LP_StartaBulkOrder_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Mailist6_NoRestrictions);
		Click(ID, Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_FulfillmentMarketingBulk);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_FulfillmentMarketingBulk));
		Thread.sleep(2000);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "QA_FulfillmentMarketingBulk")),
				"QA_FulfillmentMarketingBulk is not displayed in mini 'Shopping Cart' widget");
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("h1", "Checkout")),
				"Checkout page is not displayed");
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();
	}

	@Test(priority = 2, enabled = false)
	public void Pimco_TC_2_7_1_2() throws InterruptedException, IOException, AWTException {

		/*
		 * Backorder Check for Mail List Orders in Pimco (Both) via Bulk Order
		 * */
		Clearcarts();
		Click(Xpath, Pimco_LP_Home_path);
		Click(Xpath, Pimco_LP_StartaBulkOrder_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Maillist5_NoRestrictions);
		Click(Xpath, Pimco_MLU_UploadMailList_Path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));

		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Stockpart);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_Stockpart));
		ExplicitWait_Element_Visible(Xpath, Textpath("div", "Successfully Added 1!"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1!")),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", QA_Stockpart)),
				QA_Stockpart + " is not displayed in mini 'Shopping Cart' widget");
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		System.out.println("The " + Get_Text(Xpath, Containspath("span", "class", "Inactive")));
		softAssert.assertTrue(
				Get_Text(Xpath, Containspath("span", "class", "Inactive")).equalsIgnoreCase("Contacts Uploaded: 5"),
				"Contacts Uploaded: 5 is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Backordered: 5")),
				"Backordered: 5 is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_SCP_Qty_Path, "value").trim().equals("1"),
				"'1' is not displayed in 'Quantity' field");
		softAssert.assertAll();
		Type(Xpath, Pimco_SCP_Qty_Path, "2");
		Click(Xpath, Pimco_SCP_Update_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_SCP_Loading_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Backordered: 10")),
				"'Quantity for Form # QA_Stockpart has been changed to 10 unit(s).' message is not displayed in red");
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();
	}

	@Test(priority = 3, enabled = true)
	public void Pimco_TC_2_7_2_1AndPimco_TC_2_7_2_2() throws InterruptedException, IOException, AWTException {

		/*
		 * Verify Users are restricted from creating multiple books per order
		 * */

		Clearcarts();
		Click(Xpath, Pimco_LP_Home_path);
		Click(Xpath, Pimco_LP_StartaBulkOrder_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Maillist5_NoRestrictions);
		Click(Xpath, Pimco_MLU_UploadMailList_Path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, PBB001_40827);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		ExplicitWait_Element_Visible(Xpath, Textpath("div", "Successfully Added 1!"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1!")),
				"Successfully Added 1! is not displayed");
		Thread.sleep(5000);
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, PBB002_41553);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB002_41553));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Maximum of 1 Book Builder item per Order")),
				"Maximum of 1 Book Builder item per Order is not displayed");
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();

		// Pimco_TC_2_7_2_2 Starts

		Type(Xpath, Pimco_SCO_ClientName_Path, "Pimco");
		Type(Xpath, Pimco_SCO_DayMonthYear_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_SCO_FirmName_Path, "Firmtest");
		Click(Xpath, Pimco_SCO_TOCYes_ck_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_01);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(QA_TestFundcard_01),
				QA_TestFundcard_01 + "  not displays under 'Book Content' section");
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, qa_testfundcard_02);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_03);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		DragandDrop(Xpath, Pimco_SCO_BookContent_Row3_Path, Xpath, Pimco_SCO_BookContent_Row1_Path);
		Wait_ajax();
		DragandDrop(Xpath, Pimco_SCO_BookContent_Row2_Path, Xpath, Pimco_SCO_BookContent_Row1_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_BookContent_Remove1_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_BookContent_Remove1_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_BookContent_Remove1_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "No Items Added")),
				"No Items Added is not displayed");
	}

	@Test(priority = 4, enabled = true, dependsOnMethods = "Pimco_TC_2_7_2_1AndPimco_TC_2_7_2_2")
	public void Pimco_TC_2_7_2_3() throws InterruptedException, IOException, AWTException {

		/*
		 * Validate the Page Count and ability to successfully generate the Proof with Large Book content
		 * */

		Click(Xpath, Pimco_SCO_TOCNo_ck_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_CurrentPageCount_Path).trim().equalsIgnoreCase("4"),
				"'4' not displays for 'Current Page Count' field");
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_testfactsheet_02);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_01);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_KitLookup_Path, QA_BookbuilderKit1);
		Wait_ajax();
		Click(Xpath, Textpath("td", QA_BookbuilderKit1 + " "));
		Click(Xpath, Pimco_SCO_KitLookup_Addtobook_Path);
		Wait_ajax();
		// softAssert.assertTrue(Get_Text(Xpath,
		// Pimco_SCO_CurrentPageCount_Path).trim().equalsIgnoreCase("62"),
		// "'62' not displays for 'Current Page Count' field");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_SCO_MaximumPageCount_Path).trim().equalsIgnoreCase("64"),
				"'64' not displays for 'Current Page Count' field");
		softAssert.assertAll();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Wait_ajax();
		Switch_New_Tab();
		Switch_Old_Tab();
	}

	@Test(priority = 5, enabled = true, dependsOnMethods = "Pimco_TC_2_7_2_3")
	public void Pimco_TC_2_7_2_4() throws InterruptedException, IOException, AWTException {

		Click(Xpath, Pimco_SCP_EditVariables_btn_Path);
		Type(Xpath, Pimco_SCO_KitLookup_Path, QA_BookbuilderKit1);
		Wait_ajax();
		Click(Xpath, Textpath("td", QA_BookbuilderKit1 + " "));
		Click(Xpath, Pimco_SCO_KitLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "It exceed the Maximum allowed Pages")),
				"It exceed the Maximum allowed Pages not displayed");
		Assert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("*", qa_testfundcard_02)),
				qa_testfundcard_02 + " is displayed under 'Product Code' column in 'Book Content' section");

	}

	@Test(priority = 6, enabled = true)
	public void Pimco_TC_2_7_2_5() throws InterruptedException, IOException, AWTException {

		/*
		 * Verify Footnote, Table of Contents, sort order and Dedupe pieces for Book Builder kits in Order confirmation page and pick ticket
		 * */

		Clearcarts();
		Click(Xpath, Pimco_LP_Home_path);
		Click(Xpath, Pimco_LP_StartaBulkOrder_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, PIMCO_Maillist5_NoRestrictions);
		Click(Xpath, Pimco_MLU_UploadMailList_Path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, PBB001_40827);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		ExplicitWait_Element_Visible(Xpath, Textpath("div", "Successfully Added 1!"));
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Type(Xpath, Pimco_SCO_ClientName_Path, "Pimco");
		Type(Xpath, Pimco_SCO_DayMonthYear_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_SCO_FirmName_Path, "Firmtest");
		Click(Xpath, Pimco_SCO_TOCYes_ck_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_KitLookup_Path, QA_BookbuilderKit1);
		Wait_ajax();
		Click(Xpath, Textpath("td", QA_BookbuilderKit1 + " "));
		Click(Xpath, Pimco_SCO_KitLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_TestFundcard_03")),
				"QA_TestFundcard_03 not displayed in the under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_testfactsheet_01")),
				"QA_testfactsheet_01 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_TestFundcard_01")),
				"QA_TestFundcard_01 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_testfactsheet_05")),
				"QA_testfactsheet_05 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "qa_testfundcard_02")),
				"QA_Testfundcard_02 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_TestFundCard_05")),
				"QA_TestFundCard_05 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_Testsupp_04")),
				"QA_Testsupp_04 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_Replace_Test_2")),
				"QA_Replace_Test_2 not displayed in under 'Product Code' column page");
		Click_BookContent_Expand(QA_TestFundcard_03);
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_TestFundcard_03, QA_Testsupp_03)),
				QA_Testsupp_03 + " not displayed in the Product Code - " + QA_TestFundcard_03);
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded("QA_TestFundcard_03", QA_Testsupp_02)),
				QA_Testsupp_02 + "not displayed in the Product Code - " + QA_TestFundcard_03);
		Click_BookContent_Expand(QA_TestFundcard_03);
		Click_BookContent_Expand(QA_testfactsheet_05);
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_testfactsheet_05, QA_Testsupp_03)),
				QA_Testsupp_03 + " not displayed in the Product Code - " + QA_testfactsheet_05);
		Click_BookContent_Expand(QA_testfactsheet_05);
		Click_BookContent_Expand(qa_testfundcard_02);
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(qa_testfundcard_02, QA_Testsupp_02)),
				QA_Testsupp_02 + " not displayed in the Product Code - " + qa_testfundcard_02);
		Click_BookContent_Expand(qa_testfundcard_02);
		Click_BookContent_Expand(QA_TestFundCard_05);
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_TestFundCard_05, QA_Testsupp_03)),
				QA_Testsupp_03 + " not displayed in the Product Code - " + QA_TestFundCard_05);
		Click_BookContent_Expand(QA_TestFundCard_05);
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Wait_ajax();
		Switch_New_Tab();
		Switch_Old_Tab();

		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", PBB001_40827)),
				PBB001_40827 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_TestFundcard_03)),
				QA_TestFundcard_03 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_testfactsheet_01)),
				QA_testfactsheet_01 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_TestFundcard_01)),
				QA_TestFundcard_01 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_testfactsheet_05)),
				QA_testfactsheet_05 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", qa_testfundcard_02)),
				qa_testfundcard_02 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_TestFundCard_05)),
				QA_TestFundCard_05 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Testsupp_04)),
				QA_Testsupp_04 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Replace_Test_2)),
				QA_Replace_Test_2 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Testsupp_02)),
				QA_Testsupp_02 + " is not displayed in the Order conformation page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Testsupp_03)),
				QA_Testsupp_03 + " is not displayed in the Order conformation page");
		softAssert.assertAll();

		Click(ID, Pimco_LP_Logout_ID);
		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Containstextpath("a", "Pick"));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains("OrderID=" + OrderNumber), "Pick is not opened in the new window");
		Get_URL(Inventory_Authentication_Stage("Yannamalai", "Sachin05", "1767808"));
		ExplicitWait_Element_Not_Visible(Xpath, Containspath("span", "class", "WaitText"));
		Assert.assertTrue(Get_Title().trim().equalsIgnoreCase("PickTicketPIMCO - Report Viewer"),
				"PickTicketPIMCO - Report Viewer is not opened in the new window");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_TestFundcard_01)),
				QA_TestFundcard_01 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_TestFundcard_03)),
				QA_TestFundcard_03 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_testfactsheet_01)),
				QA_testfactsheet_01 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_testfactsheet_05)),
				QA_testfactsheet_05 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", qa_testfundcard_02)),
				qa_testfundcard_02 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_TestFundCard_05)),
				QA_TestFundCard_05 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Testsupp_04)),
				QA_Testsupp_04 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Replace_Test_2)),
				QA_Replace_Test_2 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Testsupp_02)),
				QA_Testsupp_02 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", QA_Testsupp_03)),
				QA_Testsupp_03 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		Switch_Old_Tab();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		// login(PIMCOBoth1Username, PIMCOBoth1Password);
		login(PIMCOBoth1Username, PIMCOBoth1Password);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
