package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class ValidateLoginTest extends BaseTestPimco  {

	
	//Validate login screen fields
	@Test
	public void  Pimco_1_0_1_1 (){
		
		
		Assert.assertTrue(Element_Is_Present(ID, Pimco_Login_UserName_ID),"Login Field is missing");
		
		Assert.assertTrue(Element_Is_Present(ID, Pimco_Login_Password_ID),"Password Field is missing");

		Assert.assertTrue(Element_Is_Present(ID, Pimco_Login_LoginBtn_ID),"Login Button is missing");

		
	}
	
	
	//Verify  Active user login
	@Test
	public void  Pimco_1_0_1_2() throws IOException, InterruptedException{
		login(PIMCOBothUsername, PIMCOBothPassword);
		   logout();
		
	}
	
	//verify  Inactive user login
	@Test
	public void  Pimco_1_0_1_3() throws InterruptedException{
		

		Type(ID, Pimco_Login_UserName_ID, "QA1234");
		Type(ID, Pimco_Login_Password_ID, "QA1234");
		Click(ID, Pimco_Login_LoginBtn_ID);
		
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "Invalid user name or password, please try again.")),"Invalid user name or password, please try again. - message is missing");
		
	}
	
	
	//Verify the error message is displayed upon "Invalid user Name and password" combination
	@Test
	public void  Pimco_1_0_2_1() throws InterruptedException{
		
		Type(ID, Pimco_Login_UserName_ID, "Qaauto");
		Type(ID, Pimco_Login_Password_ID, "QA1234");
		Click(ID, Pimco_Login_LoginBtn_ID);
		
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "Invalid user name or password, please try again.")),"Invalid user name or password, please try again. - message is missing");
		
		Type(ID, Pimco_Login_UserName_ID, "QA1234");
		Type(ID, Pimco_Login_Password_ID, "Qaauto");
		Click(ID, Pimco_Login_LoginBtn_ID);
		
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "Invalid user name or password, please try again.")),"Invalid user name or password, please try again. - message is missing");
		
	}
	
	
	//Verify the error message when user login without userName
	@Test
	public void  Pimco_1_0_2_2() throws InterruptedException{
		Clear(ID, Pimco_Login_UserName_ID);
		Type(ID, Pimco_Login_Password_ID, "QA1234");
		Click(ID, Pimco_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "User Name is required")),"User Name is required - message is missing");

		
		
	}
	
	//Verify the error message when user login without password
	@Test
	public void  Pimco_1_0_2_3() throws InterruptedException{
		Type(ID, Pimco_Login_UserName_ID, "Qaauto");
		Clear(ID, Pimco_Login_Password_ID);
		Click(ID, Pimco_Login_LoginBtn_ID);
		
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "User Password is required.")),"User Password is required. - message is missing");

	}
	
	//Verify error message when user Name and password is not provided for login
	@Test
	public void  Pimco_1_0_2_4() throws InterruptedException{
		Clear(ID, Pimco_Login_UserName_ID);

		Clear(ID, Pimco_Login_Password_ID);
		
		Click(ID, Pimco_Login_LoginBtn_ID);
		
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "User Password is required.")),"User Password is required. - message is missing");
		Assert.assertTrue(Element_Is_Present(Xpath,Textpath("li", "User Name is required")),"User Name is required - message is missing");

	}
}
