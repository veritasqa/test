package com.Pimco.RegressionTestBoth;

import java.io.IOException;
import java.awt.AWTException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageAnnouncements extends BaseTestPimco {

	public static String Announcement = "QA Test " + Get_Todaydate("MM/d/YYYY HH:mm:SS");
	public static String EditAnnouncement = "QA Test for Announcement " + Get_Todaydate("MM/d/YYYY HH:mm:SS");
	public static String HyperlinkURL_Stage = "https://staging.veritas-solutions.net/TheStandard/";
	public static String HyperlinkURL_Prod = "https://www.veritas-solutions.net/TheStandard/";
	public static String Itemof;

	public void CreateAnnouncement(String StartDate_d, String InactiveDate_d) throws InterruptedException {

		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);
		ExplicitWait_Element_Visible(Xpath, Pimco_MA_Title_Path);
		Click(Xpath, Pimco_MA_Addannouncementbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MA_Createannouncementbtn_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, Pimco_MA_Announcement_iFrame_ID));
		Type(Xpath, Pimco_MA_AddAnnouncement_Path, Announcement);
		Switch_To_Default();
		Datepicker2(Xpath, Pimco_MA_AddStartdateCal_Path, Xpath, Pimco_MA_AddStartdateCalmonth_Path,
				Get_Todaydate("MMM"), StartDate_d, Get_Todaydate("YYYY"));
		Click(Xpath, Pimco_MA_AddStartdateTime_Path);
		Datepicker2(Xpath, Pimco_MA_AddinactivedateCal_Path, Xpath, Pimco_MA_AddinactivedateCalmonth_Path,
				Get_Futuredate("MMM"), InactiveDate_d, Get_Futuredate("YYYY"));
		Click(Xpath, Pimco_MA_AddinactivedateTime_Path);
		Click(Xpath, UserGroup_Checkbox("US"));
		Click(Xpath, UserGroup_Checkbox("CA"));
		Type(Xpath, Pimco_MA_AddSort_Path, "1");
		Click(Xpath, Pimco_MA_Createannouncementbtn_Path);
		Wait_ajax();
		Click(Xpath, Pimco_LP_Home_path);
		Wait_ajax();

	}

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[1]");
	}

	@Test(enabled = true, priority = 1)
	public void Pimco_TC_2_4_4_1_1() throws InterruptedException {

		// Validate page opens and appropriate fields/tables are displaying

		ExplicitWait_Element_Visible(Xpath, Pimco_MA_Title_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Title_Path),
				"'Manage Announcements' page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Announcement_Path),
				"'Announcement' search field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Announcementfilter_Path),
				" 'Filter' icon is not displayed by 'Announcement' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Startdate_Path),
				"'Start Date' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Startdatefilter_Path),
				" 'Filter' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_StartdateCalicon_Path),
				" 'Calendar' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Inactivedate_Path),
				" 'Inactive Date' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Inactivedatefilter_Path),
				" 'Filter' icon is not displayed by 'Inactive Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_InactivedateCalicon_Path),
				" 'Calendar' icon is not displayed by 'Inactive Date' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Sort_Path),
				" 'Sort' field is not displayed on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Sortfilter_Path),
				"'Filter' icon is not displayed by 'Sort' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_OnlyActiveck_Path),
				" 'Only Active' checkbox is not displayed  on 'Manage Announcements' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_OnlyActivefilter_Path),
				" 'Filter' icon is not displayed by 'Only Active' checkbox");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Addannouncementbtn_Path),
				"'Add Announcement' button is not displayed on 'Manage Announcements' page at the bottom of the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Refreshbn_Path),
				" 'Refresh' button is not displayed on 'Manage Announcements' page at the bottom of the page");
		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 2)
	public void Pimco_TC_2_4_4_1_2() throws InterruptedException, AWTException {

		// Add Announcement

		CreateAnnouncement(Get_Todaydate("d"), Get_Futuredate("d"));
		Click(Xpath, Textpath("a", "View All Announcements"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Test' is not displayed in 'View All Announcements' page ");

	}

	@Test(enabled = true, priority = 4)
	public void Pimco_TC_2_4_4_1_3() throws InterruptedException, AWTException {

		// Edit the Announcement

		Type(Xpath, Pimco_MA_Announcement_Path, Announcement);
		Selectfilter(Pimco_MA_Announcementfilter_Path, "EqualTo");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Announcement)),
				"Searched result 'QA Test' is not displayed under 'Announcement' column with 'Edit' link");
		Click(Xpath, Pimco_MA_Edit1_Path);
		Switch_To_Iframe_byWebelement(Element(Xpath, Pimco_MA_Announcement_iFrame_ID));
		Type(Xpath, Pimco_MA_AddAnnouncement_Path, EditAnnouncement);
		Switch_To_Default();
		Click(Xpath, Pimco_MA_Updateammouncementbtn_Path);
		Wait_ajax();
		Click(Xpath, Pimco_LP_Home_path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "View All Announcements"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", EditAnnouncement)),
				"'QA Test' is not displayed in 'View All Announcements'");

	}

	@Test(enabled = true, priority = 5)
	public void Pimco_TC_2_4_4_1_4() throws InterruptedException, AWTException {

		// Pre-Req

		CreateAnnouncement(Get_Future_Nth_Date("d", 2), Get_Futuredate("d"));
		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);

		// Activate/Deactivate Announcements

		Type(Xpath, Pimco_MA_Announcement_Path, Announcement);
		Selectfilter(Pimco_MA_Announcementfilter_Path, "EqualTo");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Announcement)),
				"Searched result 'QA Testing' is not displayed under 'Announcement' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("td", Get_Future_Nth_Date("M/d/YYYY", 2) + " 12:00:00 AM")),
				" <Future Date & Time- MM/DD/YYYY HH:MM:SS AM/PM> is  not displayed under 'Start Date' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Containstextpath("td", Get_Futuredate("M/d/YYYY") + " 12:00:00 AM")),
				" <Future Date & Time- MM/DD/YYYY HH:MM:SS AM/PM> is  not displayed under 'Inactive Date' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Activate1_Path), "Activate is not displayed");
		softAssert.assertFalse(Element_Is_selected(Xpath, Pimco_MA_Onlyactiveck1_Path),
				"'Only Active' checkbox is checked off for 'QA Testing'");
		softAssert.assertAll();
		Click(Xpath, Pimco_MA_Activate1_Path);
		Wait_ajax();
		Click(Xpath, Pimco_LP_Home_path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Testing' is not displayed in View All Announcements page");
		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);
		ExplicitWait_Element_Visible(Xpath, Pimco_MA_Title_Path);
		Type(Xpath, Pimco_MA_Announcement_Path, Announcement);
		Selectfilter(Pimco_MA_Announcementfilter_Path, "EqualTo");
		Click(Xpath, Pimco_MA_Activate1_Path);
		Click(Xpath, Pimco_LP_Home_path);
		Click(Xpath, Textpath("a", "View All Announcements"));
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Testing' is displayed in View All Announcements after deactivated");

	}

	@Test(enabled = true, priority = 6)
	public void Pimco_TC_2_4_4_1_5() throws InterruptedException, AWTException {

		// Validate Filtering Functionality

		// CreateAnnouncement(Get_Todaydate("d"), Get_Futuredate("d"));

		ExplicitWait_Element_Visible(Xpath, Pimco_MA_Title_Path);
		Itemof = Get_Text(Xpath, Pimco_MA_Itemof_Path).trim();

		Type(Xpath, Pimco_MA_Announcement_Path, "QA Test");
		Selectfilter(Pimco_MA_Announcementfilter_Path, "Contains");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Announcement1_Path).trim().toLowerCase().contains("qa test"),
				"'QA Test' search result  is not displayed under  'Announcement'  column");
		Selectfilter(Pimco_MA_Announcementfilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Annmouncement");

		Type(Xpath, Pimco_MA_Startdate_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(Pimco_MA_Startdatefilter_Path, "EqualTo");
		softAssert.assertTrue(
				Get_Text(Xpath, Pimco_MA_Startdate1_Path).trim().toLowerCase().contains(Get_Todaydate("M/d/YYYY")),
				" <MM/DD/YYYY>  search result is not displayed under 'Start Date' column");
		Selectfilter(Pimco_MA_Startdatefilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Start Date");

		Type(Xpath, Pimco_MA_Inactivedate_Path, Get_Futuredate("M/d/YYYY"));
		Selectfilter(Pimco_MA_Inactivedatefilter_Path, "EqualTo");
		softAssert.assertTrue(
				Get_Text(Xpath, Pimco_MA_Inactivedate1_Path).trim().toLowerCase().contains(Get_Futuredate("M/d/YYYY")),
				" <MM/DD/YYYY>  search result is not displayed under 'Inactive Date' column");
		Selectfilter(Pimco_MA_Inactivedatefilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Inactive date");

		Type(Xpath, Pimco_MA_Sort_Path, "1");
		Selectfilter(Pimco_MA_Sortfilter_Path, "EqualTo");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Sort1_Path).trim().toLowerCase().contains("1"),
				"'1' search result  is displayed under  'Sort'  column");
		Selectfilter(Pimco_MA_Sortfilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - Sort");

		Click(Xpath, Pimco_MA_OnlyActiveck_Path);
		Selectfilter(Pimco_MA_OnlyActivefilter_Path, "EqualTo");
		softAssert.assertTrue(Element_Is_selected(Xpath, Pimco_MA_Onlyactiveck1_Path),
				"only the checked rows are displayed under the 'Only Active' column");
		Selectfilter(Pimco_MA_OnlyActivefilter_Path, "NoFilter");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MA_Itemof_Path).trim().equals(Itemof),
				"<All> the records in the grid are not displayed - OnlyActive");

		softAssert.assertAll();
	}

	@Test(enabled = true, priority = 3)
	public void Pimco_TC_2_4_4_1_7() throws InterruptedException, AWTException, IOException {

		// Validate User is able to view announcement by changing the audience

		// CreateAnnouncement(Get_Todaydate("d"), Get_Futuredate("d"));
		// Announcement = "QA Test 05/21/2018 18:57:83";

		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);
		Type(Xpath, Pimco_MA_Announcement_Path, Announcement);
		Selectfilter(Pimco_MA_Announcementfilter_Path, "EqualTo");
		Click(Xpath, Pimco_MA_Edit1_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, UserGroup_Checkbox("Advisory/Brokerage")),
				"Advisory/Brokerage is not checked");
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBoth1Username);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBoth1Username)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Clickedituser(PIMCOBoth1Username);

		if (Element_Is_selected(Xpath, UsergroupCheckbox("Advisory/Brokerage"))) {

			Click(Xpath, UsergroupCheckbox("Advisory/Brokerage"));
			Click(Xpath, Pimco_MU_Edit_Updatebtn_Path);
		}
		Click(ID, Pimco_LP_Logout_ID);

		login(PIMCOBoth1Username, PIMCOBoth1Password);
		Click(Xpath, Textpath("a", "View All Announcements"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", Announcement)),
				"'QA Test' is not displayed in 'View All Announcements'page ");
		Click(ID, Pimco_LP_Logout_ID);

		login(PIMCOBothUsername, PIMCOBothPassword);
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBoth1Username);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBoth1Username)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Clickedituser(PIMCOBoth1Username);
		Click(Xpath, UsergroupCheckbox("Advisory/Brokerage"));
		Click(Xpath, Pimco_MU_Edit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

	}

	@Test(enabled = true, priority = 7)
	public void Pimco_TC_2_4_4_1_8() throws InterruptedException, AWTException, IOException {

		// Validate paging and page size functionality.
		Click(Xpath, Textpath("span", "2"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "page 2 is not displayed upon clicking page 2");
		Type(Xpath, Pimco_MA_Pagesize_Path, "1");
		Click(Xpath, Pimco_MA_Pagechangebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MA_Row1_Path), "Only one row is not  is not displayed");
		Assert.assertFalse(Element_Is_Displayed(Xpath, Pimco_MA_Row2_Path), "Only one row is not  is not displayed");
		Type(Xpath, Pimco_MA_Pagenotext_Path, "2");
		Click(Xpath, Pimco_MA_Pagegobtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "page 2 is not displayed upon go to page 2");
		Click(Xpath, Pimco_MA_Nextpagebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")), "page 3 is not displayed");
		Click(Xpath, Pimco_MA_Firstpagebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")), "1st page is not displayed");
		Click(Xpath, Pimco_MA_Lastpagebtn_Path);
		String MA_lastpageno = Get_Text(Xpath, Pimco_MU_Currentpage_Path).trim();
		Assert.assertEquals(MA_lastpageno,
				Get_Text(Xpath, Pimco_MA_Pageof_Path).substring(Get_Text(Xpath, Pimco_MA_Pageof_Path).indexOf(" ") + 1,
						Get_Text(Xpath, Pimco_MA_Pageof_Path).length()),
				"Last page is not highlighted");
		Click(Xpath, Pimco_MA_Previouspagebtn_Path);
		Assert.assertNotEquals(Get_Text(Xpath, Pimco_MU_Currentpage_Path).trim(), MA_lastpageno,
				"(second last page) is not highlighted");
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);
		// Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		NavigateMenu(LP_Admin_path, LP_ManageAnnouncements_path);

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		// Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
