package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulfillmentuserPrivilageTest  extends BaseTestPimco {
	
	
	//Verify the part is available for Pimco (Both) user with same user group as the user logged in
	@Test
	public void Pimco_TC_2_8_6_1() throws InterruptedException{
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Usergroup);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath,ClickButtononSearchresultspath(QA_Usergroup, "Edit"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CA_Checkbox_Path),
				" CA is checked off in 'User Group' field");
		
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_US_Checkbox_Path),
				"US is checked off in 'User Group' field");
		
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaAdminTeam_Checkbox_Path),
				"CanadaAdminTeam is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaFAs_Checkbox_Path),
				" CanadaFAsis checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaSalesTeam_Checkbox_Path),
				"CanadaSalesTeam is checked off in 'User Group' field");
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Users"));
		Wait_ajax();
		//Xpath, UsergroupCheckbox("CA")
		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBothUsername);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBothUsername)),
				PIMCOBothUsername+"<User Name> is not displayed under the 'User Name' column header");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Wait_ajax();
		Clickedituser(PIMCOBothUsername);
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,UsergroupCheckbox("US")),"US checkbox is not checked");
		Assert.assertTrue(Element_Is_Displayed(Xpath,UsergroupCheckbox("CA")),"CA  checkbox is not checked");
		Assert.assertTrue(Element_Is_Displayed(Xpath,UsergroupCheckbox("Canada FAs")),"Canada FAs checkbox is not checked");
		Assert.assertTrue(Element_Is_Displayed(Xpath,UsergroupCheckbox("Canada Sales Team")),"Canada Sales Team checkbox is not checked");

		
		FulfilmentSearch(QA_Usergroup);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsAddtoCartBtnXpath(QA_Usergroup)),"QA_Usergroup is not visible in the shopping cart page");
		Wait_ajax();
		Clearcarts();
		
	}
		
	
	//Verifying the part is not available when no user group is selected for the part
	@Test
	public void Pimco_TC_2_8_6_2() throws InterruptedException, IOException{
		Clearcarts();
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Usergroup);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath,ClickButtononSearchresultspath(QA_Usergroup, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_CA_Checkbox_Path);
		Wait_ajax();
		Click(Xpath, Pimco_IM_US_Checkbox_Path);
		Wait_ajax();
		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		
		logout();
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	 	
    	FulfilmentSearch(QA_Usergroup);
    	Thread.sleep(6000);
    	ContactClick("QanoFirm", "IL");
    	Thread.sleep(6000);

    	Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");
	
    	
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Usergroup);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath,ClickButtononSearchresultspath(QA_Usergroup, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_CA_Checkbox_Path);
		Wait_ajax();
		Click(Xpath, Pimco_IM_US_Checkbox_Path);
		Wait_ajax();
		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		
		logout();
		
	}
	
	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[1]");
	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
	
}
