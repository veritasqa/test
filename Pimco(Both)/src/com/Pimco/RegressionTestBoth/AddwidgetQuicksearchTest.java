package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class AddwidgetQuicksearchTest extends BaseTestPimco {

	// Search by product code/keyword/title for Pimco (Both) User
	@Test
	public void Pimco_TC_2_5_4_1_1() throws InterruptedException {
		Type(Xpath, Pimco_LP_QS_Materialsby_Textbox_path, QA_Multifunction);
		Click(Xpath, Pimco_LP_QS_Search_Btn_path);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE")),
				"QA_Multifunction not Displayed");
		Click(ID, Pimco_SR_Clear_Btn_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Type(Xpath, Pimco_LP_QS_Materialsby_Textbox_path, "QA_MULTIFUNCTION_KEYWORDS");
		Click(Xpath, Pimco_LP_QS_Search_Btn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE")),
				"QA_Multifunction not Displayed");
		Click(ID, Pimco_SR_Clear_Btn_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Type(Xpath, Pimco_LP_QS_Materialsby_Textbox_path, "QA_MULTIFUNCTION_TITLE");
		Click(Xpath, Pimco_LP_QS_Search_Btn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE")),
				"QA_Multifunction not Displayed");
		Click(ID, Pimco_SR_Clear_Btn_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Type(Xpath, Pimco_LP_QS_Materialsby_Textbox_path, "QA_MULTIFUNCTION_CA");
		Click(Xpath, Pimco_LP_QS_Search_Btn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_CATITLE")),
				"QA_Multifunction not Displayed");
		Click(ID, Pimco_SR_Clear_Btn_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Home"));
		Wait_ajax();

		Type(Xpath, Pimco_LP_QS_Materialsby_Textbox_path, "QA_Multifunction_US");
		Click(Xpath, Pimco_LP_QS_Search_Btn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_USTITLE")),
				"QA_Multifunction not Displayed");
		Click(ID, Pimco_SR_Clear_Btn_ID);
		Wait_ajax();

		Clearcarts();
	}

	// Verify adding part to cart from product search and clearing cart

	@Test
	public void Pimco_TC_2_5_4_2_1() throws InterruptedException {
		Clearcarts();
		Type(Xpath, Pimco_LP_QS_Materialsby_Textbox_path, QA_Multifunction);
		Click(Xpath, Pimco_LP_QS_Search_Btn_path);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE")),
				"QA_Multifunction not Displayed");

		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")),
				"(1) is displaying next to 'Checkout' link");
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();

		Clearcarts();
	}

	@BeforeClass
	public void Widgetverify() throws InterruptedException {
		if (!Element_Is_Displayed(Xpath, WidgetNamePath("Quick Search"))) {
			Hover(Xpath, Textpath("h3", "Add Widgets"));
			Thread.sleep(2000);
			Wait_ajax();
			Click(Xpath, Textpath("a", "Quick Search"));
		}
	}

	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		// System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);

	}

}
