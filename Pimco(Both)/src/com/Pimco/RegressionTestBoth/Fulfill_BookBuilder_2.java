package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class Fulfill_BookBuilder_2 extends BaseTestPimco {

	public void Click_BookContent_Expand(String ProductCode) throws InterruptedException {

		Click(Xpath, ".//div[contains(text(),'" + ProductCode + "')]/preceding::input[1]");
		Wait_ajax();
		Thread.sleep(3000);
	}

	public String BookContent_Expanded(String ProductCode, String Expanded_Content) throws InterruptedException {

		return ".//div[contains(text(),'" + ProductCode + "')]/following::tr//td[text()='" + Expanded_Content + "']";

	}

	public void ManageinventoryNav() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

	}

	@Test(priority = 3, enabled = true)
	public void Pimco_TC_2_8_1_5_1_7() throws InterruptedException {
		/*
		 * Verify Error message display when saving the book builder without entering any value 
		 * */

		FulfilmentSearch("PBB001");
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("span", "There are no items in book. Please add some items before saving.")),
				"There are no items in book. Please add some items before saving. is not displayed");

	}

	@Test(priority = 4, enabled = true)
	public void Pimco_TC_2_8_1_5_1_8() throws InterruptedException {

		/*
		 * Verify user is allowed to place order for Customizable item along with book builder in the same order
		 */

		FulfilmentSearch("PBB001");
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Thread.sleep(5000);
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, "CBB004_40859");
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(CBB004_40859));
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_1Contactlookup_Path, "Veritas");
		Click(Xpath, Textpath("td", "Veritas QA1 QA "));
		Wait_ajax();
		Type(Xpath, Pimco_SCO_ContactFirmName_Path, "Advisor");
		Type(Xpath, Pimco_SCO_ClientName_Path, "Pimco");
		Type(Xpath, Pimco_SCO_DayMonthYear_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_SCO_FirmName_Path, "QA Test");
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QATest_FundCard1);
		Wait_ajax();
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", QATest_FundCard1));
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", CBB004_40859)),
				CBB004_40859 + " not displayed in the checkout page");
		Click(Xpath, Pimco_SCP_Proof1_btn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(Xpath, Pimco_SCP_Proof2_btn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);

		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");

		Click(ID, Pimco_SCP_Checkout_ID);

		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);

		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "PBB001_40827")),
				"PBB001_40827 is not displayed in the Order conformation page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "QATest_FundCard1")),
				"QATest_FundCard1 is not displayed in the Order conformation page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "CBB004_40859")),
				"CBB004_40859 is not displayed in the Order conformation page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_OC_Printfile1_Path),
				"PrintFile' button not displays under 'Proof' column for 'PBB001_40827");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_OC_Printfile2_Path),
				"'PrintFile' button not displays under 'Proof' column for 'CBB004_40859'");
	}

	@Test(priority = 5, enabled = true)
	public void Pimco_TC_2_8_1_5_2_1() throws InterruptedException {

		/*
		 * Verify Required items are also added into the Book content on selecting appropriate Firm
		 */

		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_FirmRestrictionTest);
		Wait_ajax();
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", QA_FirmRestrictionTest));
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(
				QA_FirmRestrictionTest), " 'QA_FirmRestrictionTest' not displays under 'Book Content' section");
		Click_BookContent_Expand(QA_FirmRestrictionTest);
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_FirmRestrictionTest, "QA_Testsupp_03")),
				"QA_Testsupp_03 not displayed in the Product Code - QA_FirmRestrictionTest");
		Click_BookContent_Expand(QA_FirmRestrictionTest);
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page - Without firmname");
		Clearcarts();

		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("Qa_AXA_Firm", "Automation");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_FirmRestrictionTest);
		Wait_ajax();
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", QA_FirmRestrictionTest));
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Clearcarts();

		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("Qa_AXAGroup", "Automation");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(PBB001_40827)),
				"PBB001_40827 is not displayed - Qa_AXAGroup_Firm");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Restricted by Firm")),
				"Restricted by Firm is not displayed");

		// Qa_AXA_Firm
		// Qa_AXAGroup
	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_8_1_5_2_2() throws InterruptedException {

		/*
		* Inventory search results are restricted based on the Publish Date		 
		*/

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_TesteffectiveDate);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_TesteffectiveDate, "Edit"));
		Wait_ajax();
		Select_lidropdown(Pimco_IM_DD_Viewability_Arrow_ID, ID, "Orderable");
		System.out.println("Future CST is " + Get_FutureCSTTime("M/d/YYYY H:mm"));
		Type(ID, Pimco_IM_PublishDate_Textbox_ID, Get_FutureCSTTime("M/d/YYYY H:mm"));
		Click(ID, Pimco_IM_Save_Btn_ID);
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);

	}

	@Test(priority = 2, enabled = true)
	public void Pimco_TC_2_8_1_5_2_3() throws InterruptedException {

		/*
		 * Inventory search results are restricted based on the Obsolete Date
		 * */

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Obsolete);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_Obsolete, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_Rulestab_Path);
		Type(Xpath, Pimco_IM_RT_ObsoleteDate_TextBOX_Path, Get_FutureCSTTime("M/d/YYYY H:mm"));
		Click(ID, Pimco_IM_Save_Btn_ID);
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);

	}

	@Test(priority = 6, enabled = true)
	public void Pimco_TC_2_8_1_5_2_4() throws InterruptedException, IOException {

		/*
		 * Verify piece displays in Inventory Search field when piece have Book Print PDF attached and the piece does not display when Book Print PDF is not attached to the piece
		 * */

		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_04);
		Click(Xpath, Containstextpath("td", QA_TestFundcard_04));
		Clearcarts();

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_TestFundcard_04);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_TestFundcard_04, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_AT_Attachmenttab_Path);
		Click(Xpath, Pimco_IM_AT_BPP_Remove_Path);
		Accept_Alert();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("label", "File Removed!")),
				"File Removed! is not displayed");
		Click(ID, Pimco_IM_Save_Btn_ID);
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Click(ID, Pimco_LP_Logout_ID);

		login(PIMCOBoth1Username, PIMCOBoth1Password);
		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_04);
		Click(Xpath, Containstextpath("td", QA_TestFundcard_04));

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_TestFundcard_04);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_TestFundcard_04, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_AT_Attachmenttab_Path);
		Wait_ajax();
		MoveandClick(Xpath, Pimco_IM_AT_BPP_Browse_Path);
		// Click(Xpath, Pimco_IM_AT_BPP_Browse_Path);
		Thread.sleep(2000);
		Runtime.getRuntime().exec(System.getProperty("user.dir")
				+ "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_4_13_1_4_ProfilePhoto.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_8_1_5_2_4.pdf");
		Thread.sleep(2000);
		Click(Xpath, Pimco_IM_AT_BPP_Upload_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Textpath("label", "Running Preflight! This may take some time..."));
		Wait_ajax();
		Click(ID, Pimco_IM_Save_Btn_ID);
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

	}

	@Test(priority = 7, enabled = true)
	public void Pimco_TC_2_8_1_5_3_1() throws InterruptedException, IOException {

		// Book Builder Exception SLA for Fulfillment Orders before 5 PM

		FulfilmentSearch("PBB001");
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_01);
		Wait_ajax();

		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
		ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, GreyedOutDate(Get_Nextbussinesday("d", 1))),
				"(Next Business day - MM/DD/YYYY) date is not greyed out");
		Click(Xpath, Textpath("a", Get_Nextbussinesday("d", 2)));

	}

	@Test(priority = 8, enabled = true)
	public void Pimco_TC_2_8_1_5_2_5() throws InterruptedException, IOException {

		/*
		 * Verify Book Builder child components need to be entered as transactions, but should not be deducted from inventory locations
		 * */

		Inventory_Login();
		Click(Xpath, Inventory_EditViewInventory_Path);
		Select_DropDown_VisibleText(Xpath, Inventory_EV_Customerdd_Path, "PIMCO");
		Type(Xpath, Inventory_EV_Barcode_Path, QA_TestFundcard_01);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_EV_EditView_Path(QA_TestFundcard_01));
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsonHand_Path, "value").trim().equals("2000"),
				"(20000) is not displayed in 'Units on Hand' field - " + QA_TestFundcard_01);
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsAvailable_Path, "value").trim().equals("2000"),
				"(20000) is not displayed in 'UnitsAvailable' field - " + QA_TestFundcard_01);
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_BackOrderQty_Path, "value").trim().equals("0"),
				"(0) is not displayed in 'Back Order Qty' field - " + QA_TestFundcard_01);
		Click(Xpath, Inventory_EditViewInventory_Path);
		Select_DropDown_VisibleText(Xpath, Inventory_EV_Customerdd_Path, "PIMCO");
		Type(Xpath, Inventory_EV_Barcode_Path, "QA_Testsupp_01");
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_EV_EditView_Path("QA_TestSupp_01"));
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsonHand_Path, "value").trim().equals("20000"),
				"(20000) is not displayed in 'Units on Hand' field - QA_Testsupp_01");
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsAvailable_Path, "value").trim().equals("20000"),
				"(20000) is not displayed in 'UnitsAvailable' field - QA_Testsupp_01");
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_BackOrderQty_Path, "value").trim().equals("0"),
				"(0) is not displayed in 'Back Order Qty' field - QA_Testsupp_01");
		softAssert.assertAll();

		Get_URL(URL);
		Implicit_Wait();
		login(PIMCOBoth1Username, PIMCOBoth1Password);
		FulfilmentSearch("PBB001");
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_ClientName_Path, "Pimco");
		Type(Xpath, Pimco_SCO_DayMonthYear_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_SCO_FirmName_Path, "QA Test");
		Wait_ajax();
		Click(Xpath, Pimco_SCO_TOCNo_ck_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TestFundcard_01);
		Wait_ajax();

		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, "QA_TestSupp_01");
		Wait_ajax();

		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Wait_ajax();
		Switch_New_Tab();
		Switch_Old_Tab();
		Wait_ajax();
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
		Wait_ajax();
		Click(ID, Pimco_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);

		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("a", "QA_TestSupp_01")),
				"QA_TestSupp_01 is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("a", QA_TestFundcard_01)),
				QA_TestFundcard_01 + " is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("a", PBB001_40827)),
				PBB001_40827 + " is not displayed");
		softAssert.assertAll();

		Click(Xpath, Inventory_EditViewInventory_Path);
		Select_DropDown_VisibleText(Xpath, Inventory_EV_Customerdd_Path, "PIMCO");
		Type(Xpath, Inventory_EV_Barcode_Path, QA_TestFundcard_01);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_EV_EditView_Path(QA_TestFundcard_01));
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsonHand_Path, "value").trim().equals("20000"),
				"(20000) is not displayed in 'Units on Hand' field after order placed - " + QA_TestFundcard_01);
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsAvailable_Path, "value").trim().equals("20000"),
				"(20000) is not displayed in 'UnitsAvailable' field after order placed - " + QA_TestFundcard_01);
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_BackOrderQty_Path, "value").trim().equals("0"),
				"(0) is not displayed in 'Back Order Qty' field after order placed - " + QA_TestFundcard_01);
		Click(Xpath, Inventory_EditViewInventory_Path);
		Select_DropDown_VisibleText(Xpath, Inventory_EV_Customerdd_Path, "PIMCO");
		Type(Xpath, Inventory_EV_Barcode_Path, "QA_Testsupp_01");
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_EV_EditView_Path("QA_TestSupp_01"));
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsonHand_Path, "value").trim().equals("20000"),
				"(20000) is not displayed in 'Units on Hand' field after order placed - QA_Testsupp_01");
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_UnitsAvailable_Path, "value").trim().equals("20000"),
				"(20000) is not displayed in 'UnitsAvailable' field after order placed - QA_Testsupp_01");
		softAssert.assertTrue(Get_Attribute(Xpath, Inventory_EV_BackOrderQty_Path, "value").trim().equals("0"),
				"(0) is not displayed in 'Back Order Qty' field after order placed - QA_Testsupp_01");
		softAssert.assertAll();

	}

	@Test(priority = 9, enabled = true)
	public void Pimco_TC_2_8_1_5_2_3_Continues() throws InterruptedException {

		/*
		 * Inventory search results are restricted based on the Obsolete Date
		 * */

		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_TesteffectiveDate);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", QA_TesteffectiveDate)),
				"QA_TesteffectiveDate is not not shown");

	}

	@Test(priority = 10, enabled = true)
	public void Pimco_TC_2_8_1_5_2_2_Continues() throws InterruptedException {

		FulfilmentSearch(PBB001_40827);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_Obsolete);
		Wait_ajax();

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Obsolete);
		Select_lidropdown(Pimco_IM_StatusDrowpdown_Arrow_ID, ID, "Active");
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_Obsolete, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_Rulestab_Path);
		Click(Xpath, Pimco_IM_RT_UnObsolete_Path);
		Click(ID, Pimco_IM_Save_Btn_ID);
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBoth1Username, PIMCOBoth1Username);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Clearcarts();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
