package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulfilmentPartViewTest extends BaseTestPimco{
	
	//Validate that admin only part is viewable for an admin user
	
	@Test(enabled = true , priority = 1)
	public void Pimco_TC_2_8_3_1() throws InterruptedException{
		FulfilmentSearch(QA_adminonly);
		ContactClick("QanoFirm", "IL");
		Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_adminonly)),"QA_Adminonly displays in 'Materials' page");
			
		
	}
	
	//Validate when a part is set to viewable and not orderable that it's viewable
	@Test(enabled = true , priority = 2)
	public void Pimco_TC_2_8_3_2() throws InterruptedException, IOException{
		
	    ManageinventoryNav();
        Wait_ajax();
        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Viewability);
        Click(ID, Pimco_IM_Search_Btn_ID);
        Wait_ajax();
        
        Click(Xpath, ClickButtononSearchresultspath(QA_Viewability, "Edit"));
        Wait_ajax();
        
        Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
        Wait_ajax();
        Thread.sleep(3000);
        Click(Xpath, Textpath("li", "Viewable Not Orderable"));
        Wait_ajax();
        Thread.sleep(3000);
        Click(ID, Pimco_IM_Save_Btn_ID);
    	Wait_ajax();
    	Click(ID, Pimco_IM_PMSG_OK_Button_ID);
    	Wait_ajax();
    	
    	logout();
    	PimcoGWMLogin();
    	
    	FulfilmentSearch(QA_Viewability);
    	Thread.sleep(6000);

    	ContactClick("QanoFirm", "IL");
    	Thread.sleep(6000);

    	Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_Viewability))," QA_Viewability Not appears in 'Materials' Page");
    	Assert.assertTrue(Get_Text(Xpath, Pimco_SR_PieceAvailablity_Status_Path).trim().equalsIgnoreCase("Not Orderable"),"Not Orderable Not displays for 'QA_Viewability' ");
    	Thread.sleep(3000);
	}
	
	//Validate when a part is set to not viewable that is not viewable
	@Test(enabled = true , priority = 3)
	public void Pimco_TC_2_8_3_3() throws InterruptedException, IOException{
		
	    ManageinventoryNav();
        Wait_ajax();
        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Viewability);
        Click(ID, Pimco_IM_Search_Btn_ID);
        Wait_ajax();
        
        Click(Xpath, ClickButtononSearchresultspath(QA_Viewability, "Edit"));
        Wait_ajax();
        
        Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
        Wait_ajax();
        Thread.sleep(3000);
        Click(Xpath, Textpath("li", "Not Viewable"));
        Wait_ajax();
        Thread.sleep(3000);
        Click(ID, Pimco_IM_Save_Btn_ID);
    	Wait_ajax();
    	Click(ID, Pimco_IM_PMSG_OK_Button_ID);
    	Wait_ajax();
    	
    	logout();
    	PimcoGWMLogin();
    	
    	FulfilmentSearch(QA_Viewability);
    	Thread.sleep(6000);
    	ContactClick("QanoFirm", "IL");
    	Thread.sleep(6000);

    	Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");
	
    	Click(Xpath, Textpath("span", "Clear Cart"));
    	Wait_ajax();
    	Thread.sleep(3000);

	}
	
	//Validate when the part is set to orderable that it is viewable and orderable
	@Test(enabled = true , priority = 4)
	public void Pimco_TC_2_8_3_4() throws InterruptedException, IOException{
		
	    ManageinventoryNav();
        Wait_ajax();
        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Viewability);
        Click(ID, Pimco_IM_Search_Btn_ID);
        Wait_ajax();
        
        Click(Xpath, ClickButtononSearchresultspath(QA_Viewability, "Edit"));
        Wait_ajax();
        
        Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
        Wait_ajax();
        Thread.sleep(3000);
        Click(Xpath, Textpath("li", "Orderable"));
        Wait_ajax();
        Thread.sleep(3000);
        Click(ID, Pimco_IM_Save_Btn_ID);
    	Wait_ajax();
    	Click(ID, Pimco_IM_PMSG_OK_Button_ID);
    	Wait_ajax();
    	
    	logout();
    	PimcoGWMLogin();
    	
    	FulfilmentSearch(QA_Viewability);
    	Thread.sleep(6000);
    	ContactClick("QanoFirm", "IL");
    	Thread.sleep(6000);

    	Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsAddtoCartBtnXpath(QA_Viewability)),"QA_Viewability displays in 'Materials' Page");
	
    	Click(Xpath, Textpath("span", "Clear Cart"));
    	Wait_ajax();
    	Thread.sleep(3000);

	}
	
	public void ManageinventoryNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		

	}

	@BeforeMethod
	public void ClearCart() throws InterruptedException   {
		Clearcarts();
	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}

}
