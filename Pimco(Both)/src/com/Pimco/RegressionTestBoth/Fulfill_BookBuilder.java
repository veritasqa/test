package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class Fulfill_BookBuilder extends BaseTestPimco {

	public void Click_BookContent_Expand(String ProductCode) throws InterruptedException {

		Click(Xpath, ".//div[contains(text(),'" + ProductCode + "')]/preceding::input[1]");
		Wait_ajax();
		Thread.sleep(3000);
	}

	public String BookContent_Expanded(String ProductCode, String Expanded_Content) throws InterruptedException {

		return ".//div[contains(text(),'" + ProductCode + "')]/following::tr//td[text()='" + Expanded_Content + "']";

	}

	public String Inventory_Authentication_Stage(String Inven_Username, String Inven_Password, String Orderno) {

		return "http://" + Inven_Username + ":" + Inven_Password
				+ "@ver-sqlnew.cgx.net/ReportServer/Pages/ReportViewer.aspx?/Veritas/Staging/PickTickets/PickTicketPIMCO&rs:Command=Render&OrderID="
				+ Orderno;

	}

	public String Inventory_Authentication_Prod(String Inven_Username, String Inven_Password, String Orderno) {

		return "http://" + Inven_Username + ":" + Inven_Password
				+ "@ver-sqlnew.cgx.net/ReportServer/Pages/ReportViewer.aspx?/Veritas/Production/PickTickets/PickTicketPIMCO&rs:Command=Render&OrderID="
				+ Orderno;

	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_8_1_5_1_1() throws InterruptedException, AWTException {

		/* Verify Users are restricted from creating multiple books per order */

		FulfilmentSearch("PBB001");
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB001_40827));
		Thread.sleep(5000);
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, "PBB002");
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoCartBtnXpath(PBB002_41553));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Maximum of 1 Book Builder item per Order")),
				"Maximum of 1 Book Builder item per Order is not displayed");
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCO_Title_Path),
				" 'Set Customizable Options' page not displays");
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_testfactsheet_01);
		Wait_ajax();
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", QA_testfactsheet_01));

		// Click(Xpath, TextpathContains("td", QA_testfactsheet_01));
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(QA_testfactsheet_01),
				" 'QA_Testfactsheet_01' not displays under 'Book Content' section");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCO_BookContent_Msg_Path),
				"'Please note that Book Builder items in fulfillment mode may take up to 48 hours to process' message is not displayed above 'Save'button");
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("span", PBB002_41553)),
				PBB002_41553 + " displayed in the checkout page");
	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "Pimco_TC_2_8_1_5_1_1")
	public void Pimco_TC_2_8_1_5_1_2() throws InterruptedException {

		// Verify Remove button and Drag and Drop functionality

		// Click(Xpath, Containstextpath("span", "Checkout ("));
		// ContactClick("QanoFirm", "IL");
		// Wait_ajax();
		Click(Xpath, Pimco_SCP_EditVariables_btn_Path);
		Type(Xpath, Pimco_SCO_ClientName_Path, "Pimco");
		Type(Xpath, Pimco_SCO_DayMonthYear_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_SCO_FirmName_Path, "Firmtest");
		Click(Xpath, Pimco_SCO_TOCYes_ck_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_testfactsheet_02);
		Wait_ajax();
		Thread.sleep(5000);
		// Click(Xpath, TextpathContains("td", QA_testfactsheet_02));
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", QA_testfactsheet_02));

		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(QA_testfactsheet_01),
				" 'QA_Testfactsheet_01' not displays under 'Book Content' section");
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode2_Path).trim().equalsIgnoreCase(QA_testfactsheet_02),
				" 'QA_Testfactsheet_02' not displays under 'Book Content' section");
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, qa_testfundcard_02);
		Wait_ajax();
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", qa_testfundcard_02));

		// Click(Xpath, TextpathContains("td", qa_testfundcard_02));
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		DragandDrop(Xpath, Pimco_SCO_BookContent_Row3_Path, Xpath, Pimco_SCO_BookContent_Row1_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(qa_testfundcard_02),
				" 'qa_testfundcard_02' not displays under 'Book Content' section");
		Click(Xpath, Pimco_SCO_BookContent_Remove1_Path);
		Wait_ajax();
		Assert.assertFalse(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(qa_testfundcard_02),
				" 'qa_testfundcard_02'is displays under 'Book Content' section after removed");

	}

	@Test(priority = 3, enabled = true, dependsOnMethods = "Pimco_TC_2_8_1_5_1_2")
	public void Pimco_TC_2_8_1_5_1_3() throws InterruptedException {

		// Validate the Page Count and ability to successfully generate the
		// Proof with Large Book content
		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_CurrentPageCount_Path).trim().equalsIgnoreCase("10"),
				"'10' not displays for 'Current Page Count' field");
		Click(Xpath, Pimco_SCO_BookContent_Remove2_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_CurrentPageCount_Path).trim().equalsIgnoreCase("8"),
				"'8' not displays for 'Current Page Count' field");
		Click(Xpath, Pimco_SCO_BookContent_Remove1_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_CurrentPageCount_Path).trim().equalsIgnoreCase("6"),
				"'6' not displays for 'Current Page Count' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "No Items Added")),
				"No Items Added is not displayed");
		Wait_ajax();
		Type(Xpath, Pimco_SCO_KitLookup_Path, QA_BookbuilderKit1);
		Wait_ajax();
		Thread.sleep(4000);
		ExplicitWait_Element_Clickable(Xpath, Textpath("td", QA_BookbuilderKit1 + " "));
		Click(Xpath, Pimco_SCO_KitLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_CurrentPageCount_Path).trim().contains("5"),
				"'56' not displays for 'Current Page Count' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCO_BookContent_Row9_Path),
				"'9  kit components available for use as book content' not displays under 'Book Builder' section above 'Table Of Contents' field");
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");
	}

	@Test(priority = 4, enabled = true, dependsOnMethods = "Pimco_TC_2_8_1_5_1_3")
	public void Pimco_TC_2_8_1_5_1_4() throws InterruptedException {

		/*Verify the Error message for Exceeding Maximum Number of Page Count*/

		Click(Xpath, Pimco_SCP_EditVariables_btn_Path);
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QATest_FundCard1);
		Wait_ajax();
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(Xpath, TextpathContains("td", QATest_FundCard1));
		// Click(Xpath, TextpathContains("td", QATest_FundCard1));

		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "It exceed the Maximum allowed Pages")),
				"It exceed the Maximum allowed Pages not displayed");
		Assert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("*", QATest_FundCard1)),
				"'QATEST_FUNDCARD1' is displayed under 'Product Code' column in 'Book Content' section");
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");

	}

	@Test(priority = 5, enabled = true, dependsOnMethods = "Pimco_TC_2_8_1_5_1_4")
	public void Pimco_TC_2_8_1_5_1_5() throws InterruptedException {

		/*Verify Footnote, Table of Contents, sort order and Dedupe pieces for Book 
		 * Builder kits in Order confirmation page and pick ticket */

		Click(Xpath, Pimco_SCP_EditVariables_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_TestFundcard_03")),
				"QA_TestFundcard_03 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_testfactsheet_01")),
				"QA_testfactsheet_01 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_TestFundcard_01")),
				"QA_TestFundcard_01 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_testfactsheet_05")),
				"QA_testfactsheet_05 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "qa_testfundcard_02")),
				"QA_Testfundcard_02 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_TestFundCard_05")),
				"QA_TestFundCard_05 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_Testsupp_04")),
				"QA_Testsupp_04 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_Replace_Test_2")),
				"QA_Replace_Test_2 not displayed in under 'Product Code' column page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_FulfillmentOnly")),
				"QA_FulfillmentOnly not displayed in under 'Product Code' column page");
		Click_BookContent_Expand("QA_TestFundcard_03");
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded("QA_TestFundcard_03", "QA_Testsupp_02")),
				"QA_Testsupp_02 not displayed in the Product Code");
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded("QA_TestFundcard_03", "QA_Testsupp_03")),
				"QA_Testsupp_03 not displayed in the Product Code");
		Click_BookContent_Expand("QA_TestFundcard_03");
		Click_BookContent_Expand("QA_testfactsheet_05");
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded("QA_testfactsheet_05", "QA_Testsupp_03")),
				"QA_Testsupp_03 not displayed in the Product Code - QA_testfactsheet_05");
		Click_BookContent_Expand("QA_testfactsheet_05");
		Click_BookContent_Expand("qa_testfundcard_02");
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded("qa_testfundcard_02", "QA_Testsupp_02")),
				"QA_Testsupp_02 not displayed in the Product Code - qa_testfundcard_02");
		Click_BookContent_Expand("qa_testfundcard_02");
		Click_BookContent_Expand("QA_TestFundCard_05");
		Assert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded("QA_TestFundCard_05", "QA_Testsupp_03")),
				"QA_Testsupp_03 not displayed in the Product Code - QA_Testsupp_03");
		Click_BookContent_Expand("QA_TestFundCard_05");
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Wait_ajax();
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
		Wait_ajax();

		Click(ID, Pimco_SCP_Checkout_ID);

		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);

		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "QA_Testsupp_03")),
				"QA_Testsupp_03 is not displayed in the Order conformation page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "QA_Testsupp_02")),
				"QA_Testsupp_02 is not displayed in the Order conformation page");
		Click(ID, Pimco_LP_Logout_ID);
		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Containstextpath("a", "Pick"));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains("OrderID=" + OrderNumber), "Pick is not opened in the new window");
		Get_URL(Inventory_Authentication_Prod("Yannamalai", "Sachin05", "1767808"));
		ExplicitWait_Element_Not_Visible(Xpath, Containspath("span", "class", "WaitText"));
		Assert.assertTrue(Get_Title().trim().equalsIgnoreCase("PickTicketPIMCO - Report Viewer"),
				"PickTicketPIMCO - Report Viewer is not opened in the new window");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_Testsupp_02")),
				"QA_Testsupp_02 is not displayed in the PickTicketPIMCO - Report Viewer page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "QA_Testsupp_03")),
				"QA_Testsupp_03 is not displayed in the PickTicketPIMCO - Report Viewer page");
		Switch_Old_Tab();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBoth1Username, PIMCOBoth1Password);
		Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		// Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
