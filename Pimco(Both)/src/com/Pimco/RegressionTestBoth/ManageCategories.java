package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageCategories extends BaseTestPimco {

	public void Navigate_ManageCategory() throws InterruptedException {

		NavigateMenu(LP_Admin_path, LP_ManageCategory_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MC_Addcategory_Path);
	}

	public void Click_Insertbutton(String Category_Name) {

		Click(Xpath, ".//td[text()='" + Category_Name + "']//following::a[1][text()='Insert']");
	}

	public void Click_Deletebutton(String Category_Name) {

		Click(Xpath, ".//td[text()='" + Category_Name + "']//following::a[3][text()='Delete']");
	}

	public String Sub_Category(int ColumnOrder, String Category_Name, String ColumnName, String ColumnValue) {

		return ".//td[text()='" + Category_Name + "']//following::th[" + ColumnOrder + "][text()='" + ColumnName
				+ "']//following::td[text()='" + ColumnValue + "'][1]";
	}

	public String Sub_Category_Activeck(String Category_Name) {

		return ".//td[text()='" + Category_Name
				+ "']//following::th[5][text()='Active'][1]//following::input[@type='checkbox'][1]";
	}

	public void Click_Sub_ExpandCollapse(String Category_Name) {

		Click(Xpath, ".//td[text()='" + Category_Name + "']//preceding::input[1]");
	}

	public void Click_Sub_Category_Buttons(String Category_Name, String Button) {

		Click(Xpath, ".//td[text()='" + Category_Name + "']//following::a[text()='" + Button + "'][2]");
	}

	public void Click_Sub_Category_EditButtons(String Category_Name, String Button) {

		Click(Xpath, ".//td[text()='" + Category_Name + "']//following::a[text()='" + Button + "']");
	}

	public void Edit_Sub_Category(int ColumnOrder, String Category_Name, String ColumnName, String Text) {

		Type(Xpath, ".//td[text()='" + Category_Name + "']//following::th[" + ColumnOrder + "][text()='" + ColumnName
				+ "']//following::input[@type='text'][" + (ColumnOrder - 1) + "]", Text);
	}

	public void ManageinventoryNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

	}

	@Test(priority = 1, enabled = false)
	public void Pimco_TC_2_4_5_1_1() throws InterruptedException {

		/*
		 * Validate page opens and appropriate fields/tables are displaying
		 * */

		Navigate_ManageCategory();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Title_Path),
				"Manage Categories page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_CategoryName_Path),
				"'Category Name' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_CategoryDescription_Path),
				" 'Category Description' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Sort_Path), "'Sort' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Active_Path), " 'Active' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Active1_Path),
				"'Checkbox'  is not displayed under 'Active' column for each row in the 'Manage Categories' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Insert1_Path),
				" 'Insert' button is not displayed for each row in the 'Manage Categories' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Edit1_Path),
				"'Edit' button is not displayed for each row in the 'Manage Categories' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Delete1_Path),
				" 'Delete' button is not displayed for each row in the 'Manage Categories' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Addcategory_Path),
				"'Add Category' button is not displayed at the end of the 'Manage Categories' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MC_Refresh_Path),
				"'Refresh' button is not displayed at the end of the 'Manage Categories' grid");
		softAssert.assertAll();
	}

	@Test(priority = 2, enabled = false)
	public void Pimco_TC_2_4_5_1_2andPimco_TC_2_4_5_1_3() throws InterruptedException {

		/*
		 * Verify user is able to Insert Sub Category to the existing Category on Manage Categories page
		 * */

		Navigate_ManageCategory();
		Click_Insertbutton("QA Test Category");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Sub_Category(2, "QA Test Category", "Category Name", "New Item")),
				" 'New Item' is not displayed under 'Category Name' column");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Sub_Category(3, "QA Test Category", "Category Description", "New Item")),
				" 'New Item' is not displayed under 'Category Description' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Sub_Category(4, "QA Test Category", "Sort", "0")),
				" '0' is not displayed under 'Sort' column");
		softAssert.assertTrue(Element_Is_selected(Xpath, Sub_Category_Activeck("QA Test Category")),
				" checkbox is not checked off under 'Active' column");
		softAssert.assertAll();
		Click_Sub_ExpandCollapse("QA Test Category");
		Wait_ajax();
		Assert.assertFalse(
				Element_Is_Displayed(Xpath, Sub_Category(2, "QA Test Category", "Category Name", "New Item")),
				"sub category 'New Item' is displayed after collapsed");
		Click_Sub_ExpandCollapse("QA Test Category");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Sub_Category(2, "QA Test Category", "Category Name", "New Item")),
				"sub category 'New Item' is not displayed after Expanded");

		/*
		 * TC Pimco_TC_2_4_5_1_3 Starts Here
		 * 
		 * Verify user is able to Edit, Activate and Update the Sub Category and click Refresh
		 * */

		Click_Sub_Category_Buttons("QA Test Category", "Edit");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Edit_Sub_Category(2, "QA Test Category", "Category Name", "QA New Category Name");
		Edit_Sub_Category(3, "QA Test Category", "Category Description", "QA New Category Description");
		Edit_Sub_Category(4, "QA Test Category", "Sort", "1");
		Assert.assertTrue(Element_Is_selected(Xpath, Sub_Category_Activeck("QA Test Category")),
				" checkbox is not checked off under 'Active' column");
		Click_Sub_Category_EditButtons("QA Test Category", "Update");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Click(Xpath, Pimco_MC_Refresh_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Click_Sub_ExpandCollapse("QA Test Category");
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Sub_Category(2, "QA Test Category", "Category Name", "QA New Category Name")),
				"sub category 'New Item' is not displayed after Expanded - Pimco_TC_2_4_5_1_3");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Sub_Category(2, "QA Test Category", "Category Name", "QA New Category Name")),
				" 'New Item' is not displayed under 'Category Name' column");
		softAssert
				.assertTrue(
						Element_Is_Displayed(Xpath,
								Sub_Category(3, "QA Test Category", "Category Description",
										"QA New Category Description")),
						" 'New Item' is not displayed under 'Category Description' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Sub_Category(4, "QA Test Category", "Sort", "1")),
				" '0' is not displayed under 'Sort' column");
		softAssert.assertTrue(Element_Is_selected(Xpath, Sub_Category_Activeck("QA Test Category")),
				" checkbox is not checked off under 'Active' column");
		softAssert.assertAll();
		Click_Sub_Category_Buttons("QA Test Category", "Delete");
		Accept_Alert();
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Assert.assertFalse(
				Element_Is_Displayed(Xpath,
						Sub_Category(2, "QA Test Category", "Category Name", "QA New Category Name")),
				"sub category is displayed after deleted");

	}

	@Test(priority = 3, enabled = false)
	public void Pimco_TC_2_4_5_2_1() throws InterruptedException {

		/*
		 * Verify user is able to Add Category and the New Category displays on Manage Categories page
		 * */

		Navigate_ManageCategory();
		Click(Xpath, Pimco_MC_Addcategory_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Type(Xpath, Pimco_MC_AddCategoryName_Path, "QA Test Name");
		Type(Xpath, Pimco_MC_AddCategoryDescription_Path, "QA Test Description");
		Type(Xpath, Pimco_MC_Addsort_Path, "1");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_MC_AddActive_Path), "Active Checkbox is not checked");
		Click(Xpath, Pimco_MC_AddInsert_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "QA Test Name")),
				"Added Category is not displayed");

	}

	@Test(priority = 4, enabled = false, dependsOnMethods = "Pimco_TC_2_4_5_2_1")
	public void Pimco_TC_2_4_5_2_2() throws InterruptedException {

		/*
		 * Verify user is able to Insert Sub Category to the New Category on Manage Categories page
		 * */

		Click_Insertbutton("QA Test Name");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Click_Sub_Category_Buttons("QA Test Name", "Edit");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Edit_Sub_Category(2, "QA Test Name", "Category Name", "QA New Category Name");
		Edit_Sub_Category(3, "QA Test Name", "Category Description", "QA New Category Description");
		Assert.assertTrue(Element_Is_selected(Xpath, Sub_Category_Activeck("QA Test Name")),
				" checkbox is not checked off under 'Active' column");
		Click_Sub_Category_EditButtons("QA Test Name", "Update");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Wait_ajax();
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Sub_Category(2, "QA Test Name", "Category Name", "QA New Category Name")),
				" 'New Item' is not displayed under 'Category Name' column - Pimco_TC_2_4_5_2_2");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Sub_Category(3, "QA Test Name", "Category Description", "QA New Category Description")),
				" 'New Item' is not displayed under 'Category Description' column  - Pimco_TC_2_4_5_2_2");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Sub_Category(4, "QA Test Name", "Sort", "1")),
				" '0' is not displayed under 'Sort' column Pimco_TC_2_4_5_2_2");
		softAssert.assertTrue(Element_Is_selected(Xpath, Sub_Category_Activeck("QA Test Name")),
				" checkbox is not checked off under 'Active' column - Pimco_TC_2_4_5_2_2");
		softAssert.assertAll();

	}

	@Test(priority = 5, enabled = true)
	public void Pimco_TC_2_4_5_2_3() throws InterruptedException, IOException {

		/*
		 * Verify the New Category displays under Materials when a piece is assigned to that Category
		 * */

		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_Multifunction, "Edit"));
		Wait_ajax();
		Click(Xpath, CatagoriesCheckbox("QA Test Name"));
		Click(Xpath, CatagoriesCheckbox("QA New Category Name"));
		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		login(PIMCOBothUsername, PIMCOBothPassword);
		Hover(Xpath, Textpath("span", "Materials"));
		Hover(Xpath, Textpath("span", "QA Test Name"));
		Hover(Xpath, Textpath("span", "QA New Category Name"));

	}

	@Test(priority = 6, enabled = true)
	public void Pimco_TC_2_4_5_2_4() throws InterruptedException, IOException {

		/*
		 * Verify user is able to Delete the New Category that is created
		 * */
		Navigate_ManageCategory();
		Click_Deletebutton("QA Test Name");
		Thread.sleep(2000);
		Accept_Alert();
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MC_Loading_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("td", "QA Test Name")),
				"Added Category is displayed after Deletion");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}
}
