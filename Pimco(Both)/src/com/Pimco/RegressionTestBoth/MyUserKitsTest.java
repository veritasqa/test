package com.Pimco.RegressionTestBoth;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MyUserKitsTest extends BaseTestPimco{

	public static String MyuserKit_Kitname = "QAutomation_QA_Kit_"+Get_Todaydate("MM")+Get_Todaydate("dd")+Get_Todaydate("YY");

	
	//Validate Select a recipient link is working properly (Admin)

	@Test(enabled=true, priority = 2)
	public void Pimco_TC_2_5_7_1_1() throws InterruptedException {
		
		Clearcarts();
		Click(Xpath, Pimco_LP_MUK_SelectaRecipient_Path);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("i","QAutomation_QA_Kit_"+Get_Todaydate("MM")+Get_Todaydate("dd")+Get_Todaydate("YY") )),"QAutomation_QA_Kit_"+Get_Todaydate("MM")+Get_Todaydate("dd")+Get_Todaydate("YY")+"-- Kit is missing");
	}
	
	
//	Place an order using "Add to cart" button from "My User Kits" widget
	@Test(priority = 3)
	public void Pimco_TC_2_5_7_1_3() throws InterruptedException{
		 
		Clearcarts();
		Click(Xpath, Textpath("span","Home"));
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
			Click(Xpath, Pimco_LP_MUK_SelectaRecipient_Path);
			Wait_ajax();
			ContactClick("QanoFirm", "IL");
		Type(Xpath, Pimco_LP_MUK_Qty_Path(MyuserKit_Kitname), "1");
		Wait_ajax();
		Click(Xpath, Pimco_LP_MUK_AddtoCart_Path(MyuserKit_Kitname));
		Wait_ajax();
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Thread.sleep(2000);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		
		
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
				
		
	}
	
	//Validate Edit/View My user Kits are working properly (Admin)
	@Test(priority = 4)
	public void Pimco_TC_2_5_7_1_2() throws InterruptedException{
		Clearcarts();
		Click(Xpath, Pimco_LP_MUK_EditMyUserKits_Btn_Path);
		
	     ExplicitWait_Element_Clickable(Xpath,EditKitBuildEditRemoveBtnPath(MyuserKit_Kitname,"Edit"));

		Click(Xpath,EditKitBuildEditRemoveBtnPath(MyuserKit_Kitname,"Edit"));
		
	     ExplicitWait_Element_Clickable(ID, Pimco_UK_NWUK_AddToCart_ID);
	   //  Saved!
	     
	        Click(ID, Pimco_UK_NWUK_Kitcontainer_Arrow_ID);
			Wait_ajax();

	    	Click(Xpath, Pimco_UK_NWUK_QA_Folder3_Path);
			Wait_ajax();

	        Click(ID, Pimco_UK_NWUK_SaveUserKit_ID);
			Wait_ajax();
			
			
	        Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", "Saved!")),"Saved Message is Missing");
	        Assert.assertTrue(Get_Attribute(ID, Pimco_UK_NWUK_Kitcontainer_DDinput_ID, "value").equalsIgnoreCase("QA_Folder3"),"QA_Folder3 -  Is missing on the Kit container Field DropDown");

	        
	        Click(Xpath, Textpath("span","Home"));
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
			Click(Xpath, Pimco_LP_MUK_SelectaRecipient_Path);
			Wait_ajax();
			ContactClick("QanoFirm", "IL");
			
			Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("i","QAutomation_QA_Kit_"+Get_Todaydate("MM")+Get_Todaydate("dd")+Get_Todaydate("YY") )),"QAutomation_QA_Kit_"+Get_Todaydate("MM")+Get_Todaydate("dd")+Get_Todaydate("YY")+"-- Kit is missing");
		
			
			
			
			ManageUserKitsNav();
			
			Click(Xpath,EditKitBuildEditRemoveBtnPath(MyuserKit_Kitname,"Delete"));
			
			Accept_Alert();
			   
		    Thread.sleep(5000);
	        Assert.assertFalse(Element_Is_Displayed(Xpath,Textpath("td", MyuserKit_Kitname)),"Userkits not diplayed in userkits page");

	        
	}
	
	
	
	
	
	/*Pre-condition:  
	�A user kit should be created in 'Admin-)Manage User  Kits' page with :
		Product Code:QA_Kit
		Title:Admin Test
		Kit Container:QA_Folder */

	@Test(enabled= false,priority = 1)
	public void MyuserPrecondition() throws InterruptedException{
		
		ManageUserKitsNav();
		
		Click(ID, Pimco_UK_AddNewUserKit_Btn_ID);
		Wait_ajax();
		
		
		//Steps -18-30
        Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("h1", "User Kit")),"User Kitheader  Not displayed");
      
        Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("h1", "User Kit Owner")),"User Kit Owner header Not displayed");
        
        Type(ID, Pimco_UK_NWUK_ProductCodeTxtBox_ID, "QA_Kit");
        
        Type(ID, Pimco_UK_NWUK_TitleTextBox_ID, "Admin Test");
        
        Click(ID, Pimco_UK_NWUK_Kitcontainer_Arrow_ID);
		Wait_ajax();
		
		Click(Xpath, Pimco_UK_NWUK_QA_Folder_Path);
		
		
        Click(ID, Pimco_UK_NWUK_SaveUserKit_ID);
        Wait_ajax();
		
        ExplicitWait_Element_Clickable(ID, Pimco_UK_NWUK_AddToCart_ID);
        
        Assert.assertTrue(Get_Attribute(ID, Pimco_UK_NWUK_ProductCodeTxtBox_ID, "value").equalsIgnoreCase("QAutomation_QA_Kit_"+Get_Todaydate("MM")+Get_Todaydate("dd")+Get_Todaydate("YY"))," Kit name Not displays (Login Username_QA_Kit_MMDDYY) in 'User Kit' section in 'Product Code' field");
	
        Click(ID, Pimco_UK_AFI_Location_DD_Arrow_ID);
        Wait_ajax();
        Click(Xpath, li_value("Folder"));
        
        
        Type(ID, Pimco_UK_AFI_ProductCode_Input_ID, "QA_Folder");
        Thread.sleep(3000);
        Click(ID, Pimco_UK_AFI_Addtokit_ID);
        
        ExplicitWait_Element_Clickable(Xpath,EditKitBuildEditRemoveBtnPath("QA_Folder", "Edit"));
        
        Click(Xpath, Textpath("span","Home"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
	}
	
	
	public void ManageUserKitsNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage User Kits"));
		ExplicitWait_Element_Clickable(ID, Pimco_UK_AddNewUserKit_Btn_ID);
		Wait_ajax();
		

	}
	
	@BeforeClass
	public void Widgetverify() throws InterruptedException{
	if(!Element_Is_Displayed(Xpath, WidgetNamePath("My User Kits"))){
		Hover(Xpath, Textpath("h3", "Add Widgets"));
		Wait_ajax();
		Click(Xpath, Textpath("a","My User Kits"));
		
		
	}
	
	ManageUserKitsNav();
	
	Click(ID, Pimco_UK_AddNewUserKit_Btn_ID);
	Wait_ajax();

	Click(ID, Pimco_UK_NWUK_Kitcontainer_Arrow_ID);
	Wait_ajax();
	
	List<WebElement> TRCollection = driver
			.findElements(By.xpath("//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li"));

	for (WebElement tr : TRCollection) {

		UserKits.add(tr.getText());
	}

	int i = 1;
	for (String s : UserKits) {

		if (s.trim().contains("QA_Folder")&&!(s.trim().contains("QA_Folder2"))&&!(s.trim().contains("QA_Folder3"))) {
			Pimco_UK_NWUK_QA_Folder_Path = "//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li["+i+"]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
		}
		
		if (s.trim().contains("QA_Folder2")) {
			Pimco_UK_NWUK_QA_Folder2_Path = "//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li["+i+"]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
		}
		
		if (s.trim().contains("QA_Folder3")) {
			Pimco_UK_NWUK_QA_Folder3_Path = "//*[@id='ctl00_cphContent_cbKitContainer_DropDown']/div/ul/li["+i+"]/table/tbody/tr/td[1]/table/tbody/tr[1]/td";
		}
		
		i++;
	}	

	Click(ID, Pimco_UK_NWUK_Kitcontainer_Arrow_ID);
	Wait_ajax();
	}
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
}
