package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class Reports extends BaseTestPimco {

	public void View_Report(String Report) {

		Click(Xpath, ".//td[text()='" + Report + "']//following::a[1][text()='View Report']");

	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_3_1_1() throws InterruptedException {

		/* Validate Reports Page displays section headers "Available Reports
		Sections, Report Scheduler, My Reports Subscription" 
		*/

		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Available_Path),
				"'Available Reports' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Scheduler_Path),
				"'Report Scheduler' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Subscriptions_Path),
				" 'My Report Subscriptions' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Scheduler_Path),
				" 'Schedular section' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_ScheduleDD_Path),
				" 'Schedule drop down' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_ReportTypeDD_Path),
				" 'Report type drop down' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Email_Path), " 'Email' field is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Comments_Path),
				" 'Comments' field is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_Subscribe_Path),
				" 'Subscribe' field is not displays");

		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = true)
	public void Pimco_TC_2_3_1_2() throws InterruptedException {

		/* 
		 * Validate User is able to view report upon clicking "View Report"
		*/

		Assert.assertTrue(Get_DropDown(Xpath, Pimco_Reports_Owner_Path).equalsIgnoreCase("PIMCO_GWM"),
				"PIMCO_GWM is not displayed in the Business owner drop down");
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		View_Report("Items_InventoryStatusReport");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Switch_To_Iframe("ContentWindow");
		ExplicitWait_Element_Clickable(Xpath, Pimco_Reports_Viewreportbtn_Path);
		softAssert.assertTrue(Get_DropDown(Xpath, Pimco_Reports_ItemStatus_Path).equalsIgnoreCase("Active"),
				"'Active' is not displayed in 'Item Status' dropdown ");
		softAssert.assertTrue(Get_DropDown(Xpath, Pimco_Reports_Formowner_Path).equalsIgnoreCase("All"),
				"'All' is not displayed in 'Item Status' dropdown ");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_ProductCode_Path),
				"'Product Code' field is not displayed and is read only");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_ItemCode_Path),
				"'Job No' field is not displayed and is read only");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_JobNo_Path),
				"'Item Code' field is not displayed and is read only");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_ProductCodeNull_Path),
				"'ProductCode Null' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_ItemCodeNull_Path),
				"'ItemCode Null' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_JobNoNull_Path),
				"'Job No Null' is not displays");
		softAssert.assertTrue(Get_DropDown(Xpath, Pimco_Reports_Country_Path).equalsIgnoreCase("(Null)"),
				"'(Null)' is not displayed in 'Country Status' dropdown ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Inventory Status Report")),
				"''Inventory Status Report' is not displays");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("div", "Report run on " + Get_Todaydate("M/d/YYYY"))),
				"Report run on (Today's date MM/DD/YYYY HH:MM:SS AM/PM ) is not displayed");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div",
								"Report run criteria: Item Status = Active; Role = PIMCO_GWM; Form Owner = ALL; ")),
				"Report run criteria: Item Status = Active; Role = PIMCO_GWM; Form Owner = ALL; is not displayed");
		softAssert.assertAll();
		Switch_To_Default();
		Click(Xpath, Pimco_Reports_Close_Path);

	}

	@Test(priority = 3, enabled = true)
	public void Pimco_TC_2_3_1_3() throws InterruptedException {

		/*
		 * Validate "Available Reports Section" displays only 10 Report Name by default
		 * */

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_TenthRow_Path),
				"'10' reports are not displayed by default under 'Report Name' column in 'Available Reports' Section - 1st page");
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_TenthRow_Path),
				"'10' reports are not displayed by default under 'Report Name' column in 'Available Reports' Section - 2nd page");
	}

	@Test(priority = 4, enabled = true)
	public void Pimco_TC_2_3_1_4() throws InterruptedException {

		/*
		 * Validate upon selecting any of the Report Name "Report Schedule" is enabled
		 * */

		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_ScheduleDD_Path),
				"'Schedule' drop down is not disabled under 'Report Scheduler' section");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_ReportTypeDD_Path),
				"'Report Type' drop down is not disabled under 'Report Scheduler' section");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_Email_Path),
				"'Email' is not disabled under 'Report Scheduler' section");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_Comments_Path),
				"Comments field is not disabled");
		softAssert.assertFalse(Element_Is_Enabled(Xpath, Pimco_Reports_Subscribe_Path),
				"Subscribe button is not disabled");
		softAssert.assertAll();
		Click(Xpath, Textpath("td", "Billing_Monthly"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Billing_Monthly Scheduler")),
				"'Billing_ Monthly Scheduler' header is not displayed ");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ScheduleDD_Path),
				"'Schedule' drop down is not enabled under 'Report Scheduler' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ReportTypeDD_Path),
				"'Report Type' drop down is not enabled under 'Report Scheduler' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_Email_Path),
				"'Email' not enabled under 'Report Scheduler' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_Comments_Path), "Comments field is not enabled");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_Subscribe_Path),
				"Subscribe button is not enabled");
		softAssert.assertAll();
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_Parameters_Path),
				"'Parameters' section is not displayed under 'Billing_ Monthly Scheduler' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersFormowner_Path),
				"'Form Owner' drop down is not displayed in 'Parameters' section");
		softAssert.assertTrue(
				Get_Attribute(Xpath, Pimco_Reports_ParametersFormowner_Path, "value").equalsIgnoreCase("All"),
				"'ALL' is populated in 'Form Owner' drop down");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersStartDate_Path),
				"'Start Date' field is not displayed in 'Parameters' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersStartDateCal_Path),
				"'Calendar' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersStartDateTime_Path),
				" 'Clock' icon is not displayed by 'Start Date' field");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersEndDate_Path),
				"'End Date' field is not displayed in 'Parameters' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersEndDateCal_Path),
				"'Calendar' icon is not displayed by 'End Date' field");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersEndDateTime_Path),
				"'Clock' icon is not displayed by 'End Date' field");
		softAssert.assertTrue(false, "'Business Owner' field is not displayed in 'Parameters' section");
		softAssert.assertTrue(Element_Is_Enabled(Xpath, Pimco_Reports_ParametersEndDateTime_Path),
				"'Clock' icon is not displayed by 'End Date' field");
		softAssert.assertAll();

	}

	@Test(priority = 5, enabled = true)
	public void Pimco_TC_2_3_1_5() throws InterruptedException {

		/*
		 * Validate all the fields/buttons/calendar/dropdowns of  "Report Schedule" are working fine
		 * */

		Click(Xpath, Textpath("td", "Billing_Monthly"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Select_lidropdown(Pimco_Reports_ScheduleDD_Path, Xpath, "Weekly - Monday");
		Select_lidropdown(Pimco_Reports_ReportTypeDD_Path, Xpath, "Excel");
		Type(Xpath, Pimco_Reports_Email_Path, Email);
		Type(Xpath, Pimco_Reports_Comments_Path, "QA Test");
		Select_lidropdown(Pimco_Reports_ParametersFormowner_Path, Xpath, "Regulatory");
		Type(Xpath, Pimco_Reports_ParametersStartDate_Path, Get_FutureCSTTime("M/d/YYYY"));
		Type(Xpath, Pimco_Reports_ParametersEndDate_Path, Get_Futuredate("M/d/YYYY"));
		Click(Xpath, Pimco_Reports_Subscribe_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Subscriptions_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Sucessfully Subscribed To Billing_Monthly!")),
				"Sucessfully Subscribed To Billing_Monthly! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "User Qaauto Automation")),
				"User (User's First Name Last Name) [ID number] Subscribed To Billing_ Monthly (Email: firstname.lastname@rrd.com), Format: Excel, Period: Weekly - Monday)' is not displayed under 'Subscription' column in 'My Report Subscriptions' section");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("td",
								"Subscribed To Billing_Monthly (Email: ver.qaauto@rrd.com, Format: Excel, Period: Weekly - Monday)")),
				"User (User's First Name Last Name) [ID number] Subscribed To Billing_ Monthly (Email: firstname.lastname@rrd.com), Format: Excel, Period: Weekly - Monday)' is not displayed under 'Subscription' column in 'My Report Subscriptions' section");
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Unsubscribe"));
		Click(Xpath, Textpath("span", "OK"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Subscriptions_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_TenthRow_Path),
				"'10' reports are not displayed by default under 'Report Name' column in 'Available Reports' Section");
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_Reports_ItemsinPage_Path).trim().equalsIgnoreCase("31 items in 4 pages"),
				"( 31 items in 4 pages) is not displays on the  right side end of the grid in 'Available Reports' section");
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "page '2' is not highlighted");
		Click(Xpath, Pimco_Reports_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")), "page '3' is not highlighted - Next page Nav");
		Click(Xpath, Pimco_Reports_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("4")), "page '4' is not highlighted - Last page Nav");
		Click(Xpath, Pimco_Reports_Previouspage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"page '3' is not highlighted - Previous page Nav");
		Click(Xpath, Pimco_Reports_Firstpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")), "page '1' is not highlighted - First page Nav");

	}

	@Test(priority = 6, enabled = true)
	public void Pimco_TC_2_3_1_6() throws InterruptedException {

		/*
		 * Ensure paging functionality on the available reports table is working fine.
		 * */

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_TenthRow_Path),
				"'10' reports are not displayed by default under 'Report Name' column in 'Available Reports' Section");
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "page '2' is not highlighted");
		Click(Xpath, Pimco_Reports_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")), "page '3' is not highlighted - Next page Nav");
		Click(Xpath, Pimco_Reports_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("4")), "page '4' is not highlighted - Last page Nav");
		Click(Xpath, Pimco_Reports_Previouspage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"page '3' is not highlighted - Previous page Nav");
		Click(Xpath, Pimco_Reports_Firstpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")), "page '1' is not highlighted - First page Nav");

	}

	@Test(priority = 7, enabled = true)
	public void Pimco_TC_2_3_1_7() throws InterruptedException {

		/*
		 * Ensure Page Size selection is working fine
		 * */

		Select_lidropdown(Pimco_Reports_Pagesizearrow_Path, Xpath, "20");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_Reports_TwentiethRow_Path),
				"'20' reports are not displayed by default under 'Report Name' column in 'Available Reports' Section");
		Select_lidropdown(Pimco_Reports_Pagesizearrow_Path, Xpath, "50");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Pimco_Reports_ThirtysecondRow_Path),
				"(50) Reports are not displayed in 'Available Reports' section");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Pimco_Reports_Pagesize_Path),
				"(50) Reports are not displayed in 'Available Reports' section - page size is field is still displayed");
		softAssert.assertAll();

	}

	@Test(priority = 8, enabled = true)
	public void Pimco_TC_2_3_1_8() throws InterruptedException {

		/*
		 * Verify user is able to filter Country field by US and appropriate piece displays 
		 * */

		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		View_Report("Items_InventoryStatusReport");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Switch_To_Iframe("ContentWindow");
		ExplicitWait_Element_Clickable(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_Reports_Country_Path, "US");
		Click(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Thread.sleep(10000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "US")),
				"'US' is not displayed under 'Country' column in 'Inventory Status Report'");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Textpath("div", "CA")),
				"only 'US' is not displayed under 'Country' column in 'Inventory Status Report' ");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Textpath("div", "Both")),
				"only 'US' is not displayed under 'Country' column in 'Inventory Status Report' ");
		softAssert.assertAll();
		Click(Xpath, Pimco_Reports_ProductCodeNull_Path);
		Type(Xpath, Pimco_Reports_ProductCode_Path, "QA_Multifunction_US");
		Click(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Thread.sleep(5000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "QA_Multifunction_US")),
				"'QA_Multifunction_US' is not displayed ");
		Select_DropDown_VisibleText(Xpath, Pimco_Reports_Exportdd_Path, "Excel");
		Click(Xpath, Pimco_Reports_Export_Path);
		Switch_To_Default();
		softAssert.assertAll();
		Click(Xpath, Pimco_Reports_Close_Path);

	}

	@Test(priority = 9, enabled = true)
	public void Pimco_TC_2_3_1_9() throws InterruptedException {

		/*
		 * Verify user is able to filter Country field by CA and appropriate piece displays 
		 * */

		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		View_Report("Items_InventoryStatusReport");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Switch_To_Iframe("ContentWindow");
		ExplicitWait_Element_Clickable(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_Reports_Country_Path, "CA");
		Click(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Thread.sleep(10000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "CA")),
				"'CA' is not displayed under 'Country' column in 'Inventory Status Report'");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Textpath("div", "US")),
				"only 'CA' is not displayed under 'Country' column in 'Inventory Status Report' ");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Textpath("div", "Both")),
				"only 'CA' is not displayed under 'Country' column in 'Inventory Status Report' ");
		softAssert.assertAll();
		Click(Xpath, Pimco_Reports_ProductCodeNull_Path);
		Type(Xpath, Pimco_Reports_ProductCode_Path, "QA_MarketingOnlyCA");
		Click(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Thread.sleep(5000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "QA_MarketingonlyCA")),
				"'QA_MarketingonlyCA' is not displayed ");
		Select_DropDown_VisibleText(Xpath, Pimco_Reports_Exportdd_Path, "PDF");
		Switch_To_Default();
		softAssert.assertAll();
		Click(Xpath, Pimco_Reports_Close_Path);

	}

	@Test(priority = 10, enabled = true)
	public void Pimco_TC_2_3_1_10() throws InterruptedException {

		/*
		 * Verify user is able to filter Country field by Both and appropriate piece displays  
		 * */

		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		View_Report("Items_InventoryStatusReport");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Switch_To_Iframe("ContentWindow");
		ExplicitWait_Element_Clickable(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_Reports_Country_Path, "Both");
		Click(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Thread.sleep(10000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Both")),
				"'Both' is not displayed under 'Country' column in 'Inventory Status Report'");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Textpath("div", "US")),
				"only 'CA' is not displayed under 'Country' column in 'Inventory Status Report' ");
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Textpath("div", "CA")),
				"only 'Both' is not displayed under 'Country' column in 'Inventory Status Report' ");
		softAssert.assertAll();
		Click(Xpath, Pimco_Reports_ProductCodeNull_Path);
		Type(Xpath, Pimco_Reports_ProductCode_Path, "qa_kitonfly_4");
		Click(Xpath, Pimco_Reports_Viewreportbtn_Path);
		Thread.sleep(5000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "qa_kitonfly_4")),
				"'qa_kitonfly_4' is not displayed ");
		Select_DropDown_VisibleText(Xpath, Pimco_Reports_Exportdd_Path, "Word");
		Switch_To_Default();
		softAssert.assertAll();
		Click(Xpath, Pimco_Reports_Close_Path);

	}

	@Test(priority = 11, enabled = true)
	public void Pimco_TC_2_3_2_1() throws InterruptedException {

		/*
		 * Validate "My Report Subscriptions" displays Subscription link   
		 * */

		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);
		Click(Xpath, Textpath("td", "Items_InventoryStatusReport"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Path);

		Select_lidropdown(Pimco_Reports_ScheduleDD_Path, Xpath, "Daily - 3PM");
		Select_lidropdown(Pimco_Reports_ReportTypeDD_Path, Xpath, "PDF");
		Type(Xpath, Pimco_Reports_Email_Path, Email);
		Type(Xpath, Pimco_Reports_Comments_Path, "QA Test");
		Click(Xpath, Pimco_Reports_Subscribe_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Subscriptions_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("span", "Sucessfully Subscribed To Items_InventoryStatusReport!")),
				"Sucessfully Subscribed To Items_InventoryStatusReport! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "User Qaauto Automation")),
				"User (User's First Name Last Name) [ID number] Subscribed To Billing_ Monthly (Email: firstname.lastname@rrd.com), Format: Excel, Period: Weekly - Monday)' is not displayed under 'Subscription' column in 'My Report Subscriptions' section");
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containstextpath("td",
								"Subscribed To Items_InventoryStatusReport (Email: ver.qaauto@rrd.com, Format: PDF, Period: Daily - 3PM)")),
				"User (User's First Name Last Name) [ID number] Subscribed To Billing_ Monthly (Email: firstname.lastname@rrd.com), Format: Excel, Period: Weekly - Monday)' is not displayed under 'Subscription' column in 'My Report Subscriptions' section");
		softAssert.assertAll();
		Click(Xpath, Textpath("a", "Unsubscribe"));
		Click(Xpath, Textpath("span", "OK"));
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_Reports_Loading_Subscriptions_Path);

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Click(Xpath, LP_Reports_path);
		Wait_ajax();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
