package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageOrder extends BaseTestPimco {

	public void ManageOrdersNav() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Orders"));
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Searchbtn_Path);
		Wait_ajax();

	}

	public void Pre_Req() throws InterruptedException {

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, SearchResultsAddtoCartBtnXpath(QA_Multifunction));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
		ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);
		Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
		ExplicitWait_Element_Visible(Xpath, Pimco_SCP_DeliveryMethod_Path);
		Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
		Click(ID, Pimco_SCP_Checkout_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number is " + OrderNumber);
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));

	}

	@Test(priority = 2, enabled = false)
	public void Pimco_TC_2_4_10_1_2() throws InterruptedException {

		// Verify Search and Clear functionality in Search Order works

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Type(Xpath, Pimco_MO_Shipperfield_Path, "UPS Ground");
		Select_lidropdown(Pimco_MO_Statusdrop_arrow_Path, Xpath, "NEW");
		Type(Xpath, Pimco_MO_Startdatefield_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_MO_Enddatefield_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_MO_RecipientName_Path, "QanoFirm IL");
		Type(Xpath, Pimco_MO_Statefield_Path, "IL");
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderNo1_Path).equalsIgnoreCase(OrderNumber),
				OrderNumber + " (Order #) from Pre-Condition is not displays in 'Search Results' grid ");
		softAssert.assertTrue(
				Get_Text(Xpath, Pimco_MO_SR_Orderedby1_Path).equalsIgnoreCase(Both_Fname1 + " " + Both_Lname),
				"(First Name Last Name) not displays under 'Ordered by' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderDate1_Path).contains(Get_Todaydate("M/d/YYYY")),
				"Date is not displayed under Order date column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Shipper1_Path).equalsIgnoreCase("UPS Ground"),
				"UPS Ground is not displayed under shippers column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Status1_Path).equalsIgnoreCase("NEW"),
				"NEW is not displayed under Status column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Recipient1_Path).equalsIgnoreCase("QanoFirm IL"),
				"Recipient name is not displayed under Recipient column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_State1_Path).equalsIgnoreCase("IL"),
				"State name is not displayed under State column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Trackingno1_Path).trim().equalsIgnoreCase(""),
				"\"\" is not displayed under Tracking link column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Ordertype1_Path).equalsIgnoreCase("PIMCO_GWM_FULFILLMENT"),
				"PIMCO_GWM_FULFILLMENT is not displayed under Tracking link column");
		softAssert.assertAll();
		Click(Xpath, Pimco_MO_Clearbtn_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Ordernofield_Path, "value").isEmpty(),
				"'Order #' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Trackingnofield_Path, "value").isEmpty(),
				"'Tracking #' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Statusdrop_Path, "value").isEmpty(),
				"'Status' dropdown is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Typedropdown_Path, "value").isEmpty(),
				"'Type' dropdown is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Userfield_Path, "value").isEmpty(),
				"'User' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Shipperfield_Path, "value").isEmpty(),
				"'Shipper' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_RecipientName_Path, "value").isEmpty(),
				"'Recipient' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Startdatefield_Path, "value").isEmpty(),
				"'Start date' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Enddatefield_Path, "value").isEmpty(),
				"'End Date' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Addressfield_Path, "value").isEmpty(),
				"'Address' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Cityfield_Path, "value").isEmpty(),
				"'City' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Statefield_Path, "value").isEmpty(),
				"'State/Province' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_Zipfield_Path, "value").isEmpty(),
				"'Zip/Postal Code' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertTrue(Get_Attribute(Xpath, Pimco_MO_UserGroup_Path, "value").isEmpty(),
				"'UserGroup' field is not cleared in the 'Search Order' grid after clicking 'Clear' button");
		softAssert.assertAll();
		Wait_ajax();

	}

	@Test(priority = 3, enabled = false)
	public void Pimco_TC_2_4_10_1_3() throws InterruptedException {

		// Verify search order copy functionality

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Click(Xpath, Pimco_MO_SearchresultsCopy(OrderNumber));
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("span", "Please select a contact to copy the order for.")),
				"Please select a contact to copy the order for. is not dispalyed in the contact search page");
		ContactClick("QanoFirm", "IL");
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not dispalyed in the Shopping cart page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not dispalyed in the Shopping cart page");
		Click(Xpath, Pimco_SCP_Remove_btn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Item(s) have been removed from your cart.")),
				"Item(s) have been removed from your cart. is not dispalyed in the Shopping cart page");
	}

	@Test(priority = 4, enabled = false)
	public void Pimco_TC_2_4_10_1_4() throws InterruptedException {

		// Validate that select order - view functionality works appropriately
		ManageOrdersNav();
		Type(Xpath, Pimco_MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, Pimco_MO_Viewbtn_Path);

		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Gen_OrderSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_GeneralTab_Path), "General Tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_Statustitle_Path), "'Status' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_Ticketnumber1_Path).equalsIgnoreCase(OrderNumber),
				"(Order Number) is not displays in 'Manage Order' field on header bar");
		softAssert.assertTrue(
				Get_Text(Xpath, Pimco_MO_Gen_Username1_Path).equalsIgnoreCase(Both_Fname1 + " " + Both_Lname),
				" (First Name Last Name) not displays under 'User Name' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_Orderdate1_Path).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"(Today's Date - MM/DD/YYYY) not displays under 'Order Date' column");
		softAssert.assertFalse(Get_Text(Xpath, Pimco_MO_Gen_Shipdate1_Path).isEmpty(),
				"(Date - MM/DD/YYYY) not displays under 'Ship Date' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_Closedate1_Path).trim().equalsIgnoreCase(""),
				"\"\" not displays under 'Close Date' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_Canceldate1_Path).trim().equalsIgnoreCase(""),
				"\"\" not displays under 'Cancel Date' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_OrderType1_Path).equalsIgnoreCase("PIMCO_GWM_FULFILLMENT"),
				"PIMCO_GWM_FULFILLMENT not displays under 'Order Type' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_ApprovalRequired1_Path).equalsIgnoreCase("False"),
				"False not displays under 'Approval Required' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_HoldForComplete1_Path).contains("False"),
				"False not displays under 'Hold For Complete' column");
		softAssert.assertFalse(Element_Is_selected(Xpath, Pimco_MO_Gen_OnBackOrder1_Path),
				"On Backorder check box is checked");
		softAssert.assertFalse(Element_Is_selected(Xpath, Pimco_MO_Gen_OnHold1_Path), "On hold check box is checked");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_SelectOrder_Path),
				"'Select Order button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_EditStatus_Path),
				"'Edit Status button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_OrderSearchbtn_Path),
				"'Order Search button' is not displayed");
		softAssert.assertAll();

		Click(Xpath, Pimco_MO_ItemsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Items_Nextbtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Productcode1_Path).equalsIgnoreCase(QA_Multifunction),
				QA_Multifunction + "  not displays under 'Product Code' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Title1_Path).equalsIgnoreCase(QA_Multifunction + "_Title"),
				QA_Multifunction + "_Title not displays under 'Title' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Qty1_Path).equalsIgnoreCase("1"),
				"'1' not displays under 'Qty' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Shipped1_Path).equalsIgnoreCase("0"),
				"'0' not displays under 'Shipped' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Backorder1_Path).equalsIgnoreCase("0"),
				"'0' not displays under 'Backorder' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Chargeback1_Path).equalsIgnoreCase("$0.00"),
				"'$0.00' not displays under 'Chargeback' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Print1_Path).equalsIgnoreCase("$0.01"),
				"'$0.00' not displays under 'Print' column ");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Fulfillment1_Path).equalsIgnoreCase("$0.53"),
				" '$0.53' not displays under 'Fulfillment' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Total1_Path).equalsIgnoreCase("$0.54"),
				"'$0.54' not displays under 'Total' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Items_Proof_Path),
				"'Proof' column is not displays in the items tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Items_Updatebtn_Path),
				"'Proof' column is not displays in the items tab");
		softAssert.assertTrue(
				Get_Attribute(Xpath, Pimco_MO_Items_Costcenterfield_Path, "value").equalsIgnoreCase("9999"),
				"'9999' not displays in 'Billing Cost Center' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Items_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Items_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side");
		softAssert.assertAll();

		Click(Xpath, Pimco_MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Shipping_Nextbtn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "QanoFirm IL")),
				"User Name is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "913 Commerce Ct")),
				"Address is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "Buffalo Grove")),
				"City is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "IL")),
				"State is not displayed under Address column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "1234657899")),
				"Phone no is not displayed under Address column");
		softAssert.assertTrue(
				Get_Text(Xpath, Pimco_MO_Shipping_RecipientNotes1_Path).equalsIgnoreCase("This is a QA piece"),
				"This is a QA piece is not displayed under Recipient Notes column ");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_Shipmethod1_Path).equalsIgnoreCase("UPS Ground"),
				"This is a QA piece is not displayed under Recipient Notes column ");
		softAssert.assertTrue(
				Get_Text(Xpath, Pimco_MO_Shipping_RecipientNotes1_Path).equalsIgnoreCase("This is a QA piece"),
				"UPS Ground is not displayed under Ship method column ");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_ShippingInformation1_Path).contains("Charges:$0.00"),
				"Charges:$0.00 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_ShippingInformation1_Path).contains("Weight:0"),
				"Weight:0 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_ShippingInformation1_Path).contains("Boxes:$0.00"),
				"Boxes:$0.00 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_ShippingInformation1_Path).contains("Skids:$0.00"),
				"Skids:$0.00 is not displayed under Shipping information column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_TrackingNumber1_Path).trim().equalsIgnoreCase(""),
				"'   ' not displays under 'Tracking Number' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Shipping_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side - Shipping tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Shipping_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the right side - Shipping tab");
		softAssert.assertAll();

		Click(Xpath, Pimco_MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Ticketno1_Path).trim().equalsIgnoreCase(OrderNumber),
				"(Order Number) not displays under 'Ticket Number' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Firstname1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'First Name' column ");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_lastname1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'Last Name' column ");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Company1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'Company' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Address11_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'Address 1' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_City1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'City' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_State1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'State/Province' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Zip1_Path).trim().equalsIgnoreCase(""),
				" ' ' not displays under 'Zip/Postal Code' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Phone1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'Phone' column");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Maillist_Trackeingno1_Path).trim().equalsIgnoreCase(""),
				"' ' not displays under 'Tracking Number' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Maillist_Nextbtn_Path),
				"'Next' button is not displays at the end of the page on the right side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Maillist_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");
		Click(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		softAssert.assertAll();

		Click(Xpath, Pimco_MO_AttachmentTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Attachment_Savebtn_Path);
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed - Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Type_Path),
				"'Attachment Type' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Createuser_Path),
				"'Create user' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Createdate_Path),
				"'Create Date' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Linktofile_Path),
				"'Link to File' column is not displays in the Attachment tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Refreshicon_Path),
				" 'Refresh' link is not displays on the right side of the page in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_AddAttachicon_Path),
				"'Add Attachment is not ' link displays in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Savebtn_Path),
				"'Save' button is not displays below 'Refresh' button in the Attachments Tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Attachment_Backbtn_Path),
				"'Back' button is not displays at the end of the page on the left side in the Attachments Tab");
		softAssert.assertAll();

	}

	@Test(priority = 5, enabled = false)
	public void Pimco_TC_2_4_10_1_5() throws InterruptedException {

		// Validate that select order copy functionality works appropriately

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, Pimco_MO_Copybtn_Path);
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not dispalyed in the Shopping cart page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not dispalyed in the Shopping cart page");
		Click(Xpath, Pimco_SCP_Remove_btn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Item(s) have been removed from your cart.")),
				"Item(s) have been removed from your cart. is not dispalyed in the Shopping cart page");

	}

	@Test(priority = 7, enabled = false)
	public void Pimco_TC_2_4_10_1_6() throws InterruptedException {

		// Create Order Template

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_SelectOrderfield_Path, OrderNumber);
		Wait_ajax();
		Click(Xpath, Textpath("li", OrderNumber));
		Click(Xpath, Pimco_MO_Copybtn_Path);
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not dispalyed in the Shopping cart page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				QA_Multifunction + " is not dispalyed in the Shopping cart page");
		Click(Xpath, Pimco_SCP_CreateOrderTemplate_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCP_COT_Title_Path),
				"'Create Order Template' button at the end of the page");
		Type(Xpath, Pimco_SCP_COT_Name_Path, "QA Template Test");
		Type(Xpath, Pimco_SCP_COT_Description_Path, "QA Test");
		Click(Xpath, Pimco_SCP_COT_Createbtn_Path);
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("div", "QA Template Test order template has been successfully created.")),
				"'QA Template Test order template has been successfully created. is not displayed");
		Click(Xpath, Pimco_LP_Home_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Template Test"),
				"QA Template Test is not displayed in the Order template widget");

	}

	@Test(priority = 8, enabled = true)
	public void Pimco_TC_2_4_10_1_7() throws InterruptedException {

		// Validate Collaboration works appropriately

		NavigateMenu(LP_Admin_path, LP_Support_path);
		Click(Xpath, Pimco_ST_VT_Addticketicon_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_ST_CST_Categorydd_Path, "Order Requests");
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, Pimco_ST_CST_SubCategorydd_Path, "Incorrect Order Received");
		Type(Xpath, Pimco_ST_CST_Ordernofield_Path, OrderNumber);
		Click(Xpath, Pimco_ST_CST_Createbtn_Path);
		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Click(Xpath, Pimco_MO_SearchresultsCollaboration(OrderNumber));
		Click(Xpath, Pimco_STM_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_STM_Createcomment_Path);
		Type(Xpath, Pimco_STM_Commenttext_Path, "This is Test");
		Click(Xpath, Pimco_STM_Createcomment_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_ST_Loading_Path);
		Click(Xpath, Pimco_STM_UpdateTicketbtn_Path);
		ExplicitWait_Element_Visible(Xpath, Pimco_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

	}

	@Test(priority = 6, enabled = false)
	public void Pimco_TC_2_4_10_1_8() throws InterruptedException {

		// Verify that you can select an order and works appropriately

		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Ordernofield_Path, OrderNumber);
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Gen_OrderSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_GeneralTab_Path), "General Tab is not displayed");

		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_ManageOrderNo_Path).equalsIgnoreCase(OrderNumber),
				"'Manage order No' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_Statustitle_Path), "'Status' is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_Gen_Status_Path).equalsIgnoreCase("NEW"),
				"'Status New' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_SelectOrder_Path),
				"'Select Order button' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Gen_EditStatus_Path),
				"'Edit Status button' is not displayed");
		softAssert.assertAll();

		Click(Xpath, Pimco_MO_ItemsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Items_Nextbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_Items_Productcode1_Path).equalsIgnoreCase(QA_Multifunction),
				QA_Multifunction + "  not displays under 'Product Code' column");

		Click(Xpath, Pimco_MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Shipping_Nextbtn_Path);
		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_Shipping_Shipmethod1_Path).equalsIgnoreCase("UPS Ground"),
				"This is a QA piece is not displayed under Recipient Notes column ");

		Click(Xpath, Pimco_MO_Maillisttab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Maillist_Maillistfilebtn_Path),
				"'Mail list file Production button is not displays at the end of the page on the left side in the Mail List Recipients Tab");

		Click(Xpath, Pimco_MO_AttachmentTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Attachment_Savebtn_Path);
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "There are no attachments associated with this order.")),
				"There are no attachments associated with this order. not displayed");
	}

	@Test(priority = 9, enabled = false)
	public void Pimco_TC_2_4_10_1_9() throws InterruptedException, IOException {

		// Verify "Tracking link" takes user directly to FedEx site

		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Click(Xpath, Inventory_Edit_Btn_Path);
		Type(Xpath, Inventory_ShippingInst_btn_Path, "QA TEST PLEASE DO NOT SHIP");
		Assert.assertTrue(Get_DropDown(Xpath, Inventory_Shipper_btn_Path).equalsIgnoreCase("UPS Ground"),
				"UPS Ground not displayed in the shipper");
		Type(Xpath, Inventory_Trackingno_btn_Path, "1ZTest");
		Type(Xpath, Inventory_Shippingcharges_btn_Path, "0.00");
		Click(Xpath, Inventory_ShipNote_btn_Path);
		Assert.assertTrue(Alert_Text().trim().equalsIgnoreCase("Are you sure you want to ship note this order?"),
				"Are you sure you want to ship note this order? alert not displayed");
		Accept_Alert();
		Assert.assertTrue(Get_Attribute(Xpath, Inventory_Order_Status, "value").equalsIgnoreCase("CLOSED"),
				"CLOSED is not displayed in the Status field");
		Click(Xpath, Inventory_logout_btn_Path);
		Get_URL(URL);
		login(PIMCOBoth1Username, PIMCOBoth1Password);
		ManageOrdersNav();
		Type(Xpath, Pimco_MO_Trackingnofield_Path, "1ZTest");
		Click(Xpath, Pimco_MO_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MO_SR_Loadingpanel_Path);
		Click(Xpath, MO_SearchresultsSelect(OrderNumber));
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Gen_OrderSearchbtn_Path);
		Click(Xpath, Pimco_MO_ShippingTab_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MO_Shipping_Nextbtn_Path);
		Click(Xpath, Textpath("a", "1ZTest"));
		Assert.assertTrue(Get_Title().contains("UPS"), "'UPS' page not displays");
		NavigateBack();

	}

	@Test(priority = 1, enabled = false)
	public void Pimco_TC_2_4_10_1_1() throws InterruptedException, IOException {

		// Ensure Select order, search order, and search results grids appear
		// appropriately

		ManageOrdersNav();

		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_SelectOrdertitle_Path).trim().contains("Select Order"),
				"'Select Order' section is not displays in header bar");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_SelectOrderfield_Path),
				"'Select Order' field is not displays in 'Select Order' section ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Viewbtn_Path), "'View' button is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Copybtn_Path), "'Copy' button is not displays");

		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_Searchordertitle_Path).trim().contains("Search Order"),
				"'Search Order' section is not displays in header bar");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Ordernofield_Path),
				"'Order Number' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Trackingnofield_Path),
				"'Tracking No' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Typedropdown_Path),
				"'Type' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Userfield_Path),
				"'User' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Shipperfield_Path),
				"'Shipper' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Statusdrop_Path),
				"'Status' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Startdatefield_Path),
				"'Start Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Startdatecalender_Path),
				"'Start Date' calender icon is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Enddatefield_Path),
				"'End Date' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Enddatecalender_Path),
				"'End Date' calender icon is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_RecipientName_Path),
				"'Recipient' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Addressfield_Path),
				"'Address' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Cityfield_Path),
				"'City' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Statefield_Path),
				"'State' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Zipfield_Path),
				"'Zip' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_UserGroup_Path),
				"'UserGroup' field is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Searchbtn_Path),
				"'Search' button is not displays in 'Search order' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MO_Clearbtn_Path),
				"'Clear' button is not displays in 'Search order' section");

		Assert.assertTrue(Get_Text(Xpath, Pimco_MO_SearchResultstitle_Path).trim().contains("Search Results"),
				"'Search Results' section is not displays in header bar");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderNo_Path).trim().equalsIgnoreCase("Order#"),
				" 'Order #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Orderedby_Path).trim().equalsIgnoreCase("Ordered by"),
				" 'Ordered by' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Trackingno_Path).trim().equalsIgnoreCase("Tracking#"),
				" 'Tracking #' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Orderdate_Path).trim().equalsIgnoreCase("Order Date"),
				" 'Order Date' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Shipper_Path).trim().equalsIgnoreCase("Shipper"),
				" 'Shipper' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Status_Path).trim().equalsIgnoreCase("Status"),
				" 'Status' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_Recipient_Path).trim().equalsIgnoreCase("Recipient"),
				" 'Recipient' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_State_Path).trim().equalsIgnoreCase("State/Province"),
				" 'State/Province' column is not displays in 'Search Results' grid");
		softAssert.assertTrue(Get_Text(Xpath, Pimco_MO_SR_OrderType_Path).trim().equalsIgnoreCase("OrderType"),
				" 'OrderType' column is not displays in 'Search Results' grid");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBoth1Username, PIMCOBoth1Password);
		Clearcarts();
		Pre_Req();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}