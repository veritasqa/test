package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class ManagePhrasesTest extends BaseTestPimco {

	
	//Verify Search Grid is displayed with all appropriate fields
	@Test(priority =1)
	public void PIMCO_TC_2_4_11_1_1() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Phrases"));
		
		ExplicitWait_Element_Visible(Xpath, Textpath("h1", "Manage Phrases"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Name")),"Name Coloumn is missing in manage phrases page ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Default Phrase")),"Default Phrase Coloumn is missing in manage phrases page ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Custom Phrase")),"Custom Phrase Coloumn is missing in manage phrases page ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Custom Active")),"Custom Active Coloumn is missing in manage phrases page ");
		
		
		
	}
	
	//Validate the filter functionality
	@Test(priority =2)
	public void PIMCO_TC_2_4_11_1_2() throws InterruptedException {
		
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Phrases"));
		
		ExplicitWait_Element_Visible(Xpath, Textpath("h1", "Manage Phrases"));
		
		Type(ID, Pimco_MP_Name_TextBox_ID, "Bad User Name");
		Click(ID, Pimco_MP_Name_Filter_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "Contains"));
		Wait_ajax();
		
        Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("td", "Login Bad User Name and/or Password")),"Login Bad User Name and/or Password is missing");
    
    	Click(ID, Pimco_MP_Name_Filter_ID);
		Wait_ajax();
        Click(Xpath, Textpath("span", "NoFilter"));
      	Wait_ajax();
		
      	
      	
		Type(ID, Pimco_MP_Defaultphrase_TextBox_ID, "Bad User Name");
		Click(ID, Pimco_MP_Defaultphrase_Filter_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "Contains"));
		Wait_ajax();
		
        Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("td", "Login Bad User Name and/or Password")),"Login Bad User Name and/or Password is missing");
    
    	Click(ID, Pimco_MP_Defaultphrase_Filter_ID);
		Wait_ajax();
        Click(Xpath, Textpath("span", "NoFilter"));
      	Wait_ajax();
      	
      	
       	
		Type(ID, Pimco_MP_CustomPhrase_TextBox_ID, "Invalid user name or password, please try again.");
		Click(ID, Pimco_MP_CustomPhrase_Filter_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "Contains"));
		Wait_ajax();

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("td", "Invalid user name or password, please try again.")),
				"Invalid user name or password, please try again.-- is missing");

		Click(ID, Pimco_MP_CustomPhrase_Filter_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "NoFilter"));
		Wait_ajax();
		
		

		Click(ID, Pimco_MP_Customactive_Checkboc_ID);
		Click(ID, Pimco_MP_Customactive_Filter_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "EqualTo"));
		Wait_ajax();
		
        Assert.assertTrue(Element_Is_selected(Xpath, Pimco_MP_Customactive_CheckBoxFirstpiece_path),"Checkbox is not checked in the first piece");
    
    	Click(ID, Pimco_MP_Customactive_Filter_ID);
		Wait_ajax();
        Click(Xpath, Textpath("span", "NoFilter"));
      	Wait_ajax();		

	}
	
	//Validate page and page size functionality works appropriately
	@Test(priority =2)
	public void PIMCO_TC_2_4_11_1_3() throws InterruptedException{
		
		
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Phrases"));
		
		ExplicitWait_Element_Visible(Xpath, Textpath("h1", "Manage Phrases"));
		
		
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("1")),"Page 1 - not hightligted");
	    
	    Click(Xpath, Textpath("span", "2"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("2")),"Page 2 - not hightligted");

	    Click(Xpath, "//input[@class='rgPageNext']");
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("3")),"Page 3 - not hightligted");

	    Click(Xpath, "//input[@class='rgPagePrev']");
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("2")),"Page 2 - not hightligted");

	    
		Type(ID, Pimco_MP_Pagenumber_ID, "1");
		Click(ID, Pimco_MP_GoBtn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("1")),"Page 1 - not hightligted");

		Type(ID, Pimco_MP_Pagenumber_ID, "2");
		Click(ID, Pimco_MP_GoBtn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("2")),"Page 2 - not hightligted");


	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
	}
}
