package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulFillmentClearCartOrder extends BaseTestPimco{
	
	//Validate part is removed on clicking Clear Cart button
	
	@Test(enabled = true , priority = 1)
	public void Pimco_TC_2_8_1_3_1() throws InterruptedException{
		Clearcarts();

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
		
		
	    Click(Xpath, Textpath("span", "Clear Cart"));
    	Wait_ajax();
    	Thread.sleep(3000);
		
	}
	
	
	//Validate that in the mini shopping cart if you change Qty to 0 and click update the part is removed
	
	@Test(enabled = true , priority = 2)
	public void Pimco_TC_2_8_1_3_2ANDPimco_TC_2_8_1_3_3() throws InterruptedException{
		Clearcarts();

		
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		
	    Type(Xpath, MiniCartQtyTextfieldPath(QA_Multifunction), "0");
	    Click(ID, Pimco_SR_MSC_Update_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "QA_Multifunction has been removed from your cart")),"QA_Multifunction has been removed from your cart- Message not displayed");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");
	    
		
		//Pimco.TC.2.8.1.3.3
		
		//Validate on the check out page that the remove button removes part from cart
		
		
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));

	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID,Pimco_SCP_Remove_Btn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div","Item(s) have been removed from your cart.")),"Item(s) have been removed from your cart.- Message not found");
	
	    Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");

	}
	
	//Validate when you click the clear cart button on the shopping cart page the part is removed
	
	@Test(enabled = true , priority = 3)
	public void Pimco_TC_2_8_1_3_4() throws InterruptedException{
		Clearcarts();
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    
	    Click(Xpath, Textpath("span", "Clear Cart"));
    	Wait_ajax();
    	Thread.sleep(3000);
	}
	
	@BeforeMethod
	public void ClearCart() throws InterruptedException   {
	//	Clearcarts();
	}

	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}

}
