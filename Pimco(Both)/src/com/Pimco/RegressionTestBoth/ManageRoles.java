package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageRoles extends BaseTestPimco {

	public String SecurityRole(String Role) {

		return ".//div[contains(@id,'cphContent_rtvSecurityPanel')]//span[text()='" + Role + "']";

	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_4_12_1_1() throws InterruptedException {

		/*
		 * Validate that the page opens with the appropriate sections
		 * */

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MR_Securitytab_Path), " 'Security' tab is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MR_Otherstab_Path), " 'Others' tab is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SecurityRole("Admin")),
				"'Admin' role is displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SecurityRole("Basic User")),
				"'Basic User' role is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SecurityRole("Order Approval Admin")),
				"'Order Approval Admin' role is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SecurityRole("Special Projects")),
				"'Special Project' role is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SecurityRole("Super Admin")),
				"'Super Admin' role is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SecurityRole("Veritas Admin")),
				"'Veritas Admin' role is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", "To edit a role, please click on it,")),
				"To edit a role, please click on it, is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("p", "to add a new, please click below")),
				"to add a new, please click below is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, Pimco_MR_CreateSecurityRole_Btn_ID),
				" 'Create Security Role' button is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("h2", "Permissions")),
				" 'Permissions' section is not displayed under 'Security' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Members")),
				" 'Members' list is not displayed under 'Security' tab");
		softAssert.assertAll();
		Click(Xpath, Pimco_MR_Otherstab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "CA")), " 'CA' section is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "US")), " 'US' section is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, Pimco_MR_CreateRole_Btn_ID),
				" 'Create Role' button is not displayed under 'Others' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MR_Otherstab_Members_Path),
				" 'Members' list is not displayed under 'Others' tab");
		Select_DropDown_VisibleText(ID, Pimco_MR_AudienceDropDown_ID, "InternalOnly");
		Select_DropDown_VisibleText(ID, Pimco_MR_AudienceDropDown_ID, "Audience");
		Select_DropDown_VisibleText(ID, Pimco_MR_AudienceDropDown_ID, "BusinessOwner");
		Select_DropDown_VisibleText(ID, Pimco_MR_AudienceDropDown_ID, "Firms");
		Select_DropDown_VisibleText(ID, Pimco_MR_AudienceDropDown_ID, "Office Code");
		Select_DropDown_VisibleText(ID, Pimco_MR_AudienceDropDown_ID, "Selling Agreement");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		NavigateMenu(LP_Admin_path, LP_ManageRoles_path);
		ExplicitWait_Element_Clickable(ID, Pimco_MR_CreateSecurityRole_Btn_ID);

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

		Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
