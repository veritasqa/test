package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class MarketingBookBuilder extends BaseTestPimco {

	public void Click_BookContent_Expand(String ProductCode) throws InterruptedException {

		Click(Xpath, ".//div[contains(text(),'" + ProductCode + "')]/preceding::input[1]");
		Wait_ajax();
		Thread.sleep(3000);
	}

	public String BookContent_Expanded(String ProductCode, String Expanded_Content) throws InterruptedException {

		return ".//div[contains(text(),'" + ProductCode + "')]/following::tr//td[text()='" + Expanded_Content + "']";

	}

	public String Inventory_Authentication(String Inven_Username, String Inven_Password, String Orderno) {

		return "http://" + Inven_Username + ":" + Inven_Password
				+ "@ver-sqlnew.cgx.net/ReportServer/Pages/ReportViewer.aspx?/Veritas/Staging/PickTickets/PickTicketPIMCO&rs:Command=Render&OrderID="
				+ Orderno;

	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_6_1_6_1_1() throws InterruptedException, IOException {

		/*
		 * Verify Users are restricted from creating multiple books per order
		 * */
		Click(Xpath, Pimco_LP_StartaMailing_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, Pimco_TC_2_6_1_6_1_1_File);
		Click(ID, Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, PBB001_40827);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoDripBtnXpath(PBB001_40827));
		Thread.sleep(3000);
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, PBB002_41553);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoDripBtnXpath(PBB002_41553));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Maximum of 1 Book Builder item per Order")),
				"Maximum of 1 Book Builder item per Order is not displayed");
	}

	@Test(priority = 2, enabled = true)
	public void Pimco_TC_2_6_1_6_1_2andPimco_TC_2_6_1_6_1_3andPimco_TC_2_6_1_6_1_4()
			throws InterruptedException, IOException {

		/*
		 * Verify Remove button and Drag and Drop functionality
		 * */

		Click(Xpath, Pimco_LP_StartaMailing_path);
		Click(ID, Pimco_MLU_Select_File_ID);
		Uploadfile(Upload_AutoIT_exe, Pimco_TC_2_6_1_6_1_1_File);
		Click(ID, Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, PBB001_40827);
		Wait_ajax();
		Click(Xpath, Pimco_SR_Search_Btn_Path);
		Click(Xpath, SearchResultsAddtoDripBtnXpath(PBB001_40827));
		Wait_ajax();
		Click(ID, Pimco_SR_CheckOut_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCO_Title_Path),
				" 'Set Customizable Options' page not displays");
		Type(Xpath, Pimco_SCO_ClientName_Path, "Pimco");
		Type(Xpath, Pimco_SCO_DayMonthYear_Path, Get_Todaydate("M/d/YYYY"));
		Type(Xpath, Pimco_SCO_FirmName_Path, "Firmtest");
		Click(Xpath, Pimco_SCO_TOCYes_ck_Path);
		Wait_ajax();
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_testfactsheet_01);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(QA_testfactsheet_01),
				" 'QA_Testfactsheet_01' not displays under 'Book Content' section");
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");
		Click(Xpath, Pimco_SCP_EditVariables_btn_Path);
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QA_testfactsheet_02);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode2_Path).trim().equalsIgnoreCase(QA_testfactsheet_02),
				" 'QA_Testfactsheet_02' not displays under 'Book Content' section");
		DragandDrop(Xpath, Pimco_SCO_BookContent_Row2_Path, Xpath, Pimco_SCO_BookContent_Row1_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(QA_testfactsheet_02),
				" 'QA_Testfactsheet_02' not displays under 'Book Content' section - First row");
		Click(Xpath, Pimco_SCO_BookContent_Remove1_Path);
		Wait_ajax();
		Assert.assertFalse(
				Get_Text(Xpath, Pimco_SCO_BookContent_Productcode1_Path).trim().equalsIgnoreCase(QA_testfactsheet_02),
				" 'qa_testfundcard_02'is displays under 'Book Content' section after removed");

		// Pimco_TC_2_6_1_6_1_3 Starts

		/*
		 * Validate the Page Count and ability to successfully generate the Proof with Large Book content
		 * */

		Assert.assertTrue(Get_Text(Xpath, Pimco_SCO_MaximumPageCount_Path).trim().equalsIgnoreCase("64"),
				" '64' is not displayed for 'Maximum Page Count' field");
		Type(Xpath, Pimco_SCO_KitLookup_Path, QA_BookbuilderKit1);
		Click(Xpath, Textpath("td", QA_BookbuilderKit1 + " "));
		Click(Xpath, Pimco_SCO_KitLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("span", "9 kit components available for use as book content")),
				"9 kit components available for use as book content is not displayed");
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, "QA_TestSupp_01");
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				PBB001_40827 + " not displayed in the checkout page");
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCP_Proofed_btn_Path),
				"Proofed button not displayed in the checkout page");

		// Pimco_TC_2_6_1_6_1_4 Starts

		Click(Xpath, Pimco_SCP_EditVariables_btn_Path);
		Type(Xpath, Pimco_SCO_InventoryLookup_Path, QATest_FundCard1);
		Wait_ajax();
		Click(Xpath, Pimco_SCO_InventoryLookup_Addtobook_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "It exceed the Maximum allowed Pages")),
				"It exceed the Maximum allowed Pages is not displayed");
		Assert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("div", QATest_FundCard1)),
				QATest_FundCard1 + " is not displayed in 'Book Content' section");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_testfactsheet_01)),
				QA_testfactsheet_01 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_testfactsheet_02)),
				QA_testfactsheet_02 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestFundcard_03)),
				QA_TestFundcard_03 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_testfactsheet_05)),
				QA_testfactsheet_05 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestFundcard_01)),
				QA_TestFundcard_01 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", qa_testfundcard_02)),
				qa_testfundcard_02 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestFundCard_05)),
				QA_TestFundCard_05 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_Testsupp_04)),
				QA_Testsupp_04 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_Replace_Test_2)),
				QA_Replace_Test_2 + "  not displayed in the checkout page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_MarketingOnly)),
				QA_MarketingOnly + "  not displayed in the checkout page");
		Click_BookContent_Expand(QA_TestFundcard_03);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_TestFundcard_03, QA_Testsupp_02)),
				"QA_Testsupp_02 not displayed in the Product Code");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_TestFundcard_03, QA_Testsupp_03)),
				"QA_Testsupp_03 not displayed in the Product Code");
		Click_BookContent_Expand(QA_TestFundcard_03);
		Click_BookContent_Expand(QA_testfactsheet_05);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_testfactsheet_05, QA_Testsupp_03)),
				"QA_Testsupp_02 not displayed in the Product Code - " + QA_testfactsheet_05);
		Click_BookContent_Expand(QA_testfactsheet_05);
		Click_BookContent_Expand(qa_testfundcard_02);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(qa_testfundcard_02, QA_Testsupp_02)),
				"QA_Testsupp_03 not displayed in the Product Code");
		Click_BookContent_Expand(qa_testfundcard_02);
		Click_BookContent_Expand(QA_TestFundCard_05);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, BookContent_Expanded(QA_TestFundCard_05, QA_Testsupp_03)),
				"QA_Testsupp_03 not displayed in the Product Code");
		softAssert.assertAll();
		Click(Xpath, Pimco_SCO_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Proof Generation in Process")),
				"'Proof Generation in Process�' message box not displays");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Proof Generation in Process"));
		Click(Xpath, Pimco_SCP_Proof_btn_Path);
		Switch_New_Tab();
		Switch_Old_Tab();
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();

		Inventory_Login();
		Click(Xpath, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		Type(Xpath, Inventory_Ordernumber_Path, OrderNumber);
		Click(Xpath, Inventory_Search_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"Order no is not displayed in the inventory page");
		Click(Xpath, Containstextpath("a", "Pick"));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains("OrderID=" + OrderNumber), "Pick is not opened in the new window");
		Get_URL(Inventory_Authentication("Yannamalai", "Sachin05", "1767808"));
		ExplicitWait_Element_Not_Visible(Xpath, Containspath("span", "class", "WaitText"));
		Assert.assertTrue(Get_Title().trim().equalsIgnoreCase("PickTicketPIMCO - Report Viewer"),
				"PickTicketPIMCO - Report Viewer is not opened in the new window");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", PBB001_40827)),
				PBB001_40827 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_testfactsheet_01)),
				QA_testfactsheet_01 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_testfactsheet_02)),
				QA_testfactsheet_02 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestFundcard_03)),
				QA_TestFundcard_03 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestFundcard_01)),
				QA_TestFundcard_01 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_testfactsheet_05)),
				QA_testfactsheet_05 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", qa_testfundcard_02)),
				qa_testfundcard_02 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestFundCard_05)),
				QA_TestFundCard_05 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_Testsupp_04)),
				QA_Testsupp_04 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_Replace_Test_2)),
				QA_Replace_Test_2 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_MarketingOnly)),
				QA_MarketingOnly + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_TestSupp_01)),
				QA_TestSupp_01 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_Testsupp_02)),
				QA_Testsupp_02 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", QA_Testsupp_03)),
				QA_Testsupp_03 + " is not displayed in the PickTicketPIMCO - Report Viewer page");
		softAssert.assertAll();
		Switch_Old_Tab();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBoth1Username, PIMCOBoth1Password);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Clearcarts();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
