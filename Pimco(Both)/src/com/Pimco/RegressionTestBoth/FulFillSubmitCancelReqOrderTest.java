package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulFillSubmitCancelReqOrderTest extends BaseTestPimco{

	
	@Test(enabled = true)
	public void Pimco_TC_2_8_1_2_1ANDPimco_TC_2_8_1_2_2ANDPimco_TC_2_8_1_2_3ANDPimco_TC_2_8_1_2_4() throws InterruptedException{
		
		//Validate an order is placed
		
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
			
		//Pimco.TC.2.8.1.2.2
		//Validate on confirmation page submit a cancel request and verify a support ticket is create
		
		Click(ID, Pimco_OC_SubmitCancelRequest_Btn);
		
		Thread.sleep(2000);
		Accept_Alert();
		
		Click(ID, Pimco_OC_Close_btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
		
		Hover(Xpath, Textpath("span","Admin"));
		Wait_ajax();
		
		Click(Xpath, Textpath("span","Support"));
		
	    ExplicitWait_Element_Clickable(ID, Pimco_SP_AddTicket_ID);

	    Click(Xpath, EditButtonOrderNumber(OrderNumber));
	    
	    Click(Xpath, Pimco_SP_AddComment_Path);
	    ExplicitWait_Element_Clickable(ID, Pimco_SP_CreateComment_Btn_ID);
	    Type(ID, Pimco_SP_Comment_TextBox_ID, "QA Test");
	    Click(ID, Pimco_SP_CreateComment_Btn_ID);
	    
	    Click(ID, Pimco_SP_Closed_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SP_TicketUpdateMsg_ID);

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_SP_TicketUpdateMsg_ID),"Ticket Succefully Updated Message NOT displays");

		Assert.assertTrue(Get_Attribute(ID, Pimco_SP_Closed_ID, "class").equalsIgnoreCase("statusBtnSelected"),"Closed is displayed under 'Status' column"); 
		
		Hover(Xpath, Textpath("span","Admin"));
		Wait_ajax();
		
		Click(Xpath, Textpath("span","Support"));
		
	    ExplicitWait_Element_Clickable(ID, Pimco_SP_AddTicket_ID);
		
	    Type(ID, Pimco_SP_Status_TextBox_ID, "Closed");
	    Click(ID, Pimco_SP_Status_Filter_ID);
	    
		ExplicitWait_Element_Visible(Xpath,Textpath("span", "Contains"));

	    Click(Xpath,Textpath("span", "Contains"));

	    
		Assert.assertTrue(Element_Is_Displayed(Xpath, EditButtonOrderNumber(OrderNumber)),"Ticket Not listed under Closed");

	    		
	}
	

	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}

}
