package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageUsers extends BaseTestPimco {

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[1]");
	}

	@Test(priority = 1, enabled = false)
	public void Pimco_TC_2_4_13_1_1() throws InterruptedException {

		/* Validate Search Grid is displayed with all options */

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Title_Path), "'Manage Users' page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_UserName_Path),
				"'Username' Field is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Firstname_Path),
				"'First name' Field is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Lastname_Path),
				"'Last name' Field is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Email_Path),
				"'Email' Field is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Activeck_Path),
				"'Actuive check' Field is not displayed  above the grid on 'Manage Users' page");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_UserNamefilter_Path),
				"'Username' Fitler icon is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Firstnamefilter_Path),
				"'First name' Fitler icon is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Lastnamefilter_Path),
				"'Last nam' Fitler icon is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Emailfilter_Path),
				"'Email' Fitler icon is not displayed  above the grid on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Activefilter_Path),
				"'Active' Fitler icon is not displayed  above the grid on 'Manage Users' page");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Edit1_Path),
				"Edit button is not displayed on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Adduser_Path),
				"Edit button is not displayed on 'Manage Users' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Refresh_Path),
				"Edit button is not displayed on 'Manage Users' page");

		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = false)
	public void Pimco_TC_2_4_13_1_2() throws InterruptedException {

		/*
		 * Validate that filtering functionality works appropriately
		 */

		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBothUsername);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBothUsername)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		Selectfilter(Pimco_MU_UserNamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

		Type(Xpath, Pimco_MU_Firstname_Path, Both_Fname);
		Click(Xpath, Pimco_MU_Firstnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Both_Fname)),
				"Entered <User FName> is not displayed under the 'User FName' column header");
		Selectfilter(Pimco_MU_Firstnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

		Type(Xpath, Pimco_MU_Lastname_Path, Both_Lname);
		Click(Xpath, Pimco_MU_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Both_Lname)),
				"Entered <User LName> is not displayed under the 'User LName' column header");
		Selectfilter(Pimco_MU_Lastnamefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

		Type(Xpath, Pimco_MU_Email_Path, Email);
		Click(Xpath, Pimco_MU_Emailfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Email)),
				"Entered <Email> is not displayed under the 'Email' column header");
		Selectfilter(Pimco_MU_Emailfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

		Click(Xpath, Pimco_MU_Activefilter_Path);
		Selectfilter(Pimco_MU_Emailfilter_Path, "EqualTo");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_MU_Activeck1_Path),
				"Checked 'Active' checkboxes  are not displayed on 'Manage Users' page");
		Selectfilter(Pimco_MU_Activefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
	}

	@Test(priority = 3, enabled = false)
	public void Pimco_TC_2_4_13_1_3() throws InterruptedException {

		/*
		 * Validate Edit option opens on Search Result Row and allows you to Edit the user
		 */

		Type(Xpath, Pimco_MU_UserName_Path, PIMCOBothUsername);
		Click(Xpath, Pimco_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", PIMCOBothUsername)),
				"entered <User Name> is not displayed under the 'User Name' column header");
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Wait_ajax();
		Clickedituser(PIMCOBothUsername);
		Type(Xpath, Pimco_MU_Add_Address_Path, "913 Commerce Court-123");
		Click(Xpath, Pimco_MU_Edit_Updatebtn_Path);

		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Clickedituser(PIMCOBothUsername);
		Assert.assertTrue(
				Get_Attribute(Xpath, Pimco_MU_Add_Address_Path, "value").equalsIgnoreCase("913 Commerce Court-123"),
				"'913 Commerce Court-123' is not displayed in 'Address' field");
		Type(Xpath, Pimco_MU_Add_Address_Path, "913 Commerce Court");
		Click(Xpath, Pimco_MU_Edit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

	}

	@Test(priority = 4, enabled = true)
	public void Pimco_TC_2_4_13_1_4() throws InterruptedException, IOException {

		/*
		 * Validate Add User link opens "Add User mode" and user is allowed to enter values 
		 */

		Click(Xpath, Pimco_MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Type(Xpath, Pimco_MU_Add_Username_Path, "QAUSER");
		Type(Xpath, Pimco_MU_Add_Userpwd_Path, "Password");
		Type(Xpath, Pimco_MU_Add_Firstname_Path, "qaauto");
		Type(Xpath, Pimco_MU_Add_Lastname_Path, "SI_TC_2_3_10_1_4");
		Type(Xpath, Pimco_MU_Add_Email_Path, Email);
		Type(Xpath, Pimco_MU_Add_Address_Path, Address);
		Type(Xpath, Pimco_MU_Add_City_Path, City);
		Select_DropDown_VisibleText(Xpath, Pimco_MU_Add_State_Path, "Illinois");
		Type(Xpath, Pimco_MU_Add_Zip_Path, Zip);
		Select_DropDown_VisibleText(Xpath, Pimco_MU_Add_Costcenter_Path, "9999 - Veritas Cost Center");
		softAssert.assertTrue(Element_Is_selected(Xpath, Pimco_MU_Add_Activeck_Path),
				" 'Active User' checkbox is not checked for 'Active' field");
		MoveandClick(Xpath, Pimco_MU_Add_ProfilePhoto_Path);
		Runtime.getRuntime()
				.exec(System.getProperty("user.dir")
						+ "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_4_13_1_4_ProfilePhoto.exe" + " "
						+ System.getProperty("user.dir")
						+ "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_4_13_1_4_ProfilePhoto.jpg");

		ExplicitWait_Element_Visible(Xpath, Pimco_MU_Add_ProfilePhoto_Remove_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Add_ProfilePhoto_Remove_Path),
				"Profile Picture Removed is not displayed");
		Click(Xpath, UsergroupCheckbox("US"));
		Click(Xpath, UsergroupCheckbox("CA"));
		Click(Xpath, SecurityCheckbox("Veritas Admin"));
		Click(Xpath, SecurityCheckbox("Admin"));
		Click(Xpath, SecurityCheckbox("Order Approval Admin"));
		Click(Xpath, SecurityCheckbox("Super Admin"));
		Click(Xpath, SecurityCheckbox("Special Project"));
		Click(Xpath, Pimco_MU_Add_Cancelbtn_Path);
		softAssert.assertAll();

	}

	@Test(priority = 5, enabled = false)
	public void Pimco_TC_2_4_13_1_5() throws InterruptedException, IOException {

		/*Verify on Manage Users page
		Parent Group- CA
		Sub user group:Canada Admin Team 
		Sub user group: Canada FAs 
		Sub user group:Canada Sales Team displays*/

		NavigatePage("2");
		Click(Xpath, Textpath("span", "2"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "page 2 is not displayed upon clicking page 2");
		Type(Xpath, Pimco_MU_Pagesize_Path, "1");
		Click(Xpath, Pimco_MU_Pagesizechangebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_MU_Row1_Path), "Search Results is not displayed");
		Assert.assertFalse(Element_Is_Displayed(Xpath, Pimco_MU_Row2_Path), "Only one result is not displayed");
		Type(Xpath, Pimco_MU_Pageno_Path, "2");
		Click(Xpath, Pimco_MU_Pagegobtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "page 2 is not displayed upon entered page 2");
		Click(Xpath, Pimco_MU_Nextpagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"page 3 is not displayed upon Clicking next page button");
		Click(Xpath, Pimco_MU_Fistpagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"page 1 is not displayed upon Clicking First page button");
		Click(Xpath, Pimco_MU_Lastpagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);

		String ManageUserslastpageno = Get_Text(Xpath, Pimco_MU_Currentpage_Path).trim();
		System.out.println(Get_Text(Xpath, Pimco_MU_Pageofno_Path).substring(
				Get_Text(Xpath, Pimco_MU_Pageofno_Path).indexOf(" ") + 1,
				Get_Text(Xpath, Pimco_MU_Pageofno_Path).length()));
		Assert.assertEquals(ManageUserslastpageno,
				Get_Text(Xpath, Pimco_MU_Pageofno_Path).substring(
						Get_Text(Xpath, Pimco_MU_Pageofno_Path).indexOf(" ") + 1,
						Get_Text(Xpath, Pimco_MU_Pageofno_Path).length()),
				"Last page is not highlighted");
		Click(Xpath, Pimco_MU_Previouspagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		Assert.assertNotEquals(Get_Text(Xpath, Pimco_MU_Currentpage_Path).trim(), ManageUserslastpageno,
				"(second last page) is not highlighted");

	}

	@Test(priority = 6, enabled = false)
	public void Pimco_TC_2_4_13_1_7() throws InterruptedException, IOException {

		/*Verify on Manage Users page
		Parent Group- CA
		Sub user group:Canada Admin Team 
		Sub user group: Canada FAs 
		Sub user group:Canada Sales Team displays*/

		Click(Xpath, Pimco_MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_MU_Loading_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, UsergroupCheckbox("CA")),
				"CA parent checkbox is not  displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, UsergroupCheckbox("Canada Admin Team")),
				"Canada Admin Team checkbox is not displayed under CA");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, UsergroupCheckbox("Canada FAs")),
				"Canada FAs Team checkbox is not displayed under CA");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, UsergroupCheckbox("Canada Sales Team")),
				"Canada Sales Team checkbox is not displayed under CA");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		System.out.println(System.getProperty("user.dir")
				+ "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_4_13_1_4_ProfilePhoto.jpg");
		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_MU_Adduserbtn_Path);

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		// Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
