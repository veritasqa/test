package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class AdminSupport extends BaseTestPimco {

	// Support : Veritas Tickets
	public static final String SVT = "VeritasSupportTickets";
	// Support : Client Tickets
	public static final String SCT = "ClientSupportTickets";

	public static String SubCategory;

	public static final String CurrentVeritaspage = ".//a[@class='rgCurrentPage' and contains(@href,'" + SVT
			+ "')]//span";
	public static final String CurrentClientpage = ".//a[@class='rgCurrentPage' and contains(@href,'" + SCT
			+ "')]//span";

	public static final String SI_TC_2_3_1_3_2_file = System.getProperty("user.dir")
			+ "\\src\\com\\Standard\\Utils\\PDF to upload.pdf";

	public String NavigatePage(String Pageno, String Ticketname) {

		return ".//a[contains(@href,'" + Ticketname + "')]//span[text()='" + Pageno + "']";
	}

	public String CurrentPage(String Pageno, String Ticketname) {

		return ".//a[@class='rgCurrentPage' and contains(@href,'" + Ticketname + "')]//span[text()='" + Pageno + "']";
	}

	public void Createveritasticket() throws InterruptedException {

		NavigateMenu(LP_Admin_path, LP_Support_path);
		Click(Xpath, Pimco_ST_VT_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_ST_CST_Createbtn_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_ST_CST_Categorydd_Path, "Other");
		Wait_ajax();
		Type(Xpath, Pimco_ST_CST_Commentfield_Path, "Test");
		Select_DropDown_VisibleText(Xpath, Pimco_ST_CST_SubCategorydd_Path, "Other");
		Click(Xpath, Pimco_ST_CST_Createbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_STM_UpdateTicketbtn_Path);

		Supporttickenumber = Get_Text(Xpath, Pimco_STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		System.out.println("Created Support ticket # is " + Supporttickenumber);
		Reporter.log(Supporttickenumber);

	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_4_2_1_1() throws InterruptedException {

		// Verify support page opens with Veritas/Client tickets search grid

		NavigateMenu(LP_Admin_path, LP_Support_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_ST_CT_Addticketicon_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Veritasticketitle_Path),
				"Support : Veritas Tickets' grid is not displayed on the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_TicketNotxt_Path),
				" 'Ticket #' column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_OrderNotxt_Path),
				" 'order #'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_SubCattxt_Path),
				" 'Sub category'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Productcodetxt_Path),
				" 'Product code'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Submittedbytxt_Path),
				" 'Submitted by'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Updatedontxt_Path),
				" 'Updated on'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Teamtxt_Path), " 'Team'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Statustxt_Path), " 'Status'column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_Clientticketitle_Path),
				"Support : Client Tickets' grid is not displayed on the page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_TicketNotxt_Path),
				" 'Ticket #' column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_OrderNotxt_Path),
				" 'order #'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_SubCattxt_Path),
				" 'Sub category'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_ProductCodetxt_Path),
				" 'Product code'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_Submittedbytxt_Path),
				" 'Submitted by'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_Updatedontxt_Path),
				" 'Updated on'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_Teamtxt_Path),
				" 'Team'column is not displayed in CT");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_CT_Statustxt_Path),
				" 'Status'column is not displayed in CT");
		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = true)
	public void Pimco_TC_2_4_2_1_2() throws InterruptedException {

		// Support Page- paging and page size functionality

		NavigateMenu(LP_Admin_path, LP_Support_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_ST_CT_Addticketicon_Path);
		Type(Xpath, Pimco_ST_Updatedontxt_Path, Get_Todaydate("M/d/YYYY"));
		Selectfilter(Pimco_ST_Updatedonfilter_Path, "LessThan");
		Assert.assertEquals(Get_Attribute(Xpath, Pimco_ST_VT_Pagenotxt_Path, "value"), "1",
				"'1' not displays in 'Page' field");
		Assert.assertEquals(Get_Attribute(Xpath, Pimco_ST_VT_Pagesizetxt_Path, "value"), "5",
				"'5' not displays in 'Page size' field");
		Assert.assertTrue(Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).contains("of "),
				"(Item 1 to 5 of xxx) not displays on the  right side end of the grid in 'Support : Veritas Tickets' section");
		Type(Xpath, Pimco_ST_VT_Pagesizetxt_Path, "3");
		Click(Xpath, Pimco_ST_VT_Changtbn_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_ST_VT_Itemto_Path).contains("Item 1 to 3"),
				"'Item 1 to 3' not displays at the end on right side of the grid in 'Support : Veritas Tickets' section");
		Type(Xpath, Pimco_ST_VT_Pagesizetxt_Path, "5");
		Click(Xpath, Pimco_ST_VT_Changtbn_Path);
		Wait_ajax();
		Click(Xpath, NavigatePage("2", SVT));
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_ST_VT_Itemto_Path).contains("Item 6 to 10"),
				"'Item 6 to 10' not displays at the end on right side of the grid in 'Support : Veritas Tickets' section");
		Type(Xpath, Pimco_ST_VT_Pagenotxt_Path, "3");
		Click(Xpath, Pimco_ST_VT_Gobtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("3", SVT)),
				"Page '3' is not highlighted - Veritas ticket");
		Click(Xpath, Pimco_ST_VT_Nextpage_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("4", SVT)),
				"Page '4' is not highlighted - Veritas ticket");
		Click(Xpath, Pimco_ST_VT_Lastpage_Path);
		Wait_ajax();
		String Veritaslastpageno = Get_Text(Xpath, CurrentVeritaspage).trim();
		System.out.println(Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).substring(
				Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).length() - 2,
				Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).length()));
		Assert.assertEquals(Veritaslastpageno,
				Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).substring(
						Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).indexOf(" ") + 1,
						Get_Text(Xpath, Pimco_ST_VT_Pagesizeof_Path).length()),
				"Last page is not displayed VT");
		Click(Xpath, Pimco_ST_VT_Prevpage_Path);
		Assert.assertNotEquals(Get_Text(Xpath, CurrentVeritaspage).trim(), Veritaslastpageno,
				"<Second Last Page> is not highlighted");
		Click(Xpath, Pimco_ST_VT_Firstpage_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, CurrentPage("1", SVT)),
				"Page '1' is not highlighted - Veritas ticket");

	}

	@Test(priority = 3, enabled = true)
	public void Pimco_TC_2_4_2_1_4() throws InterruptedException {

		// Validate Filter functionality works for all the column heading

		NavigateMenu(LP_Admin_path, LP_Support_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_ST_CT_Addticketicon_Path);
		Supporttickenumber = Get_Text(Xpath, Pimco_ST_VT_TicketNo1_Path).trim();
		Type(Xpath, Pimco_ST_TicketNotxt_Path, Supporttickenumber);
		Click(Xpath, Pimco_ST_SubCatfilter_Path);
		Wait_ajax();
		Assert.assertEquals(Supporttickenumber, Get_Text(Xpath, Pimco_ST_VT_TicketNo1_Path),
				"the search result for the <Ticket #>  entered is not displayed under the  'Ticket #' column - Veritas ticket");
		Clear(Xpath, Pimco_ST_TicketNotxt_Path);
		Click(Xpath, Pimco_ST_SubCatfilter_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_ST_VT_Gobtn_Path),
				"(All tickets) not appear under 'Ticket #' column");

		Type(Xpath, Pimco_ST_OrderNotxt_Path, OrderNumber);
		Selectfilter(Pimco_ST_OrderNofilter_Path, "Contains");
		Assert.assertEquals(OrderNumber, Get_Text(Xpath, Pimco_ST_VT_OrderNo1_Path),
				"the search result for the <Order #>  entered is not displayed under the  'Order #' column - Veritas ticket");
		Selectfilter(Pimco_ST_OrderNofilter_Path, "NoFilter");

		Type(Xpath, Pimco_ST_SubCattxt_Path, Get_Text(Xpath, Pimco_ST_VT_SubCat1_Path).trim());
		SubCategory = Get_Text(Xpath, Pimco_ST_VT_SubCat1_Path).trim();
		Selectfilter(Pimco_ST_SubCatfilter_Path, "Contains");
		Assert.assertEquals(SubCategory, Get_Text(Xpath, Pimco_ST_VT_SubCat1_Path).trim(),
				"the search result for the Sub category  entered is not displayed under the  'Sub category' column - Veritas ticket");
		Selectfilter(Pimco_ST_SubCatfilter_Path, "NoFilter");

		Type(Xpath, Pimco_ST_Productcodetxt_Path, "qa");
		Selectfilter(Pimco_ST_Productcodefilter_Path, "Contains");
		Assert.assertTrue(Get_Text(Xpath, Pimco_ST_VT_Stockno1_Path).toLowerCase().contains("qa"),
				"<qa> is not displayed under 'Stock#' column");
		Selectfilter(Pimco_ST_Productcodefilter_Path, "NoFilter");

		Type(Xpath, Pimco_ST_Submittedbytxt_Path, PIMCOBothUsername);
		Selectfilter(Pimco_ST_Submittedbyfilter_Path, "Contains");
		Assert.assertTrue(Get_Text(Xpath, Pimco_ST_VT_Submittedby1_Path).trim().contains(PIMCOBothUsername),
				"the search result for the Submitted by entered is not displayed under the  'Submittedby' column - Veritas ticket");
		Selectfilter(Pimco_ST_Submittedbyfilter_Path, "NoFilter");

		Type(Xpath, Pimco_ST_Teamtxt_Path, "Fulfillment");
		Selectfilter(Pimco_ST_Teamfilter_Path, "Contains");
		Assert.assertTrue(Get_Text(Xpath, Pimco_ST_VT_Team1_Path).toLowerCase().contains("fulfillment"),
				" <Fulfillment> is not displayed under 'Team' column - Veritas ticket");
		Selectfilter(Pimco_ST_Teamfilter_Path, "NoFilter");

		Type(Xpath, Pimco_ST_Statustxt_Path, "Closed");
		Selectfilter(Pimco_ST_Statusfilter_Path, "Contains");
		Assert.assertEquals("Closed", Get_Text(Xpath, Pimco_ST_VT_Status1_Path),
				"the search result for the status entered is not displayed under the  'Status' column - Veritas ticket");
		Selectfilter(Pimco_ST_Statusfilter_Path, "NoFilter");

	}

	@Test(priority = 4, enabled = true)
	public void Pimco_TC_2_4_2_3_1() throws InterruptedException, IOException {

		// Add a new support ticket in Veritas section

		NavigateMenu(LP_Admin_path, LP_Support_path);
		Click(Xpath, Pimco_ST_VT_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_ST_CST_Createbtn_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_ST_CST_Categorydd_Path, "Other");
		Wait_ajax();
		Type(Xpath, Pimco_ST_CST_Commentfield_Path, "QATest");
		Select_DropDown_VisibleText(Xpath, Pimco_ST_CST_SubCategorydd_Path, "Other");
		Click(Xpath, Pimco_ST_CST_Createbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_STM_UpdateTicketbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_UpdateTicketbtn_Path),
				"new support ticket page is not displayed");
		Supporttickenumber = Get_Text(Xpath, Pimco_STM_Ticketno_Path).trim();
		Supporttickenumberlist.add(Supporttickenumber);
		Reporter.log(Supporttickenumber);
		Click(ID, Pimco_LP_Logout_ID);
		login(PIMCOCAUsername, PIMCOCAPassword);
		NavigateMenu(LP_Admin_path, LP_Support_path);
		Type(Xpath, Pimco_ST_TicketNotxt_Path, Supporttickenumber);
		Click(Xpath, Pimco_ST_SubCatfilter_Path);
		Wait_ajax();
		Assert.assertEquals(Supporttickenumber, Get_Text(Xpath, Pimco_ST_VT_TicketNo1_Path),
				"the search result for the <Ticket #>  entered is not displayed under the  'Ticket #' column - Veritas ticket");

	}

	@Test(priority = 5, enabled = true)
	public void Pimco_TC_2_4_2_3_2AndPimco_TC_2_4_2_3_3() throws InterruptedException, IOException {

		// Validate user is able to update the Ticket on Edit/View Page

		login(PIMCOBothUsername, PIMCOBothPassword);
		NavigateMenu(LP_Admin_path, LP_Support_path);
		Type(Xpath, Pimco_ST_TicketNotxt_Path, Supporttickenumber);
		Click(Xpath, Pimco_ST_SubCatfilter_Path);
		Wait_ajax();
		Click(Xpath, Pimco_ST_VT_Edit1_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_NewStatus_Path), " 'New' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_Inprogress_Path),
				" 'Inprogress' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_Hold_Path), " 'Hold' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_Closedstatus_Path),
				" 'Closed' button is not displayed");
		softAssert.assertAll();
		Assert.assertTrue(Get_Text(Xpath, Pimco_STM_Ticketno_Path).trim().equalsIgnoreCase(Supporttickenumber),
				" the ticket is not opened and displayed with the <Ticket #> displayed on the top right side of the page ");
		Click(Xpath, Pimco_STM_Inprogress_Path);
		ExplicitWait_Element_Visible(Xpath, Pimco_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");

		/* Close the ticket and verify the ticket does not show up on the Support page
		 * 
		 * Pimco_TC_2_4_2_3_3
		 */

		Click(Xpath, Pimco_STM_Closedstatus_Path);
		ExplicitWait_Element_Visible(Xpath, Pimco_STM_UpdateTicketmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_STM_UpdateTicketmsg_Path),
				"the message is not displayed as 'The Ticket was Successfully Updated'");
		NavigateMenu(LP_Admin_path, LP_Support_path);
		Type(Xpath, Pimco_ST_TicketNotxt_Path, Supporttickenumber);
		Click(Xpath, Pimco_ST_SubCatfilter_Path);
		Wait_ajax();
		Assert.assertEquals("Closed", Get_Text(Xpath, Pimco_ST_VT_Status1_Path),
				"'Closed' is not displayed under 'Status' column ");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
