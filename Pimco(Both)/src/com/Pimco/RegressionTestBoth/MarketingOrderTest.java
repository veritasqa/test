package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingOrderTest  extends BaseTestPimco {

	//Validate that an order is able to be placed
	@Test(enabled = true,priority =1)
	public void Pimco_TC_2_6_1_2_1ANDPimco_TC_2_6_1_2_2() throws InterruptedException, AWTException{
	    Clearcarts();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		Wait_ajax();
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_SR_Search_Btn_ID);
		
		
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Multifunction));
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
		
		
		
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	   // ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
	
		
		//Validate that the order confirmation page appears
		
		//Pimco_TC_2_6_1_2_2
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Order Information")),"Order Information grid  is missing");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Order Details")),"Order Details grid  is missing");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_Status_ID).trim().equalsIgnoreCase("NEW"),"NEW displays by 'Status' field");
	
		Assert.assertTrue(Get_Text(ID, Pimco_OC_HoldForComplete_ID).trim().equalsIgnoreCase("No"),"No is not displayed by 'Hold For Complete' field");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_OrderDate_ID).trim().equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),"Today's Date: MM/DD/YYYY missing for 'Order Date' field");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_CostCenter_ID).trim().equalsIgnoreCase("9999"),"9999 is not displayed by 'Billed to Cost Center' field");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedBy_ID).trim().equalsIgnoreCase("USPS"),"USPS  not displays by 'Shipped By' field ");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedTo_ID).trim().equalsIgnoreCase("Order Mail List (9 Records)"),"Order Mail List (9 Records)  not displays by 'Shipped To' field ");
		

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_OC_SubmitCancelRequest_Btn),"Submit Cancel Request button missing in 'Order Details' grid");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "QA_Multifunction")),"QA_Multifunction displays under 'Product Code' column");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "QA_MULTIFUNCTION_TITLE")),"QA_MULTIFUNCTION_TITLE displays  under 'Title' column");
		
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		}
	
	
    //Validate that a Holiday is not able to be selected as delivery date
	@Test(enabled = true,priority =2)
	public void Pimco_TC_2_6_1_2_3ANDPimco_TC_2_6_1_2_5() throws InterruptedException, AWTException{
		
		 Clearcarts();
	     ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_SR_Search_Btn_ID);
		
		Click(Xpath, CalenderbtnNexttoPiece(QA_Multifunction));
		
		//to check holiday is disabled
		
		Click(ID, "ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_rdpDripDate_calendar_Title");
		Click(Xpath, ".//a[text()='Dec']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='2018']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='2018']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Calendar_OK_Btn_ID);
		Click(ID, Pimco_SCP_Calendar_OK_Btn_ID);
		Thread.sleep(2000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ".//td[@title='Holiday']//span[text()='25']"), "User is  able to select  holiday date 25 December ");
		Click(Xpath, ".//a[text()='24']");
		Thread.sleep(2000);
		

		//Pimco_TC_2_6_1_2_5
		//Validate that an order can be created for Postcard with multiple drips with different dates and verify Order Confirmation page

		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, AB_36827);
		Click(ID, Pimco_SR_Search_Btn_ID);
		
		Click(Xpath, SearchResultsAddtoDripBtnXpath(AB_36827));
		   Click(Xpath, Textpath("span", "Checkout (1)"));
		    Wait_ajax();
			
			 ExplicitWait_Element_Clickable(ID, Pimco_CO_SaveBtn_ID);
			    
			    Type(ID,Pimco_CO_ExternalResourceLookup_ID , "John ");
			    
			    Click(Xpath, Textpath("td", "John Cardillo "));
			    
			    Type(ID,Pimco_CO_InternalResourceLookup_ID , "Kevin ");
			    
			    Click(Xpath, Textpath("td", "Kevin Peters "));
			    Wait_ajax();
			    Thread.sleep(2000);
			    Click(ID, Pimco_CO_SaveBtn_ID);
			    Wait_ajax();
			    Thread.sleep(6000);
				 ExplicitWait_Element_Clickable(Xpath, "//span[text()='AB_36827']//following::a[text()='Proof']");
			    Assert.assertTrue(Element_Is_Displayed(Xpath,"//div[contains(text(),'Proof Generation in Process')][1]"),"Proof Generation in Process is missing");
			    
			    
			    Click(Xpath, "//span[text()='AB_36827']//following::a[text()='Proof']");
			    Switch_New_Tab();
				Switch_Old_Tab();
			    
				Assert.assertTrue(Element_Is_Displayed(Xpath, "//span[text()='AB_36827']//following::a[text()='Proofed']"),"Proof' link is changed to Proofed");
				 
				
				Click(ID, Pimco_SCP_Checkout_ID);
				
				 ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
				    
				    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
					OrderNumberlist.add(OrderNumber);
					Reporter.log("Order Number is " + OrderNumber);
					System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
					Click(ID, Pimco_OC_OK_Btn_ID);
					Wait_ajax();
				
				
				
	}
	
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
}
