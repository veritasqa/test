package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingPartViewModeTest  extends BaseTestPimco{
	
	//Validate that admin only part is viewable for an admin user
	@Test(enabled = false,priority = 1)
	public void  Pimco_TC_2_6_3_1() throws InterruptedException, AWTException, IOException{
		Clearcarts();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Updated_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_adminonly);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_adminonly)),"QA_Adminonly displays in 'Materials' page");
		logout();
	}
	
	//Validate that an admin only part is not viewable for a basic use
	
	@Test(enabled = false,priority = 2)
	public void  Pimco_TC_2_6_3_2() throws IOException, InterruptedException, AWTException{

		login(PIMCOBothBasicUsername, PIMCOBothBasicPassword);
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Updated_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_adminonly);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");
    	logout();
		
		
	}
	
	
    //Validate when a part is set to viewable and not orderable that it's viewable
	
	@Test(enabled = false,priority = 3)
	public void  Pimco_TC_2_6_3_3() throws IOException, InterruptedException, AWTException{
	
		login(PIMCOBothUsername, PIMCOBothPassword);
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
	    ManageinventoryNav();
        Wait_ajax();
        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Viewability);
        Click(ID, Pimco_IM_Search_Btn_ID);
        Wait_ajax();
        
        Click(Xpath, ClickButtononSearchresultspath(QA_Viewability, "Edit"));
        Wait_ajax();
        
        Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
        Wait_ajax();
        Thread.sleep(3000);
        Click(Xpath, Textpath("li", "Viewable Not Orderable"));
        Wait_ajax();
        Thread.sleep(3000);
        Click(ID, Pimco_IM_Save_Btn_ID);
    	Wait_ajax();
    	Click(ID, Pimco_IM_PMSG_OK_Button_ID);
    	Wait_ajax();
    	
    	logout();
    	PimcoGWMLogin();
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Updated_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Viewability);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		
    	Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_Viewability))," QA_Viewability Not appears in 'Materials' Page");
    	Assert.assertTrue(Get_Text(Xpath, Pimco_SR_PieceAvailablity_Status_Path).trim().equalsIgnoreCase("Not Orderable"),"Not Orderable Not displays for 'QA_Viewability' ");
    	Thread.sleep(3000);
		
		
	}
	
	
	//Validate when a part is set to not viewable that is not viewable
	
	
	@Test(enabled = true,priority = 4)
	public void  Pimco_TC_2_6_3_4() throws InterruptedException, IOException, AWTException {

	    ManageinventoryNav();
        Wait_ajax();
        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Viewability);
        Click(ID, Pimco_IM_Search_Btn_ID);
        Wait_ajax();
        
        Click(Xpath, ClickButtononSearchresultspath(QA_Viewability, "Edit"));
        Wait_ajax();
        
        Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
        Wait_ajax();
        Thread.sleep(3000);
        Click(Xpath, Textpath("li", "Not Viewable"));
        Wait_ajax();
        Thread.sleep(3000);
        Click(ID, Pimco_IM_Save_Btn_ID);
    	Wait_ajax();
    	Click(ID, Pimco_IM_PMSG_OK_Button_ID);
    	Wait_ajax();
    	
    	logout();
    	PimcoGWMLogin();
    	
    	Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Updated_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Viewability);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
    	
	 	Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");
	
    	Click(Xpath, Textpath("span", "Clear Cart"));
    	Wait_ajax();
    	Thread.sleep(3000);
		
		
	}
	
	//Validate when the part is set to orderable that it is viewable and orderable
	@Test(enabled = true,priority = 5)
	public void  Pimco_TC_2_6_3_5() throws InterruptedException, IOException, AWTException {

	    ManageinventoryNav();
        Wait_ajax();
        Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Viewability);
        Click(ID, Pimco_IM_Search_Btn_ID);
        Wait_ajax();
        
        Click(Xpath, ClickButtononSearchresultspath(QA_Viewability, "Edit"));
        Wait_ajax();
        
        Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
        Wait_ajax();
        Thread.sleep(3000);
        Click(Xpath, Textpath("li", "Orderable"));
        Wait_ajax();
        Thread.sleep(3000);
        Click(ID, Pimco_IM_Save_Btn_ID);
    	Wait_ajax();
    	Click(ID, Pimco_IM_PMSG_OK_Button_ID);
    	Wait_ajax();
    	
    	logout();
    	PimcoGWMLogin();
    	
    	Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Updated_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Viewability);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		
		Wait_ajax();
    	Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsAddtoDripBtnXpath(QA_Viewability)),"QA_Viewability displays in 'Materials' Page");
	
    	logout();
		
	}
	
	
	
	public void ManageinventoryNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		

	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
}
