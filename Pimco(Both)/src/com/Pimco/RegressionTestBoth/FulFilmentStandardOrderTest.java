package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulFilmentStandardOrderTest extends BaseTestPimco{

	//Add a part to cart from the fulfillment search product page and complete order
	
	@Test(enabled = true , priority= 1)
	public void Pimco_TC_2_8_1_1_1() throws InterruptedException{

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
	}
	
	//Add a part to cart from the materials dropdown and complete order
	
	@Test(enabled = true , priority= 2)
	public void Pimco_TC_2_8_1_1_2ANDPimco_TC_2_8_1_1_3() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Materials"));
		ExplicitWait_Element_Visible(Xpath, Textpath("span", "Regulatory"));
		Click(Xpath, Textpath("span", "Regulatory"));
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();		
		ExplicitWait_Element_Clickable(ID, Pimco_SR_Search_Btn_ID);
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		
		Click(ID, Pimco_SR_Search_Btn_ID);
		Thread.sleep(6000);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Thread.sleep(6000);
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	
	    Click(ID, Pimco_SR_CheckOut_Btn_ID);
	    
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		//Pimco.TC.2.8.1.1.3
		//Validate that order confirmation page appears
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Order Information")),"Order Information grid  is missing");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Order Details")),"Order Details grid  is missing");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_Status_ID).trim().equalsIgnoreCase("NEW"),"NEW displays by 'Status' field");
	
		Assert.assertTrue(Get_Text(ID, Pimco_OC_HoldForComplete_ID).trim().equalsIgnoreCase("No"),"No is not displayed by 'Hold For Complete' field");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_OrderDate_ID).trim().equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),"Today's Date: MM/DD/YYYY missing for 'Order Date' field");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_CostCenter_ID).trim().equalsIgnoreCase("9999"),"9999 is not displayed by 'Billed to Cost Center' field");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedBy_ID).trim().equalsIgnoreCase("UPS Ground"),"UPS Ground  not displays by 'Shipped By' field ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("b", "QanoFirm IL")),"QanoFirm IL not displays by 'Shipped To'  field ");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_TotalChargebackCost).trim().equalsIgnoreCase("$0.00"),"$0.00 is not displayed by 'Total Chargeback Cost' field");

		//Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("input", "Submit Cancel Request")),"Submit Cancel Request button missing in 'Order Details' grid");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "QA_Multifunction")),"QA_Multifunction displays under 'Product Code' column");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "QA_MULTIFUNCTION_TITLE")),"QA_MULTIFUNCTION_TITLE displays  under 'Title' column");

		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "(1/0)")),"(1/0)) displays under 'Quantity(Available/BackOrder)' column");

		Assert.assertTrue(Element_Is_Displayed(ID,Pimco_OC_Copy_Btn_ID),"Copy button Missing");
		Assert.assertTrue(Element_Is_Displayed(ID,Pimco_OC_OK_Btn_ID),"Ok button Missing");

		//b[text()='QanoFirm IL']
		//div[@id="cphContent_cphSection_divAddress"][contains(.,'913 Commerce Ct')]
		
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
	}
	
	//Create an expedite order and verify order goes on Hold
	
	@Test(enabled = true , priority= 3)
	public void Pimco_TC_2_8_1_1_5() throws InterruptedException, IOException{
		Click(Xpath, Textpath("span","Home"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
	 
	    Click(ID, Pimco_SCP_ExpeditedDeliveryMethod_Checkbox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_ExpeditedDeliveryMethod_DropDown_ID);
		
	    
	    Select_DropDown_VisibleText(ID, Pimco_SCP_ExpeditedDeliveryMethod_DropDown_ID, "Next Day Early AM");
	    ExplicitWait_Element_Visible(Xpath,Textpath("li","This order is requesting an upgraded shipping method which requires approval."));
	    
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("li","This order is requesting an upgraded shipping method which requires approval.")),"'The order will be held for approval for the following reasons :");
	    

        Click(ID, Pimco_SCP_Checkout_ID);
        Wait_ajax();
	
	    ExplicitWait_Element_Clickable(Xpath, Pimco_SCP_Popup_Ok_Btn_Path);
	    Click(Xpath, Pimco_SCP_Popup_Ok_Btn_Path);
	    Thread.sleep(3000);
	
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_Status_ID).trim().equalsIgnoreCase("HOLD"),"HOLD displays by 'Status' field");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedBy_ID).trim().equalsIgnoreCase("Next Day Early AM"),"Next Day Early AM  not displays by 'Shipped By' field ");

	    Hover(Xpath, Textpath("span","Admin"));
	    
	    Click(Xpath, Textpath("span","Manage Orders"));
	    
	    Type(ID, Pimco_MO_OrderNumber_TxtBox_ID, OrderNumber);
	    
	    Click(ID, Pimco_MO_Search_Btn_ID);
	    ExplicitWait_Element_Clickable(Xpath, OrderNumberSelectorCopy(OrderNumber, "Select"));
	    
	    Click(Xpath, OrderNumberSelectorCopy(OrderNumber, "Select"));
	    ExplicitWait_Element_Clickable(Xpath, Pimco_MO_SelectOrder_Btn_Path);
	    
	    Click(Xpath, Pimco_MO_EditStatus_Btn_Path);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_MO_Update_Btn_ID);

	    
	    Click(ID, Pimco_MO_Update_Btn_ID);
	    
	    logout();
	    
	    login(PIMCOBothUsername, PIMCOBothPassword);
	    
	    Hover(Xpath, Textpath("span","Admin"));
	    
	    Click(Xpath, Textpath("span","Manage Orders"));
	    
	    Type(ID, Pimco_MO_OrderNumber_TxtBox_ID, OrderNumber);
	    
	    Click(ID, Pimco_MO_Search_Btn_ID);
	    ExplicitWait_Element_Clickable(Xpath, OrderNumberSelectorCopy(OrderNumber, "Select"));
	    
	     
		Assert.assertTrue(Element_Is_Displayed(Xpath, OrderNumberSearchResults(OrderNumber, "Next Day Early AM")),"Next Day Early AM displays under 'Shipper' column in 'Search Results' grid");   

	    
		Assert.assertTrue(Element_Is_Displayed(Xpath, OrderNumberSearchResults(OrderNumber, "NEW")),"NEW displays under 'Status' column in 'Search Results' grid");   
	    
	}
	
	//Validate that a Holiday is not able to be selected as delivery date
	@Test(enabled = true ,  priority= 4)
	
	public void Pimco_TC_2_8_1_1_6ANDPimco_TC_2_8_1_1_7() throws InterruptedException{
		
			Click(Xpath, Textpath("span","Home"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		  FulfilmentSearch(QA_Multifunction);
		  Wait_ajax();
		  ContactClick("QanoFirm", "IL");
		  Wait_ajax();
		  Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		  Wait_ajax();
		  ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
		   Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		  Click(Xpath, Textpath("span", "Checkout (1)"));
		  Wait_ajax();
		  ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		    Wait_ajax();
		    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		    
		    Click(ID, Pimco_SCP_Next_Btn_ID);
		    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    
		    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);
		
		   
			Click(ID, Pimco_SCP_Calendar_Title_ID);
			Click(Xpath, ".//a[text()='Dec']");
			ExplicitWait_Element_Clickable(Xpath, ".//a[text()='2018']");
			Thread.sleep(2000);
			Click(Xpath, ".//a[text()='2018']");
			Thread.sleep(2000);
			ExplicitWait_Element_Clickable(ID, Pimco_SCP_Calendar_OK_Btn_ID);
			Click(ID, Pimco_SCP_Calendar_OK_Btn_ID);
			Thread.sleep(2000);
			Assert.assertTrue(Element_Is_Displayed(Xpath, ".//td[@title='Holiday']//span[text()='25']"), "User is  able to select  holiday date 25 December ");
			Click(Xpath, ".//a[text()='24']");
			
			Thread.sleep(2000);
			
		//	Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
		
			//Thread.sleep(2000);
			
			
			//Validate an order template can be created
			//Pimco_TC_2_8_1_1_7
			
			Click(ID, Pimco_SCP_CreateOrderTemplate_Btn_ID);
			ExplicitWait_Element_Clickable(ID, Pimco_SCP_OrderTemplate_Create_Btn_ID);

		
		
			Type(ID, Pimco_SCP_OrderTemplateName_TxtBox_ID, "QA Template Test");
			Type(ID, Pimco_SCP_OrderTemplateDescription_TxtBox_ID, "QA Test");

			Click(ID, Pimco_SCP_OrderTemplate_Create_Btn_ID);
			Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "QA Template Test order template has been successfully created.")),"QA Template Test order template has been successfully created message displays");
			
			Click(Xpath, Textpath("span","Home"));
			Wait_ajax();
			
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			Assert.assertTrue(Element_Is_Displayed(Xpath,"//em[text()='Order Templates']/following::*[text()='QA Template Test'][1]")," QA Template Test not displays in 'Order Templates' widget");

			
			Click(Xpath, Textpath("span","Clear Cart"));
			
	}
	
	
	//Verify the message "UPS Worldwide Expedited" in 'Delivery Method' field in Pimco (Both)
	
	@Test(enabled = true, priority= 5)
	public void Pimco_TC_2_8_1_1_8() throws InterruptedException{
		
		Click(Xpath, Textpath("span","Home"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		  FulfilmentSearch(QA_FulfillmentOnly);
		  Wait_ajax();
		  ContactClick("QanoFirm", "IL");
		  Wait_ajax();
		  Thread.sleep(3000);
		  Click(Xpath, AddToCartbtnXpath("FulfillmentOnly"));
		  Wait_ajax();
		  
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("FulfillmentOnly"));
		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		    Click(Xpath, Textpath("span", "Checkout (1)"));
		    Wait_ajax();
		   
			ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
			Select_DropDown_VisibleText(ID, Pimco_SCP_Country_Dropdown_ID, "Japan");   
			 Wait_ajax();
			 Thread.sleep(3000);
		    Type(ID, Pimco_SCP_Address1_TxtBox_ID, "Toranomon Towers Office 18F");
		    Type(ID,Pimco_SCP_Address2_TxtBox_ID , "4-1-28");
		    Type(ID,Pimco_SCP_City_TxtBox_ID,  "Toranomon");
		    Type(ID,Pimco_SCP_State_TxtBox_ID,  "Minato-ku");

		    
		    Type(ID,Pimco_SCP_Zip_TxtBox_ID,  "1050001");
		    
		    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		    Wait_ajax();
		    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		    
		    Click(ID, Pimco_SCP_Next_Btn_ID);
		    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    
		    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

		    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
		    
		    Type(ID, Pimco_SCP_RecipientNotes_ID, "QA Test Order");
		    
		    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
		    
		    Click(ID, Pimco_SCP_Checkout_ID);
		    
		    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		    
		    Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedBy_ID).trim().equalsIgnoreCase("UPS Worldwide Expedited"),"UPS Worldwide Expedited not displays in 'Shipped By' field");
		    
		    
		    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
			OrderNumberlist.add(OrderNumber);
			Reporter.log("Order Number is " + OrderNumber);
			System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
			Click(ID, Pimco_OC_OK_Btn_ID);
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
		    
	}
	
	//Verify the Error Message " Invalid Destination Postal Code in TimeInTransit" in Pimco (Both)
	@Test(enabled = true ,  priority= 6)
	public void Pimco_TC_2_8_1_1_9() throws InterruptedException{
		
		Click(Xpath, Textpath("span","Home"));
	      Wait_ajax();
	      ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		  FulfilmentSearch(QA_FulfillmentOnly);
		  Wait_ajax();
		  ContactClick("QanoFirm", "IL");
		  Wait_ajax();
		  Thread.sleep(4000);
		  Click(Xpath, AddToCartbtnXpath("FulfillmentOnly"));
	  
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("FulfillmentOnly"));
		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		    Click(Xpath, Textpath("span", "Checkout (1)"));
		    Wait_ajax();
		    Wait_ajax();
			ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		    Select_DropDown_VisibleText(ID, Pimco_SCP_Country_Dropdown_ID, "Japan");       
		    Thread.sleep(3000);
		    Type(ID, Pimco_SCP_Address1_TxtBox_ID, "Toranomon Towers Office 18F");
		    Type(ID,Pimco_SCP_Address2_TxtBox_ID , "4-1-28");
		    Type(ID,Pimco_SCP_City_TxtBox_ID,  "Toranomon");
		    Type(ID,Pimco_SCP_State_TxtBox_ID,  "Minato-ku");


		    Type(ID,Pimco_SCP_Zip_TxtBox_ID,  "105-0001");
		    
		    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		    Wait_ajax();
		    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		    
		    Click(ID, Pimco_SCP_Next_Btn_ID);
		    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    
		    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

		    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
		    
		    Type(ID, Pimco_SCP_RecipientNotes_ID, "QA Test Order");
		    
		    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
		    
		    Click(ID, Pimco_SCP_Checkout_ID);
		    
		    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		    
		    Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedBy_ID).trim().equalsIgnoreCase("Error: Invalid Destination Postal Code in TimeInTransit"),"Error: Invalid Destination Postal Code in TimeInTransitnot displays in 'Shipped By' field");
		    
		    
		    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
			OrderNumberlist.add(OrderNumber);
			Reporter.log("Order Number is " + OrderNumber);
			System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
			Click(ID, Pimco_OC_OK_Btn_ID);
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
	}
	
	@BeforeMethod
	public void ClearCart() throws InterruptedException   {
		Clearcarts();
	}

	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
		
	//	Clearcarts();
	}
	
}
