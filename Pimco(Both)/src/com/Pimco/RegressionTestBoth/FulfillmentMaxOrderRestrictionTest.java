package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulfillmentMaxOrderRestrictionTest extends BaseTestPimco{
    
	
	//Validate max order quantity: Pimco Admin
	@Test
	public void PIMCO_TC_2_8_4_1() throws InterruptedException{
		FulfilmentSearch(QA_Maxorder);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		
		
		Type(Xpath, PieceQuantityPath(QA_Maxorder), "3");
		Click(Xpath,SearchResultsAddtoCartBtnXpath(QA_Maxorder));
		Wait_ajax();
		
		//ExplicitWait_Element_Visible(Xpath,AddtoCartPiecenameXpath(QA_Maxorder, "Successfully Added 3!"));
		Assert.assertTrue(Element_Is_Displayed(Xpath,AddtoCartPiecenameXpath(QA_Maxorder, "Successfully Added 3!")),"Successfully Added 3!-  Message is missing");
		Wait_ajax();
		Thread.sleep(2000);
		Clear(Xpath, PieceQuantityPath(QA_Maxorder));
		Type(Xpath, PieceQuantityPath(QA_Maxorder), "5");
		Click(Xpath,SearchResultsAddtoCartBtnXpath(QA_Maxorder));
	//	Wait_ajax();
	//	ExplicitWait_Element_Visible(Xpath,AddtoCartPiecenameXpath(QA_Maxorder, "Max Order Quantity is 3 (3 in cart)"));

		Assert.assertTrue(Get_Text(Xpath,"//span[@class='productListFormNumber'][contains(.,'QA_Maxorder')]/following::div[@class='errorMessageCart']").contains("Max Order Quantity is 3 (3 in cart)"),"Max Order Quantity is 3 (3 in cart)-  Message is missing");
		Wait_ajax();
		
		
		Clearcarts();
		
	}
	
	
	
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();
	}
}
