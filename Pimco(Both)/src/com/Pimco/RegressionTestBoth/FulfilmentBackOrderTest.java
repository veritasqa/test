package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulfilmentBackOrderTest extends BaseTestPimco{

	//Validate search for part that allows backorders
	@Test(enabled = true , priority= 1)
	public void Pimco_TC_2_8_5_1() throws InterruptedException{
		FulfilmentSearch(QA_Backordertest);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("Back Order Test"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("Back Order Test"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")),
				"(1) is displaying next to 'Checkout' link");
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
	

		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Backordered: 1")),
				"Backordered: 1 error message is  missing ");
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);

		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Checkout_ID);

		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);

		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Quantity (Available/BackOrder)")),
				"Quantity (Available/BackOrder)  missing in the order details coloumn");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "(0/1)")),
				"(0/1) is  missing in the order details coloumn");
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
	}
	
	
	//Validate that a Holiday is not able to be selected as delivery date
	@Test(enabled = true , priority= 2)
	public void Pimco_TC_2_8_5_2() throws InterruptedException {
		FulfilmentSearch(QA_Backordertest);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("Back Order Test"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("Back Order Test"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")),
				"(1) is displaying next to 'Checkout' link");
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
	

		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Backordered: 1")),
				"Backordered: 1 error message is  missing ");
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		
	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);
	
	   
		Click(ID, Pimco_SCP_Calendar_Title_ID);
		Click(Xpath, ".//a[text()='Dec']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='2018']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='2018']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Calendar_OK_Btn_ID);
		Click(ID, Pimco_SCP_Calendar_OK_Btn_ID);
		Thread.sleep(2000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ".//td[@title='Holiday']//span[text()='25']"), "User is  able to select  holiday date 25 December ");
		Click(Xpath, ".//a[text()='24']");
		
		Thread.sleep(2000);
		
		Clearcarts();
		
	}
	
	
	//Validate a part that doesn't allow backorders
	@Test(enabled = true , priority= 3)
	public void Pimco_TC_2_8_5_3() throws InterruptedException{
		FulfilmentSearch(QA_AllowBackOrder);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("*", "Out of Stock")),"Out of stock not displayed on search results page");
		
	}
	
	
	//Kit Backorder Acknowledgement
	@Test(enabled = true , priority= 4)
	public void Pimco_TC_2_8_5_4() throws InterruptedException {
		
		FulfilmentSearch(QA_Kitacknowledgetest);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("Acknowledge Kit"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("Acknowledge Kit"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")),
				"(1) is displaying next to 'Checkout' link");
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Component is Backordered")),"Component is Backordered error message is  missing ");
		
		
		Click(Xpath, Textpath("a", "Show Component Details"));
		
		Switch_To_Iframe("oRadWindowComponentDetails");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Backordered: 1")),
				"Backordered: 1 error message is  missing ");
		Switch_To_Default();
		Click(Xpath, Pimco_SCP_Showcomponent_Frame_CloseBtn_Path);
		Wait_ajax();
		
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Next_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("*",Pimco_SCP_BackorderMsg)),Pimco_SCP_BackorderMsg+"--Message is missing");
		Assert.assertTrue(Element_Is_Displayed(ID,Pimco_SCP_BackorderAcceptBtn_ID),"Accept button is missing");
		
		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_Checkout_ID);

		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);

		OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
	
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
		
	//	Clearcarts();
	}
}
