package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class ValidateFulfillmentSearchTest  extends BaseTestPimco{

    //Validate that parts can be searched thru fulfillment and Marketing search field	
	@Test(enabled = false , priority = 1)
	public void Pimco_TC_2_8_1_1() throws InterruptedException, AWTException{
		Clearcarts();
		
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE")),"QA_Multifunction doesnot appears on 'Materials' page");

		Click(Xpath, Textpath("span","Home"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist5_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		ExplicitWait_Element_Clickable(ID, Pimco_SR_Search_Btn_ID);
		
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		
		Click(ID,Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_Multifunction)),"QA_Multifunction displays in 'Materials' page");
		
		Thread.sleep(6000);
		Click(Xpath, Textpath("span","Clear Cart"));

		Thread.sleep(6000);
	}
	
	//Validate that parts can be searched by using the materials menu
	   @Test(enabled = false , priority = 2)
		public void Pimco_TC_2_8_1_2() throws InterruptedException{
			
			Hover(Xpath, Textpath("span", "Materials"));
			Click(Xpath, Textpath("span", "FINRA letter"));
			
			ContactClick("QanoFirm", "IL");
	    	Thread.sleep(6000);
	    	
		   
	    	
			ExplicitWait_Element_Clickable(ID, Pimco_SR_Search_Btn_ID);
			
			
			Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
			
			Click(ID,Pimco_SR_Search_Btn_ID);
			Wait_ajax();
			
			Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_Multifunction)),"QA_Multifunction displays in 'Materials' page");
			Thread.sleep(6000);
			Click(Xpath, Textpath("span","Clear Cart"));
			Thread.sleep(6000);
		}
	   
	   
	   //Validate that parts with marketing mode only do not appear in fulfillment search results
	   @Test(enabled = false, priority = 3)
			public void Pimco_TC_2_8_1_3() throws InterruptedException{
		    FulfilmentSearch(QA_Marketingonly);
			ContactClick("QanoFirm", "IL");
	    	Thread.sleep(6000);

	    	Wait_ajax();
	    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");
	    	Thread.sleep(6000);
	    	Click(Xpath, Textpath("span", "Clear Cart"));
	    	Wait_ajax();
	   }
	
	   
	   //Validate that parts with fulfillment only appear in fulfillment search results
	   @Test(enabled = false, priority = 4)
		public void Pimco_TC_2_8_1_4() throws InterruptedException{
		   
		    FulfilmentSearch(QA_FulfillmentOnly);
				ContactClick("QanoFirm", "IL");
		    	Thread.sleep(6000);
		    	
				Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_FulfillmentOnly)),"QA_FulfillmentOnly displays in 'Materials' page");

				Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Marketingonly);
				
				Click(ID,Pimco_SR_Search_Btn_ID);
				Wait_ajax();
				
				Wait_ajax();
		    	Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");
		    	Thread.sleep(6000);
		    	Click(Xpath, Textpath("span", "Clear Cart"));
		    	Thread.sleep(6000);
				
	   }
	   
	   //Verify appropriate piece displays by selecting appropriate Firm in Advance Search filters on Materials page
	   @Test(enabled = false, priority = 5)
		public void Pimco_TC_2_8_1_5() throws InterruptedException {
		   
		    FulfilmentSearch("QA_");
			ContactClick("QanoFirm", "IL");
			Thread.sleep(6000);
			
			Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_FirmRestriction);
		  	Wait_ajax();
			Click(ID, Pimco_SR_AdvancedSearch_Btn_ID);
	    	Wait_ajax();

			Click(ID, Pimco_SR_Pre_Approved_Firm_DD_Arrow_ID);
			Wait_ajax();
			Click(Xpath, li_value("AXA"));
			Wait_ajax();
			Click(ID, Pimco_SR_Exclude_User_Kits_Checkbox_ID);
			Wait_ajax();
			Click(ID,Pimco_SR_Search_Btn_ID);
			Wait_ajax();
			
			Wait_ajax();
			Assert.assertTrue(Element_Is_Displayed(Xpath,SearchResultsPiceNameXpath(QA_FirmRestriction)),"QA_FirmRestriction displays in 'Materials' page");
		
			Thread.sleep(6000);
			Click(Xpath, Textpath("span", "Clear Cart"));
			Thread.sleep(6000);
			
		   		
	   }
	  
	//Verify Proof button turns to proofed in Component Details page     
	   @Test(enabled = true , priority = 6)
		public void Pimco_TC_2_8_1_6() throws InterruptedException{
		   
		    FulfilmentSearch(QA_ProofTest);
			ContactClick("QanoFirm", "IL");
			Thread.sleep(6000);
			ExplicitWait_Element_Clickable(Xpath,SearchResultsAddtoCartBtnXpath(QA_ProofTest));
			Click(Xpath,SearchResultsAddtoCartBtnXpath(QA_ProofTest));
		
			
			Wait_ajax();
		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		    Click(Xpath, Textpath("span", "Checkout (1)"));
			
		    ExplicitWait_Element_Clickable(ID, Pimco_CO_SaveBtn_ID);
		    
		    Type(ID,Pimco_CO_ExternalResourceLookup_ID , "Alex ");
		    
		    Click(Xpath, Textpath("td", "Alex  Heron "));
		    
		    Type(ID,Pimco_CO_InternalResourceLookup_ID , "David ");
		    
		    Click(Xpath, Textpath("td", "David Morrison "));
		 
		    Click(ID, Pimco_CO_SaveBtn_ID);
		    Wait_ajax();
		    Thread.sleep(6000);
		    
		    Click(Xpath,Textpath("a", "Show Component Details"));
		    
		    Switch_To_Iframe(Pimco_SR_Frame_Name);
		   
		    Click(Xpath,Pimco_SR_ExpandBtn_Xpath);
		    
		    Click(Xpath,Pimco_SR_ProofBtn_Xpath);
		    Switch_New_Tab();
			Switch_Old_Tab();
		    
			Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SR_ProofedBtn_Xpath),"Proof' link is changed to Proofed");
			 Click(Xpath,Pimco_SR_Close_Btn_Xpath);
			 
			Switch_To_Default();
			
			Thread.sleep(6000);
			Click(Xpath, Textpath("span", "Clear Cart"));
			Thread.sleep(6000);
			
	
	   }
	   
	   
	   //Hide PTS Backorder Message from Component Details on Checkout Page 
	   
	   @Test(enabled = true, priority = 7)
		public void Pimco_TC_2_8_1_7() throws InterruptedException{
		   
		    FulfilmentSearch(QA_Kit_Ptsmessagetest);
			ContactClick("QanoFirm", "IL");
			Thread.sleep(6000);
			
			ExplicitWait_Element_Clickable(Xpath,SearchResultsAddtoCartBtnXpath(QA_Kit_Ptsmessagetest));
			Click(Xpath,SearchResultsAddtoCartBtnXpath(QA_Kit_Ptsmessagetest));
		
			
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath(QA_Kit_Ptsmessagetest));
		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		    Click(Xpath, Textpath("span", "Checkout (1)"));
		    Wait_ajax();	    

		    Click(Xpath,Textpath("a", "Show Component Details"));
		    
		    Switch_To_Iframe(Pimco_SR_Frame_Name);
		    
		    Click(Xpath,Pimco_SR_Close_Btn_Xpath);
			 
			Switch_To_Default();
			Thread.sleep(6000);
			Click(Xpath, Textpath("span", "Clear Cart"));
			Thread.sleep(6000);
	   }
	
	   
	   //Verify Parent piece is Out of Stock when the component is Not set to allow for backorder
	   @Test(enabled = false, priority = 8)
		public void Pimco_TC_2_8_1_8() throws InterruptedException{
			Hover(Xpath, Textpath("span", "Admin"));
			Click(Xpath, Textpath("span", "Manage Inventory"));
			ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
			Wait_ajax();
			
			Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_KitOnFly_BO_01);
		    Click(ID, Pimco_IM_Search_Btn_ID);
		    Wait_ajax();
		    Click(Xpath, ClickButtononSearchresultspath(QA_KitOnFly_BO_01, "Edit"));
		    Wait_ajax();
		    
		    Click(Xpath, Textpath("span", "Kitting"));
	        Wait_ajax();
	        
	        Click(Xpath, Pimco_IM_KT_ViewDetails_Btn_Xpath("QA_AllowBackOrder", "View Details"));
	        Wait_ajax();
	        
	        Click(Xpath,Textpath("span", "OK"));
	        Wait_ajax();
	        
    
	        Click(Xpath, Textpath("span", "Rules"));
	        Wait_ajax();
	        
		    Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_RT_Allow_Backorders_ID), "Yes checkbox for 'Allow Backorders?' field is unchecked");

		    Click(Xpath, Textpath("span","Home"));
			Wait_ajax();
			
			FulfilmentSearch(QA_KitOnFly_BO_01);
			ContactClick("QanoFirm", "IL");
			Thread.sleep(6000);
			
			Assert.assertTrue(Get_Text(Xpath, Pimco_SR_PieceAvailablity_Status_Path).equalsIgnoreCase("Out of Stock"), "Out of Stock displays for 'QA_KitOnFly-BO_01'");
			Thread.sleep(6000);
			Click(Xpath, Textpath("span", "Clear Cart"));
		    Wait_ajax();
	   }
	   
		@BeforeMethod
		public void ClearCart() throws InterruptedException   {
			Clearcarts();
		}
		
	   
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}

}
