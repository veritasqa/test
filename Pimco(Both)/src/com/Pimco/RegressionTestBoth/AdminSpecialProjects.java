package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class AdminSpecialProjects extends BaseTestPimco {

	public static String Duedate = "";

	public void Addsplproject() throws InterruptedException {

		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		SplprjTicketNumber = Get_Text(Xpath, SPP_Gen_TicketNo_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		System.out.println("Created ticket number is " + SplprjTicketNumber);
		Reporter.log("Created ticket number is " + SplprjTicketNumber);

	}

	public void SpecialProjectsNav() {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Special Projects"));
		ExplicitWait_Element_Clickable(Xpath, SPP_Addticketicon_Path);

	}

	public String ClickViewEdit(String Ticketno) throws InterruptedException {

		return ".//a[text()='" + Ticketno + "']//following::a[1][text()='View/Edit']";
	}

	public String History(String Changehistory) {

		return ".//td[contains(text(),'" + Changehistory + "')]";
	}

	public String Status(String Ticketno) throws InterruptedException {

		return ".//a[text()='" + Ticketno + "']//following::td[3]";
	}

	@Test(enabled = true, priority = 1)
	public void Pimco_TC_2_4_1_1_3() throws InterruptedException {

		// Verify "Add Ticket", "View/Edit", "Refresh" button displays on the
		// Special Projects table.

		SpecialProjectsNav();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Addticketicon_Path),
				"'Add Ticket' button is not displays on the bottom of the 'Special Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Refreshicon_Path),
				" 'Refresh' button is not displays on the bottom of the 'Special Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_ViewEdit1_Path),
				" 'View/Edit' button is not displays for all the rows in 'Special Projects : View Tickets' grid on the 'Special Projects' page");
		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 2)
	public void Pimco_TC_2_4_1_1_1() throws InterruptedException, IOException {

		// Validate Filter functionality for all the column headings on Special
		// Projects: View Tickets page

		SpecialProjectsNav();
		Addsplproject();
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Duedate = Get_Attribute(Xpath, SPP_Prog_Duedate_Path, "value").trim();
		Wait_ajax();
		SpecialProjectsNav();
		Type(Xpath, SPP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, SPP_TicketNofilter_Path);
		Click(Xpath, Textpath("span", "Contains"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"<Ticket Number>  is not displayed under  'Ticket #' column  in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_TicketNofilter_Path);
		Click(Xpath, Textpath("span", "NoFilter"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Ticket # filter ");

		Type(Xpath, SPP_Jobtitle_Path, "QA Test");
		Click(Xpath, SPP_JobtitleFilter_Path);
		Click(Xpath, Textpath("span", "Contains"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtitle1_Path).trim().contains("QA Test"),
				"<Job Title>  is not displayed under  'Job Title' column  in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_JobtitleFilter_Path);
		Click(Xpath, Textpath("span", "NoFilter"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Jobtitle filter ");

		Select_lidropdown(SPP_Jobtypearrow_Path, Xpath, "Other");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Jobtype1_Path).trim().equalsIgnoreCase("Other"),
				" 'Other' is not displayed in all rows under 'Job Type' column in 'Special Projects : View Tickets' grid ");
		Select_lidropdown(SPP_Jobtypearrow_Path, Xpath, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Jobtype filter ");

		Select_lidropdown(SPP_Statusarrow_Path, Xpath, "New");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Status1_Path).trim().equalsIgnoreCase("New"),
				" 'New' is not displayed in all rows under 'Status' column in 'Special Projects : View Tickets' grid ");
		Select_lidropdown(SPP_Statusarrow_Path, Xpath, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Status filter ");

		Type(Xpath, SPP_Duedate_Path, Duedate);
		Click(Xpath, SPP_DuedateFilter_Path);
		Click(Xpath, Textpath("span", "EqualTo"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertFalse(Get_Text(Xpath, SPP_Duedate1_Path).trim().isEmpty(),
				"<Due date>  is not displayed under  'Due date' column  in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_DuedateFilter_Path);
		Click(Xpath, Textpath("span", "NoFilter"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Due date filter ");

		Click(Xpath, SPP_Rushck_Path);
		Click(Xpath, SPP_Rushckfilter_Path);
		Click(Xpath, Textpath("span", "EqualTo"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, SPP_Rushck1_Path),
				"only the checked checkboxes rows are not displayed under  'Rush' column in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_Rushckfilter_Path);
		Click(Xpath, Textpath("span", "NoFilter"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Rush filter ");

		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_Closedckfilter_Path);
		Click(Xpath, Textpath("span", "EqualTo"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, SPP_Closedck1_Path),
				"only the checked checkboxes rows are not displayed under  'Closed' column in 'Special Projects : View Tickets' grid ");
		Click(Xpath, SPP_Closedckfilter_Path);
		Click(Xpath, Textpath("span", "NoFilter"));
		// Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Pagesize_Path),
		// " <All> available ticket records are not displayed in the 'Special
		// Projects : View Tickets' grid - Close filter ");
	}

	@Test(enabled = true, priority = 3)
	public void Pimco_TC_2_4_1_1_4() throws InterruptedException {

		// Validate "Add ticket button",Edit/View button takes to "General
		// Special projects" page

		SpecialProjectsNav();
		Click(Xpath, SPP_Addticketicon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job title' field is not displays  in 'General' tab on 'Special Projects' page");
		Click(Xpath, SPP_Gen_Cancel_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_ViewEdit1_Path);
		Click(Xpath, SPP_ViewEdit1_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job title' field is not displays  in 'General' tab on 'Special Projects' page");
		Click(Xpath, SPP_Gen_Cancel_Path);

	}

	@Test(enabled = true, priority = 4)
	public void Pimco_TC_2_4_1_1_5() throws InterruptedException, IOException {

		// Add a new special project and verify new ticket appears on Special
		// projects: view tickets table

		SpecialProjectsNav();
		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_TicketNo_Path),
				"New <Ticket Number> is not displayed for the 'Ticket #' field");
		SplprjTicketNumber = Get_Text(Xpath, SPP_Gen_TicketNo_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		System.out.println("Created ticket number is " + SplprjTicketNumber);
		Reporter.log("Created ticket number is " + SplprjTicketNumber);
		SpecialProjectsNav();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				"New <Ticket Number> is not displayed under the 'Ticket #' column on the 'Special Projects: View Tickets ' grid");
		Click(ID, Pimco_LP_Logout_ID);
		login(PIMCOCAUsername, PIMCOCAPassword);
		SpecialProjectsNav();
		Type(Xpath, SPP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, SPP_TicketNofilter_Path);
		Click(Xpath, Textpath("span", "Contains"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				" Entered <Pimco (Both) Ticket #>  is displayed under  'Ticket #' column  in 'Special Projects : View Tickets' grid - CA users");
		Click(ID, Pimco_LP_Logout_ID);
		login(PIMCOUSUsername, PIMCOUSthPassword);
		SpecialProjectsNav();
		Type(Xpath, SPP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, SPP_TicketNofilter_Path);
		Click(Xpath, Textpath("span", "Contains"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				" Entered <Pimco (Both) Ticket #>  is displayed under  'Ticket #' column  in 'Special Projects : View Tickets' grid - US users");
		Click(ID, Pimco_LP_Logout_ID);

	}

	@Test(enabled = true, priority = 5)
	public void Pimco_TC_2_4_1_1_7() throws InterruptedException, IOException {

		// Validate change history updates made

		if (Element_Is_Displayed(ID, Pimco_Login_LoginBtn_ID)) {
			login(PIMCOBothUsername, PIMCOBothPassword);

		}

		SpecialProjectsNav();
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA is Testing");
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page");
		Click(Xpath, SPP_HistoryTab_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_History_DateTime1_Path).trim().contains(Get_Todaydate("M/d/yyyy")),
				"the date and time of the description added<MM/DD/YYYY HH:MM:SS AM/PM> is not displayed under 'Date/Time' column in the 'History' tab");
		Assert.assertTrue(
				Get_Text(Xpath, SPP_History_User1_Path).trim().equalsIgnoreCase(Both_Fname + " " + Both_Lname),
				"the <User's first name&Last name> is not displayed under the 'User' column in the 'History' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Job Title From")),
				"'Changed Description From '' To 'QA is Testing''  is not displayed under 'Details' Column in the 'History' tab - 1");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("QA is Testing")),
				"'Changed kDescription From '' To 'QA is Testing''  is not displayed under 'Details' Column in the 'History' tab - 2");

	}

	@Test(enabled = true, priority = 6)
	public void Pimco_TC_2_4_1_1_9() throws InterruptedException, IOException {

		// Verify changes are not saved on clicking Cancel button

		SpecialProjectsNav();
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		Type(Xpath, SPP_Gen_Splinst_Path, "Test Special Instructions");
		Click(Xpath, SPP_Gen_Cancel_Path);
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Splinst_Path, "value").equalsIgnoreCase(""),
				"Test Special Instructions is displayed in 'Special Instructions' field");

	}

	@Test(enabled = true, priority = 7)
	public void Pimco_TC_2_4_1_1_10() throws InterruptedException, IOException {

		// Validate the cancel functionality

		SpecialProjectsNav();
		Click(Xpath, ClickViewEdit(SplprjTicketNumber));
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Click(Xpath, SPP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
		Type(Xpath, SPP_Prog_Comment_Path, "'QA is validating cancel functionality");
		Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
		Wait_ajax();
		Click(Xpath, SPP_Prog_Submit_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, SPP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed by the 'Status' field on the header of the 'Progress/Files' tab");
		SpecialProjectsNav();
		Click(Xpath, SPP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				" <Ticket #> is not displayed under the 'Ticket #' column on the 'Special Projects : View Tickets' grid");
		Assert.assertTrue(Get_Text(Xpath, Status(SplprjTicketNumber)).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed under 'Status' column for the copied <Ticket #> on 'Special Projects : View Tickets' grid");
	}

	@Test(enabled = true, priority = 8)
	public void Pimco_TC_2_4_1_1_2() throws InterruptedException, IOException, AWTException {

		// Validate the combination of filter functionality

		SpecialProjectsNav();
		Click(Xpath, SPP_Addticketicon_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Cancel_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_JobTitle_Path),
				"'Job title' field is not displays  in 'General' tab on 'Special Projects' page");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_JobTitle_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Job Title' field");
		Type(Xpath, SPP_Gen_JobTitle_Path, "QA Test 123");
		softAssert.assertTrue(Get_DropDown(Xpath, SPP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"'-Select-'  is not displays in 'Job Type' drop down field");
		Select_DropDown_VisibleText(Xpath, SPP_Gen_JobType_Path, "Other");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Description_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Description' field");
		Type(Xpath, SPP_Gen_Description_Path, "QA is Testing");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Gen_Createdby_Path).trim().equalsIgnoreCase(PIMCOBothUsername),
				" <username> is not displays for 'Created By' field and is read only");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Costcenter_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Cost center' field");
		Type(Xpath, SPP_Gen_Costcenter_Path, "9999");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Gen_Splinst_Path, "value").equalsIgnoreCase(""),
				"' ' is not displaying in 'Special Instructions' field");
		Type(Xpath, SPP_Gen_Splinst_Path, "QA Test");
		softAssert.assertFalse(Element_Is_selected(Xpath, SPP_Gen_Isrush_Path), "'Is Rush?' checkbox is checked");
		Click(Xpath, SPP_Gen_Isrush_Path);
		softAssert.assertFalse(Element_Is_selected(Xpath, SPP_Gen_IsQuoteneeded_Path),
				"'Is Quote needed?' checkbox is checked");
		Click(Xpath, SPP_Gen_IsQuoteneeded_Path);
		Click(Xpath, SPP_Gen_Create_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Gen_Save_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page");
		softAssert.assertAll();
		Assert.assertFalse(Element_Is_Displayed(Xpath, SPP_Gen_Create_Path), " 'Create' button is displayed");
		Click(Xpath, SPP_Gen_Save_Path);

		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Click(Xpath, SPP_Prog_Duedatecal_Path);
		Wait_ajax();
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath,
						Containspath("td", "class", "rcOutOfRange") + "//following::" + "span[text()='"
								+ Get_Pastdate("d") + "']"),
				"<Past Date - MM/DD/YYYY> from 'Calendar' icon is selectable ");
		Click(Xpath, Textpath("a", Get_Todaydate("d")));
		Wait_ajax();
		Click(Xpath, SPP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
		Type(Xpath, SPP_Prog_Comment_Path, "QA Test");
		softAssert.assertTrue(Get_Attribute(Xpath, SPP_Prog_Type_Path, "value").equalsIgnoreCase("Acknowledged"),
				"'Acknowledged' is not displays in 'Type' dropdown");
		// Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
		Click(Xpath, SPP_Prog_Attachment_Path);
		Fileupload(Pimco_TC_2_4_1_1_6);
		Wait_ajax();
		Click(Xpath, SPP_Prog_Submit_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Attachment1_Path);
		softAssert.assertTrue(Get_Text(Xpath, SPP_Prog_Createdate1_Path).contains(Get_Todaydate("M/d/YYYY")),
				"(MM/DD/YYYY HH:MM:SS AM/PM) is not displayed under 'Create Date' column for the comment added");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Prog_UserName1_Path).equalsIgnoreCase("qaauto"),
				" logged in user's (User name) is not displayed under 'User Name' column");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Prog_Comment1_Path).equalsIgnoreCase("QA Test"),
				"'QA Test' is not displayed under 'Comment' column");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Prog_Type1_Path).equalsIgnoreCase("Acknowledged"),
				"'Acknowledged' is not displayed under 'Type' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Prog_Attachment1_Path),
				" 'File' is not displayed under 'Attachment' column");
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Gen_Status_Path).trim().equalsIgnoreCase("Acknowledged"),
				"'Acknowledged' is not displayed by the 'Status' field on the header of the 'Progress/Files' tab");
		softAssert.assertAll();

		Click(Xpath, SPP_CostsTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Costs_Addcostitemicon_Path);
		Click(Xpath, SPP_Costs_Billedck_Path);
		Click(Xpath, SPP_Costs_Addcostitemicon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Costs_Insert_Path);
		Type(Xpath, SPP_Costs_Description_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, SPP_Costs_Type_Path, "Veritas Postage");
		Datepicker2(Xpath, SPP_Costs_Billdatecal_Path, Xpath, SPP_Costs_BilldateCal_Month_Path, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		Type(Xpath, SPP_Costs_Billammount_Path, "0.010");
		Click(Xpath, SPP_Costs_Insert_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Costs_Edit1_Path);
		softAssert.assertTrue(Get_Text(Xpath, SPP_Costs_Description1_Path).equalsIgnoreCase("QA Test"),
				"'QA Test' is not displayed under 'Description' column");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Costs_Type1_Path).equalsIgnoreCase("Veritas Postage"),
				"'Veritas Postage' is not displayed under 'Type' column");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Costs_Billdate1_Path).contains(Get_Futuredate("M/d/YYYY")),
				"(MM/DD/YYYY HH:MM:SS AM/PM) is not displayed under 'Bill Date' column for the cost item added");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Costs_Billamount1_Path).equalsIgnoreCase("$0.010"),
				"'$0.010'  is not displayed under 'Bill Amount' column");
		softAssert.assertAll();
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Costs Tab");
		Click(Xpath, SPP_InventoryTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Inventory_Addoprojbtn_Path);
		Type(Xpath, SPP_Inventory_Product_Code_Path, QA_Multifunction);
		Click(Xpath, SPP_Inventory_Product_suggestion_Path);
		// Click(Xpath, ".//strong[text()='FormNumber:']");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_Multifunction)),
				"QA_MultiFunction is not displayed in 'Product Code'  field");
		Click(Xpath, SPP_Inventory_Addoprojbtn_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Inventory_Delete1_Path);
		softAssert.assertTrue(Get_Text(Xpath, SPP_Inventory_Productcode1_Path).equalsIgnoreCase(QA_Multifunction),
				"'QA_Multifunction'  is not displayed under 'Product Code' column in 'Order for Project' grid");
		Type(Xpath, SPP_Inventory_Qty1_Path, "2");
		softAssert.assertAll();
		Click(Xpath, SPP_Gen_Save_Path);
		ExplicitWait_Element_Visible(Xpath, SPP_Gen_Successmsg_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Gen_Successmsg_Path),
				"'Data has been saved.' field is not displays  in 'General' tab on 'Special Projects' page - Programs Tab");
		softAssert.assertAll();
		Click(Xpath, SPP_HistoryTab_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_History_DateTime_Path),
				" 'Date/Time'  column not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_History_User_Path), " 'User'  column not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_History_Details_Path), " 'Details'  column not displays");
		softAssert.assertAll();
		Click(Xpath, SPP_ProgramTab_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
		Click(Xpath, SPP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
		Type(Xpath, SPP_Prog_Comment_Path, "'QA is validating cancel functionality");
		Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
		Wait_ajax();
		Click(Xpath, SPP_Prog_Submit_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, SPP_Gen_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displayed by the 'Status' field on the header of the 'Progress/Files' tab");

	}

	@Test(enabled = true, priority = 9)
	public void Pimco_TC_2_4_1_1_6() throws InterruptedException, IOException, AWTException {

		// Validate the combination of filter functionality

		SpecialProjectsNav();
		String Temp_Shortticketno = Get_Text(Xpath, SPP_TicketNo1_Path).trim().substring(0, 4);
		Type(Xpath, SPP_TicketNo_Path, Temp_Shortticketno);
		Click(Xpath, SPP_TicketNofilter_Path);
		Click(Xpath, Textpath("span", "Contains"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Type(Xpath, SPP_Jobtitle_Path, "QA");
		Click(Xpath, SPP_JobtitleFilter_Path);
		Click(Xpath, Textpath("span", "Contains"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Type(Xpath, SPP_Duedate_Path, Duedate);
		Click(Xpath, SPP_DuedateFilter_Path);
		Click(Xpath, Textpath("span", "EqualTo"));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().contains(Temp_Shortticketno),
				"(000X(Stage)/000Y(Prod)) ticket number is not displays under 'Ticket #' column");
		softAssert.assertTrue(Get_Text(Xpath, SPP_Jobtitle1_Path).trim().contains("QA"),
				" (QA) is not displays under 'Job Title' column");
		softAssert.assertFalse(Get_Text(Xpath, SPP_Duedate1_Path).trim().contains(Get_Todaydate("MM/dd/YYYY")),
				"<Due date>  is not displayed under  'Due date' column  in 'Special Projects : View Tickets' grid ");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);

	}

	@BeforeMethod
	public void beforeMethod() {

		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {

		for (String Cancelspecialproject : SplprjTicketNumberlist) {

			SpecialProjectsNav();
			Type(Xpath, SPP_TicketNo_Path, Cancelspecialproject);
			Click(Xpath, SPP_TicketNofilter_Path);
			Click(Xpath, Textpath("span", "Contains"));

			if (Element_Is_Displayed(Xpath, Textpath("div", "No Special Projects Available"))) {

			} else {

				Click(Xpath, ClickViewEdit(Cancelspecialproject));
				Click(Xpath, SPP_ProgramTab_Path);
				ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Duedatecal_Path);
				Click(Xpath, SPP_Prog_Addcommenticon_Path);
				ExplicitWait_Element_Clickable(Xpath, SPP_Prog_Submit_Path);
				Type(Xpath, SPP_Prog_Comment_Path, "'QA cancel");
				Select_lidropdown(SPP_Prog_Type_Path, Xpath, "Cancel");
				Wait_ajax();
				Click(Xpath, SPP_Prog_Submit_Path);
				Wait_ajax();

			}

		}

	}

}
