package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class ManageCostCenterTest extends BaseTestPimco {

	
	//Validate page opens and appropriate fields/tables are displaying
	@Test(priority =1)
	public void PIMCO_TC_2_4_8_1_1() throws InterruptedException{
	      ManageCostCenterNav();
		  Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Manage Cost Centers")),"Manage Cost Centers -  missing ");
		  
		  Assert.assertTrue(Element_Is_Displayed(ID, Pimco_MCC_CostCenter_txtbox_ID),"Next Button is missing ");
		  Assert.assertTrue(Element_Is_Displayed(ID, Pimco_MCC_CostCenter_FilterIcon_ID),"Next Button is missing ");

		  Assert.assertTrue(Element_Is_Displayed(ID, Pimco_MCC_Name_textbox_ID),"Next Button is missing ");
		  Assert.assertTrue(Element_Is_Displayed(ID, Pimco_MCC_Name_FilterIcon_ID),"Next Button is missing ");

		  Assert.assertTrue(Element_Is_Displayed(ID, Pimco_MCC_Active_Checkbox_ID),"Next Button is missing ");
		  Assert.assertTrue(Element_Is_Displayed(ID, Pimco_MCC_Active_Filter_ID),"Next Button is missing ");

		  
		  Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", " Add Cost Center"))," Add Cost Center-  missing ");
		  Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", " Refresh")),"  Refresh -   missing ");
		  
		  //TestCostcenter - Automation

		  
		  
	}
	

	@Test(priority =2, enabled = true)
	public void PIMCO_TC_2_4_8_1_2() throws InterruptedException, IOException, AWTException{
		ManageCostCenterNav();

		Click(Xpath, Textpath("a", " Add Cost Center"));
		ExplicitWait_Element_Clickable(ID, Pimco_MCC_CreateCostcenter_Btn_ID);
		Type(ID, Pimco_MCC_CostCenter_Textbox_ID, "TestCostcenter");
		Type(ID, Pimco_MCC_Name_TextBox_ID, "Automation");
		Click(ID, Pimco_MCC_Active_CheckBox_ID);
		Click(ID, Pimco_MCC_CreateCostcenter_Btn_ID);
		
		Click(Xpath, Textpath("a", "Logout"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");

	    Select_DropDown_VisibleText(ID, Pimco_SCP_CostCenter_Dropdown_ID, "TestCostcenter - Automation");
	    
	    Clearcarts();
	    
	   /* 
	    
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(PIMCO_Maillist_Allfields_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_SR_Search_Btn_ID);
		
		
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Multifunction));
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
		
		
		
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    Select_DropDown_VisibleText(ID, Pimco_SCP_CostCenter_Dropdown_ID, "TestCostcenter - Automation");
	    
	    Clearcarts();
	    
	    
	    
	    
		Click(ID, Pimco_LP_StartaBulkOrder_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(PIMCO_Maillist_Allfields_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_SR_Search_Btn_ID);
		
		
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Multifunction));
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
		
		
		
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    Select_DropDown_VisibleText(ID, Pimco_SCP_CostCenter_Dropdown_ID, "TestCostcenter - Automation");
	    
	    
	    */
	    Clearcarts();
	    	    
	    
	}
	
	
	@Test(priority =3)
	public void PIMCO_TC_2_4_8_1_6ANDPIMCO_TC_2_4_8_1_4() throws InterruptedException {
		
		ManageCostCenterNav();
		Type(ID, Pimco_MCC_CostCenter_txtbox_ID, "TestCostcenter");
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Wait_ajax();
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Click(Xpath, Textpath("span", "Contains"));
	    Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "TestCostcenter")), "TestCostcenter- Is not Listed in the table");
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Wait_ajax();
	    Click(Xpath, Textpath("span", "NoFilter"));
	    Wait_ajax();
	  	    
	    
		Type(ID, Pimco_MCC_Name_textbox_ID, "Automation");
	    Click(ID, Pimco_MCC_Name_FilterIcon_ID);
	    Wait_ajax();
	    Click(ID, Pimco_MCC_Name_FilterIcon_ID);
	    Click(Xpath, Textpath("span", "Contains"));
	    Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Automation")), "Automation- Is not Listed in the table");
	    
	    Click(Xpath,Editordeletebutton("TestCostcenter", "Edit"));
		ExplicitWait_Element_Clickable(ID, Pimco_MCC_ECC_CreateCostcenter_Btn_ID);
	    Click(ID, Pimco_MCC_ECC_Active_CheckBox_ID);
	    Click(ID, Pimco_MCC_ECC_CreateCostcenter_Btn_ID);
	    Wait_ajax();
	    Click(ID, Pimco_MCC_Name_FilterIcon_ID);
	    Wait_ajax();
	    Click(Xpath, Textpath("span", "NoFilter"));
	    Wait_ajax();
	    
	//    Click(ID, Pimco_MCC_Active_Checkbox_ID);
	    Click(ID, Pimco_MCC_Active_Filter_ID);
	    Click(Xpath, Textpath("span", "EqualTo"));
	    Wait_ajax();
	    
	    
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "TestCostcenter")), "TestCostcenter- Is not Listed in the table");
	    Click(ID, Pimco_MCC_Active_Filter_ID);
	    Click(Xpath, Textpath("span", "NoFilter"));
	    Wait_ajax();
	    
	    
	    	    
		
	}
	
	
	@Test(priority =4)
	public void PIMCO_TC_2_4_8_1_3ANDPIMCO_TC_2_4_8_1_5() throws InterruptedException{
		ManageCostCenterNav();
		Type(ID, Pimco_MCC_CostCenter_txtbox_ID, "TestCostcenter");
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Wait_ajax();
	    
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Click(Xpath, Textpath("span", "Contains"));
	    Wait_ajax();
	    
	    Click(Xpath,Editordeletebutton("TestCostcenter", "Edit"));
	  	ExplicitWait_Element_Clickable(ID, Pimco_MCC_ECC_CreateCostcenter_Btn_ID);
	  	Type(ID, Pimco_MCC_ECC_Name_TextBox_ID, "Test-12345");
	    Click(ID, Pimco_MCC_ECC_CreateCostcenter_Btn_ID);
	    Wait_ajax();
	    
	    ManageCostCenterNav();
	    Click(ID, Pimco_MCC_Name_FilterIcon_ID);
	    Wait_ajax();
	    
	 	
		Type(ID, Pimco_MCC_Name_textbox_ID, "Test-12345");
	    Click(ID, Pimco_MCC_Name_FilterIcon_ID);
	    Wait_ajax();
	    Click(ID, Pimco_MCC_Name_FilterIcon_ID);
	    Click(Xpath, Textpath("span", "Contains"));
	    Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Test-12345")), "Test-12345- Is not Listed in the table");
	    Click(Xpath,Editordeletebutton("TestCostcenter", "Delete"));
		  
	}
	
	
	@Test(priority =5)
	public void PIMCO_TC_2_4_8_1_7() throws InterruptedException {
		
		ManageCostCenterNav();
		
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("1")),"Page 1 - not hightligted");
	    	    
	    Click(Xpath, Textpath("span", "2"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("2")),"Page 2 - not hightligted");

	    Click(Xpath, "//input[@class='rgPageNext']");
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("3")),"Page 3 - not hightligted");

	    Click(Xpath, "//input[@class='rgPagePrev']");
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("2")),"Page 2 - not hightligted");

	    
		Type(ID, Pimco_MCC_Pagenumber_ID, "1");
		Click(ID, Pimco_MCC_GoBtn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("1")),"Page 1 - not hightligted");

		Type(ID, Pimco_MCC_Pagenumber_ID, "2");
		Click(ID, Pimco_MCC_GoBtn_ID);
	    Assert.assertTrue(Element_Is_Displayed(Xpath,Pagination("2")),"Page 2 - not hightligted");


	}
	
	
	
	
	
	@Test(priority =6)
	public void PIMCO_TC_2_4_8_1_9() throws IOException, InterruptedException{
		
		logout();
		login("qaauto1", "qaauto1");
		Click(Xpath, Textpath("a", " Add Cost Center"));
		ExplicitWait_Element_Clickable(ID, Pimco_MCC_CreateCostcenter_Btn_ID);
		Type(ID, Pimco_MCC_CostCenter_Textbox_ID, "TestCostcenter1");
		Type(ID, Pimco_MCC_Name_TextBox_ID, "Automation");
		Click(ID, Pimco_MCC_Active_CheckBox_ID);
		Click(ID, Pimco_MCC_CreateCostcenter_Btn_ID);
		Wait_ajax();
		Click(Xpath, Textpath("a", "Logout"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		ManageCostCenterNav();
		Type(ID, Pimco_MCC_CostCenter_txtbox_ID, "TestCostcenter1");
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Wait_ajax();
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Click(Xpath, Textpath("span", "Contains"));
	    Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "TestCostcenter1")), "TestCostcenter- Is not Listed in the table");
	    Click(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
	    Wait_ajax();
	    Click(Xpath, Textpath("span", "NoFilter"));
	    Wait_ajax();
	}

	
	
	
	
	public void ManageCostCenterNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Cost Centers"));
		ExplicitWait_Element_Clickable(ID, Pimco_MCC_CostCenter_FilterIcon_ID);
		Wait_ajax();
		

	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
	}
}
