package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class NewFormsandMaterials extends BaseTestPimco {

	public static String Pimco_TC_2_5_2_2_1_Piece = "QA_NewTestPart_AT_8";
	public static int Pimco_TC_2_5_2_2_1_Piece1;

	public void ManageinventoryNav() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

	}

	public static String NewFormsandMaterials_qty(String PieceName) {

		return ".//a[contains(text(),'" + PieceName + "')]//following::input[1][contains(@id,'txtQuantity1')]";

	}

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_5_2_1_1() throws InterruptedException, IOException {

		/*
		 * Validating number of new forms and materials items that appear in widget
		 */

		Click(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Selectrecipient_Path));
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Form5_Path)),
				"(5) pieces are not displayed in 'New Forms and Materials' widget");
	}

	@Test(priority = 2, enabled = true)
	public void Pimco_TC_2_5_2_2_1() throws InterruptedException, IOException {

		/*
		 * Verify New piece displays under New Forms and Materials widget
		 * */

		// Pre-Req

		ManageinventoryNav();
		Wait_ajax();
		Pimco_TC_2_5_2_2_1_Piece = "QA_NewTestPart_AT_1";
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_5_2_2_1_Piece);
		Select_lidropdown(Pimco_IM_StatusDrowpdown_Arrow_ID, ID, "- Any -");
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		while (Element_Is_Displayed(Xpath, Pimco_IM_SearchResults_Firstresult_Path)) {

			Pimco_TC_2_5_2_2_1_Piece1 = Integer
					.parseInt(Pimco_TC_2_5_2_2_1_Piece.substring(18, Pimco_TC_2_5_2_2_1_Piece.length())) + 1;
			Pimco_TC_2_5_2_2_1_Piece = "QA_NewTestPart_AT_" + Pimco_TC_2_5_2_2_1_Piece1;
			Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_5_2_2_1_Piece);
			Click(ID, Pimco_IM_Search_Btn_ID);
			Wait_ajax();
		}

		Click(ID, Pimco_IM_CreateNewtype_Arrow_ID);
		Click(Xpath, li_value("POD"));
		Click(ID, Pimco_IM_CreateNewtype_Add_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Text(ID, Pimco_IM_PageHeader_ID).trim().equalsIgnoreCase("New Item"),
				"The Manage Inventory Item: New Item page not opens");
		Type(ID, Pimco_IM_ProductCode_ID, Pimco_TC_2_5_2_2_1_Piece);
		Type(ID, Pimco_IM_Title_ID, "QA TEST");
		Type(ID, Pimco_IM_AsofDate_Txtbox_ID, Get_Todaydate("0" + "MM/dd/YYYY"));
		Select_lidropdown(Pimco_IM_ddlDocumentType_Arrow_ID, ID, "Form");
		Select_lidropdown(Pimco_IM_ddlFormOwner_Arrow_ID, ID, "Marketing");
		Select_lidropdown(Pimco_IM_DD_Viewability_Arrow_ID, ID, "Orderable");
		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Reporter.log("Create piece name is " + Pimco_TC_2_5_2_2_1_Piece);
		System.out.println("Create piece name is " + Pimco_TC_2_5_2_2_1_Piece);
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		Click(Xpath, Pimco_IM_Pricingtab_Path);
		Click(Xpath, Pimco_IM_PT_IsPrintCostApproved_Path);
		Type(ID, Pimco_IM_PricePrintProduction_ID, "0.01");
		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		Click(ID, Pimco_LP_Logout_ID);
		login(PIMCOBothUsername, PIMCOBothPassword);
		Click(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Selectrecipient_Path));
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", Pimco_TC_2_5_2_2_1_Piece)),
				" 'QA_NewTestPart_(X)' piece is not displayed in 'New Forms and Materials' widget");
	}

	@Test(priority = 3, enabled = true, dependsOnMethods = "Pimco_TC_2_5_2_2_1")
	public void Pimco_TC_2_5_2_2_2() throws InterruptedException, IOException {

		// Place a new form and material order

		Type(Xpath, NewFormsandMaterials_qty(Pimco_TC_2_5_2_2_1_Piece), "1");
		Click(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Checkout_Path));
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("h1", "Checkout")),
				"Checkout page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Pimco_TC_2_5_2_2_1_Piece)),
				" QA_NewTestPart_(X) piece is displayed in 'Checkout' page");
		Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		Wait_ajax();
		Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		Click(ID, Pimco_SCP_Next_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece order");
		Click(ID, Pimco_SCP_Checkout_ID);
		Get_Orderno();

	}

	@Test(priority = 4, enabled = true)
	public void Pimco_TC_2_5_2_2_3() throws InterruptedException, IOException {

		// Firm Restriction for New piece

		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_5_2_2_1_Piece);
		Select_lidropdown(Pimco_IM_StatusDrowpdown_Arrow_ID, ID, "- Any -");
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(Pimco_TC_2_5_2_2_1_Piece, "Edit"));
		Wait_ajax();
		Click(Xpath, Pimco_IM_Rulestab_Path);
		Select_DropDown_VisibleText(Xpath, Pimco_IM_RT_PreApprovalFirm_Path, "AMERIPRISE GROUP");
		Type(Xpath, Pimco_IM_RT_FirmApprovalCode_Path, "1234");
		Type(Xpath, Pimco_IM_RT_FirmApprovalExpirationDate_Path, Get_Futuredate("M/d/YYYY"));
		Click(Xpath, Pimco_IM_RT_AddIncludeRule_Path);
		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		Click(ID, Pimco_LP_Logout_ID);
		login(PIMCOBothUsername, PIMCOBothPassword);
		Click(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Selectrecipient_Path));
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("*", Pimco_TC_2_5_2_2_1_Piece)),
				" 'QA_NewTestPart_(X)' piece is displayed in 'New Forms and Materials' widget - No firm");
		Clearcarts();
		Click(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Selectrecipient_Path));
		Wait_ajax();
		ContactClick("QA_AG", "IL");
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("*", Pimco_TC_2_5_2_2_1_Piece)),
				" 'QA_NewTestPart_(X)' piece is not displayed in 'New Forms and Materials' widget");
		Clearcarts();
		Click(Xpath, Wid_Xpath("New Forms and Materials", NFMWidget_Selectrecipient_Path));
		Wait_ajax();
		ContactClick("QA_AXA", "IL");
		Wait_ajax();
		softAssert.assertFalse(Element_Is_Displayed(Xpath, Containstextpath("*", Pimco_TC_2_5_2_2_1_Piece)),
				" 'QA_NewTestPart_(X)' piece is displayed in 'New Forms and Materials' widget - AXA Firm");
		softAssert.assertAll();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBothUsername, PIMCOBothPassword);
		Clearcarts();
		AddWidget("New Forms and Materials");

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
