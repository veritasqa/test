package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingSearchbackOrderHolidayTest extends BaseTestPimco {
	
	//Validate search for part that allows backorders
	@Test(enabled = true , priority= 1)
	public void Pimco_TC_2_6_5_1ANDPimco_TC_2_6_5_2() throws InterruptedException, AWTException{
		
	    ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
			Click(ID, Pimco_LP_StartaMailing_ID);
			ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
		
			Thread.sleep(6000);
			Click(ID, Pimco_MLU_Select_File_ID);
			
			Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
			Wait_ajax();
			Click(ID,Pimco_MLU_UploadMailList_ID);
			Wait_ajax();
			Click(ID,Pimco_MLU_Ok_Btn_ID);
			
			Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Backordertest);
			Click(ID, Pimco_SR_Search_Btn_ID);
			
			Click(Xpath, CalenderbtnNexttoPiece(QA_Backordertest));
			
			//to check holiday is disabled
			
			Click(ID, "ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_rdpDripDate_calendar_Title");
			Click(Xpath, ".//a[text()='Dec']");
			ExplicitWait_Element_Clickable(Xpath, ".//a[text()='2018']");
			Thread.sleep(2000);
			Click(Xpath, ".//a[text()='2018']");
			Thread.sleep(2000);
			ExplicitWait_Element_Clickable(ID, Pimco_SCP_Calendar_OK_Btn_ID);
			Click(ID, Pimco_SCP_Calendar_OK_Btn_ID);
			Thread.sleep(2000);
			Assert.assertTrue(Element_Is_Displayed(Xpath, ".//td[@title='Holiday']//span[text()='25']"), "User is  able to select  holiday date 25 December ");
			Click(Xpath, ".//a[text()='24']");
			Thread.sleep(2000);
			Clearcarts();
		
	}
	
	
	//Validate a part that doesn't allow backorders
	@Test(enabled = true , priority= 1)
	public void Pimco_TC_2_6_5_3() throws InterruptedException, AWTException{
				ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
				Click(ID, Pimco_LP_StartaMailing_ID);
				ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
			
				Thread.sleep(6000);
				Click(ID, Pimco_MLU_Select_File_ID);
				
				Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
				Wait_ajax();
				Click(ID,Pimco_MLU_UploadMailList_ID);
				Wait_ajax();
				Click(ID,Pimco_MLU_Ok_Btn_ID);
				
				Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_AllowBackOrder);
				Click(ID, Pimco_SR_Search_Btn_ID);
				
				Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("*", "Out of Stock")),"Out of stock not displayed on search results page");
				Clearcarts();
	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
		
	//	Clearcarts();
	}

}
