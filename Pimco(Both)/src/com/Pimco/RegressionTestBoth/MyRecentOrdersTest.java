 package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MyRecentOrdersTest extends BaseTestPimco {
	
	@Test(enabled = true, priority = 1 )
	public void Pimco_TC_2_5_5_1_1() throws InterruptedException{
		

		FulfilmentSearch(QA_Flyer);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_Flyer"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_Flyer"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
				
	
	}
	
	
	
	@Test(enabled = true, priority = 2 )
	public void Pimco_TC_2_5_5_1_2() throws InterruptedException{
		
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,RecentorderStatus(OrderNumber)),"OrderNumber status is not New");
		
		Click(Xpath,RecentorderButtonpath(OrderNumber, "Copy"));
		Wait_ajax();
		ExplicitWait_Element_Clickable(ID, Pimco_CSP_Search_Btn_ID);
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("span", "Please select a contact to copy the order for.")),"Please select a contact to copy the order for. - Message not displayed");
		
		ContactClick("QanoFirm", "IL");
		Clearcarts();
		
				
	}
	
	@Test(enabled = true, priority = 3 )
	public void Pimco_TC_2_5_5_1_3() throws InterruptedException{
		FulfilmentSearch(QA_FirmRestriction);
		Wait_ajax();
		ContactClick("QAamripri", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_FirmRestriction1"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_FirmRestriction1"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,RecentorderStatus(OrderNumber)),"OrderNumber status is not New");
		
		Click(Xpath,RecentorderButtonpath(OrderNumber, "Copy"));
		Wait_ajax();
		
		ContactClick("QAaxa", "IL");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("div", "No items in the cart")),"No items in the cart- Message is missing");
		
		//QA_FirmRestriction1
		
		
	}
	
	
	@Test(enabled = true, priority = 4 )
	public void Pimco_TC_2_5_5_1_4() throws InterruptedException{
		
		for(int i=1;i<=5;i++){
			

			FulfilmentSearch(QA_Flyer);
			Wait_ajax();
			ContactClick("QanoFirm", "IL");
			Wait_ajax();
			Click(Xpath, AddToCartbtnXpath("QA_Flyer"));
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_Flyer"));
		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		    Click(Xpath, Textpath("span", "Checkout (1)"));
		    Wait_ajax();
		    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		    Wait_ajax();
		    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		    
		    Click(ID, Pimco_SCP_Next_Btn_ID);
		    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		    Click(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);
		    
		    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
		    
		    Click(ID, Pimco_SCP_Checkout_ID);
		    
		    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		    
		    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
			OrderNumberlist.add(OrderNumber);
			RecentOrderNumberlist.add(OrderNumber);
			Reporter.log("Order Number is " + OrderNumber);
			System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
			Click(ID, Pimco_OC_OK_Btn_ID);
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
			Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		}
		
		
		for (String Onum : RecentOrderNumberlist){
			
			Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("td", Onum)),Onum + " - is not displayed");
			
		}
	}
	
	@BeforeClass
	public void Widgetverify() throws InterruptedException{
	if(!Element_Is_Displayed(Xpath, WidgetNamePath("My Recent Orders"))){
		Hover(Xpath, Textpath("h3", "Add Widgets"));
		Wait_ajax();
		Click(Xpath, Textpath("a","My Recent Orders"));
	}
	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}

	
}
