package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class Materials extends BaseTestPimco {

	public void ManageinventoryNav() throws InterruptedException {

		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

	}

	public void Materialsnav(String Select_Option) throws InterruptedException {

		Hover(Xpath, Textpath("span", "Materials"));
		Click(Xpath, Textpath("span", Select_Option));
		Wait_ajax();

	}

	@Test(enabled = true, priority = 1)
	public void Pimco_TC_2_2_1_1() throws InterruptedException, IOException {

		// Edit Part categories and validate category materials dropdown should
		// appear

		// Pre - Req
		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_Multifunction, "Edit"));
		Wait_ajax();
		if (Element_Is_selected(Xpath, CatagoriesCheckbox("QA Test Materials"))) {

		} else {
			Click(Xpath, CatagoriesCheckbox("QA Test Materials"));
			Click(ID, Pimco_IM_Save_Btn_ID);
			Wait_ajax();
			Click(ID, Pimco_IM_PMSG_OK_Button_ID);
			Wait_ajax();

		}
		logout();

		// Test cases starts here

		login(PIMCOBothUsername, PIMCOBothPassword);
		Hover(Xpath, LP_Materials_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "QA Test Materials")),
				"'QA Test Materials' link is not displayed under 'Materials' ");

	}

	@Test(enabled = true, priority = 2)
	public void Pimco_TC_2_2_1_2() throws InterruptedException, IOException {

		// Validate part appears in product search with appropriate header on
		// Materials page

		Materialsnav("QA Test Materials");
		ContactClick("QanoFirm", "IL");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SR_Materials_Path), "'Materials' page is not displayed");
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_SR_Matchingitems_Materials_Path).trim()
						.equalsIgnoreCase("Matching item(s) for: Category of 'QA Test Materials'"),
				"'Matching item(s) for: Category of 'QA Test Materials'' is not displayed on the header bar");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Multifunction)),
				" 'QA_Multifunction' is not displayed in 'Materials' page");
		Click(Xpath, Pimco_LP_ClearCart_Path);
		Wait_ajax();

	}

	@Test(enabled = true, priority = 3)
	public void Pimco_TC_2_2_1_3() throws InterruptedException, IOException {

		// Edit Part categories and validate category materials dropdown
		// shouldn't appear

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_Multifunction, "Edit"));
		Wait_ajax();
		if (Element_Is_selected(Xpath, CheckBoxXpath("QA Test Materials"))) {
			Click(Xpath, CheckBoxXpath("QA Test Materials"));
			Click(ID, Pimco_IM_Save_Btn_ID);
			Wait_ajax();
			Click(ID, Pimco_IM_PMSG_OK_Button_ID);
			Wait_ajax();
		}
		logout();
		login(PIMCOBothUsername, PIMCOBothPassword);
		Hover(Xpath, LP_Materials_path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("span", "QA Test Materials")),
				"'QA Test Materials' link is displayed under 'Materials' ");

	}

	@Test(enabled = true, priority = 4)
	public void Pimco_TC_2_2_2_1() throws InterruptedException, IOException {

		// Validate Hyphens entered into the search field and in Materials page
		// will be ignored to display the pieces

		FulfilmentSearch("QA-Hyphen-TestGWM");
		ContactClick("QanoFirm", "IL");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Hyphen_TestGWM)),
				QA_Hyphen_TestGWM + " is not displayed in 'Materials' page - Fulfilment Search");
		SearchbyMaterials("QA-Hyphen");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_Hyphen_TestGWM)),
				QA_Hyphen_TestGWM + " is not displayed in 'Materials' page - Searched in Search by Material Page");
	}

	@Test(enabled = true, priority = 5)
	public void Pimco_TC_2_2_2_2() throws InterruptedException, IOException {

		// Validate "Exclude Regulatory" and "Exclude User Kits" filters for
		// Pimco (Both) Admin
		// Pre- Req

		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_userkittest);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(QA_userkittest, "Edit"));
		Wait_ajax();
		Select_lidropdown(Pimco_IM_ddlInventoryType_Arrow_ID, ID, "User Kit");
		if (Element_Is_selected(Xpath, CatagoriesCheckbox("Regulatory"))) {

		} else {
			Click(Xpath, CatagoriesCheckbox("Regulatory"));
			Click(ID, Pimco_IM_Save_Btn_ID);
			Wait_ajax();
			Click(ID, Pimco_IM_PMSG_OK_Button_ID);
			Wait_ajax();

		}
		Click(Xpath, Pimco_IM_Pricingtab_Path);
		if (Element_Is_selected(Xpath, Pimco_IM_PT_IsPrintCostApproved_Path)) {

		} else {
			Click(Xpath, Pimco_IM_PT_IsPrintCostApproved_Path);
			Click(ID, Pimco_IM_Save_Btn_ID);
			Wait_ajax();
			Click(ID, Pimco_IM_PMSG_OK_Button_ID);
			Wait_ajax();

		}
		FulfilmentSearch(QA_userkittest);
		ContactClick("QanoFirm", "IL");
		Assert.assertTrue(Element_Is_Displayed(Xpath, SearchResultsPiceNameXpath(QA_userkittest)),
				QA_userkittest + " is not displayed in 'Materials' page - Fulfilment Search");
		Click(Xpath, Pimco_SR_AdvancedFilter_Path);
		Click(Xpath, Pimco_SR_ExcludeRegulatoryck_Path);
		Click(Xpath, Pimco_SR_ExcludeUserKitsck_Path);
		Click(Xpath, Pimco_SR_SearchbyMaterials_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Pimco_SR_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Pimco_SR_NoSearchresults_msg_Path)),
				Pimco_SR_NoSearchresults_msg_Path + " message is not displayed");

	}

	@Test(enabled = true, priority = 6)
	public void Pimco_TC_2_2_2_3() throws InterruptedException, IOException {

		// Validate loading panel displays when navigating pages on material
		// search page in Pimco (Both) Admin

		FulfilmentSearch("test");
		ContactClick("QanoFirm", "IL");
		Click(Xpath, Textpath("span", "2"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Loading...")),
				"'Loading...' pop up window is not displayed");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Loading..."));
		Click(Xpath, Containspath("input", "title", "Next Page"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Loading...")),
				"'Loading...' pop up window is not displayed - Next Page");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Loading..."));
		Click(Xpath, Containspath("input", "title", "Last Page"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Loading...")),
				"'Loading...' pop up window is not displayed - Last Page");
		ExplicitWait_Element_Not_Visible(Xpath, Containstextpath("div", "Loading..."));
		Click(Xpath, Containspath("input", "title", "First Page"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("div", "Loading...")),
				"'Loading...' pop up window is not displayed - First Page");
		Wait_ajax();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBoth1Username, PIMCOBoth1Password);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();
		Clearcarts();

	}

	@AfterMethod(enabled = true)
	public void afterMethod() throws InterruptedException {
		Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
