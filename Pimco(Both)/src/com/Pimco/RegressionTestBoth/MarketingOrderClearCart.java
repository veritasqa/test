package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingOrderClearCart extends BaseTestPimco{
	
	
	//Validate part is removed on clicking Clear Cart link from header bar on Materials page
	@Test(enabled = true)
	public void Pimco_TC_2_6_1_4_1() throws InterruptedException, AWTException {
	     
		Clearcarts();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Marketingonly);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Marketingonly));
		 Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    Thread.sleep(3000);
	    Click(Xpath, Textpath("span", "Clear Cart"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout Button is missing ");
	   
    	Wait_ajax();
    	Thread.sleep(3000);
    	
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");

	}
	
	
	//Validate on the check out page that the remove button removes part from cart
	@Test(enabled = true)
	public void Pimco_TC_2_6_1_4_2() throws InterruptedException, AWTException{
		Clearcarts();

	     ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Marketingonly);
		Click(ID, Pimco_SR_Search_Btn_ID);
	  	Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Marketingonly));
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath(" Marketing piece"));

		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
		    Click(Xpath, Textpath("span", "Checkout (1)"));
		    Wait_ajax();
		    
		    Wait_ajax();
		//    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		    Click(ID,Pimco_SCP_Remove_Btn_ID);
		    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div","Item(s) have been removed from your cart.")),"Item(s) have been removed from your cart.- Message not found");
		
		    Click(Xpath, Textpath("span", "Clear Cart"));
	    	Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");
		
	}
	
 //Validate when you click the clear cart button on the checkout page the part is removed
	@Test(enabled = true)
	public void Pimco_TC_2_6_1_4_3() throws InterruptedException, AWTException{
		Clearcarts();

	     ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Marketingonly);
		Click(ID, Pimco_SR_Search_Btn_ID);
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Marketingonly));
		 Wait_ajax();
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
		
	    Click(ID, Pimco_SCP_ClearCart_Btn_ID);
	    Wait_ajax();
	    Thread.sleep(6000);
	    ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");
	
	}
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
	
}
