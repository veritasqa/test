package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class OrderLookupTest extends BaseTestPimco {

	//Search by date selection
	@Test(priority = 2)
	public void  Pimco_TC_2_5_3_1_1() throws InterruptedException{
		
		Type(Xpath, Pimco_LP_OL_FromDate_TxtBox_path, Get_Pastdate("m/d/YYYY"));
		
		Type(Xpath, Pimco_LP_OL_FromDate_TxtBox_path, Get_Todaydate("m/d/YYYY"));
		
		Click(Xpath, Pimco_LP_OL_Search_Btn_path);
		
		Wait_ajax();
		
		ExplicitWait_Element_Clickable(ID, Pimco_LP_OS_SearchBtn_ID);
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,Textpath("td", OrderNumber)),"the (Order #) from Pre-condition is displayed in 'Order Search' grid");
		
	    Click(Xpath, Textpath("span", "Home"));
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout")),"Checkout link on top right side of the page is  Not grayed out");

	}
	
	@Test(priority = 3)
	public void  Pimco_TC_2_5_3_1_2() throws InterruptedException{
		
	Type(Xpath, Pimco_LP_OL_OrderNumber_TxtBox_path, OrderNumber);
		
		Wait_ajax();
		
		ExplicitWait_Element_Visible(Xpath,Textpath("li", OrderNumber));

		Click(Xpath,Textpath("li", OrderNumber));
		
		
		Click(Xpath, Pimco_LP_OL_View_Btn_path);
		
		
		ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		    
		Assert.assertTrue(Get_Text(ID, Pimco_OC_OrderNumber_ID).equalsIgnoreCase(OrderNumber),"OrderNumber Mismatch");
		
		
	}
	
	//Copy by order number and able to place an order
	@Test(priority = 4)
	public void  Pimco_TC_2_5_3_1_3() throws InterruptedException{
		
		Type(Xpath, Pimco_LP_OL_OrderNumber_TxtBox_path, OrderNumber);
		
		Wait_ajax();
		
		ExplicitWait_Element_Visible(Xpath,Textpath("li", OrderNumber));

		Click(Xpath,Textpath("li", OrderNumber));
		
		Click(Xpath, Pimco_LP_OL_Copy_Btn_path);
		
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		
		  ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
		    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
		    Wait_ajax();
		    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
		    
		    Click(ID, Pimco_SCP_Next_Btn_ID);
		    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

		    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    
		    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
		    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

		    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
		    
		    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
		    
		    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
		    
		    Click(ID, Pimco_SCP_Checkout_ID);
		    
		    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
		    
		    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
			OrderNumberlist.add(OrderNumber);
			Reporter.log("Order Number is " + OrderNumber);
			System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
			Click(ID, Pimco_OC_OK_Btn_ID);
			Wait_ajax();
			ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
			
			Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
			
		
		
	}
	
	@Test(priority = 1)
	public void OrderLookupPrecondition() throws InterruptedException{
		

		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		System.out.println("Order Number placed in method  is --" + OrderNumber + "'");
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
	}
	
	@BeforeClass
	public void Widgetverify() throws InterruptedException{
	if(!Element_Is_Displayed(Xpath, WidgetNamePath("Order Lookup"))){
		Hover(Xpath, Textpath("h3", "Add Widgets"));
		Wait_ajax();
		Click(Xpath, Textpath("a","Order Lookup"));
	}
	}

	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
}
