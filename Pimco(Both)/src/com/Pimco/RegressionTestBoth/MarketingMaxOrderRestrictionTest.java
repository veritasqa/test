package com.Pimco.RegressionTestBoth;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class MarketingMaxOrderRestrictionTest extends BaseTestPimco{

	
	// Validate max order quantity: Pimco Admin
	@Test(enabled = true)
	public void Pimco_TC_2_6_4_1() throws InterruptedException, AWTException{
    	ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
	
		Click(ID, Pimco_LP_StartaMailing_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_MLU_UploadMailList_ID);
	
		Thread.sleep(6000);
		Click(ID, Pimco_MLU_Select_File_ID);
		
		Fileupload(PIMCO_MailListUpload_Sample_Path);
		Wait_ajax();
		Click(ID,Pimco_MLU_UploadMailList_ID);
		Wait_ajax();
		Click(ID,Pimco_MLU_Ok_Btn_ID);
		
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Maxorder);
		Click(ID, Pimco_SR_Search_Btn_ID);
		
		
		Click(Xpath, SearchResultsAddtoDripBtnXpath(QA_Maxorder));
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_Maxorder"));
		
		
		
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "There are items over max order qty in your cart.  If you proceed with your order, those items will be removed from the order.")),"There are items over max order qty in your cart.  If you proceed with your order, those items will be removed from the order.- Message not displayed");
	    
	    Click(Xpath, Textpath("a", "Remove items Over Max Order Qty"));
	    
	    
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Removed items over the max qty")),"Removed items over the max qty - Message not displayed ");

	    Clearcarts();
	    

	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
	}
}
