package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.Pimco.Base.BaseTestPimco;

public class FulFilmentStandardOrder extends BaseTestPimco{

	//Add a part to cart from the fulfillment search product page and complete order
	
	@Test(enabled = true)
	public void Pimco_TC_2_8_1_1_1() throws InterruptedException{
		
		FulfilmentSearch(QA_Multifunction);
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	    Click(Xpath, Textpath("span", "Checkout (1)"));
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	    Type(ID, Pimco_SCP_RecipientNotes_ID, "This is a QA piece order");
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
	}
	
	//Add a part to cart from the materials dropdown and complete order
	
	@Test
	public void Pimco_TC_2_8_1_1_2ANDPimco_TC_2_8_1_1_3() throws InterruptedException {
		
		Hover(Xpath, Textpath("span", "Materials"));
		ExplicitWait_Element_Visible(Xpath, Textpath("span", "Regulatory"));
		Click(Xpath, Textpath("span", "Regulatory"));
		Wait_ajax();
		ContactClick("QanoFirm", "IL");
		Wait_ajax();		
		ExplicitWait_Element_Clickable(ID, Pimco_SR_Search_Btn_ID);
		Type(ID, Pimco_SR_SearchMaterialsBy_TxtBox_ID, QA_Multifunction);
		
		Click(ID, Pimco_SR_Search_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Thread.sleep(6000);
		Click(Xpath, AddToCartbtnXpath("QA_MULTIFUNCTION_TITLE"));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath("QA_MULTIFUNCTION_TITLE"));
	    Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Checkout (1)")), "(1) is displaying next to 'Checkout' link");
	
	    Click(ID, Pimco_SR_CheckOut_Btn_ID);
	    
	    Wait_ajax();
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_Next_Btn_ID);
	    Click(ID, Pimco_SCP_OrderConfirmation_Checkbox_ID);
	    Wait_ajax();
	    Click(ID, Pimco_SCP_ShippedConfirmation_Checkbox_ID);
	    
	    Click(ID, Pimco_SCP_Next_Btn_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_SCP_ShippingLowCostMethod_Checkbox_ID);

	    Click(ID, Pimco_SCP_DeliveryDate_CheckBox_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    
	    Click(ID, Pimco_SCP_DeliveryDate_popupButton_ID);
	    ExplicitWait_Element_Visible(ID, Pimco_SCP_Calendar_Title_ID);

	    Datepicker(Pimco_SCP_Calendar_Title_ID, Pimco_SCP_Calendar_OK_Btn_ID);
	    
	    Assert.assertTrue(Get_DropDown(ID, Pimco_SCP_CostCenter_Dropdown_ID).equalsIgnoreCase("9999 - Veritas Cost Center"),"9999- Veritas Cost Center value is NOT pre-populated in 'Cost Center' drop down");
	    
	
	    
	    Click(ID, Pimco_SCP_Checkout_ID);
	    
	    ExplicitWait_Element_Clickable(ID, Pimco_OC_OK_Btn_ID);
	    
	    OrderNumber = Get_Text(ID, Pimco_OC_OrderNumber_ID).trim();
		OrderNumberlist.add(OrderNumber);
		Reporter.log("Order Number is " + OrderNumber);
		
		//Pimco.TC.2.8.1.1.3
		//Validate that order confirmation page appears
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Order Information")),"Order Information grid  is missing");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Order Details")),"Order Details grid  is missing");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_Status_ID).trim().equalsIgnoreCase("NEW"),"NEW displays by 'Status' field");
	
		Assert.assertTrue(Get_Text(ID, Pimco_OC_HoldForComplete_ID).trim().equalsIgnoreCase("No"),"No is not displayed by 'Hold For Complete' field");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_OrderDate_ID).trim().equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),"Today's Date: MM/DD/YYYY missing for 'Order Date' field");

		Assert.assertTrue(Get_Text(ID, Pimco_OC_CostCenter_ID).trim().equalsIgnoreCase("9999"),"9999 is not displayed by 'Billed to Cost Center' field");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_ShippedBy_ID).trim().equalsIgnoreCase("UPS Ground"),"UPS Ground  not displays by 'Shipped By' field ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("b", "QanoFirm IL")),"QanoFirm IL not displays by 'Shipped To'  field ");
		
		Assert.assertTrue(Get_Text(ID, Pimco_OC_TotalChargebackCost).trim().equalsIgnoreCase("$0.00"),"$0.00 is not displayed by 'Total Chargeback Cost' field");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Submit Cancel Request")),"Submit Cancel Request button missing in 'Order Details' grid");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "QA_Multifunction")),"QA_Multifunction displays under 'Product Code' column");
		
		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "QA_MULTIFUNCTION_TITLE")),"QA_MULTIFUNCTION_TITLE displays  under 'Title' column");

		Assert.assertTrue(Element_Is_Displayed(Xpath,TextpathContains("td", "(1/0)")),"(1/0)) displays under 'Quantity(Available/BackOrder)' column");

		Assert.assertTrue(Element_Is_Displayed(ID,Pimco_OC_Copy_Btn_ID),"Copy button Missing");
		Assert.assertTrue(Element_Is_Displayed(ID,Pimco_OC_OK_Btn_ID),"Ok button Missing");

		//b[text()='QanoFirm IL']
		//div[@id="cphContent_cphSection_divAddress"][contains(.,'913 Commerce Ct')]
		
		Click(ID, Pimco_OC_OK_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("em", "Announcements"));
		
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("em", "Announcements")),"Home page NOT displays");
		
		
	}
	
	//Create an expedite order and verify order goes on Hold
	
	@Test
	public void Pimco_TC_2_8_1_1_5(){
		
	}
	
	
	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		//System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);
		
	}
	
}
