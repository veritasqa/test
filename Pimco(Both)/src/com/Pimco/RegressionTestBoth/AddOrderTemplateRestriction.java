package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class AddOrderTemplateRestriction extends BaseTestPimco {

	@Test(priority = 1, enabled = true)
	public void Pimco_TC_2_5_8_1_3() throws InterruptedException {

		// Firm Restrictions - Include

		// Pre-Req

		CreateOrderTemplate(QA_OTFirmRestrictionIN, "OT Firm Restriction IN", "Qa_AXA_Firm", "Automation");
		Clearcarts();
		Click(Xpath, Ordertemplate_selectrecipient);
		ContactClick("Qa_AXAGroup", "Automation");
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order template widget");
		Click(Xpath, Ordertemplate_View1_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, Textpath("span", QA_OTFirmRestrictionIN)),
				"Order Template 'QA Test' with part 'QA_OTFirmRestrictionIN' is displayed in 'Order Templates' page");
		Clearcarts();
		Click(Xpath, Ordertemplate_selectrecipient);
		ContactClick("Qa_AXA_Firm", "Automation");
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order template widget");
		Click(Xpath, Ordertemplate_View1_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", QA_OTFirmRestrictionIN)),
				"Order Template 'QA Test' with part 'QA_OTFirmRestrictionIN' is not displayed in 'Order Templates' page");
		Click(Xpath, Containspath("input", "value", "Add To Cart"));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", PBB001_40827)),
				QA_OTFirmRestrictionIN + " not displayed in the checkout page");
	}

	public void CreateOrderTemplate(String Piece, String Piece_Title, String Contact_Fname, String Contact_Lname)
			throws InterruptedException {

		FulfilmentSearch(Piece);
		Wait_ajax();
		ContactClick(Contact_Fname, Contact_Lname);
		Wait_ajax();
		Click(Xpath, AddToCartbtnXpath(Piece_Title));
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, SuccessfullyaddtoCartXpath(Piece_Title));
		Click(Xpath, Textpath("span", "Checkout (1)"));
		Wait_ajax();
		Click(Xpath, Pimco_SCP_CreateOrderTemplate_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_SCP_COT_Title_Path),
				"'Create Order Template' button at the end of the page");
		Type(Xpath, Pimco_SCP_COT_Name_Path, "QA Test");
		Type(Xpath, Pimco_SCP_COT_Description_Path, "QA Test");
		Click(Xpath, Pimco_SCP_COT_Createbtn_Path);
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "QA Test order template has been successfully created.")),
				"'QA Test order template has been successfully created. is not displayed");
		Click(Xpath, Pimco_LP_Home_path);
		ExplicitWait_Element_Clickable(Xpath, Pimco_LP_ViewAllAnnouncements_path);
		Assert.assertTrue(Get_Text(Xpath, Ordertemplate_Form1_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order template widget");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(PIMCOBoth1Username, PIMCOBoth1Password);
		Clearcarts();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = false)
	public void afterMethod() throws InterruptedException {

		Clearcarts();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
