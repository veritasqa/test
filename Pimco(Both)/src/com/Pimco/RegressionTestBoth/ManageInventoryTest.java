package com.Pimco.RegressionTestBoth;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Pimco.Base.BaseTestPimco;

public class ManageInventoryTest extends BaseTestPimco {

	// Page opens with create new item, search items, and search results grids
	// and displays appropriate fields
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_1_1() throws InterruptedException {
		ManageinventoryNav();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_CreateNewItem_Section_Path),
				"Create New Item section not displays on page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_SearchItems_Section_Path),
				"Search Item section not displays on page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_SearchResults_Section_Path),
				"Search Results section not displays on page");
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_CreateNewtype_Input_ID, "value").trim().equalsIgnoreCase("- Any -"),
				Get_Attribute(ID, Pimco_IM_CreateNewtype_Input_ID, "value")
						+ "-Any- not displays as default value for 'Create New Type' dropdown");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_CreateNewtype_Add_Btn_ID),
				"Add button not displays for 'Create New Type' drop down");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_ToCopy_TxtBox_ID),
				"Product Code To Copy field not displays in 'Create New Item' section");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_CreateNewtype_Copy_Btn_ID),
				"Copy button not displays near 'Product Code To Copy' text field");

		// Steps 14 - 22
		Click(ID, Pimco_IM_CreateNewtype_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Book Builder")),
				"Book Builder- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				"Kit - On the Fly- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Prebuilt")),
				"Kit - Prebuilt- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD")),
				"POD- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD � Customizable")),
				"POD - Customizable- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premium Item")),
				"Premium Item- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Print to Shelf")),
				"Print to Shelf- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				"Stock- not displays in the 'Create New Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("User Kit")),
				"User Kit- not displays in the 'Create New Type' drop-down");
		Click(ID, Pimco_IM_CreateNewtype_Arrow_ID);
		// Steps 23-38
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_ProductCode_TxtBox_ID),
				"Product Code field displays in 'Search Items' section");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Title_TxtBox_ID),
				"Text field displays in 'Search Items' section");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Keyword_TxtBox_ID),
				"Keyword field displays in 'Search Items' section");
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						Textpath("span", "*Searches on: Item ID, Item Description, Notes and Keyword Fields")),
				"*Searches on: Item ID, Item Description, Notes and Keyword Fields - message Not displayed");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_InventoryTypeDropdown_ID),
				"Inventory Type Dropdown field displays in 'Search Items' section");
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_InventoryTypeDropdown_ID, "value").trim().equalsIgnoreCase("- Any -"),
				"-Any- not displays as default value for Inventory Type Dropdown");

		Click(ID, Pimco_IM_InventoryTypeDropdown_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Book Builder")),
				"Book Builder- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				"Kit - On the Fly- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Prebuilt")),
				"Kit - Prebuilt- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD")),
				"POD- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD � Customizable")),
				"POD - Customizable- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premium Item")),
				"Premium Item- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Print to Shelf")),
				"Print to Shelf- not displays in the 'Inventory' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				"Stock- not displays in the 'Inventory Type' drop-down");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("User Kit")),
				"User Kit- not displays in the 'Inventory Type' drop-down");
		Click(ID, Pimco_IM_InventoryTypeDropdown_Arrow_ID);
		Wait_ajax();
		// Steps 39-44
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_FormownerDD_ID, "value").trim().equalsIgnoreCase("- Any -"),
				"-Any- not displays as default value for 'Form Owner' dropdown");
		Click(ID, Pimco_IM_FormownerDD_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Marketing")),
				"Marketing Not Displayed Under Form owner Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Regulatory")),
				"Regulatory Not Displayed Under Form owner Dropdown");
		Click(ID, Pimco_IM_FormownerDD_Arrow_ID);
		Wait_ajax();

		// Steps 45-58
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_CatagoryDrowpdown_ID, "value").trim().equalsIgnoreCase("- Any -"),
				"-Any- not displays as default value for 'Catagory' dropdown");
		Click(ID, Pimco_IM_CatagoryDrowpdown_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("FINRA letter")),
				"FINRA Letter Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premiums")),
				"Premiums Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Regulatory")),
				"Regulatory Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Marketing Kits")),
				"Marketing Kits Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Product Documents")),
				"Product Documents Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other - Insight Documents")),
				"Other - Insight Documents Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other - Marketing Documents")),
				"Other - Marketing Documents Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other - Product Documents")),
				"Other - Product Documents Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other - Support Documents")),
				"Other - Support Documents Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Personalized / Customized")),
				"Personalized / Customized Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("VA Use Only")),
				"VA Use Only Not Displayed Under Category Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Business Cards")),
				"Business Cards Not Displayed Under Category Dropdown");
		Click(ID, Pimco_IM_CatagoryDrowpdown_Arrow_ID);
		Wait_ajax();
		// Steps 59-85
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_UserGroupsDrowpdown_ID, "value").trim().equalsIgnoreCase("- Any -"),
				"-Any- not displays as default value for 'User Groups' dropdown");
		Click(ID, Pimco_IM_UserGroupsDrowpdown_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Advisory/Brokerage")),
				"Advisory/Brokerage Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("AST")), "AST Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Bank Trust")),
				"Bank Trust Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("BFDS")),
				"BFDS Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("CA")), "CA Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Canada Admin Team")),
				"Canada Admin Team Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Canada FAs")),
				"Canada FAs Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Canada Sales Team")),
				"Canada Sales Team Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("DC")), "DC Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("ETF")), "ETF Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Events")),
				"Events Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("FA�s/Clients")),
				"FA's/Clients Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Fund Ops")),
				"Fund Ops Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("HR")), "HR Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("IDDG")),
				"IDDG Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Managed Accounts")),
				"Managed Accounts Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Marketing")),
				"Marketing Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("National Accounts")),
				"National Accounts Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Other")),
				"Other Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Regulatory")),
				"Regulatory Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("RIA")), "RIA Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Sales Associate")),
				"Sales Associate Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("US")), "US Not Displayed Under User Groups Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("VA")), "VA Not Displayed Under User Groups Dropdown");
		Click(ID, Pimco_IM_UserGroupsDrowpdown_Arrow_ID);
		Wait_ajax();
		// Steps 86-90
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("- Any -")),
				"- Any - Not Displayed Under Status Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Active")),
				"Active Not Displayed Under Status Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Inactive")),
				"Inactive Not Displayed Under Status Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Obsolete")),
				"Obsolete Not Displayed Under Status Dropdown");
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		// Steps 91-98
		Click(ID, Pimco_IM_UnitofHandDrowpdown_Arrow_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Low Stock")),
				"Low Stock Not Displayed Units On Hand Dropdown");
		Assert.assertTrue(Element_Is_Displayed(Xpath, li_value("Out of Stock")),
				"Low Stock Not Displayed Units On Hand Dropdown");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Search_Btn_ID),
				"Search Button Missing Under Search items Section");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Clear_Btn_ID),
				"Clear Button Missing Under Search items Section");
		Click(ID, Pimco_IM_UnitofHandDrowpdown_Arrow_ID);
	}

	// Ability to select create new type and click add with appropriate page
	// opening
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_1_2() throws InterruptedException {

		// Precondition
		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, "QA_TESTNEW");
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Obsolete"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, Pimco_IM_SearchResults_Createdate_Path);
		Wait_ajax();
		Click(Xpath, Pimco_IM_SearchResults_Createdate_Path);
		Wait_ajax();
		Pimco_TC_2_4_9_1_2_Piece = "QA_TESTNEWITEM_"
				+ Integer.parseInt(Get_Text(Xpath, Pimco_IM_SearchResults_Firstresult_Path).substring(15,
						Get_Text(Xpath, Pimco_IM_SearchResults_Firstresult_Path).length()))
				+ 1;

		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_1_2_Piece);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		if (Element_Is_Displayed(Xpath, Pimco_IM_SearchResults_Firstresult_Path)) {
			Pimco_TC_2_4_9_1_2_Piece = "QA_TESTNEWITEM_"
					+ Integer.parseInt(Pimco_TC_2_4_9_1_2_Piece.substring(15, Pimco_TC_2_4_9_1_2_Piece.length())) + 1;
		}

		/*
		 * String inpu = "QA_TESTNEWITEM_999"; String Conver =
		 * inpu.substring(15, inpu.length());
		 * 
		 * int i = Integer.parseInt( Conver )+1; String output =
		 * "QA_TESTNEWITEM_"+i; System.out.println(output);
		 */

		/* END OF PRECONDITION */

		// Steps 1-36

		Click(ID, Pimco_IM_CreateNewtype_Arrow_ID);
		Click(Xpath, li_value("POD"));
		Click(ID, Pimco_IM_CreateNewtype_Add_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Text(ID, Pimco_IM_PageHeader_ID).trim().equalsIgnoreCase("New Item"),
				"The Manage Inventory Item: New Item page not opens");
		Type(ID, Pimco_IM_ProductCode_ID, Pimco_TC_2_4_9_1_2_Piece);
		Type(ID, Pimco_IM_Title_ID, "QA TEST");
		Type(ID, Pimco_IM_AsofDate_Txtbox_ID, Get_Todaydate("MM/dd/YYYY"));
		Click(ID, Pimco_IM_ddlInventoryType_Arrow_ID);
		Click(Xpath, li_value("POD"));

		Click(ID, Pimco_IM_ddlDocumentType_Arrow_ID);
		Click(Xpath, li_value("Form"));

		Click(ID, Pimco_IM_ddlFormOwner_Arrow_ID);
		Click(Xpath, li_value("Marketing"));

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_DD_Country_Section_ID, "value").trim().equalsIgnoreCase("Both"),
				"Both displays in 'Country' field");

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		Click(Xpath, li_value("Not Viewable"));

		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CA_Checkbox_Path),
				" CA is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaAdminTeam_Checkbox_Path),
				"CanadaAdminTeam is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaFAs_Checkbox_Path),
				" CanadaFAsis checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaSalesTeam_Checkbox_Path),
				"CanadaSalesTeam is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_US_Checkbox_Path),
				"US is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Advisory_Brokerage_CheckBox_Path),
				"Advisory_Brokerage is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_AST_CheckBox_Path),
				"AST CheckBox is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Bank_Trust_CheckBox_Path),
				" Bank_Trust_CheckBox is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_BFDS_CheckBox_Path),
				"BFDS is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_DC_CheckBox_Path),
				"DC is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_ETF_CheckBox_Path),
				" ETF is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Events_CheckBox_Path),
				"Events is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_FA_Clients_CheckBox_Path),
				"FA_Clients is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Fund_Ops_CheckBox_Path),
				"Fund_Ops is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_HR_CheckBox_Path),
				"HR is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_IDDG_CheckBox_Path),
				"IDDG is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Managed_Accounts_CheckBox_Path),
				"Managed_Accounts is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Marketing_CheckBox_Path),
				"Marketing is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_National_Accounts_CheckBox_Path),
				"National_Accounts is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Other_CheckBox_Path),
				"Other is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Regulatory_CheckBox_Path),
				"Regulatory is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_RIA_CheckBox_Path),
				"RIA is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Sales_Associate_CheckBox_Path),
				"Sales_Associate is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_VA_CheckBox_Path),
				"VA is checked off in 'User Group' field");

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Rules"));
		Type(ID, Pimco_IM_RT_ObsoleteDate_TextBOX_ID, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Click(ID, Pimco_IM_Save_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_IM_PMSG_OK_Button_ID);
		Reporter.log(CreatePart + " is Obsoleted");

	}

	// Validate that you can copy from the create new item grid

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_1_3() throws InterruptedException {
		// Precondition
		ManageinventoryNav();
		Pimco_TC_2_4_9_1_3_Piece = "QA_TESTNEWITEM_"
				+ Integer.parseInt(Pimco_TC_2_4_9_1_2_Piece.substring(15, Pimco_TC_2_4_9_1_2_Piece.length())) + 1;

		/*End Of Precondition*/

		// Step 1-15
		Type(ID, Pimco_IM_ToCopy_TxtBox_ID, "QA_TESTNEWITEM_X");
		Click(ID, Pimco_IM_CreateNewtype_Copy_Btn_ID);
		Wait_ajax();

		/*Click(Xpath,ClickButtononSearchresultspath("qa_testnewitem_x", "Copy"));
		Click(Xpath, Pimco_IM_Popuop_OKBtn_Path);
		Wait_ajax(); */

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("textarea", "qa_testnewitem_x")),
				"QA_TESTNEWITEM_X Not displays in 'Title' field");
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_Predecessor_Txtbox_ID, "value").trim().equalsIgnoreCase("qa_testnewitem_x"),
				"QA_TESTNEWITEM_X Not displays in 'Predecessor' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlInventoryType_ID, "value").trim().equalsIgnoreCase("POD"),
				"POD displays in 'Inventory Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "value").trim().equalsIgnoreCase("1"),
				"1 displays in 'Units Per Pack' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlDocumentType_ID, "value").trim().equalsIgnoreCase("Form"),
				"Form not displays in the 'Document Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlFormOwner_ID, "value").trim().equalsIgnoreCase("Marketing"),
				"Marketing is Not selected in 'Form Owner' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_DD_Country_Section_ID, "value").trim().equalsIgnoreCase("Both"),
				"Both displays in 'Country' field");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Fulfillment_CheckBox_ID),
				"Fulfillment checkbox is checked off in 'View In' field");
		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_BulkOrders_CheckBox_ID),
				"Bulk Order checkbox is checked off in 'View In' field");

		// Step 16-

		Type(ID, Pimco_IM_ProductCode_ID, Pimco_TC_2_4_9_1_3_Piece);

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		Click(Xpath, li_value("Not Viewable"));

		Click(ID, Pimco_IM_ddlDocumentType_Arrow_ID);
		Click(Xpath, li_value("Brochure"));

		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CA_Checkbox_Path),
				" CA is checked off in 'User Group' field");

		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_US_Checkbox_Path),
				"US is checked off in 'User Group' field");

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_1_3_Piece);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Pimco_TC_2_4_9_1_3_Piece)),
				Pimco_TC_2_4_9_1_3_Piece + "is not displayed");
		Click(Xpath, ClickButtononSearchresultspath(Pimco_TC_2_4_9_1_3_Piece, "Edit"));
		Click(Xpath, Textpath("span", "Rules"));
		Type(ID, Pimco_IM_RT_ObsoleteDate_TextBOX_ID, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Click(ID, Pimco_IM_Save_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_IM_PMSG_OK_Button_ID);
		Reporter.log(CreatePart + " is Obsoleted");

	}

	// Validate that a part can be copied from the Search Results table
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_1_4() throws InterruptedException {
		// Precondition
		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, "QA_Copy_SearchResults");
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Obsolete"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, Pimco_IM_SearchResults_Createdate_Path);
		Wait_ajax();
		Click(Xpath, Pimco_IM_SearchResults_Createdate_Path);
		Wait_ajax();
		Pimco_TC_2_4_9_1_4_Piece = "QA_Copy_SearchResults_"
				+ Integer.parseInt(Get_Text(Xpath, Pimco_IM_SearchResults_Firstresult_Path).substring(22,
						Get_Text(Xpath, Pimco_IM_SearchResults_Firstresult_Path).length()))
				+ 1;

		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_1_4_Piece);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		if (Element_Is_Displayed(Xpath, Pimco_IM_SearchResults_Firstresult_Path)) {
			Pimco_TC_2_4_9_1_4_Piece = "QA_Copy_SearchResults_"
					+ Integer.parseInt(Pimco_TC_2_4_9_1_4_Piece.substring(22, Pimco_TC_2_4_9_1_4_Piece.length())) + 1;

		}

		/*End Of Precondition*/

		// Step 1-11
		ManageinventoryNav();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, "QA_Copy_SearchResults_X");
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath("QA_Copy_SearchResults_X", "Copy"));

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Popuop_OKBtn_Path),
				"OK button is displayed in the pop-up window");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Popuop_CancelBtn_Path),
				"Cancel button is displayed in the pop-up window ");
		Click(Xpath, Pimco_IM_Popuop_OKBtn_Path);
		Wait_ajax();

		// Step 12-
		Type(ID, Pimco_IM_ProductCode_ID, Pimco_TC_2_4_9_1_4_Piece);

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("textarea", "QA Test")),
				"QA Test Not displays in 'Title' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("textarea", "THIS IS A QA TEST")),
				"THIS IS A QA TEST Not displays in 'Synopsis' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Predecessor_Txtbox_ID, "value").trim().equalsIgnoreCase(
				"QA_Copy_SearchResults_X"), "QA_Copy_SearchResults_X Not displays in 'Predecessor' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlInventoryType_ID, "value").trim().equalsIgnoreCase("POD"),
				"POD displays in 'Inventory Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "value").trim().equalsIgnoreCase("1"),
				"1 displays in 'Units Per Pack' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlDocumentType_ID, "value").trim().equalsIgnoreCase("Brochure"),
				"Form not displays in the 'Document Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlFormOwner_ID, "value").trim().equalsIgnoreCase("Marketing"),
				"Marketing is Not selected in 'Form Owner' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_DD_Country_Section_ID, "value").trim().equalsIgnoreCase("Both"),
				"Both displays in 'Country' field");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Fulfillment_CheckBox_ID),
				"Fulfillment checkbox is checked off in 'View In' field");
		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_BulkOrders_CheckBox_ID),
				"Bulk Order checkbox is checked off in 'View In' field");

		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CA_Checkbox_Path),
				" CA is checked off in 'User Group' field");

		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_US_Checkbox_Path),
				"US is checked off in 'User Group' field");

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		Click(Xpath, li_value("Not Viewable"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_1_4_Piece);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", Pimco_TC_2_4_9_1_4_Piece)),
				Pimco_TC_2_4_9_1_4_Piece + "is not displayed");
		Click(Xpath, ClickButtononSearchresultspath(Pimco_TC_2_4_9_1_4_Piece, "Edit"));
		Click(Xpath, Textpath("span", "Rules"));
		Type(ID, Pimco_IM_RT_ObsoleteDate_TextBOX_ID, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Click(ID, Pimco_IM_Save_Btn_ID);
		ExplicitWait_Element_Clickable(ID, Pimco_IM_PMSG_OK_Button_ID);
		Reporter.log(CreatePart + " is Obsoleted");

		Click(Xpath, Pimco_LP_Home_path);
		Wait_ajax();

		FulfilmentSearch(Pimco_TC_2_4_9_1_4_Piece);
		ContactClick("qaauto", "automation");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span",
				"Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active.")),
				"Error Message is missing");
	}

	// Ensure all the Search items fields are working and appropriate search
	// results appear
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_1_5ANDPimco_TC_2_4_9_1_6() throws InterruptedException {
		// Steps 1 - 12
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Multifunction);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", QA_Multifunction)),
				"QA_Multifunction displays in 'Search Results' grid");
		Click(ID, Pimco_IM_Clear_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ProductCode_TxtBox_ID, "value").isEmpty(),
				"Product Code Field is not empty");

		Type(ID, Pimco_IM_Title_TxtBox_ID, "QATest");
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_SR_FR_Title_Path).trim().equalsIgnoreCase("QATest"),
				"QA Test displays under the 'Title' column in the 'Search Results' grid");
		Click(ID, Pimco_IM_Clear_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Title_TxtBox_ID, "value").isEmpty(), "Title Field is not empty");

		// Steps 13 -31
		Type(ID, Pimco_IM_Keyword_TxtBox_ID, "QA");
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_Title_ID, "value").trim().contains("QA")
						|| Get_Attribute(ID, Pimco_IM_Keywords_Txtbox_ID, "value").trim().contains("QA"),
				"QA displays in the 'Key Words' field or in 'Title' field");

		ManageinventoryNav();

		Click(ID, Pimco_IM_InventoryTypeDropdown_Arrow_ID);
		Click(Xpath, Textpath("li", "POD"));
		Wait_ajax();

		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_SR_FR_Type_Path).trim().equalsIgnoreCase("POD"),
				"POD Not displays in 'Type' column in 'Search Results' grid");

		Click(ID, Pimco_IM_Clear_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_InventoryTypeDropdown_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'Inventory Type' dropdown");

		Click(ID, Pimco_IM_FormownerDD_Arrow_ID);
		Click(Xpath, Textpath("li", "Marketing"));
		Wait_ajax();

		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, Pimco_IM_SR_FR_Edit_Btn_Path);
		Wait_ajax();

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlFormOwner_ID, "value").contains("Marketing"),
				"Marketing displays in 'Form Owner' dropdown");

		ManageinventoryNav();

		// Steps 32 -37
		Click(ID, Pimco_IM_CatagoryDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "FINRA letter"));
		Wait_ajax();
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, Pimco_IM_SR_FR_Edit_Btn_Path);
		Wait_ajax();

		Assert.assertTrue(Element_Is_selected(Xpath, checkedBoxXpathinCatagories("FINRA letter")),
				"FINRA letter check-box is checked off in 'Categories' section");

		// Steps 38
		ManageinventoryNav();

		Click(ID, Pimco_IM_UserGroupsDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Canada Admin Team"));
		Wait_ajax();
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, Pimco_IM_SR_FR_Edit_Btn_Path);
		Wait_ajax();

		Assert.assertTrue(Element_Is_selected(Xpath, CheckBoxXpath("Canada Admin Team")),
				"Canada Admin Team from 'User Groups' dropdown in 'Search Items' section");

		ManageinventoryNav();
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Active"));
		Wait_ajax();
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_SR_FR_Status_Path).trim().equalsIgnoreCase("A"),
				"A - NOT displays under 'Status' column in 'Search Results' grid ");

		Click(ID, Pimco_IM_Clear_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_StatusDrowpdown_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'Status' dropdown");

		Click(ID, Pimco_IM_UnitofHandDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Out of Stock"));
		Wait_ajax();
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_SR_FR_ONHand_Path).trim().equalsIgnoreCase("0"),
				"0 Not displays under 'On Hand' column in 'Search Results' grid");

		Click(ID, Pimco_IM_Clear_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitofHandDrowpdown_ID, "value").contains("All"),
				"All Not  displays in 'Unit on hand' dropdown");

		// Pimco_TC_2_4_9_1_6 --Ensure all the Search items clear button is
		// working and appropriate search results appear
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_Multifunction);
		Type(ID, Pimco_IM_Title_TxtBox_ID, "QATest");
		Type(ID, Pimco_IM_Keyword_TxtBox_ID, "QA");

		Click(ID, Pimco_IM_InventoryTypeDropdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Book Builder"));
		Wait_ajax();

		Click(ID, Pimco_IM_FormownerDD_Arrow_ID);
		Click(Xpath, Textpath("li", "Marketing"));
		Wait_ajax();

		Click(ID, Pimco_IM_CatagoryDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "FINRA letter"));
		Wait_ajax();

		Click(ID, Pimco_IM_UserGroupsDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Bank Trust"));
		Wait_ajax();

		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Active"));
		Wait_ajax();

		Click(ID, Pimco_IM_UnitofHandDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Out of Stock"));
		Wait_ajax();

		Click(ID, Pimco_IM_Clear_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ProductCode_TxtBox_ID, "value").isEmpty(),
				"Product Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Title_TxtBox_ID, "value").isEmpty(), "Title Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Keyword_TxtBox_ID, "value").isEmpty(),
				"KeyWord Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_InventoryTypeDropdown_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'Inventory Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_FormownerDD_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'Form Owner' dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_CatagoryDrowpdown_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'Catagory' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UserGroupsDrowpdown_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'User Groups' dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_StatusDrowpdown_ID, "value").contains("- Any -"),
				"- Any - Not  displays in 'Status' dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitofHandDrowpdown_ID, "value").contains("All"),
				"All Not  displays in 'Unit on hand' dropdown");

	}

	// Need to do it later or mostly leave it for manual

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_1_7ANDPimco_TC_2_4_9_1_8() {

	}

	// Validate all fields in general tab appear and have all tabs enabled

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_2_1() throws InterruptedException {

		// Steps 92-109
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_NewTestPart_X);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, ClickButtononSearchresultspath(QA_NewTestPart_X, "Edit"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_ProductCode_ID, "value").trim().equalsIgnoreCase("QA_NewTestPart_X"),
				"QA_NewTestPart_X Not  displays in 'Product Code' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AsteriskXpath("Product Code")),
				"Red asterisk displays for 'Product Code' field");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_ItemCode_Txtbox_ID, "value").trim().equalsIgnoreCase("QA_NewTestPart"),
				"QA_NewTestPart Not  displays in 'Item Code' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_JoBNumber_Txtbox_ID, "value").isEmpty(),
				"Job number field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Title_ID, "value").trim().equalsIgnoreCase("QA_NewTestPart"),
				"QA_NewTestPart Not  displays in 'Title' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AsteriskXpath("Title")),
				"Red asterisk  not displays for 'Title' field");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_Synopsis_Txtbox_ID, "value").trim()
						.equalsIgnoreCase("QA_NewTestPart is a QA TEST"),
				"QA_NewTestPart is a QA TEST Not  displays in 'Synopsis' field");
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_Keywords_Txtbox_ID, "value").trim().equalsIgnoreCase("QA_NewTestPart"),
				"QA_NewTestPart  Not  displays in 'KeyWords' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AsofDate_Txtbox_ID, "value").trim().equalsIgnoreCase("08/22/2016"),
				"QA_NewTestPart  Not  displays in 'KeyWords' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AsteriskXpath("As Of Date")),
				"Red asterisk not displays for 'As Of Date' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Predecessor_Txtbox_ID, "value").isEmpty(),
				"Predecessor field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlInventoryType_ID, "value").trim().equalsIgnoreCase("Stock"),
				"Stock Not  displays in 'Inventory Type' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AsteriskXpath("Inventory Type:")),
				"Red asterisk not  displays for 'Inventory Type' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "value").trim().equalsIgnoreCase("1"),
				"1 Not  displays in 'Units Per Kit' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlDocumentType_ID, "value").trim().equalsIgnoreCase("Form"),
				"Form Not  displays in 'Document Type' field");

		// Assert.assertTrue(Element_Is_Displayed(Xpath, AsteriskXpath("Document
		// Type:")), "Red asterisk not displays for 'Document Type' field");

		// Steps 110
		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_UserKitContainer_CheckBox_ID),
				"User Kit Container is checked Off");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ContentSteward_Textbox_ID, "value").isEmpty(),
				"Contact Steward Field is Not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlFormOwner_ID, "value").trim().equalsIgnoreCase("Regulatory"),
				" Regulatory Not  displays in 'Form Owner' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, AsteriskXpath("Form Owner")),
				"Red asterisk  not displays for 'Form Owner' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_DD_Country_Section_ID, "value").trim().equalsIgnoreCase("Both"),
				" Both Not  displays in 'Country' field");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_DD_Viewability_Section_ID, "value").trim().equalsIgnoreCase("Orderable"),
				" Orderable Not  displays in 'Viewability' field");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Fulfillment_CheckBox_ID), "Fullfilment is not  checked Off");
		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_Marketingcampaingns_CheckBox_ID),
				"Marketing Campaingn is   checked Off");
		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_BulkOrders_CheckBox_ID), "Bulk Order is   checked Off");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_CA_Checkbox_Path), "CA is not  checked Off");
		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_US_Checkbox_Path), "US is not  checked Off");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Notes_TextBox_ID), "Notes Field is Missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_SpecialMessage_TextBox_ID),
				"Special Message Field is Missing ");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_ProductDocumentIsAbout_TextBox_ID),
				"Product printed document is about field is missing ");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ShareClass_DropDown_ID, "value").trim().equalsIgnoreCase("A"),
				" A   Not  displays in 'Share Class' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Supplement_DropDown_ID, "value").trim()
				.equalsIgnoreCase("- Please Select -"), " - Please Select -  Not  displays in 'Supplement Type' field");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_ProductDocumentIsAbout_TextBox_ID),
				"Product printed document is about field is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Authors_TextBox_ID), "Authors Field is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Topics_TextBox_ID), "Topics Field is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_FirmDocumentIsAbout_TextBox_ID),
				"Firm printed Document is about field is missing  ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Language_DropDown_ID),
				"language Dropdown field is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Departmentowner_TxtBox_ID),
				"Department owner Field is Missing ");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Save_Btn_ID), "Save Button is Missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Cancel_Btn_ID), "Cancel Button is Missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Next_Btn_ID), "Next Button is Missing ");

		Click(Xpath, Textpath("span", "Attachments"));
		Wait_ajax();
		Assert.assertTrue(
				Get_Title().trim().equalsIgnoreCase("Manage Inventory Attachments | PIMCO // Veritas Superfulfillment"),
				"Attachment tab not opening");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "QA_NewTestPart_X : QA_NewTestPart")),
				"Attachments   :  QA_NewTestPart_X : QA_NewTestPart page Doesnt opens");

		Click(Xpath, Textpath("span", "Pricing"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Chargeback Cost")), "Pricing tab not opened");

		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Veritas Pricing Details")),
				"Veritas Pricing Details  not displays on the 'Pricing' page");

		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Flat Pricing")),
				"Flat Pricing  not displays on the 'Pricing' page");

		Click(Xpath, Textpath("span", "Notifications"));
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("h1", "Notification Options"))
						&& Element_Is_Displayed(Xpath, Textpath("span", "QA_NewTestPart_X : QA_NewTestPart")),
				"Notification Options  :  QA_NewTestPart_X : QA_NewTestPart NOT displays on the page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Notification Addresses")),
				"Notification Addresses Section not Displaying");

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("td", "Allow Backorders?:")),
				"Rules tab not openes");
		Assert.assertTrue(
				Element_Is_Displayed(Xpath, TextpathContains("td", "Default Maximum Order Quantity Frequency:")),
				"Max Order Quantity appears on the 'Rules' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Superseding")),
				"Superseding appears on the 'Rules' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Required Items")),
				"Required Items appears on the 'Rules' page");

		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Include Pre-Approval Firms")),
				"Include Pre-Approval Firms appears on the 'Rules' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Max Order Quantity Details")),
				"Max Order Quantity Details appears on the 'Rules' page");

		Click(Xpath, Textpath("span", "Metrics"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Metrics")),
				"Metrics appears on the 'Rules' page");

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Change History")),
				"Change History appears on the 'Rules' page");

		Click(Xpath, Textpath("span", "Pageflex"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Pageflex Job Name")),
				"Pageflex Job Name appears on the 'Rules' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("h1", "Inventory Options")),
				"Inventory Options appears on the 'Rules' page");

	}

	// Validate all fields that are not greyed out can be edited and saved
	@Test(enabled = true)
	public void Pimco_TC_2_4_9_2_2() throws InterruptedException, IOException {
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_NewTestPart_X);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, ClickButtononSearchresultspath(QA_NewTestPart_X, "Edit"));
		Wait_ajax();
		// Steps 166-186
		Click(Xpath, Textpath("span", "General"));
		Wait_ajax();

		Type(ID, Pimco_IM_Title_ID, "QA_NewTestParttitle");
		Type(ID, Pimco_IM_Synopsis_Txtbox_ID, "QA_NewTestPart  IS A QA TESTSynopsis");

		Type(ID, Pimco_IM_Keywords_Txtbox_ID, "QA_NewTestPartKeyword");

		Type(ID, Pimco_IM_AsofDate_Txtbox_ID, "0" + Get_Todaydate("MM/dd/YYYY"));

		Click(ID, Pimco_IM_ddlInventoryType_Arrow_ID);
		// Wait_ajax();
		Click(Xpath, Textpath("li", "POD"));
		Wait_ajax();

		Type(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "2");

		Click(ID, Pimco_IM_ddlDocumentType_Arrow_ID);
		// Wait_ajax();
		Click(Xpath, Textpath("li", "Brochure"));
		Wait_ajax();

		Click(ID, Pimco_IM_UserKitContainer_CheckBox_ID);

		Type(ID, Pimco_IM_ContentSteward_Textbox_ID, "Content Steward");

		Click(ID, Pimco_IM_ddlFormOwner_Arrow_ID);
		// Wait_ajax();
		Click(Xpath, Textpath("li", "Marketing"));
		Wait_ajax();

		Click(ID, Pimco_IM_DD_Country_Arrow_ID);
		// Wait_ajax();
		Click(Xpath, Textpath("li", "US"));
		Wait_ajax();

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		// Wait_ajax();
		Click(Xpath, Textpath("li", "Admin Only"));
		Wait_ajax();

		Type(ID, Pimco_IM_PublishDate_Textbox_ID, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));

		Click(ID, Pimco_IM_Marketingcampaingns_CheckBox_ID);

		if (Element_Is_selected(Xpath, Pimco_IM_CA_Checkbox_Path)) {

			Click(Xpath, Pimco_IM_CA_Checkbox_Path);

			Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaAdminTeam_Checkbox_Path),
					"Verify the checkbox for Canada Admin Team is unchecked for 'User Group' field");

			Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaFAs_Checkbox_Path),
					"Verify the checkbox for Canada FAs is unchecked for 'User Group' field");

			Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaSalesTeam_Checkbox_Path),
					"Verify the checkbox for Canada Sales Team is unchecked for 'User Group' field");

		} else {

			Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaAdminTeam_Checkbox_Path),
					"Verify the checkbox for Canada Admin Team is unchecked for 'User Group' field");

			Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaFAs_Checkbox_Path),
					"Verify the checkbox for Canada FAs is unchecked for 'User Group' field");

			Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaSalesTeam_Checkbox_Path),
					"Verify the checkbox for Canada Sales Team is unchecked for 'User Group' field");

		}

		// Steps 187
		Type(ID, Pimco_IM_Notes_TextBox_ID, "Notes");

		Type(ID, Pimco_IM_SpecialMessage_TextBox_ID, "Special Message");

		Click(ID, Pimco_IM_SplMSG_StartDate_CalenderIcon_ID);
		Wait_ajax();
		DatepickerTodaysDate(ID, Pimco_IM_SplMSG_StartDate_Month_ID, Pimco_IM_Calender_OKBtn_ID);
		Wait_ajax();

		Click(ID, Pimco_IM_SplMSG_EndDate_CalenderIcon_ID);
		Wait_ajax();
		Datepicker(Pimco_IM_SplMSG_EndDate_Month_ID, Pimco_IM_Calender_OKBtn_ID);
		Wait_ajax();

		Click(ID, Pimco_IM_ShareClass_DropDown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("label", "A"));
		Wait_ajax();
		Click(Xpath, Textpath("label", "B"));
		Wait_ajax();

		Click(ID, Pimco_IM_Supplement_DropDown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("li", "Permanent"));
		Wait_ajax();

		Type(ID, Pimco_IM_ProductDocumentIsAbout_TextBox_ID, "Product This Document");

		Type(ID, Pimco_IM_Authors_TextBox_ID, "Authors");
		Type(ID, Pimco_IM_Topics_TextBox_ID, "Topic");

		Type(ID, Pimco_IM_FirmDocumentIsAbout_TextBox_ID, "Firms This Document");

		Click(ID, Pimco_IM_Language_DropDown_Arrow_ID);
		Wait_ajax();

		Click(Xpath, Textpath("li", "English"));
		Wait_ajax();

		Type(ID, Pimco_IM_Departmentowner_TxtBox_ID, "Department Owner");

		Click(Xpath, UncheckedBoxXpathinCatagories("FINRA letter"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		logout();

	}

	// Validate appropriate changes are recorded in change history: for edits
	@Test(enabled = true)
	public void Pimco_TC_2_4_9_2_3() throws IOException, InterruptedException {
		PimcoGWMLogin();
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_NewTestPart_X);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, ClickButtononSearchresultspath(QA_NewTestPart_X, "Edit"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td",
				"LongDescription changed from 'QA_NewTestPart IS A QA TEST' to 'QA_NewTestPart IS A QA TESTSynopsis'")),
				"LongDescription changed from 'QA_NewTestPart IS A QA TEST' to 'QA_NewTestPart IS A QA TESTSynopsis' - Message Not Displyed in change History");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "FormOwner changed from 'Regulatory' to 'Marketing'")),
				"FormOwner changed from 'Regulatory' to 'Marketing'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "DocumentType changed from 'Form' to 'Brochure'")),
				"DocumentType changed from 'Form' to 'Brochure'- Message Not Displayes in Action Coloumn");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "AsOfDate changed from '8/22/2016'
		// to '(Today's Date - MM/DD/YYYY)' " )),"- Message Not Displayes in
		// Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "InventoryTypeID changed from 'Stock' to 'POD'")),
				"InventoryTypeID changed from 'Stock' to 'POD'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Country changed from 'Both' to 'US'")),
				"Country changed from 'Both' to 'US'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"Description changed from 'QA_NewTestPart' to 'QA_NewTestParttitle'")),
				"Description changed from 'QA_NewTestPart' to 'QA_NewTestParttitle'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"Viewability changed from 'ViewableAndOrderable' to 'AdminOnly'")),
				"Viewability changed from 'ViewableAndOrderable' to 'AdminOnly'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "IsMarketing changed from 'False' to 'True'")),
				"IsMarketing changed from 'False' to 'True'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "UnitsPerPack changed from '1' to '2'")),
				"UnitsPerPack changed from '1' to '2'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"KeyWords changed from 'QA_NewTestPart' to 'QA_NewTestPartKeyword'")),
				"KeyWords changed from 'QA_NewTestPart' to 'QA_NewTestPartKeyword'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "IsUserKitContainer changed from 'False' to 'True'")),
				"IsUserKitContainer changed from 'False' to 'True'- Message Not Displayes in Action Coloumn");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "EffectiveDate changed from
		// '12:00:00 AM' to '(Today's Date - MM/DD/YYYY)'")),"- Message Not
		// Displayes in Action Coloumn");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "SpecialMessageActivationDate
		// changed from '(Past Date - MM/DD/YYYY)' to '(today's Date -
		// MM/DD/YYYY )'")), "");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "SpecialMessageDeactivationDate
		// changed from '(Past Date - MM/DD/YYYY)' to '(today's Date -
		// MM/DD/YYYY )'")),"" );

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "ShareClass changed from 'A' to 'B'")),
				"ShareClass changed from 'A' to 'B'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "ContentSteward changed from '' to 'Content Steward'")),
				"ContentSteward changed from '' to 'Content Steward'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Language changed from '' to 'English'")),
				"Language changed from '' to 'English'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Notes changed from '' to 'NOTES'")),
				"Notes changed from '' to 'NOTES'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "SpecialMessage changed from '' to 'Special Message'")),
				"SpecialMessage changed from '' to 'Special Message'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Authors changed from '' to 'Authors'")),
				"Authors changed from '' to 'Authors'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "SupplementTypeID changed from '0' to 'Permanent'")),
				"SupplementTypeID changed from '0' to 'Permanent'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"ProductThisDocumentIsAbout changed from '' to 'Product This Document'")),
				"ProductThisDocumentIsAbout changed from '' to 'Product This Document'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Topics changed from '' to 'Topic'")),
				"Topics changed from '' to 'Topic'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"FirmsThisDocumentIsAbout changed from '' to 'Firms This Document'")),
				"FirmsThisDocumentIsAbout changed from '' to 'Firms This Document'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "DepartmentOwner changed from '' to 'Department Owner'")),
				"DepartmentOwner changed from '' to 'Department Owner'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "Category Added: {FINRA letter}; Category Removed: {-None-}")),
				"Category Added: {FINRA letter}; Category Removed: {-None-}- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td",
				"UserGroups Added: {-None-}; UserGroups Removed: {CA, Canada Admin Team, Canada FAs, Canada Sales Team}")),
				"UserGroups Added: {-None-}; UserGroups Removed: {CA, Canada Admin Team, Canada FAs, Canada Sales Team}- Message Not Displayes in Action Coloumn");

	}

	// Validate all fields that are not greyed out can have the edits undone and
	// saved
	@Test(enabled = true)
	public void Pimco_TC_2_4_9_2_4() throws InterruptedException, IOException {

		Click(Xpath, Textpath("span", "General"));
		Wait_ajax();

		Type(ID, Pimco_IM_Title_ID, "QA_NewTestPart");
		Type(ID, Pimco_IM_Synopsis_Txtbox_ID, "QA_NewTestPart  IS A QA TEST");

		Type(ID, Pimco_IM_Keywords_Txtbox_ID, "QA_NewTestPart");

		Type(ID, Pimco_IM_AsofDate_Txtbox_ID, "08/22/2016");

		Click(ID, Pimco_IM_ddlInventoryType_Arrow_ID);
		Wait_ajax();

		Click(Xpath, Textpath("li", "Stock"));
		Wait_ajax();

		Type(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "1");

		Click(ID, Pimco_IM_ddlDocumentType_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("li", "Form"));
		Wait_ajax();

		Click(ID, Pimco_IM_UserKitContainer_CheckBox_ID);

		Clear(ID, Pimco_IM_ContentSteward_Textbox_ID);

		Click(ID, Pimco_IM_ddlFormOwner_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("li", "Regulatory"));
		Wait_ajax();

		Click(ID, Pimco_IM_DD_Country_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("li", "Both"));
		Wait_ajax();

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("li", "Orderable"));
		Wait_ajax();

		Clear(ID, Pimco_IM_PublishDate_Textbox_ID);

		Click(ID, Pimco_IM_Marketingcampaingns_CheckBox_ID);

		Click(Xpath, Pimco_IM_CA_Checkbox_Path);

		Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaAdminTeam_Checkbox_Path),
				"Verify the checkbox for Canada Admin Team is unchecked for 'User Group' field");

		Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaFAs_Checkbox_Path),
				"Verify the checkbox for Canada FAs is unchecked for 'User Group' field");

		Assert.assertFalse(Element_Is_selected(Xpath, Pimco_IM_CanadaSalesTeam_Checkbox_Path),
				"Verify the checkbox for Canada Sales Team is unchecked for 'User Group' field");

		Clear(ID, Pimco_IM_Notes_TextBox_ID);

		Clear(ID, Pimco_IM_SpecialMessage_TextBox_ID);

		Click(ID, Pimco_IM_ShareClass_DropDown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("label", "B"));
		Wait_ajax();
		Click(Xpath, Textpath("label", "A"));
		Wait_ajax();

		Click(ID, Pimco_IM_Supplement_DropDown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, Textpath("li", "- Please Select -"));
		Wait_ajax();

		Clear(ID, Pimco_IM_ProductDocumentIsAbout_TextBox_ID);

		Clear(ID, Pimco_IM_Authors_TextBox_ID);
		Clear(ID, Pimco_IM_Topics_TextBox_ID);

		Clear(ID, Pimco_IM_FirmDocumentIsAbout_TextBox_ID);

		Click(ID, Pimco_IM_Language_DropDown_Arrow_ID);
		Wait_ajax();

		Click(Xpath, Textpath("li", "- Please Select -"));
		Wait_ajax();

		Clear(ID, Pimco_IM_Departmentowner_TxtBox_ID);

		Click(Xpath, UncheckedBoxXpathinCatagories("FINRA letter"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		logout();
	}

	// Validate appropriate changes are recorded in change history: for undo
	// changes
	@Test(enabled = true)
	public void Pimco_TC_2_4_9_2_5ANDPimco_TC_2_4_9_2_6ANDPimco_TC_2_4_9_2_7()
			throws IOException, InterruptedException {
		PimcoGWMLogin();
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, QA_NewTestPart_X);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, ClickButtononSearchresultspath(QA_NewTestPart_X, "Edit"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td",
				"LongDescription changed from 'QA_NewTestPart IS A QA TEST' to 'QA_NewTestPart IS A QA TESTSynopsis'")),
				"LongDescription changed from 'QA_NewTestPart IS A QA TEST' to 'QA_NewTestPart IS A QA TESTSynopsis' - Message Not Displyed in change History");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "FormOwner changed from 'Regulatory' to 'Marketing'")),
				"FormOwner changed from 'Regulatory' to 'Marketing'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "DocumentType changed from 'Form' to 'Brochure'")),
				"DocumentType changed from 'Form' to 'Brochure'- Message Not Displayes in Action Coloumn");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "AsOfDate changed from '8/22/2016'
		// to '(Today's Date - MM/DD/YYYY)' " )),"- Message Not Displayes in
		// Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "InventoryTypeID changed from 'Stock' to 'POD'")),
				"InventoryTypeID changed from 'Stock' to 'POD'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Country changed from 'Both' to 'US'")),
				"Country changed from 'Both' to 'US'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"Description changed from 'QA_NewTestPart' to 'QA_NewTestParttitle'")),
				"Description changed from 'QA_NewTestPart' to 'QA_NewTestParttitle'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"Viewability changed from 'ViewableAndOrderable' to 'AdminOnly'")),
				"Viewability changed from 'ViewableAndOrderable' to 'AdminOnly'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "IsMarketing changed from 'False' to 'True'")),
				"IsMarketing changed from 'False' to 'True'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "UnitsPerPack changed from '1' to '2'")),
				"UnitsPerPack changed from '1' to '2'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"KeyWords changed from 'QA_NewTestPart' to 'QA_NewTestPartKeyword'")),
				"KeyWords changed from 'QA_NewTestPart' to 'QA_NewTestPartKeyword'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "IsUserKitContainer changed from 'False' to 'True'")),
				"IsUserKitContainer changed from 'False' to 'True'- Message Not Displayes in Action Coloumn");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "EffectiveDate changed from
		// '12:00:00 AM' to '(Today's Date - MM/DD/YYYY)'")),"- Message Not
		// Displayes in Action Coloumn");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "SpecialMessageActivationDate
		// changed from '(Past Date - MM/DD/YYYY)' to '(today's Date -
		// MM/DD/YYYY )'")), "");

		// Assert.assertTrue(Element_Is_Displayed(Xpath,
		// XpathStringWithSingleQuote("td", "SpecialMessageDeactivationDate
		// changed from '(Past Date - MM/DD/YYYY)' to '(today's Date -
		// MM/DD/YYYY )'")),"" );

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "ShareClass changed from 'A' to 'B'")),
				"ShareClass changed from 'A' to 'B'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "ContentSteward changed from '' to 'Content Steward'")),
				"ContentSteward changed from '' to 'Content Steward'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Language changed from '' to 'English'")),
				"Language changed from '' to 'English'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Notes changed from '' to 'NOTES'")),
				"Notes changed from '' to 'NOTES'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "SpecialMessage changed from '' to 'Special Message'")),
				"SpecialMessage changed from '' to 'Special Message'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Authors changed from '' to 'Authors'")),
				"Authors changed from '' to 'Authors'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "SupplementTypeID changed from '0' to 'Permanent'")),
				"SupplementTypeID changed from '0' to 'Permanent'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"ProductThisDocumentIsAbout changed from '' to 'Product This Document'")),
				"ProductThisDocumentIsAbout changed from '' to 'Product This Document'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td", "Topics changed from '' to 'Topic'")),
				"Topics changed from '' to 'Topic'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"FirmsThisDocumentIsAbout changed from '' to 'Firms This Document'")),
				"FirmsThisDocumentIsAbout changed from '' to 'Firms This Document'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "DepartmentOwner changed from '' to 'Department Owner'")),
				"DepartmentOwner changed from '' to 'Department Owner'- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "Category Added: {FINRA letter}; Category Removed: {-None-}")),
				"Category Added: {FINRA letter}; Category Removed: {-None-}- Message Not Displayes in Action Coloumn");

		Assert.assertTrue(Element_Is_Displayed(Xpath, XpathStringWithSingleQuote("td",
				"UserGroups Added: {-None-}; UserGroups Removed: {CA, Canada Admin Team, Canada FAs, Canada Sales Team}")),
				"UserGroups Added: {-None-}; UserGroups Removed: {CA, Canada Admin Team, Canada FAs, Canada Sales Team}- Message Not Displayes in Action Coloumn");

		// Pimco.TC.2.4.9.2.6
		// Verify the Change history tab is not updated for existing piece with
		// Firm Approvals when you are not making any changes . Then make
		// changes and verify for Pimco (Both) admin

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Select_DropDown_VisibleText(ID, Pimco_IM_RT_Pre_Approval_DD_FirmID, "AIG SunAmerica");

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertFalse(Element_Is_Displayed(Xpath, XpathStringContainsSingleQuote("td",
				"Revised existing include firms 'AMERIPRISE GROUP, AXA, BANC OF AMERICA / MERRILL LYNCH GROUP, BB&T INVESTMENT SERVICES, CAPITAL ONE INVESTMENTS LLC, CCO INVESTMENT SERVICES CORP, CITIGROUP, COMERICA GROUP, JP MORGAN CHASE GROUP, LINCOLN FINANCIAL GROUP, MASS MUTUAL GROUP, MORGAN STANLEY SMITH BARNEY GROUP, NORTHWESTERN MUTUAL INVESTMENT SERVICES, PNC INVESTMENTS LLC, RAYMOND JAMES GROUP, SUNTRUST GROUP, UBS GROUP, Protective Life - MEGA, EDWARD D JONES & CO, BANCWEST INVESTMENT SERVICES INC, FIFTH THIRD SECURITIES, INC, UNIONBANC INVESTMENT SERVICES LLC,")),
				"Revised existing include firms 'AMERIPRISE GROUP, AXA, BANC OF AMERICA / MERRILL LYNCH GROUP, BB&T INVESTMENT SERVICES, CAPITAL ONE INVESTMENTS LLC, CCO INVESTMENT SERVICES CORP, CITIGROUP, COMERICA GROUP, JP MORGAN CHASE GROUP, LINCOLN FINANCIAL GROUP, MASS MUTUAL GROUP, MORGAN STANLEY SMITH BARNEY GROUP, NORTHWESTERN MUTUAL INVESTMENT SERVICES, PNC INVESTMENTS LLC, RAYMOND JAMES GROUP, SUNTRUST GROUP, UBS GROUP, Protective Life - MEGA, EDWARD D JONES & CO, BANCWEST INVESTMENT SERVICES INC, FIFTH THIRD SECURITIES, INC, UNIONBANC INVESTMENT SERVICES LLC, -  Is Visible");

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Select_DropDown_VisibleText(ID, Pimco_IM_RT_Pre_Approval_DD_FirmID, "AXA");

		Type(ID, Pimco_IM_RT_Firm_Approval_Code_txt_BoxID, "123");

		Click(ID, Pimco_IM_RT_Firm_Approval_Expiration_Date_ID);

		DatepickerTodaysDate(ID, Pimco_IM_RT_Firm_Approval_Expiration_Date_Calender_ID, Pimco_IM_Calender_OKBtn_ID);

		Wait_ajax();

		Click(Xpath, Textpath("button", "Add Include Rule"));

		ExplicitWait_Element_Clickable(Xpath, RulesFirmRemoveBtnPath("AXA"));

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim().equalsIgnoreCase(
				"AXA Pre-approval firm was added."), "AXA Pre-approval firm was added. - Message is missing ");

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Click(Xpath, RulesFirmRemoveBtnPath("AXA"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim()
						.equalsIgnoreCase("AXA Pre-approval firm was removed."),
				"AXA Pre-approval firm was removed. - Message is missing ");

		// Pimco.TC.2.4.9.2.7
		// Verify user group is not recorded in change history on navigating
		// between the tabs and then make changes to the user group to validate
		// appropriate changes are recorded in change history

		// step number -335
		Click(Xpath, Textpath("span", "General"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim()
						.equalsIgnoreCase("UserGroups Added: {AST, IDDG}; UserGroups Removed: {-None-}"),
				"UserGroups Added: {AST, IDDG}; UserGroups Removed: {-None-}- Message is missing ");

		// step number -338
		Click(Xpath, Textpath("span", "Attachments"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim()
						.equalsIgnoreCase("UserGroups Added: {AST, IDDG}; UserGroups Removed: {-None-}"),
				"UserGroups Added: {AST, IDDG}; UserGroups Removed: {-None-}- Message is missing ");

		// step number -341
		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim()
						.equalsIgnoreCase("UserGroups Added: {AST, IDDG}; UserGroups Removed: {-None-}"),
				"UserGroups Added: {AST, IDDG}; UserGroups Removed: {-None-}- Message is missing ");

		// Step number- 344
		Click(Xpath, Textpath("span", "General"));
		Wait_ajax();

		Click(Xpath, CheckBoxXpath("DC"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim()
						.equalsIgnoreCase("UserGroups Added: {-None-}; UserGroups Removed: {DC}"),
				"UserGroups Added: {-None-}; UserGroups Removed: {DC} - Message is missing ");
		Wait_ajax();

		// Steps number 351
		Click(Xpath, Textpath("span", "General"));
		Wait_ajax();

		Click(Xpath, CheckBoxXpath("DC"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();
		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_His_FirstRow_Xpath).trim()
						.equalsIgnoreCase("UserGroups Added: {DC}; UserGroups Removed: {-None-}"),
				"UserGroups Added: {DC}; UserGroups Removed: {-None-} - Message is missing ");

	}

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_3_1ANDPimco_TC_2_4_9_3_2() throws InterruptedException {

		// Validate all fields in general tab appear and are blank
		ManageinventoryNav();
		Wait_ajax();

		// Steps 1-20
		Click(ID, Pimco_IM_CreateNewtype_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Stock"));
		Wait_ajax();

		Click(ID, Pimco_IM_CreateNewtype_Add_Btn_ID);
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ProductCode_ID, "value").isEmpty(),
				"Product Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ItemCode_Txtbox_ID, "value").isEmpty(),
				"Item Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_JoBNumber_Txtbox_ID, "value").isEmpty(),
				"JobNumber Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Title_ID, "value").isEmpty(), "Title Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Synopsis_Txtbox_ID, "value").isEmpty(),
				"Synopsis Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Keywords_Txtbox_ID, "value").isEmpty(),
				"KeyWord Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AsofDate_Txtbox_ID, "value").isEmpty(),
				"As Of Date Code Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Predecessor_Txtbox_ID, "value").isEmpty(),
				"Predecessor Field is not empty");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlInventoryType_ID, "value").equalsIgnoreCase("Stock"),
				"Stock was not Displyed in Inventory Type DropDown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "value").equalsIgnoreCase("1"),
				"1 was not Displyed in Units per Pack Text Box ");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlDocumentType_ID, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - was not Displyed in Document Type Dropdown");

		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_UserKitContainer_CheckBox_ID),
				"User kit Container is Selected");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ContentSteward_Textbox_ID, "value").isEmpty(),
				"Content Steward Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlFormOwner_ID, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - was not Displyed in Form Owner Dropdown");

		// Steps 21-

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_DD_Country_Section_ID, "value").equalsIgnoreCase("Both"),
				"Both was not Displyed in Country Dropdown");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_DD_Viewability_Section_ID, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - was not Displyed in Viewability Dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_PublishDate_Textbox_ID, "value").isEmpty(),
				"Publish Date Field is not empty");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Fulfillment_CheckBox_ID),
				"Fullfilment Checkbox is Not Selected");

		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_Marketingcampaingns_CheckBox_ID),
				"Marketing Checkbox is Selected");

		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_BulkOrders_CheckBox_ID), "BulkOrders Checkbox is Selected");

		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CA_Checkbox_Path),
				" CA is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaAdminTeam_Checkbox_Path),
				"CanadaAdminTeam is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaFAs_Checkbox_Path),
				" CanadaFAsis checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_CanadaSalesTeam_Checkbox_Path),
				"CanadaSalesTeam is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_US_Checkbox_Path),
				"US is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Advisory_Brokerage_CheckBox_Path),
				"Advisory_Brokerage is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_AST_CheckBox_Path),
				"AST CheckBox is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Bank_Trust_CheckBox_Path),
				" Bank_Trust_CheckBox is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_BFDS_CheckBox_Path),
				"BFDS is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_DC_CheckBox_Path),
				"DC is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_ETF_CheckBox_Path),
				" ETF is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Events_CheckBox_Path),
				"Events is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_FA_Clients_CheckBox_Path),
				"FA_Clients is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Fund_Ops_CheckBox_Path),
				"Fund_Ops is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_HR_CheckBox_Path),
				"HR is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_IDDG_CheckBox_Path),
				"IDDG is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Managed_Accounts_CheckBox_Path),
				"Managed_Accounts is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Marketing_CheckBox_Path),
				"Marketing is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_National_Accounts_CheckBox_Path),
				"National_Accounts is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Other_CheckBox_Path),
				"Other is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Regulatory_CheckBox_Path),
				"Regulatory is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_RIA_CheckBox_Path),
				"RIA is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_Sales_Associate_CheckBox_Path),
				"Sales_Associate is checked off in 'User Group' field");
		Assert.assertTrue(Element_Is_selected(Xpath, Pimco_IM_VA_CheckBox_Path),
				"VA is checked off in 'User Group' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Notes_TxtBox_ID, "value").isEmpty(), "Notes Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_SpecialMessage_TextBox_ID, "value").isEmpty(),
				"special Message Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_SplMSG_StartDate_TextBox_ID, "value").isEmpty(),
				"Special Message Start Date Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_SplMSG_EndDate_TextBox_ID, "value").isEmpty(),
				"Special Message end Date Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ShareClass_DropDown_ID, "value").isEmpty(),
				"Share Class Field is not empty");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_Supplement_DropDown_ID, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - was not Displyed in Supplement Type Dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ProductDocumentIsAbout_TextBox_ID, "value").isEmpty(),
				"Share Class Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Authors_TextBox_ID, "value").isEmpty(),
				"Share Class Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Topics_TextBox_ID, "value").isEmpty(),
				"Share Class Field is not empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_FirmDocumentIsAbout_TextBox_ID, "value").isEmpty(),
				"Share Class Field is not empty");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_Language_DropDown_ID, "value").equalsIgnoreCase("- Please Select -"),
				"- Please Select - was not Displyed in Supplement Type Dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Departmentowner_TxtBox_ID, "value").isEmpty(),
				"Share Class Field is not empty");

		// Pimco.TC.2.4.9.3.2
		// Validate that clicking save without filling out any fields provides
		// an error message for all tabs

		Click(ID, Pimco_IM_Save_Btn_ID);

		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_InvalidProductCode_ErrorMsg_ID).trim()
				.equalsIgnoreCase("Invalid Product Code!"), "Invalid Product Code! Error Message Not Displayed");
		Assert.assertTrue(
				Get_Text(Xpath, Pimco_IM_Titleisrequired_ErrorMsg_ID).trim().equalsIgnoreCase("Title is required!"),
				"Title is required! Error message not displyed");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_InvalidRevisionDate_ErrorMsg_ID).trim()
				.equalsIgnoreCase("Invalid Revision Date!"), "Invalid Revision Date! error Message not Displayed");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_DocumentTypeisrequired_ErrorMsg_ID).trim().equalsIgnoreCase(
				"Document Type is required!"), "Document Type is required! Error Message Not Displayed");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Required_ErrorMsg_ID).trim().equalsIgnoreCase("Required"),
				"Required Error Message Not displayed");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Viewabilityisrequired_ErrorMsg_ID).trim()
				.equalsIgnoreCase("Viewability is required!"), "Viewability is required! Error message Not displyed");

	}

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_3_3Precondition() throws InterruptedException {

		ManageinventoryNav();
		Wait_ajax();

		Pimco_TC_2_4_9_3_3_Piece = "QA_NewItemAddTest_AT_1";
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_3_3_Piece);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Pimco_TC_2_4_9_3_3_Piece = "QA_NewItemAddTest_AT_1";

		while (Element_Is_Displayed(Xpath, Pimco_IM_SearchResults_Firstresult_Path)) {

			Pimco_TC_2_4_9_3_3_Piece1 = Integer
					.parseInt(Pimco_TC_2_4_9_3_3_Piece.substring(21, Pimco_TC_2_4_9_3_3_Piece.length())) + 1;
			Pimco_TC_2_4_9_3_3_Piece = "QA_NewItemAddTest_AT_" + Pimco_TC_2_4_9_3_3_Piece1;

			Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_3_3_Piece);
			Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
			Wait_ajax();
			Click(Xpath, li_value("- Any -"));
			Click(ID, Pimco_IM_Search_Btn_ID);
			Wait_ajax();
		}
	}

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_3_3() throws InterruptedException {
		// Validate that filling out all required fields on general tab enable
		// the other tabs
		ManageinventoryNav();
		Wait_ajax();

		Click(ID, Pimco_IM_CreateNewtype_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Stock"));
		Click(ID, Pimco_IM_CreateNewtype_Add_Btn_ID);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Textpath("span", "Attachments"));

		Type(ID, Pimco_IM_ProductCode_ID, Pimco_TC_2_4_9_3_3_Piece);

		Type(ID, Pimco_IM_Title_ID, "QA Test");

		Type(ID, Pimco_IM_AsofDate_Txtbox_ID, Get_Todaydate("MM/dd/YYYY"));

		Click(ID, Pimco_IM_ddlDocumentType_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Brochure"));
		Wait_ajax();

		Click(ID, Pimco_IM_ddlFormOwner_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Marketing"));
		Wait_ajax();

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Not Viewable"));
		Wait_ajax();

		Click(ID, Pimco_IM_Next_Btn_ID);

		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Attachments")),
				"'Attachments' header  Not displayed");

		Click(Xpath, Textpath("span", "Pricing"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Unit Price")),
				"'Unit Price' header  Not displayed");

		Click(Xpath, Textpath("span", "Notifications"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Notification Options")),
				"Notification Options header  Not displayed");

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Rules")), "Rules header  Not displayed");

		Click(Xpath, Textpath("span", "Metrics"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Metrics")), "Metrics header  Not displayed");

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Change History")),
				"Change History header  Not displayed");

		Click(Xpath, Textpath("span", "Pageflex"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Pageflex")), "Pageflexz header  Not displayed");

	}

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_3_4ANDPimco_TC_2_4_9_3_5ANDPimco_TC_2_4_9_3_6() throws InterruptedException {
		Click(Xpath, Textpath("span", "Attachments"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Attachments")),
				"'Attachments' header  Not displayed");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("legend", "Product Thumbnail")),
				"Product Thumbnail displays in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("legend", "Online sample")),
				"Online sample displays in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("legend", "FINRA letter")),
				"FINRA letter displays in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_ProductThumbnail_Choosefiles_ID),
				"Product Thumbnails No files  not displayed choosen in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_ProductThumbnail_Upload_ID),
				"Product Thumbnails Upload Button was not displayed in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("legend", "Online sample")),
				"Online sample displays in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_OnlineSample_Choosefiles_ID),
				"Online Sample No files  not displayed choosen in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_OnlineSample_Upload_ID),
				"Online Sample Upload Button was not displayed in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("legend", "FINRA letter")),
				"FINRA letter displays in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_FinraLetter_Choosefiles_ID),
				"Finra Letter No files choosen not displayed in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_Finraletter_Upload_ID),
				"Finra Letter Upload Button was not displayed in the Attachments");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_PrintReadyPdf_Choosefilebtn_ID),
				"printreadyPdf choose file buttonwas not displayed in the Attachments");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_AT_BookPrint_Choosefilebtn_ID),
				"Book print choose file button Upload Button was not displayed in the Attachments");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_Colors_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in Colors Dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_PageSize_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in PageSize Dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_Finish_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in Finish Dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_Stock_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in Stock Dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_Coating_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in Coating Dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_Biding_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in bidding Dropdown");
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_AT_PrintMethod_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in Printmethod Dropdown");

		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_AT_PcsPrinttype_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in PCS print Type Dropdown");
		Assert.assertTrue(
				Get_Attribute(ID, Pimco_IM_AT_CoverStock_DD_ID, "value").equalsIgnoreCase("- Please Select - "),
				"- Please Select - was not Displyed in Cover Stock Dropdown");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_NumberofPages_ID, "value").equalsIgnoreCase("0"),
				"0 was not Displyed in Number of pages");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_SheetSize_Textbox_ID, "value").isEmpty(),
				"Sheet Size field is empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_Composition_Txtbox_ID, "value").isEmpty(),
				"Composition field is empty");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_AT_FinishSize_Textbox_ID, "value").isEmpty(),
				"Finish Size field is empty");

		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Unit Price")),
				"'Unit Price' header  Not displayed");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Price_ChargebackCost_ID, "value").equalsIgnoreCase("$0.00"),
				"$0.00 was not Displyed in Chargeback Cost");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_PricePrintProduction_ID, "value").equalsIgnoreCase("$0.00"),
				"$0.00 was not Displyed in Print production");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Price_PricingType_Flat_ID),
				"Flat is not selected in pricing type");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Price_NocostItem_ID),
				"No Cost Item  is not selected in pricing type");

		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_Price_CostApproved_ID),
				"Price Cost approved is selected in pricing type");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Price_PricingType_Tired_ID),
				"Tiered radio button is missing");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Save_Btn_ID), "Save Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Back_Btn_ID), "Back Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Next_Btn_ID), "Next Button is missing ");

		// Steps 70

		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_N_Notification_Checkbox_ID),
				"Send Reorder Point Email- check-box not displays");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("label", "Send Reorder Point Email")),
				"Send Reorder Point Email-  Message not displays after the check-box");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_N_BusinessDaysLeft_ID, "value").trim().equalsIgnoreCase("0"),
				"0 displays in 'Business Days Left' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_N_AtQuantityLevel_ID, "value").trim().equalsIgnoreCase("0"),
				"0 displays in 'At Quantity Level' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_N_Re_OrderQuantity_ID, "value").trim().equalsIgnoreCase("0"),
				"0 displays in 'Order Quantity' field");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_N_AddEmail_Btn_ID), "Add Email btn is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_N_RefereshBtn_ID), "Refresh btn Is missing");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Save_Btn_ID), "Save Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Back_Btn_ID), "Back Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Next_Btn_ID), "Next Button is missing ");

		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Max Order Quantity")),
				"'Max Order Quantity' header  Not displayed");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_RT_AllowbackOrder_ID), "Allow Backorders? is not selected ");
		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_RT_SelfMailer_ID),
				"Can be Self Mailer?:	 is selected in Rules");

		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_RT_Removeduplicate_ID),
				"Can Remove Duplicates When Used As Child Item?: is selected in Rules");
		Assert.assertFalse(Element_Is_selected(ID, Pimco_IM_RT_OrderContianer_ID),
				"Can be Order Container?: is selected in Rules");

		Assert.assertTrue(
				Get_Attribute(Xpath, Pimco_IM_RT_DefaultMaxOrderQty_Path, "value").trim().equalsIgnoreCase("0"),
				"0 not displays in 'Default Maximum Order Quantity:	' field");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_RT_NoMax_Path), "0 = No Max is not Displayed");

		Assert.assertTrue(Get_Attribute(Xpath, Pimco_IM_RT_DefaultMaxOrderQtyFrq_Path, "value").trim()
				.equalsIgnoreCase("Per Order"), "Per Order not displayed on Default Maximum Order Quantity Frequency");

		Assert.assertTrue(
				Get_Attribute(Xpath, Pimco_IM_RT_MaxOrderQtyPerRole_Field_Path, "value").trim()
						.equalsIgnoreCase("Max Qty for Role"),
				"Max Qty for Role  : not displayed in the Maximum Order Quantity Per Role field");

		Assert.assertTrue(
				Get_Attribute(Xpath, Pimco_IM_RT_MaxOrderQtyPerRole_Drop_Path, "value").trim()
						.equalsIgnoreCase("Please select one"),
				"Please select one not displayed on Maximum Order Quantity Per Role dropdown ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_RT_MaxOrderQtyPerRole_Add_Path),
				"Add button not displayed on Maximum Order Quantity Per Role dropdown ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_RT_Superseding_Path), "Supperseding header is missing");

		Assert.assertTrue(Get_Attribute(Xpath, Pimco_IM_RT_ReplaceType_Path, "value").trim().equalsIgnoreCase("None"),
				"None is not displayed in the replaceType Dropdown");

		Assert.assertTrue(Get_Attribute(Xpath, Pimco_IM_RT_ReplaceDate_Path, "value").isEmpty(),
				"Replace Date is not empty");

		Assert.assertTrue(Get_Attribute(Xpath, Pimco_IM_RT_ObsoleteDate_Path, "value").isEmpty(),
				"Obsolete Date is not empty");

		Assert.assertTrue(Get_Attribute(Xpath, Pimco_IM_RT_Reason_Field_Path, "value").isEmpty(),
				"Reason TextBox is not empty");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_RT_UnObsoleteNowbtn_Path),
				"Un-Obsolete Button is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("*", "Product Code")),
				"Product Code is missing in the required items grid");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("*", "Quantity")),
				"Quantity is missing in the required items grid");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("*", "Content Location")),
				"Current location  is missing in the required items grid");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Refresh")),
				"Refresh button is missing in the required items section");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Add Item")),
				"Add item button is missing in the required items section");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Firm Name")), "Firm name is missing ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Firm ID")), "Firm ID is missing ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Firm Approval Code")),
				"Firm Approval Code is missing ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Firm Expiration Date")),
				"Firm Expiration Date is missing ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("th", "Remove")), "Remove is missing ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Max Order Quantity Details")),
				"Max Order Quantity Details is missing ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Frequency")), "Frequency is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Role Name")), "Role Name is missing ");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Max Order Quantity")),
				"Max Order Quantity is missing");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Save_Btn_ID), "Save Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Back_Btn_ID), "Back Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Next_Btn_ID), "Next Button is missing ");

		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_UnitsonHand_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed Units on Hand");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_UnitsAvailable_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed Units Available");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_Allocated_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed Allocated");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_BackOrderQTY_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed BackOrder QTY");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_MTD_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed MTD");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_YTD_Path).trim().equalsIgnoreCase("0"),
				"0 is not displayed YTD");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_AverageMonthlyUsage_Path).trim().equalsIgnoreCase("0"),
				"Average Monthly Usage");
		Assert.assertTrue(Get_Text(Xpath, Pimco_IM_Metrics_AverageMonthlyDownloads_Path).trim().equalsIgnoreCase("0"),
				"Average Monthly Downloads");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Back_Btn_ID), "Back Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Next_Btn_ID), "Next Button is missing ");

		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_ChangeHistory_Date_Path), "Date Coloumn is missing ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_ChangeHistory_User_Path), "user coloumn is missing");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_ChangeHistory_Action_Path), "Action coloumn is missing");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Back_Btn_ID), "Back Button is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Next_Btn_ID), "Next Button is missing ");

		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Pageflex_FriendlyName_Path),
				"Frindly Name field is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_FriendlyName_Textbox_ID),
				"Frindly Name Text box is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_FriendlyName_Filter_ID),
				"Friendly Filter is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Pageflex_FieldGroup_Path),
				"Field Group coloumn is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_FieldGroup_TextBox_ID),
				"Field group textbox is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_FieldGroup_Filter_ID),
				"Field Group Filter is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Pageflex_MaxLength_Path),
				"max Length coloumn is missing ");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_MaxLength_TextBox_ID),
				"Maxlength textbox is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_MaxLength_FilterID),
				"Maxlength filter is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Pageflex_Visible_Path), "Visible coloumn is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_Visible_Checkbox_ID),
				"Visible Checkbox is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_Visible_Filter_ID), "Visible filter is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Pageflex_Required_Path), "Required coloumn is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_Required_Checkbox_ID),
				"Required Checkbox is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_Required_Filter_ID), "Required Filter is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pimco_IM_Pageflex_Active_Path), "Active Coloumn is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_Active_Checkbox_ID), "Active Checkbox is missing");
		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Pageflex_Active_Filter_ID), "Active Filter is missing");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Refresh")),
				"Refresh button is missing in the required items section");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Add new record")),
				"Add new record button is missing in the required items section");

		Assert.assertTrue(Element_Is_Displayed(ID, Pimco_IM_Save_Btn_ID), "Save Button is missing ");

		// Pimco.TC.2.4.9.3.5

		// Validate that not filling out all required fields on Pricing Tab
		// doesn't enable you to move to another tab
		Click(Xpath, Textpath("span", "Pricing"));
		Wait_ajax();

		Click(ID, Pimco_IM_Price_NocostItem_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Print Cost must be greater than zero!")),
				"Print Cost must be greater than zero! - message is missing");

		// Pimco.TC.2.4.9.3.6

		// Validate that filling out all required fields on Pricing tab enables
		// you to move to another tab

		Type(ID, Pimco_IM_PricePrintProduction_ID, "0.01");

		Click(ID, Pimco_IM_Next_Btn_ID);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Notification Options")),
				"Notification Options header  Not displayed");

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

	}

	// Validate that a part can be obsolete
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_3_7() throws InterruptedException, IOException {
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_3_3_Piece);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, ClickButtononSearchresultspath(Pimco_TC_2_4_9_3_3_Piece, "Edit"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Rules")), "Rules header  Not displayed");

		Type(Xpath, Pimco_IM_RT_ObsoleteDate_Path, Get_FutureCSTTime("M/d/YYYY HH:mm:ss a"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Change History")),
				"Change History header  Not displayed");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringContainsSingleQuote("td", "ObsoleteDate changed from '12:00:00 AM' to")),
				"ObsoleteDate changed from '12:00:00 AM' to - is missing in the history");

		logout();

		login(PIMCOBothUsername, PIMCOBothPassword);

		FulfilmentSearch(Pimco_TC_2_4_9_3_3_Piece);
		Thread.sleep(6000);
		ContactClick("QanoFirm", "IL");
		Thread.sleep(6000);

		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", Pimco_SR_PieceNotAvailable_Status_Message)),
				"Please review filter and category selections or clear filters to retry search. The item requested may not be available due to Firm Restrictions or may no longer be Active. message  Not appears ");

		Click(Xpath, Textpath("span", "Clear Cart"));

	}

	// Validate that a part can be unobsolete

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_3_8ANDPimco_TC_2_4_9_3_9() throws InterruptedException, IOException {
		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_3_3_Piece);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Click(Xpath, Textpath("li", "Obsolete"));
		Click(ID, Pimco_IM_Search_Btn_ID);

		Click(Xpath, ClickButtononSearchresultspath(Pimco_TC_2_4_9_3_3_Piece, "Edit"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Rules")), "Rules header  Not displayed");

		Click(Xpath, Pimco_IM_RT_UnObsoleteNowbtn_Path);

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		logout();

		login(PIMCOBothUsername, PIMCOBothPassword);

		ManageinventoryNav();
		Wait_ajax();
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_3_3_Piece);
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

		Click(Xpath, ClickButtononSearchresultspath(Pimco_TC_2_4_9_3_3_Piece, "Edit"));
		Wait_ajax();

		// Pimco.TC.2.4.9.3.9
		// Validate appropriate changes are recorded in Change History

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Change History")),
				"Change History header  Not displayed");

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Reactivating Part")),
				"Reactivating Part message is missing");

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Rules")), "Rules header  Not displayed");

		Type(Xpath, Pimco_IM_RT_ObsoleteDate_Path, Get_FutureCSTTime("M/d/YYYY HH:mm:ss a"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

	}

	@Test(enabled = false)
	public void Pimco_TC_2_4_9_4_1Precondition() throws InterruptedException {

		ManageinventoryNav();
		Wait_ajax();

		Pimco_TC_2_4_9_4_1_Piece = "QA_Copy_NewItem_AT_1";
		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_3_3_Piece);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Pimco_TC_2_4_9_4_1_Piece = "QA_Copy_NewItem_AT_1";

		while (Element_Is_Displayed(Xpath, Pimco_IM_SearchResults_Firstresult_Path)) {

			Pimco_TC_2_4_9_4_1_Piece1 = Integer
					.parseInt(Pimco_TC_2_4_9_4_1_Piece.substring(19, Pimco_TC_2_4_9_4_1_Piece.length())) + 1;
			Pimco_TC_2_4_9_4_1_Piece = "QA_Copy_NewItem_AT_1" + Pimco_TC_2_4_9_4_1_Piece1;

			Type(ID, Pimco_IM_ProductCode_TxtBox_ID, Pimco_TC_2_4_9_4_1_Piece);
			Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
			Wait_ajax();
			Click(Xpath, li_value("- Any -"));
			Click(ID, Pimco_IM_Search_Btn_ID);
			Wait_ajax();
		}
	}

	// Validate when copying a part the original part name appears in
	// predecessor field and part number field is able to be edited
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_4_1() throws InterruptedException {
		ManageinventoryNav();
		Wait_ajax();

		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, "QA_Copy_NewItem_AT_1");
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath("QA_Copy_NewItem_AT_1", "Copy"));

		Click(Xpath, Pimco_IM_Popuop_OKBtn_Path);
		Wait_ajax();

		Type(ID, Pimco_IM_ProductCode_ID, Pimco_TC_2_4_9_4_1_Piece);

		Click(ID, Pimco_IM_DD_Viewability_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("Orderable"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("textarea", "QA Test")),
				"QA Test Not displays in 'Title' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("textarea", "THIS IS A QA TEST")),
				"THIS IS A QA TEST Not displays in 'Synopsis' field");

		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_Predecessor_Txtbox_ID, "value").trim().equalsIgnoreCase(
				"QA_Copy_SearchResults_X"), "QA_Copy_SearchResults_X Not displays in 'Predecessor' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlInventoryType_ID, "value").trim().equalsIgnoreCase("POD"),
				"POD displays in 'Inventory Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_UnitsPerPack_TxtBox_ID, "value").trim().equalsIgnoreCase("1"),
				"1 displays in 'Units Per Pack' field");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlDocumentType_ID, "value").trim().equalsIgnoreCase("Brochure"),
				"Form not displays in the 'Document Type' dropdown");
		Assert.assertTrue(Get_Attribute(ID, Pimco_IM_ddlFormOwner_ID, "value").trim().equalsIgnoreCase("Marketing"),
				"Marketing is Not selected in 'Form Owner' dropdown");
		// Assert.assertTrue(Get_Attribute(ID, Pimco_IM_DD_Country_Section_ID,
		// "value").trim().equalsIgnoreCase("Both"),"Both displays in 'Country'
		// field");

		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_Fulfillment_CheckBox_ID),
				"Fulfillment checkbox is checked off in 'View In' field");
		Assert.assertTrue(Element_Is_selected(ID, Pimco_IM_BulkOrders_CheckBox_ID),
				"Bulk Order checkbox is checked off in 'View In' field");

		// Pimco.TC.2.4.9.4.3

		// Validate appropriate changes are recorded in change history

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Change History")),
				"Change History header  Not displayed");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td",
								"FormNumber changed from 'Copy Of QA_Copy_NewItem_1' to 'QA_Copy_NewItem_(X)'")),
				"FormNumber changed from 'Copy Of QA_Copy_NewItem_1' to 'QA_Copy_NewItem_(X)' message is missing");

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringWithSingleQuote("td", "Viewability changed from 'NONE' to 'ViewableAndOrderable'")),
				"Viewability changed from 'NONE' to 'ViewableAndOrderable' message is missing");

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("h1", "Rules")), "Rules header  Not displayed");

		Type(Xpath, Pimco_IM_RT_ObsoleteDate_Path, Get_FutureCSTTime("M/d/YYYY HH:mm:ss a"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		Click(Xpath, Textpath("span", "Change History"));
		Wait_ajax();

		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						XpathStringContainsSingleQuote("td", "ObsoleteDate changed from '12:00:00 AM' to")),
				"ObsoleteDate changed from '12:00:00 AM' to message is missing");

	}

	// Validate a part can be replaced
	@Test(enabled = false)
	public void Pimco_TC_2_4_9_4_4() throws InterruptedException, IOException {

		ManageinventoryNav();
		Wait_ajax();

		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, qa_kitonfly_4);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(qa_kitonfly_4, "Edit"));

		Click(Xpath, Textpath("span", "Kitting"));
		Wait_ajax();

		Click(Xpath, Pimco_IM_KT_ViewDetails_Btn_Xpath("qa_replace_1", "View Details"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "OK"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Type(Xpath, PIMCO_MI_Rules_ReplaceType_Path, "qa_replace_ ");
		Wait_ajax();
		Click(Xpath, Textpath("li", "qa_replace_2"));
		Thread.sleep(6000);

		Type(Xpath, PIMCO_MI_Rules_ReplaceDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		logout();
		login(PIMCOBothUsername, PIMCOBothPassword);

		ManageinventoryNav();
		Wait_ajax();

		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, qa_kitonfly_4);
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath(qa_kitonfly_4, "Edit"));

		Assert.assertTrue(
				Element_Is_Displayed(Xpath, Pimco_IM_KT_ViewDetails_Btn_Xpath("qa_replace_2", "View Details")),
				"ObsoleteDate changed from '12:00:00 AM' to message is missing");

		Click(Xpath, Pimco_IM_KT_ViewDetails_Btn_Xpath("qa_replace_2", "Remove"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "OK"));
		Wait_ajax();

		Type(Xpath, PIMCO_MI_Kitting_ProductCode_Path, "qa_replace_ ");
		Wait_ajax();
		Click(Xpath, Textpath("li", "qa_replace_1"));
		Thread.sleep(6000);

		Click(Xpath, PIMCO_MI_Kitting_Type_Arrow_Path);

		Wait_ajax();
		Click(Xpath, Textpath("li", "Component"));
		Thread.sleep(6000);

		Click(Xpath, PIMCO_MI_Kitting_Location_Arrow_Path);

		Wait_ajax();
		Click(Xpath, Textpath("li", "Inside"));
		Thread.sleep(6000);

		Click(Xpath, PIMCO_MI_Kitting_Perkitck_Path);

		Type(Xpath, PIMCO_MI_Kitting_Qty_Path, "1");

		Type(Xpath, PIMCO_MI_Kitting_Sortorder_Path, "1");

		Click(Xpath, PIMCO_MI_Kitting_Additembtn_Path);

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		Click(Xpath, Pimco_IM_KT_ViewDetails_Btn_Xpath("qa_replace_1", "View Details"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "OK"));
		Wait_ajax();

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Click(Xpath, Textpath("a", "Un-Obsolete"));
		Wait_ajax();

		logout();
		login(PIMCOBothUsername, PIMCOBothPassword);

		ManageinventoryNav();
		Wait_ajax();

		Type(ID, Pimco_IM_ProductCode_TxtBox_ID, "qa_replace_1");
		Click(ID, Pimco_IM_StatusDrowpdown_Arrow_ID);
		Wait_ajax();
		Click(Xpath, li_value("- Any -"));
		Click(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();
		Click(Xpath, ClickButtononSearchresultspath("qa_replace_1", "Edit"));

		Click(Xpath, Textpath("span", "Rules"));
		Wait_ajax();

		Click(Xpath, PIMCO_MI_Rules_ReplaceType_Icon_Path);
		Wait_ajax();
		Click(Xpath, li_value("None"));

		Clear(Xpath, PIMCO_MI_Rules_ReplaceWith_Field_Path);

		Click(ID, Pimco_IM_Save_Btn_ID);
		Wait_ajax();
		Click(ID, Pimco_IM_PMSG_OK_Button_ID);
		Wait_ajax();

		logout();
	}

	public void ManageinventoryNav() throws InterruptedException {
		Hover(Xpath, Textpath("span", "Admin"));
		Click(Xpath, Textpath("span", "Manage Inventory"));
		ExplicitWait_Element_Clickable(ID, Pimco_IM_Search_Btn_ID);
		Wait_ajax();

	}

	@BeforeMethod
	public void beforeMethod() {

		softAssert = new SoftAssert();
	}

	@BeforeTest
	public void PimcoGWMLogin() throws IOException, InterruptedException {
		// System.out.println(Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		login(PIMCOBothUsername, PIMCOBothPassword);

	}

}
