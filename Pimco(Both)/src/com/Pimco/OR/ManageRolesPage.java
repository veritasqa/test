package com.Pimco.OR;

public class ManageRolesPage extends ManagePhrasesPage {

	// manage Roles -MR
	public static final String Pimco_MR_CreateSecurityRole_Btn_ID = "cphContent_btnCreateSecurityRole";

	// Security Tab
	public static final String Pimco_MR_Securitytab_Path = ".//span[text()='Security']";

	// Others Tab
	public static final String Pimco_MR_Otherstab_Path = ".//span[text()='Others']";
	public static final String Pimco_MR_Otherstab_Members_Path = ".//div[contains(@id,'cphContent_gridOtherUsersPanel')]//th[text()='Members']";
	public static final String Pimco_MR_CreateRole_Btn_ID = "cphContent_btnCreateRole";

	public static final String Pimco_MR_AudienceDropDown_ID = "cphContent_ddlRoleTypes";
}
