package com.Pimco.OR;

public class ShoppingCart extends SearchResultsPage {

	public static final String Pimco_SCP_CostCenter_Dropdown_ID = "cphContent_ddlCostCenter";
	public static final String Pimco_SCP_Remove_btn_Path = ".//a[text()='Remove']";
	public static final String Pimco_SCP_EditVariables_btn_Path = ".//a[text()='Edit Variables']";
	public static final String Pimco_SCP_Proof_btn_Path = ".//a[text()='Proof']";
	public static final String Pimco_SCP_Proofed_btn_Path = ".//a[text()='Proofed']";
	public static final String Pimco_SCP_Qty_Path = ".//input[contains(@id,'txtQuantity') and @type='text']";
	public static final String Pimco_SCP_Update_Path = ".//a[text()='Update']";

	public static final String Pimco_SCP_Proof1_btn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[5]/a[4]";
	public static final String Pimco_SCP_Proof2_btn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__1']/td[5]/a[4]";

	// Shopping Cart Page - SCP

	public static final String Pimco_SCP_OrderConfirmation_Checkbox_ID = "cphContent_cbxOrderConfirmation";
	public static final String Pimco_SCP_ShippedConfirmation_Checkbox_ID = "cphContent_cbxShippedConfirmation";

	public static final String Pimco_SCP_CreateOrderTemplate_Btn_Path = ".//a[text()='Create Order Template']";
	public static final String Pimco_SCP_ClearCart_Btn_ID = "cphContent_btnClearCart";
	public static final String Pimco_SCP_Next_Btn_ID = "cphContent_btnShowShipping";
	public static final String Pimco_SCP_Remove_Btn_ID= "ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete";

	// Shipping Method

	public static final String Pimco_SCP_ShippingLowCostMethod_Checkbox_ID = "cphContent_rdoShippingLowCostMethod";
	public static final String Pimco_SCP_DeliveryDate_CheckBox_ID = "cphContent_rdoDeliveryDate";
	public static final String Pimco_SCP_DeliveryDate_popupButton_ID = "ctl00_cphContent_rdtepkrDeliveryDate_popupButton";
	public static final String Pimco_SCP_Calendar_Title_ID = "ctl00_cphContent_rdtepkrDeliveryDate_calendar_Title";
	public static final String Pimco_SCP_Calendar_OK_Btn_ID = "rcMView_OK";
	public static final String Pimco_SCP_DeliveryMethod_Path = ".//span[text()='UPS Ground']";

	public static final String Pimco_SCP_RecipientNotes_ID = "cphContent_txtShippingInstructions";

	public static final String Pimco_SCP_Checkout_ID = "cphContent_btnCheckout";

	// Create Order Template Pop up
	public static final String Pimco_SCP_COT_Path = ".//div[@class='Box']";
	public static final String Pimco_SCP_COT_Title_Path = ".//div[contains(text(),'Create Order Template')]";
	public static final String Pimco_SCP_COT_Name_Path = ".//input[contains(@id,'txtOrderTemplateName') and @type='text']";
	public static final String Pimco_SCP_COT_Description_Path = ".//textarea[contains(@id,'txtOrderTemplateDescription')]";
	public static final String Pimco_SCP_COT_Createbtn_Path = ".//a[text()='Create']";
	public static final String Pimco_SCP_COT_Cancelbtn_Path = ".//a[text()='Cancel']";
public static final String Pimco_SCP_Loading_Path = ".//*[@id='cphContent_RadAjaxLoadingPanelctl00_cphContent_rgrdShoppingCart']";

//Shipping Method 

	
	public static final String Pimco_SCP_ExpeditedDeliveryMethod_Checkbox_ID = "cphContent_rdoExpeditedDeliveryMethod";
	public static final String Pimco_SCP_ExpeditedDeliveryMethod_DropDown_ID = "cphContent_ddlExpeditedDeliveryMethod";
	
	//OrderTemplate
	public static final String Pimco_SCP_CreateOrderTemplate_Btn_ID = "cphContent_btnShowCreateOrderTemplatePopup";
	public static final String Pimco_SCP_OrderTemplateName_TxtBox_ID ="ctl00_cphContent_txtOrderTemplateName";
	public static final String Pimco_SCP_OrderTemplateDescription_TxtBox_ID ="ctl00_cphContent_txtOrderTemplateDescription";
	public static final String Pimco_SCP_OrderTemplate_Create_Btn_ID ="cphContent_btnCreateOrderTemplate";
	public static final String Pimco_SCP_OrderTemplate_Cancel_Btn_ID ="cphContent_btnHideCreateOrderTemplatePopup";
	

	public static final String Pimco_SCP_Address1_TxtBox_ID= "cphContent_txtAddress1";
	public static final String Pimco_SCP_Address2_TxtBox_ID= "cphContent_txtAddress2";
	public static final String Pimco_SCP_Address3_TxtBox_ID= "cphContent_txtAddress3";
	public static final String Pimco_SCP_City_TxtBox_ID= "cphContent_txtCity";
	public static final String Pimco_SCP_State_TxtBox_ID= "cphContent_txtState";
	public static final String Pimco_SCP_Zip_TxtBox_ID= "cphContent_txtZip";
	public static final String Pimco_SCP_Country_Dropdown_ID= "cphContent_ddlCountry";
	public static final String Pimco_SCP_Phone_TxtBox_ID= "cphContent_txtPhone";
	
	public static final String Pimco_SCP_PopupMessage_Path= "//ul[@class='ValidationErrorsBox']//li[text()='This order is requesting an upgraded shipping method which requires approval.']";
	

//	public static final String Pimco_SCP_Popup_Message_Path	= "";
	public static final String Pimco_SCP_Popup_Ok_Btn_Path	= "//td[@class='rwWindowContent']//span[text()='OK']";
	public static final String Pimco_SCP_Popup_Cancel_Btn_Path	= "//td[@class='rwWindowContent']//span[text()='Cancel']";

//frame component details
	public static final String Pimco_SCP_Showcomponent_FrameName ="oRadWindowComponentDetails";
	public static final String Pimco_SCP_Showcomponent_Frame_CloseBtn_Path= "//a[@title='Close']";
	
	public static final String Pimco_SCP_BackorderMsg= "Your order includes backordered Kit Components. The Kit will not ship until the backorder can be filled or the backordered pieces are removed. To proceed without changes and put the Kit on Hold until the backordered item arrives, click Accept below.";
	public static final String Pimco_SCP_BackorderAcceptBtn_ID="cphContent_cbxAccept";
}
