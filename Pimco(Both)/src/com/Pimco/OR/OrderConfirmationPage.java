package com.Pimco.OR;

public class OrderConfirmationPage extends ManageusersOR{
 
	//Order Confirmation- OC
	
	public static final String Pimco_OC_OrderNumber_ID ="cphContent_cphSection_lblOrderNumber";
	
	public static final String Pimco_OC_Printfile1_Path ="//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10_ctl04_lblProofName']";

	public static final String Pimco_OC_Printfile2_Path ="//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10_ctl10_hlProof']";

	public static final String Pimco_OC_Status_ID= "cphContent_cphSection_lblStatus";
	public static final String Pimco_OC_HoldForComplete_ID= "cphContent_cphSection_lblHoldForComplete";
	public static final String Pimco_OC_OrderDate_ID= 	"cphContent_cphSection_lblCreateDate";
	public static final String Pimco_OC_CostCenter_ID= 	"cphContent_cphSection_lblBilledToCostCenter";
	public static final String Pimco_OC_ShippedTo_ID= "cphContent_cphSection_lnkGoodContacts";
	public static final String Pimco_OC_ShippedBy_ID= "cphContent_cphSection_lblShippedBy";
	public static final String Pimco_OC_TotalChargebackCost= 	"cphContent_cphSection_lblTotalChargeBackCost";
	public static final String Pimco_OC_SubmitCancelRequest_Btn ="CancelOrder";
	
	public static final String Pimco_OC_Copy_Btn_ID = "cphContent_cphPageFooter_btnCopy";
	public static final String Pimco_OC_OK_Btn_ID = "cphContent_cphPageFooter_btnOk";
	
	//Order Cancel Button -POPUP
		public static final String Pimco_OC_Close_btn_ID= "cphContent_cphSection2_btnClose";
}
