package com.Pimco.OR;

public class InventoryManagementPage extends CustomizableVariablesOR {

	public static String Pimco_TC_2_4_9_1_2_Piece = "";
	public static String Pimco_TC_2_4_9_1_3_Piece = "";
	public static String Pimco_TC_2_4_9_1_4_Piece = "";
	public static String Pimco_TC_2_4_9_3_3_Piece = "";
	public static int Pimco_TC_2_4_9_3_3_Piece1;

	public static String Pimco_TC_2_4_9_4_1_Piece = "";
	public static int Pimco_TC_2_4_9_4_1_Piece1;

	// Inventory Management - IM

	// Create New Item
	public static final String Pimco_IM_CreateNewItem_Section_Path = "//*[@id='cphContent_pnlCommand']/div[1]/div[1]";

	public static final String Pimco_IM_CreateNewtype_Input_ID = "ctl00_cphContent_ddlNewType_Input";

	public static final String Pimco_IM_CreateNewtype_Arrow_ID = "ctl00_cphContent_ddlNewType_Arrow";

	public static final String Pimco_IM_CreateNewtype_Add_Btn_ID = "cphContent_btnAdd";

	public static final String Pimco_IM_ToCopy_TxtBox_ID = "ctl00_cphContent_ddlCopy_Input";

	public static final String Pimco_IM_CreateNewtype_Copy_Btn_ID = "cphContent_btnCopy";

	// Search Items
	public static final String Pimco_IM_SearchItems_Section_Path = "//*[@id='cphContent_pnlSearch']/div[1]/div[1]";

	public static final String Pimco_IM_ProductCode_TxtBox_ID = "ctl00_cphContent_txtFormNumber";

	public static final String Pimco_IM_Title_TxtBox_ID = "ctl00_cphContent_txtDescription";

	public static final String Pimco_IM_Keyword_TxtBox_ID = "ctl00_cphContent_txtKeyWord";

	public static final String Pimco_IM_InventoryTypeDropdown_ID = "ctl00_cphContent_ddlType_Input";

	public static final String Pimco_IM_InventoryTypeDropdown_Arrow_ID = "ctl00_cphContent_ddlType_Arrow";

	public static final String Pimco_IM_FormownerDD_ID = "ctl00_cphContent_ddlFormOwner_Input";
	public static final String Pimco_IM_FormownerDD_Arrow_ID = "ctl00_cphContent_ddlFormOwner_Arrow";

	public static final String Pimco_IM_CatagoryDrowpdown_ID = "ctl00_cphContent_ddlCategory_Input";
	public static final String Pimco_IM_CatagoryDrowpdown_Arrow_ID = "ctl00_cphContent_ddlCategory_Arrow";

	public static final String Pimco_IM_UserGroupsDrowpdown_ID = "ctl00_cphContent_ddlAudience_Input";
	public static final String Pimco_IM_UserGroupsDrowpdown_Arrow_ID = "ctl00_cphContent_ddlAudience_Arrow";

	public static final String Pimco_IM_StatusDrowpdown_ID = "ctl00_cphContent_ddlActive_Input";
	public static final String Pimco_IM_StatusDrowpdown_Arrow_ID = "ctl00_cphContent_ddlActive_Arrow";

	public static final String Pimco_IM_UnitofHandDrowpdown_ID = "ctl00_cphContent_ddlUOH_Input";
	public static final String Pimco_IM_UnitofHandDrowpdown_Arrow_ID = "ctl00_cphContent_ddlUOH_Arrow";

	public static final String Pimco_IM_Search_Btn_ID = "cphContent_btnSearch";

	public static final String Pimco_IM_Clear_Btn_ID = "cphContent_btnClear";

	// Search results

	public static final String Pimco_IM_SearchResults_Section_Path = "//*[@id='divContent clearfix']/div[8]/div[1]";
	public static final String Pimco_IM_SearchResults_Createdate_Path = "//a[text()='Create Date']";

	public static final String Pimco_IM_SearchResults_Firstresult_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]";

	public String ClickButtononSearchresultspath(String PieceName, String ButtonName) {
		return "//td[text()='" + PieceName + "']/following::a[text()='" + ButtonName + "']";

	}

	// Pagination - Page

	public String PageNumberXpath(String Pagenumber) {

		return "//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[2]/a[" + Pagenumber
				+ "]";

	}

	public static final String Pimco_IM_Page_FirstPage_Path = "//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String Pimco_IM_Page_PreviousPage_Path = "//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";

	public static final String Pimco_IM_Page_NextPage_Path = "//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String Pimco_IM_Page_LastPage_Path = "//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";

	// public static final String Pimco_IM_Page_ //div[@class="rgWrap
	// rgInfoPart"]

	// Under Search results Grid Search results / First result - - SR_FR
	public static final String Pimco_IM_SR_FR_ProductCode_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]";
	public static final String Pimco_IM_SR_FR_Status_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[3]";
	public static final String Pimco_IM_SR_FR_Type_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[4]";
	public static final String Pimco_IM_SR_FR_Title_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[5]";
	public static final String Pimco_IM_SR_FR_CreateDate_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[6]";
	public static final String Pimco_IM_SR_FR_ONHand_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[7]";
	public static final String Pimco_IM_SR_FR_UnitsAvailable_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td[8]";
	public static final String Pimco_IM_SR_FR_Copy_Btn_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td/a[text()='Copy']";
	public static final String Pimco_IM_SR_FR_Edit_Btn_Path = "//*[@id='ctl00_cphContent_Results_ctl00__0']/td/a[text()='Edit']";

	// popup window for copying orders

	public static final String Pimco_IM_Popuop_OKBtn_Path = "//span[text()='OK']";
	public static final String Pimco_IM_Popuop_CancelBtn_Path = "//span[text()='Cancel']";

	// Buttons inside general page
	public static final String Pimco_IM_Save_Btn_ID = "cphContent_btnSave";
	public static final String Pimco_IM_Cancel_Btn_ID = "cphContent_btnCancel";
	public static final String Pimco_IM_Next_Btn_ID = "cphContent_btnNext";
	public static final String Pimco_IM_Back_Btn_ID = "cphContent_btnBack";

	// Popup Message- PMSG
	public static final String Pimco_IM_PMSG_OK_Button_ID = "cphContent_NavigationBar_btnOk";

	// General TAB
	public static final String Pimco_IM_PageHeader_ID = "cphContent_lblHeader";
	public static final String Pimco_IM_ProductCode_ID = "ctl00_cphContent_txtFormNumber";
	public static final String Pimco_IM_ItemCode_Txtbox_ID = "ctl00_cphContent_txtAlias";
	public static final String Pimco_IM_JoBNumber_Txtbox_ID = "ctl00_cphContent_txtVersion";
	public static final String Pimco_IM_Title_ID = "ctl00_cphContent_txtDescription";
	public static final String Pimco_IM_Synopsis_Txtbox_ID = "ctl00_cphContent_txtLongDescription";
	public static final String Pimco_IM_Keywords_Txtbox_ID = "ctl00_cphContent_txtKeyWords";
	public static final String Pimco_IM_AsofDate_Txtbox_ID = "ctl00_cphContent_txtRevisionDate";

	public static final String Pimco_IM_Predecessor_Txtbox_ID = "ctl00_cphContent_txtPredecessor";

	public static final String Pimco_IM_ddlInventoryType_ID = "ctl00_cphContent_ddlInventoryType_Input";
	public static final String Pimco_IM_ddlInventoryType_Arrow_ID = "ctl00_cphContent_ddlInventoryType_Arrow";

	public static final String Pimco_IM_UnitsPerPack_TxtBox_ID = "ctl00_cphContent_txtUnitsPerPack";

	public static final String Pimco_IM_ddlDocumentType_ID = "ctl00_cphContent_ddlDocumentType_Input";
	public static final String Pimco_IM_ddlDocumentType_Arrow_ID = "ctl00_cphContent_ddlDocumentType_Arrow";

	public static final String Pimco_IM_UserKitContainer_CheckBox_ID = "cphContent_chkIsUserKitContainer";
	public static final String Pimco_IM_ContentSteward_Textbox_ID = "ctl00_cphContent_txtContentSteward";

	public static final String Pimco_IM_ddlFormOwner_ID = "ctl00_cphContent_ddlFormOwner_Input";
	public static final String Pimco_IM_ddlFormOwner_Arrow_ID = "ctl00_cphContent_ddlFormOwner_Arrow";

	public static final String Pimco_IM_DD_Country_Section_ID = "ctl00_cphContent_ddlNACountry_Input";
	public static final String Pimco_IM_DD_Country_Arrow_ID = "ctl00_cphContent_ddlNACountry_Arrow";

	public static final String Pimco_IM_DD_Viewability_Section_ID = "ctl00_cphContent_ddlViewability_Input";
	public static final String Pimco_IM_DD_Viewability_Arrow_ID = "ctl00_cphContent_ddlViewability_Arrow";

	public static final String Pimco_IM_PublishDate_Textbox_ID = "ctl00_cphContent_dtEffectiveDate_dateInput";

	public static final String Pimco_IM_PublishDate_CalenderIcon_ID = "ctl00_cphContent_dtEffectiveDate_popupButton";
	public static final String Pimco_IM_PublishDate_CalenderTitle_ID = "ctl00_cphContent_dtEffectiveDate_calendar_Title";
	public static final String Pimco_IM_Calender_OK_Btn_ID = "rcMView_OK";
	public static final String Pimco_IM_Fulfillment_CheckBox_ID = "cphContent_chkFulfillment";
	public static final String Pimco_IM_Marketingcampaingns_CheckBox_ID = "cphContent_chkMarketing";
	public static final String Pimco_IM_BulkOrders_CheckBox_ID = "cphContent_chkIsBulk";

	public static final String Pimco_IM_Notes_TxtBox_ID = "ctl00_cphContent_txtNotes";

	public static final String Pimco_IM_TxtBox_ID = "ctl00_cphContent_txtSpecialMessage";

	// Checkbox List
	public static final String Pimco_IM_CA_Checkbox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[1]/div/label/input";
	public static final String Pimco_IM_CanadaAdminTeam_Checkbox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[1]/ul/li[1]/div/label/input";
	public static final String Pimco_IM_CanadaFAs_Checkbox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[1]/ul/li[2]/div/label/input";
	public static final String Pimco_IM_CanadaSalesTeam_Checkbox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[1]/ul/li[3]/div/label/input";
	public static final String Pimco_IM_US_Checkbox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/div/label/input";
	public static final String Pimco_IM_Advisory_Brokerage_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[1]/div/label/input";
	public static final String Pimco_IM_AST_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[2]/div/label/input";
	public static final String Pimco_IM_Bank_Trust_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[3]/div/label/input";
	public static final String Pimco_IM_BFDS_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[4]/div/label/input";
	public static final String Pimco_IM_DC_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[5]/div/label/input";
	public static final String Pimco_IM_ETF_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[6]/div/label/input";
	public static final String Pimco_IM_Events_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[7]/div/label/input";
	public static final String Pimco_IM_FA_Clients_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[8]/div/label/input";
	public static final String Pimco_IM_Fund_Ops_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[9]/div/label/input";
	public static final String Pimco_IM_HR_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[10]/div/label/input";
	public static final String Pimco_IM_IDDG_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[11]/div/label/input";
	public static final String Pimco_IM_Managed_Accounts_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[12]/div/label/input";
	public static final String Pimco_IM_Marketing_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[13]/div/label/input";
	public static final String Pimco_IM_National_Accounts_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[14]/div/label/input";
	public static final String Pimco_IM_Other_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[15]/div/label/input";
	public static final String Pimco_IM_Regulatory_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[16]/div/label/input";
	public static final String Pimco_IM_RIA_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[17]/div/label/input";
	public static final String Pimco_IM_Sales_Associate_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[18]/div/label/input";
	public static final String Pimco_IM_VA_CheckBox_Path = "//*[@id='ctl00_cphContent_rtvAudience']/ul/li[2]/ul/li[19]/div/label/input";

	// method will identiry Xpath of * based on the option on the left
	// example "Product Code" is the field name
	// Dont give "Product Code:" give only "Product Code"
	public String AsteriskXpath(String FieldName) {
		return "//*[text()='" + FieldName + "']/following::span[text()='* ']";

	}

	// this method generates xpaths for checkboxes which are checked off under
	// categories Section

	// Example :
	// h1[text()='Categories']/following::span[text()='FINRA
	// letter']/preceding::input[1][@checked="checked"]

	public String checkedBoxXpathinCatagories(String Checkboxoption) {

		return ".//h1[text()='Categories']/following::span[text()='" + Checkboxoption
				+ "']/preceding::input[1][@checked='checked']";
	}

	public String UncheckedBoxXpathinCatagories(String Checkboxoption) {

		return "//h1[text()='Categories']/following::span[text()='" + Checkboxoption + "'][1]/preceding::input[1]";
	}

	public String CatagoriesCheckbox(String Checkboxoption) {

		return ".//h1[text()='Categories']/following::span[text()='" + Checkboxoption + "']/preceding::input[1]";
	}

	// method for creating Xpath For all checkboxes inside the page
	public String CheckBoxXpath(String Checkboxoption) {

		return ".//span[text()='" + Checkboxoption + "']/preceding::input[1][@type='checkbox']";
	}

	// h1[text()='Categories']/following::span[text()=''][1]/preceding::input[1]

	public static final String Pimco_IM_Notes_TextBox_ID = "ctl00_cphContent_txtNotes";
	public static final String Pimco_IM_SpecialMessage_TextBox_ID = "ctl00_cphContent_txtSpecialMessage";

	public static final String Pimco_IM_SplMSG_StartDate_TextBox_ID = "ctl00_cphContent_dtSpecialMessageActivationDate_dateInput";
	public static final String Pimco_IM_SplMSG_StartDate_CalenderIcon_ID = "ctl00_cphContent_dtSpecialMessageActivationDate_popupButton";
	public static final String Pimco_IM_SplMSG_StartDate_Month_ID = "ctl00_cphContent_dtSpecialMessageActivationDate_calendar_Title";

	public static final String Pimco_IM_SplMSG_EndDate_TextBox_ID = "ctl00_cphContent_dtSpecialMessageDeactivationDate_dateInput";
	public static final String Pimco_IM_SplMSG_EndDate_CalenderIcon_ID = "ctl00_cphContent_dtSpecialMessageDeactivationDate_popupButton";
	public static final String Pimco_IM_SplMSG_EndDate_Month_ID = "ctl00_cphContent_dtSpecialMessageDeactivationDate_calendar_Title";

	public static final String Pimco_IM_Calender_OKBtn_ID = "rcMView_OK";

	public static final String Pimco_IM_ShareClass_DropDown_ID = "ctl00_cphContent_ddlShareClass_Input";
	public static final String Pimco_IM_ShareClass_DropDown_Arrow_ID = "ctl00_cphContent_ddlShareClass_Arrow";

	public static final String Pimco_IM_Supplement_DropDown_ID = "ctl00_cphContent_ddlSupplementType_Input";
	public static final String Pimco_IM_Supplement_DropDown_Arrow_ID = "ctl00_cphContent_ddlSupplementType_Arrow";

	public static final String Pimco_IM_ProductDocumentIsAbout_TextBox_ID = "ctl00_cphContent_txtProductThisDocumentIsAbout";
	public static final String Pimco_IM_Authors_TextBox_ID = "ctl00_cphContent_txtAuthors";
	public static final String Pimco_IM_Topics_TextBox_ID = "ctl00_cphContent_txtTopics";
	public static final String Pimco_IM_FirmDocumentIsAbout_TextBox_ID = "ctl00_cphContent_txtFirmsThisDocumentIsAbout";

	public static final String Pimco_IM_Language_DropDown_ID = "ctl00_cphContent_ddlLanguage_Input";
	public static final String Pimco_IM_Language_DropDown_Arrow_ID = "ctl00_cphContent_ddlLanguage_Arrow";

	public static final String Pimco_IM_Departmentowner_TxtBox_ID = "ctl00_cphContent_txtDepartmentOwner";

	// Attachment Tab - AT

	/*	cphContent_uplThumbnail
		cphContent_btnThumbnail
		
		cphContent_uplSample
		cphContent_btnSample
		
		cphContent_uplFINRA
		cphContent_btnFINRA */

	public static final String Pimco_IM_AT_Attachmenttab_Path = ".//span[text()='Attachments']";
	public static final String Pimco_IM_AT_BPP_Browse_Path = ".//*[@id='SWFUpload_1']";
	public static final String Pimco_IM_AT_BPP_Remove_Path = ".//*[@id='PreflightUploadForBook_lnkRemovePdf']";
	public static final String Pimco_IM_AT_BPP_Upload_Path = "//*[@id='PreflightUploadForBook_btnUpload']";
	public static final String Pimco_IM_AT_ProductThumbnail_Choosefiles_ID = "cphContent_uplThumbnail";
	public static final String Pimco_IM_AT_ProductThumbnail_Upload_ID = "cphContent_btnThumbnail";

	public static final String Pimco_IM_AT_OnlineSample_Choosefiles_ID = "cphContent_uplSample";
	public static final String Pimco_IM_AT_OnlineSample_Upload_ID = "cphContent_btnSample";

	public static final String Pimco_IM_AT_FinraLetter_Choosefiles_ID = "cphContent_uplFINRA";
	public static final String Pimco_IM_AT_Finraletter_Upload_ID = "cphContent_btnFINRA";

	public static final String Pimco_IM_AT_PrintReadyPdf_Choosefilebtn_ID = "PreflightUpload_fuPdf";
	public static final String Pimco_IM_AT_BookPrint_Choosefilebtn_ID = "PreflightUploadForBook_fuPdf";

	public static final String Pimco_IM_AT_Colors_DD_ID = "ctl00_cphContent_ddlColors_Input";
	public static final String Pimco_IM_AT_PageSize_DD_ID = "ctl00_cphContent_ddlPageSize_Input";
	public static final String Pimco_IM_AT_Stock_DD_ID = "ctl00_cphContent_ddlStock_Input";
	public static final String Pimco_IM_AT_Finish_DD_ID = "ctl00_cphContent_ddlFinish_Input";
	public static final String Pimco_IM_AT_Coating_DD_ID = "ctl00_cphContent_ddlCoating_Input";

	public static final String Pimco_IM_AT_NumberofPages_ID = "ctl00_cphContent_txtImpressions";

	public static final String Pimco_IM_AT_Biding_DD_ID = "ctl00_cphContent_ddlBinding_Input";
	public static final String Pimco_IM_AT_PrintMethod_DD_ID = "ctl00_cphContent_ddlPrintMethod_Input";

	public static final String Pimco_IM_AT_SheetSize_Textbox_ID = "ctl00_cphContent_txtSheetSize";
	public static final String Pimco_IM_AT_Composition_Txtbox_ID = "ctl00_cphContent_txtComposition";
	public static final String Pimco_IM_AT_FinishSize_Textbox_ID = "ctl00_cphContent_txtFinishSize";

	public static final String Pimco_IM_AT_PcsPrinttype_DD_ID = "ctl00_cphContent_ddlPCSPrintType_Input";
	public static final String Pimco_IM_AT_CoverStock_DD_ID = "ctl00_cphContent_ddlCoverStock_Input";

	// Notifications - N

	public static final String Pimco_IM_N_Notification_Checkbox_ID = "cphContent_chkNotificationEmail";

	public static final String Pimco_IM_N_BusinessDaysLeft_ID = "ctl00_cphContent_txtDaysLeft";

	public static final String Pimco_IM_N_AtQuantityLevel_ID = "ctl00_cphContent_txtQuantity";

	public static final String Pimco_IM_N_Re_OrderQuantity_ID = "ctl00_cphContent_txtOrderQuantity";

	public static final String Pimco_IM_N_AddEmail_Btn_ID = "ctl00_cphContent_Addresses_ctl00_ctl03_ctl01_InitInsertButton";

	public static final String Pimco_IM_N_RefereshBtn_ID = "ctl00_cphContent_Addresses_ctl00_ctl03_ctl01_RebindGridButton";

	// Rules tab - RT

	public static final String Pimco_IM_RT_AllowbackOrder_ID = "cphContent_chkBackorder";
	public static final String Pimco_IM_RT_SelfMailer_ID = "cphContent_ChkSelfMailer";
	public static final String Pimco_IM_RT_Removeduplicate_ID = "cphContent_chkRemoveDuplicates";
	public static final String Pimco_IM_RT_OrderContianer_ID = "cphContent_ChkOrderContainer";

	public static final String Pimco_IM_RT_DefaultMaxOrderQty_Path = ".//*[@id='ctl00_cphContent_txtMaxOrderQty']";
	public static final String Pimco_IM_RT_NoMax_Path = "//td[contains(.,'0 = No Max')]";
	public static final String Pimco_IM_RT_DefaultMaxOrderQtyFrq_Path = ".//*[@id='ctl00_cphContent_ddlMaxOrderQuantityFrequency_Input']";
	public static final String Pimco_IM_RT_DefaultMaxOrderQtyFrq_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlMaxOrderQuantityFrequency_Arrow']";
	public static final String Pimco_IM_RT_MaxOrderQtyPerRole_Field_Path = ".//*[@id='ctl00_cphContent_txtMaxOrderQtyRole']";
	public static final String Pimco_IM_RT_MaxOrderQtyPerRole_Drop_Path = ".//*[@id='ctl00_cphContent_cboMaxOrderQtyRole_Input']";
	public static final String Pimco_IM_RT_MaxOrderQtyPerRole_Add_Path = ".//*[@id='btnAddMaxOrderQty']";
	// public static final String Pimco_IM_RT_AllowBackordersRadio_Path =
	// ".//*[@id='ctl00_cphContent_chkBackorder']";
	// public static final String Pimco_IM_RT_ChildItem_Path =
	// ".//*[@id='ctl00_cphContent_chkBackorder']";
	public static final String Pimco_IM_RT_Superseding_Path = ".//h1[text()='Superseding']";
	public static final String Pimco_IM_RT_ReplaceType_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Input']";
	public static final String Pimco_IM_RT_ReplaceType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Arrow']";
	public static final String Pimco_IM_RT_ReplaceWith_Field_Path = ".//*[@id='ctl00_cphContent_ddlReplaceWith_Input']";
	public static final String Pimco_IM_RT_ReplaceDate_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_dateInput']";
	public static final String Pimco_IM_RT_ReplaceDateCalender_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_popupButton']";
	public static final String Pimco_IM_RT_ReplaceDateTime_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_timePopupLink']";
	public static final String Pimco_IM_RT_ObsoleteDate_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_dateInput']";
	public static final String Pimco_IM_RT_ObsoleteDateCaleder_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_popupButton']";
	public static final String Pimco_IM_RT_ObsoleteDateTime_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_timePopupLink']";
	public static final String Pimco_IM_RT_ObsoleteNowbtn_Path = ".//a[text()='Obsolete Now']";
	public static final String Pimco_IM_RT_UnObsoleteNowbtn_Path = ".//a[text()='Un-Obsolete']";
	public static final String Pimco_IM_RT_UnObsoleteNowmsg_Path = ".//span[text()='Item reactivated!']";
	public static final String Pimco_IM_RT_Reason_Field_Path = "//*[@id='ctl00_cphContent_txtReason']";

	public static final String Pimco_IM_RT_PreApprovalFirm_Dropdown_ID = "ddlPreApprovalFirm";
	public static final String Pimco_IM_RT_FirmApprovalCode_textbox_ID = "txtFirmApprovalCode";
	public static final String Pimco_IM_RT_FirmApprovalExpirationDate_ID = "ctl00_cphContent_RulesPreApprovalFirms_rdpFirmApprovalExpirationDate_dateInput";
	public static final String Pimco_IM_RT_FirmApprovalExpirationDate_Calicon_ID = "//a[@id='popupButton']";
	public static final String Pimco_IM_RT_AddIncludeRule_Button_ID = "//button[text()='Add Include Rule']";
	public static final String Pimco_IM_RT_SetIncludeforAllPreApprovalFirms_Btn_ID = "//button[text()='Set Include for All Pre-Approval Firms']";

	public static final String Pimco_IM_RT_Allow_Backorders_ID = "cphContent_chkBackorder";

	public static final String Pimco_IM_RT_Pre_Approval_DD_FirmID = "ddlPreApprovalFirm";

	public static final String Pimco_IM_RT_Firm_Approval_Code_txt_BoxID = "txtFirmApprovalCode";

	public static final String Pimco_IM_RT_Firm_Approval_Expiration_Date_ID = "popupButton";

	public static final String Pimco_IM_RT_Firm_Approval_Expiration_Date_Calender_ID = "ctl00_cphContent_RulesPreApprovalFirms_rdpFirmApprovalExpirationDate_calendar_Title";

	public String RulesFirmRemoveBtnPath(String PreApprovalFirm) {

		return "//td[text()='" + PreApprovalFirm + "']/following::a[text()='Remove']";

	}

	public static final String PIMCO_MI_Rules_ReplaceType_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Input']";
	public static final String PIMCO_MI_Rules_ReplaceType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Arrow']";
	public static final String PIMCO_MI_Rules_ReplaceWith_Field_Path = ".//*[@id='ctl00_cphContent_ddlReplaceWith_Input']";
	public static final String PIMCO_MI_Rules_ReplaceDate_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_dateInput']";
	public static final String PIMCO_MI_Rules_ReplaceDateCalender_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_popupButton']";
	public static final String PIMCO_MI_Rules_ReplaceDateTime_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_timePopupLink']";
	public static final String Pimco_IM_Rulestab_Path = ".//span[text()='Rules']";

	public static final String Pimco_IM_RT_ObsoleteDate_TextBOX_ID = "ctl00_cphContent_dtObsoleteDate_dateInput";
	public static final String Pimco_IM_RT_ObsoleteDate_TextBOX_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_dateInput']";
	public static final String Pimco_IM_RT_UnObsolete_Path = ".//span[text()='Un-Obsolete']";
	public static final String Pimco_IM_RT_PreApprovalFirm_Path = ".//*[@id='ddlPreApprovalFirm']";
	public static final String Pimco_IM_RT_FirmApprovalCode_Path = ".//*[@id='txtFirmApprovalCode']";
	public static final String Pimco_IM_RT_FirmApprovalExpirationDate_Path = "//*[@id='ctl00_cphContent_RulesPreApprovalFirms_rdpFirmApprovalExpirationDate_dateInput']";
	public static final String Pimco_IM_RT_AddIncludeRule_Path = ".//button[text()='Add Include Rule']";

	// HistoryTab Text Xpath to extract text with Single quotes --His

	public static final String Pimco_IM_His_FirstRow_Xpath = "//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";

	public String XpathStringWithSingleQuote(String Tagname, String Text) {
		return "//" + Tagname + "[text()=\"" + Text + "\"]";
	}

	public String XpathStringContainsSingleQuote(String Tagname, String Text) {
		return "//" + Tagname + "[contains(text(),\"" + Text + "\")]";

	}

	// Pricing Tab

	public static final String Pimco_IM_Pricingtab_Path = ".//span[text()='Pricing']";
	public static final String Pimco_IM_PT_IsPrintCostApproved_Path = ".//td[text()='Is Print Cost Approved:']/following::input[1][@type='checkbox']";
	public static final String Pimco_IM_PricePrintProduction_ID = "ctl00_cphContent_txtPrintCost";

	public static final String Pimco_IM_Price_ChargebackCost_ID = "ctl00_cphContent_txtChargeBackCost";

	public static final String Pimco_IM_Price_PricingType_Flat_ID = "cphContent_optType_0";

	public static final String Pimco_IM_Price_NocostItem_ID = "cphContent_chkNoCostItem";

	public static final String Pimco_IM_Price_CostApproved_ID = "cphContent_chkPrintCostApproved";

	public static final String Pimco_IM_Price_PricingType_Tired_ID = "cphContent_optType_1";
	
	//Metrics
	
		public static final String Pimco_IM_Metrics_UnitsonHand_Path = ".//*[@id='ctl00_cphContent_lblUnitsOnHand_Value']";
		public static final String Pimco_IM_Metrics_UnitsAvailable_Path = ".//*[@id='ctl00_cphContent_lblUnitsAvailable_Value']";
		public static final String Pimco_IM_Metrics_Allocated_Path = ".//*[@id='ctl00_cphContent_lblUnitsAllocated_Value']";
		public static final String Pimco_IM_Metrics_BackOrderQTY_Path = ".//*[@id='ctl00_cphContent_lblBackOrderQty_Value']";
		public static final String Pimco_IM_Metrics_MTD_Path = ".//*[@id='ctl00_cphContent_lblMTDUsage_Value']";
		public static final String Pimco_IM_Metrics_YTD_Path = ".//*[@id='ctl00_cphContent_lblYTDUsage_Value']";
		public static final String Pimco_IM_Metrics_AverageMonthlyUsage_Path = ".//*[@id='ctl00_cphContent_lblAverageMonthlyUsage_Value']";
		public static final String Pimco_IM_Metrics_AverageMonthlyDownloads_Path = ".//*[@id='ctl00_cphContent_lblAverageDownload_Value']";
		
		
		
		
		//ChangeHistory
		
		public static final String Pimco_IM_ChangeHistoryTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[7]/a/span/span/span";
		public static final String Pimco_IM_ChangeHistoryTitle_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
		
		public static final String Pimco_IM_ChangeHistoryAction1_Path =".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";
		public static final String Pimco_IM_ChangeHistoryAction2_Path =".//*[@id='ctl00_cphContent_History_ctl00__1']/td[3]";
		public static final String Pimco_IM_ChangeHistoryAction3_Path =".//*[@id='ctl00_cphContent_History_ctl00__2']/td[3]";
		public static final String Pimco_IM_ChangeHistoryAction4_Path =".//*[@id='ctl00_cphContent_History_ctl00__3']/td[3]";
		public static final String Pimco_IM_ChangeHistoryAction5_Path =".//*[@id='ctl00_cphContent_History_ctl00__4']/td[3]";
		public static final String Pimco_IM_ChangeHistoryAction6_Path =".//*[@id='ctl00_cphContent_History_ctl00__5']/td[3]";
		public static final String Pimco_IM_ChangeHistory_Date_Path =".//a[text()='Date']";
		public static final String Pimco_IM_ChangeHistory_User_Path =".//a[text()='User']";
		public static final String Pimco_IM_ChangeHistory_Action_Path =".//a[text()='Action']";
		
		//Kitting---KT
		
		//public static final String  Pimco_IM_KT_ViewDetails_Btn_Xpath	= "//td[text()='QA_AllowBackOrder']/following::a[text()='View Details']";
		
		public String Pimco_IM_KT_ViewDetails_Btn_Xpath(String piecename, String BtnName){
			return "//td[text()='"+piecename+"']/following::a[text()='"+BtnName+"']";
		}
		
		public static final String PIMCO_MI_Kitting_Path =".//span[text()='Kitting']";
		public static final String PIMCO_MI_Kitting_Additembtn_Path =".//*[@id='ctl00_cphContent_btnKitItem']";
		public static final String PIMCO_MI_Kitting_RemovepopupOK_Path =".//span[text()='OK']";

		public static final String PIMCO_MI_Kitting_ProductCode_Path =".//*[@id='ctl00_cphContent_ddlKitFormNumber_Input']";

		public static final String PIMCO_MI_Kitting_Type_Path =".//*[@id='ctl00_cphContent_ddlKitType_Input']";

		public static final String PIMCO_MI_Kitting_Type_Arrow_Path =".//*[@id='ctl00_cphContent_ddlKitType_Arrow']";

		public static final String PIMCO_MI_Kitting_Location_Path =".//*[@id='ctl00_cphContent_ddlKitLocation_Input']";
		public static final String PIMCO_MI_Kitting_Location_Arrow_Path =".//*[@id='ctl00_cphContent_ddlKitLocation_Arrow']";


		public static final String PIMCO_MI_Kitting_Qty_Path =".//*[@id='ctl00_cphContent_txtKitQuantity']";
		public static final String PIMCO_MI_Kitting_Perkitck_Path =".//*[@id='cphContent_rdbPerParent']";
		public static final String PIMCO_MI_Kitting_Perorderck_Path =".//*[@id='cphContent_rdbPerOrder']";
		public static final String PIMCO_MI_Kitting_Optionalck_Path =".//*[@id='cphContent_chkKitOptional']";
		public static final String PIMCO_MI_Kitting_Sortorder_Path =".//*[@id='ctl00_cphContent_txtKitSortOrder']";



		public static final String PIMCO_MI_Kitting_Additemloadingpanel_Path =".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_pnlKitItem']";;
		
		//PageFlex
		
		public static final String Pimco_IM_PageflexTab_Path =".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[8]/a/span/span/span";
		public static final String Pimco_IM_Pageflextitle_Path =".//*[@id='divContent clearfix']/div[4]/h1";
		public static final String Pimco_IM_Pageflex_Jobnamefield_Path =".//*[@id='ctl00_cphContent_txtJobName']";
		
		public static final String Pimco_IM_PageflexInventory_Path =".//*[@id='divContent clearfix']/div[7]/h1";
		
		public static final String Pimco_IM_PageflexOptiontype__Path =".//a[text()='Option Type']";
		public static final String Pimco_IM_PageflexOptiontype_Textox_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterTextBox_Type";
		public static final String Pimco_IM_PageflexOptiontype_Filter_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_Type";
		
		public static final String Pimco_IM_Pageflex_FriendlyName_Path =".//a[text()='Friendly Name']";
		public static final String Pimco_IM_Pageflex_FriendlyName_Textbox_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterTextBox_FriendlyName";
		public static final String Pimco_IM_Pageflex_FriendlyName_Filter_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_FriendlyName";
		
		public static final String Pimco_IM_Pageflex_FieldGroup_Path =".//a[text()='Group']";
		public static final String Pimco_IM_Pageflex_FieldGroup_TextBox_ID ="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterTextBox_FieldGroup";
		public static final String Pimco_IM_Pageflex_FieldGroup_Filter_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_FieldGroup";
		
		public static final String Pimco_IM_Pageflex_MaxLength_Path =".//a[text()='Max Length']";
		public static final String Pimco_IM_Pageflex_MaxLength_TextBox_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterTextBox_MaxLength";
		public static final String Pimco_IM_Pageflex_MaxLength_FilterID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_MaxLength";
		
		public static final String Pimco_IM_Pageflex_Visible_Path =".//a[text()='Visible']";
		public static final String Pimco_IM_Pageflex_Visible_Checkbox_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterCheckBox_Visible";
		public static final String Pimco_IM_Pageflex_Visible_Filter_ID="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_Visible";

		public static final String Pimco_IM_Pageflex_Required_Path =".//a[text()='Required']";
		public static final String Pimco_IM_Pageflex_Required_Checkbox_ID 	="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterCheckBox_Required";
		public static final String Pimco_IM_Pageflex_Required_Filter_ID 	="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_Required";
		
		public static final String Pimco_IM_Pageflex_Active_Path =".//a[text()='Active']";
		public static final String Pimco_IM_Pageflex_Active_Checkbox_ID ="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_FilterCheckBox_Active";
		public static final String Pimco_IM_Pageflex_Active_Filter_ID ="ctl00_cphContent_InventoryOptions_ctl00_ctl02_ctl02_Filter_Active";
		
		

	// Error message

	public static final String Pimco_IM_InvalidProductCode_ErrorMsg_ID = "cphContent_cfvFormNumber";
	public static final String Pimco_IM_Titleisrequired_ErrorMsg_ID = "cphContent_cfvDescription";
	public static final String Pimco_IM_InvalidRevisionDate_ErrorMsg_ID = "cphContent_cfvRevDate";
	public static final String Pimco_IM_DocumentTypeisrequired_ErrorMsg_ID = "cphContent_cfvDocumentType";

	public static final String Pimco_IM_Required_ErrorMsg_ID = "cphContent_cfvFormOwner";

	public static final String Pimco_IM_Viewabilityisrequired_ErrorMsg_ID = "cphContent_cfvViewability";

}
