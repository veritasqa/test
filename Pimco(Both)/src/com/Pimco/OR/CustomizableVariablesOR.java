package com.Pimco.OR;

public class CustomizableVariablesOR extends ContactSearchPage {

	public static final String Pimco_SCO_Title_Path = ".//h1[contains(text(),'Set Customizable Options')]";
	public static final String Pimco_SCO_1Contactlookup_Path = ".//input[contains(@id,'1) Contact Lookup_Input')]";
	public static final String Pimco_SCO_2Contactlookup_Path = ".//input[contains(@id,'2) Contact Lookup_Input')]";
	public static final String Pimco_SCO_3Contactlookup_Path = ".//input[contains(@id,'3) Contact Lookup_Input')]";
	public static final String Pimco_SCO_4Contactlookup_Path = ".//input[contains(@id,'4) Contact Lookup_Input')]";
	public static final String Pimco_SCO_ContactFirmName_Path = ".//input[contains(@id,'cphContent_cphSection_Firm  Name')]";
	public static final String Pimco_SCO_ContactFirmName_Msg_Path = ".//*[@id='cphContent_cphSection_pnlVariables']/div[5]/div/div";

	public static final String Pimco_SCO_CoverVariables_Path = ".//span[text()='Cover Variables']";
	public static final String Pimco_SCO_ClientName_Path = ".//input[contains(@id,'cphContent_cphSection_Client Name')]";
	public static final String Pimco_SCO_ClientName_Msg_Path = ".//input[contains(@id,'cphContent_cphSection_Client Name')]/following::div[1][contains(text(),'(Max Length: 100)')]";
	public static final String Pimco_SCO_DayMonthYear_Path = ".//input[contains(@id,'cphContent_cphSection_Day Month Year')]";
	public static final String Pimco_SCO_DayMonthYear_Msg_Path = ".//input[contains(@id,'cphContent_cphSection_Day Month Year')]/following::div[1][contains(text(),'(Max Length: 100)')]";
	public static final String Pimco_SCO_FirmName_Path = ".//input[contains(@id,'cphContent_cphSection_Firm Name')]";
	public static final String Pimco_SCO_FirmName_Msg_Path = ".//input[contains(@id,'cphContent_cphSection_Firm Name')]/following::div[1][contains(text(),'(Max Length: 100)')]";
	public static final String Pimco_SCO_BookBuilder_Path = ".//label[text()='Book Builder']";
	public static final String Pimco_SCO_TOCYes_ck_Path = ".//*[@id='cphContent_cphSection_BookBuild_rbtnTOC_0']";
	public static final String Pimco_SCO_TOCNo_ck_Path = ".//*[@id='cphContent_cphSection_BookBuild_rbtnTOC_1']";
	public static final String Pimco_SCO_InventoryLookup_Path = ".//*[@id='racbFormNumbers_Input']";
	public static final String Pimco_SCO_InventoryLookup_Addtobook_Path = ".//*[@id='cphContent_cphSection_BookBuild_btnAddInventory']";
	public static final String Pimco_SCO_KitLookup_Path = ".//*[@id='racbKit_Input']";
	public static final String Pimco_SCO_KitLookup_Addtobook_Path = ".//*[@id='cphContent_cphSection_BookBuild_btnAddKit']";
	public static final String Pimco_SCO_BookContent_Path = ".//labesl[text()='Book Content']";
	public static final String Pimco_SCO_MaximumPageCount_Path = ".//*[@id='cphContent_cphSection_BookBuild_lblMaximumPageCount']";
	public static final String Pimco_SCO_CurrentPageCount_Path = ".//*[@id='cphContent_cphSection_BookBuild_lblCurrentPageCount']";
	public static final String Pimco_SCO_BookContent_Productcode1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__0']/td[4]/div";
	public static final String Pimco_SCO_BookContent_Title1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__0']/td[5]/div";
	public static final String Pimco_SCO_BookContent_Remove1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__0']/td[7]/table/tbody/tr/td/input";
	public static final String Pimco_SCO_BookContent_Productcode2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__1']/td[4]/div";
	public static final String Pimco_SCO_BookContent_Title2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__1']/td[5]/div";
	public static final String Pimco_SCO_BookContent_Remove2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__1']/td[7]/table/tbody/tr/td/input";
	public static final String Pimco_SCO_BookContent_Row1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__0']";
	public static final String Pimco_SCO_BookContent_Row2_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__1']";
	public static final String Pimco_SCO_BookContent_Row3_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__2']";
	public static final String Pimco_SCO_BookContent_Row9_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_BookBuild_Parts_ctl00__8']";
	
	public static final String Pimco_CO_ExternalResourceLookup_ID ="ctl00_ctl00_cphContent_cphSection_1) External Resource Lookup_Input";
	public static final String Pimco_CO_InternalResourceLookup_ID =	"ctl00_ctl00_cphContent_cphSection_2) Internal Resource Lookup_Input";
	public static final String Pimco_CO_SaveBtn_ID	= "cphContent_cphSection_btnCreateProofs";

	public static final String Pimco_SCO_BookContent_Msg_Path = ".//label[text()='Please note that Book Builder items in fulfillment mode may take up to 48 hours to process.']";
	public static final String Pimco_SCO_Save_Path = ".//input[@value='Save']";
	public static final String Pimco_SCO__Path = "";

}
