package com.Pimco.OR;

public class UserKitsPage extends SupportTicketsOR {

	// UserKits - UK

	public static final String Pimco_UK_ProductCode_TextBox_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_FormNumber";
	public static final String Pimco_UK_ProductCode_Filter_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_FormNumber";

	public static final String Pimco_UK_Title_TextBox_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_Description";
	public static final String Pimco_UK_Title_Filter_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_Description";

	public static final String Pimco_UK_LastUpdated_TextBox_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_LastUpdated";
	public static final String Pimco_UK_LastUpdated_Filter_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_LastUpdated";

	public static final String Pimco_UK_AddNewUserKit_Btn_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl03_ctl01_AddNewRecordButton";

	public static final String Pimco_UK_Refresh_ID = "ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl03_ctl01_RebindGridButton";

	// new userkits page - NWUK

	public static final String Pimco_UK_NWUK_ProductCodeTxtBox_ID = "cphContent_txtFormNumber";

	public static final String Pimco_UK_NWUK_TitleTextBox_ID = "cphContent_txtDescription";

	public static final String Pimco_UK_NWUK_Kitcontainer_DDinput_ID = "ctl00_cphContent_cbKitContainer_Input";
	public static final String Pimco_UK_NWUK_Kitcontainer_Arrow_ID = "ctl00_cphContent_cbKitContainer_Arrow";

	public static final String Pimco_UK_NWUK_SaveUserKit_ID = "cphContent_btnSave";

	public static final String Pimco_UK_NWUK_AddToCart_ID = "cphContent_btnAddToCart";

	public static final String Pimco_UK_NWUK_SearchforNewOwner_Textbox_ID = "ctl00_cphContent_rtxtSearchOwner_Input";

	public static final String Pimco_UK_NWUK_Updateowner_ID = "cphContent_lnkUpdateOwner";

	public static String Pimco_UK_NWUK_QA_Folder_Path = "";

	public static String Pimco_UK_NWUK_QA_Folder2_Path = "";

	public static String Pimco_UK_NWUK_QA_Folder3_Path = "";

	public static final String Pimco_UK_NWUK_Saved_Msg_ID = "cphContent_lblErrorMessage";

	// AddFrom Inventory -AFI

	public static final String Pimco_UK_AFI_ProductCode_Input_ID = "ctl00_cphContent_acbAddFormNumber_Input";
	public static final String Pimco_UK_AFI_Quantity_ID = "ctl00_cphContent_txtQuantity";
	public static final String Pimco_UK_AFI_Location_DD_ID = "ctl00_cphContent_ddlLocation_Input";
	public static final String Pimco_UK_AFI_Location_DD_Arrow_ID = "ctl00_cphContent_ddlLocation_Arrow";
	public static final String Pimco_UK_AFI_SortOrder_ID = "ctl00_cphContent_txtSortOrder";

	public static final String Pimco_UK_AFI_Addtokit_ID = "cphContent_btnAddToKit";

	public String EditKitBuildEditRemoveBtnPath(String PieceName, String BtnName) {
		return "//td[text()='" + PieceName + "']/following::a[text()='" + BtnName + "']";
	}

	// Kits Created

}
