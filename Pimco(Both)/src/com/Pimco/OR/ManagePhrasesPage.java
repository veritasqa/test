package com.Pimco.OR;

public class ManagePhrasesPage extends ManageCostCentersPage  {

	
	//ManagePhrases-MP
	
	
	public static final String Pimco_MP_Name_TextBox_ID = 	"ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterTextBox_DisplayName";
	public static final String Pimco_MP_Name_Filter_ID = "ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_DisplayName";
	
	public static final String Pimco_MP_Defaultphrase_TextBox_ID = "ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterTextBox_DefaultPhrase";
	public static final String Pimco_MP_Defaultphrase_Filter_ID ="ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_DefaultPhrase";
	
	public static final String Pimco_MP_CustomPhrase_TextBox_ID = 	"ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterTextBox_CustomPhrase";
	public static final String Pimco_MP_CustomPhrase_Filter_ID ="ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_CustomPhrase";
	
	
	public static final String Pimco_MP_Customactive_Checkboc_ID = "ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_FilterCheckBox_IsCustomActive";
	public static final String Pimco_MP_Customactive_Filter_ID ="ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl02_ctl02_Filter_IsCustomActive";
	
	public static final String Pimco_MP_Customactive_CheckBoxFirstpiece_path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl04_ctl00']";
	
	
	
	public static final String Pimco_MP_Pagenumber_ID = "ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl03_ctl01_GoToPageTextBox";
	
	public static final String Pimco_MP_GoBtn_ID = "ctl00_ctl00_cphContent_cphSection_rgrdPhrases_ctl00_ctl03_ctl01_GoToPageLinkButton";
}
