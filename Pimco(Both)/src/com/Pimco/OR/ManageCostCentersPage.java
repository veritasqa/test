package com.Pimco.OR;

public class ManageCostCentersPage extends ManageCategoriesOR {
	
	//manage cost centers -MCC
	
	public static final String Pimco_MCC_CostCenter_txtbox_ID = "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_FilterTextBox_CostCenter";
	
	public static final String Pimco_MCC_CostCenter_FilterIcon_ID= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_Filter_CostCenter";
	
	
	
	public static final String Pimco_MCC_Name_textbox_ID= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_FilterTextBox_Name";
	
	public static final String Pimco_MCC_Name_FilterIcon_ID= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_Filter_Name";
	
	
	
	public static final String Pimco_MCC_Active_Checkbox_ID= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_FilterCheckBox_IsActive";
	
	public static final String Pimco_MCC_Active_Filter_ID= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl02_Filter_IsActive";
	
	//Create New CostCenter 
	
	public static final String Pimco_MCC_CostCenter_Textbox_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_rtxtCostCenter";
	
	public static final String Pimco_MCC_Name_TextBox_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_rtxtName";
	
	public static final String Pimco_MCC_Active_CheckBox_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_cbxIsActive";
	
	public static final String Pimco_MCC_CreateCostcenter_Btn_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_btnSaveRole";
	
	public static final String Pimco_MCC_Cancel_Btn_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl02_ctl03_btnCancel";
	
	
	//excisting CostCenter -ECC
	
	
	
	public static final String Pimco_MCC_ECC_CostCenter_Textbox_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl05_rtxtCostCenter";
	
	public static final String Pimco_MCC_ECC_Name_TextBox_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl05_rtxtName";
	
	public static final String Pimco_MCC_ECC_Active_CheckBox_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl05_cbxIsActive";
	
	public static final String Pimco_MCC_ECC_CreateCostcenter_Btn_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl05_btnSaveRole";
	
	public static final String Pimco_MCC_ECC_Cancel_Btn_ID	= "ctl00_cphContent_rgrdCostCenters_ctl00_ctl05_btnCancel";
	

	public String Editordeletebutton(String Costcenter,String ButtonName){
		return "//td[text()='"+Costcenter+"']/following::a[text()='"+ButtonName+"']";
	}
	
	public static final String Pimco_MCC_Pagenumber_ID = "ctl00_cphContent_rgrdCostCenters_ctl00_ctl03_ctl02_GoToPageTextBox";
	
	public static final String Pimco_MCC_GoBtn_ID = "ctl00_cphContent_rgrdCostCenters_ctl00_ctl03_ctl02_GoToPageLinkButton";
}
