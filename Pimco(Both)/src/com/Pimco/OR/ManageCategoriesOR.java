package com.Pimco.OR;

import com.Pimco.OR.ReportsPageOR;

public class ManageCategoriesOR extends ManageAnnouncementsPage {

	public static final String Pimco_MC_Title_Path = ".//h1[contains(text(),'Manage Categories')]";
	public static final String Pimco_MC_CategoryName_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00']/thead/tr/th[2]";
	public static final String Pimco_MC_CategoryDescription_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00']/thead/tr/th[3]";
	public static final String Pimco_MC_Sort_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00']/thead/tr/th[4]";
	public static final String Pimco_MC_Active_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00']/thead/tr/th[5]";
	public static final String Pimco_MC_Active1_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl04_ctl01']";
	public static final String Pimco_MC_Insert1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00__0']/td[6]/a";
	public static final String Pimco_MC_Edit1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl04_EditButton']";
	public static final String Pimco_MC_Delete1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00__0']/td[8]/a";
	public static final String Pimco_MC_Addcategory_Path = ".//a[text()='Add Category']";
	public static final String Pimco_MC_Refresh_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String Pimco_MC_AddCategoryName_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_TB_CategoryName']";
	public static final String Pimco_MC_AddCategoryDescription_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_TB_CategoryDescription']";
	public static final String Pimco_MC_Addsort_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_RNTB_SortOrder']";
	public static final String Pimco_MC_AddActive_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_ctl01']";
	public static final String Pimco_MC_AddInsert_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_PerformInsertButton']";
	public static final String Pimco_MC_AddCancel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_CancelButton']";
	public static final String Pimco_MC__Path = "";

	public static final String Pimco_MC_Loading_Path = ".//*[@id='cphContent_cphSection_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_rgCategories']";

}
