package com.Pimco.OR;

public class SearchResultsPage extends ReportsPageOR {

	// Search Results - SR

	// Search Results - SR

	public static final String Pimco_SR_SearchMaterialsBy_TxtBox_ID = "ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword";
	public static final String Pimco_SR_Search_Btn_ID = "cphContent_cphRightSideHeader_btnAdvancedSearch";
	public static final String Pimco_SR_Search_Btn_Path = ".//*[@id='cphContent_cphRightSideHeader_btnAdvancedSearch']";

	public static final String Pimco_SR_NoSearchresults_msg_Path = "Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active.";

	public String SearchResultsPiceNameXpath(String Piecename) {
		return ".//span[@class='productListFormNumber'][contains(.,'" + Piecename + "')]";
	}

	public String SearchResultsAddtoCartBtnXpath(String Piecename) {
		return "//span[@class='productListFormNumber'][contains(.,'" + Piecename
				+ "')]/following::a[text()='Add to Cart']";
	}

	public String SearchResultsAddtoDripBtnXpath(String Piecename) {
		return "//span[@class='productListFormNumber'][contains(.,'" + Piecename
				+ "')]/following::a[text()='Add to Drip']";
	}

	public String AddToCartbtnXpath(String PieceTitle) {
		return "//span[@class='productListFormNumber'][contains(.,'" + PieceTitle
				+ "')]/following::a[text()='Add to Cart'][1]";
	}

	public String SuccessfullyaddtoCartXpath(String PieceTitle) {
		return "//span[@class='description'][contains(.,'" + PieceTitle
				+ "')]/following::div[text()='Successfully Added 1!']";
	}

	public static final String Pimco_SR_PieceAvailablity_Status_Path = "//div[@id='divAvailability']";
	public static final String Pimco_SR_PieceNotAvailable_Status_Message = "Please review filter and category selections or clear filters to retry search.  The item requested may not be available due to Firm Restrictions or may no longer be Active.";
	public static final String Pimco_SR_Materials_Path = ".//h1[contains(text(),'Materials')]";
	public static final String Pimco_SR_Matchingitems_Materials_Path = ".//*[@id='cphContent_cphRightSideCommandBar_lblSearchMessage']/div";
	public static final String Pimco_SR_SearchbyMaterials_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword']";
	public static final String Pimco_SR_SearchbyMaterials_Searchbtn_Path = ".//a[text()='Search']";
	public static final String Pimco_SR_Clear_Path = ".//a[text()='Clear']";
	public static final String Pimco_SR_Loading_Path = ".//*[@id='cphContent_cphMain_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphRightSideContent_rgdProductList']";
	public static final String Pimco_SR_AdvancedFilter_Path = ".//a[text()='Advanced Filter']";
	public static final String Pimco_SR_ExcludeRegulatoryck_Path = ".//span[text()='Exclude Regulatory']/following::input[1][@type='checkbox']";
	public static final String Pimco_SR_ExcludeUserKitsck_Path = ".//span[text()='Exclude User Kits']/following::input[1][@type='checkbox']";
	public static final String Pimco_SR_CheckOut_Btn_ID = "cphContent_cphRightSideContent_btnSearchResultsCheckOut";

	// Search Results - SR

	public static final String Pimco_SR_Clear_Btn_ID = "cphContent_cphRightSideHeader_btnClearForm";

	public static final String Pimco_SR_Frame_Name = "oRadWindowComponentDetails";

	public static final String Pimco_SR_Close_Btn_Xpath = "//a[@title='Close']";

	public static final String Pimco_SR_ExpandBtn_Xpath = "//span[text()='a_JoshTest']/preceding::input[1]";

	public static final String Pimco_SR_ProofBtn_Xpath = "//span[text()='a_JoshTest']//following::a[text()='Proof']";

	public static final String Pimco_SR_ProofedBtn_Xpath = "//span[text()='a_JoshTest']//following::a[text()='Proof']";

	// Advanced Search Filters

	public static final String Pimco_SR_AdvancedSearch_Btn_ID = "showHideFilter";
	public static final String Pimco_SR_Pre_Approved_Firm_DD_Arrow_ID = "ctl00_ctl00_cphContent_cphRightSideCommandBar_ddlFirm_Arrow";

	public static final String Pimco_SR_Exclude_User_Kits_Checkbox_ID = "cphContent_cphRightSideCommandBar_chkExcludeUserKits";

	// method that returns xpath of the searched piece name
	// Note : it has contains so it might find 2 or more elements in case of
	// more results
	// Note : Give Exact pice name as it is in the search results (Case
	// Sensitive )
	// span[@class="productListFormNumber"][contains(.,'QA_adminonly')]

	public String AddtoCartMessageXpath(String PieceTitle, String Message) {
		return "//span[@class='description'][contains(.,'" + PieceTitle + "')]/following::div[text()='" + Message
				+ "']";
	}

	public String AddtoCartPiecenameXpath(String PieceTitle, String Message) {
		return "//span[@class='productListFormNumber'][contains(.,'" + PieceTitle + "')]/following::div[text()='"
				+ Message + "']";
	}

	public String PieceQuantityPath(String PieceTitle) {

		return "//span[@class='productListFormNumber'][contains(.,'" + PieceTitle + "')]/following::input[1]";

	}

	// Mini Shopping Cart - MSC

	public static final String Pimco_SR_MSC_Checkout_Btn_ID = "cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout";
	public static final String Pimco_SR_MSC_Update_Btn_ID = "cphContent_cphLeftSide_ucMiniShoppingCart_btnUpdate";

	// QA_Multifunction has been removed from your cart
	public String MiniCartQtyTextfieldPath(String Piecename) {

		return "//a[text()='" + Piecename + "']/following::input[contains(@id,'txtQuantity')][1]";

	}
	
	public String CalenderbtnNexttoPiece(String Piecename){
		return "//span[@class='productListFormNumber'][contains(.,'"+Piecename+"')]/following::a[@class='rcCalPopup']";
}

}
