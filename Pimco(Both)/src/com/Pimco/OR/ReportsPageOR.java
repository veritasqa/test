package com.Pimco.OR;

public class ReportsPageOR extends OrderManagement {

	public static final String Pimco_Reports_Available_Path = ".//h1[contains(text(),'Available Reports')]";
	public static final String Pimco_Reports_Owner_Path = ".//*[@id='cphContent_ddlBusinessOwners']";

	public static final String Pimco_Reports_8WeekUsageReportScheduler_Path = ".//span[contains(text(),'8WeekUsageReport Scheduler')]";
	public static final String Pimco_Reports_Subscriptions_Path = ".//h1[contains(text(),'My Report Subscriptions')]";
	public static final String Pimco_Reports_8WeekUsageByAliasReport_Path = ".//td[text()='8WeekUsageByAliasReport']";
	public static final String Pimco_Reports_8WeekUsageByAliasReportview_Path = ".//td[text()='8WeekUsageByAliasReport']//following::a[1][contains(text(),'View Report')]";
	public static final String Pimco_Reports_8WeekUsageReport_Path = ".//td[text()='8WeekUsageReport']";
	public static final String Pimco_Reports_8WeekUsageReportView_Path = ".//td[text()='8WeekUsageReport']//following::a[1][contains(text(),'View Report')]";
	public static final String Pimco_Reports_Firstpage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String Pimco_Reports_Previouspage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String Pimco_Reports_Nextpage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String Pimco_Reports_Lastpage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String Pimco_Reports_Pagesize_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00_ctl03_ctl01_PageSizeComboBox_Input']";
	public static final String Pimco_Reports_Pagesizearrow_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00_ctl03_ctl01_PageSizeComboBox_Arrow']";
	public static final String Pimco_Reports_TenthRow_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00__9']";
	public static final String Pimco_Reports_TwentiethRow_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00__19']";
	public static final String Pimco_Reports_ThirtysecondRow_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00__31']";

	public static final String Pimco_Reports_ItemsinPage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[5]";

	public static final String Pimco_Reports_Scheduler_Path = ".//span[contains(text(),'Report Scheduler')]";
	public static final String Pimco_Reports_ScheduleDD_Path = ".//b[text()='Schedule:']//following::td//input[contains(@id,'ddlSchedule_Input')]";
	public static final String Pimco_Reports_ReportTypeDD_Path = ".//b[text()='Report Type:']//following::td//input[contains(@id,'ddlType_Input')]";
	public static final String Pimco_Reports_Email_Path = ".//b[text()='Email:']//following::input[contains(@id,'txtEmail') and contains(@class,'riTextBox')]";
	public static final String Pimco_Reports_Comments_Path = ".//b[text()='Comments:']//following::textarea[contains(@id,'txtComments') and contains(@class,'riTextBox')]";
	public static final String Pimco_Reports_Subscribe_Path = ".//input[@value='Subscribe']";

	// ReportPage

	public static final String Pimco_Reports_Title_Path = ".//div[contains(text(),'8 Week Usage Report by Alias')]";
	public static final String Pimco_Reports_LiteratureType_Path = ".//*[@id='rptTest_ctl00_ctl03_ddValue']";
	public static final String Pimco_Reports_Channel_Path = ".//*[@id='rptTest_ctl00_ctl05_ddValue']";
	public static final String Pimco_Reports_CostCenter_Path = ".//*[@id='rptTest_ctl00_ctl07_ddValue']";
	public static final String Pimco_Reports_ProductCateg_Path = ".//*[@id='rptTest_ctl00_ctl09_ddDropDownButton']";
	public static final String Pimco_Reports_ProductCategSelectall_Path = ".//*[@id='rptTest_ctl00_ctl09_divDropDown_ctl00']";
	public static final String Pimco_Reports_Nullck_Path = ".//*[@id='rptTest_ctl00_ctl11_cbNull']";
	public static final String Pimco_Reports_Close_Path = ".//a[@class='rwCloseButton']";

	public static final String Pimco_Reports_Viewreportbtn_Path = ".//input[@value='View Report']";
	public static final String Pimco_Reports_ItemStatus_Path = ".//*[@id='rptTest_ctl00_ctl03_ddValue']";
	public static final String Pimco_Reports_Formowner_Path = ".//*[@id='rptTest_ctl00_ctl05_ddValue']";
	public static final String Pimco_Reports_ProductCode_Path = ".//*[@id='rptTest_ctl00_ctl07_txtValue']";
	public static final String Pimco_Reports_ProductCodeNull_Path = ".//*[@id='rptTest_ctl00_ctl07_cbNull']";
	public static final String Pimco_Reports_ItemCode_Path = ".//*[@id='rptTest_ctl00_ctl09_txtValue']";
	public static final String Pimco_Reports_ItemCodeNull_Path = ".//*[@id='rptTest_ctl00_ctl09_cbNull']";
	public static final String Pimco_Reports_JobNo_Path = ".//*[@id='rptTest_ctl00_ctl11_txtValue']";
	public static final String Pimco_Reports_JobNoNull_Path = ".//*[@id='rptTest_ctl00_ctl11_cbNull']";
	public static final String Pimco_Reports_Country_Path = ".//*[@id='rptTest_ctl00_ctl13_ddValue']";
	public static final String Pimco_Reports_Exportdd_Path = ".//*[@id='rptTest_ctl01_ctl05_ctl00']";
	public static final String Pimco_Reports_Export_Path = ".//a[@title='Export']";
	public static final String Pimco_Reports__Path = "";



	public static final String Pimco_Reports_Parameters_Path = ".//span[text()='Parameters']";
	public static final String Pimco_Reports_ParametersFormowner_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i1_FormOwner_Input']";
	public static final String Pimco_Reports_ParametersStartDate_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i2_StartDate_dateInput']";
	public static final String Pimco_Reports_ParametersStartDateCal_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i2_StartDate_popupButton']";
	public static final String Pimco_Reports_ParametersStartDateTime_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i2_StartDate_timePopupLink']";

	public static final String Pimco_Reports_ParametersEndDate_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i3_EndDate_dateInput']";
	public static final String Pimco_Reports_ParametersEndDateCal_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i3_EndDate_popupButton']";
	public static final String Pimco_Reports_ParametersEndDateTime_Path = ".//*[@id='ctl00_cphContent_Parameters_i0_i3_EndDate_timePopupLink']";

	public static final String Pimco_Reports_Aonlogo_Path = ".//img[@alt='Logo']";

	public static final String Pimco_Reports_Loading_Path = ".//*[@id='cphContent_RadAjaxLoadingPanelctl00_cphContent_Reports']";
	public static final String Pimco_Reports_Loading_Subscriptions_Path = ".//*[@id='cphContent_RadAjaxLoadingPanelctl00_cphContent_Subscriptions']";

}
