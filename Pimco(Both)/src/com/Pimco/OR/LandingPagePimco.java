package com.Pimco.OR;

public class LandingPagePimco extends InventoryPage {

	public static final String Pimco_LP_Logout_ID = "lbnLogout";
	public static final String Pimco_LP_ViewAllAnnouncements_path = ".//a[text()='View All Announcements']";
	public static final String Pimco_LP_StartaMailing_path = ".//a[text()='Start a Mailing']";
	public static final String Pimco_LP_StartaBulkOrder_path = ".//a[text()='Start a Bulk Order']";

	public static final String Pimco_LP_Home_path = ".//span[text()='Home']";
	public static final String Pimco_LP_Selectedcontact_ID = "lblUserName";
	public static final String LP_Admin_path = ".//span[text()='Admin']";
	public static final String LP_Specialproject_path = ".//span[text()='Special Projects']";
	public static final String LP_Materials_path = ".//span[text()='Materials']";
	public static final String LP_Support_path = ".//span[text()='Support']";
	public static final String LP_ManageUsers_path = ".//span[text()='Manage Users']";
	public static final String LP_ManageRoles_path = ".//span[text()='Manage Roles']";
	public static final String LP_ManageAnnouncements_path = ".//span[text()='Manage Announcements']";
	public static final String LP_Reports_path = ".//span[text()='Reports']";
	public static final String LP_ManageCategory_path = ".//span[text()='Manage Categories']";

	public static final String Pimco_LP_FulfilmentSearch_TxtBox_ID = "txtQuickSearch";
	public static final String Pimco_LP_FulfilmentSearch_Btn_ID = "btnQuickSearch";
	public static final String Pimco_LP_ClearCart_Path = ".//span[text()='Clear Cart']";
	

	public static final String Pimco_LP_StartaMailing_ID= "cphContent_lnkMarketingCampaign";
	public static final String Pimco_LP_StartaBulkOrder_ID= "cphContent_lnkBulkOrder";
	public static final String Pimco_LP_ClearCart_disabled_ID = "Navigation_aCancelOrder";

	// Widgets

	public static final String Add_Shortcut_Path = ".//h3[text()='Add Widgets']";

	// Order Template

	public static final String Ordertemplate_Path = ".//a[text()='Order Templates']";
	public static final String Ordertemplate_Form1_Path = ".//*[@id='divOrderTemplates']/tbody/tr[1]/td[1]/div";
	public static final String Ordertemplate_Form2_Path = ".//*[@id='divOrderTemplates']/tbody/tr[2]/td[1]/div";
	public static final String Ordertemplate_Form3_Path = ".//*[@id='divOrderTemplates']/tbody/tr[3]/td[1]/div";
	public static final String Ordertemplate_View1_Path = ".//*[@id='divOrderTemplates']/tbody/tr[1]/td[2]/a[1]";
	public static final String Ordertemplate_View2_Path = ".//*[@id='divOrderTemplates']/tbody/tr[2]/td[2]/a[1]";
	public static final String Ordertemplate_View3_Path = ".//*[@id='divOrderTemplates']/tbody/tr[3]/td[2]/a[1]";
	public static final String Ordertemplate_Addtocart1_Path = "_ctl00_repOrderTemplates_ctl01_btnAddToCart']";
	public static final String Ordertemplate_Addtocart2_Path = "_ctl00_repOrderTemplates_ctl03_btnAddToCart']";
	public static final String Ordertemplate_Addtocart3_Path = "_ctl00_repOrderTemplates_ctl05_btnAddToCart']";
	public static final String Ordertemplate_selectrecipient = ".//em[text()='Order Templates']//following::a[text()='select a recipient'][1]";

	// New Forms and Materials

	public static final String NFMWidget_Selectrecipient_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String NFMWidget_Form1_Path = "_ctl00_lnkFormNumber1']";
	public static final String NFMWidget_Form2_Path = "_ctl00_lnkFormNumber2']";
	public static final String NFMWidget_Form3_Path = "_ctl00_lnkFormNumber3']";
	public static final String NFMWidget_Form4_Path = "_ctl00_lnkFormNumber4']";
	public static final String NFMWidget_Form5_Path = "_ctl00_lnkFormNumber5']";
	public static final String NFMWidget_Qty1_Path = "_ctl00_txtQuantity1']";
	public static final String NFMWidget_Qty2_Path = "_ctl00_txtQuantity2']";
	public static final String NFMWidget_Qty3_Path = "_ctl00_txtQuantity3']";
	public static final String NFMWidget_Qty4_Path = "_ctl00_txtQuantity4']";
	public static final String NFMWidget_Qty5_Path = "_ctl00_txtQuantity5']";
	public static final String NFMWidget_Addtocart_Path = "_ctl00_btnAddItem']";
	public static final String NFMWidget_Checkout_Path = "_ctl00_btnAddToCart']";

	// Express Checkout-EC

	public static final String Pimco_LP_EC_SelectaRecipient_Path = "//em[text()='Express Checkout']/following::a[text()='select a recipient'][1]";
	public static final String Pimco_LP_EC_ProductCode_Txtbox1_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber1_Input')]";
	public static final String Pimco_LP_EC_ProductCode_Txtbox2_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber2_Input')]";
	public static final String Pimco_LP_EC_ProductCode_Txtbox3_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber3_Input')]";
	public static final String Pimco_LP_EC_ProductCode_Txtbox4_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber4_Input')]";
	public static final String Pimco_LP_EC_ProductCode_Txtbox5_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'rtxtFormNumber5_Input')]";

	public static final String Pimco_LP_EC_Qty_Txtbox1_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity1')]";
	public static final String Pimco_LP_EC_Qty_Txtbox2_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity2')]";
	public static final String Pimco_LP_EC_Qty_Txtbox3_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity3')]";
	public static final String Pimco_LP_EC_Qty_Txtbox4_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity4')]";
	public static final String Pimco_LP_EC_Qty_Txtbox5_Path = "//em[text()='Express Checkout']/following::input[contains(@id,'C_ctl00_txtQuantity5')]";

	public static final String Pimco_LP_EC_CheckoutBtn_path = "//em[text()='Express Checkout']/following::a[text()='Checkout']";

	// OrderLookup -OL

	public static final String Pimco_LP_OL_FromDate_TxtBox_path = "//em[text()='Order Lookup']/following::input[contains(@id,'ctl00_rdpOrderSearchDateFrom_dateInput')][1]";

	public static final String Pimco_LP_OL_ToDate_TxtBox_path = "//em[text()='Order Lookup']/following::input[contains(@id,'ctl00_rdpOrderSearchDateTo_dateInput')][1]";

	public static final String Pimco_LP_OL_Search_Btn_path = "//em[text()='Order Lookup']/following::a[contains(@id,'C_ctl00_btnSearch')][1]";

	public static final String Pimco_LP_OL_OrderNumber_TxtBox_path = "//em[text()='Order Lookup']/following::input[contains(@id,'ctl00_rcbOrderNumber_Input')][1]";

	public static final String Pimco_LP_OL_Copy_Btn_path = "//em[text()='Order Lookup']/following::a[contains(@id,'C_ctl00_btnCopy')][1]";

	public static final String Pimco_LP_OL_View_Btn_path = "//em[text()='Order Lookup']/following::a[contains(@id,'C_ctl00_btnSubmit')][1]";

	// QuickSearch - QS

	public static final String Pimco_LP_QS_Search_Btn_path = "//em[text()='Quick Search']/following::a[text()='Search'][1]";
	public static final String Pimco_LP_QS_Materialsby_Textbox_path = "//em[text()='Quick Search']/following::input[1]";

	// Order Search-OS

	public static final String Pimco_LP_OS_SearchBtn_ID = "cphContent_btnSearch";

	// MyUserKits- MUK

	public static final String Pimco_LP_MUK_SelectaRecipient_Path = "//em[text()='My User Kits']/following::a[text()='select a recipient'][1]";

	public static final String Pimco_LP_MUK_EditMyUserKits_Btn_Path = "//em[text()='My User Kits']/following::a[text()='Edit/View My User Kits'][1]";

	public String Pimco_LP_MUK_Qty_Path(String KitName) {
		return "//i[text()='" + KitName + "']/following::input[1]";
	}

	public String Pimco_LP_MUK_AddtoCart_Path(String KitName) {
		return "//i[text()='" + KitName + "']/following::a[text()='Add To Cart'][1]";
	}

	public String Pimco_LP_MUK_Checkout_Path(String KitName) {
		return "//i[text()='" + KitName + "']/following::a[text()='Checkout'][1]";
	}

	public String Pimco_LP_MUK_EditMyUserKits_Path(String KitName) {
		return "//i[text()='" + KitName + "']/following::a[text()='Edit/View My User Kits'][1]";
	}

	// RecentOrders -RO

	public static String RecentorderButtonpath(String OrderNumber, String ButtonName) {
		return "//td[text()='" + OrderNumber + "']/following::a[text()='" + ButtonName + "'][1]";
	}

	public static String RecentorderStatus(String OrderNumber) {
		return "//td[text()='" + OrderNumber + "']/following::td[text()='NEW'][1]";
	}

	public String WidgetNamePath(String Widgetname) {
		return "//em[text()='" + Widgetname + "']";
	}
	// td[text()='1768428']/following::a[text()='View'][1]

}
