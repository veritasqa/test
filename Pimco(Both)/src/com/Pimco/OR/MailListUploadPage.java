package com.Pimco.OR;

public class MailListUploadPage extends Loginpage {

	public static final String Pimco_MLU_Select_File_ID = "ctl00_cphContent_fupMailList";

	public static final String Pimco_MLU_Select_Btn_Path = "//span[@class='ruFileWrap ruStyled']/input[@type='button']";

	public static final String Pimco_MLU_UploadMailList_ID = "cphContent_btnMailList";

	public static final String Pimco_MLU_UploadMailList_Path = ".//a[text()='Upload Mail List']";
	public static final String Pimco_MLU_Select_Path = "//*[@id='ctl00_cphContent_fupMailListListContainer']/li/span/input[3]";

	public static final String Pimco_MLU_Ok_Btn_ID = "cphContent_btnBack";

	public static final String Pimco_MLU_PIMCO_Maillist5_NoRestrictions_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_Maillist(5)_NoRestrictions.xlsx";

	public static final String Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_Mailist(6)_NoRestrictions.xlsx";

	public static final String Pimco_MLU_PIMCO_Maillist6_NoRestrictions_Updated_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_Mailist(6)_NoRestrictions_updated.xlsx";

	public static final String PIMCO_MailListUpload_Sample_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_MailListUpload_Sample.xlsx";

	public static final String PIMCO_Maillist_Allfields_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_Maillist_Allfields.xlsx";

	public static final String PIMCO_Maillist_AllMissing_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_Mailist_All_MISSING.xlsx";

}
