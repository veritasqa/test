package com.Pimco.Values;

import java.util.ArrayList;
import java.util.List;

import org.testng.asserts.SoftAssert;

public class ValueRepositoryPimco {

	public static final String Xpath = "xpath";
	public static final String ID = "id";
	public static final String Tag = "tag";
	public static final String Name = "name";
	public static final String CSSselector = "cssSelector";
	public static final String LinkText = "linkText";
	public static final String PartialLinkText = "partialLinkText";
	public static final String Classname = "classname";
	public static String SplprjTicketNumber = "007896";
	public static String SplprjTicketNumber2 = "";
	public static List<String> SplprjTicketNumberlist = new ArrayList<String>();
	public static List<String> OrderNumberlist = new ArrayList<String>();
	public static List<String> RecentOrderNumberlist = new ArrayList<String>();
	public static List<String> UserKits = new ArrayList<String>();

	public static String Browser = "chrome";
	public static SoftAssert softAssert;

	public static String Browserpath = System.getProperty("user.dir") + "\\src\\com\\Pimco\\Driver\\chromedriver.exe";
	// URLs
	public static final String URL = "https://order.pimco.com/login.aspx";
	public static final String Production_URL = "https://order.pimco.com/login.aspx";

	public static final String Inventory_URL = "https://www.veritas-solutions.net/Inventory/login.aspx";

	// Pre prod: https://staging.veritas-solutions.net/pimcopreprod/login.aspx
	// Stage: https://stageorder.pimco.com/login.aspx
	// Prod: https://order.pimco.com/login.aspx

	// Inven- Prod: https://www.veritas-solutions.net/Inventory/login.aspx
	// Inven- Stage: https://staging.veritas-solutions.net/Inventory/login.aspx

	// Credentials
	public static final String PIMCOBothUsername = "qaauto";
	public static final String PIMCOBothPassword = "qaauto";

	public static final String PIMCOBoth1Username = "qaauto1";
	public static final String PIMCOBoth1Password = "qaauto1";

	public static final String PIMCOCAUsername = "qaautoca";
	public static final String PIMCOCAPassword = "qaautoca";

	public static final String PIMCOUSUsername = "qaautous";
	public static final String PIMCOUSthPassword = "qaautous";

	public static final String PIMCOBothBasicUsername = "rkhanambasic";
	public static final String PIMCOBothBasicPassword = "rkhanambasic";

	// User details

	public static final String Both_Fname = "qaauto";
	public static final String Both_Lname = "automation";

	public static final String Both_Fname1 = "qaauto1";
	public static final String Both_Lname1 = "automation";

	public static final String CA_Fname = "qaautoca";
	public static final String CA_LPname = "automation";

	public static final String US_Fname = "qaautous";
	public static final String US_Lname = "automation";

	public static final String Inventory_UserName = "yannamalai";
	public static final String Inventory_Password = "yannamalai590";

	// Shipping Details

	public static final String Email = "ver.qaauto@rrd.com";
	public static final String Address = "913 Commerce Ct";
	public static final String City = "Buffalo Grove";
	public static final String State = "Illinois";
	public static final String Zip = "60089";

	public static String OrderNumber = "5717040";
	public static String Supporttickenumber = "Test";
	public static String Clienttickenumber = "";
	public static List<String> ordernum = new ArrayList<String>();
	public static List<String> Supporttickenumberlist = new ArrayList<String>();
	public static List<String> Clienttickenumberlist = new ArrayList<String>();
	public static String[] OrderNumberArray;
	public static String CreatePart = "";

	// Special Project Attachements

	public static String Pimco_TC_2_4_1_1_6 = System.getProperty("user.dir")
			+ "\\src\\com\\Pimco\\Utils\\PIMCO_Mailist(6)_NoRestrictions.xlsx";

	// Parts

	public static final String QA_Multifunction = "QA_Multifunction";
	public static final String QA_NewTestPart_X = "QA_NewTestPart_X";
	public static final String QA_adminonly = "QA_adminonly";
	public static final String QA_Viewability = "QA_Viewability";
	public static final String QA_Hyphen_TestGWM = "QA_HyphenTest-GWM";
	public static final String QA_userkittest = "QA_userkittest";
	public static final String QA_TesteffectiveDate = "QA_TesteffectiveDate";
	public static final String QA_Obsolete = "QA_Obsolete";

	public static final String PBB001_40827 = "PBB001_40827";
	public static final String PBB002_41553 = "PBB002_41553";
	public static final String CBB004_40859 = "CBB004_40859";

	public static final String QA_testfactsheet_01 = "QA_testfactsheet_01";
	public static final String QA_testfactsheet_02 = "QA_testfactsheet_02";
	public static final String QA_testfactsheet_05 = "QA_testfactsheet_05";
	public static final String QA_TestFundcard_01 = "QA_TestFundcard_01";
	public static final String qa_testfundcard_02 = "qa_testfundcard_02";
	public static final String QA_TestFundcard_03 = "QA_TestFundcard_03";
	public static final String QA_TestFundCard_05 = "QA_TestFundCard_05";
	public static final String QA_Replace_Test_2 = "QA_Replace_Test_2";
	public static final String QA_MarketingOnly = "QA_MarketingOnly";
	public static final String QA_TestSupp_01 = "QA_TestSupp_01";
	public static final String QA_Testsupp_02 = "QA_Testsupp_02";
	public static final String QA_Testsupp_03 = "QA_Testsupp_03";
	public static final String QA_Testsupp_04 = "QA_Testsupp_04";
	public static final String QATest_FundCard1 = "QATest_FundCard1";
	public static final String QA_TestFundcard_04 = "QA_TestFundcard_04";
	public static final String QA_FirmRestrictionTest = "QA_FirmRestrictionTest";
	public static final String QA_BookbuilderKit1 = "QA_BookbuilderKit1";
	public static final String QA_FulfillmentMarketingBulk = "QA_FulfillmentMarketingBulk";
	public static final String QA_Stockpart = "QA_Stockpart";
	public static final String QA_FirmRestriction = "qa_firmrestriction";
	public static final String QA_Maxorder = "QA_Maxorder";
	public static final String QA_Usergroup = "QA_Usergroup";
	public static final String QA_Backordertest = "QA_Backordertest";
	public static final String QA_AllowBackOrder = "QA_AllowBackOrder";
	public static final String QA_Kitacknowledgetest = "QA_Kitacknowledgetest";
	public static final String QA_FulfillmentOnly = "QA_FulfillmentOnly";
	public static final String qa_kitonfly_4 = "qa_kitonfly_4";
	public static final String QA_Marketingonly = "QA_Marketingonly";
	public static final String AB_36827 = "QA_AB_36827";
	public static final String QA_Flyer = "QA_Flyer";
	public static final String QA_Summaryprospectus = "QA_Summaryprospectus";
	public static final String QA_OTFirmRestrictionIN = "QA_OTFirmRestrictionIN";
	public static final String QA_ProofTest = "QA_ProofTest";
	public static final String QA_Kit_Ptsmessagetest = "QA_Kit_Ptsmessagetest";
	public static final String QA_KitOnFly_BO_01 = "QA_KitOnFly-BO_01";

	// Upload files
	public static String Upload_AutoIT_exe = "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_4_13_1_4_ProfilePhoto.exe";
	public static String Pimco_TC_2_6_1_6_1_1_File = "\\src\\com\\Pimco\\Utils\\Upload_Files\\Pimco_TC_2_6_1_6_1_1.xlsx";
	public static String PIMCO_Mailist6_NoRestrictions = "\\src\\com\\Pimco\\Utils\\Upload_Files\\PIMCO_Mailist(6)_NoRestrictions.xlsx";
	public static String PIMCO_Maillist5_NoRestrictions = "\\src\\com\\Pimco\\Utils\\Upload_Files\\PIMCO_Maillist(5)_NoRestrictions.xlsx";

}
