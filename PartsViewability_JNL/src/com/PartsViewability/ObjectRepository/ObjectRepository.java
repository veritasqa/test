package com.PartsViewability.ObjectRepository;

import java.util.ArrayList;
import java.util.List;

import com.PartsViewability.ValueRepository.ValueRepository;

//import org.openqa.selenium.By;

public class ObjectRepository extends ValueRepository{
	
	
	public List<String> Part = new ArrayList<String>() ;
	public List<String> Action = new ArrayList<String>() ;
	public List<String> Results = new ArrayList<String>() ;

	public int Total_parts	 ;
	
	 // locator type 
	
 	public static String locatorType ="xpath";
 	
 	//Login Page
 	
 	public static final String UserName_Path = "//*[@id='txtUserName']";
 	public static final String Password_Path = "//*[@id='txtPassword']";
 	public static final String Login_Btn_Path = "//*[@id='btnSubmit']";
 	public static final String Logout_Btn_Path = ".//*[@id='lbnLogout']";

 	public static final String Materials_Btn_Path = ".//*[@id='ctl00_Navigation_divLiList']/li[2]/a/span";
 	public static final String Admin_Btn_Path = ".//span[text()='Admin']";
 	
 	public static final String Manage_Inventory_Path = ".//span[text()='Manage Inventory']";
 	public static final String Form_Number_Path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
 	public static final String Search_Btn_Path = ".//*[@id='ctl00_cphContent_btnSearch']";
 	public static final String Edit_Btn_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[9]/a";
 	
 	
 	public String MI_SearchresultsCopy(String PartName) {

		return ".//*[text()='" + PartName + "']//following::td[7]/a";

	}

	public String MI_SearchresultsEdit(String PartName) {

		return ".//*[text()='" + PartName + "']//following::a[contains(.,'Edit')]";

	}
 	// Parameters For ParentChild Window (Find on COmmon Methods)
 //	List<String> ID = new ArrayList<String>() ;
	
	
	public static final String Active_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlActive_Arrow']";
	public static final String Active_Any_Path = ".//*[@id='ctl00_cphContent_ddlActive_DropDown']/div/ul/li[1]";
 	public String Window_Handle = "";
 	
 	public static final String Viewability_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Arrow']";
 	public static final String Viewability_dropdown = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul";
 	
 	public static final String Viewability_input  = "//input[@id='ctl00_cphContent_ddlViewability_Input']";

  
 	public static final String Admin_Only_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[2]";
 	
	public static final String Not_Viewable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[3]";
 	
	public static final String Orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[4]";

 	public static final String Viewable_not_orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[5]";

 
 	
 	public static final String Save_Btn_Path = "//a[contains(.,'Save')]";
 	public static final String Ok_Btn_Path = "//input[@value='OK']";
 	
 	public static final String Clear_Btn_Path = ".//*[@id='ctl00_cphContent_btnClear']";
 	
}
