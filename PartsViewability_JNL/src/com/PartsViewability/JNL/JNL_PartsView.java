
package com.PartsViewability.JNL;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.PartsViewability.CommonMethods.CommonMethods;

public class JNL_PartsView extends CommonMethods {

	@Test

	public void Partsview() throws InterruptedException, IOException {

		// Initialization for Text File
		// StringBuilder appender = new StringBuilder();
		// String seperator = System.getProperty("line.separator");
		// appender.append("Test Report
		// ").append(seperator).append("------------").append(seperator);

		for (int i = 0; i < Total_parts; i++) {
			//Implicit_Wait(driver);
			String Parts = Part.get(i).toString().trim();
			String Activity = Action.get(i).toString().trim();

			// On main Page
			Hover_Over_Element(locatorType, Admin_Btn_Path, driver);
			// Thread.sleep(500);
			Click_On_Element(locatorType, Manage_Inventory_Path, driver);
			// On Inventory page
			Clear_Type_Charecters(locatorType, Form_Number_Path, Parts, driver);

			Click_On_Element(locatorType, Active_Arrow_Path, driver);
			ExplicitWait_Element_Visible(locatorType, Active_Any_Path, driver);
			Thread.sleep(1000);
			Click_On_Element(locatorType, Active_Any_Path, driver);

			Click_On_Element(locatorType, Search_Btn_Path, driver);

			ExplicitWait_Element_Clickable(locatorType, MI_SearchresultsEdit(Parts), driver);

			// to check If there is no part available
			if (Is_Element_Present(locatorType, MI_SearchresultsEdit(Parts), driver))

			{
				Click_On_Element(locatorType, MI_SearchresultsEdit(Parts), driver);

				// appender.append(seperator).append("The Part:
				// ").append(Parts).append(" Available And ");

				if (Activity.equalsIgnoreCase("Skip")) {

					// appender.append(seperator).append("Part
					// ").append(Parts).append(" Is skipped").append(seperator);
					Results.add("Is skipped");

				}

				else {
					// on new Tab
					// Switch_New_Tab(driver);

					// Excel reading logic for both viewable or orderable

					do {
						if (Activity.equalsIgnoreCase("Not Viewable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Not_Viewable_Path, driver);

						}

						else if (Activity.equalsIgnoreCase("Viewable And Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Viewable Not Orderable"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Viewable_not_orderable_Path, driver);

						} else if (Activity.equalsIgnoreCase("Admin Only"))

						{
							Click_On_Element(locatorType, Viewability_Arrow_Path, driver);

							Thread.sleep(500);

							Double_Click(locatorType, Admin_Only_Path, driver);

						} else

						{
							Results.add("Part " + Parts + " Available But Take a LOOk at the Second Column in excel");
							// appender.append(seperator).append("Part:
							// ").append(Parts).
							// append(" Available But Take a LOOk at the Second
							// Column in excel").append(seperator);

						}

						// Saving the settings
						Click_On_Element(locatorType, Save_Btn_Path, driver);
						Click_On_Element(locatorType, Ok_Btn_Path, driver);

						newview = Get_Attribute(locatorType, Viewability_input, Attribute_Value, driver);

					} while (!Activity.equalsIgnoreCase(newview));

					// appender.append(" Successfully made the Part:
					// ").append(Parts).append(" "+ newview).append(seperator);
					Results.add(newview);

					// Switch_Old_Tab(driver);
				}

			}

			else {
				// appender.append(seperator).append("Part
				// ").append(Parts).append(" Is Not
				// Available").append(seperator);
				Results.add("Is Not Available");

			}

		}
		// OutPut Writer
		// if (appender.length() != 0) {
		// System.out.println("Preparing Test Report ......" + seperator);
		// writeReport(OutPut_File_Path, appender.toString());
		// System.out.println("Test Report Is Ready");
		// }

	}

	@BeforeSuite
	public void beforeClass() throws IOException, InterruptedException {

		Implicit_Wait(driver);
		Open_Browser(Browser, Browserpath);
		Open_Url_Window_Max(URL, driver);
		Clear_Type_Charecters(locatorType, UserName_Path, GWM_ADmin_UserName, driver);
		Clear_Type_Charecters(locatorType, Password_Path, GWM_Admin_Password, driver);
		Click_On_Element(locatorType, Login_Btn_Path, driver);
		// Thread.sleep(500);
		Part = getexcel(JNL_Excel_File_Path);
		Action = getexcelcol2(JNL_Excel_File_Path);
		Total_parts = Part.size();
//		System.out.println(Total_parts);

	}

	@AfterSuite
	public void afterClass() throws IOException {
		Writeexcel();
		Click_On_Element(locatorType, Logout_Btn_Path, driver);
		//ExplicitWait_Element_Clickable(locatorType, Login_Btn_Path, driver);
		Close_Browser(driver);
		Quit_Browser(driver);

	}
}