package com.aon.Base;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aon.Methods.CommonMethods;

public class BaseTest extends CommonMethods {

	// public static ExtentReports report;

	@BeforeSuite
	public void openbrowser() throws IOException, InterruptedException {
		// report = new ExtentReports(
		// // System.getProperty("user.dir") + "\\ExtentReports\\ExtentReports -
		// " + currentDateTime + ".html");
		// System.getProperty("user.dir") +
		// "\\ExtentReports\\ExtentReports.html");
		// report.addSystemInfo("Host Name", "Automation User");
		// report.addSystemInfo("Environment", "QA");
		// report.addSystemInfo("User Name", "Selenium User");
		// report.loadConfig(new File(System.getProperty("user.dir") +
		// "\\extent-config.xml"));

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		Implicit_Wait();
	}

	@AfterSuite(enabled = false)
	public void AfterSuite() {

		// report.flush();
		// report.close();
		Close_Browser();
		Quit_Browser();

	}

	public void login(String Username, String Password) throws IOException, InterruptedException {

		if (Element_Is_Present(locatorType, Username_Path)) {
			Type(locatorType, Username_Path, Username);
			Type(locatorType, Password_Path, Password);
			Click(locatorType, Login_btn_Path);
			Assert.assertTrue(Element_Is_Present(locatorType, LP_Logout_Path), "Login failed");

			Implicit_Wait();
		}

		else {
			logout();
			Type(locatorType, Username_Path, Username);
			Type(locatorType, Password_Path, Password);
			Click(locatorType, Login_btn_Path);
			Assert.assertTrue(Element_Is_Present(locatorType, LP_Logout_Path));
			Implicit_Wait();
		}

	}

	public void logout() throws IOException, InterruptedException {

		Click(locatorType, LP_Home_Path);
		softAssert.assertTrue(Element_Is_Present(locatorType, LP_Logout_Path));
		Click(locatorType, LP_Logout_Path);
		ExplicitWait_Element_Clickable(locatorType, Login_btn_Path);

	}

	public void NavigateMenu(String Hoverover, String Select) throws InterruptedException {

		Hover(Xpath, Hoverover);
		Click(Xpath, Select);
		Wait_ajax();

	}

	public void Datepicker(String Month, String Ok_Btn) throws InterruptedException {

		Click(locatorType, Month);
		Click(locatorType, ".//a[text()='" + Get_Futuredate("MMM") + "']");
		Wait_ajax();
		Click(locatorType, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Wait_ajax();
		Click(locatorType, Ok_Btn);
		Wait_ajax();
		Click(locatorType, ".//a[text()='" + Get_Futuredate("d") + "']");
		Wait_ajax();
	}

}
