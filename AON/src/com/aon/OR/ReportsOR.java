package com.aon.OR;

public class ReportsOR extends ManageUsersOR {
	
	public static final String Reports_Available_Path = ".//h1[contains(text(),'Available Reports')]";
	public static final String Reports_Scheduler_Path = ".//span[contains(text(),'Report Scheduler')]";
	public static final String Reports_8WeekUsageReportScheduler_Path = ".//span[contains(text(),'8WeekUsageReport Scheduler')]";
	public static final String Reports_Subscriptions_Path = ".//h1[contains(text(),'My Report Subscriptions')]";
	public static final String Reports_8WeekUsageByAliasReport_Path = ".//td[text()='8WeekUsageByAliasReport']";
	public static final String Reports_8WeekUsageByAliasReportview_Path = ".//td[text()='8WeekUsageByAliasReport']//following::a[1][contains(text(),'View Report')]";
	public static final String Reports_8WeekUsageReport_Path = ".//td[text()='8WeekUsageReport']";
	public static final String Reports_8WeekUsageReportView_Path = ".//td[text()='8WeekUsageReport']//following::a[1][contains(text(),'View Report')]";
	public static final String Reports_Firstpage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String Reports_Previouspage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String Reports_Nextpage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String Reports_Lastpage_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00']/tfoot/tr/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String Reports_Pagesize_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00_ctl03_ctl01_PageSizeComboBox_Input']";
	public static final String Reports_Pagesizearrow_Path = ".//*[@id='ctl00_cphContent_Reports_ctl00_ctl03_ctl01_PageSizeComboBox_Arrow']";
	
	// ReportPage
	
	public static final String Reports_Title_Path = ".//div[contains(text(),'8 Week Usage Report by Alias')]";
	public static final String Reports_LiteratureType_Path = ".//*[@id='rptTest_ctl00_ctl03_ddValue']";
	public static final String Reports_Channel_Path = ".//*[@id='rptTest_ctl00_ctl05_ddValue']";
	public static final String Reports_CostCenter_Path = ".//*[@id='rptTest_ctl00_ctl07_ddValue']";
	public static final String Reports_ProductCateg_Path = ".//*[@id='rptTest_ctl00_ctl09_ddDropDownButton']";
	public static final String Reports_ProductCategSelectall_Path = ".//*[@id='rptTest_ctl00_ctl09_divDropDown_ctl00']";
	public static final String Reports_Nullck_Path = ".//*[@id='rptTest_ctl00_ctl11_cbNull']";
	public static final String Reports_Viewreportbtn_Path = ".//*[@id='rptTest_ctl00_ctl00']";
	public static final String Reports_Close_Path = ".//a[@class='rwCloseButton']";
	
	public static final String Reports_Aonlogo_Path = ".//img[@alt='AON']";

	public static final String Reports_Loading_Path = ".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_Reports']";
	
}
