package com.aon.OR;

public class ManageUsersOR extends ManageSpecialProjectOR {

	public static final String MU_Title_Path = ".//h1[contains(text(),'Manage Users')]";
	public static final String MU_UserName_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_UserName']";
	public static final String MU_UserNamefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_UserName']";
	public static final String MU_Userpwd_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_UserPassword']";
	public static final String MU_Userpwdfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_UserPassword']";
	public static final String MU_Firstname_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_FirstName']";
	public static final String MU_Firstnamefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_FirstName']";
	public static final String MU_Lastname_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_LastName']";
	public static final String MU_Lastnamefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_LastName']";
	public static final String MU_Email_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_Email']";
	public static final String MU_Emailfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_Email']";
	public static final String MU_Activeck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterCheckBox_IsActive']";
	public static final String MU_Activefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_IsActive']";
	public static final String MU_Adduserbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String MU_Adduser_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String MU_Refreshbtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String MU_Refresh_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String MU_Fistpagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String MU_Previouspagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String MU_Nextpagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String MU_Lastpagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String MU_Pageno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String MU_Pagenoof_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String MU_Pagegobtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl02_GoToPageLinkButton']";
	public static final String MU_Pageofno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl02_PageOfLabel']";
	public static final String MU_Pagesize_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String MU_Pagesizechangebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String MU_Itemofitem_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";

	public static final String MUAdd_Username_Path = ".//b[text()='User Name:']//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_Usernameash_Path = ".//b[text()='User Name:']//following::td[1]//span[contains(text(),'*')]";
	public static final String MUAdd_Userpwd_Path = ".//b[text()='User Password:']//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_Userpwdash_Path = ".//b[text()='User Password:']//following::td[1]//span[contains(text(),'*')]";
	public static final String MUAdd_Firstname_Path = ".//b[text()='First Name:']//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_Firstnameash_Path = ".//b[text()='First Name:']//following::td[1]//span[contains(text(),'*')]";
	public static final String MUAdd_Lastname_Path = ".//b[text()='Last Name:']//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_Lastnameash_Path = ".//b[text()='Last Name:']//following::td[1]//span[contains(text(),'*')]";
	public static final String MUAdd_Email_Path = ".//b[text()='Email:']//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_Emailash_Path = ".//b[text()='Email:']//following::td[1]//span[contains(text(),'*')]";
	public static final String MUAdd_Address_Path = ".//b[text()='Address:']//following::td[1]//input[contains(@type,'text') and contains(@id,'Address1')]";
	public static final String MUAdd_Address2_Path = ".//b[text()='Address:']//following::td[1]//input[contains(@type,'text') and contains(@id,'Address2')]";
	public static final String MUAdd_City_Path = ".//b[text()='City:']//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_State_Path = ".//span[text()='State/Province']//following::select[contains(@id,'State')]";
	public static final String MUAdd_Zip_Path = ".//span[contains(text(),'Zip/Postal')]//following::td[1]//input[contains(@type,'text')]";
	public static final String MUAdd_Costcenter_Path = ".//b[text()='Cost Center:']//following::select[contains(@id,'CostCenter')]";
	public static final String MUAdd_Activeck_Path = ".//b[text()='Active:']//following::input[1][contains(@type,'checkbox')]";
	public static final String MUAdd_Insertbtn_Path = ".//a[text()='Insert']";
	public static final String MUAdd_Cancelbtn_Path = ".//a[text()='Cancel']";
	public static final String MUEdit_Updatebtn_Path = ".//a[text()='Update']";
	public static final String MUAdd_Administratorsck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvAudience']/ul/li/div/label/input";
	public static final String MUAdd_Apprepsck_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvAudience\"]/ul/li[2]/div/label/input";

	public static final String MUAdd_UGAonck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvBusinessOwners']/ul/li/div/label/input";

	public static final String MUAdd_Basicusersck_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity\"]/ul/li[1]/div/label/input";
	public static final String MUAdd_VeritasAdminck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity']/ul/li[1]/div/label/input";
	public static final String MUAdd_Adminck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity']/ul/li[2]/div/label/input";
	public static final String MUAdd_AssignedAonck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvChannelBrokerDealer']/ul/li[1]/div/label/input";
	public static final String MUAdd_Veritasck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvChannelBrokerDealer']/ul/li[1]/div/label/input";
	public static final String MUAdd_Administrators_Path = ".//span[text()='User Group']//following::td[1]//span[text()='Administrators']";
	public static final String MUAdd_UGAon_Path = ".//b[text()='Business Owner(s):']//following::td[1]//span[text()='Aon']";
	public static final String MUAdd_VeritasAdmin_Path = ".//b[text()='Security:']//following::td[1]//span[text()='Veritas Admin']";
	public static final String MUAdd_Admin_Path = ".//b[text()='Security:']//following::td[1]//span[text()='Admin']";
	public static final String MUAdd_AssignedAon_Path = ".//b[text()='Assigned Firm:']//following::td[1]//span[text()='AON']";
	public static final String MUAdd_Veritas_Path = ".//b[text()='Assigned Firm:']//following::td[1]//span[text()='Veritas']";

	// Edit

	public static final String MUEdit_Administratorsck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvAudience']/ul/li/div/label/input";
	public static final String MUEdit_UGAonck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvBusinessOwners']/ul/li/div/label/input";
	public static final String MUEdit_VeritasAdminck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity']/ul/li[1]/div/label/input";
	public static final String MUEdit_Adminck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity']/ul/li[2]/div/label/input";
	public static final String MUEdit_AssignedAonck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity']/ul/li[2]/div/label/input";
	public static final String MUEdit_Veritasck_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtvSecurity']/ul/li[2]/div/label/input";

	// Error messages
	public static final String MUerror_Username_Path = ".//span[text()='UserName is Required!']";
	public static final String MUerror_PWD_Path = ".//span[text()='User Password is required.']";
	public static final String MUerror_Firstname_Path = ".//span[text()='First Name is required.']";
	public static final String MUerror_Lastname_Path = ".//span[text()='Last Name is required.']";
	public static final String MUerror_Email_Path = ".//span[text()='Email is required.']";
	public static final String MUerror_Emailval_Path = ".//span[text()='ManageUsers.InvalidEmail']";

	// Result table
	public static final String MU_Row1_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0\"]";
	public static final String MU_Row2_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__1\"]";

	public static final String MU_Username1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[2]";
	public static final String MU_Password1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[3]";
	public static final String MU_Fistname1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[4]";
	public static final String MU_Lastname1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[5]";
	public static final String MU_Email1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[6]";
	public static final String MU_Activeck1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl04_ctl00']";
	public static final String MU_Edit1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl04_EditButton']";
	public static final String MU_Loading_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_rgrdUsers']";

}
