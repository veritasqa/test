package com.aon.OR;

import com.aon.values.ValueRepository;

public class Landing_SpecialProjectsPage_OR extends ValueRepository {

	// Menus
	public static final String LP_Manageinventory_Path = ".//span[text()='Manage Inventory']";
	public static final String LP_Manageorders_Path = ".//span[text()='Manage Orders']";
	public static final String LP_Manageusers_Path = ".//span[text()='Manage Users']";
	public static final String LP_SpecialProjects_Path = ".//span[text()='Special Projects']";
	public static final String LP_Reports_Path = ".//span[text()='Reports']";

	public static final String LP_AddSpecialProjects_Path = ".//span[text()='Add Special Project']";

	public static final String LP_Titlelogo_Path = ".//*[@id='ctl00_ctl00_imgLogo']";
	public static final String LP_Contactus_Path = ".//*[@id='ctl00_ctl00_ContactUsLink']";
	public static final String LP_Logout_Path = ".//a[text()='Logout']";
	public static final String LP_Username_Path = ".//*[@id='ctl00_ctl00_lblUserName']";
	public static final String LP_Home_Path = ".//span[text()='Home']";
	public static final String LP_Admin_Path = ".//span[text()='Admin']";
	public static final String LP_Support_Path = ".//span[text()='Support']";
	public static final String LP_Clearcart_Path = ".//span[text()='Clear Cart']";
	public static final String LP_Checkout_Path = ".//span[text()='Checkout']";

	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// public static final String LP__Path = "";
	// Special Project
	public static final String SPP_Title_Path = ".//h1[text()='Special Projects']";
	public static final String SPP_Header_Path = ".//*[@id='divContent clearfix']/div[2]/h1";
	public static final String SPP_ClosedTicketsbtn_Path = ".//a[text()='Closed Tickets']";
	public static final String SPP_Myticketsbtn_Path = ".//a[text()='My Tickets']";
	public static final String SPP_Assignedticketsbtn_Path = ".//a[text()='Assigned Tickets']";
	public static final String SPP_Unassignedticbtn_Path = ".//a[text()='Unassigned Tickets']";
	public static final String SPP_Quoteneededbtn_Path = ".//a[text()='Quote Needed']";
	public static final String SPP_Recentupdatesbtn_Path = ".//a[text()='Recent Updates']";
	public static final String SPP_Tickeno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_TicketNumber']";
	public static final String SPP_Tickenofilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_TicketNumber']";
	public static final String SPP_Clientname_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_ClientName']";
	public static final String SPP_Clientnamefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_ClientName']";
	public static final String SPP_JobTitle_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_JobTitle']";
	public static final String SPP_JobTitlefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_JobTitle']";
	public static final String SPP_JobType_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes_Input']";
	public static final String SPP_JobTypearrow_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxTypes']/span/button";
	public static final String SPP_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxStatus_Input']";
	public static final String SPP_Statusarrow_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_RadComboBoxStatus']/span/button";
	public static final String SPP_Duedate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_DueDate']";
	public static final String SPP_Duedatefilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_DueDate']";
	public static final String SPP_Producer_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_UserName']";
	public static final String SPP_Producerfilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_UserName']";
	public static final String SPP_AddTicketicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SPP_Addticket_Path = ".//a[text()=' Add Ticket']";
	public static final String SPP_Refreshicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String SPP_Refresh_Path = ".//a[text()=' Refresh']";
	public static final String SPP_Firstpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String SPP_Previouspagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String SPP_Nextpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String SPP_Lastpagebtn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String SPP_Custompageno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageTextBox']";
	public static final String SPP_Pagesize_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String SPP_Changetbn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String SPP_Pageitem_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";
	public static final String SPP_Loading_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects']";
	// public static final String SPP__Path = "";
	

	// Result Table
	
	public static final String SPP_TicketNo1_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]/a";
	public static final String SPP_ViewEditbtn11_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[11]/a";
	public static final String SPP_Copybt1_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[10]/a";
	public static final String SPP_Copypopup_Path = ".//div[contains(text(),'Copy?')]";
	public static final String SPP_CopyOK_Path = ".//span[text()='OK']";
	public static final String SPP_CopyCancel_Path = ".//span[text()='Cancel']";

	public static final String SPP_JobType1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[4]";
	public static final String SPP_Status1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[5]";
	public static final String SPP_Duedate1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[6]";
	public static final String SPP_Producer1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[7]";
	public static final String SPP_Fileicon1_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_gbcAttachmentImg']";





}
