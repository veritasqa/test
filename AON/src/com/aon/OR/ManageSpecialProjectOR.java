package com.aon.OR;

public class ManageSpecialProjectOR extends LoginOR {

	public static final String MSPP_GeneralTab_Path = ".//span[text()='General']";
	public static final String MSPP_ProgramTab_Path = ".//span[text()='Progress / Files']";
	public static final String MSPP_CostsTab_Path = ".//span[text()='Costs']";
	public static final String MSPP_InventoryTab_Path = ".//span[text()='Inventory']";
	public static final String MSPP_HistoryTab_Path = ".//span[text()='History']";
	public static final String MSPP_ClientTicketNo_Path = ".//*[@id='spanTicketNumber']";
	public static final String MSPP_TicketStatus_Path = ".//*[@id='spanStatus']";
	public static final String MSPP_Jobstatus_Path = ".//*[@id='spanJobTitle']";
	public static final String MSPP_Ticketno_Path = ".//*[@id='divTicketNumber']";
	public static final String MSPP_Jobtitle_Path = ".//*[@id='ctl00_cphContent_txtJobTitle']";
	public static final String MSPP_Jobtitleasterisk_Path = ".//*[@id='tabs-1']/div[1]/div[1]/div[2]/div[2]/span";
	public static final String MSPP_Jobtype_Path = ".//*[@id='ctl00_cphContent_ddlJobType_Input']";
	public static final String MSPP_Jobtypearrow_Path = ".//*[@id='ctl00_cphContent_ddlJobType']/span/button";
	public static final String MSPP_Jobtypeasterisk_Path = ".//*[@id='tabs-1']/div[1]/div[1]/div[2]/div[2]/span";
	public static final String MSPP_Frequency_Path = ".//*[@id='ctl00_cphContent_ddlFrequencey']";
	public static final String MSPP_Printspecs_Path = ".//*[@id='ctl00_cphContent_txtDescription']";
	public static final String MSPP_Assemblyinst_Path = ".//*[@id='ctl00_cphContent_txtSpecialInstructions']";
	public static final String MSPP_ClientID_Path = ".//*[@id='ctl00_cphContent_txtClientID']";
	public static final String MSPP_ClientIDasterisk_Path = ".//*[@id='ctl00_cphContent_divLeftSideGeneralTab']/div[1]/div[2]/span[1]";
	public static final String MSPP_ClientName_Path = ".//*[@id='ctl00_cphContent_txtClientName']";
	public static final String MSPP_ClientNameasterisk_Path = ".//*[@id='ctl00_cphContent_divLeftSideGeneralTab']/div[2]/div[2]/span[1]";
	public static final String MSPP_ProjectID_Path = ".//*[@id='ctl00_cphContent_txtProjectID']";
	public static final String MSPP_ProjectName_Path = ".//*[@id='ctl00_cphContent_txtProjectName']";
	public static final String MSPP_ActivityNo_Path = ".//*[@id='ctl00_cphContent_txtActivityNumber']";
	public static final String MSPP_DeptID_Path = ".//*[@id='ctl00_cphContent_txtDepartmentID']";
	public static final String MSPP_Duedate_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_dateInput']";
	public static final String MSPP_DuedateCalender_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_popupButton']";
	public static final String MSPP_DuedateCalMonth_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_calendar_Title']";
	public static final String MSPP_CalenderOk_Path = ".//*[@id='rcMView_OK']";
	public static final String MSPP_Duedateasterisk_Path = ".//*[@id='tabs-1']/div[1]/div[1]/div[7]/div[2]/span";
	public static final String MSPP_Filesdue_Path = ".//*[@id='ctl00_cphContent_dtpFileDue_dateInput']";
	public static final String MSPP_FilesdueCalender_Path = ".//*[@id='ctl00_cphContent_dtpFileDue_popupButton']";
	public static final String MSPP_RecipientCount_Path = ".//*[@id='ctl00_cphContent_txtRecipientCount']";
	public static final String MSPP_Quoteneeded_Path = ".//*[@id='ctl00_cphContent_chkQuoteNeeded']";
	public static final String MSPP_Teammembers_Path = ".//*[@id='ctl00_cphContent_RadListBoxDestination']/div/ul";
	public static final String MSPP_Teammembersempty_Path = ".//div[contains(text(),'Team Members')]//following::li[1]//span[text()]";
	public static final String MSPP_AONusersTop_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[1]";
	public static final String MSPP_AONusersDown_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[2]";
	public static final String MSPP_AONusersAlltop_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[3]";
	public static final String MSPP_AONusersAlldown_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[1]/a[4]";
	public static final String MSPP_AONusersBox_Path = ".//*[@id='ctl00_cphContent_RadListBoxSource']/div[2]";
	public static final String MSPP_Disposition_Path = ".//*[@id='ctl00_cphContent_ddlDisposition']";
	public static final String MSPP_Dispositionasterisk_Path = ".//*[@id='tabs-1']/div[1]/div[1]/div[7]/div[2]/span";
	public static final String MSPP_Dispositionerrormessage_Path = ".//span[text()='* Your Disposition selection will take place 2 weeks from the final mail/ship date.']";
	public static final String MSPP_Dispositionerrormessage2_Path = ".//span[text()='* Disposition of all left over materials for R&P projects will take place 8 weeks from the original mail/ship date (except for quarterly mailing envelopes). ']";
	public static final String MSPP_Createdby_Path = ".//*[@id='ctl00_cphContent_lblCreateUser']";
	public static final String MSPP_AddfromInventory_Path = ".//label[text()='Add from Inventory']";
	public static final String MSPP_AddfromInventoryJobtype_Path = ".//*[@id='ctl00_cphContent_txtJobType']";
	public static final String MSPP_AddfromInventoryFormno_Path = ".//*[@id='ctl00_cphContent_racbFormNumbers']/div[1]";
	public static final String MSPP_AddtoProjectbtn_Path = ".//span[text()='Add to Project']";
	public static final String MSPP_OrderforProject_Path = ".//label[text()='Order for Project']";
	public static final String MSPP_OrderFormno_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00']/thead/tr/th[2]/a";
	public static final String MSPP_OrderDesc_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00']/thead/tr/th[3]/a";
	public static final String MSPP_OrderQty_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00']/thead/tr/th[5]";
	public static final String MSPP_OrderUnitsavailable_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00']/thead/tr/th[6]/a";
	public static final String MSPP_OrderInventorytype_Path = ".//*[@id='ctl00_cphContent_Parts_ctl00']/thead/tr/th[7]/a";
	public static final String MSPP_Printfile_Path = ".//h2[text()='Print File']";
	public static final String MSPP_Data_Path = ".//h2[text()='Data File']";
	public static final String MSPP_UploadPrintfilebtn_Path = ".//*[@id='ctl00_cphContent_btnUploadPrintFile']";
	public static final String MSPP_ViewUploadedFilemsg_Path = ".//a[text()='View Uploaded File']";
	public static final String MSPP_Uploaddatafilebtn_Path = ".//*[@id='ctl00_cphContent_btnUploadDataFile']";
	public static final String MSPP_Autofileexp_Path = ".//h2[text()='Automation File Explorer']";
	public static final String MSPP_DownloadArchivebtn_Path = ".//*[@id='ctl00_cphContent_btnDownload']";
	public static final String MSPP_Cancelbtn_Path = ".//*[@id='ctl00_cphContent_btnCancel']";
	public static final String MSPP_Savebtn_Path = ".//*[@id='ctl00_cphContent_btnSave']";
	public static final String MSPP_SuccessSavemsg_Path = ".//span[text()='Data has been saved.']";
	public static final String MSPP_Loading_Path = ".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_RadListBoxDestination']";
	
	// Error messages
	
	public static final String MSPP_Error_Title_Path = ".//div[contains(text(),'You must enter a value in the following fields:')]";
	public static final String MSPP_Error_Jobtitle_Path = ".//li[text()='Job Title is required']";
	public static final String MSPP_Error_Disposition_Path = ".//li[text()='Disposition is required']";
	public static final String MSPP_Error_Clientid_Path = ".//li[text()='Client ID is required']";
	public static final String MSPP_Error_ClientName_Path = ".//li[text()='Client Name is required']";
	public static final String MSPP_Error_Jobtype_Path = ".//li[text()='Job Type is required']";
	public static final String MSPP_Errorfield_Jobtitle_Path = ".//span[text()='Job Title is required']";
	public static final String MSPP_Errorfield_Jobtype_Path = ".//span[text()='Job Type is required']";
	public static final String MSPP_Errorfield_ClientId_Path = ".//span[text()='Client ID is required']";
	public static final String MSPP_Errorfield_Clientname_Path = ".//span[text()='Client Name is required']";
	public static final String MSPP_Errorfield_Disposition_Path = ".//span[text()='Disposition is required']";
	public static final String MSPP_ErrorCreate__Path = ".//label[text()='Create the Special Project, before proceeding.']";
	public static final String MSPP_Errorfield__Path = ".//span[text()='Job Title is required']";



	// Program Tab

	public static final String MSPPProg_Created_Path = ".//*[@id='ctl00_cphContent_lblCreateDate']";
	public static final String MSPPProg_QuoteAccepted_Path = ".//*[@id='ctl00_cphContent_lblQuoteAcceptedDate']";
	public static final String MSPPProg_ProofCreated_Path = ".//*[@id='ctl00_cphContent_lblProofDate']";
	public static final String MSPPProg_ProofApproved_Path = ".//*[@id='ctl00_cphContent_lblProofApprovalDate']";
	public static final String MSPPProg_DueDate_Path = ".//*[@id='ctl00_cphContent_lblDueDate']";
	public static final String MSPPProg_Completed_Path = ".//*[@id='ctl00_cphContent_lblCompleteDate']";
	public static final String MSPPProg_Cancelled_Path = ".//*[@id='ctl00_cphContent_lblCancelDate']";
	public static final String MSPPProg_HoldPlaced_Path = ".//*[@id='ctl00_cphContent_lblHoldDate']";
	public static final String MSPPProg_CreateDate_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/thead/tr[2]/td[2]";
	public static final String MSPPProg_UserName_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_UserName']";
	public static final String MSPPProg_Topic_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_Topic']";
	public static final String MSPPProg_Comment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_Comment']";
	public static final String MSPPProg_Status_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_FilterTextBox_Type']";
	public static final String MSPPProg_Attachment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/thead/tr[2]/td[7]";
	public static final String MSPPProg_Addcommenticon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String MSPPProg_Addcomment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String MSPPProg_Refreshicon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String MSPPProg_Refresh_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String MSPPProg_Laading_Path = ".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_Comments']";
	public static final String MSPPProg_AddTopic_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_txtTopic']";
	public static final String MSPPProg_AddComment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_txtComment']";
	public static final String MSPPProg_AddStatus_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_ddlType_Input']";
	public static final String MSPPProg_AddStatusarrow_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_ddlType_Arrow']";
	public static final String MSPPProg_Addattachment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_uploadCommentAttachmentrow0']/span";
	public static final String MSPPProg_InsertCommentbtn_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_btnSaveComment']";
	public static final String MSPPProg_CancelComment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl03_btnCancel']";
	public static final String MSPPProg_Firstpage_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String MSPPProg_Previuospage_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String MSPPProg_Nextpage_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String MSPPProg_Lastpage_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String MSPPProg_Pagesize_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
	public static final String MSPPProg_Changebtn_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
	public static final String MSPPProg_Itemof_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]";
	public static final String MSPPProg_Itemtext_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]/text()[1]";
	public static final String MSPPProg_1text_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]/strong[1]";
	public static final String MSPPProg_totext_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]/text()[2]";
	public static final String MSPPProg_counttext_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]/strong[2]";
	public static final String MSPPProg_oftext_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]/text()[3]";
	public static final String MSPPProg_ntext_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[4]/strong[3]";
		
	public static final String AonExcelFile_1 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Aon- Excel File_1.xlsx";
	public static final String Multiuploaddirectory = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload";
	public static final String Multiupload1 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload\\Aon- Excel File_1.xlsx";
	public static final String Multiupload2 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload\\Aon_Sample.docx";
	public static final String Multiupload3 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload\\Download File.docx";
	public static final String Multiupload4 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload\\Quality icon.png";
	public static final String Multiupload5 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload\\Test panda file.pdf";
	public static final String Multiupload6 = System.getProperty("user.dir")
			+ "\\src\\com\\aon\\Utils\\Multiple Fileupload\\Visio file.vsdx";
	

	

	public static final String MSPPProg_Attachmentrow1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']";
	public static final String MSPPProg_CreateDate1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[3]";
	public static final String MSPPProg_Green1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl04_btnSelectRow']";
	public static final String MSPPProg_Red1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl04_btnDeselectRow']";
	public static final String MSPPProg_Hiddengrey1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl04_btnArchiveRow']";

	public static final String MSPPProg_UserName1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[3]";
	public static final String MSPPProg_Topic1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[4]";
	public static final String MSPPProg_Comment1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[5]";
	public static final String MSPPProg_Status1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[6]";
	public static final String MSPPProg_Attachment1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[7]/a";
	public static final String MSPPProg_NoneAttachment1_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00__0']/td[7]";
	// public static final String MSPPProg__Path = "";

	// Costs Tab

	public static final String MSPPCost_QuoteCost_Path = ".//*[@id='ctl00_cphContent_txtQuoteCost']";
	public static final String MSPPCost_PrintCost_Path = ".//*[@id='ctl00_cphContent_txtPrintCost']";
	public static final String MSPPCost_VeritasPostage_Path = ".//*[@id='ctl00_cphContent_txtVeritasPostageCost']";
	public static final String MSPPCost_Fulfillment_Path = ".//*[@id='ctl00_cphContent_txtFulfillmentCost']";
	public static final String MSPPCost_ClientPostage_Path = ".//*[@id='ctl00_cphContent_txtClientPostageCost']";
	public static final String MSPPCost_Rush_Path = ".//*[@id='ctl00_cphContent_txtRushCost']";
	public static final String MSPPCost_Billed_Path = "//*[@id='ctl00_cphContent_ckBilled']";
	public static final String MSPPCost_Addcosticon_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String MSPPCost_Addcostitem_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String MSPPCost_Refreshicon_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String MSPPCost_Refresh_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String MSPPCost_AddDescription_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_txtDescription']";
	public static final String MSPPCost_AddType_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType_Input']";
	public static final String MSPPCost_AddTypearrow_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_ddlType_Arrow']";
	public static final String MSPPCost_AddBilldate_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rdpBillDate_dateInput']";
	public static final String MSPPCost_AddBillamount_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_rtxtBillAmount']";
	public static final String MSPPCost_UpdateBillamount_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl05_rtxtBillAmount']";

	public static final String MSPPCost_AddInsert_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_btnSave']";
	public static final String MSPPCost_UpdateInsert_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl05_btnSave']";

	public static final String MSPPCost_AddCancel_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl02_ctl02_btnCancelCost']";
	public static final String MSPPCost_Description1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[1]";
	public static final String MSPPCost_Type1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[2]";
	public static final String MSPPCost_Billdate1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[3]";
	public static final String MSPPCost_Billamount1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[4]";
	public static final String MSPPCost_Edit1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl04_EditButton']";
	public static final String MSPPCost_Delete1_Path = ".//*[@id='ctl00_cphContent_rgdCosts_ctl00__0']/td[6]/a";
	public static final String MSPPCost_Deletepop_Path = ".//div[contains(text(),'Delete Item?')]";
	public static final String MSPPCost_DeleteOK_Path = ".//span[text()='OK']";
	public static final String MSPPCost_DeleteCancel_Path = ".//span[text()='Cancel']";
	public static final String MSPPCost_Loading_Path = "//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_rgdCosts']";

	// Inventory Tab

	public static final String MSPPInv_Jobtype_Path = ".//*[@id='ctl00_cphContent_txtJobType']";
	public static final String MSPPInv_FormNo_Path = ".//*[@id='ctl00_cphContent_racbFormNumbers']/div[1]";
	public static final String MSPPInv_AddtoProject_Path = ".//*[@id='ctl00_cphContent_btnAddFormNumbers']/span";
	public static final String MSPPInv__Path = "";

	// History Tab

	public static final String MSPPHist_DateTime1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[1]";
	public static final String MSPPHist_User1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[2]";
	public static final String MSPPHist_Details1_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";

	// public static final String MSPP__Path = "";

}
