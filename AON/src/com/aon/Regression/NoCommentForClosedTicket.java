package com.aon.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.aon.Base.BaseTest;

public class NoCommentForClosedTicket extends BaseTest {

	@Test
	public void AON_TC_2_2_10_1_24() throws InterruptedException, IOException {

		login(BasicUserName, BasicPassword);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Title_Path), "'Special Projects' page is not displays");
		Click(Xpath, SPP_AddTicketicon_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_GeneralTab_Path), "General Tab is not displayed");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Ticketno_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displays by 'Ticket Number' field");
		Type(Xpath, MSPP_Jobtitle_Path, "QA Test");
		Select_lidropdown(MSPP_Jobtypearrow_Path, "Assembly Only");
		Type(Xpath, MSPP_ClientID_Path, "AON Test");
		Type(Xpath, MSPP_ClientName_Path, "AON Test");
		Select_DropDown_VisibleText(Xpath, MSPP_Disposition_Path, "Return To Bond Street");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		SplprjTicketNumber = Get_Text(Xpath, MSPP_Ticketno_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Reporter.log("Created Ticket# is " + SplprjTicketNumber);
		Click(locatorType, LP_Logout_Path);
		Closealltabs();
		login(UserName, Password);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Title_Path), "'Special Projects' page is not displays");
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, MSPPProg_Addcommenticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Type(Xpath, MSPPProg_AddComment_Path, "QA Test");
		Select_lidropdown(MSPPProg_AddStatusarrow_Path, "Closed");
		Click(Xpath, MSPPProg_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_GeneralTab_Path), "General Tab is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Assert.assertTrue(Get_Text(Xpath, MSPP_TicketStatus_Path).trim().equalsIgnoreCase("Closed"),
				" 'Closed' is not displays by 'Status' on top right of the page");
		Click(locatorType, LP_Logout_Path);
		Closealltabs();
		login(BasicUserName, BasicPassword);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Title_Path), "'Special Projects' page is not displays");
		Click(Xpath, SPP_ClosedTicketsbtn_Path);
		Thread.sleep(4000);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, MSPPProg_Addcommenticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Assert.assertFalse(Element_Is_Enabled(Xpath, MSPPProg_InsertCommentbtn_Path),
				"'Insert Comment' is not grayed out");
		Assert.assertFalse(GetCSS_Backgroundcolor(Xpath, MSPPProg_InsertCommentbtn_Path).equalsIgnoreCase("#840a2d"),
				"'Insert Comment' is not grayed out");

	}

}
