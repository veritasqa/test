package com.aon.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aon.Base.BaseTest;

public class ManageUsers extends BaseTest {

	public SoftAssert softAssert;

	public void Manageusersfilter(String Filter, String Filteroption) throws InterruptedException {

		Click(Xpath, Filter);
		Wait_ajax();
		Click(Xpath, Filteropt(Filteroption));
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Wait_ajax();

	}

	@Test(enabled = true, priority = 1)
	public void AON_TC_2_2_9_2() throws InterruptedException, IOException {

		// Validate that filtering functionality works appropriately

		NavigateMenu(LP_Admin_Path, LP_Manageusers_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MU_Title_Path), "'Manage Users' page is not displays");

		Type(Xpath, MU_UserName_Path, UserName);
		Click(Xpath, MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, MU_Username1_Path).trim().equalsIgnoreCase(UserName),
				"<User Name> is not displays in 'User Name' column");
		Manageusersfilter(MU_UserNamefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, MU_Pagesize_Path, "value"))) > 1,
				"<all results> is not displayed");

		Type(Xpath, MU_Userpwd_Path, Password);
		Click(Xpath, MU_Userpwdfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, MU_Password1_Path).trim().contains("**"),
				"<Password> is not displays in 'User Name' column");
		Manageusersfilter(MU_Userpwdfilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, MU_Pagesize_Path, "value"))) > 1,
				"<all results> is not displayed - Password");

		Type(Xpath, MU_Firstname_Path, UserFName);
		Click(Xpath, MU_Firstnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, MU_Fistname1_Path).trim().equalsIgnoreCase(UserFName),
				"<First Name> is not displays in 'User Name' column");
		Manageusersfilter(MU_Firstnamefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, MU_Pagesize_Path, "value"))) > 1,
				"<all results> is not displayed - Firstname");

		Type(Xpath, MU_Lastname_Path, UserLName);
		Click(Xpath, MU_Lastnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, MU_Lastname1_Path).trim().equalsIgnoreCase(UserLName),
				"<Last Name> is not displays in 'User Name' column");
		Manageusersfilter(MU_Lastnamefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, MU_Pagesize_Path, "value"))) > 1,
				"<all results> is not displayed - Lastname");

		Type(Xpath, MU_Email_Path, Useremail);
		Click(Xpath, MU_Emailfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, MU_Email1_Path).trim().equalsIgnoreCase(Useremail),
				"<Email> is not displays in 'User Name' column");
		Manageusersfilter(MU_Emailfilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, MU_Pagesize_Path, "value"))) > 1,
				"<all results> is not displayed - Email");

		Click(Xpath, MU_Activeck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, MU_Activeck1_Path), "<all active users> records is not display");
		Manageusersfilter(MU_Activefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, MU_Pagesize_Path, "value"))) > 1,
				"<all results> is not displayed - Active");

	}

	@Test(enabled = true, priority = 2)
	public void AON_TC_2_2_9_3() throws InterruptedException, IOException {

		// Validate Edit option opens on Search Result Row and allows you to
		// edit the user

		Type(Xpath, MU_UserName_Path, UserName);
		Click(Xpath, MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, MU_Username1_Path).trim().equalsIgnoreCase(UserName),
				"<User Name> is not displays in'User Name' column");
		Click(Xpath, MU_Edit1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MU_Title_Path), "'Manage Users' page is not displays");
		Type(Xpath, MUAdd_Address_Path, Address1 + "-123");
		Click(Xpath, MUEdit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Click(Xpath, MU_Edit1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Get_Attribute(Xpath, MUAdd_Address_Path, "value").equalsIgnoreCase(Address1 + "-123"),
				"<913 Commerce court-123> is not displays in 'Address' field");
		Type(Xpath, MUAdd_Address_Path, Address1);
		Click(Xpath, MUEdit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Manageusersfilter(MU_UserNamefilter_Path, "NoFilter");

	}

	@Test(enabled = true, priority = 3)
	public void AON_TC_2_2_9_4() throws InterruptedException, IOException {

		// Validate Add User link opens "Add User mode" and user is allowed to
		// enter values

		Click(Xpath, MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Type(Xpath, MUAdd_Username_Path, "QA Test AON_TC_2_2_9_4");
		Type(Xpath, MUAdd_Userpwd_Path, "QA Test AON_TC_2_2_9_4");
		Type(Xpath, MUAdd_Firstname_Path, "QA Test First name");
		Type(Xpath, MUAdd_Lastname_Path, "QA Test Last name");
		Type(Xpath, MUAdd_Email_Path, "Ver.QAAUTO@rrd.com");
		Type(Xpath, MUAdd_Address_Path, Address1);
		Type(Xpath, MUAdd_City_Path, City);
		Select_DropDown_VisibleText(Xpath, MUAdd_State_Path, "Illinois");
		Type(Xpath, MUAdd_Zip_Path, Zipcode);
		Select_DropDown_VisibleText(Xpath, MUAdd_Costcenter_Path, "9999 - Veritas Cost Center");
		Assert.assertTrue(Element_Is_selected(Xpath, MUAdd_Activeck_Path),
				"'Active User' Checkbox is not checked off in 'Active' field");
		Click(Xpath, MUAdd_Administratorsck_Path);
		Click(Xpath, MUAdd_Apprepsck_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, MUAdd_UGAonck_Path),
				"'Aon' checkbox not appears in 'Business Owner(s)' field");
		Click(Xpath, MUAdd_Basicusersck_Path);
		Click(Xpath, MUAdd_VeritasAdminck_Path);
		Click(Xpath, MUAdd_Veritasck_Path);
		Click(Xpath, MUAdd_AssignedAon_Path);
		Click(Xpath, MUAdd_Veritas_Path);
		Click(Xpath, MUAdd_Cancelbtn_Path);
	}

	@Test(enabled = true, priority = 4)
	public void AON_TC_2_2_9_5() throws InterruptedException, IOException {

		// Validate page and page size functionality works appropriately.

		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"Page '2' is not highlighted at the bottom of the page");

		Type(Xpath, MU_Pagesize_Path, "1");
		Click(Xpath, MU_Pagesizechangebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, MU_Row2_Path), "Only one row is not displayed");
		Type(Xpath, MU_Pageno_Path, "2");
		Click(Xpath, MU_Pagegobtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"Page '2' is not highlighted at the bottom of the page - Go button navigation");
		Click(Xpath, MU_Nextpagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"Page '3' is not highlighted at the bottom of the page");
		Click(Xpath, MU_Fistpagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted at the bottom of the page");
		Click(Xpath, MU_Lastpagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		String Pageof = (Get_Text(Xpath, MU_Pagenoof_Path).substring(3, Get_Text(Xpath, MU_Pagenoof_Path).length()));
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination(Pageof)),
				"Last Page is not highlighted at the bottom of the page");
		Click(Xpath, MU_Previouspagenav_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Assert.assertFalse(Element_Is_Displayed(Xpath, Pagination(Pageof)),
				"last Previous is not highlighted at the bottom of the page - Previous page navigation");

		

	}

	@Test(enabled = true, priority = 5)
	public void AON_TC_2_2_9_7() throws InterruptedException, IOException {

		// Validate error messages are displaying correctly

		Assert.assertTrue(Element_Is_Displayed(Xpath, MU_Title_Path), "'Manage Users' page is not displays");
		Click(Xpath, MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MU_Loading_Path);
		Click(Xpath, MUAdd_Insertbtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MUerror_Username_Path),
				"'UserName is Required' error message is not displays near 'User Name' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MUerror_PWD_Path),
				"'Password is Required' error message is not displays near 'User Name' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MUerror_Firstname_Path),
				"'FirstName is Required' error message is not displays near 'User Name' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MUerror_Lastname_Path),
				"'LastName is Required' error message is not displays near 'User Name' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MUerror_Email_Path),
				"'Email is Required' error message is not displays near 'User Name' field");

	}

	@BeforeClass
	public void BeforeClass() throws InterruptedException, IOException {

		login(UserName, Password);

	}

	@BeforeMethod
	public void BefMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void AfterMethod() throws InterruptedException {

		NavigateMenu(LP_Admin_Path, LP_Manageusers_Path);
		Wait_ajax();

	}

	@AfterClass
	public void AfterTest() throws InterruptedException {

	}

}
