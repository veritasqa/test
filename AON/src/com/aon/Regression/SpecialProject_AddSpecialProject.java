package com.aon.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aon.Base.BaseTest;

public class SpecialProject_AddSpecialProject extends BaseTest {

	public SoftAssert softAssert;

	public void AddNewTicket() throws InterruptedException {

		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Click(Xpath, SPP_AddTicketicon_Path);
		Wait_ajax();
		Type(Xpath, MSPP_Jobtitle_Path, "QA Test");
		Get_DropDown(MSPP_Jobtypearrow_Path, "Print & Assembly");
		Type(Xpath, MSPP_Duedate_Path, Get_Todaydate("MM/dd/yyyy"));
		Select_DropDown_VisibleText(Xpath, MSPP_Disposition_Path, "Recycle");
		Type(Xpath, MSPP_ClientID_Path, "123");
		Type(Xpath, MSPP_ClientName_Path, "AON");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		SplprjTicketNumber = Get_Text(Xpath, MSPP_Ticketno_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Reporter.log("Created Ticket# is " + SplprjTicketNumber);

	}

	public void Selectfilter(String Filter, String Filteroption) throws InterruptedException {

		Click(Xpath, Filter);
		Wait_ajax();
		Click(Xpath, Filteropt(Filteroption));
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Wait_ajax();

	}

	public String Teammemberlist(String Username) {

		return ".//div[contains(text(),'Team Members')]//following::li[1]//span[text()='" + Username + " - (Veritas)']";
	}

	public String AONuserslist(String Username) {

		return ".//div[contains(text(),'AON Users')]//following::li//span[text()='" + Username + "']";
	}

	public String History(String Changehistory) {

		return ".//td[contains(text(),'" + Changehistory + "')]";
	}

	public String Multifiles(String Filename) {

		return ".//span[text()='" + Filename + "']//following::input[@value='Remove']";
	}

	@Test(enabled = true, priority = 1)
	public void AON_TC_2_2_10_1_1() throws InterruptedException, IOException {

		// Validate Filter functionality for all the column headings on Special
		// Projects: View Tickets page

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		// Pre-Req
		AddNewTicket();
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);

		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_Tickenofilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - Ticket# Filter");
		Type(Xpath, SPP_Clientname_Path, "AON");
		Click(Xpath, SPP_Clientnamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_Clientnamefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - Client Name filter");

		Type(Xpath, SPP_JobTitle_Path, "QA Test");
		Click(Xpath, SPP_JobTitlefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Selectfilter(SPP_JobTitlefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - job title filter");

		Get_DropDown(SPP_JobType_Path, "Print & Assembly");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_JobType1_Path).equalsIgnoreCase("Print & Assembly"),
				"'Print & Assembly' from 'Job Type' column is not displays ");
		Get_DropDown(SPP_JobType_Path, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - job type filter");

		Get_DropDown(SPP_Statusarrow_Path, "New");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Status1_Path).equalsIgnoreCase("New"),
				"'New' from 'Status' column is not displays ");
		Get_DropDown(SPP_Statusarrow_Path, "All");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - Status filter");

		Type(Xpath, SPP_Duedate_Path, Get_Todaydate("MM/dd/yyyy"));
		Click(Xpath, SPP_Duedatefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Duedate1_Path).equalsIgnoreCase(Get_Todaydate("MM/dd/yyyy")),
				"'Today date' from 'Due date' column is not displays ");
		Selectfilter(SPP_Duedatefilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - Due date filter");

		Type(Xpath, SPP_Producer_Path, UserName);
		Click(Xpath, SPP_Producerfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Producer1_Path).equalsIgnoreCase(UserName),
				"'Today date' from 'Due date' column is not displays ");
		Selectfilter(SPP_Producerfilter_Path, "NoFilter");
		Assert.assertTrue((Integer.parseInt(Get_Attribute(Xpath, SPP_Pagesize_Path, "value"))) > 1,
				"All tickets are not shown - Producer Name filter");
	}

	@Test(enabled = true, priority = 2)
	public void AON_TC_2_2_10_1_2() throws InterruptedException, IOException {

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Get_DropDown(SPP_JobType_Path, "Assembly Only");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_JobType1_Path).equalsIgnoreCase("Assembly Only"),
				"Assembly Only results is not display");
		Get_DropDown(SPP_Statusarrow_Path, "New");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Status1_Path).equalsIgnoreCase("New"),
				"'New' from 'Status' column is not displays ");

	}

	@Test(enabled = true, priority = 2)
	public void AON_TC_2_2_10_1_3() throws InterruptedException, IOException {

		// Verify "Add Ticket", "View/Edit", "Refresh" button, "Closed Tickets",
		// "My Tickets", "Assigned Tickets", "Unassigned Tickets", "Quote
		// Needed" and "Recent Updates" displays on the Special Projects table.

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_AddTicketicon_Path),
				"Add ticket button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Refreshicon_Path), "Refresh Button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_ViewEditbtn11_Path), "View/Edit is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Copybt1_Path), "Copy button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_ClosedTicketsbtn_Path),
				"Closed tickets is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Myticketsbtn_Path), "My Tickets button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Assignedticketsbtn_Path),
				"Assigned Ticket is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Unassignedticbtn_Path),
				"Unassigned Ticket is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Quoteneededbtn_Path),
				"Quote Needed button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, SPP_Recentupdatesbtn_Path),
				"Recent updates is not displayed");
		softAssert.assertAll();
	}

	@Test(enabled = true, priority = 3)
	public void AON_TC_2_2_10_1_4() throws InterruptedException, IOException {

		// Validate "Add ticket button",
		// Edit/View button takes to "General" Special Projects page

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Click(Xpath, SPP_AddTicketicon_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_GeneralTab_Path), "General Tab is not displayed");
		Click(Xpath, MSPP_Cancelbtn_Path);
		Wait_ajax();
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_GeneralTab_Path), "General Tab is not displayed");
		Switch_Old_Tab();

	}

	@Test(enabled = true, priority = 4)
	public void AON_TC_2_2_10_1_5() throws InterruptedException, IOException {

		// Add a new Special Projects and verify new ticket appears on Special
		// Projects: view tickets table

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Click(Xpath, SPP_AddTicketicon_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_GeneralTab_Path), "General Tab is not displayed");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Ticketno_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displays by 'Ticket Number' field");
		Type(Xpath, MSPP_Jobtitle_Path, "QA Test");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Jobtitleasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Job Title' field as Required field ");
		Get_DropDown(MSPP_Jobtypearrow_Path, "Print & Assembly");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Jobtypeasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Job Type' field as Required field ");

		Type(Xpath, MSPP_Duedate_Path, Get_Futuredate("MM/dd/yyyy"));
		Assert.assertTrue(Get_Text(Xpath, MSPP_Duedateasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Due date' field as Required field ");

		Select_DropDown_VisibleText(Xpath, MSPP_Disposition_Path, "Recycle");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Dispositionasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Disposition' field as Required field ");

		Type(Xpath, MSPP_ClientID_Path, "123");
		Assert.assertTrue(Get_Text(Xpath, MSPP_ClientIDasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Client ID' field as Required field ");

		Type(Xpath, MSPP_ClientName_Path, "AON");
		Assert.assertTrue(Get_Text(Xpath, MSPP_ClientNameasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Client name' field as Required field ");

		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");

		SplprjTicketNumber = Get_Text(Xpath, MSPP_Ticketno_Path);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Reporter.log("Created Ticket# is " + SplprjTicketNumber);
		Click(locatorType, LP_Home_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				" Newly created <Ticket #> is not displays on the first row of 'Special Projects : View Tickets' grid");

	}

	@Test(enabled = true, priority = 5, dependsOnMethods = "AON_TC_2_2_10_1_5")
	public void AON_TC_2_2_10_1_6() throws InterruptedException, IOException, AWTException {

		// Validate that user's receive Special Projects Validation email with
		// appropriate contents

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Type(Xpath, MSPP_Printspecs_Path, "QA Testing Print Spec");
		Type(Xpath, MSPP_Assemblyinst_Path, "QA Testing Assembly Instructions");
		Assert.assertFalse(
				Get_Attribute(Xpath, MSPP_Duedate_Path, "value").equalsIgnoreCase(Get_Todaydate("MM/d/yyyy")),
				"<Future Date - MM/DD/YYYY> is not displays in ' Due Date' field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_DuedateCalender_Path),
				" 'Calendar icon' is not displays by  'Due Date' ");
		Click(Xpath, MSPP_DuedateCalender_Path);
		Wait_ajax();
		Datepicker(MSPP_DuedateCalMonth_Path, MSPP_CalenderOk_Path);
		Assert.assertTrue(Get_Attribute(Xpath, MSPP_RecipientCount_Path, "value").equalsIgnoreCase("0"),
				" '0' is not displays in 'Recipient Count' field ");
		Type(Xpath, MSPP_RecipientCount_Path, "10");
		Assert.assertFalse(Element_Is_selected(Xpath, MSPP_Quoteneeded_Path), "'Is Quote Needed?' checkbox is checked");
		Click(Xpath, MSPP_Quoteneeded_Path);
		Assert.assertTrue(Get_Text(Xpath, MSPP_Createdby_Path).trim().equalsIgnoreCase(UserName),
				"<Login User Name> is not displays by 'Created By' field");
		Type(Xpath, MSPP_ProjectID_Path, "456");
		Type(Xpath, MSPP_ProjectName_Path, "Test");
		Type(Xpath, MSPP_ActivityNo_Path, "001");
		Type(Xpath, MSPP_DeptID_Path, "002");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Teammemberlist(UserName)),
				" <User Name - (Veritas)> is not displays under 'Team Members' fields");
		Assert.assertTrue(Get_Attribute(Xpath, MSPP_AONusersTop_Path, "title").equalsIgnoreCase("To Top"),
				" 'To Top' is not displays on hovering over 'Top Arrow'");
		Assert.assertTrue(Get_Attribute(Xpath, MSPP_AONusersDown_Path, "title").equalsIgnoreCase("To Bottom"),
				" 'To Bottom' is not displays on hovering over 'Top Arrow'");
		Assert.assertTrue(Get_Attribute(Xpath, MSPP_AONusersAlltop_Path, "title").equalsIgnoreCase("All to Top"),
				" 'All To Top' is not displays on hovering over 'All to Top'");
		Assert.assertTrue(Get_Attribute(Xpath, MSPP_AONusersAlldown_Path, "title").equalsIgnoreCase("All to Bottom"),
				" 'All To Top' is not displays on hovering over 'All to Bottom'");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Jobstatus_Path).trim().equalsIgnoreCase("QA Test"),
				"'QA Test' is not displays by 'Job' field on top right of the page");
		Assert.assertTrue(Get_Text(Xpath, MSPP_TicketStatus_Path).trim().equalsIgnoreCase("New"),
				" 'New' is not displays by 'Status' on top right of the page");
		Assert.assertTrue(Get_Text(Xpath, MSPP_ClientTicketNo_Path).trim().equalsIgnoreCase("Aon"),
				" 'Aon' is not displays by 'Client' on top right of the page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_DownloadArchivebtn_Path),
				"'Download Archive' button is not displays at the bottom right of the 'General' page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Cancelbtn_Path),
				" 'Cancel' button is not displays on 'General' page at the bottom right of the page");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_DownloadArchivebtn_Path),
				"'Download Archive' button is not displays at the bottom right of the 'General' page");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Jobstatus_Path).trim().equalsIgnoreCase("QA Test"),
				"'QA Test' is not displays by 'Job' field on top right of the page");
		Assert.assertTrue(Get_Text(Xpath, MSPP_TicketStatus_Path).trim().equalsIgnoreCase("New"),
				" 'New' is not displays by 'Status' on top right of the page");
		Assert.assertTrue(Get_Text(Xpath, MSPP_ClientTicketNo_Path).trim().equalsIgnoreCase("Aon"),
				" 'Aon' is not displays by 'Client' on top right of the page");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Ticketno_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"<Ticket #> is not displays by 'Ticket Number' field");
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Created_Path).trim().equalsIgnoreCase(Get_Todaydate("MM/d/yyyy")),
				" <Today's Date - MM/DD/YYYY> is not displays under 'Created' column");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_QuoteAccepted_Path).trim().equalsIgnoreCase("N/A"),
				"'N/A' is not displays under 'Quote Accepted' column");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_ProofCreated_Path).trim().equalsIgnoreCase("N/A"),
				"'N/A' is not  displays under 'Proof Created' column");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_ProofApproved_Path).trim().equalsIgnoreCase("N/A"),
				" 'N/A' is not  displays under 'Proof Approved' column");
		Assert.assertFalse(Get_Text(Xpath, MSPPProg_DueDate_Path).trim().equalsIgnoreCase(Get_Todaydate("MM/d/yyyy")),
				"<Future Date - MM/DD/YYYY>  is not under 'Due Date' column");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Completed_Path).trim().equalsIgnoreCase("N/A"),
				"'N/A' is not  displays under 'Completed' column");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Cancelled_Path).trim().equalsIgnoreCase("N/A"),
				" 'N/A' is not  displays under 'Cancelled' column");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_HoldPlaced_Path).trim().equalsIgnoreCase("N/A"),
				" 'N/A' is not  displays under 'Hold Placed' column");
		Assert.assertTrue(Get_Attribute(Xpath, MSPPProg_Red1_Path, "src").contains("red"),
				"'Red' checkbox button is not displays on the left side of the grid");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_UserName1_Path).trim().equalsIgnoreCase(UserName),
				"<User's login> is not displays under 'User Name'");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Topic1_Path).trim().equalsIgnoreCase("Quote Needed"),
				"'Quote Needed' is not displays under 'Topic'");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Comment1_Path).trim().equalsIgnoreCase("Quote Needed"),
				"'Quote Needed' is not displays under 'Comment' column ");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Status1_Path).trim().equalsIgnoreCase("Quote Needed"),
				"'Quote Needed' is not displays under 'Status' column ");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_NoneAttachment1_Path).trim().equalsIgnoreCase("None"),
				"'None' is not displays under 'Attachment' column ");

		Click(Xpath, MSPPProg_Addcommenticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Type(Xpath, MSPPProg_AddTopic_Path, "Test");

		Type(Xpath, MSPPProg_AddComment_Path, "QA Test");
		Get_DropDown(MSPPProg_AddStatusarrow_Path, "Acknowledged");
		Click(Xpath, MSPPProg_Addattachment_Path);
		Fileupload(AonExcelFile_1);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile1));
		Click(Xpath, MSPPProg_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(Xpath, MSPPProg_Green1_Path, "src").contains("green"),
				"'Green' checkbox button is not displays on the left side of the grid");
		Click(Xpath, MSPPProg_Green1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Assert.assertTrue(Get_Attribute(Xpath, MSPPProg_Red1_Path, "src").contains("red"),
				"'Red' checkbox button is not displays on the left side of the grid");

		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, MSPPProg_Attachmentrow1_Path).equalsIgnoreCase("#828282"),
				"The whole row is not greys out");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_UserName1_Path).trim().equalsIgnoreCase(UserName),
				"<User's login> is not displays under 'User Name'");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Topic1_Path).trim().equalsIgnoreCase("Test"),
				"'Test' is not displays under 'Topic'");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Comment1_Path).trim().equalsIgnoreCase("QA Test"),
				"'QA Test' is not displays under 'Comment' column ");
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Status1_Path).trim().equalsIgnoreCase("Acknowledged"),
				"'Acknowledged' is not displays under 'Status' column ");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPPProg_Attachment1_Path),
				"'File' is not displays under 'Attachment' ");
		Click(Xpath, MSPPProg_Hiddengrey1_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Assert.assertTrue(GetCSS_Backgroundcolor(Xpath, MSPPProg_Attachmentrow1_Path).equalsIgnoreCase("#000000"),
				"the hidden row is not 'Disabled'");
		Click(Xpath, MSPP_DownloadArchivebtn_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Cancelbtn_Path),
				"'Cancel' button is not displays on the 'Progress/Files' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Savebtn_Path),
				"'Save' button is not displays on the 'Progress/Files' tab");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Click(Xpath, MSPP_CostsTab_Path);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, MSPPCost_QuoteCost_Path);
		Click(Xpath, MSPPCost_Billed_Path);
		Type(Xpath, MSPPCost_QuoteCost_Path, "0.1");
		Type(Xpath, MSPPCost_PrintCost_Path, "0.1");
		Type(Xpath, MSPPCost_Fulfillment_Path, "0.1");
		Type(Xpath, MSPPCost_Rush_Path, "0.1");
		Type(Xpath, MSPPCost_VeritasPostage_Path, "0.1");
		Type(Xpath, MSPPCost_ClientPostage_Path, "0.1");
		Click(Xpath, MSPPCost_Addcosticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPCost_Loading_Path);
		Type(Xpath, MSPPCost_AddDescription_Path, "QA Test");
		Get_DropDown(MSPPCost_AddTypearrow_Path, "Assembly");
		Type(Xpath, MSPPCost_AddBilldate_Path, Get_Futuredate("MM/dd/yyyy"));
		Type(Xpath, MSPPCost_AddBillamount_Path, "1.00");
		Click(Xpath, MSPPCost_AddInsert_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPCost_Loading_Path);
		Click(Xpath, MSPPCost_Edit1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPCost_Loading_Path);
		Type(Xpath, MSPPCost_UpdateBillamount_Path, "2.00");
		Click(Xpath, MSPPCost_UpdateInsert_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPCost_Loading_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, MSPPCost_Billamount1_Path).trim().equalsIgnoreCase("$2.000"),
				" the value '$1.000' is not changes to '$2.000' under the 'Bill Amount' column in the 'Table");
		Click(Xpath, MSPPCost_Delete1_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPPCost_Deletepop_Path),
				"'Delete' pop up window is not displays");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPPCost_DeleteOK_Path),
				"'OK' button is not displays on 'Delete' pop up window");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPPCost_DeleteCancel_Path),
				"'Cancel' button is not displays on 'Delete' pop up window");
		Click(Xpath, MSPPCost_DeleteOK_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPCost_Loading_Path);

		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Click(Xpath, MSPP_InventoryTab_Path);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, MSPPInv_Jobtype_Path);

		Assert.assertTrue(Get_Text(Xpath, MSPPInv_Jobtype_Path).trim().equalsIgnoreCase("Print & Assembly"),
				"'Print & Assembly' displays for 'Job Type' field ");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab - Inventory");

		Assert.assertTrue(false, "Unable to select QA_PODTEST");
	}

	@Test(enabled = true, priority = 6)
	public void AON_TC_2_2_10_1_8() throws InterruptedException, IOException {

		// Validate change history updates made

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Click(Xpath, MSPP_HistoryTab_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, MSPPHist_DateTime1_Path).trim().contains(Get_Todaydate("MM/d/yyyy")), "");
		Assert.assertTrue(Get_Text(Xpath, MSPPHist_User1_Path).trim().equalsIgnoreCase(UserFName + " " + UserLName),
				"User first and last name is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Assembly Instructions From")),
				"<Changed Assembly Instructions From '' To 'QA testing Assembly instruction'> displays under 'Details' column");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Quote Needed From")),
				"'Changed Quote Needed From 'False' To 'True'' displays under 'Details' column");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Project Number From")),
				"'Changed Project Number From '' To '456'' displays under 'Details' column");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Project Name From")),
				"'Changed Project Name From '' To 'Test'' displays under 'Details' column");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Recipient Count From")),
				"'Changed Recipient Count From '0' To '10'' displays under 'Details' column");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Department ID From")),
				"'Changed Department ID From '' To '002'' displays under 'Details' column");
		Assert.assertTrue(Element_Is_Displayed(Xpath, History("Changed Activity Number From")),
				"' Changed Activity Number From '' To '0001'' under 'Details' column displays");

	}

	@Test(enabled = true, priority = 7)
	public void AON_TC_2_2_10_1_10() throws InterruptedException, IOException {

		// Verify changes are not saved on clicking Cancel button

		login(UserName, Password);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Type(Xpath, MSPP_Assemblyinst_Path, "QA is retesting");
		Click(Xpath, MSPP_Cancelbtn_Path);
		Wait_ajax();
		Closealltabs();
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertFalse(Get_Attribute(Xpath, MSPP_Assemblyinst_Path, "value").equalsIgnoreCase("QA is retesting"),
				"'QA is retesting' is displaying in the 'Assembly Instructions' box");
	}

	@Test(enabled = true, priority = 8)
	public void AON_TC_2_2_10_1_11() throws InterruptedException, IOException {

		// Validate the cancel functionality

		login(UserName, Password);
		AddNewTicket();
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, MSPPProg_Addcommenticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Type(Xpath, MSPPProg_AddComment_Path, "QA is validating cancel functionality");
		Get_DropDown(MSPPProg_AddStatusarrow_Path, "Cancelled");
		Wait_ajax();
		Click(Xpath, MSPPProg_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Assert.assertTrue(Get_Text(Xpath, MSPP_TicketStatus_Path).trim().equalsIgnoreCase("Cancelled"),
				"'Cancelled' is not displays by 'Status' on top right of the page");
		Click(Xpath, LP_Home_Path);
		Get_DropDown(SPP_Statusarrow_Path, "Cancelled");
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ".//a[text()='" + SplprjTicketNumber + "']"),
				" Cancelled <Ticket # > is not displays on the grid with 'Cancelled' status");

	}

	@Test(enabled = true, priority = 8)
	public void AON_TC_2_2_10_1_13() throws InterruptedException, IOException {

		// Verify Copied Special Projects should not copy the Team Members

		login(UserName, Password);
		AddNewTicket();
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_Copybt1_Path);
		ExplicitWait_Element_Clickable(Xpath, SPP_CopyOK_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Copypopup_Path), "Copy popup is not displayed");
		Click(Xpath, SPP_CopyOK_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Wait_ajax();

		if (SplprjTicketNumber.contains(Get_Text(Xpath, SPP_TicketNo1_Path).trim())) {

			Assert.assertTrue(false, "Ticket is not copied");
		}

		else {

			SplprjTicketNumber = Get_Text(Xpath, SPP_TicketNo1_Path);
			SplprjTicketNumberlist.add(SplprjTicketNumber);
			Reporter.log("Copied Ticket# is " + SplprjTicketNumber);
			Click(Xpath, SPP_ViewEditbtn11_Path);
			Switch_New_Tab();
			Wait_ajax();
			Assert.assertFalse(Element_Is_Displayed(Xpath, Teammemberlist(UserName)),
					"' ' is not displays by 'Team Members' field");
			Click(Xpath, MSPP_HistoryTab_Path);
			Wait_ajax();
			Assert.assertTrue(Get_Text(Xpath, MSPPHist_DateTime1_Path).trim().contains(Get_Todaydate("MM/d/yyyy")), "");
			Assert.assertTrue(Get_Text(Xpath, MSPPHist_User1_Path).trim().equalsIgnoreCase(UserFName + " " + UserLName),
					"");
			Assert.assertTrue(Element_Is_Displayed(Xpath, History("Copied from ticket")),
					"'Copied from ticket (<Ticket #>) is not displays under 'Details' column of history tab");
		}

	}

	@Test(enabled = true, priority = 9)
	public void AON_TC_2_2_10_1_14() throws InterruptedException, IOException {

		// Verify "Closed Tickets" button shows Cancelled and Closed Tickets

		login(UserName, Password);
		AddNewTicket();
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, MSPPProg_Addcommenticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Type(Xpath, MSPPProg_AddComment_Path, "QA is validating cancel functionality");
		Get_DropDown(MSPPProg_AddStatusarrow_Path, "Cancelled");
		Wait_ajax();
		Click(Xpath, MSPPProg_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Wait_ajax();
		Click(Xpath, SPP_ClosedTicketsbtn_Path);
		Thread.sleep(4000);
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Status_Path, "value").equalsIgnoreCase("Closed"),
				" 'Closed' / 'Cancelled' tickets is not displays in 'Status' column");
		Click(Xpath, LP_Home_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(Xpath, SPP_Status_Path, "value").equalsIgnoreCase("All"),
				" 'All' is not displays in 'Status' dropdown");
	}

	@Test(enabled = true, priority = 10)
	public void AON_TC_2_2_10_1_15() throws InterruptedException, IOException {

		// Verify "My Tickets" shows <User LoginName> in "Producer" column
		login(UserName, Password);
		Wait_ajax();
		Click(Xpath, SPP_Myticketsbtn_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, SPP_Producer1_Path).trim().equalsIgnoreCase(UserName),
				"<User Login Name> displays under 'Producer' column");
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, MSPP_Createdby_Path).trim().equalsIgnoreCase(UserName),
				"<Login User Name> is not displays by 'Created By' field");
	}

	@Test(enabled = true, priority = 11)
	public void AON_TC_2_2_10_1_16andAON_TC_2_2_10_1_17() throws InterruptedException, IOException {

		// Verify "Assigned Tickets" are assigned to the specific User

		login(UserName, Password);
		AddNewTicket();
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Teammemberlist(UserName)),
				"'Login User' is not displays by 'Team Members' field");
		Click(Xpath, Teammemberlist(UserName));
		Click(Xpath, MSPP_AONusersDown_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPP_Loading_Path);
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, Teammemberlist(UserName)),
				"<Login User Name> is not removed from 'Team Members' field");
		Click(Xpath, AONuserslist("qaauto1"));
		Click(Xpath, MSPP_AONusersTop_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPP_Loading_Path);
		Wait_ajax();
		Assert.assertTrue(
				Element_Is_Displayed(Xpath,
						".//div[contains(text(),'Team Members')]//following::li[1]//span[text()='qaauto1']"),
				"'qaauto1' is not displays by 'Team Members' field");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Click(locatorType, LP_Logout_Path);
		Closealltabs();
		Type(locatorType, Username_Path, UserName2);
		Type(locatorType, Password_Path, Password2);
		Click(locatorType, Login_btn_Path);
		Click(Xpath, SPP_Assignedticketsbtn_Path);
		Thread.sleep(4500);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, SPP_Producer1_Path).trim().equalsIgnoreCase(UserName),
				"<User Login Name> is not displays under 'Producer' column");
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Assert.assertTrue(Get_Text(Xpath, MSPP_Createdby_Path).trim().equalsIgnoreCase(UserName),
				"<Login User Name> is not displays by 'Created By' field");
		Closealltabs();

		// Verify "Unassigned Tickets" are not assigned to the "User"

		Click(Xpath, Teammemberlist(UserName2));
		Click(Xpath, MSPP_AONusersDown_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPP_Loading_Path);
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, Teammemberlist(UserName2)),
				"<Login User Name> is not removed from 'Team Members' field");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Click(Xpath, LP_Home_Path);
		Wait_ajax();
		Click(Xpath, SPP_Unassignedticbtn_Path);
		Thread.sleep(4500);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				" <Ticket #>  is not displays on the first row on 'Projects' page");
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertFalse(Element_Is_Displayed(Xpath, MSPP_Teammembersempty_Path),
				"'' is not displays in 'Team Members' field");
		Click(locatorType, LP_Logout_Path);
		Closealltabs();
		login(UserName, Password);
		Click(Xpath, SPP_Unassignedticbtn_Path);
		Thread.sleep(4500);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				" <Ticket #>  is not displays on the first row on 'Projects' page");

	}

	@Test(enabled = true, priority = 12)
	public void AON_TC_2_2_10_1_18() throws InterruptedException, IOException {

		// Verify "Quote Needed" shows the tickets with Quote Needed

		login(UserName, Password);
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Click(Xpath, MSPP_Quoteneeded_Path);
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);
		Click(Xpath, SPP_Quoteneededbtn_Path);
		Thread.sleep(4500);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertTrue(Element_Is_selected(Xpath, MSPP_Quoteneeded_Path),
				"Checkbox is not checked off in 'Is Quote Needed' checkbox");

	}

	@Test(enabled = true, priority = 13)
	public void AON_TC_2_2_10_1_19() throws InterruptedException, IOException {

		// Verify "Recent Updates" shows the updates made to the site recently

		login(UserName, Password);
		AddNewTicket();
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Type(Xpath, MSPP_Assemblyinst_Path, "QA Test");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Click(Xpath, LP_Home_Path);
		Wait_ajax();
		Click(Xpath, SPP_Recentupdatesbtn_Path);
		Thread.sleep(4500);
		Assert.assertTrue(Get_Text(Xpath, SPP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"Recent update ticket is not displayed in the first Row");
		Closealltabs();
		Click(Xpath, SPP_ViewEditbtn11_Path);
		Switch_New_Tab();
		Wait_ajax();
		Assert.assertTrue(Get_Attribute(Xpath, MSPP_Assemblyinst_Path, "value").equalsIgnoreCase("QA Test"),
				"'QA Test' is not displays in 'Assembly Instructions' field");

	}

	@Test(enabled = true, priority = 14)
	public void AON_TC_2_2_10_1_21() throws InterruptedException, IOException {

		// Paging Functionality on Progress/Files tab

		login(UserName, Password);
		AddNewTicket();
		Wait_ajax();
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();

		for (int Prog_comment_count = 1; Prog_comment_count <= 10; Prog_comment_count++) {
			Click(Xpath, MSPPProg_Addcommenticon_Path);
			// Wait_ajax();
			ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
			Type(Xpath, MSPPProg_AddComment_Path, "QA is Testing");
			// Select_li_Dropdown(MSPPProg_AddStatusarrow_Path, "Comment");
			Click(Xpath, MSPPProg_InsertCommentbtn_Path);
			ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
			// Wait_ajax();
		}

		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted under 'Add Comment / Progress / Files'");
		Click(Xpath, MSPPProg_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"Page '2 is not highlighted under 'Add Comment / Progress / Files'");
		Click(Xpath, MSPPProg_Previuospage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted under 'Add Comment / Progress / Files -Firstpagebtn");
		Click(Xpath, MSPPProg_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"Page '2 is not highlighted under 'Add Comment / Progress / Files'");
		Click(Xpath, MSPPProg_Firstpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted under 'Add Comment / Progress / Files -Firstpagebtn");
		Assert.assertTrue(Get_Attribute(Xpath, MSPPProg_Pagesize_Path, "value").equalsIgnoreCase("5"),
				"'5' is is not displaying in 'Page size' ");
		Type(Xpath, MSPPProg_Pagesize_Path, "8");
		Click(Xpath, MSPPProg_Changebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		System.out.println(Get_Text(Xpath, MSPPProg_Itemof_Path).trim());
		Assert.assertTrue(Get_Text(Xpath, MSPPProg_Itemof_Path).trim().contains("Item 1 to 8 of 10"),
				"'Item 1 to 8 of 10' text  is not displaying on right bottom of the page");

	}

	@Test(enabled = true, priority = 15)
	public void AON_TC_2_2_10_1_22() throws InterruptedException, IOException, AWTException {

		// Multiple File Upload in Progress/File tab in Special Projects
		login(UserName, Password);
		Wait_ajax();
		Click(Xpath, SPP_AddTicketicon_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, MSPP_Ticketno_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displays by 'Ticket Number' field");
		Type(Xpath, MSPP_Jobtitle_Path, "QA Test");
		Get_DropDown(MSPP_Jobtypearrow_Path, "Print & Assembly");
		Type(Xpath, MSPP_Duedate_Path, Get_Futuredate("MM/dd/yyyy"));
		Select_DropDown_VisibleText(Xpath, MSPP_Disposition_Path, "Recycle");
		Type(Xpath, MSPP_ClientID_Path, "003");
		Type(Xpath, MSPP_ClientName_Path, "QA Test1");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");

		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		Click(Xpath, MSPPProg_Addcommenticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Type(Xpath, MSPPProg_AddTopic_Path, "Test");
		Type(Xpath, MSPPProg_AddComment_Path, "QA is Testing");
		Assert.assertTrue(Get_Attribute(Xpath, MSPPProg_AddStatus_Path, "value").equalsIgnoreCase("Comment"),
				"'Comment' is not displays in 'Status' dropdown");
		Click(Xpath, MSPPProg_Addattachment_Path);
		MultiFileupload(Multiuploaddirectory, 7);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile1));
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile2));
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile3));
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile4));
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile5));
		ExplicitWait_Element_Visible(Xpath, Multifiles(Multifile6));
		// Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Multifiles(Multifile1)),
				"'Remove' link displays by file " + Multifile1);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Multifiles(Multifile2)),
				"'Remove' link displays by file " + Multifile2);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Multifiles(Multifile3)),
				"'Remove' link displays by file " + Multifile3);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Multifiles(Multifile4)),
				"'Remove' link displays by file " + Multifile4);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Multifiles(Multifile5)),
				"'Remove' link displays by file " + Multifile5);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Multifiles(Multifile6)),
				"'Remove' link displays by file " + Multifile6);
		Click(Xpath, MSPPProg_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPPProg_Attachment1_Path),
				"'File' button is not displays under 'Attachment' column");
		Click(Xpath, LP_Home_Path);
		Wait_ajax();
		Type(Xpath, SPP_Tickeno_Path, SplprjTicketNumber);
		Click(Xpath, SPP_Tickenofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, SPP_Fileicon1_Path),
				"'File Attachment' symbol is not displays fot the 'Ticket #' ");

	}

	@Test(enabled = true, priority = 16)
	public void AON_TC_2_2_10_1_23() throws InterruptedException, IOException {

		// Verify in Special project "R&P" 'Job Type' displays the Additional
		// Message in 'Disposition'

		login(UserName, Password);
		Wait_ajax();
		Click(Xpath, SPP_AddTicketicon_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, MSPP_Ticketno_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displays by 'Ticket Number' field");
		Type(Xpath, MSPP_Jobtitle_Path, "QA Test");
		Get_DropDown(MSPP_Jobtypearrow_Path, "R&P Assembly Only");
		Wait_ajax();
		Assert.assertTrue(Get_DropDown(Xpath, MSPP_Frequency_Path).equalsIgnoreCase("One Time"),
				"'One Time' is not displays in 'Frequency' dropdown");
		Type(Xpath, MSPP_Assemblyinst_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, MSPP_Disposition_Path, "Return To Bond Street");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Dispositionerrormessage_Path),
				"'* Your Disposition selection will take place 2 weeks from the final mail/ship date.' is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Dispositionerrormessage2_Path),
				"* Disposition of all left over materials for R&P projects will take place 8 weeks from the original mail/ship date (except for quarterly mailing envelopes)' is not displayed");
		Type(Xpath, MSPP_ClientID_Path, "Aon Test");
		Type(Xpath, MSPP_ClientName_Path, "Aon");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Dispositionerrormessage_Path),
				"'* Your Disposition selection will take place 2 weeks from the final mail/ship date.' is not displayed after created");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Dispositionerrormessage2_Path),
				"* Disposition of all left over materials for R&P projects will take place 8 weeks from the original mail/ship date (except for quarterly mailing envelopes)' is not displayed after created");
		Get_DropDown(MSPP_Jobtypearrow_Path, "R&P Print Only");
		Wait_ajax();
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Dispositionerrormessage_Path),
				"'* Your Disposition selection will take place 2 weeks from the final mail/ship date.' is not displayed for R&P Print Only");
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Dispositionerrormessage2_Path),
				"* Disposition of all left over materials for R&P projects will take place 8 weeks from the original mail/ship date (except for quarterly mailing envelopes)' is not displayed for R&P Print Onlys");

	}

	// Add Special Project

	@Test(enabled = true, priority = 17)
	public void AON_TC_2_2_10_1_1_1() throws InterruptedException, IOException {

		// Validate upon clicking "Add Special Projects" takes to General
		// Special projects

		login(UserName, Password);
		Hover(Xpath, LP_Admin_Path);
		Hover(Xpath, LP_SpecialProjects_Path);
		Click(Xpath, LP_AddSpecialProjects_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, MSPP_GeneralTab_Path), "'General tab is not displays");

	}

	@Test(enabled = true, priority = 18)
	public void AON_TC_2_2_10_1_2_1() throws InterruptedException, IOException {

		// Validate upon clicking "Add Special Projects" takes to General
		// Special projects

		login(UserName, Password);
		Hover(Xpath, LP_Admin_Path);
		Hover(Xpath, LP_SpecialProjects_Path);
		Click(Xpath, LP_AddSpecialProjects_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, MSPP_Ticketno_Path).trim().equalsIgnoreCase("New"),
				"'New' is not displays by 'Ticket Number' field");
		softAssert.assertTrue(Get_Attribute(Xpath, MSPP_Jobtitle_Path, "value").isEmpty(),
				" ' ' is not displays in 'Job Title' field");
		softAssert.assertTrue(Get_Text(Xpath, MSPP_Jobtitleasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Job Title' field as Required field ");
		softAssert.assertTrue(Get_Attribute(Xpath, MSPP_Jobtype_Path, "value").trim().equalsIgnoreCase("- Select -"),
				"'- Select -' is not displays in 'Job Type'");
		softAssert.assertTrue(Get_Text(Xpath, MSPP_Jobtypeasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Job Type' field as Required field ");
		softAssert.assertFalse(
				Get_Attribute(Xpath, MSPP_Duedate_Path, "value").equalsIgnoreCase(Get_Todaydate("MM/dd/yyyy")),
				"<Future Date - MM/DD/YYYY > is not displays in 'Due Date' field");
		softAssert.assertTrue(Get_DropDown(Xpath, MSPP_Disposition_Path).equalsIgnoreCase("- Select Method -"),
				"'- Select Method -' is not displays in 'Disposition' field");
		Assert.assertTrue(Get_Text(Xpath, MSPP_Dispositionasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Disposition' field as Required field ");
		softAssert.assertTrue(Get_Attribute(Xpath, MSPP_ClientID_Path, "value").isEmpty(),
				" ' ' is not displays in 'Client ID' field");
		softAssert.assertTrue(Get_Text(Xpath, MSPP_ClientIDasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Client ID' field as Required field ");
		softAssert.assertTrue(Get_Attribute(Xpath, MSPP_ClientName_Path, "value").isEmpty(),
				" ' ' is not displays in 'Client Name' field");
		softAssert.assertTrue(Get_Text(Xpath, MSPP_ClientNameasterisk_Path).trim().equalsIgnoreCase("*"),
				"'*' is not displays in 'Client name' field as Required field ");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Jobtitle_Path),
				"Job Title is required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Jobtype_Path),
				" Job Type is required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Disposition_Path),
				"Disposition is required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_ClientName_Path),
				"Client Name is required' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Clientid_Path),
				"Client Id is required' is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Errorfield_Jobtitle_Path),
				" 'Job Title is required' is not displays by 'Job Title' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Jobtype_Path),
				"'Job Type is required' is not displays by 'Job Type' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Errorfield_ClientId_Path),
				"'Client ID is required' is not displays by 'Client ID' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Errorfield_Clientname_Path),
				"'Client Name is required' is not displays by 'Client Name' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Errorfield_Disposition_Path),
				"'Disposition is required' is not displays by 'Disposition' field");

		Get_DropDown(MSPP_Jobtypearrow_Path, "Assembly Only");
		Type(Xpath, MSPP_ClientID_Path, "AON");
		Type(Xpath, MSPP_ClientName_Path, "AON");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed - 2");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Jobtitle_Path),
				"Job Title is required is not displayed - 2");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Disposition_Path),
				"Disposition is required is not displayed - 2");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Jobtitle_Path),
				"Job Title is required is not displayed - 2");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Disposition_Path),
				"Disposition is required is not displayed - 2");

		Type(Xpath, MSPP_Jobtitle_Path, "QA Test");
		Click(Xpath, MSPP_Savebtn_Path);
		Wait_ajax();

		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Title_Path),
				"'You must enter a value in the following fields: is not displayed - 3");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Disposition_Path),
				"Disposition is required is not displayed - 3");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_Error_Disposition_Path),
				"Disposition is required is not displayed - 3");

		Select_DropDown_VisibleText(Xpath, MSPP_Disposition_Path, "Return To Bond Street");
		Click(Xpath, MSPP_ProgramTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_ErrorCreate__Path),
				"'Create the Special Project, before proceeding.' is not displayed - 1");

		Click(Xpath, MSPP_CostsTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_ErrorCreate__Path),
				"'Create the Special Project, before proceeding.' is not displayed - 2");

		Click(Xpath, MSPP_InventoryTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_ErrorCreate__Path),
				"'Create the Special Project, before proceeding.' is not displayed - 3");

		Click(Xpath, MSPP_HistoryTab_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, MSPP_ErrorCreate__Path),
				"'Create the Special Project, before proceeding.' is not displayed - 4");

		softAssert.assertAll();

	}

	@BeforeMethod
	public void BeforeMethod() throws IOException, InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod(enabled = true)
	public void AfterMethod() {

		Click(locatorType, LP_Logout_Path);
		Closealltabs();
	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {

		login(UserName, Password);

		for (String Splticknum : SplprjTicketNumberlist) {

			Type(Xpath, SPP_Tickeno_Path, Splticknum);
			Click(Xpath, SPP_Tickenofilter_Path);
			ExplicitWait_Element_Not_Visible(Xpath, SPP_Loading_Path);
			Click(Xpath, SPP_ViewEditbtn11_Path);
			Switch_New_Tab();
			Wait_ajax();
			if (Get_Text(Xpath, MSPP_TicketStatus_Path).contains("Closed")
					|| Get_Text(Xpath, MSPP_TicketStatus_Path).contains("Cancelled")) {

			} else {
				Click(Xpath, MSPP_ProgramTab_Path);
				Wait_ajax();
				Click(Xpath, MSPPProg_Addcommenticon_Path);
				Wait_ajax();
				ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
				Type(Xpath, MSPPProg_AddComment_Path, "QA Test");
				Get_DropDown(MSPPProg_AddStatusarrow_Path, "Closed");
				Click(Xpath, MSPPProg_InsertCommentbtn_Path);
				ExplicitWait_Element_Not_Visible(Xpath, MSPPProg_Laading_Path);
				Wait_ajax();
				Click(Xpath, MSPP_Savebtn_Path);
				Wait_ajax();
			}

			Closealltabs();
			NavigateMenu(LP_Admin_Path, LP_SpecialProjects_Path);

		}

	}

}
