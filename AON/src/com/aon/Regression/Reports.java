package com.aon.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import com.aon.Base.BaseTest;

public class Reports extends BaseTest {

	public String Availablereportscount(int Row) {

		return ".//h1[contains(text(),'Available Reports')]//following::tbody[3]//tr[" + Row + "]";

	}

	@Test(enabled = true, priority = 1)
	public void AON_TC_2_2_11_1() throws InterruptedException, IOException {

		// Validate Reports Page displays section headers "Available Reports
		// Sections, Report Scheduler, My Reports Subscription

		NavigateMenu(LP_Admin_Path, LP_Reports_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Reports_Available_Path),
				"'Available Reports' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Reports_Scheduler_Path),
				"'Report Scheduler' is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Reports_Subscriptions_Path),
				" 'My Report Subscriptions' is not displays");
		softAssert.assertAll();
	}

	@Test(enabled = true, priority = 2)
	public void AON_TC_2_2_11_2() throws InterruptedException, IOException {

		// Validate User is able to view report upon clicking "View Report"

		NavigateMenu(LP_Admin_Path, LP_Reports_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Reports_Available_Path),
				"'Available Reports' is not displays");
		Click(locatorType, Reports_8WeekUsageByAliasReportview_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Reports_Loading_Path);
		Switch_To_Iframe("ContentWindow");
		ExplicitWait_Element_Clickable(Xpath, Reports_Viewreportbtn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Reports_Title_Path), "<Report Name> page is not opens");
		softAssert.assertTrue(Get_Attribute(Xpath, Reports_Aonlogo_Path, "title").equalsIgnoreCase("AON"),
				" 'AON' is not displays on left side of the page");
		softAssert.assertTrue(Get_DropDown(Xpath, Reports_LiteratureType_Path).equalsIgnoreCase("- All Types -"),
				" ' - All Types - ' is not displays in 'Literature Type' dropdown");
		softAssert.assertTrue(Get_DropDown(Xpath, Reports_Channel_Path).equalsIgnoreCase("- Select a Value -"),
				"'- Select a Value-' displays in 'Channel' dropdown");
		softAssert.assertTrue(Get_DropDown(Xpath, Reports_CostCenter_Path).equalsIgnoreCase("- Select a Value -"),
				"'- Select a Value-' is not displays in 'Cost Center' dropdown");
		softAssert.assertTrue(Element_Is_selected(Xpath, Reports_Nullck_Path),
				"'Null' checkbox is not checked off in 'Alias Number' ");
		Click(locatorType, Reports_ProductCateg_Path);
		softAssert.assertTrue(Element_Is_selected(Xpath, Reports_ProductCategSelectall_Path),
				"'Corporate, Total Compensation, Total Rewards' is not displays in 'Product Categories' dropdown");
		Click(locatorType, Reports_ProductCateg_Path);
		Switch_To_Default();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Reports_Close_Path),
				"'X' is not displays on the right top corner of the page");
		Click(locatorType, Reports_Close_Path);

		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 3)
	public void AON_TC_2_2_11_3() throws InterruptedException, IOException {

		// Validate "Available Reports Section" displays only 10 Report Name by
		// default

		NavigateMenu(LP_Admin_Path, LP_Reports_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Reports_Available_Path), "'Available Reports' is not displays");
		Assert.assertTrue(Get_Attribute(Xpath, Reports_Pagesize_Path, "value").equalsIgnoreCase("10"),
				"'10' is not displays in 'Page Size' dropdown");

		for (int Rowcount = 1; Rowcount <= 10; Rowcount++) {

			if (Element_Is_Displayed(Xpath, Availablereportscount(Rowcount))) {

			}

			else {
				Assert.assertTrue(false, "<10 reports> are not display under 'Report Name'");
			}

		}

	}

	@Test(enabled = true, priority = 4)
	public void AON_TC_2_2_11_4() throws InterruptedException, IOException {

		// Validate upon selecting any of the Report Name "Report Schedule" is
		// enabled

		NavigateMenu(LP_Admin_Path, LP_Reports_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Reports_Available_Path), "'Available Reports' is not displays");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Reports_Scheduler_Path), "'Report Scheduler' is not displays");
		Click(locatorType, Reports_8WeekUsageReport_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Reports_8WeekUsageReportScheduler_Path),
				"<8WeekUsageReport> Scheduler is not displays");

	}

	@Test(enabled = true, priority = 5)
	public void AON_TC_2_2_11_7() throws InterruptedException, IOException {

		// Ensure paging functionality on the available reports table is working
		// fine.

		NavigateMenu(LP_Admin_Path, LP_Reports_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Reports_Available_Path), "'Available Reports' is not displays");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted at the bottom of the page");
		Click(locatorType, Reports_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")),
				"Page '2' is not highlighted at the bottom of the page");
		Click(locatorType, Reports_Previouspage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page '1' is not highlighted at the bottom of the page");
		Click(locatorType, Reports_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Reports_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")),
				"Page '3' is not highlighted at the bottom of the page");

	}

	@Test(enabled = true, priority = 6)
	public void AON_TC_2_2_11_8() throws InterruptedException, IOException {

		// Ensure Page Size selection is working fine

		NavigateMenu(LP_Admin_Path, LP_Reports_Path);
		Assert.assertTrue(Get_Attribute(Xpath, Reports_Pagesize_Path, "value").equalsIgnoreCase("10"),
				"'10' is not displays in 'Page Size' dropdown");
		Select_lidropdown(Reports_Pagesizearrow_Path, "20");
		ExplicitWait_Element_Not_Visible(Xpath, Reports_Loading_Path);
		for (int Rowcount = 1; Rowcount <= 20; Rowcount++) {

			if (Element_Is_Displayed(Xpath, Availablereportscount(Rowcount))) {

			}

			else {
				Assert.assertTrue(false, "<20 reports> are not display under 'Report Name'");
			}

		}

	}

	@BeforeClass
	public void BeforeClass() throws InterruptedException, IOException {

		login(UserName, Password);

	}

	@BeforeMethod
	public void BeforeMethod() {
		softAssert = new SoftAssert();
	}

	@AfterMethod(enabled = true)
	public void AfterMethod() throws InterruptedException {

		Click(locatorType, LP_Home_Path);
		Wait_ajax();
	}

	@AfterClass
	public void AfterClass() throws InterruptedException {

	}

}