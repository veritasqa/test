package com.aon.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.aon.Base.BaseTest;

public class Logout extends BaseTest {

	@Test(priority = 1, enabled = true)
	public void AON_TC_2_6_1() throws IOException, InterruptedException {

		// Validates display of "Logout" on all pages of the site (Header)

		Type(locatorType, Username_Path, UserName);
		Type(locatorType, Password_Path, Password);
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, LP_Logout_Path), "Logout is not displayed in Home page");
		Hover(locatorType, LP_Admin_Path);
		Click(locatorType, LP_Manageinventory_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, LP_Logout_Path),
				"Logout is not displayed in Manage inventory page");

	}

	@Test(priority = 2, enabled = true)
	public void AON_TC_2_6_2() throws IOException, InterruptedException {

		// Validate "Logout" function and redirection back to the "Login page"

		Click(locatorType, LP_Home_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, LP_Logout_Path), "Logout is not displayed in Home page");
		Click(locatorType, LP_Logout_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Username_Path), "Username field is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Password_Path), "Password field is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Login_btn_Path), "Login Button is not displayed");

	}

}
