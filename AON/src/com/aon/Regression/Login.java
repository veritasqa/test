package com.aon.Regression;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.aon.Base.BaseTest;

public class Login extends BaseTest {

	//public static HashMap<String, WebDriver> driverObjMap = new HashMap<String, WebDriver>();

	@Test(priority = 1, enabled = true)
	public void AON_TC_1_0_1_1() throws IOException, InterruptedException {

		// Verify Active user login

		login(UserName, Password);
		Click(locatorType, LP_Logout_Path);
		Reporter.log("Logged in Successfully");
		Click(locatorType, LP_Logout_Path);
		ExplicitWait_Element_Clickable(locatorType, Login_btn_Path);

	}

	@Test(priority = 2, enabled = true)
	public void AON_TC_1_0_2_1() throws IOException, InterruptedException {

		// Verify the error message is displayed upon "Invalid user name and
		// password" combination

		Type(locatorType, Username_Path, "QA");
		Type(locatorType, Password_Path, "test");
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, InvalidUNPWD_Path),
				"'Invalid user name or password, please try again' is not displayed");
		Type(locatorType, Username_Path, "Test");
		Type(locatorType, Password_Path, Password);
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, InvalidUNPWD_Path),
				"'Invalid user name or password, please try again' is not displayed");

	}

	@Test(priority = 3, enabled = true)
	public void AON_TC_1_0_2_2() throws IOException, InterruptedException {

		// Verify the error message when user login without username

		Type(locatorType, Username_Path, "");
		Type(locatorType, Password_Path, Password);
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, EmptyUsernamemsg_Path),
				"'User Name is required' is not displayed");
		Reporter.log("Not Logged in Successfully");
		Assert.assertTrue(false, "Test fail");

	}

	@Test(priority = 4, enabled = true)
	public void AON_TC_1_0_2_3() throws IOException, InterruptedException {

		// Verify the error message when user login without password

		Type(locatorType, Username_Path, UserName);
		Type(locatorType, Password_Path, "");
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, EmptyPasswordmsg_Path),
				"'Password is required' is not displayed");

	}

	@Test(priority = 5, enabled = true)
	public void AON_TC_1_0_2_4() throws IOException, InterruptedException {

		// Verify error message when user name and password is not provided for
		// login

		Type(locatorType, Username_Path, "");
		Type(locatorType, Password_Path, "");
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, EmptyUsernamemsg_Path),
				"'User Name is required' is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, EmptyPasswordmsg_Path),
				"'Password is required' is not displayed");

	}


}
