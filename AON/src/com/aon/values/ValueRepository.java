package com.aon.values;

import java.util.ArrayList;
import java.util.List;

public class ValueRepository {

	public static String Browser = "chrome";
	public static final String  locatorType = "xpath";
	public static final String Xpath = "xpath";
	public static final String ID = "id";
	public static final String Tag = "tag";
	public static final String Name = "name";
	public static final String CSSselector = "cssSelector";
	public static final String LinkText = "linkText";
	public static final String PartialLinkText = "partialLinkText";
	public static final String Classname = "classname";

	// File path for Chrome Driver
	public static String Browserpath = System.getProperty("user.dir")+"\\src\\com\\aon\\Driver\\chromedriver.exe";
	// File Path GroupA and GroupA1 ExcelList Files
	public static String Excel_File_Path = "C:\\Users\\A.xlsx";

	//public static final String URL = "https://staging.veritas-solutions.net/GlobalAtlantic/login.aspx";
	
	public static final String URL= "https://staging.veritas-solutions.net/AON/login.aspx";

	
	//public static final String Inventory_URL = "https://staging.veritas-solutions.net/inventory/login.aspx";

	public static final String UserName = "qaauto";
	public static final String Password = "qaauto";
	public static final String UserName2 = "qaauto1";
	public static final String Password2 = "qaauto1";
	public static final String EmailId = "ver.qaauto@rrd.com";
	public static final String UserId = "QA.Auto";
	
	public static final String UserFName = "qa";
	public static final String UserLName = "automation";
	public static final String Useremail = "ver.qaauto@rrd.com";
	
	public static final String BasicUserName = "qaautobasic";
	public static final String BasicPassword = "qaautobasic";
		
	
	public static final String Address1 = "913 Commerce ct";
	public static final String City = "Buffalo Grove";
	public static final String State = "IL";
	public static final String Zipcode = "60089";
	public static final String Shipper = "UPS Ground";
	

	public String Parent_Window_ID = "";
	public String Dropdown_Clear = "- Please Select -";

	public static String OutPut_File_Path = "C:\\Users\\rr249046\\Desktop\\Veritas\\Protective\\PartsViewability"; // Output
	
	public static final String Qty = "1";

	public static String OrderNumber = "";
	public static String TicketNumber = "";
	
	public static final String Multifile1 = "Aon- Excel File_1.xlsx";
	public static final String Multifile2 = "Aon_Sample.docx";
	public static final String Multifile3 = "Download File.docx";
	public static final String Multifile4 = "Quality icon.png";
	public static final String Multifile5 = "Test panda file.pdf";
	public static final String Multifile6 = "Visio file.vsdx";
	
	public static String SplprjTicketNumber = "";
	public static String SplprjTicketNumber2 = "";
	public static List<String> SplprjTicketNumberlist = new ArrayList<String>() ;
//	ordernum.add(OrderNumber);

	
	//Spl Projects
	public static final String SPPErrorTilte = "You must enter a value in the following fields:";
	public static final String SPPErrorJobTilte = "Job Title Is Required";
	public static final String SPPErrorJobType = "Job Type Is Required";
	public static final String SPPErrorCC = "Cost Center Is Required";
	
	
	//  Filter options
	

	
	
	
}
