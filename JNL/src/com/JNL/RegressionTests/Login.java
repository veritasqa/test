package com.JNL.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.JNL.Base.BaseTestJNL;

public class Login extends BaseTestJNL {

	@Test(priority = 1, enabled = true)
	public void JNL_Mob_TC_1_0_1_4() {

		// Validate Admin user with JNL Admin Role and no cost center selected
		// is not able to see the WMC Mobile link

		Type(ID, JNL_Login_UserName_ID, qamobilenocc);
		Type(ID, JNL_Login_Password_ID, qamobilenocc);
		Click(ID, JNL_Login_LoginBtn_ID);
		Assert.assertFalse(Element_Is_Displayed(Xpath, JNL_LP_WMCmobile_path),
				"WMC Mobile\" link is displayed under 'Favorites' section displayed on the left navigation pane");
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}
}
