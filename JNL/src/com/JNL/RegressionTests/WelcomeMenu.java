package com.JNL.RegressionTests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.JNL.Base.BaseTestJNL;

public class WelcomeMenu extends BaseTestJNL {

	@Test(priority = 1, enabled = true)
	public void JNL_Mob_TC_2_1_1_1() throws InterruptedException {

		// Verify the options displayed in the Welcome Menu.

		Click(ID, JNL_LP_WMCmobile_path);
		Wait_ajax();
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}
}