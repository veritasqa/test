package com.JNL.OR;

public class OrderConfirmationPage extends ManageUsersPage {

	// Order Confirmation- OC

	public static final String JNL_OC_OrderNumber_Path = "//*[@id=\"ctl00_ctl00_cphContent_cphSection_lblOrderNumber\"]";

	public static final String JNL_OC_Printfile1_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10_ctl04_lblProofName']";

	public static final String JNL_OC_Printfile2_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10_ctl10_hlProof']";

	public static final String JNL_OC_Status_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblStatus";
	public static final String JNL_OC_HoldForComplete_ID = "cphContent_cphSection_lblHoldForComplete";
	public static final String JNL_OC_OrderDate_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblCreateDate";
	public static final String JNL_OC_CostCenter_ID = "cphContent_cphSection_lblBilledToCostCenter";
	public static final String JNL_OC_ShippedBy_ID = "ctl00_ctl00_cphContent_cphOrderInformation_lblShippedBy";
	public static final String JNL_OC_TotalChargebackCost = "cphContent_cphSection_lblTotalChargeBackCost";
	public static final String JNL_OC_Ticketno_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[3]";
	public static final String JNL_OC_Status_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[4]/span";

	public static final String JNL_OC_Stockno_Path = "//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[1]";
	public static final String JNL_OC_Description_Path = "//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[2]";
	public static final String JNL_OC_Qty_Path = "//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00_ctl06_Detail10__0:0_0']/td[4]";

	public static final String JNL_OC_Shipbydate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[5]";
	public static final String JNL_OC_Deliverdate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphOrderDetails_rgdOrderItems_ctl00__0']/td[6]";
	public static final String JNL_OC_Cancelorder_Path = ".//a[text()='Cancel Order']";

	public static final String JNL_OC_Cancelreq_Btn_Path = ".//a[text()='Submit Cancel Request']";

	public static final String JNL_OC_Copy_Btn_Path = ".//a[text()='Copy']";
	public static final String JNL_OC_OK_Btn_ID = "cphContent_cphPageFooter_btnOk";
	public static final String JNL_OC_Home_Btn_Path = ".//a[text()='Home/New Order']";

	// Shopping Cart Page

	public static final String JNL_SCP_Next_Btn_ID = ".//a[text()='Next �']";
	public static final String JNL_SCP_Deliverydate_Path = "//*[@id=\"ctl00_cphContent_rdtepkrDeliveryDate_popupButton\"]";
	public static final String JNL_SCP_DeliverydateCal_Path = "//*[@id=\"ctl00_cphContent_rdtepkrDeliveryDate_popupButton\"]";
	public static final String JNL_SCP_DeliverydateCalMonth_Path = "//*[@id=\"ctl00_cphContent_rdtepkrDeliveryDate_calendar_Title\"]";
	public static final String JNL_SCP_Checkout_path = ".//a[text()='Checkout']";
	public static final String JNL_SCP_Qty_Path = ".//input[contains(@id,'txtQuantity') and @type='text']";

	public static final String JNL_SCP_Comments_ID = "cphContent_txtShippingInstructions";
	public static final String JNL_SCP_CreateOrderTemplate_Btn_Path = ".//a[text()='Create Order Template']";
	public static final String JNL_SCP_ClearCart_Btn_ID = "cphContent_btnClearCart";
	public static final String JNL_SCP_SubmitOrder_Btn_Path = ".//a[contains(@id,'Checkout') and text()='Submit Order']";

	// Create Order Template Pop up
	public static final String JNL_SCP_COT_Path = ".//div[@class='Box']";
	public static final String JNL_SCP_COT_Title_Path = ".//div[contains(text(),'Create Order Template')]";
	public static final String JNL_SCP_COT_Name_Path = ".//input[contains(@id,'txtOrderTemplateName') and @type='text']";
	public static final String JNL_SCP_COT_Description_Path = ".//textarea[contains(@id,'txtOrderTemplateDescription')]";
	public static final String JNL_SCP_COT_Createbtn_Path = ".//a[text()='Create']";
	public static final String JNL_SCP_COT_Cancelbtn_Path = ".//a[text()='Cancel']";

}
