package com.Hartford.Value;

import java.util.ArrayList;
import java.util.List;

import org.testng.asserts.SoftAssert;

public class ValueRepository {
	public static String Browser = "chrome";
	public static SoftAssert softAssert;
	public static List<String> OrderNumberlist = new ArrayList<String>();

	// Credentials
	public static final String Username = "qaauto";
	public static final String Password = "qaauto";

	public static String OrderNumber = "";

	public static String Browserpath = System.getProperty("user.dir")
			+ "\\src\\com\\Hartford\\Driver\\chromedriver.exe";
	// URLs
	public static final String URL = "https://staging.veritas-solutions.net/hartfordpef/login.aspx";
	public static final String Production_URL = "https://www.veritas-solutions.net/hartfordpef/login.aspx";

	public static final String Inventory_URL = "https://www.veritas-solutions.net/Inventory/login.aspx";

}
