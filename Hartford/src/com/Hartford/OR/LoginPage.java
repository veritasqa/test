package com.Hartford.OR;

import com.Hartford.Value.ValueRepository;

public class LoginPage extends ValueRepository{

	public static final String A_Login_UserName_ID = "txtUsername";
	public static final String A_Login_Password_ID = "txtPassword";
	public static final String A_Login_LoginBtn_ID = "btnLogin";
	
	public static final String A_LogoutBtn_Path = "//a[@href='/HartfordPEF/Logout.aspx']";
	
}
