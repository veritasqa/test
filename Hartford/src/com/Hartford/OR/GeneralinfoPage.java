package com.Hartford.OR;

public class GeneralinfoPage extends InventoryPage {

	public static final String newOrderPath = "//*[@id='divHeaderContent']/table/tbody/tr[3]/td/table/tbody/tr/td/a[3]";

	public static final String Generic_Path = "//input[@value='Generic']";
	public static final String OrderNo_Path = ".//strong[contains(text(),'General Info: Editing Order#')]";

	public static final String Personalized_Path = "//input[@value='Personalized']";

	public static final String Combined_Checkbox_ID = "ctl00_cphContent_ckGG18_C";
	public static final String Enrollmentperiod_Checkbox_ID = "ckGG18_S";
	public static final String Newlyeligiblefamilystatuschange_Checkbox_ID = "ctl00_cphContent_ckGG18_N";

	public static final String Bhsonlyneeded_DD_ID = "ctl00_cphContent_ddlGG22";

	public static final String Casename_TextBox_ID = "ctl00_cphContent_txtCaseName";

	public static final String CaseIdentifierDetails_TextBox_ID = "ctl00_cphContent_txtCaseID";

	public static final String EffectiveDate_TextBox_ID = "ctl00_cphContent_txtEnrollDate";

	public static final String CalanderCloseBtn_Path = "//a[text()='Close']";

	public static final String Dateforemployeeagecost_Id = "ctl00_cphContent_txtGG9";

	public static final String Ageband_DD_Id = "ctl00_cphContent_ddlGG23";

	public static final String EnrollmentPeriodFromDate_ID = "txtFromDate";

	public static final String EnrollmentPeriodToDate_ID = "txtToDate";

	public static final String PEF_CheckBox_ID = "ctl00_cphContent_ckPEFlogo";

	public static final String GenericForms_Checkbox_ID = "ctl00_cphContent_ckGenericLogo";

	public static final String BHS_CheckBox_ID = "ctl00_cphContent_ckBHSBFSLogo";

	public static final String WelcomeLetter_CheckBox_ID = "ctl00_cphContent_ckWelcomeLetterLogo";

	public static final String Envelopes_Checkbox_ID = "ctl00_cphContent_ckEnvelopesLogo";

	public static final String BlackWhite_CheckBox_ID = "ctl00_cphContent_ckIsBWLogo";

	public static final String Numberofdeductions_DD_ID = "ctl00_cphContent_lbCoverages";

	public static final String ContactInformation_DD_ID = "ctl00_cphContent_ddlGG12";

	public static final String IsthereaMyTomorrowsite_DD_ID = "ctl00_cphContent_ddlGG14";

	public static final String Employerliketheforms_DD_ID = "ctl00_cphContent_ddlEmpRef";

	public static final String Employerliketheforms_TextBox_ID = "ctl00_cphContent_txtEmpRef";

	public static final String DomesticPartnerCoverage_DD_ID = "ctl00_cphContent_ddlDomPartnerCov";

	public static final String ERISAcase_DD_ID = "ctl00_cphContent_ddlGG17";

	public static final String SitusState_DD_ID = "ctl00_cphContent_ddlGG10";

	public static final String Segment_DD_ID = "ctl00_cphContent_ddlSegment";

	public static final String CaseSalesOffice_DD_ID = "ctl00_cphContent_ddlG16_CaseSalesOffice";

	public static final String Instructionsforemployees_TextBox_ID = "ctl00_cphContent_txtFrmReturn";

	public static final String employeespaytheirpremium_DD_ID = "ctl00_cphContent_ddlGG21PayDeduct";

	public static final String StandardWelcomeLetter_DD_ID = "ctl00_cphContent_ddlWelcomeLtr";

	public static final String descriptionfooteronBHSandLimitationsExclusionsdocument_DD_ID = "ctl00_cphContent_ddlVA37";

	public static final String ChooseFile_ID = "ctl00_cphContent_RecipientList";

	public static final String Earnings_CheckBox_ID = "ctl00_cphContent_ckGG19_1";
	public static final String EmployeeID_Checkox_ID = "ctl00_cphContent_ckGG19_2";
	public static final String StreetAdress_Checkox_ID = "ctl00_cphContent_ckGG19_3";
	public static final String City_Checkox_ID = "ctl00_cphContent_ckGG19_4";
	public static final String State_Checkox_ID = "ctl00_cphContent_ckGG19_5";
	public static final String ZipCode_Checkox_ID = "ctl00_cphContent_ckGG19_6";
	public static final String EmailAddress_Checkox_ID = "ctl00_cphContent_ckGG19_7";
	public static final String Location_Checkox_ID = "ctl00_cphContent_ckGG19_8";
	public static final String DepartMent_Checkox_ID = "ctl00_cphContent_ckGG19_9";
	public static final String GroupPolicyNumber_Checkox_ID = "ctl00_cphContent_ckGG19_10";
	public static final String Class_Checkox_ID = "ctl00_cphContent_ckGG19_11";
	public static final String PositionJobtitle_Checkox_ID = "ctl00_cphContent_ckGG19_12";
	public static final String NumberofHrsWorked_Checkox_ID = "ctl00_cphContent_ckGG19_13";
	public static final String Gender_Checkox_ID = "ctl00_cphContent_ckGG19_14";
	public static final String MarriedPartnered_Checkox_ID = "ctl00_cphContent_ckGG19_15";
	public static final String CurrentCoverageamounts_Checkox_ID = "ctl00_cphContent_ckGG19_27";

	public static final String SaveNextStep_ID = "ctl00_cphContent_btnSave";
}
