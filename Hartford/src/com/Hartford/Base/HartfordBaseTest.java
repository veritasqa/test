package com.Hartford.Base;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.Hartford.Methods.HartfordCommonMethods;

public class HartfordBaseTest extends HartfordCommonMethods{
	@BeforeSuite
	public void openbrowser() throws IOException, InterruptedException {

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		Implicit_Wait();
	}

	@AfterSuite(enabled = true)
	public void Aftersuite() throws InterruptedException {

	/*	if (OrderNumberlist.isEmpty() == false) {

			Cancelallorders();
		} 
        Close_Browser();
		Quit_Browser();*/

	}
	

	public void login(String UserName, String Password) throws IOException, InterruptedException {

		if (Element_Is_Present(ID, A_Login_UserName_ID)) {

			Type(ID, A_Login_UserName_ID, UserName);
			Type(ID, A_Login_Password_ID, Password);
			Click(ID, A_Login_LoginBtn_ID);
			Assert.assertTrue(Element_Is_Present(Xpath, A_LogoutBtn_Path));
			Implicit_Wait();
		}

		else {
			logout();
			Type(ID, A_Login_UserName_ID, UserName);
			Type(ID, A_Login_Password_ID, Password);
			Click(ID, A_Login_LoginBtn_ID);
			Assert.assertTrue(Element_Is_Present(Xpath, A_LogoutBtn_Path));
			Implicit_Wait();
		}

	}
	
	public void logout() throws IOException, InterruptedException {


		Click(Xpath, A_LogoutBtn_Path);
		ExplicitWait_Element_Clickable(ID, A_Login_LoginBtn_ID);

	}
	
	
	public void Cancelallorders() {

		// Open_Browser(Browser,Browserpath);
		Get_URL(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		for (String Onum : OrderNumberlist) {

			ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
			Click(Xpath, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
			Type(Xpath, Inventory_Ordernumber_Path, Onum);
			Click(Xpath, Inventory_Search_Btn_Path);
			Click(Xpath, Inventory_Edit_Btn_Path);

			if (Get_Attribute(Xpath, Inventory_Order_Status, "Value").equalsIgnoreCase("NEW")) {

				Click(Xpath, Inventory_Cancelorder_Btn_Path);
				Accept_Alert();

				Assert.assertTrue(Get_Attribute(Xpath, Inventory_Order_Status, "Value").equalsIgnoreCase("Cancelled"),
						"Order is not cancelled is not displayed");

			}
			Click(Xpath, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		}

		Click(Xpath, Inventory_logout_btn_Path);
		ExplicitWait_Element_Visible(Xpath, Inventory_Username_Path);

	}
}
