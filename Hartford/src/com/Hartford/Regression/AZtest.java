package com.Hartford.Regression;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Hartford.Base.HartfordBaseTest;

public class AZtest extends HartfordBaseTest {

	@Test(priority = 1, enabled = true)
	public void AZ1() throws InterruptedException, AWTException {
		Click(Xpath, newOrderPath);
		System.out.println(Get_Text(Xpath, OrderNo_Path));

		Click(Xpath, Personalized_Path);

		Click(ID, Enrollmentperiod_Checkbox_ID);
		Click(ID, Newlyeligiblefamilystatuschange_Checkbox_ID);
		Select_DropDown_VisibleText(ID, Bhsonlyneeded_DD_ID, "No");
		Type(ID, Casename_TextBox_ID, "QA_Test");
		Type(ID, CaseIdentifierDetails_TextBox_ID, "AZ1");
		Type(ID, EffectiveDate_TextBox_ID, Get_Pastdate("dd/MM/YYYY"));
		Click(Xpath, CalanderCloseBtn_Path);

		Type(ID, Dateforemployeeagecost_Id, Get_Todaydate("dd/MM/YYYY"));
		Click(Xpath, CalanderCloseBtn_Path);

		Select_DropDown_VisibleText(ID, Ageband_DD_Id, "Anniversary on or following birthday");

		Type(ID, EnrollmentPeriodFromDate_ID, Get_Pastdate("dd/MM/YYYY"));
		Click(Xpath, CalanderCloseBtn_Path);

		Type(ID, EnrollmentPeriodToDate_ID, Get_Todaydate("dd/MM/YYYY"));
		Click(Xpath, CalanderCloseBtn_Path);

		Select_DropDown_VisibleText(ID, Numberofdeductions_DD_ID, "24");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, IsthereaMyTomorrowsite_DD_ID, "No");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, Employerliketheforms_DD_ID, "Employee");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, DomesticPartnerCoverage_DD_ID, "No");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, ERISAcase_DD_ID, "No");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, SitusState_DD_ID, "AZ");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, Segment_DD_ID, "Priority Accounts");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, CaseSalesOffice_DD_ID, "Atlanta");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, StandardWelcomeLetter_DD_ID, "Yes");
		Wait_ajax();
		Select_DropDown_VisibleText(ID, descriptionfooteronBHSandLimitationsExclusionsdocument_DD_ID, "Yes");
		Wait_ajax();

		Click(ID, ChooseFile_ID);
		Fileupload(System.getProperty("user.dir") + "\\src\\com\\Hartford\\Utils\\New Good Census_ NO TO Test.xlsx");
		Thread.sleep(10000);
		Click(ID, SaveNextStep_ID);

		/*
				OrderNumber = Get_Text(Xpath, OrderNo_Path).substring.(Get_Text(Xpath, OrderNo_Path).indexOf("# "),le).trim();
				OrderNumberlist.add(OrderNumber);
				Reporter.log("Order number is " + OrderNumber);
				System.out.println("Order number is " + OrderNumber);*/

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(Username, Password);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
