package com.parts.viewability;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;

import org.openqa.selenium.safari.SafariDriver;
//import org.openqa.selenium.support.ui.Select;

public class CommonMethods extends ObjectRepository {

	public WebDriver driver;
	public String result = "Pass";
	public String reason;
	public Alert alert = null;
	public static String OriginalWindow = "";

	public WebDriver Open_Browser(String browser, String path) {

		try {
			if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", path);
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", path);

				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("opera")) {
				driver = new OperaDriver();
			} else if (browser.equalsIgnoreCase("safari")) {
				driver = new SafariDriver();
			} else if (browser.equalsIgnoreCase("internetexplorer")) {
				driver = new InternetExplorerDriver();
			} else if (browser.equalsIgnoreCase("htmlUnitdriver")) {
				driver = new HtmlUnitDriver();
			} else {
				reason = "Invalid Browser Input. Check Browser name";
				result = "Fail";

			}

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();

		}
		return driver;

	}

	public void Open_Url_Window_Max(String url, WebDriver _driver)
	// public void openUrlandMax(String url)

	{
		driver = _driver;

		try {
			driver.navigate().to(url);
			driver.manage().window().maximize();
		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
		// finally
		// {
		// capturescreenshot("OpenURL");

		// }
	}

	public void Implicit_Wait(WebDriver _driver)

	{
		driver = _driver;
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
	}

	public void Close_Browser(WebDriver _driver) {
		driver = _driver;

		try {

			driver.close();
		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
	}

	public void Thread_Sleep(int Sleep_time) {

		try {
			Thread.sleep(Sleep_time);
		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}

	}

	public void Quit_Browser(WebDriver _driver)
	// public void quitbrowser()
	{
		driver = _driver;

		try {
			driver.quit();
		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}

	}

	public By locatorValue(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		By by;
		switch (locatorType) {
		case "id":
			by = By.id(value);
			break;
		case "name":
			by = By.name(value);
			break;
		case "xpath":
			by = By.xpath(value);
			break;
		case "cssSelector":
			by = By.cssSelector(value);
			break;
		case "linkText":
			by = By.linkText(value);
			break;
		case "partialLinkText":
			by = By.partialLinkText(value);
			break;
		case "classname":
			by = By.className(value);
			break;
		default:
			by = null;
			break;
		}
		return by;
	}

	public void Click_On_Element(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			driver.findElement(locator).click();

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
	}

	public String li_value(String option) {

		return ".//li[text()='" + option + "']";

	}

	public void Clear_Type_Charecters(String locatorType, String value, String parameter, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			driver.findElement(locator).clear();
			driver.findElement(locator).sendKeys(parameter);

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}

	}

	public String Get_Attribute(String locatorType, String value, String parameter, WebDriver _driver) {
		driver = _driver;

		try {

			By locator;
			locator = locatorValue(locatorType, value, driver);
			// driver.findElement(locator).clear();
			attrib = driver.findElement(locator).getAttribute(parameter).trim();

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
		return attrib;
	}

	public void Double_Click(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			Actions act = new Actions(driver);
			WebElement Admin = driver.findElement(locator);
			act.doubleClick(Admin).build().perform();

			// driver.findElement(locator).clear();
			// driver.findElement(locator).sendKeys(parameter);

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}

	}

	public void Switch_New_Tab() {

		Window_Handle = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		driver.switchTo().window(ID.get(1));

	}

	public void Closealltabs() {

		OriginalWindow = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		for (String handle : ID) {
			// System.out.println(handle);

			if (!handle.equalsIgnoreCase(OriginalWindow)) {

				driver.switchTo().window(handle);
				driver.close();

			}

		}
		driver.switchTo().window(OriginalWindow);
	}

	public void Hover_Over_Element(String locatorType, String value, WebDriver _driver) {
		// Thread_Sleep(4000);
		// Implicit_Wait(driver);
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			Actions act = new Actions(driver);

			// act.wait(300);
			// first part
			WebElement Admin = driver.findElement(locator);
			act.clickAndHold(Admin).build().perform();

			// act.pause(5).build().perform();

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
	}

	public void Switch_New_Tab(WebDriver _driver) {
		driver = _driver;

		try {
			// new
			// Actions(driver).sendKeys(driver.findElement(By.tagName("html")),
			// Keys.CONTROL).sendKeys
			// (driver.findElement(By.tagName("html")),Keys.NUMPAD2).build().perform();
			// Get the current window handle
			Window_Handle = driver.getWindowHandle();

			// Get the list of window handles
			// List<String> tabs = new ArrayList (driver.getWindowHandles());
			List<String> ID = new ArrayList<String>(driver.getWindowHandles());
			// System.out.println(ID.size());
			// Use the list of window handles to switch between windows
			driver.switchTo().window(ID.get(1));

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
	}

	public void Switch_Old_Tab(WebDriver _driver) {
		driver = _driver;

		try {
			Close_Browser(driver);
			// new
			// Actions(driver).sendKeys(driver.findElement(By.tagName("html")),
			// Keys.CONTROL).sendKeys(driver.findElement(By.tagName("html")),Keys.NUMPAD1).build().perform();

			driver.switchTo().window(Window_Handle);

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
		}
	}

	public Boolean Is_Element_Present(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			driver.findElement(locator);
			return true;
		} catch (Exception e) {
			reason = e.getMessage();

			return false;
		}
	}

	public String Get_Text(String locatorType, String value, WebDriver _driver) {
		driver = _driver;

		try {
			By locator;
			locator = locatorValue(locatorType, value, driver);
			String Text = driver.findElement(locator).getText();
			return Text;

		} catch (Exception e) {
			result = "Fail";
			reason = e.getMessage();
			return reason;
		}
	}

	public List<String> getexcel(String filepath) throws IOException {
		List<String> excellist = new ArrayList<String>();

		FileInputStream fis = new FileInputStream(filepath);

		// @SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Sheet1");

		// System.out.println(sheet.getLastRowNum());

		XSSFRow row = sheet.getRow(1);
		row.getLastCellNum();

		// System.out.println(row.getLastCellNum());

		// for (int colnum =0; colnum<row.getLastCellNum();colnum++ ){

		for (int rownum = 1; rownum <= sheet.getLastRowNum(); rownum++) {

			XSSFRow row2 = sheet.getRow(rownum);
			excellist.add(row2.getCell(0).toString());
			// System.out.println(row2.getCell(0).toString());

		}
		// }
		workbook.close();
		fis.close();
		return excellist;
	}

	public List<String> getexcelcol2(String filepath) throws IOException {
		List<String> excellist = new ArrayList<String>();

		FileInputStream fis = new FileInputStream(filepath);

		// @SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Sheet1");

		// System.out.println(sheet.getLastRowNum());

		XSSFRow row = sheet.getRow(1);
		row.getLastCellNum();

		// System.out.println(row.getLastCellNum());

		// for (int colnum =0; colnum<row.getLastCellNum();colnum++ ){

		for (int rownum = 1; rownum <= sheet.getLastRowNum(); rownum++) {

			XSSFRow row2 = sheet.getRow(rownum);
			excellist.add(row2.getCell(1).toString());
			// System.out.println(row2.getCell(1).toString());
		}
		// }
		workbook.close();
		fis.close();
		return excellist;
	}

	public void Write_Excel() throws IOException {
		/*
		 * SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss"); Date
		 * curDate = new Date(); String strDate = sdf.format(curDate); String
		 * TestFile = "Test" + "_"+strDate+".xlsx"; File FC = new
		 * File(TestFile); FC.createNewFile(); //String fileName= "Test.xlsx";
		 * //Create an object of File class to open xlsx file
		 */
		FileInputStream fis = new FileInputStream(Excel_File_Path);

		// @SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Sheet1");

		// Get the current count of rows in excel file

		// int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();

		// Get the first row from the sheet

		/* Row row = sheet.getRow(0); */

		// Create a new row and append it at last of sheet

		// Create a loop over the cell of newly created Row
		int i = 1;
		for (int j = 0; j < Part.size(); j++) {

			// Fill data in row
			Row newRow = sheet.getRow(i);
			/*
			 * Cell cell = newRow.createCell(0);
			 * cell.setCellValue(Part.get(j).toString());
			 */
			Cell cel = newRow.createCell(2);
			cel.setCellValue(Action.get(j).toString());
			i++;
		}

		// Close input stream
		workbook.close();
		fis.close();

		// Create an object of FileOutputStream class to create write data in
		// excel file

		FileOutputStream outputStream = new FileOutputStream(Excel_File_Path);

		// write data in the excel file

		workbook.write(outputStream);

		// close output stream

		outputStream.close();

	}

	public void Writeexcel() throws IOException {

		File file = new File(Excel_File_Path);

		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheet("Sheet1");
		sheet.setColumnWidth(2, 15000);

		CellStyle style = workbook.createCellStyle();
		style.setBorderRight(XSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		style.setBorderTop(XSSFCellStyle.BORDER_THIN);
		style.setBorderBottom(XSSFCellStyle.BORDER_THIN);

		for (int k = 0; k < Results.size(); k++) {
			Row newRow = sheet.getRow(k + 1);

			Cell cel = newRow.createCell(2);
			cel.setCellValue(Results.get(k));
			cel.setCellStyle(style);

		}

		FileOutputStream outputStream = new FileOutputStream(
				Excel_OFile_Path + Get_Todaydate("MMddyyyy_HH_mm_ss") + ".xlsx");

		workbook.write(outputStream);
		workbook.close();
		outputStream.flush();
		outputStream.close();
	}

	public String Get_Todaydate(String dateformat) {
		// driver = _driver;

		try {
			// Create object of SimpleDateFormat class and decide the format
			DateFormat dateFormat = new SimpleDateFormat(dateformat);

			// get current date time with Date()
			Date date = new Date();

			// Now format the date
			return dateFormat.format(date);

		} catch (Exception e) {
			// result = "Fail";
			// reason = e.getMessage();
			return e.getMessage();
		}
	}
}
