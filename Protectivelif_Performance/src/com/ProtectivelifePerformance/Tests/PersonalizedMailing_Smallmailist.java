package com.ProtectivelifePerformance.Tests;

import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ProtectivelifePerformance.Base.BaseTest;

public class PersonalizedMailing_Smallmailist extends BaseTest {

	@Test(priority = 1)
	public void MailListupload() throws InterruptedException {

		Click(locatorType, Personalized_mailing_path, driver);
		ExplicitWait_Element_Clickable(locatorType, MLU_Uploadbtn_Path, driver);
		Click(locatorType, MLU_Uploadtemplate_Path, driver);
		Wait_ajax();
		Fileupload(Smallmailinglist, driver);
		Click(locatorType, MLU_Uploadbtn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MLU_Ok_Path, driver);
		Click(locatorType, MLU_Ok_Path, driver);

	}

	@Test(priority = 2)
	public void MaterialSearch() throws InterruptedException {

		Type(locatorType, Searchbymaterial_Path, Part_PABD_743410_10_17, driver);
		Click(locatorType, Searchbymaterial_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Searchloadingpanel_Path, driver);

	}

	@Test(priority = 3)
	public void Addtodrip() throws InterruptedException {

		Click(locatorType, Add_To_Drip_btn_Path, driver);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);

	}

	@Test(priority = 4)
	public void Proofing() throws InterruptedException {

		Click(locatorType, Minicart_Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SCO_Savebtn_Path, driver);
		Type(locatorType, SCO_Name_Path, "QA Auto", driver);
		Type(locatorType, SCO_Date_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_Time_Path, Get_Todaydate("HH:mm"), driver);
		Type(locatorType, SCO_Location_Path, "QA Test", driver);
		Type(locatorType, SCO_Address_Path, Address1, driver);
		Type(locatorType, SCO_City_Path, City, driver);
		Select_DropDown_VisibleText(locatorType, SCO_State_Path, "IL", driver);
		Type(locatorType, SCO_Zip_Path, Zipcode, driver);
		Type(locatorType, SCO_RSVPDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Type(locatorType, SCO_RSVPName_Path, SalesdeskUserName, driver);
		Type(locatorType, SCO_RSVPPhone_Path, "222.222.2222", driver);
		Type(locatorType, SCO_RSVPEmail_Path, EmailId, driver);
		Type(locatorType, SCO_FirmName_Path, "Veritas", driver);
		Click(locatorType, SCO_Savebtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		Wait_ajax();
		Click(locatorType, SC_Proofbtn_Path, driver);
		Switch_New_Tab(driver);
		Switch_Old_Tab(driver);

	}

	@Test(priority = 5)
	public void Checkout() throws InterruptedException {

		Type(locatorType, SC_Shippingcommentsfield_Path, "QA is Testing", driver);
		Click(locatorType, SC_Checkout_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, OrderConf_Cancelreqbtn_Path, driver);
		OrderNumber = Get_Text(locatorType, OrderNumber_Path, driver);
		System.out.println("Order Number placed in method  is PL_TC_2_7_1_b_3'" + OrderNumber + "'");
		Orders.add(OrderNumber);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method PL_TC_2_7_1_b_3 is '" + OrderNumber + "'");
	}

	@BeforeTest
	public void BeforeTest() throws InterruptedException {

		Open_Url_Window_Max(URL, driver);
		Implicit_Wait(driver);
		Type(locatorType, Username_Path, UserName, driver);
		Type(locatorType, Password_Path, Password, driver);
		Click(locatorType, Login_btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
		Clearcart();
		Wait_ajax();

	}

	@AfterTest
	public void AfterTest() {
		Click(locatorType, Logout_Path, driver);

	}

}
