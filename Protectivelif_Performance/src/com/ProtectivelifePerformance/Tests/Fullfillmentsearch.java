package com.ProtectivelifePerformance.Tests;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ProtectivelifePerformance.Base.BaseTest;

public class Fullfillmentsearch extends BaseTest {

	@Test(priority = 1)
	public void FullfillmentSearch() {

		Type(locatorType, Fullfilment_Search_Path, Part_1PVAII_KIT, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

	}

	@Test(priority = 2)
	public void Contactsearch() {

		ContactSearch(Jackson_Firstname, Jackson_Lastname,"Chase Investment");
		ExplicitWait_Element_Visible(locatorType, Part_Name_Path, driver);
	}

	@Test(priority = 3)
	public void AddtoCart() {

		Click(locatorType, Add_To_Cart_btn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Minicart_Loadingpanel_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Checkoutbtn_Path, driver);

	}

	@Test(priority = 4)
	public void Checkout() throws InterruptedException {

		Click(locatorType, Checkoutbtn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SC_Updatebtn_Path, driver);
		PlaceanOrder("Checkout");

	}

	@BeforeTest
	public void BeforeTest() throws InterruptedException {

		Open_Url_Window_Max(PreProdURL, driver);
		Implicit_Wait(driver);
		Type(locatorType, Username_Path, UserName, driver);
		Type(locatorType, Password_Path, Password, driver);
		Click(locatorType, Login_btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
		Clearcart();
		Wait_ajax();

	}

	@AfterTest
	public void AfterTest() {
		Click(locatorType, Logout_Path, driver);

	}

	@AfterSuite
	public void AfterSuite() {
		Cancelallorders();
	}

}
