package com.ProtectivelifePerformance.Tests;

import java.sql.Date;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.ProtectivelifePerformance.Base.BaseTest;

public class OrderLookupWidget extends BaseTest {
	
	public void Orderwidgetsearch(){
		
		
	}
	
	@BeforeTest
	public void BeforeTest() throws InterruptedException {

		Open_Url_Window_Max(URL, driver);
		Implicit_Wait(driver);
		Type(locatorType, Username_Path, UserName, driver);
		Type(locatorType, Password_Path, Password, driver);
		Click(locatorType, Login_btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
		Clearcart();
		Wait_ajax();
		Hover(locatorType, Add_Widget_Path, driver);
		Wait_ajax();
		Widgetname = Get_Text(locatorType, OrderLookup_Path, driver);
		Click(locatorType, OrderLookup_Path, driver);
		Click(locatorType, Wid_Xpath(Widgetname,OrderLookup_Searchbylink_Path), driver);
		ExplicitWait_Element_Clickable(locatorType, Ordersearch_Searchbtn_Path, driver);
		Click(locatorType, Ordersearch_StartDate, driver);
		Wait_ajax();
		Datepickercustom(Ordersearch_StartDateMonth,Ordersearch_StartDateOk,"May","1","2017");
		Click(locatorType, Ordersearch_Enddate, driver);
		Wait_ajax();
		Datepickercustom(Ordersearch_Enddatemonth,Ordersearch_StartDateOk,"Dec","1","2017");
		Wait_ajax();
		Type(locatorType, Ordersearch_Stocknumber, Part_1PIB_KIT, driver);
		Click(locatorType, Ordersearch_Searchbtn_Path, driver);
		ExplicitWait_Element_Not_Visible(locatorType, Ordersearch_Loadingpanel_Path, driver);
		
		
	}

	@AfterTest
	public void AfterTest() {
		Click(locatorType, Logout_Path, driver);

	}

}
