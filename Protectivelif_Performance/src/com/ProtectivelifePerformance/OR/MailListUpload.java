package com.ProtectivelifePerformance.OR;

public class MailListUpload extends ManageAnnouncements {

	public static final String MLU_Upload_Title_Path = ".//h1[text()='Mail List Upload']";
	public static final String MLU_SelectTemplate_Path = ".//*[@id='ctl00_cphContent_ddlMailList_Input']";
	public static final String MLU_SelectTemplate_icon_Path = ".//*[@id='ctl00_cphContent_ddlMailList_Arrow']";
	public static final String MLU_Downloadtemplatelink_Path = ".//*[@id='ctl00_cphContent_lblTemplate']";
	public static final String MLU_Uploadtemplate_Path = ".//*[@id='ctl00_cphContent_fupMailListListContainer']";
	public static final String MLU_Uploadtemplateinput_Path = ".//*[@id='ctl00_cphContent_fupMailListTextBox0']";
	public static final String MLU_Replaceck_Path = ".//*[@id='aspnetForm']";
	public static final String MLU_Uploadbtn_Path = ".//a[text()='Upload Mail List']";
	public static final String MLU_Content_Title_Path = ".//h1[text()='Mail List Contents']";
	public static final String MLU_Nocontactsfound_Path = ".//span[text()='No contacts found, please add above.']";
	public static final String MLU_Ok_Path = ".//*[@id='ctl00_cphContent_btnBack']";
	public static final String Personilizedmailinglist = System.getProperty("user.dir")
			+ "\\src\\com\\Protectivelife\\Util\\MAILLIST204.xls";
	public static final String BulkOrdermailinglist = System.getProperty("user.dir")
			+ "\\src\\com\\Protectivelife\\Util\\MAILLIST2.xls";
	public static final String MLU_SelectMailListTemplate_Path = ".//*[@id='ctl00_cphContent_ddlMailList_Arrow']";
	public static final String MLU_Downloadtemplate_Path = ".//*[@id='ctl00_cphContent_lblTemplate']";
	public static final String MLU_Chooseasavedmaillist_Path = ".//*[@id='ctl00_cphContent_ddlMailListManage_Arrow']";
	public static final String MLU_Managelistsbtn_Path = ".//a[text()='Manage Lists']";

	// Manage Mail Lists page
	public static final String MML_Addmaillistbtn_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdMailLists_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String MML_MailListName_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdMailLists_ctl00_ctl02_ctl02_rtxtName']";
	public static final String MML_Choosefilepath_Path = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdMailLists_ctl00_ctl02_ctl02_FileUpload1']";
	public static final String MML_Insertbtn_Path = ".//a[text()='Insert']";
	public static final String MML_Cancekbtn_Path = ".//a[text()='Cancel']";
	public static final String MML_Personilizedmailinglist_Nofirm_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Protectivelife\\Util\\Maillist_NO FIRM_Automation.xlsx";
	public static final String MML_Personilizedmailinglist_Nofirm205_Path = System.getProperty("user.dir")
			+ "\\src\\com\\Protectivelife\\Util\\Maillist_NO FIRM205_Automation.xlsx";
	public static final String MML_Multimaillist_SwapnaList = System.getProperty("user.dir")
			+ "\\src\\com\\Protectivelife\\Util\\Multimaillist_Swapna.xls";
	public static final String MML_Deletebtn_Path = ".//a[text()='Delete']";

	public static final String Maillist_Nofirm_Name = "Mail list_NO FIRM_Automation";
	public static final String MML_NOFIRM_AutomationDeletebtn_Path = ".//td[text()='" + Maillist_Nofirm_Name
			+ "']//following::a[2]";

	public static final String Maillist_Nofirm205_Name = "Maillist_NO FIRM205_Automation";
	public static final String MML_NOFIRM205_AutomationDeletebtn_Path = ".//td[text()='" + Maillist_Nofirm205_Name
			+ "']//following::a[2]";

	public static final String Maillist2 = "Maillist2_Automation";
	public static final String MML_Maillist2_AutomationDeletebtn_Path = ".//td[text()='" + Maillist2
			+ "']//following::a[2]";

	public static final String MultiMaillist_Swapna = "MultiMaillist_Swapna_Automation";
	public static final String MML_MultiMaillist_Swapna_AutomationDeletebtn_Path = ".//td[text()='"
			+ MultiMaillist_Swapna + "']//following::a[2]";

	//
	public static final String Smallmailinglist = System.getProperty("user.dir")
			+ "\\src\\com\\ProtectivelifePerformance\\Utilities\\cetera-list-sorted-for-ashley-and-noah-06_08_2016.xlsx";
	public static final String Hugemailinglist = System.getProperty("user.dir")
			+ "src\\com\\ProtectivelifePerformance\\Utilities\\PIB Rate Postcard Mailing List 10.2017.xls";

}