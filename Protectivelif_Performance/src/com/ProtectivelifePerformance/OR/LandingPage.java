package com.ProtectivelifePerformance.OR;

public class LandingPage extends Login {
	
	// Top Section 
	
	public static final String Home_btn_Path =".//span[text()='Home']";
	public static final String Materials_btn_Path =".//span[text()='Materials']";
	public static final String Catalog_btn_Path =".//span[text()='Catalog']";
	public static final String Support_Path =".//span[text()='Support']";
	public static final String HomepageLogo_Path ="//*[@id='ctl00_imgLogo']";
	public static final String Homepage_Username_Path ="//*[@id='ctl00_lnkUserName']";

	public static final String Admin_btn_Path ="//span[contains(.,'Admin')]";
	public static final String Veritas_btn_Path =".//span[text()='Veritas']";

	public static final String Manage_Dropdowns_btn_Path =".//span[text()='Veritas']";
	public static final String Reset_Phrases_btn_Path =".//span[text()='Reset Phrases & Settings']";
	public static final String Kit_Quality_Control_btn_Path =".//span[text()='Kit Quality Control']";
	public static final String Clean_User_Dashboards_btn_Path =".//span[text()='Clean User Dashboards']";
	public static final String Inventory_Options_btn_Path =".//span[text()='Inventory Options']";
	public static final String Option_Types_btn_Path =".//span[text()='Option Types']";
	public static final String Contact_Search_btn_Path =".//span[text()='Contact Search']";
	
	public static final String LP_Username_Path= ".//*[@id='ctl00_lnkUserName']";

	public static final String Manage_Inventory_btn_Path =".//span[text()='Manage Inventory']";

	public static final String Manage_Announcements_btn_Path =".//span[text()='Manage Announcements']";
	public static final String Manage_Categories_btn_Path =".//span[text()='Manage Categories']";
	public static final String Manage_Cost_Centers_btn_Path =".//span[text()='Manage Cost Centers']";
	public static final String Manage_Orders_btn_Path =".//span[text()='Manage Orders']";
	public static final String Manage_User_Kits_btn_Path ="//span[contains(.,'Manage User Kits')]";
	public static final String Manage_Phrases_btn_Path =".//span[text()='Manage Phrases']";
	public static final String Manage_Roles_btn_Path =".//span[text()='Manage Roles']";
	public static final String Manage_Users_btn_Path =".//span[text()='Manage Users']";
	public static final String Bulk_Inventory_Update_btn_Path =".//span[text()='Bulk Inventory Update']";
	public static final String Manage_Preflight_btn_Path =".//span[text()='Manage Preflight']";
	public static final String Special_Projects_btn_Path =".//span[text()='Special Projects']";
	public static final String Add_Special_Projects_btn_Path =".//span[text()='Add Special Projects']";

	public static final String Reports_btn_Path =".//span[text()='Reports']";
	public static final String Add_Widget_Path =".//span[text()='Add Shortcuts']";

	
	public static final String Logout_Path = ".//a[text()='Logout']";
	public static final String Clear_Cart_Path = ".//span[text()='Clear Cart']";
	public static final String Checkout_Path = ".//*[@id='aShoppingCart']/span";
	public static final String Announcement_view_btn_Path = ".//a[text()='View All Announcements']";

	// Fullfillment search
	public static final String Fullfilment_Search_Path = ".//*[@id='ctl00_txtQuickSearch']";
	public static final String Fullfilment_Search_Btn_Path = ".//*[@id='ctl00_btnQuickSearch']";
	
	
	//Mailings
	public static final String Personalized_mailing_path = ".//*[@id='ctl00_cphContent_lnkMarketingCampaign']";
	public static final String Bulkorder_path = ".//*[@id='ctl00_cphContent_lnkBulkOrder']";
	public static final String MultiRecipientOrder_path = ".//a[text()='Start a Multi-Recipient Order']";
	
	//Widget
	public static final String Add_Shortcut_Path = 	"//*[@id='widgetList']/span";
	public static final String Product_Search_Path = "//*[@id='ctl00_cphContent_btnAddWidgetProductSearch']";
	
}
