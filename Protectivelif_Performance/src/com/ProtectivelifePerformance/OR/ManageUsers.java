package com.ProtectivelifePerformance.OR;

public class ManageUsers extends OrderManagement {

	public static final String MU_Username_Fld_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_UserName']";
	public static final String MU_Username_Filter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_UserName']";
	public static final String MU_Firstname_Fld_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_FirstName']";
	public static final String MU_Firstname_Filter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_FirstName']";
	public static final String MU_Lastname_Fld_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_LastName']";
	public static final String MU_Lastname_Filter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_LastName']";
	public static final String MU_Email_Fld_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_Email']";
	public static final String MU_Email_Filter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_Email']";
	public static final String MU_Active_Checkbox_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterCheckBox_IsActive']";
	public static final String MU_Active_Filter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_IsActive']";
	public static final String MU_AddUser_btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl03_ctl01_AddNewRecordButton']";
	
	
	public static final String MU_Result_Username_Path= ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[2]";
	public static final String MU_Result_Edit_btn_Path= ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl04_EditButton']";
	
	public static final String MU_Result_Address1_fld_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl05_rtxtAddress1']";
	public static final String MU_Result_Address2_fld_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl05_rtxtAddress2']";

	public static final String MU_Insert_Update_btn_Path ="//a[@class='buttonAction']";
	

	
	// Add user
	
	public static final String MU_Username_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditUserName']";
	public static final String MU_Password_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditPassword']";
	public static final String MU_Firstname_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditFirstName']";
	public static final String MU_Lastname_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditLastName']";
	public static final String MU_Email_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditEmail']";
	public static final String MU_Address1_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtAddress1']";
	public static final String MU_Address2_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtAddress2']";
	public static final String MU_City_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtCity']";
	public static final String MU_State_Dropdown_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_ddlState']";
	public static final String MU_Zipcode_Txt_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtZipCode']";
	public static final String MU_CostCenter_Dropdown_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_ddlCostCenter']";
	public static final String MU_UserTheme_Dropdown_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_ddlUserTheme']";
	public static final String MU_SubAccount_DropDown_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_ddlUserSubAccount']";
	public static final String MU_Active_Btn_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_cbxEditIsActive']";
	public static final String MU_Cancel_Btn_Path =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_btnCancel']";





}
