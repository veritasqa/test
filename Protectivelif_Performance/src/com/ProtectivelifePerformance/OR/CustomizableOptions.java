package com.ProtectivelifePerformance.OR;

public class CustomizableOptions extends Inventory{
	
	public static final String SCO_Title_Path = ".//*[@id='divContent clearfix']/div[2]/h1";
	public static final String SCO_Name_Path = "//input[contains(@id,'01. Name')]";
	public static final String SCO_Date_Path = "//input[contains(@id,'02. Date')]";
	public static final String SCO_Time_Path = "//input[contains(@id,'03. Time')]";
	public static final String SCO_Location_Path = "//input[contains(@id,'04. Location')]";
	public static final String SCO_Address_Path = "//input[contains(@id,'05. Address')]";
	public static final String SCO_City_Path = "//input[contains(@id,'06. City')]";
	public static final String SCO_State_Path = "//select[contains(@id,'07. State')]";
	public static final String SCO_Zip_Path = "//input[contains(@id,'08. Zip')]";
	public static final String SCO_RSVPDate_Path = "//input[contains(@id,'09. RSVP Date')]";
	public static final String SCO_RSVPName_Path = "//input[contains(@id,'10. RSVP Name')]";
	public static final String SCO_RSVPPhone_Path = "//input[@class='PHONEDOTFORMAT']";
	public static final String SCO_RSVPEmail_Path = "//input[contains(@id,'12. RSVP Email')]";
	public static final String SCO_Browselogo_Path = "//input[@type='file']";
	public static final String SCO_Field1_Path = "//input[contains(@id,'14. FIRM SPECIFIC DISCLOSURE FIELD 1')]";
	public static final String SCO_Field2_Path = "//input[contains(@id,'15. FIRM SPECIFIC DISCLOSURE FIELD 2')]";
	public static final String SCO_Field3_Path = "//input[contains(@id,'16. FIRM SPECIFIC DISCLOSURE FIELD 3')]";
	public static final String SCO_FirmName_Path = "//input[contains(@id,'17. Firm Name')]";
	public static final String SCO_Savebtn_Path = "//input[@value='Save']";
	
}
