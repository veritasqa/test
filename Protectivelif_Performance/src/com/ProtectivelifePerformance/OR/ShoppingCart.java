package com.ProtectivelifePerformance.OR;

public class ShoppingCart extends SpecialProjects {

	public static final String Order_On_Behalf_Of_Path = ".//*[@id='ctl00_cphContent_ddlOrderMethods']";

	public static final String SC_PartImage_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[1]/img";
	public static final String SC_PartName_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']";
	public static final String SC_PartDesc_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblDescription']";
	public static final String SC_Copypartmessage_Path = ".//*[@id='ctl00_cphContent_lblCommandBar']/div";
	public static final String SC_Itemremovedmessage_Path = ".//*[@id='ctl00_cphContent_lblCommandBar']/div";
	public static final String SC_Noitemmessage_Path = ".//*[@id='ctl00_cphContent_divNotOrderableItems']/span";
	public static final String SC_Createtemplatemessage_Path = ".//*[@id='ctl00_cphContent_lblCommandBar']/div";
	public static final String SC_ChargeBackCost_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[3]/span/div[1]";
	public static final String SC_Qty1_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rntxtQuantity']";
	public static final String SC_Updatebtn_Path = ".//a[text()='Update']";
	public static final String SC_Removebtn_Path = ".//a[text()='Remove']";
	public static final String SC_Proofbtn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[4]/a[5]";
	public static final String SC_Proofbtn2_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__1']/td[4]/a[5]";
	public static final String SC_ProofedBtn = "//*[text()='Proofed']";
	public static final String SC_EditVariable = "//a[text()='Edit Variables']";
	public static final String SC_PartDatefiled_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rdpDripDate_dateInput']";
	public static final String SC_PartDatefiled2_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl06_rdpDripDate_dateInput']";
	public static final String SC_Behalfof_drop_Path = ".//*[@id='ctl00_cphContent_ddlOrderMethods']";
	public static final String SC_Behalfof_Astriek_Path = ".//*[@id='ctl00_cphContent_Label1']";
	public static final String SC_ShipMethoddrop_Path = ".//*[@id='ctl00_cphContent_ddlBasicShipMethods']";

	public static final String SC_Shippingcommentsfield_Path = ".//*[@id='ctl00_cphContent_txtBasicShippingInstructions']";
	public static final String SC_ChargeBackCosttotal_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00__0']/td[3]/span/div[1]/span";
	public static final String SC_Savedlatertitle_Path = ".//*[@id='ctl00_cphContent_divItemsOnHold']/h1";
	public static final String SC_LaterProof1_Path = ".//*[@id='ctl00_cphContent_radItemsOnHold_ctl00__0']/td[2]/a";
	public static final String SC_laterMoveToCartbtn1_Path = ".//*[@id='ctl00_cphContent_radItemsOnHold_ctl00_ctl04_btnResume']";
	public static final String SC_ShipInfoTitle_Path = ".//span[text()='Shipping Information']";
	public static final String SC_Name_Path = ".//*[@id='ctl00_cphContent_txtName']";
	public static final String SC_Company_Path = ".//*[@id='ctl00_cphContent_txtCompany']";
	public static final String SC_Address1_Path = ".//*[@id='ctl00_cphContent_txtAddress1']";
	public static final String SC_Address2_Path = ".//*[@id='ctl00_cphContent_txtAddress2']";
	public static final String SC_Address3_Path = ".//*[@id='ctl00_cphContent_txtAddress3']";
	public static final String SC_City_Path = ".//*[@id='ctl00_cphContent_txtCity']";
	public static final String SC_State_Path = ".//*[@id='ctl00_cphContent_txtState']";
	public static final String SC_Zip_Path = ".//*[@id='ctl00_cphContent_txtZip']";
	public static final String SC_Country_Path = ".//*[@id='ctl00_cphContent_ddlCountry']";
	public static final String SC_Phone_Path = ".//*[@id='ctl00_cphContent_txtPhone']";
	public static final String SC_Email_Path = ".//*[@id='ctl00_cphContent_txtEmail']";
	public static final String SC_Emaildesc_Path = ".//*[@id='ctl00_cphContent_lblMultipleEmailAddresses']";
	public static final String Order_Confirmation_Checkbox_Path = ".//*[@id='ctl00_cphContent_cbxOrderConfirmation']";
	public static final String SC_OrderConfirmationlabel_Path = ".//*[@id='divOrderEmailNotifications']/div/label[1]";
	public static final String Shipped_Confirmation_Checkbox_Path = ".//*[@id='ctl00_cphContent_cbxShippedConfirmation']";
	public static final String SC_ShippedConfirmationlabel_Path = ".//*[@id='divOrderEmailNotifications']/div/label[2]";
	public static final String SC_CreateOrderTemplatebtn_Path = ".//a[text()='Create Order Template']";
	public static final String SC_Clearcartbtn_Path = ".//a[text()='Clear Cart']";
	public static final String Next_btn_Path = ".//a[contains(text(),'Next')]";
	public static final String SC_Deliverydate_Path = ".//*[@id='ctl00_cphContent_rdtepkrDeliveryDate_dateInput']";
	public static final String SC_Costcenterck_Path = ".//*[@id='ctl00_cphContent_rblPaymentType_0']";
	public static final String SC_SC_Costcenterdrop_Path_Path = ".//*[@id='ctl00_cphContent_ddlCostCenter']";
	public static final String SC_Shipcomments_Path = ".//*[@id='ctl00_cphContent_txtShippingInstructions']";
	public static final String SC_Checkout_Path = ".//a[text()='Checkout']";
	public static final String SC_Loadingpanel_Path = ".//*[@id='ctl00_cphContent_RadAjaxLoadingPanelctl00_cphContent_pnlShipping']";
	public static final String SC_DeliverydateCalender_Path = ".//*[@id='ctl00_cphContent_rdtepkrDeliveryDate_popupButton']";

	public static final String SC_CalenderMonth_Path = ".//*[@id='ctl00_cphContent_rdtepkrDeliveryDate_calendar_Title']";
	public static final String SC_CalenderOK_Path = ".//*[@id='rcMView_OK']";
	public static final String SC_UPSGround_Check_Path = ".//*[@id='ctl00_cphContent_rdbShippingGround']";
	public static final String SC_DeliveryMethod_Path = ".//*[@id='ctl00_cphContent_lblDeliveryMethod']";

	public static final String Crdt_Num_Path = ".//*[@id='ctl00_cphContent_CCCPayment_CreditCardNumber']";
	public static final String Security_Code_Path = ".//*[@id='ctl00_cphContent_CCCPayment_SecurityCode']";
	public static final String ExpirationMonth_Path = ".//*[@id='ctl00_cphContent_CCCPayment_ExpirationMonth']";
	public static final String ExpirationYear_Path = ".//*[@id='ctl00_cphContent_CCCPayment_ExpirationYear']";

	// Create Order Template Pop up
	public static final String SC_COT_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div";
	public static final String SC_COT_Title_Path = ".//*[@id='ctl00_cphContent_divOverlay']/div/div/div[2]/div/div[1]";
	public static final String SC_COT_Name_Path = ".//*[@id='ctl00_cphContent_txtOrderTemplateName']";
	public static final String SC_COT_Description_Path = ".//*[@id='ctl00_cphContent_txtOrderTemplateDescription']";
	public static final String SC_COT_Createbtn_Path = ".//*[@id='ctl00_cphContent_btnCreateOrderTemplate']";
	public static final String SC_COT_Cancelbtn_Path = ".//*[@id='ctl00_cphContent_btnHideCreateOrderTemplatePopup']";

}
