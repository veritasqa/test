package com.globalatlantic.Base;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.globalatlantic.Methods.CommonMethods;
import com.relevantcodes.extentreports.ExtentReports;

public class BaseTest extends CommonMethods {

	public static ExtentReports report;

	@BeforeSuite
	public void openbrowser() throws IOException, InterruptedException {

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		Implicit_Wait();

	}

	@AfterSuite(enabled = false)
	public void AfterSuite() {

		Cancelallorders();

		report.flush();
		report.close();
		Close_Browser();
		Quit_Browser();

	}

	public void TL_login() {
		OpenUrl_Window_Max(TL_URL);

	}

	public void login() throws IOException, InterruptedException {

		if (Element_Is_Present(locatorType, Username_Path)) {
			Type(locatorType, Username_Path, UserName);
			Type(locatorType, Password_Path, Password);
			Click(locatorType, Login_btn_Path);
			Assert.assertTrue(Element_Is_Present(locatorType, Logout_Path));
			Implicit_Wait();
		}

		else {
			logout();
			Type(locatorType, Username_Path, UserName);
			Type(locatorType, Password_Path, Password);
			Click(locatorType, Login_btn_Path);
			Assert.assertTrue(Element_Is_Present(locatorType, Logout_Path));
			Implicit_Wait();
		}

	}

	public void logout() throws IOException, InterruptedException {

		Click(locatorType, Home_btn_Path);
		softAssert.assertTrue(Element_Is_Present(locatorType, Logout_Path));
		Click(locatorType, Logout_Path);
		ExplicitWait_Element_Clickable(locatorType, Login_btn_Path);

	}

	public void Clearcart() throws InterruptedException {
		Click(locatorType, Clear_Cart_Path);
		Thread.sleep(8000);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
	}

	public void ContactSearch(String firname, String Laname) {

		Type(locatorType, FirstName_Path, firname);
		Type(locatorType, LastName_Path, Laname);

		Click(locatorType, Search_btn_Path);

		ExplicitWait_Element_Clickable(locatorType, Edit_btn_Path);

		ExplicitWait_Element_Clickable(locatorType, Contact_Click_Path);

		Click(locatorType, Contact_Click_Path);

	}

	public void PlaceanOrder(String methodname) throws InterruptedException {
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Title_path).equalsIgnoreCase("Checkout"),
				"Checkout Pageis not displayed");

		if (Element_Is_selected(locatorType, Order_Confirmation_Checkbox_Path)) {
			Assert.assertTrue(Element_Is_selected(locatorType, Order_Confirmation_Checkbox_Path));
		} else {
			Click(locatorType, Order_Confirmation_Checkbox_Path);

		}
		if (Element_Is_selected(locatorType, Shipped_Confirmation_Checkbox_Path)) {
			Assert.assertTrue(Element_Is_selected(locatorType, Shipped_Confirmation_Checkbox_Path));
		} else {
			Click(locatorType, Shipped_Confirmation_Checkbox_Path);

		}
		Click(locatorType, Next_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, StandardShipping_btn_Path);
		Select_DropDown_VisibleText(Xpath, Orderingfor, "Self");
		Click(locatorType, StandardShipping_btn_Path);

		if (Element_Is_Displayed(locatorType, CostCenter_btn_Path)) {
			Click(locatorType, CostCenter_btn_Path);
		}
		Click(locatorType, Checkout_btn_Path);

		ExplicitWait_Element_Clickable(locatorType, Submit_Cancel_Req_Btn_Path);
		// System.out.println(Get_Text(locatorType,
		// OrderConfirmation_Title_path));
		Assert.assertTrue(Get_Text(locatorType, OrderConfirmation_Title_path).equalsIgnoreCase("Order Confirmation"),
				"Order Confirmation Page is not displayed");
		OrderNumber = Get_Text(locatorType, OrderNumber_Path);
		System.out.println("Order Number placed in method  " + methodname + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + methodname + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);
	}

	public void CreateContactandclick(String RecipientFirstname, String RecipientLastname, String CityName,
			String StateName, String Zipnumber, String FirmName) throws InterruptedException {

		Type(locatorType, FirstName_Path, RecipientFirstname);
		Type(locatorType, LastName_Path, RecipientLastname);

		Click(locatorType, Search_btn_Path);
		Wait_ajax();
		if (Element_Is_Displayed(locatorType, Add_NoMatch_Path)) {

			Click(locatorType, Add_Contact_Path);
			Wait_ajax();
			Type(locatorType, Add_FirstName_Path, RecipientFirstname);
			Type(locatorType, Add_LastName_Path, RecipientLastname);
			Type(locatorType, Add_Address1_Path, Address1);
			Type(locatorType, Add_City_Path, CityName);
			Click(locatorType, Add_State_Path);
			Click(locatorType, li_value(StateName));
			Type(locatorType, Add_ZipCode_Path, Zipnumber);
			Type(locatorType, Add_Phone_Path, "(222) 222-2222");
			Type(locatorType, Add_Email_Path, EmailId);
			Click(locatorType, Add_FirmArroe_Path);
			Thread.sleep(1000);
			Click(locatorType, li_value(FirmName));
			Click(locatorType, Add_publicContact_Checkbox_Path);
			Click(locatorType, Add_Add_BTn_Path);
			Wait_ajax();
			Click(locatorType, Clear_Cart_Path);
			Wait_ajax();

			Type(locatorType, FirstName_Path, RecipientFirstname);
			Type(locatorType, LastName_Path, RecipientLastname);

			Click(locatorType, Search_btn_Path);
			Wait_ajax();

		}

		ExplicitWait_Element_Clickable(locatorType, Edit_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Contact_Click_Path);
		Click(locatorType, Contact_Click_Path);

	}

	public void Select_li_Dropdown(String Path, String liValue) throws InterruptedException {
		Click(locatorType, Path);

		Thread.sleep(1000);
		Click(locatorType, li_value(liValue));

		Thread.sleep(1000);
	}

	public void PlaceTLorder(String methodname) throws InterruptedException {

		// Placing an Order
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part10);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Click(locatorType, TL_Shoppingcartcheckout_Path);
		Type(locatorType, TL_CO_Captchafield_Path, captcha());
		Click(locatorType, TL_CO_CaptchaValidatebtn_Path);
		Type(locatorType, TL_CO_Name_Path, "QA Test");
		Type(locatorType, TL_CO_Address1_Path, Address1);
		Type(locatorType, TL_CO_City_Path, City);
		Type(locatorType, TL_CO_State_Path, State);
		Type(locatorType, TL_CO_Zip_Path, Zipcode);
		Type(locatorType, TL_CO_Email_Path, EmailId);
		Click(locatorType, TL_CO_Confirmaddressbtn_Path);
		Click(locatorType, TL_CO_Placeorderbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_OrderConfirmation_Path),
				"Order Confirmation page is not displayed");
		OrderNumber = Get_Text(locatorType, TL_OCP_OrderNo_Path);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + methodname + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	public void Cancelallorders() {

		// Open_Browser(Browser,Browserpath);
		OpenUrl_Window_Max(Inventory_URL);
		Type(locatorType, Inventory_Username_Path, Inventory_UserName);
		Type(locatorType, Inventory_Passowrd_Path, Inventory_Password);
		Click(locatorType, Inventory_Submit_Btn_Path);

		for (String Onum : ordernum) {

			ExplicitWait_Element_Visible(locatorType, Inventory_OrderSearch_Path);
			Click(locatorType, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path);
			Type(locatorType, Inventory_Ordernumber_Path, Onum);
			Click(locatorType, Inventory_Search_Btn_Path);
			Click(locatorType, Inventory_Edit_Btn_Path);

			if (Get_Attribute(locatorType, Inventory_Order_Status, "Value").equalsIgnoreCase("NEW")) {

				Click(locatorType, Inventory_Cancelorder_Btn_Path);
				Accept_Alert();

				Assert.assertTrue(
						Get_Attribute(locatorType, Inventory_Order_Status, "Value").equalsIgnoreCase("Cancelled"),
						"Order is not cancelled is not displayed");

			}
			Click(locatorType, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path);
		}

		Click(locatorType, Inventory_logout_btn_Path);
		ExplicitWait_Element_Visible(locatorType, Inventory_Username_Path);

	}

	public void Cancelsingleorder(String Onum)

	{
		OpenUrl_Window_Max(Inventory_URL);
		Type(locatorType, Inventory_Username_Path, Inventory_UserName);
		Type(locatorType, Inventory_Passowrd_Path, Inventory_Password);
		Click(locatorType, Inventory_Submit_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, Inventory_OrderSearch_Path);
		Click(locatorType, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path);
		Type(locatorType, Inventory_Ordernumber_Path, Onum);
		Click(locatorType, Inventory_Search_Btn_Path);
		Click(locatorType, Inventory_Edit_Btn_Path);
		Click(locatorType, Inventory_Cancelorder_Btn_Path);
		Accept_Alert();
		Assert.assertTrue(Get_Attribute(locatorType, Inventory_Order_Status, "Value").equalsIgnoreCase("Cancelled"),
				"Order is not cancelled is not displayed");
		Reporter.log(Onum + " is cancelled successfully");

	}

	public void IFramepage() {
		OpenUrl_Window_Max(IFrame_URL);
		ExplicitWait_Element_Clickable(locatorType, Iframe_Submitbtn_Path);
	}

	public void Appbuilder_IFramepage() {
		OpenUrl_Window_Max(Appbuilder_IFrame_URL);
		ExplicitWait_Element_Clickable(locatorType, Iframe_Submitbtn_Path);
	}

	public void SSOPage_Login() {
		OpenUrl_Window_Max(SSO_URL);
		ExplicitWait_Element_Clickable(locatorType, SSO_Submitbtn_Path);
		Type(locatorType, SSO_UserID_Path, UserId);
		Type(locatorType, SSO_Firstname_Path, Firstname);
		Type(locatorType, SSO_Lastname_Path, Lastname);
		Type(locatorType, SSO_Emailaddress_Path, EmailId);
		Type(locatorType, SSO_Address1_Path, Address1);
		Type(locatorType, SSO_City_Path, City);
		Type(locatorType, SSO_State_Path, State);
		Type(locatorType, SSO_Zip_Path, Zipcode);
		Type(locatorType, SSO_BusinessOwner_Path, "Broker Dealer");
		Type(locatorType, SSO_UserGroups_Path, "Broker Dealer");
		Type(locatorType, SSO_ReturnURL_Path, "~/DocumentLibrary/DocumentLibrary.aspx");
		Click(locatorType, SSO_Submitbtn_Path);

	}

	public void Appbuilder_SSOPage_Login() {
		OpenUrl_Window_Max(SSO_URL);
		ExplicitWait_Element_Clickable(locatorType, SSO_Submitbtn_Path);
		Type(locatorType, SSO_UserID_Path, UserId);
		Type(locatorType, SSO_Firstname_Path, Firstname);
		Type(locatorType, SSO_Lastname_Path, Lastname);
		Type(locatorType, SSO_Emailaddress_Path, EmailId);
		Type(locatorType, SSO_Address1_Path, Address1);
		Type(locatorType, SSO_City_Path, City);
		Type(locatorType, SSO_State_Path, State);
		Type(locatorType, SSO_Zip_Path, Zipcode);
		Type(locatorType, SSO_BusinessOwner_Path, "Broker Dealer");
		Type(locatorType, SSO_UserGroups_Path, "Broker Dealer");
		Type(locatorType, SSO_ReturnURL_Path, "~/DocumentLibrary/ApplicationBuilder.aspx");
		Click(locatorType, SSO_Submitbtn_Path);

	}

	public String Doclib_Partsview(String Titleunder, String Partname) {

		return ".//h3[text()='" + Titleunder + "']//following::tbody[1]//a[text()='" + Partname + "']";

	}

	public String AppBuilder_ReqformsState_Partname(String State, String Partname) {

		return ".//h3[text()='Required Forms for all sales in the state of " + State
				+ "']/following::tbody[1]//a[text()='" + Partname + "']";

	}

	public String AppBuilder_Marketing_Partname(String Partname) {

		// System.out.println(".//h3[text()='Marketing']/following::tbody[1]//a[text()='"
		// + Partname + "']");

		return ".//h3[text()='Marketing']/following::tbody[1]//a[text()='" + Partname + "']";

	}

	public String AppBuilder_NewBusiness_Partname(String Partname) {

		// System.out.println(".//h3[text()='Marketing']/following::tbody[1]//a[text()='"
		// + Partname + "']");

		return ".//h3[text()='New Business']/following::tbody[1]//a[text()='" + Partname + "']";

	}

	public String AppBuilder_ReqformsState_Ckbox(String State, int Count) {

		return ".//h3[text()='Required Forms for all sales in the state of " + State + "']/following::tbody[1]//tr["
				+ Count + "]//td[1]//input";

	}

	public String AppBuilder_ReqformsState_List(String State) {

		return ".//h3[text()='Required Forms for all sales in the state of " + State
				+ "']/following::tbody[1]//tr/td[3]/a";
	}

	public boolean AppBuilder_ReqformsState_PartnameCkbox(String locatorType, String State, String Partname) {

		By locator, locator2;
		int count = 0;
		locator = locatorValue(locatorType, AppBuilder_ReqformsState_List(State));
		// ExplicitWait_Element_Visible(locatorType, value);
		List<WebElement> rows = driver.findElements(locator);
		for (WebElement Part : rows) {
			count = count + 1;
			if (Part.getText().equalsIgnoreCase(Partname)) {
				// System.out.println(Part.getText());
				break;

			}

		}

		locator2 = locatorValue(locatorType, AppBuilder_ReqformsState_Ckbox(State, count));
		return driver.findElement(locator2).isEnabled();
	}
}
