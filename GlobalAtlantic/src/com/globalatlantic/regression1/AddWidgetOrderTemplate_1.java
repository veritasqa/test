package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class AddWidgetOrderTemplate_1 extends BaseTest {

	@Test(priority = 1, enabled = true)

	public void GA_TC_2_7_7_2_1() throws InterruptedException {

		// Add to Cart - Create order

		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part1);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part1), Part1 + "is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);

		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part1),
				Part1 + " is not displayed in checkout page");
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		Thread.sleep(2000);
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part1 + " is displays on the 'Order Template' page");
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Template1AddtoCart_Btn_Path));
		Click(locatorType, Next_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, StandardShipping_btn_Path);
		Select_DropDown_VisibleText(Xpath, Orderingfor, "Self");
		Click(locatorType, StandardShipping_btn_Path);

		if (Element_Is_Displayed(locatorType, CostCenter_btn_Path)) {
			Click(locatorType, CostCenter_btn_Path);
		}
		Click(locatorType, Checkout_btn_Path);

		ExplicitWait_Element_Clickable(locatorType, Submit_Cancel_Req_Btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderConfirmation_Title_path).equalsIgnoreCase("Order Confirmation"),
				"Order Confirmation Page is not displayed");
		OrderNumber = Get_Text(locatorType, OrderNumber_Path);
		System.out.println("Order Number placed in method  " + "GA_TC_2_7_7_2_1" + " is '" + OrderNumber + "'");
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method " + "GA_TC_2_7_7_2_1" + " is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);

	}

	@Test(priority = 2, enabled = true, dependsOnMethods = "GA_TC_2_7_7_2_1")
	public void GA_TC_2_7_7_2_2() throws IOException, InterruptedException {

		// Delete an Order Template

		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Announcement_view_btn_Path), "Home Page is not displayed");
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ContactSearch_Title_Path),
				"Contact Search button is not displayed");
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Click(locatorType, OrderTemplatePage_Delete_btn_Path);
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

	}

	@BeforeClass(enabled = true)
	public void BeforeTest() throws IOException, InterruptedException {
		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Announcement_view_btn_Path), "Home Page is not displayed");

		Hover(locatorType, Add_Widget_Path);
		Widgetname = Get_Text(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ContactSearch_Title_Path),
				"Contact Search button is not displayed");
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

		while (Element_Is_Displayed(locatorType, OrderTemplate_Template1View_Btn_Path)) {

			Click(locatorType, OrderTemplate_Template1View_Btn_Path);
			ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
			Click(locatorType, OrderTemplatePage_Delete_btn_Path);
			Click(locatorType, Home_btn_Path);
			ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

		}
		Clearcart();
	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
