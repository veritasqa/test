package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.globalatlantic.Base.BaseTest;

public class DocumentLibrary_IFrame extends BaseTest {

	public void Navigate_DocLib() {
		Click(locatorType, Document_lib_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Doclib_Searchbtn_Path);

	}

	public void Manage_Inventory_Menu_Hover() {
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);

	}

	@Test(enabled = true)
	public void GA_TC_2_5_1_5() throws InterruptedException {

		// I-Frame
		// Validate Pieces displayed in document library when
		// 1.Product line type as Broker Dealer
		// 2.Product Line as SecureFore 5
		// 3.Product Line Category is New Business
		// 4.View in as Document Library is Checked

		// Pre Req

		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Part25);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		if (Element_Is_selected(locatorType, ManageInventory_chkDocumentLibrary_Path)) {
		} else {
			Click(locatorType, ManageInventory_chkDocumentLibrary_Path);
			Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
			Click(locatorType, ManageInventory_Save_OK_Btn_Path);
			Click(locatorType, Logout_Path);
		}

		// TC Starts here
		IFramepage();
		Assert.assertTrue(Get_Attribute(locatorType, Iframe_Doctypes_Path, "value").equalsIgnoreCase("Broker Dealer"),
				"'Broker Dealer' is not displayed in 'DOCLIB_PRODUCT_LINE_TYPES'");
		Type(locatorType, Iframe_Docfirms_Path, "");
		Click(locatorType, Iframe_Submitbtn_Path);
		Select_li_Dropdown(Iframe_DocStatearrow_Path, "Alabama");
		Select_li_Dropdown(Iframe_DocProductlinearrow_Path, "SecureFore 5");
		Click(locatorType, Iframe_DocSearchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Doclib_Partsview("New Business", Part25)),
				Part25 + " is not displayed under 'New Business'");

	}

	@Test(enabled = true)
	public void GA_TC_2_5_1_6() throws IOException, InterruptedException {

		// I Frame -Validate Piece is not displayed in document library when
		// View in is unchecked

		// Pre-Req
		OpenUrl_Window_Max(URL);
		Type(locatorType, Username_Path, UserName);
		Type(locatorType, Password_Path, Password);
		Click(locatorType, Login_btn_Path);
		// login();
		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Part25);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		if (Element_Is_selected(locatorType, ManageInventory_chkDocumentLibrary_Path)) {

			Click(locatorType, ManageInventory_chkDocumentLibrary_Path);
			Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
			Click(locatorType, ManageInventory_Save_OK_Btn_Path);
			Click(locatorType, Logout_Path);
		}

		// TC Starts here
		IFramepage();
		Assert.assertTrue(Get_Attribute(locatorType, Iframe_Doctypes_Path, "value").equalsIgnoreCase("Broker Dealer"),
				"'Broker Dealer' is not displayed in 'DOCLIB_PRODUCT_LINE_TYPES'");
		Type(locatorType, Iframe_Docfirms_Path, "");
		Click(locatorType, Iframe_Submitbtn_Path);
		Select_li_Dropdown(Iframe_DocStatearrow_Path, "Alabama");
		Select_li_Dropdown(Iframe_DocProductlinearrow_Path, "SecureFore 5");
		Click(locatorType, Iframe_DocSearchbtn_Path);
		Assert.assertFalse(Element_Is_Displayed(locatorType, Doclib_Partsview("New Business", Part25)),
				Part25 + " is displayed under 'New Business'");

	}

	@BeforeTest(enabled = true)
	public void BeforTest() throws IOException, InterruptedException {
		login();
	}

	@AfterMethod(enabled = true)
	public void AfteMethod() throws IOException, InterruptedException {

	}

	@AfterClass(enabled = false)
	public void BeforeMethod() throws IOException, InterruptedException {
		logout();
	}

}