package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Admin_Manage_Users extends BaseTest {

	public void Manage_User_Menu_Hover() throws InterruptedException {
		Hover(locatorType, Admin_btn_Path, driver);
		Wait_ajax();
		Click(locatorType, Manage_Users_Path, driver);
		ExplicitWait_Element_Visible(locatorType, MU_AdduserPath, driver);

	}

	@Test
	public void GATC_2_4_13_1_3() throws InterruptedException {

		Manage_User_Menu_Hover();
		Assert.assertTrue(Get_Title(driver).trim().contains("Manage Users"),
				"Username Not matching in the Results Field");

		Type(locatorType, MU_Username_Path, MU_Username, driver);
		Click(locatorType, MU_Username_FilterIcon_Path, driver);
		Thread_Sleep(10000);

		Assert.assertTrue(Get_Text(locatorType, MU_Result1_Username_Path, driver).trim().equalsIgnoreCase(MU_Username),
				"Username Not matching in the Results Field");

		Click(locatorType, MU_Result1_Edit_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MU_Result_Update_Path, driver);
		Type(locatorType, MU_Result_Address_Path, MU_Address1, driver);

		Click(locatorType, MU_Result_Update_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MU_Result1_Edit_Path, driver);
		Thread_Sleep(5000);
		Click(locatorType, MU_Result1_Edit_Path, driver);

		Assert.assertTrue(
				Get_Text(locatorType, MU_Result_Address_Path, driver).trim().equalsIgnoreCase(MU_Result1_Edit_Path),
				"Address Not matching in the Results Field");

		// to revert back to same old address
		Type(locatorType, MU_Result_Address_Path, Address1, driver);

		Click(locatorType, MU_Result_Update_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, MU_Result1_Edit_Path, driver);
		Thread_Sleep(5000);
		Click(locatorType, MU_Result1_Edit_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MU_Result_Address_Path, driver).trim().equalsIgnoreCase(Address1),
				"Address Not reverting back");

	}

	@Test
	public void GATC_2_4_13_1_4() throws InterruptedException {
		Manage_User_Menu_Hover();
		Click(locatorType, MU_AdduserPath, driver);

		ExplicitWait_Element_Clickable(locatorType, MU_Result_Update_Path, driver);

		Type(locatorType, MU_Result_Username_Path, Address1, driver);
		Type(locatorType, MU_Result_Firstname_Path, Address1, driver);
		Type(locatorType, MU_Result_Lastname_Path, Address1, driver);
		Type(locatorType, MU_Result_Email_Path, Address1, driver);

		Click(locatorType, MU_Result_Cancel_Path, driver);

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

}
