package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfilment_StateFirmRestrictions extends BaseTest {

	/*
	public void NoRecord(String Part , String Fname , String Lname){
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
	
		Type(locatorType, Fullfilment_Search_Path, Part, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
	
		// contact name
	
		ContactSearch(Fname, Lname);
	
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, No_Records_Message_path, driver).trim()
				.equalsIgnoreCase(No_Records_Message),
				Part + " is displayed or no error message disyplayed ");
	
		Thread_Sleep(3000);
		
		
	} */

	/*Validate that a part with State Restriction W and
	 *  Firm Restriction X is not visible for a contact with State Y and Firm Z*/
	@Test(enabled = true, priority = 1)
	public void GATC_2_9_4_1() throws InterruptedException {

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part4, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_4_1", "Buffalo Grove", "Illinois", "60089", "American Equity");

		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part4 + " is displayed or no error message disyplayed ");

	}

	/*Validate kit on the fly( QA_Mainpart) with firm restriction X, Y,Z and State Restriction A,B,C has
	    1.child component 1( CC1) with firm restriction X and state Restriction A 
		2.Child component 2( CC2) with firm restriction Y and state Restriction B, 
		3.Child component 3( CC3) with No firm restriction and No state Restriction , 
		4.Child component 4 (CC4) with firm restriction Z and state Restriction C 
		appears when contact has firm restriction x or y  or z  and state restriction A, B or C and appropriate child items appears*/
	@Test(enabled = true, priority = 2)
	public void GATC_2_9_4_4() throws InterruptedException {

		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part44, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_4_4", "Buffalo Groove", "Illinois", "60089", "5/3");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part44),
				Part44 + "is not displayed");

		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		Assert.assertTrue(
				Get_Text(locatorType, Qa_Kitfly_Details_Option1, driver).trim()
						.contains("QA_APPSTATIC_STATEFIRMRESTRICTION_7"),
				" QA_APPSTATIC_STATEFIRMRESTRICTION_7 is not  displayed ");
		Assert.assertTrue(Get_Text(locatorType, Qa_Kitfly_Details_Option2, driver).trim()
				.contains("QA_BROCHURESTAT_NOFIRMRESTRICTION"), "QA_BROCHURESTAT_NOFIRMRESTRICTION is displayed");
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Click(locatorType, Qa_Kitfly_CmptDtls_Btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		Assert.assertTrue(
				Get_Text(locatorType, Qa_Kitfly_checkout_option1, driver).trim()
						.contains("QA_APPSTATIC_STATEFIRMRESTRICTION_7"),
				" QA_APPSTATIC_STATEFIRMRESTRICTION_7 is not  displayed ");
		Assert.assertTrue(Get_Text(locatorType, Qa_Kitfly_checkout_option2, driver).trim()
				.contains("QA_BROCHURESTAT_NOFIRMRESTRICTION"), "QA_BROCHURESTAT_NOFIRMRESTRICTION is displayed");
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);

		PlaceanOrder("GATC_2_9_4_4");
		Assert.assertTrue(
				Get_Text(locatorType, OrderConfirm_Child1, driver).trim()
						.contains("QA_APPSTATIC_STATEFIRMRESTRICTION_7"),
				"QA_APPSTATIC_STATEFIRMRESTRICTION_7 is not  displayed ");
		Assert.assertTrue(
				Get_Text(locatorType, OrderConfirm_Child2, driver).trim().contains("QA_APPSTATIC_STATERESTRICTION_2"),
				"QA_BROCHURESTAT_NOFIRMRESTRICTION is displayed");
		Clearcart();

		//

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part44, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_4_4", "Los Angeles", "California", "90001", "Capital Investment");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part44),
				Part44 + "is not displayed");

		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		Assert.assertTrue(
				Get_Text(locatorType, Qa_Kitfly_Details_Option1, driver).trim()
						.contains("QA_APPSTATIC_STATEFIRMRESTRICTION_7B"),
				" QA_APPSTATIC_STATEFIRMRESTRICTION_7B is not  displayed ");
		Assert.assertTrue(Get_Text(locatorType, Qa_Kitfly_Details_Option2, driver).trim()
				.contains("QA_BROCHURESTAT_NOFIRMRESTRICTION"), "QA_BROCHURESTAT_NOFIRMRESTRICTION is displayed");
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Click(locatorType, Qa_Kitfly_CmptDtls_Btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		Assert.assertTrue(
				Get_Text(locatorType, Qa_Kitfly_checkout_option1, driver).trim()
						.contains("QA_APPSTATIC_STATEFIRMRESTRICTION_7B"),
				" QA_APPSTATIC_STATEFIRMRESTRICTION_7Bis not  displayed ");
		Assert.assertTrue(Get_Text(locatorType, Qa_Kitfly_checkout_option2, driver).trim()
				.contains("QA_BROCHURESTAT_NOFIRMRESTRICTION"), "QA_BROCHURESTAT_NOFIRMRESTRICTION is displayed");
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);

		PlaceanOrder("GATC_2_9_4_4");
		Assert.assertTrue(
				Get_Text(locatorType, OrderConfirm_Child1, driver).trim()
						.contains("QA_APPSTATIC_STATEFIRMRESTRICTION_7B"),
				"QA_APPSTATIC_STATEFIRMRESTRICTION_7B is not  displayed ");
		Assert.assertTrue(
				Get_Text(locatorType, OrderConfirm_Child2, driver).trim().contains("QA_APPSTATIC_STATERESTRICTION_2"),
				"QA_BROCHURESTAT_NOFIRMRESTRICTION is displayed");

	}

	/*Validate kit on the fly( QA_Mainpart) with firm restriction X, Y,Z and State Restriction A,B,C has
	
	1.child component 1( CC1) with firm restriction X and state Restriction A 
	2.Child component 2( CC2) with firm restriction Y and state Restriction B, 
	3.Child component 3( CC3) with No firm restriction and No state Restriction , 
	4.Child component 4 (CC4) with firm restriction Z and state Restriction C 
	
	does not appear  when contact has W firm restriction and State restriction D 
	OR when contact has no firm restriction and state restriction D 
	 * */

	@Test(enabled = true, priority = 3)
	public void GATC_2_9_4_5() throws InterruptedException {
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part44, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_4_5", "Buffalo", "New York", "14201", "East Lawn");

		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part44 + " is displayed or no error message disyplayed ");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}
}
