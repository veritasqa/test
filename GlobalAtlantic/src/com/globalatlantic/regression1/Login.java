package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Login extends BaseTest{
	
	@Test(enabled = true , priority =1)
	
	public void GATC_1_0_1_2() throws IOException, InterruptedException{
		    Open_Url_Window_Max(URL, driver);
		    Type(locatorType, Username_Path, UserName, driver);
	 		Type(locatorType, Password_Path, Password, driver);
	 		Click(locatorType, Login_btn_Path, driver);
	 		Assert.assertTrue(Is_Element_Present(locatorType, Logout_Path, driver));
	 		Implicit_Wait(driver);
	 		logout();
		  }
	
	public void GATC_1_0_2_1() throws IOException, InterruptedException {

		// Verify the error message is displayed upon "Invalid user name and password" combination

		Open_Url_Window_Max(URL, driver);
		Type(locatorType, Username_Path, Firstname, driver);
		Type(locatorType, Password_Path, Password, driver);
		Click(locatorType, Login_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, BadPassword_error_Path, driver), "Bad user name or password is not displayed");
		Type(locatorType, Username_Path, "Test", driver);
		Type(locatorType, Password_Path, Password, driver);
		Click(locatorType, Login_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, BadPassword_error_Path, driver), "Bad user name or password is not displayed");

		}
	
   @Test(enabled = true , priority =2)
	
	public void GATC_1_0_2_2(){
	    Open_Url_Window_Max(URL, driver);
	    Type(locatorType, Password_Path, Password, driver);
	    Click(locatorType, Login_btn_Path, driver);
 		Assert.assertTrue(Is_Element_Present(locatorType, Username_error_Path, driver));
     }
	
	
	@Test(enabled = true , priority =3)
	
	public void GATC_1_0_2_3(){
	    Open_Url_Window_Max(URL, driver);
	    Type(locatorType, Username_Path, UserName, driver);
	    Click(locatorType, Login_btn_Path, driver);
 		Assert.assertTrue(Is_Element_Present(locatorType, Password_error_Path, driver));
     }
	
	
	@Test(enabled = true , priority =4)
	
	public void GATC_1_0_2_4(){
	    Open_Url_Window_Max(URL, driver);
	    Click(locatorType, Login_btn_Path, driver);
 		Assert.assertTrue(Is_Element_Present(locatorType, Username_error_Path, driver));
 		Assert.assertTrue(Is_Element_Present(locatorType, Password_error_Path, driver));
     }
	
	
	

}
