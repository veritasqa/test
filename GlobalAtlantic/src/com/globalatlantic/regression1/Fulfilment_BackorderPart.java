package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfilment_BackorderPart extends BaseTest {

	// Validate a Part that doesn't allow backorders
	@Test
	public void GATC_2_9_7_3() {
		Hover(locatorType, Admin_btn_Path, driver);
		Click(locatorType, Manage_Inventory_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path, driver);

		Type(locatorType, ManageInventory_FormNo_Path, Part41, driver);

		Click(locatorType, ManageInventory_Search_Btn_Path, driver);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);

		Click(locatorType, ManageInventory_RulesTab_Path, driver);
		// Thread_Sleep(3000);

		// Assert.assertFalse(Element_Isselected(locatorType,
		// ManageInventory_Rules_AllowBackordersRadio_Path, driver),
		// "Allow Backorder is Selected in rules tab");

		if (!Element_Isselected(locatorType, ManageInventory_Rules_AllowBackordersRadio_Path, driver)) {

			Click(locatorType, ManageInventory_Rules_AllowBackordersRadio_Path, driver);

		}

		Type(locatorType, Fullfilment_Search_Path, Part41, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part41),
				Part41 + "is not displayed");
	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
