package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.globalatlantic.Base.BaseTest;

public class AddWidget_ProductSearch extends BaseTest {

	@Test
	public void GATC_2_7_4_1_1() {

		Hover(locatorType, Add_Widget_Path, driver);
		// Thread.sleep(500);

		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Quick_Search_Path, driver);

		Click(locatorType, Quick_Search_Path, driver);

		Type(locatorType, Wid_Xpath(Widgetname, Quick_Search_txt_path), Part1, driver);

		Click(locatorType, Wid_Xpath(Widgetname, Quick_Search_btn_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		Type(locatorType, FirstName_Path, Firstname, driver);
		Type(locatorType, LastName_Path, Lastname, driver);

		Click(locatorType, Search_btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, Edit_btn_Path, driver);

		Click(locatorType, Contact_Click_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Click(locatorType, Add_To_Cart_btn_Path, driver);
		// ExplicitWait_Element_Visible(locatorType,Add_Cart_Message_Path,driver);
		Thread_Sleep(3000);
		// System.out.println("Message
		// is"+Get_Text(locatorType,Add_Cart_Message_Path,driver));
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		// Assert.assertTrue(Add_Cart_Message.equalsIgnoreCase("Successfully
		// Added 1!"),"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");

		Clearcart();
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout"),
				"Incorrectly displyed in the Checkout button ");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
