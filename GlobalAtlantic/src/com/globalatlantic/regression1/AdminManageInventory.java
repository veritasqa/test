package com.globalatlantic.regression1;

import java.io.IOException;
import java.util.Random;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class AdminManageInventory extends BaseTest {

	public void Manage_Inventory_Menu_Hover() {
		Hover(Xpath, Admin_btn_Path);
		Click(Xpath, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Search_Btn_Path);

	}

	@Test(enabled = false, priority = 1)
	public void GATC_2_4_7_1_1() throws InterruptedException {

		// Page opens with create new item, search items, and search results
		// grids and displays appropriate fields

		Manage_Inventory_Menu_Hover();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_CreateNewType_Path),
				"Create New Type Dropdown is nnot displayed");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_CreateNewType_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not the default value in the Create New Type dropdown ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Add_Btn_Path), "Add button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_FormNoToCopy_Path),
				"Form # to Copy field is not displayed");
		Click(Xpath, ManageInventory_CreateNewType_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Brochure - POD")),
				"Brochure-POD is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Brochure - Static")),
				"Brochure-Static is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CD")),
				"CD is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Drip Campaign")),
				"Drip Campaign is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("E-mail")),
				"E-mail is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Envelope  ")),
				"Envelope is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Flyer  - POD")),
				"Flyer-POD is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Flyer  - Static")),
				"Flyer-Static is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forms Booklet")),
				"Forms Booklet is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Formsbook - NCR")),
				"Formsbook-NCR is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Core")),
				"Kit-Core is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				"Kit-On the Fly is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("NCR - Static")),
				"NCR-Static is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Pack Item")),
				"Pack Item is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD")),
				"POD is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Podcast")),
				"PODcast is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("PowerPoint")),
				"PowerPoint is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Promotional Items  ")),
				"Promotional Items is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Prospectus")),
				"Prospectus is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Sticker")),
				"Sticker is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				"Stock is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Supplement")),
				"Supplement is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Supplement - POD")),
				"Supplement-POD is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Variable-POD")),
				"Variable-POD is not displayed in Create New type dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("View Only")),
				"View Only is not displayed in Create New type dropdown");
		Click(Xpath, li_value("- Any - "));
		Thread.sleep(1500);

		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_FormOwner_Path),
				"Formowner Dropdown is not displayed");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_FormOwner_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not the default value in the Form Owner dropdown ");
		Click(Xpath, ManageInventory_FormOwner_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/Operations")),
				"Broker Dealer/Operations is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Preneed/Operations")),
				"Preneed/Operations is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("IMO Annuity/Operations")),
				"IMO Annuity/OPerations is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("FE/Operations")),
				"FE/OPerations is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Capital Funding/Operations")),
				"Forethought Capital Funding/Operations is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Federal Savings Bank")),
				"Forethought Federal Savings Bank is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Veritas - Test")),
				"Veritas-Test is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Annuity/Marketing")),
				"Annuity/Marketing is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Life/Marketing")),
				"Life/Marketing is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("VP/Marketing")),
				"VP/Marketing is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life/Mktg")),
				"Traditional Life/Mktg is not displayed in Form Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life/OPS")),
				"Traditional Life/OPS is not displayed in Form Owner dropdown");
		Click(Xpath, li_value("- Any - "));
		Thread.sleep(1500);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Category_Icon_Path),
				"Category field is not displayed");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_Category_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not the default value in the Category dropdown ");
		Click(Xpath, ManageInventory_Category_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Annuity")),
				"Annuity is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer")),
				"Broker Dealer is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CommonWealth")),
				"CommonWealth is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate")),
				"Corporate is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Final Expense")),
				"Final Expense is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Capital Funding")),
				"Forethought Capital Funding is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Preneed")),
				"Preneed is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life")),
				"Traditional Life is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Trust")),
				"Trust is not displayed in Category dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("TrustGuard")),
				"TrustGuard is not displayed in Category dropdown");
		Click(Xpath, li_value("- Any -"));
		Thread.sleep(1500);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_BusinessOwner_Path),
				"Business Owne field is nnot displayed");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_BusinessOwner_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not the default value in the Business Owne dropdown ");
		Click(Xpath, ManageInventory_BusinessOwner_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Annuity")),
				"Annuity is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer")),
				"Broker Dealer is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CommonWealth")),
				"CommonWealth is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate")),
				"Corporate is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Final Expense")),
				"Final Expense is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Capital Funding")),
				"Forethought Capital Funding is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Preneed")),
				"Preneed is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life")),
				"Traditional Life is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Trust")),
				"Trust is not displayed in Business Owner dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("TrustGuard")),
				"TrustGuard is not displayed in Business Owner dropdown");
		Click(Xpath, li_value("- Any -"));
		Thread.sleep(1500);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_UserGroups_Path),
				"User Group field is not displayed");
		softAssert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_UserGroups_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not the default value in the User Groups dropdown ");
		Click(Xpath, ManageInventory_UserGroups_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("ALL")),
				"ALL is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Annuity ")),
				"Annuity is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer")),
				"Broker Dealer is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer External")),
				"Broker Dealer External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer Internals")),
				"Broker Dealer Internals is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/ForeAccumulation")),
				"Broker Dealer/ForeAccumlation is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/ForeCare")),
				"Broker Dealer/ForeCare is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/ForeIncome")),
				"Broker Dealer/ForeIncome is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/ForeInvestors Choice")),
				"Broker Dealer/ForeInvestors Choice is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/ForeRetirement")),
				"Broker Dealer/ForeRetirement is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/ForeRetirement Foundation")),
				"Broker Dealer/ForeRetirement Foundation is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/IMO ForeCare")),
				"Broker Dealer/IMO ForeCare is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/SecureFore 3")),
				"Broker Dealer/SecureFore 3 is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/SecureFore 5")),
				"Broker Dealer/SecureFore 5 is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Broker Dealer/SecureFore 7")),
				"Broker Dealer/SecureFore 7 is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CommonWealth")),
				"CommonWealth is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Commonwealth External")),
				"Commonwealth External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Commonwealth Internal")),
				"Commonwealth Internal is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CommonWealth/Advantage IV")),
				"CommonWealth/Advantage IV is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CommonWealth/Horizon")),
				"CommonWealth/Horizon is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CommonWealth/Preferred Plus")),
				"CommonWealth/Preferred Plus is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate")),
				"Corporate is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate/External")),
				"Corporate External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Corporate/Internals")),
				"Corporate Internals is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Elite")),
				"Elite is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Final Expense")),
				"Final Expense is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Final Expense/External")),
				"Final Expense/External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Final Expense/Internal")),
				"Final Expense/Internal is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Capital Funding")),
				"Forethought Capital Funding is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Capital Funding/External")),
				"Forethought Capital Funding/External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Forethought Capital Funding/Internal")),
				"Forethought Capital Funding/Internal is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("IMO Channel")),
				"IMO Channel is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Preneed")),
				"Preneed is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Preneed/External")),
				"Preneed/External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Preneed/Internal")),
				"Preneed/Internal is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life")),
				"Traditional Life is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life/External")),
				"Traditional Life/External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Traditional Life/Internal")),
				"Traditional Life/Internal is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Trust")),
				"Trust is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Trust/External")),
				"Trust/External is not displayed in User Groups  dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Trust/Internal")),
				"Trust/Internal is not displayed in User Groups  dropdown");
		Click(Xpath, li_value("- Any -"));
		Thread.sleep(1500);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Active_Path),
				"Category field is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, ManageInventory_Active_Path, "value").equalsIgnoreCase("Active"),
				" Active is not the default value in the Form Owner dropdown ");
		Click(Xpath, ManageInventory_Active_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Active")),
				"Active is not displayed in Active dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Inactive")),
				"Inactive is not displayed in Active dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Obsolete")),
				"Obsolete is not displayed in Active dropdown");
		Click(Xpath, li_value("- Any -"));
		Thread.sleep(1500);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_UnitsOnHand_Path),
				"Units On Hand field is nnot displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, ManageInventory_UnitsOnHand_Path, "value").equalsIgnoreCase("All"),
				"All is not the default value in the Units On Hand dropdown ");
		Click(Xpath, ManageInventory_UnitsOnHand_Icon_Path);
		Thread.sleep(2000);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Low Stock")),
				"Low Stock is not displayed in Units On Hand dropdown");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Out of Stock")),
				"Low Stock is not displayed in Units On Hand dropdown");
		Click(Xpath, li_value("All"));
		Thread.sleep(1500);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Search_Btn_Path),
				"Search button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Clear_Btn_Path),
				"Clear button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_EmptySearchresult_Path),
				"Search Results section is not blank ");
		softAssert.assertAll();

	}

	@Test(enabled = false, priority = 2)
	public void GATC_2_4_7_1_2() throws InterruptedException {

		// Ability to select create new type and click add with appropriate page
		// opening

		Random rand = new Random();
		int number = rand.nextInt(1000);
		int number1 = rand.nextInt(100);
		String Copy_NewItem = Part7 + number + number1;
		Manage_Inventory_Menu_Hover();
		Click(Xpath, ManageInventory_CreateNewType_Icon_Path);
		Thread.sleep(2000);
		Hover(Xpath, ManageInventory_POD_Path);
		Thread.sleep(2000);
		Click(Xpath, ManageInventory_POD_Path);
		Click(Xpath, ManageInventory_Add_Btn_Path);
		Assert.assertTrue(Element_Is_Present(Xpath, ManageInventory_FormTxt_Path), "General tab is not opened");
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_NewItem_header_Path).equalsIgnoreCase("New Item"),
				"New Item is not found");
		Type(Xpath, ManageInventory_FormTxt_Path, Copy_NewItem);
		Type(Xpath, ManageInventory_txtDescription_Path, "QA TEST");
		Type(Xpath, ManageInventory_txtRevisionDate_Path, "0" + Get_Todaydate("MM/YYY"));
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_ddInventoryType_Path, "value").equalsIgnoreCase("POD"),
				"POD Not Displayed");
		Click(Xpath, ManageInventory_ProductCategory_Arrow_Path);
		Thread.sleep(2000);
		Hover(Xpath, ManageInventory_ProductCategory_Marketing_Path);
		Click(Xpath, ManageInventory_ProductCategory_Marketing_Path);
		Click(Xpath, ManageInventory_FormOwner_Arrow_Path);
		Thread.sleep(2000);
		Hover(Xpath, ManageInventory_FormOwner_Broker_Dealer_Path);
		Click(Xpath, ManageInventory_FormOwner_Broker_Dealer_Path);
		Click(Xpath, ManageInventory_BusinessOwner_Arrow_Path);
		Thread.sleep(2000);
		Hover(Xpath, ManageInventory_BusinessOwner_Annuity_Path);
		Click(Xpath, ManageInventory_BusinessOwner_Annuity_Path);
		Click(Xpath, ManageInventory_Viewability_Arrow_Path);
		Thread.sleep(2000);
		Hover(Xpath, ManageInventory_Orderable_Path);
		Click(Xpath, ManageInventory_Orderable_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, ManageInventory_chkFulfillment_Path),
				" the checkbox is not checked off for 'Fulfillment' in 'View In' mode field");
		Click(Xpath, ManageInventory_All_Path);
		Click(Xpath, ManageInventory_Rules_Save_Btn_Path);
		Click(Xpath, ManageInventory_Rules_OK_Btn_Path);
		Click(Xpath, ManageInventory_RulesTab_Path);
		Type(Xpath, ManageInventory_Rules_ObsoleteDate_Path, Get_Futuredate("MM/DD/YYYY"));
		Click(Xpath, ManageInventory_Rules_Save_Btn_Path);
		Assert.assertTrue(
				Get_Text(Xpath, ManageInventory_Rules_Save_message_Path).equalsIgnoreCase("Successfully Saved!"),
				"Successfully Saved! is not present");
		Click(Xpath, ManageInventory_Rules_OK_Btn_Path);

	}

	@Test(enabled = true, priority = 3)
	public void GATC_2_4_7_1_3() throws IOException, InterruptedException {

		// Validate that you can copy from the create new item grid

		String number = Get_Todaydate("MMddYYY_HHmmss");
		String Copy_NewItem = Part7 + number;
		Manage_Inventory_Menu_Hover();
		Type(Xpath, ManageInventory_FormNoToCopy_Path, Part7);
		Thread.sleep(2000);
		Click(Xpath, ManageInventory_Copy_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Title_Path),
				"Manage Inventory page is not Displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_General_Path),
				"Manage Inventory - General Tab is not displayed is not Displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_TitlePart_Path),
				"Copied Part name is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("span", "Copy Of QA_TESTNEWITEM_X")),
				"Copy of QA_ is not displayed");
		Type(Xpath, ManageInventory_FormTxt_Path, Copy_NewItem);
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_txtDescription_Path).equalsIgnoreCase("qa_testnewitem_x"),
				"'qa_testnewitem_x' is not Pre-Populated in the Item Description field");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_txtRevisionDate_Path),
				"Revision field is not displayed is not Displayed in the Manage Inventory General Tab");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_txtRevisionDate_Path, "value").equalsIgnoreCase("12/2016"),
				"'Revision Date' field is pre-populated with <Revision Date> from original part");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_txtPredecessor_Path, "value").equalsIgnoreCase(Part7),
				"'Predecessor' field is not pre-populated with 'QA_TESTNEWITEM_X' from original part");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_InventoryTypeInput_Path, "Value").equalsIgnoreCase("POD"),
				"'Inventory Type' field is pre-populated with 'POD' from original part");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_ProductCategory_Input_Path, "Value").equalsIgnoreCase("Marketing"),
				"'Category' field is pre-populated with 'Marketing' from original part");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_FormOwner_Input_Path, "Value")
						.equalsIgnoreCase("Broker Dealer/Operations"),
				"'Form Owner' field is pre-populated with 'Broker Dealer/Operations' from original part");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_BusinessOwner_Input_Path, "Value").equalsIgnoreCase("Annuity"),
				"'Business Owner' field is pre-populated with 'Annuity' from original part");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Viewability_Input_Path),
				"Viewablility field is not displayed is not Displayed in the Manage Inventory General Tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_chkFulfillment_Path),
				"Fulfillment radio button is not displayed is not Displayed in the Manage Inventory General Tab");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_All_Path, "class").equalsIgnoreCase("rtChecked"),
				" All checkbox is not checked off");
		Select_lidropdown(ManageInventory_Viewability_Arrow_Path, Xpath, "Not Viewable");
		Click(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Click(Xpath, ManageInventory_Save_OK_Btn_Path);
		Reporter.log("Copied New part is '" + Copy_NewItem + "'");
		Manage_Inventory_Menu_Hover();
		Type(Xpath, ManageInventory_FormNo_Path, Copy_NewItem);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_Resultspart_Path).equalsIgnoreCase(Copy_NewItem),
				Copy_NewItem + " is not displayed in the 'Search Results' section");
		Click(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Click(Xpath, ManageInventory_RulesTab_Path);
		Type(Xpath, ManageInventory_Rules_ObsoleteDate_Path, Get_FutureTime("MM/dd/YYYY HH:mm:ss"));
		Click(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Click(Xpath, ManageInventory_Save_OK_Btn_Path);
		Reporter.log("Copied New part '" + Copy_NewItem + "' is Obsoleted ");
		Click(Xpath, Logout_Path);
		login();
	}

	@Test(enabled = false, priority = 4)
	public void GATC_2_4_7_1_4() throws IOException, InterruptedException {

		// Validate that a part can be copied from the search results table

		String number = Get_Todaydate("MMddYYY_HHmmss");
		String Copy_NewItem = QA_COPY_SEARCHRESULTS_X + number;
		Manage_Inventory_Menu_Hover();
		Type(Xpath, ManageInventory_FormNo_Path, QA_COPY_SEARCHRESULTS_X);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		Click(Xpath, ManageInventory_Result_copy_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Result_copy_PopupOK_Path),
				"Ok Button on pop up is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Result_copy_PopupOK_Path),
				"Ok Button on pop up is not displayed");
		Click(Xpath, ManageInventory_Result_copy_PopupOK_Path);
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_TitlePart_Path).contains("Copy Of QA_COPY_SEARCHRESULTS"),
				"Title Field Text not matching");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_txtRevisionDate_Path, "value").equalsIgnoreCase("12/2016"),
				"Revision Date is not matching");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_txtPredecessor_Path, "value")
				.equalsIgnoreCase(QA_COPY_SEARCHRESULTS_X), "Predecessor is not matching");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_ddInventoryType_Path, "value").equalsIgnoreCase("POD"),
				"Inventory Type is not matching");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_ProductCategory_Input_Path, "Value").equalsIgnoreCase("Marketing"),
				"'Category' field is pre-populated with 'Marketing' from original part");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_FormOwner_Input_Path, "Value")
						.equalsIgnoreCase("Broker Dealer/Operations"),
				"'Form Owner' field is pre-populated with 'Broker Dealer/Operations' from original part");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_BusinessOwner_Input_Path, "Value").equalsIgnoreCase("Annuity"),
				"'Business Owner' field is pre-populated with 'Annuity' from original part");
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Viewability_Input_Path),
				"Viewablility field is not displayed is not Displayed in the Manage Inventory General Tab");
		Type(Xpath, ManageInventory_FormTxt_Path, Copy_NewItem);
		Select_lidropdown(ManageInventory_Viewability_Arrow_Path, Xpath, "Not Viewable");
		Click(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Save_message_Path),
				"Successfully Saved! pop up is not displayed");
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_Save_message_Path).equalsIgnoreCase("Successfully Saved!"),
				"Successfully Saved! pop up message is dispalyed incorrectly");
		Click(Xpath, ManageInventory_Save_OK_Btn_Path);
		Reporter.log("Copied New part is '" + Copy_NewItem + "'");
		Manage_Inventory_Menu_Hover();
		Type(Xpath, ManageInventory_FormNo_Path, Copy_NewItem);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_Resultspart_Path).equalsIgnoreCase(Copy_NewItem),
				Copy_NewItem + " is not displayed in the 'Search Results' section");
		Click(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Click(Xpath, ManageInventory_RulesTab_Path);
		Type(Xpath, ManageInventory_Rules_ObsoleteDate_Path, Get_FutureTime("MM/dd/YYYY HH:mm:ss"));
		Click(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, ManageInventory_Save_message_Path),
				"Successfully Saved! pop up is not displayed");
		Assert.assertTrue(Get_Text(Xpath, ManageInventory_Save_message_Path).equalsIgnoreCase("Successfully Saved!"),
				"Successfully Saved! pop up message is dispalyed incorrectly");
		Click(Xpath, ManageInventory_Save_OK_Btn_Path);
		Reporter.log("Copied New part '" + Copy_NewItem + "' is Obsoleted ");
		Click(Xpath, Logout_Path);
		login();

	}

	@Test(enabled = false, priority = 5)
	public void GATC_2_4_7_1_5() throws InterruptedException {

		// Ensure all the Search items fields are working and appropriate search
		// results appear

		Manage_Inventory_Menu_Hover();
		Type(Xpath, ManageInventory_FormNo_Path, Part8);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(Xpath, MI_Searchresult_FormNo).equalsIgnoreCase(Part8),
				Part8 + " is not displayed in the search results");
		Click(Xpath, ManageInventory_Clear_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, MI_Searchresult_Norecords);
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_FormNo_Path, "value").isEmpty(),
				"'Form #' field is not cleared");
		Type(Xpath, ManageInventory_ItemDescription_Path, "QATEST");
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(Xpath, MI_Searchresult_ItemDEsc).contains("QATEST"),
				"QATEST is not displayed in the Item Description search results");
		Click(Xpath, ManageInventory_Clear_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, MI_Searchresult_Norecords);
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_ItemDescription_Path, "value").isEmpty(),
				"'Item Description' field is not cleared");
		Type(Xpath, ManageInventory_Keyword_Path, "QA");
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Click(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_txtKeyWords_Path, "value").contains("QA"),
				"'QA' is not displayed in the Keyword field");
		Hover(Xpath, Admin_btn_Path);
		Click(Xpath, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Search_Btn_Path);
		Click(Xpath, ManageInventory_InventoryType_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("POD"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(Xpath, MI_Searchresult_Type).equals("POD"),
				"POD is not displayed in the Type search results");
		Click(Xpath, ManageInventory_Clear_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, MI_Searchresult_Norecords);
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_InventoryType_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set to Inventory type dropdown after cleared");
		Click(Xpath, ManageInventory_FormOwner_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Annuity/Marketing"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Click(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_FormOwner_Input_Path, "value")
						.equalsIgnoreCase("Annuity/Marketing"),
				"Annuity/Marketing is not displayed in the Form Owner dropdown of the selected Part");
		Manage_Inventory_Menu_Hover();
		Click(Xpath, ManageInventory_Category_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Annuity"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Click(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(Xpath, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, ManageInventory_Category_Anunity_Path),
				"Annuity is not selected under Category in General Tab for the selected part");
		Manage_Inventory_Menu_Hover();
		Click(Xpath, ManageInventory_UserGroups_Icon_Path);
		Thread.sleep(2000);
		Click(Xpath, li_value("Annuity "));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		Click(Xpath, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_usergroup_annuity_Path, "class").equalsIgnoreCase("rtChecked"),
				"Annuity radio button is not Selected in the Manage Inventory General Tab in User Category");
		Manage_Inventory_Menu_Hover();
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_Active_Path, "value").equalsIgnoreCase("Active"),
				"Active Dropdown is not 'Active' by default");
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ManageInventory_Resultsloadingpanel_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, MI_Searchresult_FormNo), "Form Number is not displayed");
		Assert.assertTrue(Get_Text(Xpath, MI_Searchresult_Status).equalsIgnoreCase("A"), "Status type mismatching");
		Click(Xpath, ManageInventory_Clear_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, MI_Searchresult_Norecords);
		Click(Xpath, ManageInventory_UnitsOnHand_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, ManageInventory_UnitsOnHand_Outofstock_Path);
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, ManageInventory_Resultsloadingpanel_Btn_Path);
		Assert.assertTrue(Get_Text(Xpath, MI_Searchresult_Onhand).equalsIgnoreCase("0"), "On-hand type mismatching");
		Click(Xpath, ManageInventory_Clear_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, MI_Searchresult_Norecords);
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_UnitsOnHand_Path, "value").equalsIgnoreCase("All"),
				"unit On-hand Dropdown mismatching");

	}

	@Test(enabled = false, priority = 6)
	public void GATC_2_4_7_1_6() throws InterruptedException {

		// Ensure all the Search items clear button is working and appropriate
		// search results appear

		Manage_Inventory_Menu_Hover();
		Type(Xpath, ManageInventory_FormNo_Path, Part1);
		Type(Xpath, ManageInventory_ItemDescription_Path, "QA Test");
		Type(Xpath, ManageInventory_Keyword_Path, "QA");
		Click(Xpath, ManageInventory_InventoryType_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("POD"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Category_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Annuity"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_FormOwner_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Annuity/Marketing"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_BusinessOwner_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Annuity"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_UserGroups_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("ALL"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Active_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Inactive"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_UnitsOnHand_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("Out of Stock"));
		Thread.sleep(1500);
		Click(Xpath, ManageInventory_Clear_Btn_Path);
		ExplicitWait_Element_Visible(Xpath, MI_Searchresult_Norecords);
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_FormNo_Path, "value").isEmpty(),
				"'Form #' field is not cleared");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_ItemDescription_Path, "value").isEmpty(),
				"'Item Description' field is not cleared");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_Keyword_Path, "value").isEmpty(),
				"'Keyword' field is not cleared");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_InventoryType_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set to Inventory type dropdown after cleared");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_FormOwner_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set to Form Owner dropdown after cleared");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_Category_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set to Category dropdown after cleared");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_BusinessOwner_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set to BusinessOwner dropdown after cleared");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_Active_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set default value for Acive dropdown after cleared");
		Assert.assertTrue(
				Get_Attribute(Xpath, ManageInventory_UserGroups_Path, "value").equalsIgnoreCase(MI_Defaultdropdown),
				MI_Defaultdropdown + " is not set to UserGroups dropdown after cleared");
		Assert.assertTrue(Get_Attribute(Xpath, ManageInventory_UnitsOnHand_Path, "value").equalsIgnoreCase("All"),
				"All is not set to UnitsOnHand dropdown after cleared");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
