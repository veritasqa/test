package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.globalatlantic.Base.BaseTest;

public class Ful_SubmitCancelOrder extends BaseTest {

	@Test(enabled = true, priority = 1)
	public void GA_TC_2_9_1_2_1() throws InterruptedException {

		// Validate an order is placed
		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part1);
		Click(locatorType, Fullfilment_Search_Btn_Path);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part1), Part1 + "is not displayed");

		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(3000);

		Assert.assertTrue(Get_Text(locatorType, Add_Cart_Message_Path).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");

		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path);

		PlaceanOrder("GTC_2_6_1_2_1");
	}

	@Test(priority = 2, enabled = true, dependsOnMethods = { "GA_TC_2_9_1_2_1" })
	public void GA_TC_2_9_1_2_2() {

		// Validate on confirmation page submit a cancel request and verify a
		// support ticket is create

		Click(locatorType, Submit_Cancel_Req_Btn_Path);
		Accept_Alert();
		Assert.assertTrue(Element_Is_Present(locatorType, OrderCancel_Popup_Path),
				"Order cancel Popup Message is not displayed");
		Click(locatorType, OrderCancel_Popup_Close_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Announcement_Heading_Path), "Home Page is not Displayed");
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Support_Path);
		ExplicitWait_Element_Clickable(locatorType, SupportTicket_Edit_Btn_Path);
		Assert.assertTrue(Get_Text(locatorType, FirstRow_SubCategory_Path).equalsIgnoreCase("Cancel Order"),
				"Submitted Cancel Order is not appeared in first row on Support : View Tickets' Page");
		Assert.assertTrue(Element_Is_Present(locatorType, FirstRow_TicketNumber_Path),
				"Ticket number is not Listed in the UI");
		TicketNumber = Get_Text(locatorType, FirstRow_TicketNumber_Path);
		System.out.println("Ticket Number is " + TicketNumber);
		Reporter.log("Ticket Number is " + TicketNumber);
	}

	@Test(priority = 3, enabled = true, dependsOnMethods = { "GA_TC_2_9_1_2_2" })

	public void GA_TC_2_9_1_2_3() {

		// Validate cancel request ticket appears on support page

		Assert.assertTrue(Element_Is_Present(locatorType, FirstRow_OrderNumber_Path),
				"Order number is not Listed in the UI");
		Assert.assertTrue(Get_Text(locatorType, FirstRow_OrderNumber_Path).equalsIgnoreCase(OrderNumber),
				"Order Number is mismatching in the first row");
		Assert.assertTrue(Get_Text(locatorType, FirstRow_SubCategory_Path).equalsIgnoreCase("Cancel Order"),
				"Cancel Order is not displayed in the Sub Category");
		Assert.assertTrue(Get_Text(locatorType, FirstRow_Status_Path).equalsIgnoreCase("New"),
				"New is not displayed in the Status");
	}

	@Test(priority = 4, enabled = true, dependsOnMethods = { "GA_TC_2_9_1_2_3" })

	public void GA_TC_2_9_1_2_4() {
		// Verify cancel request ticket works appropriately

		Click(locatorType, SupportTicket_Edit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, UpdateTicket_Btn_Path);
		Select_DropDown(locatorType, Status_Dropdown_Path, "Closed");
		Click(locatorType, AddComment_Btn_Path);
		Type(locatorType, Comment_Textfield_Path, "QA Test");
		Click(locatorType, CreateComment_btn_Path);
		Click(locatorType, UpdateTicket_Btn_Path);
		Click(locatorType, Ticketupdate_Popup_OK_Path);
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Support_Path);
		ExplicitWait_Element_Clickable(locatorType, SupportTicket_Edit_Btn_Path);
		Assert.assertFalse(Get_Text(locatorType, FirstRow_TicketNumber_Path).equalsIgnoreCase(TicketNumber),
				"Ticket Number is displayed");
		Type(locatorType, Status_Text_Path, "Closed");
		Click(locatorType, Status_Filter_Path);
		ExplicitWait_Element_Clickable(locatorType, Status_Contains_Path);
		Click(locatorType, Status_Contains_Path);
		Assert.assertTrue(Get_Text(locatorType, FirstRow_TicketNumber_Path).equalsIgnoreCase(TicketNumber),
				"Ticket Number not is displayed");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		// logout();

	}

}
