package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.globalatlantic.Base.BaseTest;

public class Traditional_Life extends BaseTest {

	public SoftAssert softAssert1;

	public String TLsearchpart(String Partname) {

		return ".//*[contains(text(),'" + Partname + "')]";
	}

	public void EnterCaptcha() {

		Type(locatorType, TL_CO_Captchafield_Path, captcha());
		Click(locatorType, TL_CO_CaptchaValidatebtn_Path);

		while (Element_Is_Displayed(locatorType, TL_CO_Captchaerror_Path)) {

			Type(locatorType, TL_CO_Captchafield_Path, captcha());
			Click(locatorType, TL_CO_CaptchaValidatebtn_Path);

		}

	}

	@Test(enabled = true, priority = 1)
	public void GA_TL_TC_1_0_1_1() {

		// Validate Login Link/URL is taking user on TL home page

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Logo_Path),
				"Global Atlantic Financial Group is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_HomeBtn_Path),
				"Global Atlantic Financial Group is not displayed");
		softAssert1.assertAll();
	}

	@Test(enabled = true, priority = 2)
	public void GA_TL_TC_2_0_1_1() {

		// Validate landing page for TL User

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Contactus_Path), "Contact Us is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_ClearCart_Path), "Clear Cart is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Announcementstitle_Path),
				"Announcements is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OrderMaterialTitle_Path),
				"Order Material is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Shoppingcart_Path),
				"Shopping cart is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Myrecentorder_Path),
				"My recent order is not displayed");
		Click(locatorType, TL_Contactus_Path);
		softAssert1.assertAll();
	}

	@Test(enabled = true, priority = 3)
	public void GA_TL_TC_2_1_1_1() {

		// Validate Click "Here" button takes user to Ipipeline Documentation

		TL_login();
		// System.out.println(Get_Title());
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_ClickHerelink_Path),
				"Click 'Here' Link is not displays under 'Annoucements'.");
		Click(locatorType, TL_ClickHerelink_Path);
		Switch_New_Tab();
		ExplicitWait_Element_Visible(locatorType, TL_Pdfpage_Path);
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), "PDF page is not displayed");
		Switch_Old_Tab();

	}

	@Test(enabled = true, priority = 4)
	public void GA_TL_TC_2_3_1_1() throws InterruptedException {

		// Validate State dropdown is working fine

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OrderMaterialTitle_Path),
				"Order Material is not displayed");
		Click(locatorType, TL_SearchBtn_Path);
		ExplicitWait_Element_Visible(locatorType, TL_StateDropdownreqmsg_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_StateDropdownreqmsg_Path),
				"'Required' error message is not displayd under 'State' drop down.");
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartName1_Path),
				"Appropriate results is not displays for IL");
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "California");
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartName1_Path),
				"Appropriate results is not displays for CA");
		Type(locatorType, TL_Searchmaterialfield_Path, Part9);
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Get_Text(locatorType, TL_PartName1_Path).trim().equalsIgnoreCase(Part9),
				"'QA_TraditionalLife_1' is not displayed ");
		Click(locatorType, TL_Clearbtn_Path);
		softAssert1.assertAll();
	}

	@Test(enabled = true, priority = 5)
	public void GA_TL_TC_2_3_1_2() throws InterruptedException {

		// Validate Product Search Results appear based on Category

		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_Categorydd_Path, "Marketing");
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartName1_Path),
				"Appropriate results is not displays for Marketing");
		Click(locatorType, TL_Clearbtn_Path);
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_Categorydd_Path, "New Business");
		Type(locatorType, TL_Searchmaterialfield_Path, Part9);
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Get_Text(locatorType, TL_PartName1_Path).trim().equalsIgnoreCase(Part9),
				"'QA_TraditionalLife_1' is not displayed ");
		Click(locatorType, TL_Clearbtn_Path);
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 6)
	public void GA_TL_TC_2_3_1_3() throws InterruptedException {

		// Validate Product Search Results appear based on Sub Category

		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_SubCategorydd_Path, "Lifetime Assure (UL)");
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartName1_Path),
				"Appropriate results is not displays for Lifetime Assure(UL)");
		Click(locatorType, TL_Clearbtn_Path);
		softAssert1.assertAll();
	}

	@Test(enabled = true, priority = 7)
	public void GA_TL_TC_2_3_1_4() throws InterruptedException {

		// Validate Product Search Results appear based on State, Category and
		// Sub Category

		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_Categorydd_Path, "New Business");
		Select_DropDown_VisibleText(locatorType, TL_SubCategorydd_Path, "Lifetime Assure (UL)");
		Type(locatorType, TL_Searchmaterialfield_Path, Part9);
		Click(locatorType, TL_SearchBtn_Path);
		softAssert1.assertTrue(Get_Text(locatorType, TL_PartName1_Path).trim().equalsIgnoreCase(Part9),
				Part9 + " is not displayed ");
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 7)
	public void GA_TL_TC_2_3_1_8() throws InterruptedException {

		// Validate max quantity

		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part42);
		Click(locatorType, TL_SearchBtn_Path);
		Assert.assertTrue(Get_Text(locatorType, TL_PartName1_Path).trim().equalsIgnoreCase(Part42),
				Part42 + " is not displayed ");
		Type(locatorType, TL_Partqtyfield1_Path, "6");
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_PartMaxordererror1_Path),
				"Max Quantity Error! is not displayed");
		Type(locatorType, TL_Partqtyfield1_Path, "1");
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Click(locatorType, TL_Shoppingcartcheckout_Path);

	}

	@Test(enabled = true, priority = 8)
	public void GA_TL_TC_2_4_1_1() throws InterruptedException, IOException {

		// Validate max quantity in Shopping Cart widget
		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part9);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Type(locatorType, TL_Searchmaterialfield_Path, Part47);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartSuccessfullyadded1_Path),
				"Successfully Added1! is not displayed " + Part47);
		Type(locatorType, TL_Searchmaterialfield_Path, Part14);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		ExplicitWait_Element_Visible(locatorType, TL_PartSuccessfullyadded1_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartSuccessfullyadded1_Path),
				"Successfully Added1! is not displayed " + Part14);
		softAssert1.assertTrue(Get_Text(locatorType, TL_ShoppingCartPart1_Path).trim().equalsIgnoreCase(Part9),
				Part9 + " is not displayed in the shopping cart ");
		softAssert1.assertTrue(Get_Attribute(locatorType, TL_ShoppingCartPart1qty_Path, "value").equals("1"),
				"1 is not displayed for " + Part9);
		softAssert1.assertTrue(Get_Text(locatorType, TL_ShoppingCartPart2_Path).trim().equalsIgnoreCase(Part47),
				Part47 + " is not displayed in the shopping cart ");
		softAssert1.assertTrue(Get_Attribute(locatorType, TL_ShoppingCartPart2qty_Path, "value").equals("1"),
				"1 is not displayed for " + Part47);
		softAssert1.assertTrue(Get_Text(locatorType, TL_ShoppingCartPart3_Path).trim().equalsIgnoreCase(Part14),
				Part14 + " is not displayed in the shopping cart ");
		softAssert1.assertAll();

		Type(locatorType, TL_ShoppingCartPart3qty_Path, "5");
		Type(locatorType, TL_ShoppingCartPart1qty_Path, "3");
		Type(locatorType, TL_ShoppingCartPart2qty_Path, "6");
		Click(locatorType, TL_Shoppingcartupdate_Path);
		softAssert1.assertTrue(Get_Attribute(locatorType, TL_ShoppingCartPart3qty_Path, "value").equals("5"),
				"5 is not updated for " + Part14);
		softAssert1.assertTrue(Get_Attribute(locatorType, TL_ShoppingCartPart1qty_Path, "value").equals("3"),
				"3 is not updated for " + Part47);
		softAssert1.assertTrue(Get_Attribute(locatorType, TL_ShoppingCartPart2qty_Path, "value").equals("6"),
				"6 is not updated for " + Part9);
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 9, dependsOnMethods = "GA_TL_TC_2_4_1_1")
	public void GA_TL_TC_2_4_1_2() {

		// Validate Checkout Button takes user on shopping Information page.

		Click(locatorType, TL_Shoppingcartcheckout_Path);
		EnterCaptcha();
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Shippinginfo_Path), "Checkout Page is not displayed");
	}

	@Test(enabled = true, priority = 10)
	public void GA_TL_TC_2_5_1_1() throws IOException, InterruptedException {

		// Validate upon clicking on Order number takes user to Order
		// Confirmation page

		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		PlaceTLorder("GA_TL_TC_2_5_1_1");
		Click(locatorType, TL_HomeBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_MyRecentorder_Path),
				"My Recent Order Widget is not displayed");
		Click(locatorType, TL_MyRecentorder1_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_OrderConfirmation_Path),
				"Order Confirmation Page is not displayed");
		Click(locatorType, TL_OCP_Continueshoppingbtn_Path);
		softAssert1.assertAll();
		Cancelsingleorder(OrderNumber);

	}

	@Test(enabled = true, priority = 11)
	public void GA_TL_TC_2_5_1_2() {

		// Validate Clear Order button is displaying fine for My Recent Orders
		// widget

		TL_login();
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_MyrecentorderClearorder_Path),
				"Clear Order button is not displayed in the My Recent Order widget");
		Click(locatorType, TL_MyrecentorderClearorder_Path);
	}

	@Test(enabled = true, priority = 12)
	public void GTC_2_7_1_1_1() throws IOException, InterruptedException {

		// Validate an order is placed
		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part9);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		softAssert1.assertTrue(Get_Text(locatorType, TL_ShoppingCartPart1_Path).trim().equalsIgnoreCase(Part9),
				Part9 + " is not displayed in the shopping cart ");
		Click(locatorType, TL_Shoppingcartcheckout_Path);
		EnterCaptcha();
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Shippinginfo_Path),
				"Shipping Information Page is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_ProduceId_Path), " field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Name_Path), "Name field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Company_Path), "Company field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Address1_Path),
				"Addess1 field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Address2_Path),
				"Addess2 field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Address3_Path),
				"Addess3 field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_City_Path), "City field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_State_Path), "State field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Zip_Path), "Zip field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Countrydd_Path),
				"Country field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Phone_Path), "Phone field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Email_Path), "Email field is not displayed");
		softAssert1.assertAll();
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Nameasterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for Name field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Address1asterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for Address1 field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Cityasterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for City field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Stateasterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for State field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Zipasterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for Zip field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Countryasterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for Country field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Emailasterisk_Path).trim(), "*",
				"Red asterisk is not dispayed for Email field");
		Click(locatorType, TL_CO_Confirmaddressbtn_Path);
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Nameerrmsg_Path).trim(), "Required",
				"Required is not dispayed for Name field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Address1errmsg_Path).trim(), "Required",
				"Red asterisk is not dispayed for Address1 field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Cityerrmsg_Path).trim(), "Required",
				"Red asterisk is not dispayed for City field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Stateerrmsg_Path).trim(), "Required",
				"Red asterisk is not dispayed for State field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Ziperrmsg_Path).trim(), "Required",
				"Red asterisk is not dispayed for Zip field");
		softAssert1.assertEquals(Get_Text(locatorType, TL_CO_Emailerrmsg_Path).trim(), "Required",
				"Red asterisk is not dispayed for Email field");
		softAssert1.assertAll();
		Type(locatorType, TL_CO_ProduceId_Path, "Test");
		Type(locatorType, TL_CO_Name_Path, "QA Test");
		Type(locatorType, TL_CO_Address1_Path, Address1);
		Type(locatorType, TL_CO_City_Path, City);
		Type(locatorType, TL_CO_State_Path, "IL");
		Type(locatorType, TL_CO_Zip_Path, "60089");
		Type(locatorType, TL_CO_Email_Path, EmailId);
		Click(locatorType, TL_CO_Confirmaddressbtn_Path);
		ExplicitWait_Element_Clickable(locatorType, TL_CO_Placeorderbtn_Path);
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_ProduceId_Path),
				"Producer ID field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Name_Path), "Name field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Address1_Path),
				"Address1 field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Address2_Path),
				"Address2 field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Address3_Path),
				"Address3 field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_City_Path), "City field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_State_Path), "State field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Zip_Path), "Zip field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Email_Path), "Email field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Countrydd_Path),
				"Country field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Company_Path), "Company field is not greyed out");
		softAssert1.assertFalse(Element_Is_Enabled(locatorType, TL_CO_Phone_Path), "Phone field is not greyed out");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_UPSGroundradio_Path),
				"UPS Ground Radio button is not displayed");
		softAssert1.assertAll();
		Click(locatorType, TL_CO_EditAddressbtn_Path);
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_ProduceId_Path),
				"Producer ID field is not ennabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Name_Path), "Name field is not ennabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Address1_Path), "Address1 field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Address2_Path), "Address2 field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Address3_Path), "Address3 field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_City_Path), "City field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_State_Path), "State field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Zip_Path), "Zip field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Email_Path), "Email field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Countrydd_Path), "Country field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Company_Path), "Company field is not enabled");
		softAssert1.assertTrue(Element_Is_Enabled(locatorType, TL_CO_Phone_Path), "Phone field is not enabled");
		softAssert1.assertAll();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Shoppingcart_Path),
				"Shopping Cart is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_PartQty1_Path),
				"Qty field is not displayed in shopping cart");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_PartUpdatebtn1_Path),
				"Update is not displayed in shopping cart");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_PartRemovebtn1_Path),
				"Remove field is not displayed in shopping cart");
		Type(locatorType, TL_CO_PartQty1_Path, "2");
		Click(locatorType, TL_CO_PartUpdatebtn1_Path);
		Click(locatorType, TL_CO_PartRemovebtn1_Path);
		Click(locatorType, TL_CO_Placeorderbtn_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Emptyshoppingcart_Path),
				"Shopping Cart is Empty ! is not displayed");
		softAssert1.assertAll();
		Click(locatorType, TL_HomeBtn_Path);
		PlaceTLorder("GTC_2_7_1_1_1");
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 13, dependsOnMethods = "GTC_2_7_1_1_1")
	public void GTC_2_7_1_1_2() {

		// Validate that order confirmation page appears

		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_OrderNo_Path),
				"Order number field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_StatusNew_Path),
				"Status field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Orderdate_Path),
				"Order Date field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Shippedto_Path),
				"Shipped to fielld is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Shippedby_Path),
				"Shipped by field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Shippedbydate_Path),
				"Shipped by date is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Contactus_Path),
				"Contact us link is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Formno_Path), "Form # column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Itemdesc_Path),
				"Item desc column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_State_Path), "State Column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Quantity_Path), "Qty column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Downloadbtn_Path),
				"Download button is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Continueshoppingbtn_Path),
				"Continue shopping is not displayed");
		Cancelsingleorder(OrderNumber);
		softAssert1.assertAll();
	}

	@Test(enabled = true, priority = 14)
	public void GTC_2_7_1_1_5() throws IOException, InterruptedException {

		// Validate Download only functionality is working appropriately
		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part10);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Click(locatorType, TL_Shoppingcartcheckout_Path);
		EnterCaptcha();
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Shippinginfo_Path),
				"Shipping Information Page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_CO_Downloadoption_Path),
				"Download Order option Path is not displayed");
		Click(locatorType, TL_CO_DownloadOnlybtn_Path);
		ExplicitWait_Element_Clickable(locatorType, TL_OCP_Continueshoppingbtn_Path);
		System.out.println(Get_Text(locatorType, TL_OCP_OrderNo_Path));
		Assert.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_OrderConfirmation_Path),
				"Order Confirmation page is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_OrderNo_Path),
				"Order number field is not displayed");
		softAssert1.assertEquals(Get_Text(locatorType, TL_OCP_StatusClosed_Path).trim().toLowerCase(), "closed",
				"Closed is not displayed in the status");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Orderdate_Path),
				"Order Date field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_OrderNo_Path),
				"Order number field is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Formno_Path), "Form # column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Itemdesc_Path),
				"Item desc column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_State_Path), "State Column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Quantity_Path), "Qty column is not displayed");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_OCP_Continueshoppingbtn_Path),
				"Continue shopping is not displayed");
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 15)
	public void GTC_2_6_1_3_1() throws IOException, InterruptedException {

		// Validate that in the shopping cart if you change qty to 0 and click
		// update the part is removed
		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Click(locatorType, TL_ClearCart_Path);
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part10);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Thread.sleep(500);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_PartSuccessfullyadded1_Path),
				"Successfully Added1! message is not displayed ");
		Type(locatorType, TL_ShoppingCartPart1qty_Path, "0");
		Click(locatorType, TL_Shoppingcartupdate_Path);
		softAssert1.assertFalse(Element_Is_Displayed(locatorType, TL_ShoppingCartPart1_Path),
				"QA_Adminonly is still displayed in the shopping cart");
		Click(locatorType, TL_ClearCart_Path);
		softAssert1.assertAll();
	}

	@Test(enabled = true, priority = 16)
	public void GTC_2_6_1_3_2() throws IOException, InterruptedException {

		// Validate when you click the clear cart button on the shopping cart
		// page the part is removed

		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part10);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Click(locatorType, TL_ClearCart_Path);
		Assert.assertFalse(Element_Is_Displayed(locatorType, TL_ShoppingCartPart1_Path),
				"QA_Adminonly is still displayed in the shopping cart");

	}

	@Test(enabled = true, priority = 17)
	public void GTC_2_8_1_1() throws IOException, InterruptedException {

		// Validates display of "Contact Us" on all pages of the site (Header)
		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Contactus_Path),
				"Contact us link is not displayed in the home page");
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Type(locatorType, TL_Searchmaterialfield_Path, Part10);
		Click(locatorType, TL_SearchBtn_Path);
		Click(locatorType, TL_PartAddtocartbtn1_Path);
		Click(locatorType, TL_Shoppingcartcheckout_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Contactus_Path),
				"Contact us link is not displayed in the shopping cart page");
		EnterCaptcha();
		Type(locatorType, TL_CO_Name_Path, "QA Test");
		Type(locatorType, TL_CO_Address1_Path, Address1);
		Type(locatorType, TL_CO_City_Path, City);
		Type(locatorType, TL_CO_State_Path, State);
		Type(locatorType, TL_CO_Zip_Path, Zipcode);
		Type(locatorType, TL_CO_Email_Path, EmailId);
		Click(locatorType, TL_CO_Confirmaddressbtn_Path);
		Click(locatorType, TL_CO_Placeorderbtn_Path);
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, TL_Contactus_Path),
				"Contact us link is not displayed in the Order Confirmation Page page");
		OrderNumber = Get_Text(locatorType, TL_OCP_OrderNo_Path);
		Reporter.log("Order Placed Successfully");
		Reporter.log("Order Number placed in method  GTC_2_9_1_1 is '" + OrderNumber + "'");
		ordernum.add(OrderNumber);
		Cancelsingleorder(OrderNumber);
		softAssert1.assertAll();
		// PlaceTLorder("GTC_2_6_1_2_1");

	}

	@Test(enabled = true, priority = 18)
	public void GA_TL_TC_2_3_1_5() throws	 IOException, InterruptedException {

		// Validate the filter for Client materials in TL
		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_Categorydd_Path, "New Business");
		Select_DropDown_VisibleText(locatorType, TL_SubCategorydd_Path, "Benefit Builder (COLI)");
		Select_DropDown_VisibleText(locatorType, TL_Addfilterdd_Path, "Client Materials");
		Type(locatorType, TL_Searchmaterialfield_Path, Part12);
		Click(locatorType, TL_SearchBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, TLsearchpart(Part12)),
				Part12 + " is not displayed in the search results");
	}

	@Test(enabled = true, priority = 19)
	public void GA_TL_TC_2_3_1_6() throws IOException, InterruptedException {

		// Validate the filter for Agent materials in TL

		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_Categorydd_Path, "Marketing");
		Select_DropDown_VisibleText(locatorType, TL_SubCategorydd_Path, "Lifetime Assure (UL)");
		Select_DropDown_VisibleText(locatorType, TL_Addfilterdd_Path, "Agent Materials");
		Click(locatorType, TL_SearchBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, TLsearchpart(Part13)),
				Part13 + " is not displayed in the search results");
	}

	@Test(enabled = true, priority = 20)
	public void GA_TL_TC_2_3_1_7() throws IOException, InterruptedException {

		// Validate the filter for "-Any-" materials in TL

		Close_Browser();
		Quit_Browser();
		openbrowser();
		TL_login();
		Select_DropDown_VisibleText(locatorType, TL_StateDropdown_Path, "Illinois");
		Select_DropDown_VisibleText(locatorType, TL_Categorydd_Path, "Agent Contracting");
		Select_DropDown_VisibleText(locatorType, TL_SubCategorydd_Path, "Benefit Builder (COLI)");
		Select_DropDown_VisibleText(locatorType, TL_Addfilterdd_Path, "-Any-");
		Click(locatorType, TL_SearchBtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, TLsearchpart(Part14)),
				Part14 + " is not displayed in the search results");
	}

	@BeforeTest(enabled = true)
	public void BeforTest() throws IOException, InterruptedException {
		TL_login();
	}

	@AfterMethod(enabled = true)
	public void AfteMethod() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void BefMethod() {
		softAssert1 = new SoftAssert();
	}

}
