package com.globalatlantic.regression1;


import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Logout extends BaseTest{
	
	
	
 @Test (priority=1)
public void GTC_2_10_1_1() throws InterruptedException, IOException{
	 
 
	Click_On_Element(locatorType,Home_btn_Path,driver);
    ExplicitWait_Element_Clickable(locatorType,Fullfilment_Search_Btn_Path,driver);
    softAssert.assertTrue(Is_Element_Present(locatorType,Logout_Path,driver));
	
	Click_On_Element(locatorType,Materials_btn_Path,driver);
    ExplicitWait_Element_Clickable(locatorType,Fullfilment_Search_Btn_Path,driver);
    softAssert.assertTrue(Is_Element_Present(locatorType,Logout_Path,driver));
	
	Click_On_Element(locatorType,Reports_btn_Path,driver);
    ExplicitWait_Element_Clickable(locatorType,Fullfilment_Search_Btn_Path,driver);
	softAssert.assertTrue(Is_Element_Present(locatorType,Logout_Path,driver));
	

     // On main Page
	Hover_Over_Element(locatorType,Admin_btn_Path,driver);
	// 
    Click_On_Element(locatorType,Manage_Inventory_Path,driver);
    ExplicitWait_Element_Clickable(locatorType,Fullfilment_Search_Btn_Path,driver);
    softAssert.assertTrue(Is_Element_Present(locatorType,Logout_Path,driver));
    softAssert.assertAll();
}
 
 
@Test (priority=2)
public void GTC_2_10_1_2() throws IOException, InterruptedException{
	
	Click_On_Element(locatorType,Home_btn_Path,driver);
	softAssert.assertTrue(Is_Element_Present(locatorType,Logout_Path,driver));
	Click_On_Element(locatorType,Logout_Path,driver);
	softAssert.assertTrue(Is_Element_Present(locatorType,Username_Path,driver));
	softAssert.assertTrue(Is_Element_Present(locatorType,Password_Path,driver));
	softAssert.assertTrue(Is_Element_Present(locatorType,Login_btn_Path,driver));
	softAssert.assertAll();
}
 
@BeforeClass(enabled = true)
public void BeforeClass() throws IOException, InterruptedException {
	login();

}



}
