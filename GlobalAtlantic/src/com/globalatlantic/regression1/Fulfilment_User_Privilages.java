package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfilment_User_Privilages extends BaseTest {

	// Verify the Part is available with a same user group as the user logged in
	@Test
	public void GATC_2_8_8_1() throws InterruptedException {

		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);

		Type(locatorType, ManageInventory_FormNo_Path, Part43);

		Click(locatorType, ManageInventory_Search_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);

		Click(locatorType, ManageInventory_FormOwner_Icon_Path);
		Click(locatorType, ManageInventory_FormOwner_ForethoughtCapitalFundingOperations);

		Click(locatorType, ManageInventory_Rules_Save_Btn_Path);
		Click(locatorType, ManageInventory_Rules_OK_Btn_Path);

		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Users_Path);

		Type(locatorType, MUser_Username, "qaauto");

		Click(locatorType, MUser_Username_Filter);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(locatorType, MUser_loading);
		Click(locatorType, MUser_Edit);

		if (!Element_Is_selected(locatorType, MUser_forethougtOption)) {

			Click(locatorType, MUser_forethougtOption);

		}

		Type(locatorType, Fullfilment_Search_Path, Part43);
		Click(locatorType, Fullfilment_Search_Btn_Path);

		ContactSearch(Firstname, Lastname);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part43), Part43 + "is not displayed");
	}

	// Verify the part is not available with a different user group as the user
	// logged in

	@Test
	public void GATC_2_8_8_2() {

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		// ogout();

	}

}
