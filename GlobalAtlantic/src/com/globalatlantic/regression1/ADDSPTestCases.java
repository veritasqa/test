package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class ADDSPTestCases extends BaseTest {
	@Test(priority = 1, enabled = true)

	public void GATC_2_4_1_1_1_1() {

		Hover(locatorType, Admin_btn_Path, driver);
		Hover(locatorType, Special_Projects_Path, driver);
		// changes
		Click(locatorType, Add_Spl_Projects_Path, driver);

		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Generaltab_Path, driver),
				"General Tab is not displayed");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Progresstab_Path, driver),
				"Progress Tab is not displayed");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Costtab_Path, driver),
				"Cost Tab is not displayed");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Enclosuretab_Path, driver),
				"Enclousre Tab is not displayed");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Historytab_Path, driver),
				"History Tab is not displayed");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_BusinessOwner_Path, driver),
				"BusinessOwner field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Jobtitle_Path, driver),
				"Job Title field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_JobType_Path, driver),
				"Job Type field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_description_Path, driver),
				"Description field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_CreatedBy_Path, driver),
				"Created by field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_CostCenter_Path, driver),
				"Costcenter field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_SplInstruction_Path, driver),
				"Special Instruction field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Inrush_Path, driver),
				"In Rush field is missing in General Tab");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Quoteneeded_Path, driver),
				"Quote neededo field is missing in General Tab");
		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = true)
	public void GATC_2_4_1_1_1_2() throws IOException, InterruptedException {

		// login();
		Clearcart();

		Hover(locatorType, Admin_btn_Path, driver);
		Hover(locatorType, Special_Projects_Path, driver);

		Click(locatorType, Add_Spl_Projects_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SplProjects_Createbtn_Path, driver);
		softAssert.assertTrue(
				Get_DropDown(locatorType, SplProjects_BusinessOwner_Path, driver).equalsIgnoreCase("Annuity"),
				"'Annuity' is not default value in Bussiness Owner");
		// System.out.println(Get_DropDown(locatorType,
		// SplProjects_BusinessOwner_Path, driver));
		// System.out.println(Get_Text(locatorType,
		// SplProjects_BusinessOwner_Path, driver));
		softAssert.assertTrue(Get_Attribute(locatorType, SplProjects_Jobtitle_Path, "value", driver).isEmpty(),
				"'Job Title is not displaye Empty'");
		softAssert.assertTrue(
				Get_DropDown(locatorType, SplProjects_JobType_Path, driver).equalsIgnoreCase("- Select -"),
				"'- Select -' is not default value in 'JobType'");
		softAssert.assertTrue(Get_Attribute(locatorType, SplProjects_description_Path, "value", driver).isEmpty(),
				"Description field is not empty");
		softAssert.assertTrue(
				Get_DropDown(locatorType, SplProjects_CostCenter_Path, driver).equalsIgnoreCase("- Select -"),
				"'- Select -' is not default value in 'CostCenter'");
		softAssert.assertTrue(Get_Attribute(locatorType, SplProjects_SplInstruction_Path, "value", driver).isEmpty(),
				"Special Instruction field is not empty");
		softAssert.assertFalse(Element_Isselected(locatorType, SplProjects_Inrush_Path, driver),
				"'Is Rush radio button is checked'");
		Click(locatorType, SplProjects_Inrush_Path, driver);
		softAssert.assertFalse(Element_Isselected(locatorType, SplProjects_Quoteneeded_Path, driver),
				"'Is Rush radio button is checked'");
		Click(locatorType, SplProjects_Quoteneeded_Path, driver);
		Click(locatorType, SplProjects_Createbtn_Path, driver);

		ExplicitWait_Element_Visible(locatorType, SplProjects_Errtitle_Path, driver);

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errtitle_Path, driver),
				SPPErrorTilte + " Message is not displayed");
		// softAssert.assertTrue(Get_Text(locatorType,
		// SplProjects_Errtitle_Path,
		// driver).trim().equalsIgnoreCase(SPPErrorTilte),SPPErrorTilte+"
		// Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errjobtitle_Path, driver),
				SPPErrorJobTilte + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errjobtitle_Path, driver).trim().equalsIgnoreCase(SPPErrorJobTilte),
				SPPErrorJobTilte + " Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errjobtype_Path, driver),
				SPPErrorJobType + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errjobtype_Path, driver).trim().equalsIgnoreCase(SPPErrorJobType),
				SPPErrorTilte + " Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errcostcenter_Path, driver),
				SPPErrorCC + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errcostcenter_Path, driver).trim().equalsIgnoreCase(SPPErrorCC),
				SPPErrorCC + " Message is not displayed");

		softAssert.assertAll();

		Hover_Over_Element(locatorType, Admin_btn_Path, driver);
		Hover_Over_Element(locatorType, Special_Projects_Path, driver);

		Click(locatorType, Add_Spl_Projects_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SplProjects_Createbtn_Path, driver);

		Select_DropDown_VisibleText(locatorType, SplProjects_JobType_Path, "Color Print Request", driver);
		Click(locatorType, SplProjects_Createbtn_Path, driver);

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errtitle_Path, driver),
				SPPErrorTilte + " Message is not displayed");
		// softAssert.assertTrue(Get_Text(locatorType,
		// SplProjects_Errtitle_Path,
		// driver).trim().equalsIgnoreCase(SPPErrorTilte),SPPErrorTilte+"
		// Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errjobtitle_Path, driver),
				SPPErrorJobTilte + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errjobtitle_Path, driver).trim().equalsIgnoreCase(SPPErrorJobTilte),
				SPPErrorJobTilte + " Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errcostcenter_Path, driver),
				SPPErrorCC + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errcostcenter_Path, driver).trim().equalsIgnoreCase(SPPErrorCC),
				SPPErrorCC + " Message is not displayed");

		Type(locatorType, SplProjects_Jobtitle_Path, "QA", driver);
		Select_DropDown_VisibleText(locatorType, SplProjects_JobType_Path, "- Select -", driver);

		Click(locatorType, SplProjects_Createbtn_Path, driver);

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Jobtypefielderr_Path, driver),
				SPPErrorJobType + " is not displayed by 'Job Type' field");
		softAssert.assertTrue(Get_Text(locatorType, SplProjects_Jobtypefielderr_Path, driver).trim()
				.equalsIgnoreCase(SPPErrorJobType), SPPErrorJobType + " is not displayed by 'Job Type' field");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_CCfielderr_Path, driver),
				SPPErrorCC + " is not displayed by 'Cost Center' field");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_CCfielderr_Path, driver).trim().equalsIgnoreCase(SPPErrorCC),
				SPPErrorCC + " is not displayed by 'Cost Center' field");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errtitle_Path, driver),
				SPPErrorTilte + " Message is not displayed");
		// softAssert.assertTrue(Get_Text(locatorType,
		// SplProjects_Errtitle_Path,
		// driver).trim().equalsIgnoreCase(SPPErrorTilte),SPPErrorTilte+"
		// Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errjobtype_Path, driver),
				SPPErrorJobType + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errjobtype_Path, driver).trim().equalsIgnoreCase(SPPErrorJobType),
				SPPErrorTilte + " Message is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_Errcostcenter_Path, driver),
				SPPErrorCC + " Message is not displayed");
		softAssert.assertTrue(
				Get_Text(locatorType, SplProjects_Errcostcenter_Path, driver).trim().equalsIgnoreCase(SPPErrorCC),
				SPPErrorCC + " Message is not displayed");

		Click(locatorType, SplProjects_Progresstab_Path, driver);

		Assert.assertFalse(Element_Is_Displayed(locatorType, SP_ProgTab_AddCommenticon_Path, driver),
				"User can able to process to 'Progress tab' without completing General tab");

		softAssert.assertAll();

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
