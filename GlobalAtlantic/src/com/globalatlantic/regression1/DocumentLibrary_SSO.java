package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.globalatlantic.Base.BaseTest;

public class DocumentLibrary_SSO extends BaseTest {

	public SoftAssert softAssert1;

	public void Navigate_DocLib() {
		Click(locatorType, Document_lib_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Doclib_Searchbtn_Path);

	}

	public void Manage_Inventory_Menu_Hover() {
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);

	}

	@Test(enabled = true)
	public void GA_TC_2_5_1_8() throws IOException, InterruptedException {

		// SSO-
		// Validate Pieces displayed in document library when
		// 1.Product line type as Broker Dealer
		// 2.Product Line as SecureFore 5
		// 3.Product Line Category is New Business
		// 4.View in as Document Library is Checked

		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Part25);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		if (Element_Is_selected(locatorType, ManageInventory_chkDocumentLibrary_Path)) {
		} else {
			Click(locatorType, ManageInventory_chkDocumentLibrary_Path);
			Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
			Click(locatorType, ManageInventory_Save_OK_Btn_Path);
			Click(locatorType, Logout_Path);
		}
		// Test starts here

		SSOPage_Login();
		Select_li_Dropdown(Doclib_ProductLineType_arrow__Path, "Broker Dealer");
		Select_li_Dropdown(Doclib_State_arrow_Path, "Illinois");
		Select_li_Dropdown(Doclib_ProductLine_arrow_Path, "SecureFore 5");
		Click(locatorType, Doclib_Searchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Doclib_Partsview("New Business", Part25)),
				Part25 + " is not displayed under 'New Business'");
	}

	@Test(enabled = true)
	public void GA_TC_2_5_1_9() throws IOException, InterruptedException {

		// SSO
		// Validate Piece is not displayed in document library when View in is
		// unchecked

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		login();
		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Part25);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		softAssert1.assertTrue(Get_Attribute(locatorType, ManageInventory_ProductLineType_List_Input_Path, "value")
				.equalsIgnoreCase("Broker Dealer"), "'Broker Dealer' is not displayed in Product Line Type");
		softAssert1.assertTrue(Get_Attribute(locatorType, ManageInventory_ProductCategory_Input_Path, "value")
				.equalsIgnoreCase("New Business"), "'New Business' is not displayed in Category");
		softAssert1.assertTrue(Get_Attribute(locatorType, ManageInventory_Viewability_Input_Path, "value")
				.equalsIgnoreCase("Orderable"), "'Orderable' is not displayed in Viewability");
		softAssert1.assertAll();
		if (Element_Is_selected(locatorType, ManageInventory_chkDocumentLibrary_Path)) {

			Click(locatorType, ManageInventory_chkDocumentLibrary_Path);
			Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
			Click(locatorType, ManageInventory_Save_OK_Btn_Path);
			Click(locatorType, Logout_Path);
			// login();
		}

		// Test starts here

		SSOPage_Login();
		Select_li_Dropdown(Doclib_ProductLineType_arrow__Path, "Broker Dealer");
		Select_li_Dropdown(Doclib_State_arrow_Path, "Illinois");
		Select_li_Dropdown(Doclib_ProductLine_arrow_Path, "SecureFore 5");
		Click(locatorType, Doclib_Searchbtn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Doclib_Partsview("New Business", Part25)),
				Part25 + " is not displayed under 'New Business'");

	}

	@BeforeTest(enabled = true)
	public void BeforTest() throws IOException, InterruptedException {
		login();
	}

	@AfterTest(enabled = false)
	public void AfterTest() throws IOException, InterruptedException {
		logout();
	}

	@AfterMethod(enabled = false)
	public void AfteMethod() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void BefMethod() {
		softAssert1 = new SoftAssert();
	}

}