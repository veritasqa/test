package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Admin_Special_Projects extends BaseTest {

	public void Special_Projects_Menu_Hover() {

		Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Special_Projects_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);
	}

	@Test(priority = 1, enabled = true)
	public void GATC_2_4_1_1_4() {

		Special_Projects_Menu_Hover();

		/*		Hover(locatorType, Admin_btn_Path, driver);
				// Thread.sleep(500);
				Click(locatorType, Special_Projects_Path, driver);
		
				ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver); */

		Click(locatorType, SP_AddticketIcon_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_BusinessOwner_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Jobtitle_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_description_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_CreatedBy_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_JobType_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_CostCenter_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Inrush_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Quoteneeded_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, ManageInventory_Rules_Save_Btn_Path, driver),
				"Submit Cancel Request is Missing");
		softAssert.assertTrue(Is_Element_Present(locatorType, SPEnclosure_Cancel_Btn_Path, driver),
				"Submit Cancel Request is Missing");
		Click(locatorType, SPEnclosure_Cancel_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);
		softAssert.assertTrue(Is_Element_Present(locatorType, SP_ViewEdit_Path, driver),
				"Submit Cancel Request is Missing");

		Click(locatorType, SP_ViewEdit_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SPEnclosure_Cancel_Btn_Path, driver);

		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_Jobtitle_Path, driver),
				"Submit Cancel Request is Missing");

		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = true)
	public void GATC_2_4_1_1_5() throws InterruptedException {

		Special_Projects_Menu_Hover();

		/*Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Special_Projects_Path, driver);
		
		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);*/

		Click(locatorType, SP_AddticketIcon_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		Select_DropDown(locatorType, SplProjects_BusinessOwner_Path, "41", driver);

		Type(locatorType, SplProjects_Jobtitle_Path, "QA Test 123", driver);

		Select_DropDown(locatorType, SplProjects_JobType_Path, "43", driver);

		Type(locatorType, SplProjects_description_Path, "QA Is testing", driver);

		Assert.assertTrue(Get_Text(locatorType, SplProjects_CreatedBy_Path, driver).equalsIgnoreCase(UserName),
				"Username not prepopulated");
		Select_DropDown(locatorType, SplProjects_CostCenter_Path, "250-58205100", driver);

		Type(locatorType, SplProjects_SplInstruction_Path, "Test", driver);

		Click(locatorType, SplProjects_Inrush_Path, driver);

		Click(locatorType, SplProjects_Quoteneeded_Path, driver);

		Click(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, SplProjects_Savemessage_Path, driver).equalsIgnoreCase("Data has been saved."),
				"Data has been saved. message not displayed");

		Thread.sleep(1000);
		SplprjTicketNumber = Get_Text(locatorType, SplProjects_TicketNumber_Path, driver);

		Assert.assertTrue(!SplprjTicketNumber.isEmpty());

		Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Special_Projects_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, SP_1stTicket_NUmber_Path, driver).equalsIgnoreCase(SplprjTicketNumber),
				"TicketNumber");

	}

	@Test(priority = 3, enabled = true)
	public void GATC_2_4_1_1_2() {

		Special_Projects_Menu_Hover();

		/*Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Special_Projects_Path, driver);
		
		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver); */

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);
		Click(locatorType, SP_TicketFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		Type(locatorType, SP_JobTitletxt_Path, "QA Test 123", driver);
		Click(locatorType, SP_JobTitleFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		Type(locatorType, SP_DueDatetxt_Path, Get_Futuredate("MM/dd/YYYY"), driver);
		Click(locatorType, SP_DueDateFiler_Path, driver);

		Click(locatorType, Filter_EqualTo_Path, driver);
		Thread_Sleep(6000);

		Assert.assertTrue(Get_Text(locatorType, SP_TicketNoResult_Path, driver).equalsIgnoreCase(SplprjTicketNumber),
				"Ticket Numeber is not matching");
		Assert.assertTrue(Get_Text(locatorType, SP_JobTitleResult_Path, driver).equalsIgnoreCase("QA Test 123"),
				"JobTitle  is not matching");
		Assert.assertTrue(
				Get_Text(locatorType, SP_DuedateResult_Path, driver).equalsIgnoreCase(Get_Futuredate("MM/dd/YYYY")),
				"Due Date is not matching");

		Click(locatorType, SP_DueDateFiler_Path, driver);

		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(6000);

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);
		Click(locatorType, SP_TicketFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		Click(locatorType, SP_RushCheckbox_Path, driver);
		Click(locatorType, SP_RushFilter_Path, driver);

		Click(locatorType, Filter_EqualTo_Path, driver);
		Thread_Sleep(6000);

		Assert.assertTrue(Get_Text(locatorType, SP_TicketNoResult_Path, driver).equalsIgnoreCase(SplprjTicketNumber),
				"Ticket Numeber is not matching");

		Assert.assertTrue(Element_Isselected(locatorType, SP_RushResult_Path, driver),
				"Rush result is not checked off");

		Click(locatorType, SP_RushFilter_Path, driver);

		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(6000);

	}

	@Test(priority = 4, enabled = true)
	public void GATC_2_4_1_1_3() {

		Special_Projects_Menu_Hover();

		/*	Hover(locatorType, Admin_btn_Path, driver);
			// Thread.sleep(500);
			Click(locatorType, Special_Projects_Path, driver);
		
			ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);*/

		Assert.assertTrue(Is_Element_Present(locatorType, SP_ViewEdit_Path, driver), "View/Edit Path is missing");
		Assert.assertTrue(Is_Element_Present(locatorType, SP_Refreshicon_Path, driver), "Refresh button is missing");
		Assert.assertTrue(Is_Element_Present(locatorType, SP_AddticketIcon_Path, driver),
				"Add Ticket button is missing");
	}

	@Test(priority = 5, enabled = true)
	public void GATC_2_4_1_1_7() {

		Special_Projects_Menu_Hover();

		/*Hover(locatorType, Admin_btn_Path, driver);
		// Thread.sleep(500);
		Click(locatorType, Special_Projects_Path, driver);
		
		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);*/

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);
		Click(locatorType, SP_TicketFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		ExplicitWait_Element_Visible(locatorType, SP_ViewEdit_Path, driver);
		Click(locatorType, SP_ViewEdit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		String Desctext = Get_Text(locatorType, SplProjects_description_Path, driver);

		Type(locatorType, SplProjects_description_Path, "QA", driver);

		Click(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		Assert.assertTrue(
				Get_Text(locatorType, SplProjects_Savemessage_Path, driver).equalsIgnoreCase("Data has been saved."),
				"Data has been saved. message not displayed");

		Click(locatorType, SplProjects_Historytab_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		Assert.assertTrue(
				Get_Text(locatorType, SplProjects_History_user_result_Path, driver).equalsIgnoreCase(UserName),
				"Username not matching");
		Assert.assertTrue(Get_Text(locatorType, SplProjects_History_Details_result_Path, driver).equalsIgnoreCase(
				"Changed Description From '" + Desctext + "' To 'QA'"), "Change History is not proper");

	}

	@Test(priority = 6, enabled = true)

	public void GATC_2_4_1_1_6() throws IOException {

		Special_Projects_Menu_Hover();
		ExplicitWait_Element_Clickable(locatorType, SP_AddticketIcon_Path, driver);

		Click(locatorType, SP_AddticketIcon_Path, driver);

		Type(locatorType, SP_AddticketIcon_Path, UserName, driver);
		ExplicitWait_Element_Visible(locatorType, Add_Spl_ProjectsTitle_Path, driver);
		// Thread_Sleep(2000);
		softAssert.assertTrue(Is_Element_Present(locatorType, SplProjects_BusinessOwner_Path, driver),
				"'Business Owner' is not displayed  on 'General' page");
		Select_DropDown(locatorType, SplProjects_BusinessOwner_Path, "42", driver);
		Thread_Sleep(1000);
		// softAssert.assertTrue(Get_Attribute(locatorType,
		// SplProjects_Jobtitle_Path, "value", driver).isEmpty(),
		// "'Job Title' field is not empty on 'General' tab");
		Type(locatorType, SplProjects_Jobtitle_Path, "QA Test 123", driver);
		softAssert.assertTrue(
				Get_DropDown(locatorType, SplProjects_JobType_Path, driver).equalsIgnoreCase("- Select -"),
				"'Select' is not displayed for 'Job Type' field");
		Select_DropDown(locatorType, SplProjects_JobType_Path, "43", driver);
		Thread_Sleep(1000);

		softAssert.assertTrue(Get_Attribute(locatorType, SplProjects_description_Path, "value", driver).isEmpty(),
				"'Description' field is not empty on 'General' tab");
		Type(locatorType, SplProjects_description_Path, "QA is Testing", driver);
		Thread_Sleep(1000);

		softAssert.assertTrue(Get_Text(locatorType, SplProjects_CreatedBy_Path, driver).equalsIgnoreCase(UserName),
				"'Username' is not displayed on 'General' tab");
		softAssert.assertTrue(
				Get_DropDown(locatorType, SplProjects_CostCenter_Path, driver).equalsIgnoreCase("- Select -"),
				"'Select' is not displayed for 'Cost Center' field");
		Select_DropDown(locatorType, SplProjects_CostCenter_Path, "250-58205100", driver);
		Thread_Sleep(1000);

		softAssert.assertTrue(Get_Attribute(locatorType, SplProjects_SplInstruction_Path, "value", driver).isEmpty(),
				"'Special Instructions' field is not empty on 'General' tab");
		Type(locatorType, SplProjects_description_Path, "QA Test", driver);
		softAssert.assertFalse(Element_Isselected(locatorType, SplProjects_Inrush_Path, driver),
				"'Is Rush?' Checked off on General Page");
		Click(locatorType, SplProjects_Inrush_Path, driver);
		softAssert.assertFalse(Element_Isselected(locatorType, SplProjects_Quoteneeded_Path, driver),
				"'Is Quote Needed?' Checked off on General Page");
		Click(locatorType, SplProjects_Quoteneeded_Path, driver);
		Click(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, SplProjects_Savemessage_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, SplProjects_Savemessage_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed");
		softAssert.assertAll();

		// Progress/Files Tab
		Click(locatorType, SplProjects_Progresstab_Path, driver);
		Type(locatorType, SP_ProgTab_DueDate_Path, Get_Todaydate("MM/dd/YYYY"), driver);
		Click(locatorType, SplProjects_Progresstab_Path, driver);
		Assert.assertTrue(
				Get_Attribute(locatorType, SP_ProgTab_Duedateerror_Path, "class", driver)
						.equalsIgnoreCase("riTextBox riError"),
				"User can able able to select <Past Date - MM/DD/YYYY> from 'Calendar' icon");
		Type(locatorType, SP_ProgTab_DueDate_Path, Get_Futuredate("MM/dd/YYYY"), driver);
		Click(locatorType, SplProjects_Progresstab_Path, driver);
		Assert.assertFalse(
				Get_Attribute(locatorType, SP_ProgTab_Duedateerror_Path, "class", driver)
						.equalsIgnoreCase("riTextBox riError"),
				"User can not able able to select <Future Date - MM/DD/YYYY> from 'Calendar' icon");
		Click(locatorType, SP_ProgTab_AddCommenticon_Path, driver);
		Type(locatorType, SP_ProgTab_Commenttxt_Path, "QA Test", driver);
		Click(locatorType, SP_ProgTab_TypeInput_Path, driver);
		Thread_Sleep(2000);
		Click(locatorType, SP_ProgTab_TypeAck_Path, driver);
		Click(locatorType, SP_ProgTab_Choosefile_Path, driver);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\globalatlantic\\Driver");

		Click(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, SplProjects_Savemessage_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, SplProjects_Savemessage_Path, driver).equalsIgnoreCase("Data has been saved."),
				"'Data has been saved' message is not displayed");
		ExplicitWait_Element_Clickable(locatorType, SplProjects_BusinessOwner_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, SplProjects_BusinessOwner_Path, driver),
				"User is not on the General tab after Progress/Files tab has saved");

		// Cost Center Tab
		Click(locatorType, SplProjects_Costtab_Path, driver);
		Type(locatorType, SPcost_txtQuoteCost_Path, "0.1", driver);
		Type(locatorType, SPcost_txtPrintCost_Path, "0.1", driver);
		Type(locatorType, SPcost_txtVeritasPostageCost_Path, "0.1", driver);
		Type(locatorType, SPcost_txtFulfillmentCost_Path, "0.1", driver);
		Type(locatorType, SPcost_ClientPostageCost_Path, "0.1", driver);
		Type(locatorType, SPcost_txtRushCost_Path, "0.1", driver);
		Click(locatorType, SPcost_ckBilled_Path, driver);

		// Enclosures Tab
		Click(locatorType, SplProjects_Enclosuretab_Path, driver);
		Type(locatorType, SPEnclosure_Formnum_Path, Part1, driver);

		Thread_Sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, SPEnclosure_FormNumberdrop_Path, driver).equalsIgnoreCase(Part1),
				Part1 + " is not displayed in the Enclousres tab");

	}

	@Test(priority = 7, enabled = true)
	public void GATC_2_4_1_1_8() {

		Special_Projects_Menu_Hover();

		/*	Hover(locatorType, Admin_btn_Path, driver);
			// Thread.sleep(500);
			Click(locatorType, Special_Projects_Path, driver);
		
			ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver); */

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);
		Click(locatorType, SP_TicketFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		ExplicitWait_Element_Visible(locatorType, SP_ViewEdit_Path, driver);
		Click(locatorType, SP_ViewEdit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		Type(locatorType, SplProjects_description_Path, "QA", driver);

		Click(locatorType, ManageInventory_Rules_cancel_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver);

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);
		Click(locatorType, SP_TicketFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		ExplicitWait_Element_Visible(locatorType, SP_ViewEdit_Path, driver);
		Click(locatorType, SP_ViewEdit_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, SplProjects_description_Path, driver).equalsIgnoreCase("QA"),
				Get_Text(locatorType, SplProjects_description_Path, driver) + "Description message is not matching");

	}

	@Test(priority = 8, enabled = true)

	public void GATC_2_4_1_1_1() {

		Special_Projects_Menu_Hover();
		/*	Hover(locatorType, Admin_btn_Path, driver);
			Click(locatorType, Special_Projects_Path, driver);
			ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver); */

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);

		Click(locatorType, SP_TicketFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, SP_TicketNoResult_Path, driver).equalsIgnoreCase(SplprjTicketNumber),
				SplprjTicketNumber + " Ticket Number is not dispalyed");
		Reporter.log(SplprjTicketNumber + " Ticket Number is dispalyed");
		Click(locatorType, SP_TicketFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		Type(locatorType, SP_JobTitletxt_Path, "QA Test 123", driver);
		Click(locatorType, SP_JobTitleFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(5000);

		Assert.assertTrue(Get_Text(locatorType, SP_JobTitleResult_Path, driver).equalsIgnoreCase("QA Test 123"),
				Get_Text(locatorType, SP_JobTitleResult_Path, driver) + "Job title is not matching is not dispalyed");
		Reporter.log("QA Test123 Ticket Number is not dispalyed");
		Click(locatorType, SP_JobTitleFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		Type(locatorType, SP_JobType_Path, "Other", driver);
		Click(locatorType, SP_JobTypeFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(5000);

		Assert.assertTrue(Get_Text(locatorType, SP_JobTypeResult_Path, driver).equalsIgnoreCase("Other"),
				"Other is not dispalyed in the filter result");
		Reporter.log("Other status is not dispalyed in the filter result");
		Click(locatorType, SP_JobTypeFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		Type(locatorType, SP_Statustxt_Path, "New", driver);
		Click(locatorType, SP_StatusFiler_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(5000);

		Assert.assertTrue(Get_Text(locatorType, SP_StatusResult_Path, driver).trim().equalsIgnoreCase("New"),
				Get_Text(locatorType, SP_StatusResult_Path, driver) + "New status is  dispalyed in the filter result");
		Reporter.log("New status is not dispalyed in the filter result");
		Click(locatorType, SP_StatusFiler_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		Type(locatorType, SP_DueDatetxt_Path, Get_Futuredate("MM/dd/YYYY"), driver);
		Click(locatorType, SP_DueDateFiler_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_EqualTo_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(
				Get_Text(locatorType, SP_DuedateResult_Path, driver).equalsIgnoreCase(Get_Futuredate("MM/dd/YYYY")),
				Get_Text(locatorType, SP_DuedateResult_Path, driver) + "Date is not matching");
		Reporter.log(Get_Futuredate("MM/dd/YYYY") + " is  dispalyed in the filter result");
		Click(locatorType, SP_DueDateFiler_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		Click(locatorType, SP_RushCheckbox_Path, driver);
		Click(locatorType, SP_RushFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_EqualTo_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Element_Isselected(locatorType, SP_RushResult_Path, driver),
				"Rush is not checked in the results");
		Reporter.log("Rushed is checked in the results");
		Click(locatorType, SP_RushFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		Click(locatorType, SP_IsBilledCheckbox_Path, driver);
		Click(locatorType, SP_IsBilledFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_EqualTo_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(Element_Isselected(locatorType, SP_IsbilledResult_Path, driver),
				"IsBilled is not checked in the results");
		Reporter.log("IsBilled is not checked in the results");
		Click(locatorType, SP_IsBilledFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000);

		/*Type(locatorType, SP_RecipientCounttxt_Path, "Special Projects", driver);
		Click(locatorType, SP_RecipientCountFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_EqualTo_Path, driver);
		Thread_Sleep(2000);
		Assert.assertTrue(
				Get_Text(locatorType, SP_RecipentResult_Path, driver).equalsIgnoreCase(Get_Todaydate("MM/dd/YYYY")),
				"Special Projects is not dispalyed in the filter result");
		Reporter.log("Special Projects is dispalyed in the filter result");
		Click(locatorType, SP_RecipientCountFilter_Path, driver);
		Thread_Sleep(1000);
		Click(locatorType, Filter_NoFilter_Path, driver);
		Thread_Sleep(2000); */
	}

	@Test(priority = 9, enabled = true)
	public void GATC_2_4_1_1_9() {

		Special_Projects_Menu_Hover();

		/*	Hover(locatorType, Admin_btn_Path, driver);
			// Thread.sleep(500);
			Click(locatorType, Special_Projects_Path, driver);
		
			ExplicitWait_Element_Clickable(locatorType, SP_ViewEdit_Path, driver); */

		Type(locatorType, SP_Tickettxt_Path, SplprjTicketNumber, driver);
		Click(locatorType, SP_TicketFilter_Path, driver);

		Click(locatorType, Filter_Contains_Path, driver);
		Thread_Sleep(6000);

		ExplicitWait_Element_Visible(locatorType, SP_ViewEdit_Path, driver);
		Click(locatorType, SP_ViewEdit_Path, driver);

		Click(locatorType, SplProjects_Progresstab_Path, driver);

		Click(locatorType, SP_ProgTab_AddCommenticon_Path, driver);

		Type(locatorType, SP_ProgTab_Commenttxt_Path, "Test", driver);

		Click(locatorType, SP_ProgTab_Typeicon_Path, driver);

		Hover(locatorType, SP_ProgTab_type_cancel_path, driver);
		Thread_Sleep(2000);
		Click(locatorType, SP_ProgTab_type_cancel_path, driver);
		Thread_Sleep(2000);
		Click(locatorType, SP_ProgTab_Submit_Path, driver);
		Thread_Sleep(2000);

		Click(locatorType, ManageInventory_Rules_Save_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, SplProjects_Cancelbtn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, SplProjects_Ticket_Status_Path, driver).equalsIgnoreCase("Cancelled"),
				"Status not changed to cancelled status is still "
						+ Get_Text(locatorType, SplProjects_Ticket_Status_Path, driver));
		ExplicitWait_Element_Clickable(locatorType, SplProjects_Cancelbtn_Path, driver);

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}
}
