package com.globalatlantic.regression1;

import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.globalatlantic.Base.BaseTest;

public class AppBuilder extends BaseTest {

	public SoftAssert softAssert1;

	public void NaviagateAppBuilder() throws InterruptedException {

		Click(locatorType, Application_Builder_Path);
		Wait_ajax();
	}

	public void AppBuilderquestions(String Option) throws InterruptedException {

		if (Option.equalsIgnoreCase("Yes")) {
			Click(locatorType, AB_Q1Y_path);
			Click(locatorType, AB_Q2Y_path);
			Click(locatorType, AB_Q3Y_path);
			Click(locatorType, AB_Q4Y_path);
			Click(locatorType, AB_Q5Y_path);
			Click(locatorType, AB_Q6Y_path);
			Click(locatorType, AB_Q7Y_path);

		} else {

			Click(locatorType, AB_Q1N_path);
			Click(locatorType, AB_Q2N_path);
			Click(locatorType, AB_Q3N_path);
			Click(locatorType, AB_Q4N_path);
			Click(locatorType, AB_Q5N_path);
			Click(locatorType, AB_Q6N_path);
			Click(locatorType, AB_Q7N_path);

		}

	}

	@Test(enabled = true, priority = 1)
	public void GA_TC_2_6_1_1() throws InterruptedException, IOException {

		/*
		 * Verify Form is displayed under
		 * "Required Forms for all sales in the state of <State>" in App builder
		 * when "Required Form for Application Builder" in Piece Questions if
		 * either "Application/Disclosure /Suitability" is selected as "YES"
		 * Also make sure they are not selectable Verify the ability to Download
		 * Packet
		 */

		NaviagateAppBuilder();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("Yes");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29)),
				Part29 + " is not displays under 'Required Forms for all sales in the state of IL'");
		Click(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), Part29 + " is not displayed in pdf in new tab");
		Switch_Old_Tab();
		Assert.assertFalse(AppBuilder_ReqformsState_PartnameCkbox(locatorType, "IL", Part29),
				"User is able to uncheck the checkbox for " + Part29
						+ "displays under 'Required Forms for all sales in the state of IL' section");
		Click(locatorType, AB_BuildMyApplicationPacket_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AB_DownloadPacket_path),
				"'Download Packet button is not displayed'");
		Click(locatorType, AB_DownloadPacket_path);
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"),
				"'New tab is not opened upon clicking Download Packet button");
		Switch_Old_Tab();
		Click(locatorType, Home_btn_Path);

	}

	@Test(enabled = true, priority = 2)
	public void GA_TC_2_6_1_2() throws InterruptedException, IOException {

		/*
		 * Verify Form is displayed under "Marketing" in App builder when
		 * "Buyer's Guide:" in Piece Questions is selected as "YES" Also make
		 * sure they are selectable
		 */

		NaviagateAppBuilder();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("Yes");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_Marketing_Partname(Part30)),
				Part30 + " is not displays under 'Marketing Location'");
		Click(locatorType, AppBuilder_Marketing_Partname(Part30));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), Part30 + " pdf is not displayed in pdf in new tab");
		Switch_Old_Tab();
		Click(locatorType, Home_btn_Path);

	}

	@Test(enabled = true, priority = 3)
	public void GA_TC_2_6_1_3() throws InterruptedException, IOException {

		/*
		 * Verify Form is displayed under
		 * "Required Forms for all sales in the state of <State>" in App builder
		 * when
		 * "Include when applicant residence state differs from contract sold in state:"
		 * is selected as "YES" under "One State Has Product" in Piece Questions
		 * Also make sure they are selectable
		 */

		NaviagateAppBuilder();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("Yes");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part31)),
				Part31 + " is not displays under 'Required Forms for all sales in the state of IL'");
		Click(locatorType, AppBuilder_ReqformsState_Partname("IL", Part31));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), Part31 + " pdf is not displayed in pdf in new tab");
		Switch_Old_Tab();
		Click(locatorType, Home_btn_Path);

	}

	@Test(enabled = true, priority = 4)
	public void GA_TC_2_6_1_4() throws InterruptedException, IOException {

		/*
		 * Verify " Is the applicant 65 or over?" question appears only when CA
		 * state is selected for 'In what state will the contract be
		 * signed/issued/sold?' on App Builder page
		 */

		NaviagateAppBuilder();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AB_Title_path),
				"'Forethought Application Builder' page is not displays");
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Illinois");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "California");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		Assert.assertTrue(Element_Is_Displayed(locatorType, AB_Q8over65age_path),
				"'Is the applicant 65 or over?' question is not displays at the end of the page");
		Click(locatorType, Home_btn_Path);

	}

	@Test(enabled = true, priority = 5)
	public void GA_TC_2_6_1_5() throws InterruptedException, IOException {

		/*
		 * Verify Form is displayed under New Business ( Non required) when none
		 * of the questions under "Piece question" tab in manage inventory
		 */

		NaviagateAppBuilder();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("Yes");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_NewBusiness_Partname(Part32)),
				Part32 + " is not displays under 'New Business'");
		Click(locatorType, AppBuilder_NewBusiness_Partname(Part32));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), Part32 + " pdf is not displayed in pdf in new tab");
		Switch_Old_Tab();
		Click(locatorType, Home_btn_Path);

	}

	@Test(enabled = true, priority = 6)
	public void GA_TC_2_6_1_6() throws InterruptedException, IOException {

		/*
		 * Verify the forms displayed for required and Non required items when
		 * all questions were answered as "No" in App Builder
		 */

		NaviagateAppBuilder();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("No");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29)),
				Part29 + " is not displays under 'Required Forms for all sales in the state of IL'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part31)),
				Part31 + " is not displays under 'Required Forms for all sales in the state of IL'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_Marketing_Partname(Part30)),
				Part30 + " is not displays under 'Marketing Location'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_NewBusiness_Partname(Part32)),
				Part32 + " is not displays under 'New Business'");
		Click(locatorType, Home_btn_Path);
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 7)
	public void GA_TC_2_6_1_7() throws InterruptedException, IOException {

		/*
		 * Verify Form is displayed under
		 * "Required Forms for all sales in the state of <State>" in App builder
		 * when "Required Form for Application Builder" in Piece Questions if
		 * either "Application/Disclosure /Suitability" is selected as "YES"
		 * when user access through - I frame Also make sure they are not
		 * selectable
		 */

		Close_Browser();
		Quit_Browser();
		openbrowser();
		Appbuilder_IFramepage();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("Yes");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29)),
				Part29 + " is not displays under 'Required Forms for all sales in the state of IL'");
		Click(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), Part29 + " is not displayed in pdf in new tab");
		Switch_Old_Tab();
		Assert.assertFalse(AppBuilder_ReqformsState_PartnameCkbox(locatorType, "IL", Part29),
				"User is able to  uncheck the checkbox for " + Part29
						+ "displays under 'Required Forms for all sales in the state of IL' section");

	}

	@Test(enabled = true, priority = 8)
	public void GA_TC_2_6_1_8() throws InterruptedException, IOException {

		/*
		 * Verify the forms displayed for required and Non required items when
		 * all questions were answered as "No" in App Builder through I Frame
		 */
		Close_Browser();
		Quit_Browser();
		openbrowser();
		Appbuilder_IFramepage();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("No");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29)),
				Part29 + " is not displays under 'Required Forms for all sales in the state of IL'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part31)),
				Part31 + " is not displays under 'Required Forms for all sales in the state of IL'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_Marketing_Partname(Part30)),
				Part30 + " is not displays under 'Marketing Location'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_NewBusiness_Partname(Part32)),
				Part32 + " is not displays under 'New Business'");
		softAssert1.assertAll();

	}

	@Test(enabled = true, priority = 9)
	public void GA_TC_2_6_1_9() throws InterruptedException, IOException {

		/*
		 * Verify Form is displayed under
		 * "Required Forms for all sales in the state of <State>" in App builder
		 * when "Required Form for Application Builder" in Piece Questions is
		 * either "Application/Disclosure /Suitability" is selected as "YES" for
		 * SSO user Also make sure piece is not selectable
		 */

		Close_Browser();
		Quit_Browser();
		openbrowser();
		Appbuilder_SSOPage_Login();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("Yes");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29)),
				Part29 + " is not displays under 'Required Forms for all sales in the state of IL'");
		Click(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29));
		Switch_New_Tab();
		Assert.assertTrue(Get_Current_Url().contains(".pdf"), Part29 + " is not displayed in pdf in new tab");
		Switch_Old_Tab();
		Assert.assertFalse(AppBuilder_ReqformsState_PartnameCkbox(locatorType, "IL", Part29),
				"User is able to uncheck the checkbox for " + Part29
						+ "displays under 'Required Forms for all sales in the state of IL' section");

	}

	@Test(enabled = true, priority = 10)
	public void GA_TC_2_6_1_10() throws InterruptedException, IOException {

		/*
		 * Verify the forms displayed for required and Non required items when
		 * all questions were answered as "No" in App Builder for SSO User
		 */

		Close_Browser();
		Quit_Browser();
		openbrowser();
		Appbuilder_SSOPage_Login();
		Select_li_Dropdown(AB_ApplicantresideArrow_path, "Arizona");
		Select_li_Dropdown(AB_SignedissuedsoldArrow_path, "Illinois");
		Select_li_Dropdown(AB_ProductfromArow_path, "IMO ForeCare");
		AppBuilderquestions("No");
		Click(locatorType, AB_Submitbtn_path);
		Wait_ajax();
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part29)),
				Part29 + " is not displays under 'Required Forms for all sales in the state of IL'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_ReqformsState_Partname("IL", Part31)),
				Part31 + " is not displays under 'Required Forms for all sales in the state of IL'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_Marketing_Partname(Part30)),
				Part30 + " is not displays under 'Marketing Location'");
		softAssert1.assertTrue(Element_Is_Displayed(locatorType, AppBuilder_NewBusiness_Partname(Part32)),
				Part32 + " is not displays under 'New Business'");
		softAssert1.assertAll();

	}

	@AfterMethod(enabled = true)
	public void AfteMethod() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void BefMethod() {
		softAssert1 = new SoftAssert();
	}

	@BeforeTest(enabled = true)
	public void BeforeTest() throws IOException, InterruptedException {

		login();

	}

}