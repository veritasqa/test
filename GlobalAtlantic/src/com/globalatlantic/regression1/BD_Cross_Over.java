package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class BD_Cross_Over extends BaseTest {

	public void Manage_Inventory_Menu_Hover() {
		Hover(locatorType, Admin_btn_Path, driver);
		Click(locatorType, Manage_Inventory_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path, driver);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path, driver);

	}

	/*Validate part is not viewable -
	
	1. Ordering Customer Business Owner is Broker Dealer and Firm is anything except BBT
	2. Piece has Business Owner - Broker Dealer 
	3. IMO/BD is Selected
	4. Firm included ( BBT)
	
	
	Then, Firm rules will apply and part is not Viewable */
	@Test(enabled = true)
	public void GATC_2_4_7_5_1() {
		Manage_Inventory_Menu_Hover();

		Type(locatorType, ManageInventory_FormNo_Path, Part36, driver);
		Click(locatorType, ManageInventory_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Searchresult_FormNo, driver).equalsIgnoreCase(Part36),
				Part36 + " is not displayed in the search results");
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ManageInventory_General_Path, driver),
				"Manage Inventory - General Tab is not displayed is not Displayed");

		Assert.assertTrue(
				Get_Attribute(locatorType, ManageInventory_BusinessOwner_Input_Path, "Value", driver)
						.equalsIgnoreCase("Broker Dealer"),
				"'Business Owner' field is not pre-populated with 'Broker Dealer' from original part");
		Click(locatorType, ManageInventory_RulesTab_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, ManageInventory_Rules_Firms_Drop_valuePath, driver)
				.equalsIgnoreCase("BB&T Investments"), "BB&T Investments is not  dispalyed under firm rules");

		Assert.assertTrue(Element_Isselected(locatorType, ManageInventory_Rules_Crossover_Radio_Path, driver),
				"BD Crossover is not checkedoff");

		Type(locatorType, Fullfilment_Search_Path, Part36, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch("GATC", "2.4.7.5.1");

		// ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path,
		// driver);
		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part36 + " is displayed or no error message disyplayed ");

	}

	/*Validate part is orderable -
	
	1. Ordering Customer Business Owner is IMO/Annuity and Contact Firm is anything except BBT 
	2.  Piece has Business Owner - Broker Dealer 
	3. IMO/BD is Selected
	4. Firm included ( BBT)
	
	
	Then, Firm rules will Not apply and part is Orderable */

	@Test(enabled = true)
	public void GATC_2_4_7_5_2() {

		Manage_Inventory_Menu_Hover();

		Type(locatorType, ManageInventory_FormNo_Path, Part37, driver);
		Click(locatorType, ManageInventory_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Searchresult_FormNo, driver).equalsIgnoreCase(Part37),
				Part37 + " is not displayed in the search results");
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ManageInventory_General_Path, driver),
				"Manage Inventory - General Tab is not displayed is not Displayed");

		Assert.assertTrue(
				Get_Attribute(locatorType, ManageInventory_BusinessOwner_Input_Path, "Value", driver)
						.equalsIgnoreCase("Broker Dealer"),
				"'Business Owner' field is not pre-populated with 'Broker Dealer' from original part");
		Click(locatorType, ManageInventory_RulesTab_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, ManageInventory_Rules_Firms_Drop_valuePath, driver)
				.equalsIgnoreCase("BB&T Investments"), "BB&T Investments is not  dispalyed under firm rules");

		Assert.assertTrue(Element_Isselected(locatorType, ManageInventory_Rules_Crossover_Radio_Path, driver),
				"BD Crossover is not checkedoff");

		Type(locatorType, Fullfilment_Search_Path, Part37, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch("GATC", "2.4.7.5.2");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part37),
				Part37 + "is not displayed");
	}

	/*Validate part is orderable -
	
	1. Ordering Customer Business Owner is Traditional Life and Contact Firm is anything except BBT
	2.  Business Owner is  Broker Dealer 
	3. IMO/BD is Selected
	4. Firm included ( BBT)
	
	
	Then, Firm rules will Not apply and part is Orderable */

	@Test(enabled = true)
	public void GATC_2_4_7_5_3() {

		Manage_Inventory_Menu_Hover();

		Type(locatorType, ManageInventory_FormNo_Path, Part38, driver);
		Click(locatorType, ManageInventory_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Searchresult_FormNo, driver).equalsIgnoreCase(Part38),
				Part38 + " is nots displayed in the search results");
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ManageInventory_General_Path, driver),
				"Manage Inventory - General Tab is not displayed is not Displayed");

		Assert.assertTrue(
				Get_Attribute(locatorType, ManageInventory_BusinessOwner_Input_Path, "Value", driver)
						.equalsIgnoreCase("Broker Dealer"),
				"'Business Owner' field is not pre-populated with 'Broker Dealer' from original part");
		Click(locatorType, ManageInventory_RulesTab_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, ManageInventory_Rules_Firms_Drop_valuePath, driver)
				.equalsIgnoreCase("BB&T Investments"), "BB&T Investments is not  dispalyed under firm rules");

		Assert.assertTrue(Element_Isselected(locatorType, ManageInventory_Rules_Crossover_Radio_Path, driver),
				"BD Crossover is not checkedoff");

		Type(locatorType, Fullfilment_Search_Path, Part38, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch("GATC", "2.4.7.5.3");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part38),
				Part38 + "is not displayed");

	}

	/*Validate part is not viewable -
	
	1. Ordering Customer Business Owner is Broker Dealer and Contact Firm is anything except BBT
	2. Piece has Business Owner - Preneed
	3. IMO/BD is NOT Selected
	4. Firm included ( BBT)
	
	
	Then, Firm rules will apply and part is not Viewable */

	@Test
	public void GATC_2_4_7_5_4() {
		Manage_Inventory_Menu_Hover();

		Type(locatorType, ManageInventory_FormNo_Path, Part39, driver);
		Click(locatorType, ManageInventory_Search_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, MI_Searchresult_FormNo, driver).equalsIgnoreCase(Part39),
				Part39 + " is not displayed in the search results");
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path, driver);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ManageInventory_General_Path, driver),
				"Manage Inventory - General Tab is not displayed is not Displayed");

		Assert.assertTrue(
				Get_Attribute(locatorType, ManageInventory_BusinessOwner_Input_Path, "Value", driver)
						.equalsIgnoreCase("Preneed"),
				"'Business Owner' field is not pre-populated with 'Broker Dealer' from original part");
		Click(locatorType, ManageInventory_RulesTab_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, ManageInventory_Rules_Firms_Drop_valuePath, driver)
				.equalsIgnoreCase("BB&T Investments"), "BB&T Investments is not  dispalyed under firm rules");

		Assert.assertFalse(Element_Isselected(locatorType, ManageInventory_Rules_Crossover_Radio_Path, driver),
				"BD Crossover is  checkedoff");

		Type(locatorType, Fullfilment_Search_Path, Part39, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch("GATC", "2.4.7.5.1");

		// ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path,
		// driver);
		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part39 + " is displayed or no error message disyplayed ");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}
}
