package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfillment_PartViewinMode extends BaseTest {

	@Test(enabled = true, priority = 1)
	public void GA_TC_2_9_5_1() throws InterruptedException {
		// Validate that admin only part is viewable for an admin user

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

		Type(locatorType, Fullfilment_Search_Path, Part10);
		Click(locatorType, Fullfilment_Search_Btn_Path);

		// contact name

		ContactSearch(Firstname, Lastname);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part10), Part10 + "is not displayed");

	}

	@Test(enabled = true, priority = 2)
	public void GA_TC_2_9_5_2() throws InterruptedException, IOException {

		// Validate when a part is set to viewable and not orderable that it"s
		// viewable

		Clearcart();
		// On main Page
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		Type(locatorType, ManageInventory_FormNo_Path, Part40);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Select_li_Dropdown(ManageInventory_Viewability_Arrow_Path, "Viewable Not Orderable");
		Click(locatorType, ManageInventory_Rules_Save_Btn_Path);
		Click(locatorType, ManageInventory_Rules_OK_Btn_Path);
		logout();
		login();
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part40);
		Click(locatorType, Fullfilment_Search_Btn_Path);

		// contact name

		ContactSearch(Firstname, Lastname);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part40), Part40 + "is not displayed");
		Assert.assertFalse(Element_Is_Displayed(locatorType, Containstextpath("div", "PDF Only")),
				"PDF Only is not displayed");

	}

	@Test(enabled = false, priority = 3)
	public void GA_TC_2_9_5_3() throws IOException, InterruptedException {

		// Validate when a part is set to not viewable that is not viewable
		Clearcart();
		// On main Page
		Hover(locatorType, Admin_btn_Path);
		// Thread.sleep(500);
		Click(locatorType, Manage_Inventory_Path);
		// On Inventory page
		Type(locatorType, ManageInventory_FormNo_Path, Part40);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Select_li_Dropdown(ManageInventory_Viewability_Arrow_Path, "Not Viewable");
		Click(locatorType, ManageInventory_Rules_Save_Btn_Path);
		Click(locatorType, ManageInventory_Rules_OK_Btn_Path);
		logout();
		login();
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part40);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		// contact name
		ContactSearch(Firstname, Lastname);
		Assert.assertTrue(Get_Text(locatorType, No_Records_Message_path).trim().equalsIgnoreCase(No_Records_Message),
				Part40 + " is displayed or no error message disyplayed ");

	}

	@Test(enabled = true, priority = 4)
	public void GA_TC_2_9_5_4() throws IOException, InterruptedException {

		// Validate when the part is set to orderable that it is orderable
		Clearcart();
		// On main Page
		Hover(locatorType, Admin_btn_Path);
		// Thread.sleep(500);
		Click(locatorType, Manage_Inventory_Path);
		// On Inventory page
		Type(locatorType, ManageInventory_FormNo_Path, Part40);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Select_li_Dropdown(ManageInventory_Viewability_Arrow_Path, "Orderable");
		Click(locatorType, ManageInventory_Rules_Save_Btn_Path);
		Click(locatorType, ManageInventory_Rules_OK_Btn_Path);
		logout();
		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part40);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		// contact name
		ContactSearch(Firstname, Lastname);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part40), Part40 + "is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(3000);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part40), Part40 + "is not displayed");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
