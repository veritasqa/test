package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.globalatlantic.Base.BaseTest;

public class DocLibraryMultipleForms extends BaseTest {

	public SoftAssert softAssert1;

	public String Effectivedate = "";

	public void Navigate_ManageFormPermissions() {
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, ManageFormPermissions_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_Searchbtn_Path);
	}

	public String Navigate_ManageFormPermissions_Search(String Partname) {

		return ".//*[@id='tabs-1']/div[1]/div[4]/h1[contains(text(),'Search Results')]//following::tbody[2]//td[text()='"
				+ Partname + "']//following::td[4]//input[@type='checkbox']";

	}

	public void Manage_Inventory_Menu_Hover() {
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);

	}

	public void SelectAnnuity() throws InterruptedException {

		Select_li_Dropdown(MFP_ProductLineTypearrow_Path, "Annuity");
		Click(locatorType, MFP_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_Searchloading_Path);
		// Select_li_Dropdown(MFP_Pagesizearrow_Path, "All");
		// ExplicitWait_Element_Not_Visible(locatorType,
		// MFP_Pagesizeloading_Path);

	}

	public void Selectforms() {

		Click(locatorType, Navigate_ManageFormPermissions_Search(Part26));
		Click(locatorType, Navigate_ManageFormPermissions_Search(Part27));
		Click(locatorType, Navigate_ManageFormPermissions_Search(Part28));
	}

	public void Verify_Busineesownerdropdown(String Partname, String Dropdownvalue) {

		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Partname);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(
				Get_Attribute(locatorType, ManageInventory_BusinessOwner_Path, "value").equalsIgnoreCase(Dropdownvalue),
				Dropdownvalue + " is not displayed in the Business owner dropdown for " + Partname);
	}

	public void Verify_Viewabilitydropdown(String Partname, String Dropdownvalue) {

		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Partname);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(
				Get_Attribute(locatorType, ManageInventory_Viewability_Input_Path, "value")
						.equalsIgnoreCase(Dropdownvalue),
				Dropdownvalue + " is not displayed in the Viewability owner dropdown for " + Partname);
	}

	public void Verify_EffectiveDate(String Partname, String Effectivedate) {

		Manage_Inventory_Menu_Hover();
		Type(locatorType, ManageInventory_FormNo_Path, Partname);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Assert.assertTrue(Get_Attribute(locatorType, ManageInventory_EffectiveDate_dateInput_Path, "value")
				.equalsIgnoreCase(Effectivedate),
				Effectivedate + " is mis-matching in the effective date field for " + Partname);
	}

	@Test(enabled = true)
	public void GA_TC_2_5_2_5() throws IOException, InterruptedException {

		// Validate updating Business owner for multiple Forms without selecting
		// a firm

		Navigate_ManageFormPermissions();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_InventorySearchFilters_Path),
				"'Inventory Search Filters' is not displayed");
		SelectAnnuity();
		Selectforms();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_Miscellaneous_Path),
				"'Miscellaneous' is not displayed");
		Select_li_Dropdown(MFP_BusinessOwnerarrow_Path, "Broker Dealer");
		Click(locatorType, MFP_UpdateBusinessOwnerbtn_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdatesuccessfulOK_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_UpdatesuccessfulOK_Path);
		Verify_Busineesownerdropdown(Part26, "Broker Dealer");
		Verify_Busineesownerdropdown(Part27, "Broker Dealer");
		Verify_Busineesownerdropdown(Part28, "Broker Dealer");

	}

	@Test(enabled = true)
	public void GA_TC_2_5_2_6() throws IOException, InterruptedException {

		// Validate updating Viewability for multiple Forms without selecting a
		// firm

		Navigate_ManageFormPermissions();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_InventorySearchFilters_Path),
				"'Inventory Search Filters' is not displayed");
		SelectAnnuity();
		Selectforms();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_Miscellaneous_Path),
				"'Miscellaneous' is not displayed");
		Select_li_Dropdown(MFP_Viewabilityarrow_Path, "Admin Only");
		Click(locatorType, MFP_UpdateViewabilitybtn_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdatesuccessfulOK_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_UpdatesuccessfulOK_Path);
		Verify_Viewabilitydropdown(Part26, "Admin Only");
		Verify_Viewabilitydropdown(Part27, "Admin Only");
		Verify_Viewabilitydropdown(Part28, "Admin Only");

	}

	@Test(enabled = true)
	public void GA_TC_2_5_2_7() throws IOException, InterruptedException {

		// Validate updating Effective Date for multiple Forms without selecting
		// a firm

		Navigate_ManageFormPermissions();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_InventorySearchFilters_Path),
				"'Inventory Search Filters' is not displayed");
		SelectAnnuity();
		Selectforms();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_Miscellaneous_Path),
				"'Miscellaneous' is not displayed");
		Type(locatorType, MFP_EffectiveDate_Path, Get_Futuredate("MM/dd/YYYY"));
		Click(locatorType, MFP_UpdateEffectiveDatebtn_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdatesuccessfulOK_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_UpdatesuccessfulOK_Path);
		Effectivedate = Get_Attribute(locatorType, MFP_EffectiveDate_Path, "value");
		Verify_EffectiveDate(Part26, Effectivedate);
		Verify_EffectiveDate(Part27, Effectivedate);
		Verify_EffectiveDate(Part28, Effectivedate);

	}

	@Test(enabled = true)
	public void GA_TC_2_5_2_8() throws IOException, InterruptedException {

		// Validate updating Business owner, Viewability and Effective Date for
		// multiple Forms without selecting a firm

		Navigate_ManageFormPermissions();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_InventorySearchFilters_Path),
				"'Inventory Search Filters' is not displayed");
		SelectAnnuity();
		Selectforms();
		Assert.assertTrue(Element_Is_Displayed(locatorType, MFP_Miscellaneous_Path),
				"'Miscellaneous' is not displayed");
		Select_li_Dropdown(MFP_BusinessOwnerarrow_Path, "Broker Dealer");
		Select_li_Dropdown(MFP_Viewabilityarrow_Path, "Admin Only");
		Type(locatorType, MFP_EffectiveDate_Path, Get_Futuredate("MM/dd/YYYY"));
		Click(locatorType, MFP_UpdateBusinessOwnerbtn_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdatesuccessfulOK_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_UpdatesuccessfulOK_Path);
		Select_li_Dropdown(MFP_Viewabilityarrow_Path, "Admin Only");
		Click(locatorType, MFP_UpdateViewabilitybtn_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdatesuccessfulOK_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdateEffectiveDatebtn_Path);
		ExplicitWait_Element_Clickable(locatorType, MFP_UpdatesuccessfulOK_Path);
		Click(locatorType, MFP_UpdatesuccessfulOK_Path);
		ExplicitWait_Element_Not_Visible(locatorType, MFP_UpdatesuccessfulOK_Path);
		Effectivedate = Get_Attribute(locatorType, MFP_EffectiveDate_Path, "value");
		Verify_Busineesownerdropdown(Part26, "Broker Dealer");
		Verify_Busineesownerdropdown(Part27, "Broker Dealer");
		Verify_Busineesownerdropdown(Part28, "Broker Dealer");
		Verify_Viewabilitydropdown(Part26, "Admin Only");
		Verify_Viewabilitydropdown(Part27, "Admin Only");
		Verify_Viewabilitydropdown(Part28, "Admin Only");
		Verify_EffectiveDate(Part26, Effectivedate);
		Verify_EffectiveDate(Part27, Effectivedate);
		Verify_EffectiveDate(Part28, Effectivedate);

	}

	@BeforeTest(enabled = true)
	public void BeforTest() throws IOException, InterruptedException {
		login();
	}

	@AfterTest(enabled = false)
	public void AfterTest() throws IOException, InterruptedException {
		logout();
	}

	@AfterMethod(enabled = true)
	public void AfteMethod() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void BefMethod() {
		softAssert1 = new SoftAssert();
	}

}