package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Materials extends BaseTest {

	public void Materials_Menu_Hover() {
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Catagories);
		ExplicitWait_Element_Clickable(locatorType, MC_AddCatagory);

	}

	@Test
	public void GA_TC_2_2_1_1() throws IOException, InterruptedException {

		// Validate active category appears under materials dropdown

		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		Type(locatorType, ManageInventory_FormNo_Path, "QA_Multifunction");
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);

		if (Element_Is_selected(Xpath, CatagoriesCheckbox("QA Test Materials"))) {

		} else {
			Click(Xpath, CatagoriesCheckbox("QA Test Materials"));
			Click(Xpath, ManageInventory_Rules_Save_Btn_Path);
			Click(Xpath, ManageInventory_Rules_OK_Btn_Path);
			logout();
			login();
		}

		Hover(locatorType, Materials_btn_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, Textpath("span", "QA Test Materials")),
				"QA Test Materials is not displayed in the materials drop down");

		// Materials_Menu_Hover();

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		// logout();

	}

}
