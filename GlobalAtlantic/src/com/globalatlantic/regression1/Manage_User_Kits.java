package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Manage_User_Kits extends BaseTest {

	public static String AddKitName = "";

	public void Manage_UserKits_Hover() {
		Hover(locatorType, Admin_btn_Path, driver);
		Click(locatorType, Manage_UsersKits_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, UR_AddNewRecord, driver);

	}

	/*
	 * Validate page opens and appropriate fields/tables are displaying
	 * 
	 */
	@Test
	public void userkits1() {
		Manage_UserKits_Hover();
		Assert.assertTrue(Get_Text(locatorType, Userkitsheading, driver).trim().equalsIgnoreCase("User Kits"),
				"UserKits heading not Displyed ");

		Assert.assertTrue(Is_Element_Present(locatorType, UserkitsSection, driver),
				"User Kits : View Kits Section not Displayed");

		Click(locatorType, UR_AddNewRecord, driver);

		ExplicitWait_Element_Clickable(locatorType, UR_Save_Btn, driver);

		Type(locatorType, UR_Formnumber, "QA Test", driver);

		Assert.assertTrue(Is_Element_Present(locatorType, UR_Desc, driver),
				"User Kits Description Section not Displayed");
		Type(locatorType, UR_Desc, "QA Test", driver);
		Click(locatorType, UR_Save_Btn, driver);


		Assert.assertTrue(
				Get_Attribute(locatorType, UR_Formnumber, "value", driver)
						.equalsIgnoreCase("QAutomation" + "_QA Kit_" + Get_Todaydate("MMddYY")),
				"Invalid Kit name is displayed in after User kit saved");

		Assert.assertTrue(Element_Is_Displayed(locatorType, UR_Kit_Dropdown, driver),
				"User Kits Dropdown Section not Displayed");

		Assert.assertTrue(Element_Is_Displayed(locatorType, UR_Addinvent_Section, driver),
				"Add inventory Section not Displayed");

		Assert.assertTrue(Element_Is_Displayed(locatorType, UR_Addinvent_Section, driver),
				"Add inventory Section not Displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, UR_Addinvent_Section, driver),
				"Add inventory Section not Displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, UR_Addinvent_Section, driver),
				"Add inventory Section not Displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, UR_Addinvent_Section, driver),
				"Add inventory Section not Displayed");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}
}
