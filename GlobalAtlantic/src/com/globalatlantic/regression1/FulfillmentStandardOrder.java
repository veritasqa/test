package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.globalatlantic.Base.BaseTest;

public class FulfillmentStandardOrder extends BaseTest {

	@Test
	public void GA_TC_2_9_1_1_1() throws InterruptedException, IOException {

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part1);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		// contact name
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part1), Part1 + "is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(3000);
		ExplicitWait_Element_Visible(Xpath, Add_Cart_Message_Path);
		// Assert.assertTrue(Get_Text(locatorType,
		// Add_Cart_Message_Path).equalsIgnoreCase("Successfully Added 1!"),
		// "Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path);
		Thread.sleep(5000);
		PlaceanOrder("GA_TC_2_9_1_1_1");
		Click(locatorType, Ok_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
