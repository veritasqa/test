package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.globalatlantic.Base.BaseTest;

public class AdminManageOrders extends BaseTest {

	@Test(enabled = false, priority = 2, dependsOnMethods = { "Pre_Req_Order" })
	public void GATC_2_4_9_1_1() throws InterruptedException {

		// Ensure Select order, search order, and search results grids appear
		// appropriately

		// Do not run this

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);

		Click(locatorType, Manage_Orders_Path);

		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);

		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SelectOrder_Path),
				"Manage Select Order field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_View_Btn_Path),
				"View button is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Copy_Btn_Path),
				"Copy button is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Order_Num_Path),
				"Order Number field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Tracking_Num_Path),
				"Tracking Number field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Type_Value_Path),
				"Type Value field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Type_Icon_Path),
				"Type value icon field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_User_Path),
				"User field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Shipper_Path),
				"Shipper field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Status_Value_Path_),
				"Status Value field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Status_Icon_Path_),
				"Status Value icons field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_StartDate_Path),
				"Start Date field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_StartDate_Icon_Path),
				"Start Date icon is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_EndDate_Path),
				"End Date field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_EndDate_Icon_Path),
				"End Date icon is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_RecipientName_Path),
				"RecipientName field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Address_Path),
				"Address field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_city_Path),
				"City field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_State_Path),
				"State field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_ZipCode_Path),
				"Zip Code field is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Search_Btn_Path),
				"Search button is not displayed in Mangae order Page");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_Clear_Btn_Path),
				"Clear button is not displayed in Mangae order Page");

		softAssert.assertAll();

	}

	@Test(enabled = false, priority = 3)
	public void GATC_2_4_9_1_2() throws InterruptedException {

		// Verify Search and Clear functionality in Search Order works

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);
		// Thread.sleep(500);
		Click(locatorType, Manage_Orders_Path);

		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);
		Type(locatorType, Manage_Order_Num_Path, OrderNumber);
		Click(locatorType, Manage_Type_Icon_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_Type_Value_FF_Path);
		Click(locatorType, Manage_Type_Value_FF_Path);
		Type(locatorType, Manage_Shipper_Path, Shipper);
		Select_li_Dropdown(Manage_Status_Icon_Path_, "NEW");
		Type(locatorType, Manage_StartDate_Path, Get_Todaydate("MM/dd/yyyy"));
		Type(locatorType, Manage_EndDate_Path, Get_Todaydate("MM/dd/yyyy"));
		Type(locatorType, Manage_RecipientName_Path, "QA Auto");
		Type(locatorType, Manage_city_Path, City);
		Type(locatorType, Manage_ZipCode_Path, "60089-2375");
		Click(locatorType, Manage_Search_Btn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, MO_Loadingpanel);

	}

	@Test(enabled = false, priority = 4, dependsOnMethods = { "Pre_Req_Order" })
	public void GATC_2_4_9_1_3() throws InterruptedException {

		// Verify search order copy functionality

		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", OrderNumber)),
				"<Order #> from Pre-Condition not displays in 'Search Results' grid");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "QA Auto")),
				"<First Name Last Name> not displays under 'Ordered by' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", Get_Todaydate("M/d/yyyy"))),
				"<Today's Date - MM/DD/YYYY> not Fdisplays under 'Order Date' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "UPS Ground")),
				" 'UPS Ground' displays under 'Shipper' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "NEW")), "");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "IL")),
				"'IL' not displays under 'State' column");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("td", "60089")),
				" '60089-2375' in 'ZipCode' column");
		Click(locatorType, Manage_Clear_Btn_Path);

		// Assertions for clear button

		softAssert.assertFalse(Get_Text(locatorType, Manage_Order_Num_Path).equalsIgnoreCase(OrderNumber),
				"Order Number field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_Tracking_Num_Path).equalsIgnoreCase("Test Tracking"),
				"Tracking Number field is not cleared");
		softAssert.assertFalse(
				Get_Attribute(locatorType, Manage_Type_Value_Path, "Value").equalsIgnoreCase(Dropdown_Clear),
				"Type dropdown is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_User_Path).equalsIgnoreCase(UserName),
				"User field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_Shipper_Path).equalsIgnoreCase(Shipper),
				"Shipper field is not cleared");
		softAssert.assertFalse(
				Get_Attribute(locatorType, Manage_Status_Value_Path_, "Value").equalsIgnoreCase(Dropdown_Clear),
				"Status Dropdown is not cleared");

		softAssert.assertFalse(
				Get_Text(locatorType, Manage_StartDate_Path).equalsIgnoreCase(Get_Todaydate("MM/dd/yyyy")),
				"Start Date field is not cleared");
		softAssert.assertFalse(
				Get_Text(locatorType, Manage_EndDate_Path).equalsIgnoreCase(Get_Futuredate("MM/dd/yyyy")),
				"End Date field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_RecipientName_Path).equalsIgnoreCase("Test Recipient"),
				"Recipient field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_Address_Path).equalsIgnoreCase(Address1),
				"Address field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_city_Path).equalsIgnoreCase(City),
				"City field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_State_Path).equalsIgnoreCase(State),
				"State field is not cleared");
		softAssert.assertFalse(Get_Text(locatorType, Manage_ZipCode_Path).equalsIgnoreCase(Zipcode),
				"Zipcode field is not cleared");
		softAssert.assertAll();

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Orders_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);
		Type(locatorType, Manage_Order_Num_Path, OrderNumber);
		Click(locatorType, Manage_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_CopyResults_Btn_Path);
		Click(locatorType, Manage_CopyResults_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("span", Part1)), Part1 + " is not displayed");
		Click(locatorType, Textpath("a", "Remove"));
		ExplicitWait_Element_Visible(locatorType, Textpath("div", "Item(s) have been removed from your cart."));
		Assert.assertTrue(
				Element_Is_Displayed(locatorType, Textpath("div", "Item(s) have been removed from your cart.")),
				"Item(s) have been removed from your cart. is not displayed");

	}

	@Test(enabled = true, priority = 5, dependsOnMethods = { "Pre_Req_Order" })
	public void GATC_2_4_9_1_4() throws InterruptedException {

		// Validate that select order - view functionality works appropriately

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Orders_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);
		Type(locatorType, Manage_SelectOrder_Path, OrderNumber);
		Thread.sleep(80000);
		Click(locatorType, Manage_View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_GeneralTab_Path);

		// General Tab
		Assert.assertTrue(Element_Is_Displayed(locatorType, Manage_GeneralTab_Path),
				"General Tab is not Displayed in the General Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Textpath("span", OrderNumber)),
				"<Order Number> is not displays in 'Manage Order' field on header bar");
		// softAssert.assertTrue(Element_Is_Displayed(locatorType,
		// Textpath("span", "NEW")),
		// "New displays in 'Status' field on header bar");
		softAssert.assertTrue(Get_Text(locatorType, Manage_GT_TicketNumberfield1_Path).equalsIgnoreCase(OrderNumber),
				"<Order Number> is not displays under 'Ticket Number' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_GT_Usernamefield1_Path).equalsIgnoreCase("QA Auto"),
				"<First Name Last Name> is not displays under 'User Name' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_GT_FirmNamerfield1_Path).trim().equalsIgnoreCase(""),
				"' ' is not displays under 'Firm Name' column");
		softAssert.assertTrue(
				Get_Text(locatorType, Manage_GT_OrderDatefield1_Path).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"<Today's Date - MM/DD/YYYY> is not displays under 'Order Date' column");
		softAssert.assertTrue(
				Get_Text(locatorType, Manage_GT_ShipDatefield1_Path).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"<Today's Date - MM/DD/YYYY> is not displays under 'Ship Date' column");
		softAssert.assertTrue(
				Get_Text(locatorType, Manage_GT_OrderTypefield1_Path).equalsIgnoreCase("FORETHOUGHT_FULFILLMENT"),
				"FORETHOUGHT_FULFILLMENT is not displays under 'Order Type' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_GT_OnBackOrderfield1_Path).equalsIgnoreCase("False"),
				"False is not displays under 'On Backorder' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_GT_OnHoldfield1_Path).equalsIgnoreCase("False"),
				"False is not displays under 'On Hold' column");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_GT_Select_Btn_Path),
				" Select button is not Displayed in the General Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_GT_Edit_Btn_Path),
				" Edit button is not Displayed in the General Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_GT_OrderSearch_btn_Path),
				" Order Search Button is not Displayed in the General Tab");

		// Item Tab
		Click(locatorType, Manage_ItemsTab_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_IT_Update_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Manage_ItemsTab_Path), "Item Tab is not Displayed");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_FormNumberfield1_Path).trim().equalsIgnoreCase(Part1),
				Part1 + "is not displays under 'Form#' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_ItemDescfield1_Path).trim().contains("Description"),
				"This is Item Description is not displayed Item Description column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Statefield1_Path).trim().equalsIgnoreCase("IL"),
				"IL is not displays under 'State' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Qtyfield1_Path).trim().equalsIgnoreCase("1"),
				" '1' is not displays under 'Qty' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Shippedfield1_Path).trim().equalsIgnoreCase("0"),
				" '0' is not displays under 'Shipped' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_BackOrderfield1_Path).trim().equalsIgnoreCase("0"),
				" '0' is not displays under 'backorder' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Printfield1_Path).trim().equalsIgnoreCase("$0.01"),
				" '$0.01' is not displays under 'Print' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Fulfillmentfield1_Path).trim().equalsIgnoreCase("$0.53"),
				" '$0.53' is not displays under 'Fulfillment' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Chargebackfield1_Path).trim().equalsIgnoreCase("$0.00"),
				" '$0.00' is not displays under 'Chargeback' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Shippingfield1_Path).trim().equalsIgnoreCase("$0.00"),
				" '$0.00' is not displays under 'Shipping' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Totalfield1_Path).trim().equalsIgnoreCase("$0.54"),
				" '$0.54' is not displays under 'Total' column");
		softAssert.assertTrue(Get_Text(locatorType, Manage_IT_Prooffield1_Path).trim().equalsIgnoreCase(""),
				" '' is not displays under 'Proof' column");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_IT_Update_btn_Path),
				"Update Button is not Displayed in the Item Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_IT_Next_Btn_Path),
				"Next Button is not Displayed in the Item Tab");

		// Shipping Tab
		// ExplicitWait_Element_Clickable(locatorType,Manage_IT_Update_btn_Path,driver);
		Click(locatorType, Manage_ShippingTab_Path);
		Thread.sleep(4000);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Manage_ShippingTab_Path),
				"Shipping Tab is not Displayed in the Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_Address_Path),
				"Address field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_ShippingInst_Path),
				"Shipping Instruction field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_ReqDelivery_Path),
				"Required Delivery field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_ShipMethod_Path),
				"Ship method field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_ShippingInfo_Path),
				"Shipping information field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_TrackingNumber_Path),
				"Tracking Number field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_UpgradedShipping_Path),
				"Upgrade Shipping field is not displayed in Shipping Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_SP_Next_Btn_Path),
				"Next button is not displayed in Shipping Tab");

		// Mail List Recipients Tab

		Click(locatorType, Manage_MailListRecipeintsTab_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_MLP_MailListFileForProduction_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Manage_MailListRecipeintsTab_Path),
				"Mail List Recipients Tab is not Displayed in the Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Trackingnumberfield_Path),
				"Tracking Number field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_FirstNamefield_Path),
				"First Name field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_lastNamefield_Path),
				"Last Namefield is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Companyfield_Path),
				"Company field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Address1field_Path),
				"Address field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Cityfield_Path),
				"City field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Statefield_Path),
				"State field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Zipfield_Path),
				"Zip field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Phonefield_Path),
				"Phone field is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_TrackinNumberfield_Path),
				"Tracking Numnbefield is not displayed in Mail List Recipients Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_MLP_Next_btn_Path),
				"Next button is not displayed in Mail List Recipients Tab");

		// Attachments Tab

		Click(locatorType, Manage_AttachmentsTab_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_AT_Save_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Manage_AttachmentsTab_Path),
				"Attachment Tab is not displayed in the Attachment Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_AT_AttachmentTypeField_Path),
				"Attachment type field is not displayed in the Attachment Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_AT_CreateuserField_Path),
				"Create User field is not displayed in the Attachment Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_AT_CreateDateField_Path),
				"Create Date field is not displayed in the Attachment Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_AT_LinkToFileField_Path),
				"Link to File field is not displayed in the Attachment Tab");
		softAssert.assertTrue(Element_Is_Displayed(locatorType, Manage_AT_Save_btn_Path),
				"Save button is not displayed in the Attachment Tab");
		softAssert.assertAll();

	}

	@Test(enabled = true, priority = 6, dependsOnMethods = { "Pre_Req_Order" })
	public void GATC_2_4_9_1_5() throws InterruptedException {

		// Validate that Select order copy functionality works appropriately

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Orders_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);
		Type(locatorType, Manage_SelectOrder_Path, OrderNumber);
		Thread.sleep(80000);
		Click(locatorType, Manage_Copy_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("span", Part1.toUpperCase())),
				Part1 + " is not displayed");
		Click(locatorType, Textpath("a", "Remove"));
		ExplicitWait_Element_Visible(locatorType, Textpath("div", "Item(s) have been removed from your cart."));
		Assert.assertTrue(
				Element_Is_Displayed(locatorType, Textpath("div", "Item(s) have been removed from your cart.")),
				"Item(s) have been removed from your cart. is not displayed");

	}

	@Test(enabled = true, priority = 7, dependsOnMethods = { "Pre_Req_Order" })

	public void GATC_2_4_9_1_6() throws InterruptedException {

		// Create Order Template

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Orders_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);
		Type(locatorType, Manage_SelectOrder_Path, OrderNumber);
		Thread.sleep(80000);
		Click(locatorType, Manage_Copy_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Next_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("div", "Successfully Added 1 Items to Cart")),
				"Successfully Added 1 Items to Cart is not displayed");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("span", Part1.toUpperCase())),
				Part1 + " is not displayed");

		// Create order Template
		Click(locatorType, Checkout_CreateOrder_template_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_CreateOrder_template_Popup_Path),
				"Create Order Template Pop up is not displayed");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Template Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Reporter.log("Create Template is successfully done");
		Thread.sleep(3000);
		Assert.assertTrue(Get_Text(locatorType, Checkout_CreateOrder_template_Message_Path)
				.equalsIgnoreCase(CreateTemplate_Message2), "Create template order message is Mismatched");

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Template Test"),
				"QA Template Test is not displayed in the Order Widget template");
	}

	@Test(enabled = false, priority = 9, dependsOnMethods = { "Pre_Req_Order" })
	public void GATC_2_4_9_1_7() throws InterruptedException {

		// Validate Collaboration works appropriately

		Clearcart();
		MouseHover(locatorType, Admin_btn_Path);
		Click(locatorType, Support_Path);
		ExplicitWait_Element_Clickable(locatorType, SupportTicket_Edit_Btn_Path);
		Click(locatorType, AddTicket_Icon_path);
		ExplicitWait_Element_Clickable(locatorType, AddNewTicket_Btn_Path);
		Select_DropDown(locatorType, Cateogory_Dropdown_Path, "Order Requests");
		Thread.sleep(2000);
		Select_DropDown(locatorType, SubCateogory_Dropdown_Path, "Incorrect Order Received");
		Thread.sleep(2000);
		Type(locatorType, Ordernumberfield_Path, OrderNumber);
		Thread.sleep(3000);
		Click(locatorType, AddNewTicket_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, AddNewTicketupdate_Popup_Path),
				"'Support Ticket <Ticket #> has been Successfully Saved!' popup is not displayed");
		Click(locatorType, AddNewTicketupdate_Popup_OK_Path);
		Reporter.log("Order Cancel request successfully done for the Collaboration Test");

		// To verify Collaboration
		MouseHover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Orders_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_View_Btn_Path);
		Type(locatorType, Manage_Order_Num_Path, OrderNumber);
		Click(locatorType, Manage_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Manage_Collaboration_Btn_Path);
		Click(locatorType, Manage_Collaboration_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, UpdateTicket_Btn_Path);
		Click(Xpath, Containstextpath("a", "Add Comment"));
		Wait_ajax();
		Type(locatorType, Comment_Textfield_Path, "This is test");
		Click(locatorType, CreateComment_btn_Path);
		Wait_ajax();
		Click(locatorType, UpdateTicket_Btn_Path);
		Wait_ajax();
		Click(Xpath, Textpath("a", "OK"));
		Wait_ajax();

	}

	@Test(enabled = true, priority = 1)
	public void Pre_Req_Order() throws InterruptedException {

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part1);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part1), Part1 + "is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(3000);
		Assert.assertTrue(Get_Text(locatorType, Add_Cart_Message_Path).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path);
		PlaceanOrder("Pre-Req Order Number");

	}

	@BeforeClass(enabled = true)
	public void beforeClass() throws IOException, InterruptedException {

		// Place an Order
		login();

	}

	@BeforeMethod(enabled = false)
	public void BeforeMethod() throws InterruptedException {

		OpenUrl_Window_Max(URL);
		Type(locatorType, Username_Path, UserName);
		Type(locatorType, Password_Path, Password);
		Click(locatorType, Login_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Logout_Path));
		Click(locatorType, Clear_Cart_Path);
		Thread.sleep(3000);

	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

	@BeforeMethod
	public void beforeMethod() {
		softAssert = new SoftAssert();
	}

}
