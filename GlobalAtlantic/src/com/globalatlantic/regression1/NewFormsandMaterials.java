package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.globalatlantic.Base.BaseTest;

public class NewFormsandMaterials extends BaseTest {

	@Test
	public void GA_TC_2_7_2_1_1() throws InterruptedException, IOException {

		// Validating number of new forms and materials items that appear in
		// widget

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Hover(locatorType, Add_Widget_Path);
		String Widgetname = Get_Text(locatorType, New_Form_Path);
		Thread.sleep(5000);
		Click(locatorType, New_Form_Path);
		Thread.sleep(5000);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, Wid_Xpath(Widgetname, NewForm_SelectAn_Agent_Path));

		// contact search

		ContactSearch(Firstname, Form_lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Type(locatorType, Wid_Xpath(Widgetname, Item1_Qty_path), Qty);
		Thread.sleep(5000);
		Click(locatorType, Wid_Xpath(Widgetname, Form_Checkout_btn_path));
		Thread.sleep(5000);
		// Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path,
		// driver).equalsIgnoreCase("Checkout (1)"),
		// "Incorrectly displayed in the Checkout button ");
		Thread.sleep(5000);
		// place an order after pressing checkout button
		PlaceanOrder("GATC_2_7_2_1_1");
		Click(locatorType, Ok_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		// logout();

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}
