package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Validate_Fulfillmentsearch extends BaseTest {

	@Test
	public void GATC_2_9_1_1() {

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch(Firstname, Lastname);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part1),
				Part1 + "is not displayed");

	}

	@BeforeClass(enabled = true)
	public void BeforClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = false)
	public void AfteClass() throws IOException, InterruptedException {
		logout();

	}

}
