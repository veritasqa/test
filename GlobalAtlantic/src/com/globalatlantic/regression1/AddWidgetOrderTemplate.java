package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class AddWidgetOrderTemplate extends BaseTest {

	@Test(priority = 1, enabled = true)
	public void GA_TC_2_7_7_2_1() throws InterruptedException {

		// Add to Cart
		// -Create order

		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part1);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part1), Part1 + "is not displayed");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(3000);
		Assert.assertTrue(Get_Text(locatorType, Add_Cart_Message_Path).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Type(locatorType, Search_For_Materials_Path, Part5);
		Click(locatorType, Search_For_Materials_Btn_Path);
		ExplicitWait_Element_Not_Visible(locatorType, Loading_Path);
		Click(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part5), Part5 + " is not displayed");
		Thread.sleep(2000);
		Assert.assertTrue(Get_Text(locatorType, Add_Cart_Message_Path).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Thread.sleep(2000);
		Assert.assertFalse(Element_Is_Displayed(locatorType, Mini_Shoppin_Empty_Message),
				"Mini Shopping Cart is Empty");

		Assert.assertTrue(Element_Is_Displayed(locatorType, Mini_Shoppin_cart_Item_path),
				Part1 + " is not displayed in the Shopping Cart");

		Assert.assertTrue(Get_Text(locatorType, Mini_Shoppin_cart_Item_path).equalsIgnoreCase(Part1),
				Part1 + " is displayed incorrectly in the Shopping Cart");
		Assert.assertTrue(Get_Attribute(locatorType, Mini_Shoppin_cart_Item1Qty_path, "value").equalsIgnoreCase("1"),
				"Quantity 1 is not displayed fot " + Part1 + "in the Shopping Cart");

		Assert.assertTrue(Element_Is_Displayed(locatorType, Mini_Shoppin_cart_Item2_path),
				Part5 + "is not displayed in the Shopping Cart");
		Assert.assertTrue(Get_Text(locatorType, Mini_Shoppin_cart_Item2_path).equalsIgnoreCase(Part5),
				Part5 + "is displayed incorrectly in the Shopping Cart");
		Assert.assertTrue(Get_Attribute(locatorType, Mini_Shoppin_cart_Item2Qty_path, "value").equalsIgnoreCase("1"),
				"Quantity 1 is not displayed fot " + Part5 + "in the Shopping Cart");
		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);

		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_CreateOrder_template_Popup_Path),
				"Create Order Template Pop up is not displayed");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Order Template");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_CreateOrder_template_Message_Path),
				"'QA Template Test order template has been successfully created.' message is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_CreateOrder_template_Message_Path)
				.equalsIgnoreCase(CreateTemplate_Message), "Create template order message is Mismatched");

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test Template is not displayed in the Order Widget template");
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Template1AddtoCart_Btn_Path));

		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		PlaceanOrder("GATC_2_7_7_1_2");
		Click(locatorType, Logout_Path);

	}

	@Test(priority = 6, enabled = true, dependsOnMethods = { "GA_TC_2_7_7_2_1" })

	public void GATC_2_7_7_1_3() throws IOException, InterruptedException {

		// "Delete an Order Template"

		OpenUrl_Window_Max(URL);
		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Announcement_view_btn_Path), "Home Page is not displayed");

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ContactSearch_Title_Path),
				"Contact Search button is not displayed");
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Announcement_view_btn_Path), "Home Page is not displayed");

		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test Template is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Click(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Name_Path),
				"QA Test Template is displayed after delete action");
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Announcement_view_btn_Path), "Home Page is not displayed");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplate_Template1Name_Path),
				"QA Test Template is displayed after delete action");
		Reporter.log("Order Template has been deleted");
		Clearcart();
		Assert.assertTrue(Element_Is_Displayed(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path)),
				"Select Agent link is not displayed in the Order Template Widget after the clear cart");
	}

	@Test(priority = 5, enabled = true)

	public void GFM_VIP_1_1_2_4() {

		// "Validate that an order can be Cancelled"

		OpenUrl_Window_Max(Inventory_URL);
		Type(locatorType, Inventory_Username_Path, Inventory_UserName);
		Type(locatorType, Inventory_Passowrd_Path, Inventory_Password);
		Click(locatorType, Inventory_Submit_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, Inventory_OrderSearch_Path);
		Click(locatorType, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path);

		Select_DropDown(locatorType, Inventory_CustomerID_Path, "Forethought");
		Type(locatorType, Inventory_Ordernumber_Path, OrderNumber);
		Click(locatorType, Inventory_Search_Btn_Path);

		Assert.assertTrue(Get_Text(locatorType, Inventory_SearchResult_Ordernumber).equalsIgnoreCase(OrderNumber),
				"Order Number is Mismatching in the inventory search Results");
		Click(locatorType, Inventory_Edit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Inventory_Cancelorder_Btn_Path);

		Assert.assertTrue(Element_Is_Displayed(locatorType, Inventory_EditPage_Title_Path),
				"Edit Order page is not displayed");
		Click(locatorType, Inventory_Cancelorder_Btn_Path);
		Assert.assertTrue(isAlertPresent(), "Cancel Order Alert pop up is not displayed");
		Accept_Alert();

		Assert.assertTrue(Get_Attribute(locatorType, Inventory_Status_Path, "value").equalsIgnoreCase("Cancelled"),
				"Order Status is not changed to 'Cancelled'");
		Assert.assertTrue(Get_Text(locatorType, Inventory_CancelMessage_Path).equalsIgnoreCase("Order Cancelled."),
				"'Cancel Order' message is not displayed");

		Reporter.log(OrderNumber + " has been cancelled");

		Click(locatorType, Inventory_OrderSearch_Path);
		ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path);
		Type(locatorType, Inventory_Ordernumber_Path, OrderNumber);
		Click(locatorType, Inventory_Search_Btn_Path);

		Assert.assertTrue(Get_Text(locatorType, Inventory_SearchResult_Ordernumber).equalsIgnoreCase(OrderNumber),
				"Order Number is Mismatching in the inventory search Results");

		Assert.assertTrue(Get_Text(locatorType, Inventory_SearchResult_Status_Path).equalsIgnoreCase("Cancelled"),
				"Status is not Cancelled in the Inventory");

		Click(locatorType, Inventory_Logout_Path);
	}

	@BeforeClass(enabled = true)
	public void BeforeTest() throws IOException, InterruptedException {
		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Element_Is_Present(locatorType, Announcement_view_btn_Path), "Home Page is not displayed");

		Hover(locatorType, Add_Widget_Path);
		Widgetname = Get_Text(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, ContactSearch_Title_Path),
				"Contact Search button is not displayed");
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

		while (Element_Is_Displayed(locatorType, OrderTemplate_Template1View_Btn_Path)) {

			Click(locatorType, OrderTemplate_Template1View_Btn_Path);
			ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
			Click(locatorType, OrderTemplatePage_Delete_btn_Path);
			Click(locatorType, Home_btn_Path);
			ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);

		}
		Clearcart();
	}

	@AfterClass(enabled = false)
	public void AfterClass() throws IOException, InterruptedException {
		logout();

	}

}