package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class AddWidget_ExpChkOut extends BaseTest {

	@Test(enabled = true)

	public void GATC_2_7_1_1_1() throws InterruptedException {

		Hover_Over_Element(locatorType, Add_Widget_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Select_agent_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(Firstname, Form_lastname);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_num1_Path), Part1, driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Form_Suggestion_Path), driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), Qty, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);

		PlaceanOrder("GATC_2_7_1_1_1");

		Click_On_Element(locatorType, Ok_Btn_Path, driver);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

	}

	@Test(enabled = true)

	public void GATC_2_7_1_1_3() {

		Thread_Sleep(3000);
		Clearcart();
		Hover_Over_Element(locatorType, Add_Widget_Path, driver);
		// Thread.sleep(500);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Select_agent_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(No_Firm_IL_Name, No_Firm_IL_Last_Name);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_num1_Path), Part3, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), Qty, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);
		Thread_Sleep(3000);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);
		ExplicitWait_Element_Visible(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver);

		System.out.println(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver));
		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver)
				.equalsIgnoreCase(Invalid_Form_Message), "Message not Displayed ");

	}

	@Test(enabled = true)

	public void GATC_2_7_1_1_4() {

		Thread_Sleep(3000);
		Clearcart();
		Hover_Over_Element(locatorType, Add_Widget_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Select_agent_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(Firm_Restriction_Name, Firm_Restriction_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_num1_Path), Part2, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), Qty, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);

		ExplicitWait_Element_Visible(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver);
		System.out.println(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver));

		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver)
				.equalsIgnoreCase(Invalid_Form_Message), "Message not Displayed ");

	}

	@Test(enabled = true)

	public void GATC_2_7_1_1_5() {
		Thread_Sleep(3000);

		Clearcart();

		Hover_Over_Element(locatorType, Add_Widget_Path, driver);
		// Thread.sleep(500);
		Widgetname = Get_Text(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Express_Checkout_Path, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_Select_agent_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path, driver);

		ContactSearch(STATEFIRMRESTRICTION_Name, STATEFIRMRESTRICTION_last_Name);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_num1_Path), Part4, driver);

		Clear_Type_Charecters(locatorType, Wid_Xpath(Widgetname, Express_Form_Qty1_Path), Qty, driver);

		Click_On_Element(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);

		ExplicitWait_Element_Clickable(locatorType, Wid_Xpath(Widgetname, Express_checkout_Btn_Path), driver);

		ExplicitWait_Element_Visible(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver);

		System.out.println(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver));

		Assert.assertTrue(Get_Text(locatorType, Wid_Xpath(Widgetname, Express_ErrorMessage_Path), driver)
				.equalsIgnoreCase(Invalid_Form_Message), "Message not Displayed ");

	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		//logout();

	}

}
