package com.globalatlantic.regression1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;


public class Inventory extends BaseTest{
	
 @Test 
	
public void GFM_VIP_1_1_2_x () throws InterruptedException{
	
	 //Place an order
	 

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part1, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch(Firstname, Lastname);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part1),
				Part1 + "is not displayed");

		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(3000);
		// System.out.println("Message
		// is"+Get_Text(locatorType,Add_Cart_Message_Path,driver));
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");

		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Thread.sleep(5000);

		PlaceanOrder("GFM_VIP_1_1_2_x");

		Click(locatorType, Ok_Btn_Path, driver);

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);
		// logout();

	 
	 // Inventory Management
	 
	 Open_Browser(Browser,Browserpath);
	 Open_Url_Window_Max(Inventory_URL,driver);
	 Type( locatorType,Inventory_Username_Path, Inventory_UserName, driver);
	 Type( locatorType,Inventory_Passowrd_Path, Inventory_Password, driver);
	 Click(locatorType,Inventory_Submit_Btn_Path,driver);
	 ExplicitWait_Element_Visible(locatorType, Inventory_OrderSearch_Path, driver);
	 Click(locatorType,Inventory_OrderSearch_Path,driver);
	 ExplicitWait_Element_Clickable(locatorType, Inventory_Search_Btn_Path, driver);
	 Type( locatorType,Inventory_Ordernumber_Path, OrderNumber, driver);
	 Click(locatorType,Inventory_Search_Btn_Path,driver);
	 Click(locatorType,Inventory_Edit_Btn_Path,driver);
	 Click(locatorType,Inventory_Cancelorder_Btn_Path,driver);
	 Accept_Alert(driver);
	
	 Assert.assertTrue(Get_Attribute(locatorType,Inventory_Order_Status,"Value", driver).equalsIgnoreCase("Cancelled")
    		 ,"Order is not cancelled is not displayed");
	
	 Click(locatorType,Inventory_logout_btn_Path,driver);
	 ExplicitWait_Element_Visible(locatorType, Inventory_Username_Path, driver);


}
}
