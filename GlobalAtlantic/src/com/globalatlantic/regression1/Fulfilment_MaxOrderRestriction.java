package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfilment_MaxOrderRestriction extends BaseTest {

	@Test
	public void GATC_2_9_6_1() throws IOException, InterruptedException {

		// Validate max order quantity: Admin

		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);
		Type(locatorType, ManageInventory_FormNo_Path, Part42);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Click(locatorType, ManageInventory_RulesTab_Path);
		Type(locatorType, ManageInventory_Rules_DefaultMaxOrderQty_Path, "5");
		Click(locatorType, ManageInventory_Rules_Save_Btn_Path);
		Click(locatorType, ManageInventory_Rules_OK_Btn_Path);
		logout();
		login();
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part42);
		Click(locatorType, Fullfilment_Search_Btn_Path);

		// contact name

		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path).equalsIgnoreCase(Part42), Part42 + "is not displayed");
		Type(locatorType, Cart_Quantitiy_Path, "6");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(1000);
		Assert.assertTrue(Get_Text(locatorType, Add_Cart_Message_Path).contains("Max Order Quantity is 15"),
				"Max Order Quantity is 5" + "is not displayed");
		Type(locatorType, Cart_Quantitiy_Path, "3");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(1000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Successfully Added 3!")),
				"Successfully Added 3! is not displayed");
		Type(locatorType, Cart_Quantitiy_Path, "5");
		Click(locatorType, Add_To_Cart_btn_Path);
		Thread.sleep(1000);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("div", "Max Order Quantity is 5 (3 in cart)")),
				"Max Order Quantity is 5 (3 in cart) is not displayed");
	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		if (Element_Is_Present(locatorType, Username_Path)) {
			Type(locatorType, Username_Path, MaxUserName);
			Type(locatorType, Password_Path, MaxUserName);
			Click(locatorType, Login_btn_Path);
			Assert.assertTrue(Element_Is_Present(locatorType, Logout_Path));
			Implicit_Wait();
		}

		else {
			logout();
			Type(locatorType, Username_Path, MaxUserName);
			Type(locatorType, Password_Path, MaxUserName);
			Click(locatorType, Login_btn_Path);
			Assert.assertTrue(Element_Is_Present(locatorType, Logout_Path));
			Implicit_Wait();
		}

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		// logout();

	}
}
