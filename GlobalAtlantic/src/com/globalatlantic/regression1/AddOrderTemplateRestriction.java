package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import com.globalatlantic.Base.BaseTest;

public class AddOrderTemplateRestriction extends BaseTest {

	public String Ordertemplateitems(String Partname) {
		return ".//span[text()='" + Partname + "']";
	}

	public void Deletetemplate(String ContactFname, String ContactLname) throws InterruptedException {

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Hover(locatorType, Add_Widget_Path);
		Widgetname = Get_Text(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(ContactFname, ContactLname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		while (Element_Is_Displayed(locatorType, OrderTemplate_Template1View_Btn_Path)) {
			Click(locatorType, OrderTemplate_Template1View_Btn_Path);
			ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
			if (Element_Is_Displayed(locatorType, OrderTemplatePage_Delete_btn_Path)) {
				Click(locatorType, OrderTemplatePage_Delete_btn_Path);
				Click(locatorType, Home_btn_Path);
				ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
			} else {
				break;
			}
		}
		Clearcart();
	}

	@Test(enabled = true, priority = 1)
	public void GATC_2_7_7_1_3() throws InterruptedException {

		// State Restrictions - Exclude

		// Pre-Req

		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part15);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(No_Firm_IL_Name, No_Firm_IL_Last_Name);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		Thread.sleep(2000);
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test cases starts here

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_AL_State, Lastname_AL_State);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part15 + " is displays on the 'Order Template' page");
		Clearcart();
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part15),
				Part15 + " is not displayed in Order Template Page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_Shippinginfo_Path),
				"Shipping info is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part15),
				Part15 + " is not displayed in checkout page");
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Click(locatorType, OrderTemplatePage_Delete_btn_Path);
	}

	@Test(enabled = true, priority = 2)
	public void GATC_2_7_7_1_4() throws InterruptedException {

		// Firm Restrictions - Exclude

		// Pre-Req
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part16);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname_53_Firm, Lastname_53_Firm);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test cases starts here

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firm_Restriction_Name, Firm_Restriction_last_Name);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				Part16 + " is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		// ExplicitWait_Element_Clickable(locatorType,
		// OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part16 + " is displays on the 'Order Template' page");
		Clearcart();

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_53_Firm, Lastname_53_Firm);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				Part16 + " is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part16),
				Part16 + " is not displayed in Order Template Page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_Shippinginfo_Path),
				"Shipping info is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part16),
				Part16 + " is not displayed checkout page");

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Click(locatorType, OrderTemplatePage_Delete_btn_Path);

	}

	@Test(enabled = true, priority = 3)
	public void GATC_2_7_7_1_3_temp() throws InterruptedException {

		// StateFirm Restrictions - Exclude

		// Pre-Req
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part17);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname_53_Firm, Lastname_53_Firm);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test cases starts here

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_OR_AG_StateFirm, Lastname_OR_AG_StateFirm);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part17 + " is displays on the 'Order Template' page");
		Clearcart();

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_53_Firm, Lastname_53_Firm);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				Part17 + " is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part17),
				Part17 + " is not displayed in Order Template Page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_Shippinginfo_Path),
				"Shipping info is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part17),
				Part17 + " is not displayed checkout page");

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Click(locatorType, OrderTemplatePage_Delete_btn_Path);

	}

	@Test(enabled = false, priority = 4)
	public void GATC_2_7_7_1_1() throws InterruptedException {

		// Obsolete/Unobsolete

		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part1);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Type(locatorType, Search_For_Materials_Path, Part18);
		Thread.sleep(2000);
		Click(locatorType, Search_For_Materials_Btn_Path);
		ExplicitWait_Element_Not_Visible(locatorType, Loading_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Obsolete Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Obsolete Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		// Thread.sleep(2000);
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		// Clearcart();

		// Test Case Starts here

		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Obsolete Test"),
				"QA Obsolete Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Ordertemplateitems(Part1.toUpperCase())),
				Part1 + " is not displays on the 'Order Template' page");
		Assert.assertTrue(Element_Is_Displayed(locatorType, Ordertemplateitems(Part18.toUpperCase())),
				Part18 + " is not displays on the 'Order Template' page");
		Clearcart();
		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);
		Type(locatorType, ManageInventory_FormNo_Path, Part18);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(locatorType, MI_Searchresult_FormNo).equalsIgnoreCase(Part18),
				Part18 + " is not displayed in the search results");
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);

		Click(locatorType, ManageInventory_RulesTab_Path);
		Type(locatorType, ManageInventory_Rules_ObsoleteDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Click(locatorType, ManageInventory_Save_OK_Btn_Path);
		Reporter.log("Copied New part '" + Part18 + "' is Obsoleted ");
		Click(locatorType, Logout_Path);

	}

	@Test(enabled = false, priority = 5)
	public void GATC_2_7_7_1_1_AfterObsolete() throws IOException, InterruptedException {

		// GATC_2_7_7_1_4 continues

		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Hover(locatorType, Add_Widget_Path);
		Widgetname = Get_Text(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Obsolete Test"),
				"QA Obsolete Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		// ExplicitWait_Element_Clickable(locatorType,
		// OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Ordertemplateitems(Part1.toUpperCase())),
				Part1 + " is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, Ordertemplateitems(Part18.toUpperCase())),
				Part18 + " is displays on the 'Order Template' page after obsoleted");

		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Clearcart();

		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);
		Type(locatorType, ManageInventory_FormNo_Path, Part18);
		Click(locatorType, ManageInventory_Active_Icon_Path);
		Wait_ajax();
		// Thread.sleep(500);
		Click(locatorType, ManageInventory_Active_Icon_Path);
		Click(locatorType, li_value("- Any -"));
		Wait_ajax();
		// Thread.sleep(500);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);

		Click(locatorType, ManageInventory_RulesTab_Path);
		Click(locatorType, ManageInventory_Rules_UnObsoletenow_Path);
		Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Click(locatorType, ManageInventory_Save_OK_Btn_Path);
		Reporter.log("Copied New part '" + Part18 + "' is UnObsoleted ");
		Click(locatorType, Logout_Path);

	}

	@Test(enabled = false, priority = 6)
	public void GATC_2_7_7_1_2() throws IOException, InterruptedException {

		// Replace

		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, QA_KITREPLACE1);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(QA_KITREPLACE1),
				QA_KITREPLACE1 + " is not displayed checkout page");
		Click(locatorType, Checkout_ShowComponentDetails_Btn_Path);
		Switch_To_Iframe("oRadWindowComponentDetails");
		ExplicitWait_Element_Visible(locatorType, Textpath("span", "QA_REPLACE_TEST_1"));
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("span", "QA_REPLACE_TEST_1")),
				"'QA_Replace_Test_1' is not displayed in 'Component Details | Global Atlantic // Veritas Superfulfillment' window");
		Switch_To_Default();
		Click(locatorType, Containspath("a", "title", "Close"));

		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "Replace Order Template Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "Replace Order Template Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(
				Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("Replace Order Template Test"),
				"Replace Order Template Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				QA_KITREPLACE1 + " is displays on the 'Order Template' page");
		Clearcart();

		Hover(locatorType, Admin_btn_Path);
		Click(locatorType, Manage_Inventory_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Visible(locatorType, ManageInventory_BusinessOwner_Path);
		Type(locatorType, ManageInventory_FormNo_Path, Part19);
		Click(locatorType, ManageInventory_Search_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		Assert.assertTrue(Get_Text(locatorType, MI_Searchresult_FormNo).equalsIgnoreCase(QA_REPLACE_TEST_1),
				QA_REPLACE_TEST_1 + " is not displayed in the search results");
		Click(locatorType, ManageInventory_ResultsEdit_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Click(locatorType, ManageInventory_RulesTab_Path);
		Click(locatorType, ManageInventory_Rules_ReplaceType_Path);
		Wait_ajax();
		Click(locatorType, li_value("Date"));
		Thread.sleep(500);
		Type(locatorType, ManageInventory_Rules_ReplaceWith_Field_Path, QA_REPLACE_TEST_2);
		Wait_ajax();
		Type(locatorType, ManageInventory_Rules_ReplaceDate_Path, Get_FutureCSTTime("MM/dd/YYYY HH:mm:ss"));
		Click(locatorType, ManageInventory_Gen_Save_Btn_Path);
		Click(locatorType, ManageInventory_Save_OK_Btn_Path);
		Reporter.log(QA_REPLACE_TEST_2 + " is replaced with " + QA_REPLACE_TEST_2);
		Click(locatorType, Logout_Path);

	}

	@Test(enabled = false, priority = 7)
	public void GATC_2_7_7_1_2_AfterReplace() throws IOException, InterruptedException {

		login();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Hover(locatorType, Add_Widget_Path);
		Widgetname = Get_Text(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, OrderTemplate_Widget_Path);
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(
				Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("Replace Order Template Test"),
				"Replace Order Template Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Get_Text(locatorType, Ordertemplateitems(QA_KITREPLACE1.toUpperCase()))
				.equalsIgnoreCase(QA_KITREPLACE1), QA_KITREPLACE1 + " is displays on the 'Order Template' page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(QA_KITREPLACE1),
				QA_KITREPLACE1 + " is not displayed in checkout page");
		Click(locatorType, Checkout_ShowComponentDetails_Btn_Path);
		Switch_To_Iframe("oRadWindowComponentDetails");
		ExplicitWait_Element_Visible(locatorType, Textpath("span", "QA_REPLACE_TEST_2"));
		Assert.assertTrue(Element_Is_Displayed(locatorType, Textpath("span", "QA_REPLACE_TEST_1")),
				"'QA_Replace_Test_2' is not displayed in 'Component Details | Global Atlantic // Veritas Superfulfillment' window");
		Switch_To_Default();

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Click(locatorType, OrderTemplatePage_Delete_btn_Path);
		Clearcart();

	}

	@Test(enabled = true, priority = 8)
	public void GATC_2_7_7_1_6() throws InterruptedException {

		// State Restrictions - Include

		// Pre-Req
		Clearcart();
		Deletetemplate(Firstname_AZ_State, Lastname_AZ_State);
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part21);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname_AZ_State, Lastname_AZ_State);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();

		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test cases starts here

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname, Lastname);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part21 + " is displays on the 'Order Template' page");
		Clearcart();

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_AZ_State, Lastname_AZ_State);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part21),
				Part21 + " is not displayed in Order Template Page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_Shippinginfo_Path),
				"Shipping info is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part21),
				Part21 + " is not displayed in checkout page");
		Clearcart();
		Deletetemplate(Firstname_AZ_State, Lastname_AZ_State);
	}

	@Test(enabled = true, priority = 9)
	public void GATC_2_7_7_1_7() throws InterruptedException {

		// Firm Restrictions - Include

		// Pre-Req
		Clearcart();
		Deletetemplate(Firm_Restriction_Name, Firm_Restriction_last_Name);
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part22);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firm_Restriction_Name, Firm_Restriction_last_Name);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test cases starts here

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_53_Firm, Lastname_53_Firm);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part22 + " is displays on the 'Order Template' page");
		Clearcart();

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firm_Restriction_Name, Firm_Restriction_last_Name);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part22),
				Part22 + " is not displayed in Order Template Page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_Shippinginfo_Path),
				"Shipping info is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part22),
				Part22 + " is not displayed in checkout page");
		Clearcart();
		Deletetemplate(Firm_Restriction_Name, Firm_Restriction_last_Name);

	}

	@Test(enabled = true, priority = 10)
	public void GATC_2_7_7_1_8() throws InterruptedException {

		// StateFirm Restrictions - Include

		// Pre-Req
		Clearcart();
		Deletetemplate(Firm_Restriction_Name, Firm_Restriction_last_Name);
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part23);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(Firstname_AZ_AG_StateFirm, Lastname_AZ_AG_StateFirm);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, "QA Test");
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, "QA Test");
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test cases starts here

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_53_Firm, Lastname_53_Firm);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertFalse(Element_Is_Displayed(locatorType, OrderTemplatePage_Part1_Path),
				Part23 + " is displays on the 'Order Template' page");
		Clearcart();
		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(Firstname_AZ_AG_StateFirm, Lastname_AZ_AG_StateFirm);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase("QA Test"),
				"QA Test is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part23),
				Part23 + " is not displayed in Order Template Page");
		Click(locatorType, OrderTemplatePage_addtocart_Part1_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, Checkout_Shippinginfo_Path),
				"Shipping info is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_Formnum_Path).equalsIgnoreCase(Part23),
				Part23 + " is not displayed in checkout page");
		Clearcart();
		Deletetemplate(Firstname_AZ_AG_StateFirm, Lastname_AZ_AG_StateFirm);

	}

	@Test(enabled = false, priority = 11)
	public void GATC_2_7_7_1_9() throws InterruptedException {

		// Verify Order Template Widget

		// Pre-Req
		Clearcart();
		Deletetemplate(Firstname_AZ_Al_StateFirm, Lastname_AZ_AL_StateFirm);
		ExplicitWait_Element_Clickable(locatorType, Fullfilment_Search_Btn_Path);
		Type(locatorType, Fullfilment_Search_Path, Part15);
		Click(locatorType, Fullfilment_Search_Btn_Path);
		ContactSearch(No_Firm_IL_Name, No_Firm_IL_Last_Name);
		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path);
		Click(locatorType, Add_To_Cart_btn_Path);

		Click(locatorType, Mini_Shoppin_Checkout_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Checkout_CreateOrder_template_Path);
		Click(locatorType, Checkout_CreateOrder_template_Path);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Name_Path, Part15);
		Type(locatorType, Checkout_CreateOrder_template_Popup_Desc_Path, Part15);
		Click(locatorType, Checkout_CreateOrder_template_Popup_Create_btn_Path);
		Wait_ajax();
		// Thread.sleep(2000);
		Click(locatorType, Home_btn_Path);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Clearcart();

		// Test Case Starts

		Click(locatorType, Wid_Xpath(Widgetname, OrderTemplate_Selectagent_Path));
		ExplicitWait_Element_Clickable(locatorType, Search_btn_Path);
		ContactSearch(No_Firm_IL_Name, No_Firm_IL_Last_Name);
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path);
		Assert.assertTrue(Get_Text(locatorType, OrderTemplate_Template1Name_Path).equalsIgnoreCase(Part15),
				Part15 + " is not displayed in the Order Widget template");
		Click(locatorType, OrderTemplate_Template1View_Btn_Path);
		ExplicitWait_Element_Clickable(locatorType, OrderTemplatePage_Delete_btn_Path);
		Assert.assertTrue(Element_Is_Displayed(locatorType, OrderTemplatePage_Title_Path),
				"Order Templates page is not displays on the 'Order Template' page");
		Assert.assertTrue(Get_Text(locatorType, OrderTemplatePage_Part1_Path).equalsIgnoreCase(Part15),
				Part15 + " is not displayed in Order Template Page");
		Clearcart();
		Deletetemplate(No_Firm_IL_Name, No_Firm_IL_Last_Name);

	}

	@BeforeTest(enabled = true)

	public void BeforeTest() throws IOException, InterruptedException {

		login();
		Deletetemplate(Firstname, Lastname);
	}

}
