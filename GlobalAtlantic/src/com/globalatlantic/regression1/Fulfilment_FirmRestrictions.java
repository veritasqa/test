package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfilment_FirmRestrictions extends BaseTest {

	public void NoRecord(String Part, String RecipientFirstname, String RecipientLastname, String CityName,
			String StateName, String Zipnumber, String FirmName) throws InterruptedException {
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		// ContactSearch(Fname, Lname);

		CreateContactandclick(RecipientFirstname, RecipientLastname, CityName, StateName, Zipnumber, FirmName);

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);
		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part + " is displayed or no error message disyplayed ");

		Thread_Sleep(3000);
	}

	/*Validate that part with Firm Restriction X is not visible for a 
	contact with Firm Restriction Y*/
	@Test(enabled = false, priority = 1)
	public void GATC_2_9_3_1() throws InterruptedException {

		NoRecord(Part2, "GATC", "2.9.3.1", "Buffalo Grove", "Illinois", "60089", "Capital Investment");

	}

	/*Validate kit on the fly with firm restriction X, Y and Z has child component 1 with firm restriction X, 
	Child component 2 with firm restriction Y, Child component 3 with no firm restriction, and
	Child component 4 with firm restriction Z does not appear when contact has no firm restriction or firm restriction W*/
	@Test(enabled = false, priority = 2)
	public void GATC_2_9_3_4() throws InterruptedException {
		NoRecord(Part45, "GATC", "2.9.3.4_A", "Buffalo Grove", "Illinois", "60089", "Fairmount");
		NoRecord(Part45, "Qa", "Auto", "Buffalo Grove", "Illinois", "60089", "Nofirm");
	}

	/*  Validate kit on the fly ('Excluded' with firm restriction X, Y and Z has child component 1 
	 * 'Excluded' with firm restriction X, Child component 2 'Excluded' with firm restriction Y, 
	 *  Child component 3 with no firm restriction, and
		Child component 4 'Excluded' with firm restriction Z does not appear when contact has  
		firm restriction X,Y and Z */

	@Test(enabled = false, priority = 3)
	public void GATC_2_9_3_5() throws InterruptedException {
		NoRecord(Part46, "GATC", "2.9.3.5_A", "Buffalo Grove", "Illinois", "60089", "5/3");
		NoRecord(Part46, "GATC", "2.9.3.5_B", "Buffalo Grove", "Illinois", "60089", "East Lawn");
		NoRecord(Part46, "GATC", "2.9.3.5_C", "Buffalo Grove", "Illinois", "60089", "Capital Investment");

	}

	/*Validate kit on the fly ('Excluded' with firm restriction X, Y and Z has child component 1 'Excluded' 
	 * with firm restriction X, Child component 2 'Excluded' with firm restriction Y, 
	 * Child component 3 with no firm restriction, andChild component 4 'Excluded' 
	 * with firm restriction Z) appears when contact has no firm restriction or firm restriction W
	 * */

	@Test(enabled = false, priority = 4)
	public void GATC_2_9_3_9() throws InterruptedException {

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part46, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		NoRecord(Part46, "Qa", "Auto", "Buffalo Grove", "Illinois", "60089", "Nofirm");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part46),
				Part46 + "is not displayed");

	}

	/*Validate kit on the fly with firm restriction X, Y and Z has child component 1 with firm restriction X, 
	Child component 2 with firm restriction Y, Child component 3 with no firm restriction, and
	Child component 4 with firm restriction Z appears when contact has firm restriction x, y  and z and appropriate child items appear
	 * 
	 */

	@Test(enabled = true, priority = 4)
	public void GATC_2_9_3_10() throws InterruptedException {

		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part47, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_3_10", "Buffalo Groove", "Illinois", "60089", "East Lawn");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part47),
				Part47 + "is not displayed");

		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
	
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);

		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Click(locatorType, Qa_Kitfly_CmptDtls_Btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);

		PlaceanOrder("GATC_2_9_3_10");
		
		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part47, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_3_10_A", "Los Angeles", "California", "90001", "Fairmount");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part47),
				Part47 + "is not displayed");

		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
	
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Click(locatorType, Qa_Kitfly_CmptDtls_Btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);

		PlaceanOrder("GATC_2_9_3_10_A");
		
		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part47, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		CreateContactandclick("GATC_", "2_9_3_10_A", "Los Angeles", "California", "90001", "Capital Investment");

		ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path, driver);

		Assert.assertTrue(Get_Text(locatorType, Part_Name_Path, driver).equalsIgnoreCase(Part47),
				Part47 + "is not displayed");

		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Wait_ajax();
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");
		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Click(locatorType, Qa_Kitfly_CmptDtls_Btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);

		PlaceanOrder("GATC_2_9_3_10_A");
		
		Clearcart();

	}
	
	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

}
