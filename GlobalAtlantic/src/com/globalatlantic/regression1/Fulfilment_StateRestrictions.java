package com.globalatlantic.regression1;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.globalatlantic.Base.BaseTest;

public class Fulfilment_StateRestrictions extends BaseTest {

	@Test(enabled = true, priority = 1)
	public void GATC_2_9_2_1() {
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part3, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch(Firstname, Lastname);

		// ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path,
		// driver);
		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part3 + " is displayed or no error message disyplayed ");

		Thread_Sleep(3000);

	}

	@Test(enabled = true, priority = 2)
	public void GATC_2_9_2_4() {
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part33, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// contact name

		ContactSearch("QASTATEFIRM", "RESTRI");

		// ExplicitWait_Element_Clickable(locatorType, Add_To_Cart_btn_Path,
		// driver);
		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				Part33 + " is displayed or no error message disyplayed ");

		Thread_Sleep(3000);

	}

	@Test(enabled = true, priority = 3)
	public void GATC_2_9_2_5() {
		Clearcart();

		// Ist QA_BROCHURESTAT_NOSTATERESTRICTION and
		// QA_APPSTATIC_STATERESTRICTION_2
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part33, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// ContactName

		ContactSearch("GATC.", "2.9.2.5.A");
		Click(locatorType, Qa_Kitfly_Details_btn, driver);

		// Close_Alert(driver);
		ExplicitWait_Element_Clickable(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(3000);
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");

		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		PlaceanOrder("GATC_2_9_2_5_A");
		

		// 2nd QA_BROCHURESTAT_NOSTATERESTRICTION and
		// QA_APPSTATIC_STATERESTRICTION_1
		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part33, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// ContactName

		ContactSearch("GATC", "2.9.2.5.B");
		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(3000);
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");

		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		PlaceanOrder("GATC_2_9_2_5_B");
		
		// 3rd

		Clearcart();
		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part33, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// ContactName

		ContactSearch("QA", "Auto");
		Click(locatorType, Qa_Kitfly_Details_btn, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);
		
		Switch_To_defaultwindow(driver);

		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(3000);
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");

		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		PlaceanOrder("GATC_2_9_2_5_B");
		

	}

	@Test(enabled = true, priority = 4)
	public void GATC_2_9_2_9() {

		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part34, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);

		// ContactName

		ContactSearch("GATC", "2.9.2.5.B");
		// Click(locatorType,Qa_Kitfly_Details_btn,driver);

		/*	Assert.assertTrue(Get_Text(locatorType, Qa_Kitfly_Details_Option1, driver).trim()
					.contains("QA_BROCHURESTAT_NOSTATERESTRICTION"),
					" QA_BROCHURESTAT_NOSTATERESTRICTION is not  displayed ");
			Assert.assertTrue(Get_Text(locatorType, Qa_Kitfly_Details_Option2, driver).trim()
					.contains("QA_APPSTATIC_STATERESTRICTION_2"),
					" QA_APPSTATIC_STATERESTRICTION_2 is displayed");
			
			Click(locatorType,Qa_Kitfly_Details_Closebtn,driver); */
		Click(locatorType, Add_To_Cart_btn_Path, driver);
		Thread_Sleep(3000);
		Assert.assertTrue(
				Get_Text(locatorType, Add_Cart_Message_Path, driver).equalsIgnoreCase("Successfully Added 1!"),
				"Successfully Added 1! is not displayed");

		Assert.assertTrue(Get_Text(locatorType, Checkout_link_Path, driver).equalsIgnoreCase("Checkout (1)"),
				"Incorrectly displyed in the Checkout button ");
		Click(locatorType, Checkout_link_Path, driver);
		Thread_Sleep(3000);
		Click(locatorType, Show_Component_Details, driver);
		Switch_To_Iframe("oRadWindowComponentDetails", driver);

		// Click(locatorType,Qa_Kitfly_Details_Closebtn,driver);
		Switch_To_defaultwindow(driver);
		Click(locatorType, Qa_Kitfly_Details_Closebtn, driver);
		PlaceanOrder("GATC_2_9_2_9");

	}
	@Test(enabled = true, priority = 5)
	public void GATC_2_9_2_10() {
		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part35, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ContactSearch("QA", "Auto");

		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				" No records found Message  is not displayed");

		// 2nd contact.
		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part35, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ContactSearch("QASTATEFIRM", "RESTRI");

		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				" No records found Message  is not displayed");

		// 3rd contact.
		Clearcart();

		ExplicitWait_Element_Clickable(locatorType, Announcement_view_btn_Path, driver);

		Type(locatorType, Fullfilment_Search_Path, Part35, driver);
		Click(locatorType, Fullfilment_Search_Btn_Path, driver);
		ContactSearch("QA", "Auto");

		Assert.assertTrue(
				Get_Text(locatorType, No_Records_Message_path, driver).trim().equalsIgnoreCase(No_Records_Message),
				" No records found Message  is not displayed");
	}

	@BeforeClass(enabled = true)
	public void BeforeClass() throws IOException, InterruptedException {
		login();

	}

	@AfterClass(enabled = true)
	public void AfterClass() throws IOException, InterruptedException {
		//logout();

	}

}
