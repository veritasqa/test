package com.globalatlantic.values;

public class ValueRepository {

	public static String Browser = "chrome";
	public static final String Xpath = "xpath";
	// File path for Chrome Driver
	public static String Browserpath = System.getProperty("user.dir")
			+ "\\src\\com\\globalatlantic\\Driver\\chromedriver.exe";
	// File Path GroupA and GroupA1 ExcelList Files
	public static String Excel_File_Path = "C:\\Users\\rr249046\\Desktop\\A.xlsx";

	// public static final String URL =
	// "https://staging.veritas-solutions.net/GlobalAtlantic/login.aspx";

	public static final String URL = "https://www.veritas-solutions.net/GlobalAtlantic/default.aspx";
	public static final String Inventory_URL = "https://www.veritas-solutions.net/inventory/login.aspx";

	// public static final String Inventory_URL =
	// "https://staging.veritas-solutions.net/inventory/login.aspx";

	public static final String IFrame_URL = "https://www.veritas-solutions.net/GlobalAtlantic/API/DocumentLibrary/DocumentLibraryTest.aspx";

	public static final String Appbuilder_IFrame_URL = "https://www.veritas-solutions.net/GlobalAtlantic/API/AppBuilder/ApplicationBuilder.aspx";
	public static final String SSO_URL = "https://www.veritas-solutions.net/GlobalAtlantic/API/SSO/AuthenticateTest.aspx";
	public static final String TL_URL = "http://www.veritas-solutions.net/globalatlantic/traditionallife/Default.aspx";

	public static final String UserName = "qaauto";
	public static final String Password = "qaauto";

	public static final String MaxUserName = "qaautomaxorder";
	public static final String MaxPassword = "qaautomaxorder";

	public static final String EmailId = "ver.qaauto@rrd.com";
	public static final String UserId = "QA.Auto";

	public static final String Inventory_UserName = "arajasekaran";
	public static final String Inventory_Password = "arajasekaran780";

	public static final String MU_Username = "ssubbiah";
	public static final String MU_Address1 = "913 Commerce ct-123";

	public static final String Firstname = "qa";
	public static final String Lastname = "auto";
	public static final String Form_lastname = "auto";
	public static String ContactName = Firstname + " " + Lastname;

	public static final String No_Firm_IL_Name = "QA";
	public static final String No_Firm_IL_Last_Name = "Automation";

	public static final String Firm_Restriction_Name = "QAFIRMRESTRICTION";
	public static final String Firm_Restriction_last_Name = "Auto";

	public static final String STATEFIRMRESTRICTION_Name = "QASTATEFIRMRESTRICTION ";
	public static final String STATEFIRMRESTRICTION_last_Name = "Auto";

	public static final String Firstname_AL_State = "QA";
	public static final String Lastname_AL_State = "Auto_Alabama";

	public static final String Firstname_AE_Firm = "QA";
	public static final String Lastname_AE_Firm = "Auto_American Equity";

	public static final String Firstname_53_Firm = "QA";
	public static final String Lastname_53_Firm = "Auto_53";

	public static final String Firstname_IL_AE_StateFirm = "QA";
	public static final String Lastname_IL_AE_StateFirm = "Auto_IL_AE";

	public static final String Firstname_OR_AC_StateFirm = "QA";
	public static final String Lastname_OR_AC_StateFirm = "Auto_OR_AC";

	public static final String Firstname_OR_AG_StateFirm = "QA";
	public static final String Lastname_OR_AG_StateFirm = "Auto_OR_AG";

	public static final String Firstname_AZ_AG_StateFirm = "QA";
	public static final String Lastname_AZ_AG_StateFirm = "Auto_AZ_AG";

	public static final String Firstname_AZ_State = "QA";
	public static final String Lastname_AZ_State = "Auto_AZ";

	public static final String Firstname_AcceleratedCapital_Firm = "QA";
	public static final String Lastname_AcceleratedCapital_Firm = "Auto_AcceleratedCapital_Firm";

	public static final String Firstname_AZ_Al_StateFirm = "QA";
	public static final String Lastname_AZ_AL_StateFirm = "Auto_AZ_Allegis";

	public static final String Address1 = "913 Commerce ct";
	public static final String City = "Buffalo Grove";
	public static final String State = "IL";
	public static final String Zipcode = "60089";
	public static final String Shipper = "UPS Ground";

	public String Parent_Window_ID = "";
	public String Dropdown_Clear = "- Please Select -";

	public static String OutPut_File_Path = "C:\\Users\\rr249046\\Desktop\\Veritas\\Protective\\PartsViewability"; // Output
																													// File
																													// for
																													// Group
																													// A
	public String newview = "";
	public String partvalue = "";
	public String attrib = "";
	public static final String Attribute_Value = "Value";

	public static final String Part1 = "QA_Multifunction";
	public static final String Part2 = "QA_FirmRestriction";
	public static final String Part3 = "QA_STATERESTRICTION";
	public static final String Part4 = "QA_StateFirmRestriction";
	public static final String Part5 = "QA_keyword";
	public static final String Part6 = "QA_Copy_NewItem";
	public static final String Part7 = "QA_TESTNEWITEM_X";
	public static final String Part8 = "QA_InventorySearch";
	public static final String Part9 = "QA_TraditionalLife_1";
	public static final String Part10 = "QA_Adminonly";
	public static final String Part11 = "QA_TLKIT";
	public static final String Part12 = "QA_CLIENTMATERIALS";
	public static final String Part13 = "QA_AGENTMATERIALS";
	public static final String Part14 = "QA_CATEGORY";
	public static final String Part15 = "QA_OTStateRestrictionEX";
	public static final String Part16 = "QA_OTFirmRestrictionEX";
	public static final String Part17 = "QA_OTStateFirmRestrictionEX";
	public static final String Part18 = "QA_Obsolete";
	public static final String Part19 = "QA_Replace";
	public static final String Part20 = "QA_Replace1";
	public static final String Part21 = "QA_OTStateRestrictionIN";
	public static final String Part22 = "QA_OTFirmRestrictionIN";
	public static final String Part23 = "QA_OTStateFirmRestrictionIN";
	public static final String Part24 = "QA_DLVIEWABILITY";
	public static final String Part25 = "QA_DOCLIB";
	public static final String Part26 = "QA_FORM1";
	public static final String Part27 = "QA_FORM2";
	public static final String Part28 = "QA_FORM3";
	public static final String Part29 = "QA_APPBUILDERREQUIRED";
	public static final String Part30 = "QA_APPBUILDERBUYER";
	public static final String Part31 = "QA_APPBUILDERONESTATE";
	public static final String Part32 = "QA_APPBUILDERTEST";
	public static final String Part33 = "QA_kitonfly_2";
	public static final String Part34 = "QA_kitonfly_5";
	public static final String Part35 = "QA_kitonfly_6";
	public static final String Part36 = "QA_IMODB1";
	public static final String Part37 = "QA_IMODB2";
	public static final String Part38 = "QA_IMODB3";
	public static final String Part39 = "QA_IMODB4";
	public static final String Part40 = "QA_Viewability";
	public static final String Part41 = "QA_AllowBackOrder";
	public static final String Part42 = "QA_MaxOrder";
	public static final String Part43 = "QA_Usergroup";
	public static final String Part44 = "QA_KITONFLY_7";
	public static final String Part45 = "QA_KITONFLY_3";
	public static final String Part46 = "QA_KITONFLY_4";
	public static final String Part47 = "QA_KITONFLY_1";
	public static final String QA_KITREPLACE1 = "QA_KITREPLACE1";
	public static final String QA_REPLACE_TEST_1 = "QA_REPLACE_TEST_1";
	public static final String QA_REPLACE_TEST_2 = "QA_REPLACE_TEST_2";
	public static final String QA_COPY_SEARCHRESULTS_X = "QA_COPY_SEARCHRESULTS_X";

	public static final String Qty = "1";

	public static String OrderNumber = "5753496";
	public static String TicketNumber = "";

	public static String SplprjTicketNumber = "001823";

	public static final String CreateTemplate_Message = "QA Test order template has been successfully created.";

	public static final String Invalid_Form_Message = "Invalid Form Number";

	// Spl Projects
	public static final String SPPErrorTilte = "You must enter a value in the following fields:";
	public static final String SPPErrorJobTilte = "Job Title Is Required";
	public static final String SPPErrorJobType = "Job Type Is Required";
	public static final String SPPErrorCC = "Cost Center Is Required";

	public static final String No_Records_Message = "The item requested may not be available due to channel or state restriction or the form may no longer be active.";

}
