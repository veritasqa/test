package com.globalatlantic.OR;

public class TraditionalLifeOR extends WidgetOR {

	public static final String TL_Logo_Path = ".//*[@id='imgLogo']";
	public static final String TL_HomeBtn_Path = "//a[contains(.,'Home')]";
	public static final String TL_Contactus_Path = ".//a[text()='Contact Us']";
	public static final String TL_ClearCart_Path = "//a[contains(.,'Clear Cart')]";
	public static final String TL_ClickHerelink_Path = "//a[contains(.,'here ')]";
	public static final String TL_OrderMaterialTitle_Path = ".//*[@id='ContentPlaceHolder1_divSearchResultsBanner']/img";
	public static final String TL_Announcementstitle_Path = ".//span[text()='Announcements']";
	public static final String TL_Announcementsbody_Path = ".//*[@id='Announcements']/div[1]/b";
	public static final String TL_Announcementslink_Path = ".//a[contains(text(),'here')]";
	public static final String TL_StateDropdown_Path = ".//*[@id='ddlState']";
	public static final String TL_StateDropdownreq_Path = ".//span[text()='(Required)']";
	public static final String TL_StateDropdownreqmsg_Path = ".//span[text()='Required']";
	public static final String TL_Categorydd_Path = ".//*[@id='ContentPlaceHolder1_ddlProductLineCategory']";
	public static final String TL_SubCategorydd_Path = ".//*[@id='ContentPlaceHolder1_ddlProductLine']";
	public static final String TL_Addfilterdd_Path = ".//*[@id='ContentPlaceHolder1_ddlCategory']";
	public static final String TL_Searchmaterialfield_Path = ".//*[@id='ctl00_ContentPlaceHolder1_txtKeyword']";
	public static final String TL_SearchBtn_Path = ".//a[text()='Search']";
	public static final String TL_Clearbtn_Path = ".//a[text()='Clear']";
	public static final String TL_Shoppingcart_Path = ".//span[text()='Shopping Cart']";
	public static final String TL_Shoppingcartcheckout_Path = ".//a[text()='Checkout']";
	public static final String TL_Shoppingcartupdate_Path = ".//*[@id='btnUpdate']";
	public static final String TL_Myrecentorder_Path = ".//span[text()='My Recent Orders']";
	public static final String TL_MyrecentorderClearorder_Path = ".//*[@id='btnClearOrder']";
	public static final String TL_Pdfpage_Path = ".//*[@id='plugin']";
	
	// Search Results
	public static final String TL_Partimage1_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdProductList_ctl00_ctl04_Img1']";
	public static final String TL_PartName1_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdProductList_ctl00__0']/td[2]/div[1]/span";
	public static final String TL_PartDescription1_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdProductList_ctl00__0']/td[2]/div[1]/div/span[1]";
	public static final String TL_Partqtyfield1_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdProductList_ctl00_ctl04_txtQuantity']";
	public static final String TL_PartAddtocartbtn1_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdProductList_ctl00_ctl04_btnAddToCart']";
	public static final String TL_PartMaxorder1_Path = ".//*[@id='divMaxOrder']";
	public static final String TL_PartMaxordererror1_Path = ".//div[text()='Max Quantity Error!']";
	public static final String TL_PartOutofstock1_Path = ".//*[@id='divAvailability']";
	public static final String TL_PartSuccessfullyadded1_Path = ".//div[text()='Successfully Added1!']";
	public static final String TL_ShoppingCartPart1_Path = ".//*[@id='tdbody']/tr[1]/td[1]";
	public static final String TL_ShoppingCartPart2_Path = ".//*[@id='tdbody']/tr[2]/td[1]";
	public static final String TL_ShoppingCartPart3_Path = ".//*[@id='tdbody']/tr[3]/td[1]";
	public static final String TL_ShoppingCartPart1qty_Path = ".//*[@id='tdbody']/tr[1]/td[5]/input";
	public static final String TL_ShoppingCartPart2qty_Path = ".//*[@id='tdbody']/tr[2]/td[5]/input";
	public static final String TL_ShoppingCartPart3qty_Path = ".//*[@id='tdbody']/tr[3]/td[5]/input";
	public static final String TL_MyRecentorder_Path = ".//span[text()='My Recent Orders']";
	public static final String TL_MyRecentorder1_Path = ".//*[@id='tdorderbody']/tr[1]/td/a";
	public static final String TL_MyRecentorder2_Path = ".//*[@id='tdorderbody']/tr[2]/td/a";
	public static final String TL_MyRecentorder3_Path = ".//*[@id='tdorderbody']/tr[3]/td/a";
	
	// Checkout page
	
	public static final String TL_CO_Captcha_Path = "//*[@id='ctl00_ContentPlaceHolder1_captchaCtrl_CaptchaImage']";
	public static final String TL_CO_Captchafield_Path = ".//*[@id='ctl00_ContentPlaceHolder1_captchaCtrl_CaptchaTextBox']";
	public static final String TL_CO_CaptchaIns_Path = ".//*[@id='ctl00_ContentPlaceHolder1_captchaCtrl_CaptchaTextBoxLabel']";
	public static final String TL_CO_CaptchaValidatebtn_Path = ".//a[text()='Validate']";
	public static final String TL_CO_Captchaerror_Path = ".//span[text()='The code you entered is not valid']";
	public static final String TL_CO_Shoppingcart_Path = ".//h1[contains(text(),'Shopping Cart')]";
	public static final String TL_CO_PartImage1_Path = ".//*[@id='tdbody']/tr/td[1]/img";
	public static final String TL_CO_PartName1_Path = ".//*[@id='tdbody']/tr/td[2]";
	public static final String TL_CO_PartQty1_Path = ".//*[@id='tdbody']/tr/td[4]/input";
	public static final String TL_CO_PartUpdatebtn1_Path = ".//*[@id='btnUpdate']";
	public static final String TL_CO_PartRemovebtn1_Path = ".//*[@id='adelete']";
	public static final String TL_CO_Shippinginfo_Path = ".//span[text()='Shipping Information']";
	public static final String TL_CO_Downloadoption_Path = ".//h1[text()='Download Order Option']";
	public static final String TL_CO_DownloadOnlybtn_Path = ".//input[@value='Download Only']";
	public static final String TL_CO_ProduceId_Path = ".//*[@id='txtProducerId']";
	public static final String TL_CO_Name_Path = ".//*[@id='txtName']";
	public static final String TL_CO_Company_Path = ".//*[@id='txtCompany']";
	public static final String TL_CO_Address1_Path = ".//*[@id='txtAddress1']";
	public static final String TL_CO_Address2_Path = ".//*[@id='txtAddress2']";
	public static final String TL_CO_Address3_Path = ".//*[@id='txtAddress3']";
	public static final String TL_CO_City_Path = ".//*[@id='txtCity']";
	public static final String TL_CO_State_Path = ".//*[@id='txtState']";
	public static final String TL_CO_Zip_Path = ".//*[@id='txtZip']";
	public static final String TL_CO_Countrydd_Path = ".//*[@id='ddlCountry']";
	public static final String TL_CO_Phone_Path = ".//*[@id='txtPhone']";
	public static final String TL_CO_Email_Path = ".//*[@id='txtEmail']";
	public static final String TL_CO_Placeorderbtn_Path= ".//*[@id='btnOrderOnly']";
	public static final String TL_CO_Confirmaddressbtn_Path = ".//*[@id='btnShipType']";
	public static final String TL_CO_EditAddressbtn_Path = ".//*[@id='btnEdit']";
	public static final String TL_CO_UPSGroundradio_Path = ".//*[@id='rdbShippingGround']";
	public static final String TL_CO_UPSGroundlabel_Path = ".//*[@id='ContentPlaceHolder1_pnlShippingSelectMethod']/div[1]/div/div/span/label";
	public static final String TL_CO_Nameasterisk_Path = ".//*[@id='ContentPlaceHolder1_lblNameAsterisk']";
	public static final String TL_CO_Address1asterisk_Path = ".//*[@id='ContentPlaceHolder1_lblAddress1Asterisk']";
	public static final String TL_CO_Cityasterisk_Path = ".//*[@id='ContentPlaceHolder1_lblCityAsterisk']";
	public static final String TL_CO_Stateasterisk_Path = ".//*[@id='ContentPlaceHolder1_lblStAsterisk']";
	public static final String TL_CO_Zipasterisk_Path = ".//*[@id='ContentPlaceHolder1_lblZipAsterisk']";
	public static final String TL_CO_Countryasterisk_Path = ".//*[@id='ContentPlaceHolder1_lblCountryAsterisk']";
	public static final String TL_CO_Emailasterisk_Path = ".//*[@id='ContentPlaceHolder1_lblEmailAsterisk']";
	public static final String TL_CO_Nameerrmsg_Path = ".//*[@id='lblNameErrMsg']";
	public static final String TL_CO_Address1errmsg_Path = ".//*[@id='lblAddress1ErrMsg']";
	public static final String TL_CO_Cityerrmsg_Path = ".//*[@id='lblCityErrMsg']";
	public static final String TL_CO_Stateerrmsg_Path = ".//*[@id='lblStateErrMsg']";
	public static final String TL_CO_Ziperrmsg_Path = ".//*[@id='lblZipErrMsg']";
	public static final String TL_CO_Emailerrmsg_Path = ".//*[@id='lblEmailErrMsg']";
	public static final String TL_CO_Emptyshoppingcart_Path = ".//span[text()='Shopping Cart is Empty !']";
	/*public static final String TL_CO__Path = "";
	public static final String TL_CO__Path = "";
	
	public static final String TL_Pdfpage_Path = "//*[@id="plugin"]";
	public static final String TL__Path = "";
	public static final String TL__Path = "";
	public static final String TL__Path = "";
	public static final String TL__Path = "";*/
	
	// Order Confirmation Page
	
	public static final String TL_OCP_OrderConfirmation_Path = ".//h1[text()='Order Confirmation']";
	public static final String TL_OCP_OrderNo_Path = ".//*[@id='ContentPlaceHolder1_lblOrderNumber']";
	public static final String TL_OCP_StatusNew_Path = ".//span[text()='NEW']";
	public static final String TL_OCP_StatusClosed_Path = ".//span[text()='CLOSED']";
	public static final String TL_OCP_Orderdate_Path = ".//*[@id='ContentPlaceHolder1_lblCreateDate']";
	public static final String TL_OCP_Shippedto_Path = ".//*[@id='ContentPlaceHolder1_divAddress']/b";
	public static final String TL_OCP_Shippedby_Path = ".//*[@id='ContentPlaceHolder1_lblShippedBy']";
	public static final String TL_OCP_Shippedbydate_Path = ".//*[@id='ContentPlaceHolder1_lblShipbyDate']";
	public static final String TL_OCP_Orderdetails_Path = ".//span[text()='Order Details']";
	public static final String TL_OCP_Formno_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdOrderItems_ctl00__0']/td[1]";
	public static final String TL_OCP_Itemdesc_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdOrderItems_ctl00__0']/td[2]";
	public static final String TL_OCP_State_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdOrderItems_ctl00__0']/td[3]";
	public static final String TL_OCP_Quantity_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdOrderItems_ctl00__0']/td[4]";
	public static final String TL_OCP_Quantityavailable_Path = ".//*[@id='ctl00_ContentPlaceHolder1_rgdOrderItems_ctl00__0']/td[4]/span";
	public static final String TL_OCP_Downloadinst_Path = ".//*[@id='ContentPlaceHolder1_lblDownload']";
	public static final String TL_OCP_Downloadbtn_Path = ".//a[text()='Download']";
	public static final String TL_OCP_Continueshoppingbtn_Path = ".//a[text()='Continue Shopping']";
	public static final String TL_OCP_Contactus_Path = ".//a[text()='Contact Us']";
	
}
