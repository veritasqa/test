package com.globalatlantic.OR;

public class ManageordersOR extends ManageUserKitsOR {
	// Manage Orders
	public static final String Manage_Orders_Path = ".//span[text()='Manage Orders']";
	public static final String Manage_SelectOrder_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlOrders_Input']";
	public static final String Manage_SelectOrder_dropdown_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlOrders_DropDown']/div/ul/li/]";
	public static final String Manage_SelectOrder_dropdownOrder_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlOrders_DropDown']/div/ul/li/text()]";
	public static final String Manage_View_Btn_Path = ".//*[@id='cphContent_cphSection_btnView']";
	public static final String Manage_Copy_Btn_Path = ".//*[@id='cphContent_cphSection_btnCopy']";

	public static final String Manage_Order_Num_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtOrderNumber']";
	public static final String Manage_Tracking_Num_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtTrackingNumber']";
	public static final String Manage_Type_Value_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlOrderType_Input']";
	public static final String Manage_Type_Icon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlOrderType_Arrow']";
	public static final String Manage_User_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtCreateUser']";
	public static final String Manage_Shipper_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtShipper']";
	public static final String Manage_Status_Value_Path_ = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlStatus_Input']";
	public static final String Manage_Status_Icon_Path_ = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlStatus_Arrow']";
	public static final String Manage_StartDate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadStartDate_dateInput']";
	public static final String Manage_StartDate_Icon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadStartDate_popupButton']";
	public static final String Manage_EndDate_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadEndDate_dateInput']";
	public static final String Manage_EndDate_Icon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_RadEndDate_popupButton']";
	public static final String Manage_RecipientName_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtShipToName']";
	public static final String Manage_Address_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtAddress1']";
	public static final String Manage_city_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtCity']";
	public static final String Manage_State_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlState_Input']";
	public static final String Manage_ZipCode_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_txtZip']";
	public static final String Manage_Search_Btn_Path = ".//*[@id='cphContent_cphSection2_btnSearch']";
	public static final String Manage_Clear_Btn_Path = ".//*[@id='divContent clearfix']/div[5]/div/table/tbody/tr[15]/td[2]/a[2]";
	public static final String Manage_Select_Btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00_ctl04_btnSelect']";
	public static final String Manage_CopyResults_Btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00_ctl04_btnCopy']";
	public static final String Manage_Collaboration_Btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection3_rgOrders_ctl00_ctl04_lnkCollaboration']";

	// Manage Orders - Type Drop down
	public static final String Manage_Type_Value_FF_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlOrderType_DropDown']/div/ul/li[2]";
	public static final String Manage_Clear_FM_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlOrderType_DropDown']/div/ul/li[3]";
	// Manage Orders - Status Drop down
	public static final String Manage_Status_Value_New_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_ddlStatus_DropDown']/div/ul/li[2]";

	// Manage Orders - Edit Manage Order Tabs
	public static final String Manage_GeneralTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[1]/a/span/span/span";
	public static final String Manage_ItemsTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[2]/a/span/span/span";
	public static final String Manage_ShippingTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[3]/a/span/span/span";
	public static final String Manage_MailListRecipeintsTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[4]/a/span/span/span";
	public static final String Manage_AttachmentsTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span";

	// Manage Orders - General Tab
	public static final String Manage_GT_TicketNumberfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[1]/span";
	public static final String Manage_GT_Usernamefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[2]/span";
	public static final String Manage_GT_FirmNamerfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[3]/span";
	public static final String Manage_GT_OrderDatefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[4]";
	public static final String Manage_GT_ShipDatefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[5]/span";
	public static final String Manage_GT_CloseDatefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[6]/span";
	public static final String Manage_GT_CancelDatefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]";
	public static final String Manage_GT_OrderTypefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[8]/span";
	public static final String Manage_GT_OnBackOrderfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[9]/a";
	public static final String Manage_GT_OnHoldfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[10]/a";
	public static final String Manage_GT_Status_Path = ".//*[@id='cphContent_lblOrderStatus']";
	public static final String Manage_GT_Select_Btn_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[11]/a";
	public static final String Manage_GT_Edit_Btn_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[12]/a";
	public static final String Manage_GT_OrderSearch_btn_Path = ".//*[@id='cphContent_btnCancel']";

	public static final String Manage_GT_TicketNumberfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[1]";
	public static final String Manage_GT_Usernamefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[2]/a";
	public static final String Manage_GT_FirmNamerfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[3]";
	public static final String Manage_GT_OrderDatefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[4]";
	public static final String Manage_GT_ShipDatefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[5]";
	public static final String Manage_GT_CloseDatefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[6]";
	public static final String Manage_GT_CancelDatefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[7]";
	public static final String Manage_GT_OrderTypefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[8]";
	public static final String Manage_GT_OnBackOrderfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[9]";
	public static final String Manage_GT_OnHoldfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[10]";

	// Manage Orders - Items Tab
	public static final String Manage_IT_FormNumberfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[1]";
	public static final String Manage_IT_ItemDescfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[2]";
	public static final String Manage_IT_Statefield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[3]/a";
	public static final String Manage_IT_Qtyfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[4]/a";
	public static final String Manage_IT_Shippedfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[5]/a";
	public static final String Manage_IT_BackOrderfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[6]/a";
	public static final String Manage_IT_Printfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]/span";
	public static final String Manage_IT_Fulfillmentfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[8]/span";
	public static final String Manage_IT_Chargebackfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[9]/span";
	public static final String Manage_IT_Shippingfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[10]";
	public static final String Manage_IT_Totalfield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[11]/span";
	public static final String Manage_IT_Prooffield_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[12]";
	public static final String Manage_IT_Status_Path = ".//*[@id='cphContent_lblOrderStatus']";
	public static final String Manage_IT_BillingCostCenterfield_Path = ".//*[@id='cphContent_txtCostCenter']";
	public static final String Manage_IT_Update_btn_Path = ".//*[@id='cphContent_btnUpdateCostCenter']";
	public static final String Manage_IT_Next_Btn_Path = ".//*[@id='cphContent_btnBillingNext']";
	public static final String Manage_IT_Back_Btn_Path = ".//*[@id='cphContent_btnBillingBack']";

	public static final String Manage_IT_FormNumberfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[1]";
	public static final String Manage_IT_ItemDescfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[2]";
	public static final String Manage_IT_Statefield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[3]";
	public static final String Manage_IT_Qtyfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[4]";
	public static final String Manage_IT_Shippedfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[5]";
	public static final String Manage_IT_BackOrderfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[6]";
	public static final String Manage_IT_Printfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[7]";
	public static final String Manage_IT_Fulfillmentfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[8]";
	public static final String Manage_IT_Chargebackfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[9]";
	public static final String Manage_IT_Shippingfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[10]";
	public static final String Manage_IT_Totalfield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[11]";
	public static final String Manage_IT_Prooffield1_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00__0']/td[12]";

	// Manage Orders - Shipping Tab
	public static final String Manage_SP_Address_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[1]";
	public static final String Manage_SP_ShippingInst_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[2]";
	public static final String Manage_SP_ReqDelivery_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[3]";
	public static final String Manage_SP_ShipMethod_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[4]";
	public static final String Manage_SP_ShippingInfo_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[5]";
	public static final String Manage_SP_TrackingNumber_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[6]";
	public static final String Manage_SP_UpgradedShipping_Path = ".//*[@id='ctl00_cphContent_RadGrid1_ctl00']/thead/tr/th[7]";
	public static final String Manage_SP_Next_Btn_Path = ".//*[@id='cphContent_btnShippingNext']";
	public static final String Manage_SP_Back_Btn_Path = ".//*[@id='cphContent_btnShippingBack']";
	public static final String Manage_SP_Status_Path = ".//*[@id='cphContent_lblOrderStatus']";

	// Manage Orders - Mail List Recipients Tab
	public static final String Manage_MLP_Trackingnumberfield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[1]/span";
	public static final String Manage_MLP_FirstNamefield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[2]/a";
	public static final String Manage_MLP_lastNamefield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[3]/a";
	public static final String Manage_MLP_Companyfield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[4]/a";
	public static final String Manage_MLP_Address1field_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[5]/a";
	public static final String Manage_MLP_Cityfield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[6]/a";
	public static final String Manage_MLP_Statefield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[7]/a";
	public static final String Manage_MLP_Zipfield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[8]/a";
	public static final String Manage_MLP_Phonefield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[9]/a";
	public static final String Manage_MLP_TrackinNumberfield_Path = ".//*[@id='ctl00_cphContent_rgItems_ctl00']/thead/tr/th[10]";
	public static final String Manage_MLP_MailListFileForProduction_Btn_Path = ".//*[@id='cphContent_btnMailListCleaned']";
	public static final String Manage_MLP_Next_btn_Path = ".//*[@id='cphContent_btnItemsNext']";
	public static final String Manage_MLP_Back_btn_Path = ".//*[@id='cphContent_btnItemsBack']";
	public static final String Manage_MLP_Status_Path = ".//*[@id='cphContent_lblOrderStatus']";

	// Manage Orders - Attachments Tab
	public static final String Manage_AT_AttachmentTypeField_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[1]/a";
	public static final String Manage_AT_CreateuserField_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[2]/a";
	public static final String Manage_AT_CreateDateField_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[3]/a";
	public static final String Manage_AT_LinkToFileField_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/thead/tr/th[4]";
	public static final String Manage_AT_AddAttachment_Icon_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String Manage_AT_AddAttachment_Link_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String Manage_AT_Refresh_Icon_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_RefreshButton']";
	public static final String Manage_AT_Refresh_Link_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00_ctl03_ctl01_RebindGridButton']";
	public static final String Manage_AT_GridMessage_Path = ".//*[@id='ctl00_cphContent_rgdAttachments_ctl00']/tbody/tr/td/div";
	public static final String Manage_AT_Save_btn_Path = ".//*[@id='cphContent_btnSave']";
	public static final String Manage_AT_Back_btn_Path = ".//*[@id='cphContent_btnAttachmentsBack']";
	public static final String Manage_AT_Status_Path = ".//*[@id='cphContent_lblOrderStatus']";

	public static final String CreateTemplate_Message2 = "QA Template Test order template has been successfully created.";

	public static final String MO_Loadingpanel = ".//*[@id='cphContent_cphSection3_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection3_rgOrders']";

}
