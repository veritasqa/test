package com.globalatlantic.OR;

public class SpecialProjectOR extends SSO_OR {
	
	public static final String Special_Projects_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[1]/a/span";

	// Add Special Projects
	public static final String Add_Spl_Projects_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[1]/ul/li/a/span";
	public static final String SplProjects_Generaltab_Path = ".//*[@id='cphContent_lnkGeneral']/span";
	public static final String Add_Spl_ProjectsTitle_Path = ".//*[@id='cphContent_divHeaderImage']/h1";
	public static final String SPEnclosure_FormNumberdrop_Path =".//*[@id='ctl00_cphContent_Parts_ctl00_ctl02_ctl02_ddlFormNumber_DropDown']/div/ul/li";

	public static final String SplProjects_BusinessOwner_Path = ".//*[@id='cphContent_ddlBusinessOwners']";
	public static final String SplProjects_Jobtitle_Path = ".//*[@id='cphContent_txtJobTitle']";
	public static final String SplProjects_JobType_Path = ".//*[@id='cphContent_ddlJobType']";
	public static final String SplProjects_description_Path = ".//*[@id='cphContent_txtDescription']";
	public static final String SplProjects_CreatedBy_Path = ".//*[@id='cphContent_lblCreateUser']";
	public static final String SplProjects_CostCenter_Path = ".//*[@id='cphContent_ddlCostCenter']";
	public static final String SplProjects_SplInstruction_Path = ".//*[@id='cphContent_txtSpecialInstructions']";
	public static final String SplProjects_Inrush_Path = ".//*[@id='cphContent_chkRush']";
	public static final String SplProjects_Quoteneeded_Path = ".//*[@id='cphContent_chkQuoteNeeded']";
	public static final String SplProjects_Savemessage_Path =".//*[@id='cphContent_lblMessage']";
	public static final String SplProjects_TicketNumber_Path =".//*[@id='tabs']/div[1]/h1/span/span[1]";
	public static final String SplProjects_Ticket_Status_Path = ".//*[@id='tabs']/div[1]/h1/span/span[2]";
	public static final String SplProjects_Cancelbtn_Path =".//*[@id='cphContent_btnCancel']";
	public static final String SplProjects_Createbtn_Path =".//*[@id='cphContent_btnSave']";
	public static final String SplProjects_Errtitle_Path =".//*[@id='cphContent_vsRequiredFields']";
	public static final String SplProjects_Errjobtitle_Path =".//li[text()='"+SPPErrorJobTilte+"']";
	public static final String SplProjects_Errjobtype_Path =".//li[text()='"+SPPErrorJobType+"']";
	public static final String SplProjects_Errcostcenter_Path =".//li[text()='"+SPPErrorCC+"']";
	public static final String SplProjects_Jobtypefielderr_Path =".//*[@id='cphContent_cfvJobType']";
	public static final String SplProjects_CCfielderr_Path =".//*[@id='cphContent_cfvCostCenter']";
	//Progress Tab

	public static final String SplProjects_Progresstab_Path = ".//*[@id='cphContent_lnkProgress']/span";
	
	
	public static final String SP_ProgTab_DueDate_Path = ".//*[@id='ctl00_cphContent_dtpDueDate_dateInput']";
	public static final String SP_ProgTab_AddCommenticon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SP_ProgTab_AddComment_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl03_ctl01_InitInsertButton']";
	public static final String SP_ProgTab_Commenttxt_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_txtComment']";
	public static final String SP_ProgTab_TypeInput_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Input']";
	public static final String SP_ProgTab_Typeicon_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_Arrow']";
	public static final String SP_ProgTab_TypeAck_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_DropDown']/div/ul/li[2]";
	public static final String SP_ProgTab_Choosefile_Path = "//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_fCommentsAttachment']";
	public static final String SP_ProgTab_Submit_Path = ".//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_btnSaveComment']";
	public static final String SP_ProgTab_Duedateerror_Path=".//*[@id='ctl00_cphContent_dtpDueDate_dateInput']";
	public static final String SP_ProgTab_type_cancel_path = "//*[@id='ctl00_cphContent_Comments_ctl00_ctl02_ctl02_ddlType_DropDown']/div/ul/li[3]";
	
	//Cost tab
	public static final String SplProjects_Costtab_Path = ".//*[@id='cphContent_lnkCosts']/span";

	public static final String SPcost_txtQuoteCost_Path =".//*[@id='ctl00_cphContent_txtQuoteCost']";
	public static final String SPcost_txtPrintCost_Path =".//*[@id='ctl00_cphContent_txtPrintCost']";
	public static final String SPcost_txtVeritasPostageCost_Path =".//*[@id='ctl00_cphContent_txtVeritasPostageCost']";
	public static final String SPcost_ckBilled_Path =".//*[@id='cphContent_ckBilled']";
	public static final String SPcost_txtFulfillmentCost_Path =".//*[@id='ctl00_cphContent_txtFulfillmentCost']";
	public static final String SPcost_ClientPostageCost_Path =".//*[@id='ctl00_cphContent_txtClientPostageCost']";
	public static final String SPcost_txtRushCost_Path =".//*[@id='ctl00_cphContent_txtRushCost']";
	public static final String SPcost_RefreshButton_Path =".//*[@id='ctl00_cphContent_rgdCosts_ctl00_ctl03_ctl01_RefreshButton']";

	//Enclosures Tab
	public static final String SplProjects_Enclosuretab_Path = ".//*[@id='cphContent_lnkEnclosures']/span";
	
	public static final String SPEnclosure_AddItem_Path =".//*[@id='ctl00_cphContent_Parts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String SPEnclosure_Formnum_Path =".//*[@id='ctl00_cphContent_Parts_ctl00_ctl02_ctl02_ddlFormNumber_Input']";
	public static final String SPEnclosure_Quantity_Path =".//*[@id='ctl00_cphContent_Parts_ctl00_ctl02_ctl02_txtQuantity']";
	public static final String SPEnclosure_Insert_Btn_Path =".//*[@id='ctl00_cphContent_Parts_ctl00_ctl02_ctl02_btnUpdate']";
	public static final String SPEnclosure_Cancel_Btn_Path ="//*[@id='cphContent_btnCancel']";
	public static final String SPEnclosure_Refresh_Btn_Path =".//*[@id='ctl00_cphContent_Parts_ctl00_ctl03_ctl01_RefreshButton']";


	public static final String SplProjects_Historytab_Path = ".//*[@id='cphContent_lnkHistory']/span";
	public static final String SplProjects_History_user_result_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[2]";
	public static final String SplProjects_History_Details_result_Path = ".//*[@id='ctl00_cphContent_History_ctl00__0']/td[3]";


	// Special Project Page

		public static final String SpeicalProject_MainTitle_Path = ".//*[@id='cphContent_cphMain_divHeaderImage']/h1";
		public static final String SP_1stTicket_NUmber_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]";

		public static final String SpeicalProject_Title_Path = ".//*[@id='divContent clearfix']/div[2]/h1";
		public static final String SP_Tickettxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_TicketNumber']";
		public static final String SP_TicketFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_TicketNumber']";
		public static final String SP_JobTitletxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_JobTitle']";
		public static final String SP_JobTitleFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_JobTitle']";
		public static final String SP_JobType_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_SpecialProjectType']";
		public static final String SP_JobTypeFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_SpecialProjectType']";
		public static final String SP_Statustxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_Status']";
		public static final String SP_StatusFiler_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_Status']";
		public static final String SP_DueDatetxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_DueDate']";
		public static final String SP_DueDateFiler_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_DueDate']";
		public static final String SP_RushCheckbox_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_Rush']";
		public static final String SP_RushFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_Rush']";
		public static final String SP_IsBilledCheckbox_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterCheckBox_IsBilled']";
		public static final String SP_IsBilledFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_IsBilled']";
		public static final String SP_RecipientCounttxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_RecipientCount']";
		public static final String SP_RecipientCountFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_RecipientCount']";
		public static final String SP_ProofCounttxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_FilterTextBox_ProofRecipientCount']";
		public static final String SP_ProofCountFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl02_ctl02_Filter_ProofRecipientCount']";
		public static final String SP_AddticketIcon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_AddNewRecordButton']";
		public static final String SP_Addticketlink_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_InitInsertButton']";
		public static final String SP_Firstpagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
		public static final String SP_Lastpagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
		public static final String SP_Nextpagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
		public static final String SP_Previouspagenav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
		public static final String SP_Selectpage1nav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[2]/a[1]/span";
		public static final String SP_Selectpage2nav_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[2]/a[2]/span";
		public static final String SP_Selectpagenavtext_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageTextBox']";
		public static final String SP_Selectpagetxt_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeTextBox']";
		public static final String SP_GotoPage_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_GoToPageLinkButton']";
		public static final String SP_Pagechange_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl02_ChangePageSizeLinkButton']";
		public static final String SP_Refreshicon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RefreshButton']";
		public static final String SP_Refreshlink_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RebindGridButton']";
		public static final String SP_Excel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl03_ctl01_RefreshButton']";
		public static final String SP_ViewEdit_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_EditButton']";
		
		public static final String SP_TicketNoResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[1]";
		public static final String SP_JobTitleResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[2]";
		public static final String SP_JobTypeResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[3]";
		public static final String SP_StatusResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[4]";
		public static final String SP_DuedateResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[5]";
		public static final String SP_RushResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_ctl00']";
		public static final String SP_IsbilledResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00_ctl04_ctl01']";
		public static final String SP_RecipentResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[8]";
		public static final String SP_ProofcountResult_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_ctl00__0']/td[9]";
		
		
		// Filter

		public static final String Filter_NoFilter_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[1]/a/span";
		public static final String Filter_Contains_Path = ".//span[text()='Contains']";
		public static final String Filter_DoesntContains_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[3]/a/span";
		public static final String Filter_StartsWith_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[4]/a/span";
		public static final String Filter_EndsWith_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[5]/a/span";
		public static final String Filter_EqualTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[6]/a/span";
		public static final String Filter_NotEqualTo_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[7]/a/span";
		public static final String Filter_GreaterThan_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[8]/a/span";
		public static final String Filter_LessThan_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[9]/a/span";
		public static final String Filter_GreatthanorEqualto_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[10]/a/span";
		public static final String Filter_LessthanOrEqual_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[11]/a/span";
		public static final String Filter_Between_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[12]/a/span";
		public static final String Filter_NotBetween_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[13]/a/span";
		public static final String Filter_IsEmpty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[14]/a/span";
		public static final String Filter_NotIsEmpty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[15]/a/span";
		public static final String Filter_IsNull_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[16]/a/span";
		public static final String Filter_NotisNull_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdSpecialProjects_rfltMenu_detached']/ul/li[17]/a/span";

}
