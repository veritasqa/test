package com.globalatlantic.OR;

public class ManageUserKitsOR extends MaterialsOR{

	
	
	public static final String Manage_UsersKits_Path = "//span[contains(.,'Manage User Kits')]";
	
	public static final String Userkitsheading =  ".//*[@id='cphContent_cphMain_divHeaderImage']/h1";
	public static final String UserkitsSection = ".//*[@id='divContent clearfix']/div[2]";
	
	public static final String UR_AddNewRecord = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl03_ctl01_AddNewRecordButton']";
	//Add userKit page 
	
	public static final String UR_Formnumber = ".//*[@id='cphContent_txtFormNumber']";
	
	public static final String UR_Desc =  ".//*[@id='cphContent_txtDescription']";
	
	public static final String UR_Kit_Dropdown = ".//*[@id='ctl00_cphContent_cbKitContainer_Input']";

	public static final String UR_Kit_Arrow = ".//*[@id='ctl00_cphContent_cbKitContainer_Arrow']";

	
	public static final String UR_Save_Btn  = ".//*[@id='cphContent_btnSave']";
	
	//Add from Inventory Section 
	
	public static final String UR_Addinvent_Section  = ".//*[@id='cphContent_pnlAddChild']/div/div";
	
	public static final String UR_AddFormnum = ".//*[@id='ctl00_cphContent_acbAddFormNumber']/div";
	public static final String UR_AddTxt_Qty= ".//*[@id='ctl00_cphContent_txtQuantity']";
	public static final String UR_Add_Locationdropdown =".//*[@id='ctl00_cphContent_ddlLocation_Input']";
	public static final String UR_Add_Sortorder =".//*[@id='ctl00_cphContent_txtSortOrder']";
	public static final String UR_Addtokit_btn =	".//*[@id='cphContent_btnAddToKit']";
		
	//table 
		
	public static final String UR_grid_Formnum =".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[1]";
	public static final String UR_gridDescription =".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[2]";
	public static final String UR_gridqty =".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[3]";
	public static final String UR_gridtype =".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[4]";
	public static final String UR_gridlocation =".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[5]";
	public static final String UR_gridoptional =	".//*[@id='ctl00_cphContent_grdKitBuild_ctl00_ctl04_ctl00']";
	public static final String UR_sort  =	".//*[@id='ctl00_cphContent_grdKitBuild_ctl00__0']/td[7]";
		
	public static final String UR_Saveuserkit_btn =	".//*[@id='cphContent_btnSave']";
	public static final String UR_Addtocart_btn = 	".//*[@id='cphContent_btnAddToCart']";
		
	//userKit Searchpage
	public static final String UR_formnum_txtbx = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_FormNumber']";
	public static final String UR_Formnum_filter = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_FormNumber']";
	
	public static final String UR_DescTxtbx = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_Description']";
	public static final String UR_Desc_Filter = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_Description']";

	public static final String UR_LstUpdt_Txtbx = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_FilterTextBox_LastUpdated']";
	public static final String UR_LstUpdt_Filter = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUserKits_ctl00_ctl02_ctl02_Filter_LastUpdated']";
}
