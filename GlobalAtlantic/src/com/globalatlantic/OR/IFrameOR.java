package com.globalatlantic.OR;

public class IFrameOR extends InventoryOR {
	
	public static final String Iframe_Doctypes_Path = ".//*[@id='DOCLIB_PRODUCT_LINE_TYPES']"; 
	public static final String Iframe_Docfirms_Path = ".//*[@id='DOCLIB_FIRMS']"; 
	public static final String Iframe_Docstates_Path = ".//*[@id='DOCLIB_STATES']"; 
	public static final String Iframe_Doclines_Path = ".//*[@id='DOCLIB_PRODUCT_LINES']"; 
	public static final String Iframe_Docmarketing_Path = ".//*[@id='DOCLIB_NOMARKETING']"; 
	public static final String Iframe_Submitbtn_Path = ".//*[@id='btnSubmit']"; 
	
	// Doc lib page
	
	public static final String Iframe_DocStateinput_Path = ".//*[@id='ddlState_Input']";
	public static final String Iframe_DocStatearrow_Path = ".//*[@id='ddlState_Arrow']";
	public static final String Iframe_DocProductlineinput_Path = ".//*[@id='ddlProductLine_Input']";
	public static final String Iframe_DocProductlinearrow_Path = ".//*[@id='ddlProductLine_Arrow']";
	public static final String Iframe_DocSearchbtn_Path = ".//*[@id='SearchDocLibrary']";
	public static final String Iframe_Doc_Path = "";

}
