package com.globalatlantic.OR;

import java.util.ArrayList;
import java.util.List;

import org.testng.asserts.SoftAssert;

public class ObjectRepository extends SpecialProjectOR {

	public static List<String> ordernum = new ArrayList<String>();
	public static List<String> splprojects = new ArrayList<String>();

	// locator type

	public static String locatorType = "xpath";

	public String Window_Handle = "";
	// public String Att="";
	public SoftAssert softAssert = new SoftAssert();

	// Global Atlantic
	public static final String Username_Path = ".//*[@id='txtUserName']";
	public static final String Password_Path = ".//*[@id='txtPassword']";
	public static final String Login_btn_Path = ".//*[@id='btnSubmit']";
	public static final String Username_error_Path = ".//li[text()='User Name is required']";
	public static final String Password_error_Path = ".//li[text()='User Password is required.']";
	public static final String BadPassword_error_Path = ".//li[text()='Bad user name or password']";

	public static final String Logo_Path = ".//*[@id='imgLogo']";
	public static final String Contact_Us_Path = ".//*[@id='ContactUsLink']";
	public static final String Logout_Path = ".//*[@id='lbnLogout']";
	public static final String Username_verify_Path = ".//*[@id='lblUserName']";
	public static final String Fullfilment_Search_Path = ".//*[@id='txtQuickSearch']";
	public static final String Fullfilment_Search_Btn_Path = ".//*[@id='btnQuickSearch']";

	public static final String Home_btn_Path = ".//*[@id='Navigation_divLiList']/li[1]/a/span";
	public static final String Materials_btn_Path = ".//*[@id='Navigation_divLiList']/li[2]/a/span";
	public static final String Reports_btn_Path = ".//*[@id='Navigation_divLiList']/li[3]/a/span";
	public static final String Admin_btn_Path = "//span[contains(.,'Admin')]";
	public static final String Document_lib_btn_Path = ".//*[@id='Navigation_divLiList']/li[5]/a/span";
	public static final String Application_Builder_Path = ".//*[@id='Navigation_divLiList']/li[6]/a/span";
	public static final String Clear_Cart_Path = ".//*[@id='Navigation_btnCancelOrder']/span";
	public static final String Checkout_path = ".//*[@id='aShoppingCart']/span";

	// Support tickets
	public static final String Support_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[2]/a/span";
	public static final String Add_Support_Ticket_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[2]/ul/li/a/span";
	public static final String FirstRow_TicketNumber_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00__0']/td[1]";
	public static final String FirstRow_OrderNumber_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00__0']/td[2]";
	public static final String FirstRow_Category_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00__0']/td[3]";
	public static final String FirstRow_SubCategory_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00__0']/td[3]";
	public static final String FirstRow_Status_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00__0']/td[9]";
	public static final String SupportTicket_Edit_Btn_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00__0']/td[10]/a";

	public static final String Veritas_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/a/span";
	public static final String Manage_Dropdown_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[1]/a/span";
	public static final String Manage_Promail_Alias_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[2]/a/span";
	public static final String Reset_phrases_Settings_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[3]/a/span";
	public static final String kit_Quality_control_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[4]/a/span";
	public static final String Inventory_Options_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[5]/a/span";
	public static final String Options_Type_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[6]/a/span";
	public static final String Contact_Search_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[3]/ul/li[7]/a/span";

	public static final String Manage_Announcement_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[4]/a/span";
	public static final String Manage_Announcement_View_btn_Path = ".//*[@id='ctl00_cphContent_rdAnnouncements_C']/a";
	public static final String Manage_Catagories_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[5]/a/span";
	public static final String Manage_Cost_Center_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[6]/a/span";

	public static final String Manage_Form_Permissions_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[8]/a/span";
	public static final String Manage_Preflight_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[9]/a/span";

	public static final String Manage_Permissions_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[11]/a/span";
	public static final String Manage_Phrases_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[12]/a/span";
	public static final String Manage_Roles_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[13]/a/span";

	// manage Users
	public static final String Manage_Users_Path = "//span[contains(.,'Manage Users')]";
	public static final String MU_AdduserPath = "//input[@title=' Add User']";

	public static final String MU_Username_Path = "//input[@alt='Filter UserName column']";
	public static final String MU_Username_FilterIcon_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_UserName']";

	public static final String MU_Result1_Username_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00__0']/td[2]";
	public static final String MU_Result1_Edit_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl04_EditButton']";

	public static final String MU_Result_Username_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditUserName']";
	public static final String MU_Result_Firstname_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditFirstName']";
	public static final String MU_Result_Lastname_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditLastName']";
	public static final String MU_Result_Email_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_rtxtEditEmail']";

	public static final String MU_Result_Address_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl05_rtxtAddress1']";
	public static final String MU_Result_Update_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl05_btnUpdate']";
	public static final String MU_Result_Cancel_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl03_btnCancel']";

	public static final String Manage_Kit_Quality_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[15]/a/span";

	public static final String Announcement_Heading_Path = ".//*[@id='ctl00_cphContent_rdAnnouncements_T']/em";
	public static final String Announcement_view_btn_Path = ".//*[@id='ctl00_cphContent_rdAnnouncements_C']/a";

	// Contact Search
	public static final String ContactSearch_Title_Path = ".//*[@id='cphContent_divHeaderImage']/h1";

	public static final String FirstName_Path = ".//*[@id='ctl00_cphContent_txtFirstName']";
	public static final String LastName_Path = ".//*[@id='ctl00_cphContent_txtLastName']";
	public static final String FirmName_Path = ".//*[@id='ctl00_cphContent_txtFirmSearch']";
	public static final String Search_btn_Path = ".//*[@id='cphContent_btnSearch']";
	public static final String Clear_btn_Path = ".//*[@id='cphContent_pnlSearchForm']/div/div/div[2]/div[4]/div[2]/a[2]";
	public static final String Contact_Click_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[4]";

	public static final String Edit_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl04_EditButton']";
	public static final String Delete_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00__0']/td[18]/a";
	public static final String Add_Contact_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String Refresh_btn_Path = ".//*[@id='ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_RebindGridButton']";

	// Add New Contact

	public static final String Add_Contact_Path = "//a[@id='ctl00_cphContent_Contacts_ctl00_ctl03_ctl01_InitInsertButton']";

	public static final String Add_FirstName_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtFirstNameNew']";
	public static final String Add_LastName_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtLastNameNew']";
	public static final String Add_Address1_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtAddress1']";
	public static final String Add_Address2_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtAddress2']";
	public static final String Add_State_Path = "//a[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_ddlState_Arrow']";

	public static final String Add_ZipCode_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtZipCode']";
	public static final String Add_Phone_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtPhone']";

	public static final String Add_FirmArroe_Path = "//a[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_ddlFirm_Arrow']";
	public static final String Add_Email_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtEmail']";
	public static final String Add_publicContact_Checkbox_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_cbPublicContact']";
	public static final String Add_Add_BTn_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_btnUpdate']";
	public static final String Add_City_Path = "//input[@id='ctl00_cphContent_Contacts_ctl00_ctl02_ctl02_txtCity']";
	public static final String Add_NoMatch_Path = "//span[@id='cphContent_lblNoAgentsFound']";

	// Search results

	public static final String Search_For_Materials_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideHeader_txtKeyword']";
	public static final String Search_For_Materials_Btn_Path = ".//*[@id='cphContent_cphRightSideCommandBar_btnAdvancedSearch']";
	public static final String Cart_Quantitiy_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_txtQuantity']";
	public static final String Add_To_Cart_btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[4]/div/div[2]/a[1]";
	public static final String Part_Name_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00__0']/td[2]/div[1]/span[1]";
	public static final String Add_Cart_Message_Path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_divErrorMessageCart']";
	public static final String Checkout_link_Path = ".//*[@id='aShoppingCart']/span";
	public static final String Loading_Path = ".//*[@id='cphContent_cphMain_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphRightSideContent_rgdProductList']";
	public static final String OutofstockMessage = "//*[@id='divAvailability']";

	public static final String Order_Confirmation_Checkbox_Path = ".//*[@id='cphContent_cbxOrderConfirmation']";
	public static final String Shipped_Confirmation_Checkbox_Path = ".//*[@id='cphContent_cbxShippedConfirmation']";
	public static final String Next_btn_Path = ".//*[@id='cphContent_btnShowShipping']";
	public static final String Checkout_btn_Path = ".//*[@id='cphContent_btnCheckout']";
	public static final String Orderingfor = "//*[@id=\"cphContent_ddlWholeSaler\"]";

	public static final String StandardShipping_btn_Path = ".//*[@id='cphContent_rdbShippingGround']";
	public static final String CostCenter_btn_Path = "//*[@id='cphContent_rdbCostCenter']";
	public static final String No_Records_Message_path = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_NoRecordsDiv']/span";
	public static final String Qa_Kitfly_Details_btn = ".//*[@id='ctl00_ctl00_cphContent_cphRightSideContent_rgdProductList_ctl00_ctl04_lnkDetails']";
	public static final String Qa_Kitfly_Details_Option1 = "//*[@id='kitBuild_divKitBuild']/ul/li[1]";
	public static final String Qa_Kitfly_Details_Option2 = "//*[@id='kitBuild_divKitBuild']/ul/li[2]";
	public static final String Qa_Kitfly_Details_Option3 = "//*[@id='kitBuild_divKitBuild']/ul/li[3]";
	public static final String Qa_Kitfly_Details_Closebtn = "//a[@class='rwCloseButton']";
	public static final String Qa_Kitfly_CmptDtls_Btn = "//a[text()='Show Component Details']";
	public static final String Qa_Kitfly_checkout_option1 = "//span[@id='rgrdShoppingCart_ctl00_ctl04_lblFormNumber']";
	public static final String Qa_Kitfly_checkout_option2 = "//span[@id='rgrdShoppingCart_ctl00_ctl07_lblFormNumber']";

	// Order Confirmation Page

	public static final String OrderConfirmation_Title_path = ".//*[@id='cphContent_cphMain_divHeaderImage']/h1";
	public static final String OrderConfirmation_TitleImage_path = ".//*[@id='cphContent_cphMain_divHeaderImage']";
	public static final String Submit_Cancel_Req_Btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00__0']/td[7]/a";
	public static final String Ok_Btn_Path = ".//*[@id='cphContent_cphPageFooter_btnOk']";
	public static final String Copy_Btn_Path = ".//*[@id='cphContent_cphPageFooter_btnCopy']";
	public static final String Show_Component_Details = "//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_divComponentDetails']/a";
	public static final String Component_Detail_Close = "//*[@id='RadWindowWrapper_ctl00_cphContent_oRadWindowComponentDetails']/table/tbody/tr[1]/td[2]/table/tbody/tr/td[3]/ul/li[2]/a";

	public static final String Component_Details1 = "//*[@id='rgrdShoppingCart_ctl00_ctl04_lblFormNumber']";
	public static final String Component_Details2 = "//*[@id='rgrdShoppingCart_ctl00_ctl07_lblFormNumber']";
	public static final String Component_Details3 = "//*[@id='rgrdShoppingCart_ctl00_ctl10_lblFormNumber']";

	public static final String OrderNumber_Path = ".//*[@id='cphContent_cphSection_lblOrderNumber']";
	public static final String OrderStatus_Path = ".//*[@id='cphContent_cphSection_lblOrderNumber']";
	public static final String OrderDate_Path = ".//*[@id='cphContent_cphSection_lblOrderNumber']";
	public static final String Shippedto_Path = ".//*[@id='cphContent_cphSection_lblOrderNumber']";
	public static final String ShippedBy_Path = ".//*[@id='cphContent_cphSection_lblOrderNumber']";
	public static final String OrderCancel_Popup_Path = ".//*[@id='cphContent_cphSection2_lblOverlayMessage']";
	public static final String OrderCancel_Popup_Close_Path = ".//*[@id='cphContent_cphSection2_btnClose']";
	public static final String OrderConfirm_Child1 = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_1']/td[1]";
	public static final String OrderConfirm_Child2 = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_2']/td[1]";
	public static final String OrderConfirm_Child3 = "//*[@id='ctl00_ctl00_cphContent_cphSection2_rgdOrderItems_ctl00_ctl06_Detail10__0:0_3']/td[1]";

	// MiniShopping cart
	public static final String Mini_Shoppin_cart_Item_path = ".//*[@id='cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_divFormNumber_0']/a";
	public static final String Mini_Shoppin_cart_Item2_path = ".//*[@id='cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_divFormNumber_1']/a";
	public static final String Mini_Shoppin_cart_Item1Qty_path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_txtQuantity']";
	public static final String Mini_Shoppin_cart_Item2Qty_path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl01_txtQuantity']";
	public static final String Mini_Shoppin_Quantity_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_repMiniShoppingCart_ctl00_txtQuantity']";
	public static final String Mini_Shoppin_QickADD_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_rtxtFormNumbers_Input']";
	public static final String Mini_Shoppin_Qty_Path = ".//*[@id='ctl00_ctl00_cphContent_cphLeftSide_ucMiniShoppingCart_txtQuantity']";
	public static final String Mini_Shoppin_Add_btn_Path = ".//*[@id='cphContent_cphLeftSide_ucMiniShoppingCart_btnQuickAdd']";
	public static final String Mini_Shoppin_Update_btn_Path = ".//*[@id='cphContent_cphLeftSide_ucMiniShoppingCart_btnUpdate']";
	public static final String Mini_Shoppin_Checkout_btn_Path = ".//*[@id='cphContent_cphLeftSide_ucMiniShoppingCart_btnCheckout']";
	public static final String Mini_Shoppin_Empty_Message = ".//*[@id='cphContent_cphLeftSide_ucMiniShoppingCart_lblEmptyCart']";

	// Manage Users Page

	public static final String MUser_Username = "//input[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_UserName']";

	public static final String MUser_Username_Filter = "//input[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_Filter_UserName']";

	public static final String MUser_Edit = "//a[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl04_EditButton']";

	public static final String MUser_forethougtOption = "//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl05_rtvBusinessOwners']/ul[1]/li[6]/div[1]/span[2]";
	public static final String MUser_UserPassword = "//input[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_UserPassword']";

	public static final String MUser_FirstName = "//input[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_FirstName']";

	public static final String MUser_LastName = "//input[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_LastName']";

	public static final String MUser_Email = "//input[@id ='ctl00_ctl00_cphContent_cphSection_rgrdUsers_ctl00_ctl02_ctl02_FilterTextBox_Email']";

	public static final String MUser_loading = "//*[@id='cphContent_cphSection_RadAjaxLoadingPanelctl00_ctl00_cphContent_cphSection_rgrdUsers']";

	// public static final String OrderTemplate_Title_Path =
	// ".//*[@id='ctl00_cphContent_RadDocka9e96c09-ce6c-4347-8918-a00399eb812b_T']";
	// public static final String OrderTemplate_Title_Path =
	// ".//*[@id='ctl00_cphContent_RadDocka9e96c09-ce6c-4347-8918-a00399eb812b_T']";

}
