package com.globalatlantic.OR;

public class MaterialsOR extends ObjectRepository{
	
	public static final String Manage_Catagories = "//span[contains(.,'Manage Categories')]";

	public static final String MC_AddCatagory = "//input[@title='Add Category']";
	
	public static final String MC_CatagoryName  = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_TB_CategoryName']";
	public static final String MC_CatagoryDescription  = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_TB_CategoryDescription']";
	public static final String MC_SortOrder  = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_RNTB_SortOrder']";
	public static final String MC_ActiveCheckbox  = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_ctl01']";
	public static final String MC_InsertBtn  = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_PerformInsertButton']";
	public static final String MC_CancelBtn  =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl02_ctl01_CancelButton']";
	public static final String MC_Refersh  ="//input[@class='rgRefresh firepath-matching-node']";
	public static final String MC_Annuity  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[1]";
	public static final String MC_brokerdealer  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[2]";
	public static final String MC_Commmonwealth  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[3]";
	public static final String MC_Corporate  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[4]";
	public static final String MC_Finalexpense  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[5]";
	public static final String MC_forethoughtCapfund  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[6]";
	public static final String MC_preneed  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[7]";
	public static final String MC_Traditionlife  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[8]";
	public static final String MC_Trust  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[9]";
	public static final String MC_TrustGuard  =".//*[@id='ctl00_ctl00_cphContent_cphSection_ddlBusinessOwner_DropDown']/div/ul/li[10]";
	
	public static final String MC_AddnewRecord =".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl05_Detail10_ctl03_ctl01_AddNewRecordButton']";

	public static final String MCSub_CatagoryName	=".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl07_Detail20_ctl02_ctl01_TB_CategoryName']";
	public static final String MCSub_CatagoryDesc=".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl07_Detail20_ctl02_ctl01_TB_CategoryDescription']";
	public static final String MCSub_SortOrder=".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl07_Detail20_ctl02_ctl01_RNTB_SortOrder']";
	public static final String MCSub_Active=".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl07_Detail20_ctl02_ctl01_ctl01']";
	public static final String MCSub_Insertbtn=".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl07_Detail20_ctl02_ctl01_PerformInsertButton']";
	public static final String MCSub_Cancelbtn=".//*[@id='ctl00_ctl00_cphContent_cphSection_rgCategories_ctl00_ctl07_Detail20_ctl02_ctl01_CancelButton']";
	
}
