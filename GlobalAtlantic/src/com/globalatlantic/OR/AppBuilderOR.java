package com.globalatlantic.OR;


public class AppBuilderOR extends CheckoutOR {
	
	public static final String AB_Title_path = ".//em[text()='Forethought Application Builder']";
	public static final String AB_Applicantreside_path = ".//*[@id='ctl00_cphContent_ddlStateApplication_Input']";
	public static final String AB_ApplicantresideArrow_path = ".//*[contains(@id,'ddlStateApplication_Arrow')]";
	public static final String AB_Signedissuedsold_path = ".//*[@id='ctl00_cphContent_ddlState_Input']";
	public static final String AB_SignedissuedsoldArrow_path = ".//*[contains(@id,'ddlState_Arrow')]";
	public static final String AB_Productfrom_path = ".//*[@id='ctl00_cphContent_ddlProductLine_Input']";
	public static final String AB_ProductfromArow_path = ".//*[contains(@id,'ddlProductLine_Arrow')]";
	public static final String AB_Q1_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[1]/td[2]";
	public static final String AB_Q1Y_path = ".//*[@id='Answer_Y_active_duty_military']";
	public static final String AB_Q1N_path = ".//*[@id='Answer_N_active_duty_military']";
	public static final String AB_Q2_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[4]/td[2]";
	public static final String AB_Q2Y_path = ".//*[@id='Answer_Y_Annuity_owned_by_Trust']";
	public static final String AB_Q2N_path = ".//*[@id='Answer_N_Annuity_owned_by_Trust']";
	
	public static final String AB_Q3_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[7]/td[2]";
	public static final String AB_Q3Y_path = ".//*[@id='Answer_Y_premium_exceed_half_of_net_worth']";
	public static final String AB_Q3N_path = ".//*[@id='Answer_N_premium_exceed_half_of_net_worth']";
	public static final String AB_Q3U_path = ".//*[@id='Answer_U_premium_exceed_half_of_net_worth']";
	
	public static final String AB_Q4_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[11]/td[2]";
	public static final String AB_Q4Y_path = ".//*[@id='Answer_Y_have_any_existing_policies']";
	public static final String AB_Q4N_path = ".//*[@id='Answer_N_have_any_existing_policies']";
	
	public static final String AB_Q5_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[14]/td[2]";
	public static final String AB_Q5Y_path = ".//*[@id='Answer_Y_replacing_existing_insurance_contract']";
	public static final String AB_Q5N_path = ".//*[@id='Answer_N_replacing_existing_insurance_contract']";
	
	public static final String AB_Q6_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[17]/td[2]";
	public static final String AB_Q6Y_path = ".//*[@id='Answer_Y_long_term_care_prod']";
	public static final String AB_Q6N_path = ".//*[@id='Answer_N_long_term_care_prod']";
	
	public static final String AB_Q7_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[20]/td[2]";
	public static final String AB_Q7Y_path = ".//*[@id='Answer_Y_from_another_company']";
	public static final String AB_Q7N_path = ".//*[@id='Answer_N_from_another_company']";
	
	public static final String AB_Q8_path = ".//*[@id='cphContent_DetailSection']/table/tbody/tr[23]/td[2]";
	public static final String AB_Q8Y_path = ".//*[@id='Answer_Y_over_65']";
	public static final String AB_Q8N_path = ".//*[@id='Answer_N_over_65']";
	public static final String AB_Q8over65age_path = ".//td[contains(text(),'Is the applicant 65 or over?')]";
	
	public static final String AB_Submitbtn_path = ".//a[text()='Submit']";
	public static final String AB_BuildMyApplicationPacket_path = ".//a[text()='Build My Application Packet']";
	public static final String AB_DownloadPacket_path = ".//a[text()='Download Packet']";

//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";
//	public static final String AB__path = "";

}
