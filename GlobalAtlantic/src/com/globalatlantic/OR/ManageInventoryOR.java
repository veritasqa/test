package com.globalatlantic.OR;

public class ManageInventoryOR extends ManageordersOR {

	// Manage Inventory
	public static final String Manage_Inventory_Path = ".//*[@id='Navigation_divLiList']/li[4]/ul/li[7]/a/span";

	// Create New Item
	public static final String ManageInventory_CreateNewType_Path = ".//*[@id='ctl00_cphContent_ddlNewType_Input']";
	public static final String ManageInventory_CreateNewType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlNewType_Arrow']";

	public static final String ManageInventory_POD_Path = "//*[@id='ctl00_cphContent_ddlNewType_DropDown']/div/ul/li[20]";

	public static final String ManageInventory_Add_Btn_Path = ".//*[@id='cphContent_btnAdd']";
	public static final String ManageInventory_FormNoToCopy_Path = ".//*[@id='ctl00_cphContent_ddlCopy_Input']";
	public static final String ManageInventory_Copy_Btn_Path = ".//*[@id='cphContent_btnCopy']";

	// Search Items
	public static final String ManageInventory_FormNo_Path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
	public static final String ManageInventory_ItemDescription_Path = ".//*[@id='ctl00_cphContent_txtDescription']";
	public static final String ManageInventory_Keyword_Path = ".//*[@id='ctl00_cphContent_txtKeyWord']";
	public static final String ManageInventory_Madatoryfieldtext_Path = ".//*[@id='cphContent_lblKeyWordInfo']";
	public static final String ManageInventory_InventoryType_Path = ".//*[@id='ctl00_cphContent_ddlType_Input']";
	public static final String ManageInventory_InventoryType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlType_Arrow']";
	public static final String ManageInventory_FormOwner_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_Input']";
	public static final String ManageInventory_FormOwner_Icon_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_Arrow']";

	public static final String ManageInventory_FormOwner_ForethoughtCapitalFundingOperations = "//li[text()='Forethought Capital Funding/Operations']";

	public static final String ManageInventory_Category_Path = ".//*[@id='ctl00_cphContent_ddlCategory_Input']";
	public static final String ManageInventory_Category_Icon_Path = ".//*[@id='ctl00_cphContent_ddlCategory_Arrow']";
	public static final String ManageInventory_BusinessOwner_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_Input']";
	public static final String ManageInventory_BusinessOwner_Icon_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_Arrow']";
	public static final String ManageInventory_UserGroups_Path = ".//*[@id='ctl00_cphContent_ddlAudience_Input']";
	public static final String ManageInventory_UserGroups_Icon_Path = ".//*[@id='ctl00_cphContent_ddlAudience_Arrow']";
	public static final String ManageInventory_Active_Path = ".//*[@id='ctl00_cphContent_ddlActive_Input']";
	public static final String ManageInventory_Active_Icon_Path = ".//*[@id='ctl00_cphContent_ddlActive_Arrow']";
	public static final String ManageInventory_UnitsOnHand_Path = ".//*[@id='ctl00_cphContent_ddlUOH_Input']";
	public static final String ManageInventory_UnitsOnHand_Icon_Path = ".//*[@id='ctl00_cphContent_ddlUOH_Arrow']";
	public static final String ManageInventory_UnitsOnHand_Outofstock_Path = ".//li[text()='Out of Stock']";
	public static final String ManageInventory_EmptySearchresult_Path = ".//*[@id='divContent clearfix']/div[8]/div[1]";

	public static final String MI_Defaultdropdown = "- Any -";

	public static final String ManageInventory_Search_Btn_Path = ".//*[@id='cphContent_btnSearch']";
	public static final String ManageInventory_Clear_Btn_Path = ".//*[@id='cphContent_btnClear']";
	public static final String ManageInventory_ResultsCopy_Btn_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[11]/a";
	public static final String ManageInventory_ResultsEdit_Btn_Path = ".//*[@id='ctl00_cphContent_Results_ctl00_ctl04_lnkEdit']";
	public static final String ManageInventory_Resultsloadingpanel_Btn_Path = ".//*[@id='cphContent_RadAjaxLoadingPanelctl00_cphContent_Results']";
	public static final String ManageInventory_FirstPage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[1]";
	public static final String ManageInventory_LastPage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[2]";
	public static final String ManageInventory_Nextpage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[3]/input[1]";
	public static final String ManageInventory_Previouspage_Nav_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[1]/input[2]";
	public static final String ManageInventory_PageNavField_Path = "";
	public static final String ManageInventory_TotalPages_Path = ".//*[@id='ctl00_cphContent_Results_ctl00']/tfoot/tr[2]/td/table/tbody/tr/td/div[5]/strong[2]";
	public static final String ManageInventory_PageSizeField_Path = ".//*[@id='ctl00_cphContent_Results_ctl00_ctl03_ctl02_PageSizeComboBox_Input']";
	public static final String ManageInventory_Resultspart_Path = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]";

	public static final String MI_Searchresult_Norecords = ".//*[text()='No records to display.']";

	// Search Grid

	public static final String ManageInventory_Result_copy_Path = ".//*[text()='QA_COPY_SEARCHRESULTS_X']//following::td[9]/a";
	public static final String ManageInventory_Result_copy_PopupOK_Path = "//*[@class='rwDialogPopup radconfirm']//div//*[text()='OK']";
	public static final String ManageInventory_Result_copy_PopupCANCEL_Path = "//*[@class='rwDialogPopup radconfirm']//div//*[text()='Cancel']";
	public static final String ManageInventory_Result_copy_PopupCLOSE_Path = "//tr[@class ='rwTitleRow']//following::a[@class='rwCloseButton']";

	// Edit Manage Inventory
	public static final String ManageInventory_Cat_TraditionalLife_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[1]/div/span[3]";
	public static final String ManageInventory_Cat_CommonWealth_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[2]/div/span[3]";
	public static final String ManageInventory_Cat_TrustGuard_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[3]/div/span[3]";
	public static final String ManageInventory_Cat_ForethoughtCapitalFunding_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[4]/div/span[3]";
	public static final String ManageInventory_Cat_Trust_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[5]/div/span[3]";
	public static final String ManageInventory_Cat_Corporate_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[6]/div/span[3]";
	public static final String ManageInventory_Cat_Preneed_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[7]/div/span[3]";
	public static final String ManageInventory_Cat_FinalExpense_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[8]/div/span[3]";
	public static final String ManageInventory_Cat_Annuity_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[9]/div/span[3]";
	public static final String ManageInventory_Cat_BrokerDealer_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[10]/div/span[3]";
	public static final String ManageInventory_Gen_Save_Btn_Path = ".//*[@id='cphContent_btnSave']";
	public static final String ManageInventory_Gen_Cancel_Btn_Path = ".//*[@id='cphContent_btnCancel']";
	public static final String ManageInventory_Gen_Next_Btn_Path = ".//*[@id='cphContent_btnNext']";

	public static final String ManageInventory_Title_Path = ".//*[@id='divContent clearfix']/div[5]/h1";
	public static final String ManageInventory_TitlePart_Path = ".//*[@id='cphContent_lblHeader']";

	public static final String ManageInventory_Save_message_Path = ".//*[@id='cphContent_NavigationBar_lblDialog']";
	public static final String ManageInventory_Save_OK_Btn_Path = ". //*[@id='cphContent_NavigationBar_btnOk']";

	// Rules Tab
	public static final String ManageInventory_RulesTab_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[5]/a/span/span/span";
	public static final String ManageInventory_Rules_Title_Path = ".//*[@id='cphContent_lblHeader']";
	public static final String ManageInventory_Rules_DefaultMaxOrderQty_Path = ".//*[@id='ctl00_cphContent_txtMaxOrderQty']";
	public static final String ManageInventory_Rules_DefaultMaxOrderQtyFrq_Path = ".//*[@id='ctl00_cphContent_ddlMaxOrderQuantityFrequency_Input']";
	public static final String ManageInventory_Rules_DefaultMaxOrderQtyFrq_icon_Path = ".//*[@id='ctl00_cphContent_ddlMaxOrderQuantityFrequency_Arrow']";
	public static final String ManageInventory_Rules_MaxOrderQtyPerRole_Field_Path = ".//*[@id='ctl00_cphContent_txtMaxOrderQtyRole']";
	public static final String ManageInventory_Rules_MaxOrderQtyPerRole_Drop_Path = ".//*[@id='ctl00_cphContent_cboMaxOrderQtyRole_Input']";
	public static final String ManageInventory_Rules_MaxOrderQtyPerRole_Add_Path = ".//*[@id='btnAddMaxOrderQty']";
	public static final String ManageInventory_Rules_AllowBackordersRadio_Path = ".//*[@id='cphContent_chkBackorder']";
	public static final String ManageInventory_Rules_ReplaceType_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Input']";
	public static final String ManageInventory_Rules_UnObsoletenow_Path = ".//a[text()='Un-Obsolete']";
	public static final String ManageInventory_Rules_ReplaceType_Icon_Path = ".//*[@id='ctl00_cphContent_ddlRuleType_Arrow']";
	public static final String ManageInventory_Rules_ReplaceWith_Field_Path = ".//*[@id='ctl00_cphContent_ddlReplaceWith_Input']";
	public static final String ManageInventory_Rules_ReplaceDate_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_dateInput']";
	public static final String ManageInventory_Rules_ReplaceDateCalender_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_popupButton']";
	public static final String ManageInventory_Rules_ReplaceDateTime_Path = ".//*[@id='ctl00_cphContent_dtReplaceDate_timePopupLink']";
	public static final String ManageInventory_Rules_ObsoleteDate_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_dateInput']";
	public static final String ManageInventory_Rules_ObsoleteDateCaleder_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_popupButton']";
	public static final String ManageInventory_Rules_ObsoleteDateTime_Path = ".//*[@id='ctl00_cphContent_dtObsoleteDate_timePopupLink']";
	public static final String ManageInventory_Rules_Reason_Field_Path = ".//*[@id='ctl00_cphContent_txtReason']";
	public static final String ManageInventory_Rules_RequireOrders_Radio_Path = ".//*[@id='cphContent_chkRequiredOnAllOrders']";
	public static final String ManageInventory_Rules_StateMode_ExcRadio_Path = ".//*[@id='cphContent_rdbStateMode_0']";
	public static final String ManageInventory_Rules_StateMode_IncRadio_Path = ".//*[@id='cphContent_rdbStateMode_1']";
	public static final String ManageInventory_Rules_States_Drop_Path = ".//*[@id='ctl00_cphContent_ddlIncludeExcludeStates_List']/table/tbody/tr/td[1]";
	public static final String ManageInventory_Rules_FirmMode_ExcRadio_Path = ".//*[@id='cphContent_rblFirmMode_0']";
	public static final String ManageInventory_Rules_FirmMode_IncRadio_Path = ".//*[@id='cphContent_rblFirmMode_1']";
	public static final String ManageInventory_Rules_Firms_Drop_Path = ".//*[@id='ctl00_cphContent_ddlIncludeExcludeFirms_List_Input']";
	public static final String ManageInventory_Rules_Firms_Drop_valuePath = "//*[@id='cphContent_lblIncludeExcludeFirms']";

	public static final String ManageInventory_Rules_Crossover_Radio_Path = ".//*[@id='cphContent_IMOBDCheckBox']";

	public static final String ManageInventory_Rules_Save_Btn_Path = ".//*[@id='cphContent_btnSave']";
	public static final String ManageInventory_Rules_Save_message_Path = ".//*[@id='cphContent_NavigationBar_lblDialog']";
	public static final String ManageInventory_Rules_cancel_Btn_Path = ".//*[@id='cphContent_btnCancel']";

	public static final String ManageInventory_Rules_Back_Btn_Path = ".//*[@id='cphContent_btnBack']";
	public static final String ManageInventory_Rules_Next_Btn_Path = ".//*[@id='cphContent_btnNext']";
	public static final String ManageInventory_Rules_OK_Btn_Path = ".//*[@id='cphContent_NavigationBar_btnOk']";

	// General
	public static final String ManageInventory_NewItem_header_Path = ".//*[@id='cphContent_lblHeader']";

	public static final String ManageInventory_General_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[1]/a/span/span/span";
	public static final String ManageInventory_FormTxt_Path = ".//*[@id='ctl00_cphContent_txtFormNumber']";
	public static final String ManageInventory_txtAlias_Path = ".//*[@id='ctl00_cphContent_txtAlias']";
	public static final String ManageInventory_txtDescription_Path = ".//*[@id='ctl00_cphContent_txtDescription']";
	public static final String ManageInventory_txtLongDescription_Path = ".//*[@id='ctl00_cphContent_txtLongDescription']";
	public static final String ManageInventory_txtKeyWords_Path = ".//*[@id='ctl00_cphContent_txtKeyWords']";
	public static final String ManageInventory_txtRevisionDate_Path = ".//*[@id='ctl00_cphContent_txtRevisionDate']";
	public static final String ManageInventory_txtPredecessor_Path = ".//*[@id='ctl00_cphContent_txtPredecessor']";
	public static final String ManageInventory_InventoryType_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlInventoryType_Arrow']";
	public static final String ManageInventory_ddInventoryType_Path = ".//*[@id='ctl00_cphContent_ddlInventoryType_Input']";

	public static final String ManageInventory_InventoryType_DropDown_Path = ".//*[@id='ctl00_cphContent_ddlInventoryType_DropDown']/div/ul/li[1]";

	public static final String ManageInventory_InventoryType_DropDown_packitem_Path = ".//li[text()='Pack Item']";

	public static final String ManageInventory_ProductLineType_List_Arrow_Path = ".//*[@id='ctl00_cphContent_ddclProductLineType_List_Arrow']";
	public static final String ManageInventory_ProductLineType_List_Input_Path = ".//*[@id='ctl00_cphContent_ddclProductLineType_List_Input']";
	public static final String ManageInventory_ProductLineType_List_Path = ".//*[@id='ctl00_cphContent_ddclProductLineType_List_i0_Grid_ctl00__0']/td[2]";

	public static final String ManageInventory_ProductCategory_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_Arrow']";
	public static final String ManageInventory_ProductCategory_Input_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_Input']";
	public static final String ManageInventory_ProductCategory_Marketing_Path = ".//*[@id='ctl00_cphContent_ddlProductCategory_DropDown']/div/ul/li[2]";

	public static final String ManageInventory_FormOwner_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_Arrow']";
	public static final String ManageInventory_FormOwner_Input_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_Input']";
	public static final String ManageInventory_FormOwner_Broker_Dealer_Path = ".//*[@id='ctl00_cphContent_ddlFormOwner_DropDown']/div/ul/li[2]";

	public static final String ManageInventory_BusinessOwner_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_Arrow']";
	public static final String ManageInventory_BusinessOwner_Input_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_Input']";
	public static final String ManageInventory_BusinessOwner_Annuity_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_DropDown']/div/ul/li[2]";

	public static final String ManageInventory_Viewability_Arrow_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Arrow']";
	public static final String ManageInventory_Viewability_Input_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Input']";
	public static final String ManageInventory_Viewability_NotViewable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[4]";

	public static final String ManageInventory_Viewability_input = ".//*[@id='ctl00_cphContent_ddlViewability_Input']";

	public static final String ManageInventory_Admin_Only_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[2]";

	public static final String ManageInventory_Not_Viewable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[5]";

	public static final String ManageInventory_Orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[4]";

	public static final String ManageInventory_Viewable_not_orderable_Path = ".//*[@id='ctl00_cphContent_ddlViewability_DropDown']/div/ul/li[3]";

	public static final String ManageInventory_EffectiveDate_dateInput_Path = ".//*[@id='ctl00_cphContent_dtEffectiveDate_dateInput']";
	public static final String ManageInventory_chkFulfillment_Path = ".//*[@id='cphContent_chkFulfillment']";
	public static final String ManageInventory_chkMarketing_Path = ".//*[@id='cphContent_chkMarketing']";
	public static final String ManageInventory_chkDocumentLibrary_Path = ".//*[@id='cphContent_chkDocumentLibrary']";
	public static final String ManageInventory_chkAppBuilder_Path = ".//*[@id='cphContent_chkAppBuilder']";
	public static final String ManageInventory_All_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/div/span[3]";
	public static final String ManageInventory_BrokerDealer_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[1]/div/span[3]";
	public static final String ManageInventory_CommonWealth_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[2]/div/span[3]";
	public static final String ManageInventory_Corporate_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[3]/div/span[3]";
	public static final String ManageInventory_FinalExpense_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[4]/div/span[3]";
	public static final String ManageInventory_ForethoughtCaptial_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[5]/div/span[3]";
	public static final String ManageInventory_IMOChannel_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[6]/div/span[3]";
	public static final String ManageInventory_Preneed_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[7]/div/span[3]";
	public static final String ManageInventory_Traditionallife_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[8]/div/span[3]";
	public static final String ManageInventory_Trust_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[9]/div/span[3]";
	public static final String ManageInventory_TrustGuard_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[10]/div/span[3]";
	public static final String ManageInventory_txtNotes_Path = ".//*[@id='ctl00_cphContent_txtNotes']";
	public static final String ManageInventory_txtSpecialMessage_Path = ".//*[@id='ctl00_cphContent_txtSpecialMessage']";
	public static final String ManageInventory_MessageActivationDate_Path = ".//*[@id='ctl00_cphContent_dtSpecialMessageActivationDate_dateInput']";
	public static final String ManageInventory_MessageDeactivationDate_Path = ".//*[@id='ctl00_cphContent_dtSpecialMessageDeactivationDate_dateInput']";
	public static final String ManageInventory_InventoryTypeInput_Path = ".//*[@id='ctl00_cphContent_ddlInventoryType_Input']";

	public static final String ManageInventory_usergroup_annuity_Path = ".//*[@id='ctl00_cphContent_rtvAudience']/ul/li/ul/li[6]/ul/li[2]/div/span[2]";

	public static final String ManageInventory_Category_Anunity_Path = ".//*[@id='ctl00_cphContent_rtvCategory']/ul/li[9]/ul/li[1]/div/label/input";

	// pricing
	public static final String ManageInventory__Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[3]/a/span/span/span";
	public static final String ManageInventory_Flat_Path = ".//*[@id='cphContent_optType_0']";
	public static final String ManageInventory_Tiered_Path = ".//*[@id='cphContent_optType_1']";
	public static final String ManageInventory_NoCostItem_Path = ".//*[@id='cphContent_chkNoCostItem']";
	public static final String ManageInventory_ChargeItem_Path = ".//*[@id='cphContent_chkNotCreditCardChargeItem']";
	public static final String ManageInventory_ChargebackCost_Path = ".//*[@id='ctl00_cphContent_txtPrintCost']";
	public static final String ManageInventory_Tired_Path = ".//*[@id='ctl00_cphContent_ddlTieredPricingType_Input']";

	// Notification
	public static final String ManageInventory_Notifications_Path = ".//*[@id='ctl00_cphContent_NavigationBar_NavigationBar']/div/ul/li[4]/a/span/span/span";
	public static final String ManageInventory_chkNotificationEmail_Path = ".//*[@id='cphContent_chkNotificationEmail']";
	public static final String ManageInventory_txtQuantity_Path = ".//*[@id='ctl00_cphContent_txtQuantity']";
	public static final String ManageInventory_txtOrderQuantity_Path = ".//*[@id='ctl00_cphContent_txtOrderQuantity']";
	public static final String ManageInventory_EditButton_Path = ".//*[@id='ctl00_cphContent_Addresses_ctl00_ctl04_EditButton']";
	public static final String ManageInventory_DeleteButton_Path = ".//*[@id='ctl00_cphContent_Addresses_ctl00__0']/td[4]/a";
	public static final String ManageInventory_AddNewRecordButton_Path = ".//*[@id='ctl00_cphContent_Addresses_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String ManageInventory_txtEmail_Path = ".//*[@id='ctl00_cphContent_Addresses_ctl00_ctl02_ctl02_txtEmail']";
	public static final String ManageInventory_btnUpdate_Path = ".//*[@id='ctl00_cphContent_Addresses_ctl00_ctl02_ctl02_btnUpdate']";
	public static final String ManageInventory_btnCancel_Path = ".//*[@id='ctl00_cphContent_Addresses_ctl00_ctl02_ctl02_btnCancel']";

	// results on manageinventory search page

	public static final String MI_Searchresult_FormNo = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[2]";
	public static final String MI_Searchresult_Status = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[3]";
	public static final String MI_Searchresult_Type = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[4]";
	public static final String MI_Searchresult_ItemDEsc = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[5]";
	public static final String MI_Searchresult_Createdate = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[6]";
	public static final String MI_Searchresult_Onhand = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[7]";
	public static final String MI_Searchresult_Unitsavailable = ".//*[@id='ctl00_cphContent_Results_ctl00__0']/td[8]";

	public String CatagoriesCheckbox(String Checkboxoption) {

		return ".//h1[contains(text(),'Categories')]/following::span[text()='" + Checkboxoption
				+ "']/preceding::input[1]";
	}
}
