package com.globalatlantic.OR;

public class TicketsOR extends TraditionalLifeOR {

	// Support Ticket Page
	public static final String SupportTic_Title_Path = ".//*[@id='divContent clearfix']/div[2]/h1";
	public static final String AddTicket_Icon_path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String AddTicket_Label_path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl03_ctl01_InitInsertButton']";

	// Support Edit Ticket Page
	public static final String Cateogory_Dropdown_Path = ".//*[@id='cphContent_cphSection_ddlCategory']";
	public static final String SubCateogory_Dropdown_Path = ".//*[@id='cphContent_cphSection_ddlSubCategory']";
	public static final String Ordernumberfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rcmbxOrderNumber_Input']";
	public static final String Description_Path = ".//*[@id='cphContent_cphSection_txtDescription']";
	public static final String SPT_Username_Path = ".//*[@id='cphContent_cphSection_lblCreateUser']";
	public static final String Status_Dropdown_Path = ".//*[@id='cphContent_cphSection_ddlStatus']";
	public static final String Priority_Dropdown_Path = ".//*[@id='cphContent_cphSection_ddlPriority']";
	public static final String AssignedTo_Dropdown_Path = ".//*[@id='cphContent_cphSection_ddlAssignedTo']";
	public static final String UpdateTicket_Btn_Path = ".//*[@id='cphContent_cphSection_btnSubmit']";
	public static final String Choosefile_Btn_Path = ".//*[@id='cphContent_cphSection_fupAttachments']";
	public static final String AddNewTicket_Btn_Path = ".//*[@id='cphContent_cphSection_btnSubmit']";
	public static final String Ticketupdate_Popup_Path = ".//*[@id='cphContent_cphPageFooter_divOverlay']/div/div/div[2]/div";
	public static final String Ticketupdate_Popup_OK_Path = ".//*[@id='cphContent_cphPageFooter_btnOk']";
	public static final String AddNewTicketupdate_Popup_Path = ".//*[@id='cphContent_cphPageFooter_divOverlay']/div/div/div[2]/div";
	public static final String AddNewTicketupdate_Popup_OK_Path = ".//*[@id='cphContent_cphPageFooter_btnOk']";
	public static final String CommentCoulumn_Btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdCollaborationComments_ctl00__0']/td[3]";
	public static final String AddComment_Btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdCollaborationComments_ctl00_ctl03_ctl01_AddNewRecordButton']";
	public static final String Comment_Textfield_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdCollaborationComments_ctl00_ctl02_ctl02_rtxtComment']";
	public static final String CreateComment_btn_Path = ".//*[@id='ctl00_ctl00_cphContent_cphSection_rgrdCollaborationComments_ctl00_ctl02_ctl02_btnSaveRole']";

	// Support Edit Ticket Page - Filter buttons
	public static final String TicketNumber_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_CollaborationID']";
	public static final String OrderNumber_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_OrderID']";
	public static final String Category_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_Category']";
	public static final String SubCategory_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_SubCategory']";
	public static final String FormNumber_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_InventoryID']";
	public static final String SubmittedBy_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_ReportedByUserName']";
	public static final String SubmittedOn_Date_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_RDIPFReportedOn_popupButton']";
	public static final String SubmittedOn_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_ReportedOn']";
	public static final String Status_Filter_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_Filter_Status']";

	public static final String TicketNumber_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_RNTBF_CollaborationID']";
	public static final String OrderNumber_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_OrderID']";
	public static final String Category_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_Category']";
	public static final String SubCategory_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_SubCategory']";
	public static final String FormNumber_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_InventoryID']";
	public static final String SubmittedBy_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_ReportedByUserName']";
	public static final String SubmittedOn_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_RDIPFReportedOn_dateInput']";
	public static final String Status_Text_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_ctl00_ctl02_ctl02_FilterTextBox_Status']";

	// Support Edit Ticket Page - Filter Options
	public static final String Status_Contains_Path = ".//*[@id='ctl00_cphContent_rgrdCollaboration_rfltMenu_detached']/ul/li[2]/a/span";

	
	
}
