package com.globalatlantic.OR;

public class CheckoutOR extends DocmentLibraryOR {
	// Checkout Page

	public static final String Checkout_Title_path = ".//*[@id='cphContent_divHeaderImage']/h1";
	public static final String Checkout_Formnum_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_lblFormNumber']";
	public static final String Checkout_Qty_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_rntxtQuantity']";
	public static final String Checkout_Update_Btn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnUpdate']";
	public static final String Checkout_Remove_Btn_Path = ".//*[@id='ctl00_cphContent_rgrdShoppingCart_ctl00_ctl04_btnDelete']";
	public static final String Checkout_ShowComponentDetails_Btn_Path = ".//a[text()='Show Component Details']";

	public static final String Checkout_Shippinginfo_Path = ".//span[text()='Shipping Information']";
	public static final String Checkout_Name_Path = ".//*[@id='cphContent_txtName']";
	public static final String Checkout_Compny_Path = ".//*[@id='cphContent_txtCompany']";
	public static final String Checkout_Address1_Path = ".//*[@id='cphContent_txtAddress1']";
	public static final String Checkout_Address2_Path = ".//*[@id='cphContent_txtAddress2']";
	public static final String Checkout_Address3_Path = ".//*[@id='cphContent_txtAddress3']";
	public static final String Checkout_City_Path = ".//*[@id='cphContent_txtCity']";
	public static final String Checkout_State_Path = ".//*[@id='cphContent_txtState']";
	public static final String Checkout_Zip_Path = ".//*[@id='cphContent_txtZip']";
	public static final String Checkout_Country_Path = ".//*[@id='cphContent_ddlCountry']";
	public static final String Checkout_txtPhone_Path = ".//*[@id='cphContent_txtPhone']";
	public static final String Checkout_Email_Path = ".//*[@id='cphContent_txtEmail']";
	public static final String Checkout_CreateOrder_template_Path = ".//*[@id='cphContent_btnShowCreateOrderTemplatePopup']";
	public static final String Checkout_ClearCart_Btn_Path = ".//*[@id='cphContent_btnClearCart']";

	public static final String Checkout_CreateOrder_template_Popup_Path = ".//*[@id='cphContent_divOverlay']/div/div/div[2]/div";
	public static final String Checkout_CreateOrder_template_Popup_Name_Path = ".//*[@id='ctl00_cphContent_txtOrderTemplateName']";
	public static final String Checkout_CreateOrder_template_Popup_Desc_Path = ".//*[@id='ctl00_cphContent_txtOrderTemplateDescription']";
	public static final String Checkout_CreateOrder_template_Popup_Create_btn_Path = ".//*[@id='cphContent_btnCreateOrderTemplate']";
	public static final String Checkout_CreateOrder_template_Popup_Cancel_btn_Path = ".//*[@id='cphContent_btnHideCreateOrderTemplatePopup']";
	public static final String Checkout_CreateOrder_template_Message_Path = ".//*[@id='cphContent_lblCommandBar']/div";

}
