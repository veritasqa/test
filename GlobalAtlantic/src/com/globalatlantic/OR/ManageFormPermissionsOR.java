package com.globalatlantic.OR;

public class ManageFormPermissionsOR extends ManageInventoryOR {
	
	public static final String ManageFormPermissions_Path = ".//span[text()='Manage Form Permissions']";
	public static final String MFP_InventorySearchFilters_Path = ".//*[@id='cphContent_pnlSearchCBD']/div[1]/h1[text()='Inventory Search Filters']";
	public static final String MFP_ProductLineTypeinput_Path = ".//*[@id='ctl00_cphContent_ddlProductLineTypeCBD_Input']"; 
	public static final String MFP_ProductLineTypearrow_Path = ".//*[@id='ctl00_cphContent_ddlProductLineTypeCBD_Arrow']"; 
	public static final String MFP_Searchbtn_Path = ".//*[@id='cphContent_btnSearchCBD' and text()='Search']"; 
	public static final String MFP_Searchloading_Path = ".//*[@id='cphContent_RadAjaxLoadingPanelcphContent_pnlSearchCBD']"; 
	public static final String MFP_Pagesizearrow_Path = ".//*[@id='ctl00_cphContent_ResultsCBD_ctl00_ctl03_ctl02_PageSizeComboBox_Arrow']"; 
	public static final String MFP_Pagesizeloading_Path = ".//*[@id='cphContent_RadAjaxLoadingPanelctl00_cphContent_ResultsCBD']"; 
	public static final String MFP_Miscellaneous_Path = ".//*[@id='cphContent_BOPanel']/div[1]/h1[text()='Miscellaneous']"; 
	public static final String MFP_BusinessOwnerinput_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_Input']"; 
	public static final String MFP_BusinessOwnerarrow_Path = ".//*[@id='ctl00_cphContent_ddlBusinessOwner_Arrow']"; 
	public static final String MFP_Viewabilityinput_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Input']"; 
	public static final String MFP_Viewabilityarrow_Path = ".//*[@id='ctl00_cphContent_ddlViewability_Arrow']"; 
	public static final String MFP_EffectiveDate_Path = ".//*[@id='ctl00_cphContent_dtEffectiveDate_dateInput']"; 
	public static final String MFP_UpdateBusinessOwnerbtn_Path = ".//a[text()='Update Business Owner']"; 
	public static final String MFP_UpdateViewabilitybtn_Path = ".//a[text()='Update Viewability']"; 
	public static final String MFP_UpdateEffectiveDatebtn_Path = ".//a[text()='Update Effective Date']"; 
	public static final String MFP_Updatesuccessful_Path = ".//span[text()='Update is successful']"; 
	public static final String MFP_UpdatesuccessfulOK_Path = ".//*[@id='cphContent_btnOk']"; 
//	public static final String MFP__Path = ""; 
//	public static final String MFP__Path = ""; 
//	public static final String MFP__Path = ""; 
//	public static final String MFP__Path = ""; 
//	public static final String MFP__Path = ""; 


}
