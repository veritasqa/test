package com.globalatlantic.OR;

public class SSO_OR extends TicketsOR	{
	
	public static final String SSO_UserID_Path = ".//*[@id='UserID']"; 
	public static final String SSO_Firstname_Path = ".//*[@id='FIRSTNAME']"; 
	public static final String SSO_Lastname_Path = ".//*[@id='LASTNAME']"; 
	public static final String SSO_Emailaddress_Path = ".//*[@id='EmaillAddress']"; 
	public static final String SSO_Address1_Path = ".//*[@id='ADDRESSLINE1']"; 
	public static final String SSO_City_Path = ".//*[@id='CITY']"; 
	public static final String SSO_State_Path = ".//*[@id='STATE']"; 
	public static final String SSO_Zip_Path = ".//*[@id='ZipCode']"; 
	public static final String SSO_BusinessOwner_Path = ".//*[@id='BusinessOwner']"; 
	public static final String SSO_UserGroups_Path = ".//*[@id='UserGroups']"; 
	public static final String SSO_Security_Path = ".//*[@id='Security']"; 
	public static final String SSO_ReturnURL_Path = ".//*[@id='ReturnURL']"; 
	public static final String SSO_AutoLogin_Path = ".//*[@id='AutoLogin']"; 
	public static final String SSO_TestMode_Path = ".//*[@id='TestMode']"; 
	public static final String SSO_Submitbtn_Path = ".//*[@id='btnSubmit']"; 

}
