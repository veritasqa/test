package com.globalatlantic.OR;

import com.globalatlantic.values.ValueRepository;

public class WidgetOR  extends ValueRepository {

	public String Widgetname = "";

	// Add Widget
	public static final String Add_Widget_Path = ".//*[@id='addWidgetSection']/h3";

	public static final String Express_Checkout_Path = ".//*[@id='cphContent_btnAddWidgetExpressCheckout']";
	public static final String Express_Select_agent_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String Express_Form_Suggestion_Path = "_ctl00_rtxtFormNumber1_DropDown']/div/ul/li";
	public static final String Express_Form_num1_Path = "_ctl00_rtxtFormNumber1_Input']";
	public static final String Express_Form_num2_Path = "_ctl00_rtxtFormNumber2_Input']";
	public static final String Express_Form_num3_Path = "_ctl00_rtxtFormNumber3_Input']";
	public static final String Express_Form_num4_Path = "_ctl00_rtxtFormNumber4_Input']";
	public static final String Express_Form_num5_Path = "_ctl00_rtxtFormNumber5_Input']";
	public static final String Express_Form_Qty1_Path = "_ctl00_txtQuantity1']";
	public static final String Express_Form_Qty2_Path = "_ctl00_txtQuantity2']";
	public static final String Express_Form_Qty3_Path = "_ctl00_txtQuantity3']";
	public static final String Express_Form_Qty4_Path = "_ctl00_txtQuantity4']";
	public static final String Express_Form_Qty5_Path = "_ctl00_txtQuantity5']";
	public static final String Express_checkout_Btn_Path = "_ctl00_btnAddToCart']";
	public static final String Express_ErrorMessage_Path = "_ctl00_lblErrorMessage1']";

	// New Formsand materials
	public static final String New_Form_Path = ".//*[@id='cphContent_btnAddWidgetNewProducts']";
	public static final String NewForm_SelectAn_Agent_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String Item1_Qty_path = "_ctl00_txtQuantity1']";
	public static final String Item2_Qty_path = "_ctl00_txtQuantity2']";
	public static final String Item3_Qty_path = "_ctl00_txtQuantity3']";
	public static final String Form_Checkout_btn_path = "_ctl00_btnAddToCart']";

	public static final String Order_Lookup_Path = ".//*[@id='cphContent_btnAddWidgetOrderLookup']";

	// Quick Search
	public static final String Quick_Search_Path = ".//*[@id='cphContent_btnAddWidgetProductSearch']";
	public static final String Quick_Search_txt_path = "_ctl00_txtKeyword']";
	public static final String Quick_Search_btn_Path = "_ctl00_btnProductSearch']";

	public static final String My_Recent_Orders_Path = ".//*[@id='cphContent_btnAddWidgetMyOrders']";

	// Most popular Items
	public static final String WidgetId1 = "Most Popular Items";
	public static final String Most_Popular_Items_Path = ".//*[@id='cphContent_btnAddWidgetPopularItems']";
	public static final String MostPopularItems_Select_Agent_Path = "_ctl00_lblFillOutContactInfo']/a";
	public static final String MostPopularItems1_Qty_Path = "_ctl00_txtQuantity1']";
	public static final String MostPopularItems2_Qty_Path = "_ctl00_txtQuantity2']";
	public static final String MostPopularItems3_Qty_Path = "_ctl00_txtQuantity3']";
	public static final String MostPopularItems4_Qty_Path = "_ctl00_txtQuantity4']";
	public static final String MostPopularItems5_Qty_Path = "_ctl00_txtQuantity5']";
	public static final String MostPopularItems_Checkout_Btn_Path = "_ctl00_btnAddToCart']";

	
	
	   public static final String Order_Templates_Path = ".//*[@id='cphContent_btnAddWidgetOrderTemplates']";
	// Order Template Widget
		public static final String OrderTemplate_Widget_Path = ".//*[@id='cphContent_btnAddWidgetOrderTemplates']";
		public static final String OrderTemplate_Title_Path = ".//*[@id='ctl00_cphContent_RadDocka9e96c09-ce6c-4347-8918-a00399eb812b_T']";
		// public static final String OrderTemplate_HideIcon_Path =
		// ".//*[@id='ctl00_cphContent_RadDocka9e96c09-ce6c-4347-8918-a00399eb812b_T']/ul/li[1]/a/span";
		// public static final String OrderTemplate_CloseIcon_Path =
		// ".//*[@id='ctl00_cphContent_RadDocka9e96c09-ce6c-4347-8918-a00399eb812b_T']/ul/li[2]/a/span";
		public static final String OrderTemplate_Template1Name_Path = ".//*[@id='divOrderTemplates']/tbody/tr/td[1]/div";
		public static final String OrderTemplate_Template1View_Btn_Path = ".//*[@id='divOrderTemplates']/tbody/tr/td[2]/a[1]";
		public static final String OrderTemplate_Template1AddtoCart_Btn_Path = "_ctl00_repOrderTemplates_ctl01_btnAddToCart']";
		public static final String OrderTemplate_Selectagent_Path = "_ctl00_lblWidgetOff']/a";
		
		// Order Template Page
		public static final String OrderTemplatePage_Title_Path = ".//*[@id='cphContent_divHeaderImage']/h1";
		public static final String OrderTemplatePage_Name_Path = ".//*[@id='cphContent_lstOrderTemplates_divHeader_0']/a";

		public static final String OrderTemplatePage_Delete_btn_Path = ".//*[@id='cphContent_lstOrderTemplates_btnOrderTemplateDelete_0']";
		public static final String OrderTemplatePage_AddtoCart_btn_Path = ".//*[@id='cphContent_lstOrderTemplates_btnOrderTemplateAddToCart_0']";
		public static final String OrderTemplatePage_Part1_Path = ".//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_rgdOrderTemplateInventory_ctl00__0']/td[2]/span[1]";
		public static final String OrderTemplatePage_Part2_Path = ".//*[@id='ctl00_cphContent_lstOrderTemplates_ctrl0_rgdOrderTemplateInventory_ctl00__1']/td[2]/span[1]";
		  public static final String OrderTemplatePage_addtocart_Part1_Path = "//*[@id='cphContent_lstOrderTemplates_btnOrderTemplateAddToCart_0']";
}
