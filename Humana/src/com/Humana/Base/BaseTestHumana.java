package com.Humana.Base;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.Humana.Methods.CommonMethodsHumana;

public class BaseTestHumana extends CommonMethodsHumana {

	@BeforeSuite
	public void openbrowser() throws IOException, InterruptedException {

		Open_Browser(Browser, Browserpath);
		OpenUrl_Window_Max(URL);
		Implicit_Wait();
	}

	@AfterSuite(enabled = false)
	public void Aftersuite() throws InterruptedException {

		// ordernum.add("5570031");

		if (OrderNumberlist.isEmpty() == false) {

			Cancelallorders();
		}
	}

	public void login(String UserName, String Password) throws IOException, InterruptedException {

		if (Element_Is_Present(ID, HU_Login_UserName_ID)) {

			Type(ID, HU_Login_UserName_ID, UserName);
			Type(ID, HU_Login_Password_ID, Password);
			Click(ID, HU_Login_LoginBtn_ID);
			Assert.assertTrue(Element_Is_Present(Xpath, HU_LP_Logout_Path));
			Implicit_Wait();
		}

		else {
			logout();
			Type(ID, HU_Login_UserName_ID, UserName);
			Type(ID, HU_Login_Password_ID, Password);
			Click(ID, HU_Login_LoginBtn_ID);
			Assert.assertTrue(Element_Is_Present(Xpath, HU_LP_Logout_Path));
			Implicit_Wait();
		}

	}

	public void Clearcarts() throws InterruptedException {

		Click(Xpath, HU_LP_ClearCart_Path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, HU_LP_ViewAllAnnouncements_path);
	}

	public void Homepage() throws InterruptedException {

		Click(Xpath, HU_LP_Home_path);
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, HU_LP_ViewAllAnnouncements_path);

	}

	public void Cancelallorders() {

		// Open_Browser(Browser,Browserpath);
		Get_URL(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);

		for (String Onum : OrderNumberlist) {

			ExplicitWait_Element_Visible(Xpath, Inventory_OrderSearch_Path);
			Click(Xpath, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
			Type(Xpath, Inventory_Ordernumber_Path, Onum);
			Click(Xpath, Inventory_Search_Btn_Path);
			Click(Xpath, Inventory_Edit_Btn_Path);

			if (Get_Attribute(Xpath, Inventory_Order_Status, "Value").equalsIgnoreCase("NEW")) {

				Click(Xpath, Inventory_Cancelorder_Btn_Path);
				Accept_Alert();

				Assert.assertTrue(Get_Attribute(Xpath, Inventory_Order_Status, "Value").equalsIgnoreCase("Cancelled"),
						"Order is not cancelled is not displayed");

			}
			Click(Xpath, Inventory_OrderSearch_Path);
			ExplicitWait_Element_Clickable(Xpath, Inventory_Search_Btn_Path);
		}

		Click(Xpath, Inventory_logout_btn_Path);
		ExplicitWait_Element_Visible(Xpath, Inventory_Username_Path);

	}

	public void FulfilmentSearch(String PieceName) throws InterruptedException {
		Type(ID, HU_LP_FulfilmentSearch_TxtBox_ID, PieceName);
		Click(ID, HU_LP_FulfilmentSearch_Btn_ID);
		Wait_ajax();
	}

	public void logout() throws IOException, InterruptedException {

		Click(Xpath, HU_LP_Logout_Path);
		ExplicitWait_Element_Clickable(ID, HU_Login_LoginBtn_ID);

	}

	public void AddWidget(String WidgetName) throws InterruptedException {
		if (!Element_Is_Displayed(Xpath, Textpath("em", WidgetName))) {
			Hover(Xpath, Textpath("h3", "Add Widgets"));
			Wait_ajax();
			Click(Xpath, Textpath("a", WidgetName));
		}
	}

	public void Datepicker2(String CalXpath, String Cal_locvalue, String CalLocatormonthtype, String CalMonth,
			String Get_Month_MMM, String Get_Date_d, String Get_Year_YYYY) throws InterruptedException {

		Click(CalXpath, Cal_locvalue);
		Wait_ajax();
		Click(CalLocatormonthtype, CalMonth);
		Click(Xpath, ".//a[text()='" + Get_Month_MMM + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Year_YYYY + "']");
		Wait_ajax();
		Click(Xpath, ".//a[text()='" + Get_Year_YYYY + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//input[@value='OK']");
		Click(Xpath, ".//input[@value='OK']");
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Date_d + "']");
		Click(Xpath, ".//a[text()='" + Get_Date_d + "']");
	}

	public void Datepicker(String Month, String Ok_Btn) throws InterruptedException {

		Click(ID, Month);
		Click(Xpath, ".//a[text()='" + Get_Futuredate("MMM") + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Ok_Btn);
		Click(ID, Ok_Btn);
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
		Click(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
	}

	public void DatepickerTodaysDate(String MonthLocator, String Month, String Ok_Btn) throws InterruptedException {

		Click(MonthLocator, Month);
		Click(Xpath, ".//a[text()='" + Get_Todaydate("MMM") + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='" + Get_Todaydate("YYYY") + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Ok_Btn);
		Click(ID, Ok_Btn);
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
		Click(Xpath, ".//a[text()='" + Get_Todaydate("d") + "']");
	}

	public void DatepickerPastDate(String MonthLocator, String Month, String Ok_Btn) throws InterruptedException {

		Click(MonthLocator, Month);
		Click(Xpath, ".//a[text()='" + Get_Pastdate("MMM") + "']");
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("YYYY") + "']");
		Thread.sleep(2000);
		Click(Xpath, ".//a[text()='" + Get_Pastdate("YYYY") + "']");
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(ID, Ok_Btn);
		Click(ID, Ok_Btn);
		Thread.sleep(2000);
		ExplicitWait_Element_Clickable(Xpath, ".//a[text()='" + Get_Futuredate("d") + "']");
		Click(Xpath, ".//a[text()='" + Get_Pastdate("d") + "']");
	}

	public void Selectfilter(String Filter, String Filteroption) throws InterruptedException {

		Click(Xpath, Filter);
		Wait_ajax();
		Click(Xpath, ".//span[text()='" + Filteroption + "']");
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);

	}

	public void Inventory_Login() {

		Get_URL(Inventory_URL);
		Type(Xpath, Inventory_Username_Path, Inventory_UserName);
		Type(Xpath, Inventory_Passowrd_Path, Inventory_Password);
		Click(Xpath, Inventory_Submit_Btn_Path);
	}

	public void ManageInventoryNavigation() {

		MouseHover(Xpath, LP_Admin_path);

		Click(Xpath, LP_ManageInventory_Path);

		// ExplicitWait_Element_Clickable(Xpath, HU_MI_Searchbtn_Path);
	}

	public void ClickHome() throws InterruptedException {

		if (Element_Is_Displayed(Xpath, HU_LP_Home_path)) {
			Click(Xpath, HU_LP_Home_path);
			Wait_ajax();
		}
	}

}
