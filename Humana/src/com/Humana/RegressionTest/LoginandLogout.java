package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.Humana.Base.BaseTestHumana;

public class LoginandLogout extends BaseTestHumana {

	@Test(priority = 1, enabled = true)
	public void HUM_TC_1_0_1_1() {

		// Verify Active user login

		Type(ID, HU_Login_UserName_ID, qaautoadmin);
		Type(ID, HU_Login_Password_ID, qaautoadmin);
		Click(ID, HU_Login_LoginBtn_ID);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Title_Path), "Project page is not displayed");
	}

	@Test(priority = 2, enabled = true)
	public void HUM_TC_2_6_2() {

		/*
		 * Validate "Logout" function and redirection back to the "Login page"* 
		 */

		Click(ID, HU_LP_Logout_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Containstextpath("h1", "Login")), "Login page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, HU_Login_UserName_ID), "Username is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(ID, HU_Login_Password_ID), "Password is not displayed");
		softAssert.assertAll();
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
