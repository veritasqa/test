package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class AddPopTicket extends BaseTestHumana {

	@Test(priority = 1, enabled = false)
	public void HUM_TC_2_3_1_1_3() throws InterruptedException, IOException {

		/*
		 * Verify Admin User is able to add and cancel ticket on Review Proofs tab
		 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "BH CASE MANAGEMENT");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept1_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Dept1");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept_Path).equalsIgnoreCase("02148"),
				"02148 is not displayed in Dept");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_LOB_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in Line of Business");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Lettertype_Path, "IPA Collaboration Letter");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Envelopetype_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Envelope Type");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Mailclass_Path).equalsIgnoreCase("First Class"),
				"First Class is not displayed in ");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Download_File.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_RecipientType_Path, "Member");
		Type(Xpath, HU_PHI_Pop_RecipientName_Path, "QA Test");
		Type(Xpath, HU_PHI_Pop_Address1_Path, Address);
		Thread.sleep(2000);
		MoveandClick(Xpath, HU_PHI_Pop_Address2_Path);
		Type(Xpath, HU_PHI_Pop_Address2_Path, "Address 2");
		Type(Xpath, HU_PHI_Pop_City_Path, City);
		Type(Xpath, HU_PHI_Pop_State_Path, State);
		MoveandClick(Xpath, HU_PHI_Pop_Zip_Path);
		Thread.sleep(2000);
		Type(Xpath, HU_PHI_Pop_Zip_Path, Zip);
		Click(Xpath, HU_PHI_Pop_AddAddress_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_GenerateProofsPath);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_ApproveProofPath),
				"Review proofs page is not displayed");
		Click(Xpath, HU_PHI_Pop_CancelTicPath);
		Wait_ajax();
	}

	@Test(priority = 2, enabled = false)
	public void HUM_TC_2_3_1_1_4() throws InterruptedException, IOException {

		/*
		 * Verify error messages on Enter Tickets Details tab when none of the dropdowns are selected
		 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Department is required!")),
				"Department is required! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Sub department is required!")),
				"Sub department is required! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Line Of Business is required!")),
				"Line Of Business is required! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Letter Type is required!")),
				"Letter Type is required! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Envelope Type is required!")),
				"Envelope Type is required! is not displayed");
		softAssert.assertAll();

	}

	@Test(priority = 3, enabled = false)
	public void HUM_TC_2_3_1_1_5() throws InterruptedException, IOException {

		/*
		 * Verify user is able to upload word document that gets converted to pdf
		 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "BH CASE MANAGEMENT");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept1_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Dept1");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept_Path).equalsIgnoreCase("02148"),
				"02148 is not displayed in Dept");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_LOB_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in Line of Business");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Lettertype_Path, "IPA Collaboration Letter");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Envelopetype_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Envelope Type");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Mailclass_Path).equalsIgnoreCase("First Class"),
				"First Class is not displayed in ");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Download_File.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_PHI_Pop_Changedoc_Path);
		Wait_ajax();
		Assert.assertTrue(
				Get_Attribute(Xpath, HU_PHI_Pop_Filenametxt_Path, "value").equalsIgnoreCase("Download_File.pdf"),
				"Download_File.pdf is not displayed");

	}

	@Test(priority = 4, enabled = true)
	public void HUM_TC_2_3_1_1_7() throws InterruptedException, IOException {

		/*
		 * Verify User is not able to upload password protected document
		 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "BH CASE MANAGEMENT");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept1_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Dept1");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept_Path).equalsIgnoreCase("02148"),
				"02148 is not displayed in Dept");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_LOB_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in Line of Business");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Lettertype_Path, "IPA Collaboration Letter");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Envelopetype_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Envelope Type");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Mailclass_Path).equalsIgnoreCase("First Class"),
				"First Class is not displayed in ");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\PASSWORD_TEST.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span",
				"Error uploading/processing print files: Could not resave word docoument for (PASSWORD_TEST_Original.docx)")),
				"Error uploading/processing print files: Could not resave word docoument for (PASSWORD_TEST_Original.docx) is not displayed");
	}

	@Test(priority = 5, enabled = true)
	public void HUM_TC_2_3_1_1_8and9and11and12and13and14and15and16() throws InterruptedException, IOException {

		/*
		 * Verify user is able to upload document when document is open
				 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "BH CASE MANAGEMENT");
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Download_File.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_PHI_Pop_Changedoc_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_EditticdetailsPath);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "HBH Quality and Accreditation");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Dept1_Path, "HBH");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Dept_Path, "2148");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_LOB_Path, "Commercial");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Lettertype_Path, "Appeals");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Envelopetype_Path, "Double Window");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Mailclass_Path, "First Class");
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Download_File.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_PHI_Pop_Changedoc_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Humana_Sample.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_RecipientType_Path),
				"Add address page is not displayed");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_RecipientType_Path, "Member");
		Type(Xpath, HU_PHI_Pop_RecipientName_Path, "QA Test");
		Type(Xpath, HU_PHI_Pop_Address1_Path, Address);
		Thread.sleep(2000);
		MoveandClick(Xpath, HU_PHI_Pop_Address2_Path);
		Type(Xpath, HU_PHI_Pop_Address2_Path, "Address 2");
		Type(Xpath, HU_PHI_Pop_City_Path, City);
		Type(Xpath, HU_PHI_Pop_State_Path, State);
		MoveandClick(Xpath, HU_PHI_Pop_Zip_Path);
		Thread.sleep(2000);
		Type(Xpath, HU_PHI_Pop_Zip_Path, Zip);
		Click(Xpath, HU_PHI_Pop_AddAddress_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientType1_Path).equalsIgnoreCase("Member Recipient Type"),
				"is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientName1_Path).equalsIgnoreCase("QA Test"),
				"is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Address11_Path).equalsIgnoreCase("913 Commerce Ct"),
				"Address 1 is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Address21_Path).equalsIgnoreCase("Address 2"),
				"Address 2 is not displayedis not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_City1_Path).equalsIgnoreCase("Buffalo Groove"),
				"City is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_State1_Path).equalsIgnoreCase("IL"), "State is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Zip1_Path).equalsIgnoreCase("60089"), "Zip is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientType1_Path).equalsIgnoreCase("Member"),
				"is not displayed");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_GenerateProofsPath);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_ApproveProofPath);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_ApprovedPath), "Approved button is not displayed");
		Click(Xpath, HU_PHI_Pop_Changedoc_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Humana_Sample_1.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_RecipientType_Path),
				"Add address page is not displayed");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_RecipientType_Path, "Member");
		Type(Xpath, HU_PHI_Pop_RecipientName_Path, "QA Test");
		Type(Xpath, HU_PHI_Pop_Address1_Path, Address);
		Thread.sleep(2000);
		MoveandClick(Xpath, HU_PHI_Pop_Address2_Path);
		Type(Xpath, HU_PHI_Pop_Address2_Path, "Address 2");
		Type(Xpath, HU_PHI_Pop_City_Path, City);
		Type(Xpath, HU_PHI_Pop_State_Path, State);
		MoveandClick(Xpath, HU_PHI_Pop_Zip_Path);
		Thread.sleep(2000);
		Type(Xpath, HU_PHI_Pop_Zip_Path, Zip);
		Click(Xpath, HU_PHI_Pop_AddAddress_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientType1_Path).equalsIgnoreCase("Member Recipient Type"),
				"is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientName1_Path).equalsIgnoreCase("QA Test"),
				"is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Address11_Path).equalsIgnoreCase("913 Commerce Ct"),
				"Address 1 is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Address21_Path).equalsIgnoreCase("Address 2"),
				"Address 2 is not displayedis not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_City1_Path).equalsIgnoreCase("Buffalo Groove"),
				"City is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_State1_Path).equalsIgnoreCase("IL"), "State is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Zip1_Path).equalsIgnoreCase("60089"), "Zip is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientType1_Path).equalsIgnoreCase("Member"),
				"is not displayed");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_GenerateProofsPath);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Editrecaddress_Path);
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_RecipientType_Path, "Facility");
		Type(Xpath, HU_PHI_Pop_RecipientName_Path, "QA Test");
		Type(Xpath, HU_PHI_Pop_Address1_Path, Address);
		Thread.sleep(2000);
		MoveandClick(Xpath, HU_PHI_Pop_Address2_Path);
		Type(Xpath, HU_PHI_Pop_Address2_Path, "Address 2");
		Type(Xpath, HU_PHI_Pop_City_Path, City);
		Type(Xpath, HU_PHI_Pop_State_Path, State);
		MoveandClick(Xpath, HU_PHI_Pop_Zip_Path);
		Thread.sleep(2000);
		Type(Xpath, HU_PHI_Pop_Zip_Path, Zip);
		Click(Xpath, HU_PHI_Pop_AddAddress_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientType1_Path).equalsIgnoreCase("Member Recipient Type"),
				"is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientName1_Path).equalsIgnoreCase("QA Test"),
				"is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Address11_Path).equalsIgnoreCase("913 Commerce Ct"),
				"Address 1 is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Address21_Path).equalsIgnoreCase("Address 2"),
				"Address 2 is not displayedis not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_City1_Path).equalsIgnoreCase("Buffalo Groove"),
				"City is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_State1_Path).equalsIgnoreCase("IL"), "State is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_Zip1_Path).equalsIgnoreCase("60089"), "Zip is not displayed");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Pop_RecipientType1_Path).equalsIgnoreCase("Member"),
				"is not displayed");
		softAssert.assertAll();

		Click(Xpath, HU_PHI_Pop_Changedoc_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Pettigrew_Bonnie.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Select_lidropdown(HU_PHI_Pop_AddRecipientType_Path, Xpath, "Member");
		Type(Xpath, HU_PHI_Pop_AddRecipientName_Path, "QA Test");
		Click(Xpath, HU_PHI_Pop_Plus1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_GenerateProofsPath);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_ApproveProofPath);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_ApprovedPath), "Approved button is not displayed");
		Click(Xpath, HU_PHI_Pop_SubmitTicket_Path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Status1_Path).equalsIgnoreCase("Submitted"),
				"(Submitted) is not displays in 'TicketStatus' field");

	}

	@Test(priority = 6, enabled = false)
	public void HUM_TC_2_3_1_1_19() throws InterruptedException, IOException {

		/*
		 * Verify new ticket # appears in 'PHI Orders' chart in new status
		 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "BH CASE MANAGEMENT");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept1_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Dept1");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept_Path).equalsIgnoreCase("02148"),
				"02148 is not displayed in Dept");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_LOB_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in Line of Business");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Lettertype_Path, "IPA Collaboration Letter");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Envelopetype_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Envelope Type");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Mailclass_Path).equalsIgnoreCase("First Class"),
				"First Class is not displayed in ");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_Selectletterfile_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Download_File.docx");
		Click(Xpath, HU_PHI_Pop_Uploadfile_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_RecipientType_Path, "Member");
		Type(Xpath, HU_PHI_Pop_RecipientName_Path, "QA Test");
		Type(Xpath, HU_PHI_Pop_Address1_Path, Address);
		Thread.sleep(2000);
		MoveandClick(Xpath, HU_PHI_Pop_Address2_Path);
		Type(Xpath, HU_PHI_Pop_Address2_Path, "Address 2");
		Type(Xpath, HU_PHI_Pop_City_Path, City);
		Type(Xpath, HU_PHI_Pop_State_Path, State);
		MoveandClick(Xpath, HU_PHI_Pop_Zip_Path);
		Thread.sleep(2000);
		Type(Xpath, HU_PHI_Pop_Zip_Path, Zip);
		Click(Xpath, HU_PHI_Pop_AddAddress_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		Click(Xpath, HU_PHI_Pop_GenerateProofsPath);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_ApproveProofPath),
				"Review proofs page is not displayed");
		Click(Xpath, HU_PHI_Pop_ApproveProofPath);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_ApprovedPath), "Approved button is not displayed");
		Click(Xpath, HU_PHI_Pop_SubmitTicket_Path);
		Wait_ajax();
		Click(Xpath, LP_PHIOrderPortal_path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Status1_Path).equalsIgnoreCase("New"),
				"(New) is not displays in 'TicketStatus' field");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}