package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class ManageUsers extends BaseTestHumana {

	public void Clickedituser(String Username) throws InterruptedException {

		Click(Xpath, Textpath("td", Username) + "//following::a[3]");
	}

	@Test(priority = 1, enabled = true)
	public void HUM_TS_2_5_3_1() throws InterruptedException {

		/*
		 * Validate Edit option opens on Search Result Row and allows you to edit the user
		 */

		Type(Xpath, HU_MU_UserName_Path, qaautoadmin);
		Click(Xpath, HU_MU_UserNamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Clickedituser(qaautoadmin);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_MU_Add_Address_Path, "913 Commerce Court-123");
		Click(Xpath, HU_MU_Edit_Updatebtn_Path);

		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Clickedituser(qaautoadmin);
		Assert.assertTrue(
				Get_Attribute(Xpath, HU_MU_Add_Address_Path, "value").equalsIgnoreCase("913 Commerce Court-123"),
				"'913 Commerce Court-123' is not displayed in 'Address' field");
		Type(Xpath, HU_MU_Add_Address_Path, "913 Commerce Court");
		Click(Xpath, HU_MU_Edit_Updatebtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);

	}

	@Test(priority = 2, enabled = true)
	public void HUM_TS_2_5_3_7() throws InterruptedException {

		/*
		* Validate error messages are displaying correctly
		 */
		Click(Xpath, HU_MU_Adduserbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_MU_Add_Insertbtn_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MU_error_Username_Path),
				"Username error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MU_error_PWD_Path),
				"Password error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MU_error_Firstname_Path),
				"First name error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MU_error_Lastname_Path),
				"Last name error message is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MU_error_Email_Path),
				"Email error message is not displayed");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		NavigateMenu(LP_Admin_path, LP_ManageUsers_path);
		ExplicitWait_Element_Clickable(Xpath, HU_MU_Adduserbtn_Path);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
