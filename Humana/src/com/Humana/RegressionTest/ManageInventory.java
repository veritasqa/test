package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class ManageInventory extends BaseTestHumana {

	@Test(priority = 1, enabled = false)
	public void HUM_TC_2_5_2_1_1() throws InterruptedException {

		/*
		 * Page opens with create new item, search items, and search results grids and displays appropriate fields
		 */

		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_New_CreateNewTypeTitle_Path),
				"'Create New Item' section on page is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_SearchItemsTitle_Path),
				"'Search Item' section on page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_New_CreateNewType_Path),
				"'Create New Type' drop-down is not displayd in 'Create New Item' section");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_New_CreateNewType_Path, "value").equalsIgnoreCase("- Any -"),
				"'Create New Type' drop-down has '- Any -' is not the default value");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_New_Add_Btn_Path),
				"'Add' button is not displayed near 'Create New Type' text field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_New_FormNoToCopy_Path),
				" 'Form Number to Copy' field is not displayed in 'Create New Item' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_New_Copy_Btn_Path),
				" 'Copy' button displays near 'Form Number To Copy' text field");
		Click(Xpath, HU_MI_New_CreateNewType_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				" 'Kit-On the Fly' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Prebuilt")),
				" 'Kit-Prebuilt' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD � Customizable")),
				" 'POD-Customizable' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD")),
				" 'POD' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premium Item")),
				" 'Premium Item' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				" 'Stock' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Print to Shelf")),
				" 'Print to Shelf' is not displayed 'Create New Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_FormNo_Path),
				" 'Form Number' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_Description_Path),
				" 'Description' field is not displayd in 'Search Items' section");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_InventoryType_Path),
				" 'inventory Type' drop-down is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in ' inventory Type' drop-down");
		Click(Xpath, HU_MI_SI_InventoryType_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - On the Fly")),
				" 'Kit-On the Fly' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit - Prebuilt")),
				" 'Kit-Prebuilt' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("POD � Customizable")),
				" 'POD-Customizable' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Premium Item")),
				" 'Premium Item' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Print to Shelf")),
				" 'Print to Shelf' is not displayed 'Inventory Type' drop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stock")),
				" 'Stock' is not displayed 'Inventory Type' drop-down");
		Click(Xpath, HU_MI_SI_InventoryType_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_TypeItem_Path),
				" 'Type Item' is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_TypeItem_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is default value is not displaying in 'Type Item' drop-down");
		Click(Xpath, HU_MI_SI_TypeItem_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Ad")),
				" 'Ad' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Annual Report")),
				" 'Annual Report' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Application")),
				" 'Application' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Article/Newsletter")),
				" 'Article/Newsletter' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Banners")),
				" 'Banners' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Brochure")),
				" 'Brochure' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Business Card")),
				" 'Business Card' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Buyers Guide")),
				" 'Buyers Guide' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("CD/DVD")),
				" 'CD/DVD' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Disclosure Form")),
				" 'Disclosure Form' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("E-Mail Correspondence")),
				" 'E-Mail Correspondence' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Envelope")),
				" 'Envelope' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Fact Sheet")),
				" 'Fact Sheet' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Flyer")),
				" 'Flyer' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Folder/Pocket Folder")),
				" 'Folder/Pocket Folder' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Form")),
				" 'Form' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Kit")),
				" 'Kit' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Label")),
				" 'Label' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Letter")),
				" 'Letter' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Mailers")),
				" 'Mailers' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Notecard")),
				" 'Notecard' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Notecard")),
				" 'Notecard' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Performance")),
				" 'Performance' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Postcard")),
				" 'Postcard' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Poster")),
				" 'Poster' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Powerpoint Presentation")),
				" 'Powerpoint Presentation' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Press Release")),
				" 'Press Release' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Prospectus")),
				" 'Prospectus' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Report")),
				" 'Report' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Sales Idea")),
				" 'Sales Idea' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Script Talking Points")),
				" 'Script Talking Points' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Semi-annual Report")),
				" 'Semi-annual Report' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("State Replacement Form")),
				" 'State Replacement Form' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Statements of Additional Information")),
				" 'Statements of Additional Information' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Video")),
				" 'Video' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Stationary/Letterhead")),
				" 'Stationary/Letterhead' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Summary Prospectus")),
				" 'Summary Prospectus' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Supplement")),
				" 'Supplement' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Tablet App")),
				" 'Tablet App' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Training Materials")),
				" 'Training Materials' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Web Page")),
				" 'Web Page' is not displayed in the 'Type Item' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Webinar")),
				" 'Webinar' is not displayed in the 'Type Item' drop-downdrop-down");
		Click(Xpath, HU_MI_SI_TypeItem_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_Active_Path),
				" 'Active' field is not displayed in 'Search Items' section ");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_Active_Path, "value").equalsIgnoreCase("Active"),
				"  'Active' as default value in Active' field");
		Click(Xpath, HU_MI_SI_Active_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Active")),
				" 'Active' is not displayed in the  'Active' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Inactive")),
				" 'Inactive' is not displayed in the  'Active' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Obsolete")),
				" 'Obsolete' is not displayed in the  'Active' drop-downdrop-down");
		Click(Xpath, HU_MI_SI_Active_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_UnitsOnHand_Path),
				" 'Units on Hand' field is not displayed in 'Search Items' section");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' is not displayed by default in 'Units On Hand' dropdown");
		Click(Xpath, HU_MI_SI_UnitsOnHand_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("All")),
				" 'ALL' is not displayed in the  'Units On Hand' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Low Stock")),
				" 'Low Stock' is not displayed in the  'Units On Hand' drop-downdrop-down");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, li_value("Out of Stock")),
				" 'Out of Stock' is not displayed in the  'Units On Hand' drop-downdrop-down");
		Click(Xpath, HU_MI_SI_UnitsOnHand_Icon_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Searchbtn_Path), " 'Search' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Clearbtn_Path), " 'Clear' button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_SI_SearchResults_Path),
				"'Search Results' section on page is not displayed");
		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = false) // comments
	public void HUM_TC_2_5_2_1_4() throws InterruptedException {

		/*
		 * Ensure all the Search items fields are working and appropriate search results appears
		 */

		Type(Xpath, HU_MI_SI_FormNo_Path, qa_TestNewItem_X);
		Click(Xpath, HU_MI_SI_Active_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, HU_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Searchresults(qa_TestNewItem_X)),
				qa_TestNewItem_X + " is not displayed in the 'Search Results' section");
		Click(Xpath, HU_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_FormNo_Path, "value").isEmpty(),
				"Form No field is not blank after cleared");

		Type(Xpath, HU_MI_SI_Description_Path, "qa_TestNewItem_Short");
		Click(Xpath, HU_MI_SI_Active_Icon_Path);
		Thread.sleep(1000);
		Click(Xpath, li_value("- Any -"));
		Wait_ajax();
		Click(Xpath, HU_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Searchresults(qa_TestNewItem_X)),
				qa_TestNewItem_X + " is not displayed in the 'Search Results' section for Description search");
		Click(Xpath, HU_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, HU_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_Description_Path, "value").isEmpty(),
				"Description field is not blank after cleared");

		Click(Xpath, HU_MI_SI_InventoryType_Icon_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("POD"));
		Wait_ajax();
		Click(Xpath, HU_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Searchresults(qa_TestNewItem_X)),
				qa_TestNewItem_X + " is not displayed in the 'Search Results' section for Inventory Type search");
		Click(Xpath, HU_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, HU_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Inventory Type' drop-down after cleared");

		Click(Xpath, HU_MI_SI_TypeItem_Icon_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Ad"));
		Wait_ajax();
		Click(Xpath, HU_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Searchresults(qa_TestNewItem_X)),
				qa_TestNewItem_X + " is not displayed in the 'Search Results' section for Type Item search");
		Click(Xpath, HU_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, HU_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_TypeItem_Path, "value").equalsIgnoreCase("- Any -"),
				" '- Any -' is not displayed in 'Type Item' drop-down after cleared");

		Click(Xpath, HU_MI_SI_Active_Icon_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Active"));
		Wait_ajax();
		Click(Xpath, HU_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_MI_Status1_Path).trim().equals("A"),
				"A is not displayed under status column");
		Click(Xpath, HU_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, HU_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_TypeItem_Path, "value").equalsIgnoreCase("Active"),
				" 'Active' is not displayed in 'Status' drop-down after cleared");

		Click(Xpath, HU_MI_SI_UnitsOnHand_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("All"));
		Wait_ajax();
		Click(Xpath, HU_MI_Searchbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, HU_MI_Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Searchresults(qa_TestNewItem_X)),
				qa_TestNewItem_X + " is not displayed in the 'Search Results' section for Units on head search");
		Click(Xpath, HU_MI_Clearbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, HU_MI_Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_UnitsOnHand_Path, "value").equalsIgnoreCase("- Any -"),
				" 'Any' displays in 'Units On Hand' field");

	}

	@Test(priority = 3, enabled = true) // comments
	public void HUM_TC_2_5_2_3_1AndHUM_TC_2_5_2_3_2() throws InterruptedException {

		/*
		 * Validate all fields in general tab appear and almost all are blank
		 */

		Click(Xpath, HU_MI_New_CreateNewType_Path);
		Thread.sleep(500);
		Click(Xpath, li_value("Stock"));
		Click(Xpath, HU_MI_New_Add_Btn_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Gen_General_Path), "'General' tab is not displayed");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_MI_Gen_NewItem_headerpart_Path),
				"'Manage Inventory Item: New Item' is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_Gen_Formno_Path, "value").isEmpty(),
				"Form No field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_Gen_txtDescription_Path, "value").isEmpty(),
				"Description field is not blank");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_SI_InventoryType_Path, "value").equalsIgnoreCase("Stock"),
				" 'Stock' is not displayed in 'Inventory Type' drop-down ");
		softAssert.assertTrue(
				Get_Attribute(Xpath, HU_MI_SI_TypeItem_Path, "value").equalsIgnoreCase("- Please Select -"),
				" '- Please Select -' is not displayed in 'Item Type' drop-down ");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_MI_Gen_txtNotes_Path, "value").isEmpty(),
				"Notes field is not blank");
		softAssert.assertAll();

		/*
		 * Validate that clicking save without filling out any fields provides an error message for all tabs
		 * HUM_TC_2_5_2_3_12
		 */
		Click(Xpath, HU_MI_Gen_Save_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Invalid IMS #!")),
				"Invalid IMS #! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Short Description is required!")),
				"Short Description is required! is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Item Type is required!")),
				"Item Type is required! is not displayed");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		softAssert = new SoftAssert();
		ManageInventoryNavigation();
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
