package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class PHIOrderPortal extends BaseTestHumana {

	public static String FilterTicketno, JobType, Dept1, MailClass, LetterType, EnvelopeType, LOB, PHIUserName,
			MailDate, TicketStatus, Filename;

	@Test(priority = 1, enabled = false)
	public void HUM_TC_2_3_1_1() throws InterruptedException {

		Click(Xpath, LP_PHIOrderPortal_path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Title_Path), "PHI Orders page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_ViewPHIOrders_Path),
				"View PHI Orders section is not displayed");
		softAssert.assertFalse(Element_Is_selected(Xpath, HU_PHI_Closed1_Path),
				"(Active tickets) is not appear at top");
		softAssert.assertAll();
	}

	@Test(priority = 2, enabled = false)
	public void HUM_TC_2_3_1_2() throws InterruptedException {

		// Validate paging and page size functionality

		Click(Xpath, LP_PHIOrderPortal_path);
		Wait_ajax();
		Click(Xpath, Textpath("span", "2"));
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_PHI_Pagesize_Path, "1");
		Click(Xpath, HU_PHI_Change_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")), "Page 1 is not highlighted");
		Type(Xpath, HU_PHI_Page_Path, "2");
		Click(Xpath, HU_PHI_Go_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("2")), "Page 2 is not highlighted");
		Click(Xpath, HU_PHI_Nextpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("3")), "Page 3 is not highlighted");
		Click(Xpath, HU_PHI_Lastpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		String MA_lastpageno = Get_Text(Xpath, HU_PHI_Currentpage_Path).trim();
		Assert.assertEquals(MA_lastpageno,
				Get_Text(Xpath, HU_PHI_Pageof_Path).substring(Get_Text(Xpath, HU_PHI_Pageof_Path).indexOf(" ") + 1,
						Get_Text(Xpath, HU_PHI_Pageof_Path).length()),
				"Last page is not highlighted");
		Click(Xpath, HU_PHI_Previouspage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertNotEquals(Get_Text(Xpath, HU_PHI_Currentpage_Path).trim(), MA_lastpageno,
				"(second last page) is not highlighted");
		Click(Xpath, HU_PHI_Firstpage_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Pagination("1")),
				"Page 1 is not highlighted - 1st page button navigation");

	}

	@Test(priority = 3, enabled = false)
	public void HUM_TC_2_3_1_3() throws InterruptedException {

		/*
		 * Validate Filter functionality works for all the column heading
		 */

		Click(Xpath, LP_PHIOrderPortal_path);
		Wait_ajax();
		FilterTicketno = Get_Text(Xpath, HU_PHI_Ticketno1_Path).trim();
		JobType = Get_Text(Xpath, HU_PHI_Jobtype1_Path).trim();
		Dept1 = Get_Text(Xpath, HU_PHI_Dept_Path1).trim();
		MailClass = Get_Text(Xpath, HU_PHI_Mailclass1_Path).trim();
		LetterType = Get_Text(Xpath, HU_PHI_Lettertype1_Path).trim();
		EnvelopeType = Get_Text(Xpath, HU_PHI_Envelope1_Path).trim();
		LOB = Get_Text(Xpath, HU_PHI_LOB1_Path).trim();
		PHIUserName = Get_Text(Xpath, HU_PHI_Username1_Path).trim();
		MailDate = Get_Text(Xpath, HU_PHI_Maildate1_Path).trim();
		TicketStatus = Get_Text(Xpath, HU_PHI_Status1_Path).trim();
		Filename = Get_Text(Xpath, HU_PHI_Filename1_Path).trim();

		Type(Xpath, HU_PHI_Ticketno_Path, FilterTicketno);
		Click(Xpath, HU_PHI_Jobtypefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Ticketno1_Path).equalsIgnoreCase(FilterTicketno),
				"(Ticket #) is not displays in 'Ticket #' field");
		Clear(Xpath, HU_PHI_Ticketno_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - Ticket no filter");

		Type(Xpath, HU_PHI_Jobtype_Path, JobType);
		Click(Xpath, HU_PHI_Jobtypefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Jobtype1_Path).equalsIgnoreCase(JobType),
				"(Jpb type) is not displays in 'Job type' field");
		Selectfilter(HU_PHI_Jobtypefilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - Job type filter");
		Type(Xpath, HU_PHI_Dept_Path, JobType);
		Click(Xpath, HU_PHI_Deptfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Dept_Path1).equalsIgnoreCase(Dept1),
				"(Dept1 ) is not displays in 'Dept 1' field");
		Selectfilter(HU_PHI_Deptfilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - Dept1 filter");

		Type(Xpath, HU_PHI_Mailclass_Path, MailClass);
		Click(Xpath, HU_PHI_Mailclassfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Mailclass1_Path).equalsIgnoreCase(MailClass),
				"(MailClass ) is not displays in 'MailClass' field");
		Selectfilter(HU_PHI_Mailclassfilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - MailClass filter");

		Type(Xpath, HU_PHI_Lettertype_Path, LetterType);
		Click(Xpath, HU_PHI_Lettertypefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Lettertype1_Path).equalsIgnoreCase(LetterType),
				"(LetterType ) is not displays in 'LetterType' field");
		Selectfilter(HU_PHI_Lettertypefilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - LetterType filter");

		Type(Xpath, HU_PHI_Envelope_Path, EnvelopeType);
		Click(Xpath, HU_PHI_Envelopefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Envelope1_Path).equalsIgnoreCase(EnvelopeType),
				"(EnvelopeType ) is not displays in 'EnvelopeType' field");
		Selectfilter(HU_PHI_Envelopefilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - EnvelopeType filter");

		Type(Xpath, HU_PHI_LOB_Path, LOB);
		Click(Xpath, HU_PHI_LOBfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_LOB1_Path).equalsIgnoreCase(LOB),
				"(Line of Business ) is not displays in 'Line of Business' field");
		Selectfilter(HU_PHI_LOBfilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - Line of Business filter");

		Type(Xpath, HU_PHI_Username_Path, PHIUserName);
		Click(Xpath, HU_PHI_Usernamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Username1_Path).equalsIgnoreCase(PHIUserName),
				"(User name ) is not displays in 'User name' field");
		Selectfilter(HU_PHI_Usernamefilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - User Name filter");

		Type(Xpath, HU_PHI_Maildate_Path, MailDate);
		Click(Xpath, HU_PHI_Maildatefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Maildate1_Path).equalsIgnoreCase(MailDate),
				"(MailDate) is not displays in 'MailDate' field");
		Selectfilter(HU_PHI_Maildatefilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in gjavascript:__doPostBack('ctl00$ctl00$cphContent$cphSection$rgrdSpecialProjects$ctl00$ctl02$ctl01$ctl10','')rid - MailDate filter");

		Type(Xpath, HU_PHI_Status_Path, TicketStatus);
		Click(Xpath, HU_PHI_Statusfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Status1_Path).equalsIgnoreCase(TicketStatus),
				"(TicketStatus) is not displays in 'TicketStatus' field");
		Selectfilter(HU_PHI_Statusfilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - TicketStatus filter");

		Type(Xpath, HU_PHI_Filename_Path, Filename);
		Click(Xpath, HU_PHI_Filenamefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_PHI_Filename1_Path).equalsIgnoreCase(Filename),
				"(Filename) is not displays in 'Filename' field");
		Selectfilter(HU_PHI_Filenamefilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - Filename filter");

		Click(Xpath, HU_PHI_Closed_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_selected(Xpath, HU_PHI_Closed1_Path),
				"(Closed) is not displays in 'Closed' field");
		Selectfilter(HU_PHI_Closedfilter_Path, "NoFilter");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path),
				"(All Ticket #) appears in grid - Closed filter");

	}

	@Test(priority = 4, enabled = true)
	public void HUM_TC_2_3_1_4() throws InterruptedException {

		/*
		 * Validate combination filter functionality works fine
		 */

		Click(Xpath, LP_PHIOrderPortal_path);
		Wait_ajax();
		Type(Xpath, HU_PHI_Mailclass_Path, "First Class");
		Click(Xpath, HU_PHI_Mailclassfilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Selectfilter(HU_PHI_Mailclassfilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_PHI_Envelope_Path, "Medicare");
		Click(Xpath, HU_PHI_Envelopefilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Selectfilter(HU_PHI_Envelopefilter_Path, "Contains");
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Mailclass1_Path).equalsIgnoreCase("First Class"),
				"(First Class MailClass ) is not displays in 'MailClass' field");
		softAssert.assertTrue(Get_Text(Xpath, HU_PHI_Envelope1_Path).contains("Medicare"),
				"( Medicare EnvelopeType ) is not displays in 'EnvelopeType' field");
		softAssert.assertAll();
		Selectfilter(HU_PHI_Mailclassfilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Selectfilter(HU_PHI_Envelopefilter_Path, "NoFilter");
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Row2_Path), "(All Ticket #) appears in grid");
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
