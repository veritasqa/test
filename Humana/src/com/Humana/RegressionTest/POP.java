package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class POP extends BaseTestHumana {

	@Test(priority = 1, enabled = false)
	public void HUM_TC_2_0_3() throws InterruptedException, IOException {

		/*
		 * Verify menu for Pop user on landing page
		 * */
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Specialproject_path), "Special Project is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_LP_Home_path), " is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Support_path), " is not displayed");
		MouseHover(Xpath, LP_PHIOrderPortal_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_AddPOPTicket_path),
				" \"PHI Order Portal\" dropdown appears");
		// Click(Xpath, HU_LP_Logout_Path);

	}

	@Test(priority = 2, enabled = false)
	public void HUM_TC_2_3_1_1_1() throws InterruptedException, IOException {

		/*
		 * Verify Pop User is able to add and cancel cancel ticket on Upload Document tab
		 * */

		MouseHover(Xpath, LP_PHIOrderPortal_path);
		Click(Xpath, LP_AddPOPTicket_path);
		Wait_ajax();
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Jobtype_Path, "BH CASE MANAGEMENT");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept1_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Dept1");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Dept_Path).equalsIgnoreCase("02148"),
				"02148 is not displayed in Dept");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_LOB_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in Line of Business");
		Select_DropDown_VisibleText(Xpath, HU_PHI_Pop_Lettertype_Path, "IPA Collaboration Letter");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Envelopetype_Path).equalsIgnoreCase("HBH"),
				"HBH is not displayed in Envelope Type");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_PHI_Pop_Mailclass_Path).equalsIgnoreCase("First Class"),
				"First Class is not displayed in ");
		softAssert.assertAll();
		Click(Xpath, HU_PHI_Pop_Uploadfilenext_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_PHI_Pop_Selectletterfile_Path),
				"Upload page is not displayed");
		Click(Xpath, HU_PHI_Pop_CancelTicPath);
		Wait_ajax();

	}

	@Test(priority = 3, enabled = false)
	public void HUM_TC_2_3_1_5() throws InterruptedException, IOException {

		/*
		 * Verify Pop User can see their own orders in PHI Order Portal
		
		 * */

		Assert.assertTrue(false, "Own tickets are not appeared in the grid");
	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautopop, qaautopop);
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
