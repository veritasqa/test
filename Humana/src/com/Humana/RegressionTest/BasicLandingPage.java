package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class BasicLandingPage extends BaseTestHumana {

	@Test(priority = 1, enabled = true)
	public void HUM_TC_2_0_5() throws InterruptedException {

		/*
		 * Verify menu for basic User on landing page
		 * */

		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Title_Path), "Project page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_LP_Home_path), "Home is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Support_path), "Home is not displayed");
		MouseHover(Xpath, LP_Support_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Humanafaq_path), "Humana FAQ is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Specialproject_path), "Special Project is not displayed");
		MouseHover(Xpath, LP_Specialproject_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_AddSpecialproject_path),
				"Add Special Project is not displayed");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautobasic, qaautobasic);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
