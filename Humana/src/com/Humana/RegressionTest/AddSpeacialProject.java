package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class AddSpeacialProject extends BaseTestHumana {

	@Test(priority = 1, enabled = true)
	public void HUM_TC_2_4_1_1_1() throws InterruptedException {

		/*
		 * Validate upon clicking "Add Special Projects" takes to General Special projects
		 * */

		MouseHover(Xpath, LP_Specialproject_path);
		Click(Xpath, LP_AddSpecialproject_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on 'Projects' page");
	}

	@Test(priority = 2, enabled = true)
	public void HUM_TC_2_4_1_1_2() throws InterruptedException {

		/*
		 * Create a special project without entering data and Verify the error messages
		 * */

		MouseHover(Xpath, LP_Specialproject_path);
		Click(Xpath, LP_AddSpecialproject_path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on 'Projects' page");

		Assert.assertTrue(Get_Text(Xpath, HU_SP_Gen_TicketNo_Path).equalsIgnoreCase("New"),
				"New is not displayed in the Ticket Number");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Sharepoint_Path),
				"Sharepoint# field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_SharepointReq_Path),
				"Sharepoint * is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Gen_Sharepoint_Path, "value").isEmpty(),
				"\" \" is not displayed in the sharepoint");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_JobTitle_Path), "Jobtitle field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_JobTitleReq_Path), "Jobtitle * is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Gen_JobTitle_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_SP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"- Select - is not displayed in the Job type");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_SharepointReq_Path), "Jobtype * is not displayed");
		softAssert.assertAll();
		Click(Xpath, HU_SP_Gen_Create_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "You must enter a value in the following fields:")),
				"You must enter a value in the following fields: is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Sharepoint # is required")),
				"Sharepoint # is required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Job Title is required")),
				"Job Title is required is required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Job Type is required")),
				"Job Type is required is required is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Sharepoint # is required")),
				"Sharepoint # is required is not displayed by Sharepoint #");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Job Title is required")),
				"Job Title is required is required is not displayed by Job title");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Job Type is required")),
				"Job Type is required is required is not displayed by Job Type");
		softAssert.assertAll();

		Select_DropDown_VisibleText(Xpath, HU_SP_Gen_JobType_Path, "Print and Mail Request");
		Click(Xpath, HU_SP_Gen_Create_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "You must enter a value in the following fields:")),
				"You must enter a value in the following fields: is not displayed - selecting only Job type ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Sharepoint # is required")),
				"Sharepoint # is required is not displayed - selecting only Job type ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Job Title is required")),
				"Job Title is required is required is not displayed - selecting only Job type ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Sharepoint # is required")),
				"Sharepoint # is required is not displayed by Sharepoint # - selecting only Job type ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Job Title is required")),
				"Job Title is required is required is not displayed by Job title - selecting only Job type ");
		softAssert.assertAll();
		Type(Xpath, HU_SP_Gen_JobTitle_Path, "QA Test");
		Click(Xpath, HU_SP_Gen_Create_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("div", "You must enter a value in the following fields:")),
				"You must enter a value in the following fields: is not displayed - After typed Job title");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("li", "Sharepoint # is required")),
				"Sharepoint # is required is not displayed - After typed Job title ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Sharepoint # is required")),
				"Sharepoint # is required is not displayed by Sharepoint #  - After typed Job title");
		softAssert.assertAll();
		Type(Xpath, HU_SP_Gen_Sharepoint_Path, "123");
		Click(Xpath, HU_SP_ProgramTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking Program Tab");
		Click(Xpath, HU_SP_CostsTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking Costs Tab");
		Click(Xpath, HU_SP_InventoryTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking Inventory Tab");
		Click(Xpath, HU_SP_HistoryTab_Path);
		softAssert.assertTrue(
				Element_Is_Displayed(Xpath, Textpath("label", "Create the Special Project, before proceeding.")),
				"Create the Special Project, before proceeding.is not displayed - Clicking History Tab");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
