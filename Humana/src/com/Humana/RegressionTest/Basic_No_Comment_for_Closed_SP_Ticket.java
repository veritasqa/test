package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class Basic_No_Comment_for_Closed_SP_Ticket extends BaseTestHumana {

	@Test(priority = 1, enabled = true)
	public void HUM_TC_2_4_1_23() throws InterruptedException, IOException {

		/*
		 * Verify "Insert Comment" button is disabled for Humana basic user once the ticket is closed by Admin
		 * */

		Click(Xpath, HU_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on 'Projects' page");
		Assert.assertTrue(Get_Text(Xpath, HU_SP_Gen_TicketNo_Path).equalsIgnoreCase("New"),
				"New is not displayed in the Ticket Number");
		Type(Xpath, HU_SP_Gen_Sharepoint_Path, "1");
		Type(Xpath, HU_SP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, HU_SP_Gen_JobType_Path, "Print and Mail Request");
		Select_DropDown_VisibleText(Xpath, HU_SP_Gen_Frequency_Path, "Daily");
		Type(Xpath, HU_SP_Gen_Printspecs_Path, "Humana Test");
		Type(Xpath, HU_SP_Gen_Assemblyinst_Path, "Humana");
		Type(Xpath, HU_SP_Gen_Duedate_Path, Get_Futuredate("MM/dd/YYYY"));
		Type(Xpath, HU_SP_Gen_Filedue_Path, Get_Futuredate("MM/dd/YYYY"));
		Type(Xpath, HU_SP_Gen_Recipientcount_Path, "1");
		Click(Xpath, HU_SP_Gen_IsQuoteneeded_Path);
		Click(Xpath, HU_SP_Gen_Create_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed");
		SplprjTicketNumber = Get_Text(Xpath, HU_SP_Gen_TicketNo_Path);
		Reporter.log("Created Special project no is " + SplprjTicketNumber);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		login(qaautoadmin, qaautoadmin);
		// Click(Xpath, HU_LP_Logout_Path);
		// Type(ID, HU_Login_UserName_ID, qaautoadmin);
		// Type(ID, HU_Login_Password_ID, qaautoadmin);
		// Click(ID, HU_Login_LoginBtn_ID);

		Type(Xpath, HU_SP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, HU_SP_TicketNofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_SP_ViewEdit1_Path);
		Switch_New_Tab();
		Click(Xpath, HU_SP_ProgramTab_Path);
		Click(Xpath, HU_SP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_Prog_AddComment_Path, "QA Test");
		Select_lidropdown(HU_SP_Prog_AddStatusarrow_Path, Xpath, "Closed");
		Wait_ajax();
		Click(Xpath, HU_SP_Prog_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_SP_Gen_Save_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed");
		Assert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_Status_Path).trim().equalsIgnoreCase("Closed"),
				"Closed is not displayed in the Status");
		Closealltabs();
		login(qaautobasic, qaautobasic);
		Click(Xpath, Textpath("a", "Closed Tickets"));
		ExplicitWait_Element_Visible(Xpath, Containspath("input", "value", "Closed"));
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, HU_SP_TicketNofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_SP_ViewEdit1_Path);
		Switch_New_Tab();
		Click(Xpath, HU_SP_ProgramTab_Path);
		Click(Xpath, HU_SP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_Prog_AddComment_Path, "Test Comment");
		Assert.assertFalse(Element_Is_Enabled(Xpath, HU_SP_Prog_InsertCommentbtn_Path), "Insert button is clickable");

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautobasic, qaautobasic);
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
