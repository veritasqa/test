package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class SpecialProjects extends BaseTestHumana {

	@Test(priority = 1, enabled = true)
	public void HUM_TC_2_4_1_3() throws InterruptedException {

		/*
		 * Verify "Add Ticket", "View/Edit", "Refresh" button, 	"Closed Tickets", "My Tickets", "Assigned Tickets", "Unassigned Tickets", 
		 * "Quote Needed" and "Recent Updates" displays on the Special Projects table.
		 * */

		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Addticketicon_Path),
				"\"Add Ticket\" button is not displays on bottom left of 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Refreshicon_Path),
				"\"Refresh\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Addticketicon_Path),
				"\"\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "View/Edit")),
				"\"View/Edit\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Copy")),
				"\"Copy\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Closed Tickets")),
				"\"Closed Tickets\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "My Tickets")),
				"\"My Tickets\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Assigned Tickets")),
				"\"Assigned Tickets\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Unassigned Tickets")),
				"\"Unassigned Tickets\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Quote Needed")),
				"\"Quote Needed\" button is not displaysin the 'Projects' page");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", "Recent Updates")),
				"\"Recent Updates\" button is not displaysin the 'Projects' page");
		softAssert.assertAll();

	}

	@Test(priority = 2, enabled = true)
	public void HUM_TC_2_4_1_4() throws InterruptedException {

		/*
		 * Validate "Add ticket button",Edit/View button takes to "General" Special Projects page 
		 * */

		Click(Xpath, HU_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on 'Projects' page");
		Click(Xpath, HU_SP_Gen_Cancel_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Title_Path), "Project page is not displayed");
		Click(Xpath, HU_SP_ViewEdit1_Path);
		Switch_New_Tab();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on 'Projects' page");
		Switch_Old_Tab();
		Closealltabs();

	}

	@Test(priority = 3, enabled = true)
	public void HUM_TC_2_4_1_5() throws InterruptedException {

		/*
		 * Add a new Special Projects and verify new ticket appears on Special Projects: view tickets table
		 * */

		Click(Xpath, HU_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on 'Projects' page");
		Assert.assertTrue(Get_Text(Xpath, HU_SP_Gen_TicketNo_Path).equalsIgnoreCase("New"),
				"New is not displayed in the Ticket Number");
		Type(Xpath, HU_SP_Gen_Sharepoint_Path, "1");
		Type(Xpath, HU_SP_Gen_JobTitle_Path, "QA Test");
		Select_DropDown_VisibleText(Xpath, HU_SP_Gen_JobType_Path, "Print and Mail Request");
		Type(Xpath, HU_SP_Gen_Duedate_Path, Get_Futuredate("MM/dd/YYYY"));
		Click(Xpath, HU_SP_Gen_Create_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed");
		SplprjTicketNumber = Get_Text(Xpath, HU_SP_Gen_TicketNo_Path);
		Reporter.log("Created Special project no is " + SplprjTicketNumber);
		SplprjTicketNumberlist.add(SplprjTicketNumber);
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
		Assert.assertTrue(Get_Text(Xpath, HU_SP_TicketNo1_Path).trim().equalsIgnoreCase(SplprjTicketNumber),
				"Created Special Project no is not displayed first row");
	}

	@Test(priority = 4, enabled = true)
	public void HUM_TC_2_4_1_6() throws InterruptedException, IOException {

		/*
		 * Validate all the Fields/buttons of "General", Progress/Files, Costs are enabled in Special Projects is working fine
		 * */

		Click(Xpath, HU_SP_Addticketicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Get_Text(Xpath, HU_SP_Gen_TicketNo_Path).equalsIgnoreCase("New"),
				"New is not displayed in the Ticket Number");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Sharepoint_Path),
				"Sharepoint# field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_SharepointReq_Path),
				"Sharepoint * is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Gen_Sharepoint_Path, "value").isEmpty(),
				"\" \" is not displayed in the sharepoint");
		Type(Xpath, HU_SP_Gen_Sharepoint_Path, "1");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_JobTitle_Path), "Jobtitle field is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_JobTitleReq_Path), "Jobtitle * is not displayed");
		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Gen_JobTitle_Path, "value").isEmpty(),
				"\" \" is not displayed in the Jobtitle");
		Type(Xpath, HU_SP_Gen_JobTitle_Path, "QA Test 123");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_SP_Gen_JobType_Path).equalsIgnoreCase("- Select -"),
				"- Select - is not displayed in the Job type");
		Select_DropDown_VisibleText(Xpath, HU_SP_Gen_JobType_Path, "Print and Mail Request");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_SharepointReq_Path), "Jobtype * is not displayed");
		softAssert.assertTrue(Get_DropDown(Xpath, HU_SP_Gen_Frequency_Path).equalsIgnoreCase("One Time"),
				"One Time is not displayed in the Frequency dd");
		Type(Xpath, HU_SP_Gen_Printspecs_Path, "QA is Testing");
		Type(Xpath, HU_SP_Gen_Assemblyinst_Path, "QA is Testing");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_DuedateCal_Path),
				"Due date calender is not displayed");
		Click(Xpath, HU_SP_Gen_DuedateCal_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_DuedateReq_Path), "Duedate * is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_FiledueCal_Path),
				"File due date calender is not displayed");
		Datepicker2(Xpath, HU_SP_Gen_FiledueCal_Path, Xpath, HU_SP_Gen_FiledueCalMonth_Path, Get_Futuredate("MMM"),
				Get_Futuredate("d"), Get_Futuredate("YYYY"));
		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Gen_Recipientcount_Path, "value").isEmpty(),
				"\" \" is not displayed in the Recipient count");
		Type(Xpath, HU_SP_Gen_Recipientcount_Path, "25");
		softAssert.assertFalse(Element_Is_selected(Xpath, HU_SP_Gen_IsQuoteneeded_Path),
				"Is Quote Needed is not unchecked");
		Click(Xpath, HU_SP_Gen_IsQuoteneeded_Path);
		softAssert.assertTrue(
				Get_Text(Xpath, HU_SP_Gen_Createdby_Path).trim().equalsIgnoreCase(UserFName + " " + UserLName),
				"User names is not displayed in the Created by");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_Job_Path).trim().equalsIgnoreCase("New"),
				"New is not displayed in the Job");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_Status_Path).trim().equalsIgnoreCase("New"),
				"New is not displayed in the Status");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_SP_Path).trim().equalsIgnoreCase("New"),
				"New is not displayed in the SP");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Cancel_Path), "Cancel button is not displayed");
		softAssert.assertAll();
		Click(Xpath, HU_SP_Gen_Create_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Data has been saved.")),
				"Data has been saved. is not displayed");
		SplprjTicketNumber = Get_Text(Xpath, HU_SP_Gen_TicketNo_Path);
		Reporter.log("Created Special project no is " + SplprjTicketNumber);
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_Job_Path).trim().equalsIgnoreCase("QA Test 123"),
				"QA Test 123 is not displayed in the Job");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_Status_Path).trim().equalsIgnoreCase("New"),
				"New is not displayed in the Status");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_SP_Path).trim().equalsIgnoreCase("1"),
				"1 is not displayed in the SP");
		softAssert.assertAll();
		Assert.assertTrue(Element_Is_Displayed(Xpath, TextpathContains("div", "Message Definitions")),
				"Message Definitions is not displayed");

		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Addnewicon_Path),
				" \"Add new\" button is not displays by 'Message Definitions' field");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Addnew_Path), "Add new is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Pencil_Path), "Pencil icon is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Editselected_Path),
				"Edit selected is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Deleteicon_Path), "Delete icon is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Deleteitem_Path), "Delete item is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Refreshicon_Path), "Refresh icon is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Refresh_Path), "Refresh is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Code_Path), "Code column is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Description_Path),
				"Description column is not displayed");
		Click(Xpath, HU_SP_Gen_Addnewicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Save new entry")),
				"Save new entry is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Cancel editing")),
				"Cancel editing is not displayed");
		Type(Xpath, HU_SP_Gen_Codetext_Path, "QA Test");
		Type(Xpath, HU_SP_Gen_Descriptiontext_Path, "QA is Testing");
		Click(Xpath, HU_SP_Gen_Tickicon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Record updated")),
				"Record updated is not displayed");
		Click(Xpath, HU_SP_Gen_Pencilicon1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_Gen_Codeedittext_Path, "QA is Testing");
		Click(Xpath, HU_SP_Gen_Tickicon1_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Record updated")),
				"Record updated is not displayed - Edit");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Teammemberlist("qaautoadmin")),
				"qaautoadmin is not displayed in the team member list");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_usersTop_Path),
				"To top button is not displayed in in the Humana users");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_usersDown_Path),
				"To  button is not displayed in in the Humana users");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_usersAlltop_Path),
				"To  button is not displayed in in the Humana users");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_usersAlldown_Path),
				"To  button is not displayed in in the Humana users");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Downloadarch_Path),
				"Download Archieve is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Cancel_Path), "Cancel is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_Save_Path), "Save button is not displayed");
		softAssert.assertAll();

		Click(Xpath, HU_SP_ProgramTab_Path);
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Created_Path).equalsIgnoreCase(Get_Todaydate("M/d/YYYY")),
				"Today date is not displayed in the Craeted field");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Quoteaccepeted_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in the Quote accepted");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Proofcreated_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in the Proof created");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Proofapproved_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in the Proof Approved");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Completed_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in the Completed");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Cancelled_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in the Cancelled");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Holdplaced_Path).equalsIgnoreCase("N/A"),
				"N/A is not displayed in the Hold placed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Prog_ToggleArchivedComments_Path),
				"Toggle Archived Comments button is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Prog_Addcommenticon_Path),
				"Add comment is not displayed");
		Click(Xpath, HU_SP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_Prog_AddTopic_Path, "Test");
		Type(Xpath, HU_SP_Prog_AddComment_Path, "QA Testing");
		Select_lidropdown(HU_SP_Prog_AddStatusarrow_Path, Xpath, "Acknowledged");
		Click(Xpath, HU_SP_Prog_Addattachment_Path);
		Thread.sleep(4000);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\HUM_TC_2_4_1_6.exe" + " "
				+ System.getProperty("user.dir") + "\\src\\com\\Humana\\Utils\\Humana_Excel_File_1.xlsx");
		Wait_ajax();
		ExplicitWait_Element_Clickable(Xpath, Containspath("input", "value", "Remove"));
		Click(Xpath, HU_SP_Prog_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);

		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Prog_Green1_Path, "src").contains("green"),
				"'Green' checkbox button is not displays on the left side of the grid");
		Click(Xpath, HU_SP_Prog_Green1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Get_Attribute(Xpath, HU_SP_Prog_Red1_Path, "src").contains("red"),
				"'Red' checkbox button is not displays on the left side of the grid");

		softAssert.assertTrue(GetCSS_Backgroundcolor(Xpath, HU_SP_Prog_Attachmentrow1_Path).equalsIgnoreCase("#828282"),
				"The whole row is not greys out");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_UserName1_Path).trim().equalsIgnoreCase(qaautoadmin),
				"<User's login> is not displays under 'User Name'");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Topic1_Path).trim().equalsIgnoreCase("Test"),
				"'Test' is not displays under 'Topic'");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Comment1_Path).trim().equalsIgnoreCase("QA Testing"),
				"'QA Test' is not displays under 'Comment' column ");
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Status1_Path).trim().equalsIgnoreCase("Acknowledged"),
				"'Acknowledged' is not displays under 'Status' column ");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Prog_Attachment1_Path),
				"'File' is not displays under 'Attachment' ");
		Click(Xpath, HU_SP_Prog_Hiddengrey1_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertFalse(Get_Text(Xpath, HU_SP_Prog_Comment1_Path).equalsIgnoreCase("QA Test"),
				"QA Testing Comments Row is not disappeared");

		Click(Xpath, HU_SP_Prog_ToggleArchivedComments_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Comment1_Path).equalsIgnoreCase("QA Testing"),
				"QA Testing Comments Row is not shown after clicked toggle comments");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Cancelbtn_Path),
				"'Cancel' button is not displays on the 'Progress/Files' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Cancelbtn_Path),
				"'Cancel' button is not displays on the 'Progress/Files' tab");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Savebtn_Path),
				"'Save' button is not displays on the 'Progress/Files' tab");
		Click(Xpath, HU_SP_Savebtn_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on Highlighted");
		softAssert.assertAll();

		Click(Xpath, HU_SP_CostsTab_Path);

		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, HU_SP_Prog_Cost_QuoteCost_Path);
		Click(Xpath, HU_SP_Prog_Cost_Billed_Path);
		Type(Xpath, HU_SP_Prog_Cost_QuoteCost_Path, "0.1");
		Type(Xpath, HU_SP_Prog_Cost_PrintCost_Path, "0.1");
		Type(Xpath, HU_SP_Prog_Cost_Fulfillment_Path, "0.1");
		Type(Xpath, HU_SP_Prog_Cost_Rush_Path, "0.1");
		Type(Xpath, HU_SP_Prog_Cost_VeritasPostage_Path, "0.1");
		Type(Xpath, HU_SP_Prog_Cost_ClientPostage_Path, "0.1");
		Click(Xpath, HU_SP_Prog_Cost_Addcosticon_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, HU_SP_Prog_Cost_Loading_Path);
		Type(Xpath, HU_SP_Prog_Cost_AddDescription_Path, "QA Test");
		Select_lidropdown(HU_SP_Prog_Cost_AddTypearrow_Path, Xpath, "Assembly");
		Type(Xpath, HU_SP_Prog_Cost_AddBilldate_Path, Get_Futuredate("MM/dd/yyyy"));
		Type(Xpath, HU_SP_Prog_Cost_AddBillamount_Path, "1.00");
		Click(Xpath, HU_SP_Prog_Cost_AddInsert_Path);
		Wait_ajax();
		Click(Xpath, HU_SP_Prog_Cost_Edit1_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_Prog_Cost_UpdateBillamount_Path, "2.00");
		Click(Xpath, HU_SP_Prog_Cost_UpdateInsert_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Wait_ajax();
		softAssert.assertTrue(Get_Text(Xpath, HU_SP_Prog_Cost_Billamount1_Path).trim().equalsIgnoreCase("$2.000"),
				" the value '$1.000' is not changes to '$2.000' under the 'Bill Amount' column in the 'Table");
		Click(Xpath, HU_SP_Prog_Cost_Delete1_Path);
		Wait_ajax();
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Prog_Cost_Deletepop_Path),
				"'Delete' pop up window is not displays");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Prog_Cost_DeleteOK_Path),
				"'OK' button is not displays on 'Delete' pop up window");
		Click(Xpath, HU_SP_Prog_Cost_DeleteOK_Path);
		Wait_ajax();
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_SuccessSavemsg_Path),
				"'Data has been saved' message is not displays above 'General' tab");

		Click(Xpath, HU_SP_InventoryTab_Path);
		Wait_ajax();
		ExplicitWait_Element_Visible(Xpath, HU_SP_Inventory_Jobtype_Path);

		Assert.assertTrue(
				Get_Text(Xpath, HU_SP_Inventory_Jobtype_Path).trim().equalsIgnoreCase("Print and Mail Request"),
				"'Print and Mail Request' displays for 'Job Type' field ");
		Assert.assertTrue(false, "Unable to Type in Form # field");

	}

	@Test(priority = 5, enabled = true)
	public void HUM_TC_2_4_1_10() throws InterruptedException, IOException {

		/*
		 * Validate the cancel functionality
		 */
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
		Type(Xpath, HU_SP_TicketNo_Path, SplprjTicketNumber);
		Click(Xpath, HU_SP_TicketNofilter_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_SP_ViewEdit1_Path);
		Switch_New_Tab();
		Click(Xpath, HU_SP_ProgramTab_Path);
		Click(Xpath, HU_SP_Prog_Addcommenticon_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Type(Xpath, HU_SP_Prog_AddComment_Path, "QA is validating cancel functionality");
		Select_lidropdown(HU_SP_Prog_AddStatusarrow_Path, Xpath, "Cancelled");
		Wait_ajax();
		Click(Xpath, HU_SP_Prog_InsertCommentbtn_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Click(Xpath, HU_SP_Savebtn_Path);
		Wait_ajax();
		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Gen_TicketNo_Path),
				"\"General\" is displays on Highlighted");
		Assert.assertTrue(Get_Text(Xpath, HU_SP_Generatab_Status_Path).trim().equalsIgnoreCase("Cancelled"),
				"Cancelled is not displayed in the Status");
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
		Click(Xpath, HU_SP_Closedck_Path);
		ExplicitWait_Element_Not_Visible(Xpath, Loading_Path);
		Assert.assertTrue(Element_Is_Displayed(Xpath, Textpath("a", SplprjTicketNumber)),
				"\"SplprjTicketNumber\" is displays on Filters");

	}

	@Test(priority = 6, enabled = true)
	public void HUM_TC_2_4_1_13() throws InterruptedException, IOException {

		/*
		 * Verify "Closed Tickets" button shows Cancelled and Closed Tickets
		 */

		Click(Xpath, Textpath("a", "Closed Tickets"));
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Cancelled")), "Cancelled is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("td", "Closed")), "Closed is not displayed");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautoadmin, qaautoadmin);
		Click(Xpath, LP_Specialproject_path);
		Wait_ajax();
	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
