package com.Humana.RegressionTest;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.Humana.Base.BaseTestHumana;

public class SuperAdminLandingPage extends BaseTestHumana {

	@Test(priority = 1, enabled = true)
	public void HUM_TC_2_0_2() throws InterruptedException {

		/*
		 * Verify menu for Super Admin user on landing page
		 * */

		Assert.assertTrue(Element_Is_Displayed(Xpath, HU_SP_Title_Path), "Project page is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, HU_LP_Home_path), "Home is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Support_path), "Home is not displayed");
		MouseHover(Xpath, LP_Support_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Humanafaq_path), "Humana FAQ is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_PHIOrderPortal_path), "PHI Order Portal is not displayed");
		MouseHover(Xpath, LP_PHIOrderPortal_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_AddPOPTicket_path), "Add POP Ticket is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Specialproject_path), "Special Project is not displayed");
		MouseHover(Xpath, LP_Specialproject_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_AddSpecialproject_path),
				"Add Special Project is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, LP_Admin_path), "Admin is not displayed");
		MouseHover(Xpath, LP_Admin_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Veritas")), "Veritas is not displayed");
		MouseHover(Xpath, Textpath("span", "Veritas"));
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Manage Dropdowns")),
				"Manage Dropdowns is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Reset Phrases & Settings")),
				"Reset Phrases & Settings is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Manage Phrases")),
				"Manage Phrases is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Manage Roles")),
				"Manage Roles is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "PHI Manage Dropdowns")),
				"PHI Manage Dropdowns is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "PHI Manage Return Address")),
				"PHI Manage Return Address is not displayed");
		MouseHover(Xpath, LP_Admin_path);
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Manage Inventory")),
				"Manage Inventory is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Manage Users")),
				"Manage Users is not displayed");
		softAssert.assertTrue(Element_Is_Displayed(Xpath, Textpath("span", "Reports")), "Reports is not displayed");
		softAssert.assertAll();

	}

	@BeforeClass
	public void beforeclass() throws IOException, InterruptedException {

		login(qaautosuper, qaautosuper);

	}

	@BeforeMethod
	public void beforeMethod() throws InterruptedException {

		softAssert = new SoftAssert();

	}

	@AfterMethod
	public void afterMethod() throws InterruptedException {

	}

	@AfterClass
	public void AfterClass() throws IOException, InterruptedException {

	}

}
