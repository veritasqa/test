package com.Humana.Methods;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.Humana.OR.SpecialProjectsPage;

public class CommonMethodsHumana extends SpecialProjectsPage {

	public static WebDriver driver;
	public static String reason;
	public static WebElement element;
	public String attrib = "";
	public String Att = "";
	public String Window_Handle = "";
	public static String OriginalWindow = "";

	public static void Open_Browser(String browser, String path) {

		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", path);
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.addArguments("--disable-web-security");
			options.addArguments("--no-proxy-server");
			options.addArguments("disable-infobars");
			options.addArguments("--disable-download-notification");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("credentials_enable_service", false);
			prefs.put("profile.password_manager_enabled", false);
			prefs.put("profile.default_content_setting_values.plugins", 1);
			prefs.put("profile.content_settings.plugin_whitelist.adobe-flash-player", 1);
			prefs.put("profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player", 1);
			options.setExperimentalOption("prefs", prefs);
			System.setProperty("webdriver.chrome.driver", path);
			driver = new ChromeDriver(options);
		} else if (browser.equalsIgnoreCase("opera")) {
			driver = new OperaDriver();
		} else if (browser.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();
		} else if (browser.equalsIgnoreCase("internetexplorer")) {
			System.setProperty("webdriver.ie.driver", path);
			driver = new InternetExplorerDriver();
		}

		else {
			reason = "Invalid Browser Input. Check Browser name";
		}

	}

	public static By locatorValue(String locatorType, String value) {

		By by;
		switch (locatorType) {
		case "id":
			by = By.id(value);
			break;
		case "name":
			by = By.name(value);
			break;
		case "xpath":
			by = By.xpath(value);
			break;
		case "cssSelector":
			by = By.cssSelector(value);
			break;
		case "linkText":
			by = By.linkText(value);
			break;
		case "partialLinkText":
			by = By.partialLinkText(value);
			break;
		case "classname":
			by = By.className(value);
			break;
		default:
			by = null;
			break;
		}
		return by;
	}

	public void Quit_Browser() {
		driver.quit();
	}

	public void Close_Browser() {

		driver.close();

	}

	public static void OpenUrl_Window_Max(String url) {

		driver.manage().window().maximize();
		driver.get(url);

	}

	public static void Get_URL(String url) {

		driver.get(url);

	}

	// waiting Methods

	public static void Implicit_Wait()

	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	public static void Wait_ajax() throws InterruptedException {
		String a = null;
		for (int i = 0; i < 80; i++) {
			a = (String) ((JavascriptExecutor) driver)
					.executeScript("var data=window.$.active; return data.toString();");
			Thread.sleep(50);
			if (!a.equals("0")) {
				a = (String) ((JavascriptExecutor) driver)
						.executeScript("var data=window.$.active; return data.toString();");

				break;

			}
		}
		int z = 0;
		while (!a.equals("0")) {
			a = (String) ((JavascriptExecutor) driver)
					.executeScript("var data=window.$.active; return data.toString()");
			Thread.sleep(300);
			z++;
			if (z > 1200) {
				break;
			}
		}
	}

	public void ExplicitWait_Element_Clickable(String locatorType, String value) {

		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			element = wait.until(ExpectedConditions.elementToBeClickable(locator));
		}

		catch (Exception e) {

			System.out.println(e.getMessage() + "Element is " + value);
			// Assert.assertTrue(false);

		}

	}

	public static void ExplicitWait_Element_Visible(String locatorType, String value) {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebDriverWait wait = new WebDriverWait(driver, 30);
			element = driver.findElement(locator);
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {

			System.out.println(e.getMessage() + "Element is " + value);
			// Assert.assertTrue(false);

		}
	}

	public void ExplicitWait_Element_Not_Visible(String locatorType, String value) {

		try {
			By locator;
			locator = locatorValue(locatorType, value);
			WebDriverWait wait = new WebDriverWait(driver, 180);
			element = driver.findElement(locator);
			wait.until(ExpectedConditions.invisibilityOf(element));
		} catch (Exception e) {

			System.out.println(e.getMessage() + "Element is " + value);
			// Assert.assertTrue(false);

		}

	}

	public void Staleness_Wait(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		element = driver.findElement(locator);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(element)));

	}

	public WebElement Element(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);

		return driver.findElement(locator);
	}

	public void Hover(String locatorType, String value) {
		By locator;
		locator = locatorValue(locatorType, value);
		Actions act = new Actions(driver);

		WebElement Admin = driver.findElement(locator);
		act.clickAndHold(Admin).build().perform();

	}

	public void MouseHover(String locatorType, String value) {
		By locator;
		locator = locatorValue(locatorType, value);
		Actions act = new Actions(driver);
		WebElement Admin = driver.findElement(locator);
		act.moveToElement(Admin).build().perform();

	}

	public void MoveandClick(String locatorType, String value) {
		By locator;
		locator = locatorValue(locatorType, value);
		Actions act = new Actions(driver);

		WebElement Admin = driver.findElement(locator);
		act.moveToElement(Admin).click().build().perform();

	}

	public void DragandDrop(String From_locatorType, String From_Element, String To_locatorType, String To_Element) {
		By locator1, locator2;
		locator1 = locatorValue(From_locatorType, From_Element);
		locator2 = locatorValue(To_locatorType, To_Element);
		Actions act = new Actions(driver);
		WebElement From = driver.findElement(locator1);
		WebElement To = driver.findElement(locator2);

		act.clickAndHold(From).moveToElement(To).release(To).build().perform();

	}

	public void Click(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Clickable(locatorType, value);
		driver.findElement(locator).click();

	}

	public void DoubleClick(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		Actions act = new Actions(driver);
		ExplicitWait_Element_Clickable(locatorType, value);
		WebElement Admin = driver.findElement(locator);
		act.doubleClick(Admin).build().perform();

	}

	public void Scroll() throws AWTException {

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		robot.keyRelease(KeyEvent.VK_PAGE_DOWN);

	}

	// Clear and type
	public void Type(String locatorType, String value, String parameter) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Visible(locatorType, value);
		driver.findElement(locator).clear();
		driver.findElement(locator).sendKeys(parameter);

	}

	public void Clear(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Visible(locatorType, value);
		driver.findElement(locator).clear();

	}

	// Get Functionalities

	public String Get_Attribute(String locatorType, String value, String parameter) {

		By locator;
		locator = locatorValue(locatorType, value);
		// driver.findElement(locator).clear();
		ExplicitWait_Element_Visible(locatorType, value);
		return driver.findElement(locator).getAttribute(parameter).trim();

	}

	public String Get_Text(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Visible(locatorType, value);
		return driver.findElement(locator).getText();

	}

	public String Get_Title() {

		return driver.getTitle();

	}

	public String Get_Current_Url() {

		return driver.getCurrentUrl();

	}

	public static String Get_Todaydate(String dateformat) {
		// driver = _driver;

		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat(dateformat);

		// get current date time with Date()
		Date date = new Date();

		// Now format the date
		return dateFormat.format(date);

	}

	public static String Get_Futuredate(String dateform) {

		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 13);
		return dateFormat.format(c.getTime());

	}

	public static String Get_Future_Nth_Date(String dateform, int Nthday) {

		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, Nthday);
		return dateFormat.format(c.getTime());

	}

	public static String Nextbusinessday(String dateform, int Nth_next_businessday) {

		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, Nth_next_businessday);
		return dateFormat.format(c.getTime());

	}

	public String Get_Nextbussinesday(String Dateformat, int Nth_day) {

		if (Get_Todaydate("EEEE") != "Friday") {

			int i = Nth_day;
			return Nextbusinessday(Dateformat, i);

		} else {
			int i = Nth_day + 3;
			return Nextbusinessday(Dateformat, i);
		}

	}

	public String Get_Pastdate(String dateform) {

		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, -3);
		return dateFormat.format(c.getTime());

	}

	public static String Get_FutureCSTTime(String dateform) {
		// driver = _driver;

		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MINUTE, -628);
		return dateFormat.format(c.getTime());

	}

	public static String Get_FutureTime(String dateform) {

		DateFormat dateFormat = new SimpleDateFormat(dateform);
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.HOUR, 1);
		return dateFormat.format(c.getTime());

	}

	// Verification Methods

	public boolean Element_Is_Enabled(String locatorType, String value) {

		try {
			By locator;
			locator = locatorValue(locatorType, value);
			return driver.findElement(locator).isEnabled();
		}

		catch (

		Exception e) {

			return false;

		}

	}

	public boolean Element_Is_Present(String locatorType, String value) {

		try {
			By locator;
			locator = locatorValue(locatorType, value);
			driver.findElement(locator);
			return true;
		} catch (Exception e) {

			return false;

		}
	}

	public boolean Element_Is_selected(String locatorType, String value) {

		try {
			By locator;
			locator = locatorValue(locatorType, value);
			return driver.findElement(locator).isSelected();
		} catch (

		Exception e) {

			return false;

		}
	}

	public boolean Element_Is_Displayed(String locatorType, String value) {

		try {
			By locator;
			locator = locatorValue(locatorType, value);
			return driver.findElement(locator).isDisplayed();
		} catch (Exception e) {

			return false;

		}

	}

	// DropDown Handling

	public void Select_DropDown(String locatorType, String value, String option) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Visible(locatorType, value);
		element = driver.findElement(locator);
		Select Select_drop = new Select(element);
		Select_drop.selectByValue(option);
		// return Text;

	}

	public boolean Select_DropDown_VisibleText(String locatorType, String value, String option)
			throws InterruptedException {

		By locator;
		locator = locatorValue(locatorType, value);
		// ExplicitWait_Element_Visible(locatorType, value, driver);
		element = driver.findElement(locator);
		Select Select_drop = new Select(element);
		Select_drop.selectByVisibleText(option);
		Thread.sleep(1000);
		return true;

	}

	public String Get_DropDown(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Visible(locatorType, value);
		element = driver.findElement(locator);
		Select Select_drop = new Select(element);
		// Select_drop.selectByValue(option);
		return Select_drop.getFirstSelectedOption().getText();

	}

	// PopUp handling

	public void SwitchtoAlert() {

		@SuppressWarnings("unused")
		Alert popup = driver.switchTo().alert();

	}

	public void Alert_Authenticate() {

		Alert popup = driver.switchTo().alert();
		UserAndPassword UP = new UserAndPassword("Yannamalai", "Sachin05");
		popup.authenticateUsing(UP);

	}

	public boolean Accept_Alert() {

		Alert popup = driver.switchTo().alert();

		popup.accept();
		driver.switchTo().defaultContent();
		return true;
	}

	public String Alert_Text() {

		Alert popup = driver.switchTo().alert();

		return popup.getText();

	}

	public boolean isAlertPresent() {

		driver.switchTo().alert();
		return true;

	}

	// New tab handling

	public void Switch_New_Tab() {

		Window_Handle = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		driver.switchTo().window(ID.get(1));

	}

	public void Switch_New_Tab2() {

		Window_Handle = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		driver.switchTo().window(ID.get(2));

	}

	public void Switch_Old_Tab() {

		Close_Browser();

		driver.switchTo().window(Window_Handle);

	}

	public void Switch_To_Default() {

		driver.switchTo().defaultContent();

	}

	public void Switch_To_Iframe(String framename) {

		driver.switchTo().frame(framename);

	}

	public void Switch_To_Iframe_byWebelement(WebElement element) {

		driver.switchTo().frame(element);

	}

	public String CurrentWindowid() {

		return driver.getWindowHandle();

	}

	public void Closealltabs() {

		OriginalWindow = driver.getWindowHandle();

		List<String> ID = new ArrayList<String>(driver.getWindowHandles());

		for (String handle : ID) {
			// System.out.println(handle);

			if (!handle.equalsIgnoreCase(OriginalWindow)) {

				driver.switchTo().window(handle);
				driver.close();

			}

		}
		driver.switchTo().window(OriginalWindow);
	}

	public String GetCSS_Backgroundcolor(String locatorType, String value) {

		By locator;
		locator = locatorValue(locatorType, value);
		ExplicitWait_Element_Visible(locatorType, value);
		return Color.fromString(driver.findElement(locator).getCssValue("background-color")).asHex();

	}

	public int RandomInt() {
		Random rand = new Random();
		return rand.nextInt(100000000);

	}

	public void NavigateBack() {

		driver.navigate().back();

	}

	public void Refresh() {

		driver.navigate().refresh();

	}

	// FileUpload

	public void Fileupload(String FilePath) throws InterruptedException, AWTException {

		StringSelection ss = new StringSelection(FilePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		Wait_ajax();
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);

	}

	public void MultiFileupload(String FilePath, int NoofTabs) throws AWTException, InterruptedException {

		StringSelection ss = new StringSelection(FilePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		Wait_ajax();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		for (int i = 1; i <= NoofTabs; i++) {
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
		}
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_A);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

	}

	public void takeScreenShot(WebDriver _driver, String methodName) {
		// get the driver
		driver = _driver;
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// The below method will save the screen shot in d drive with test
		// method name
		try {
			String filename = methodName + Get_Todaydate("dd_MM_YYY HH_mm_ss") + ".png";
			System.out.println(filename);
			File screenShotName = new File(System.getProperty("user.dir") + "\\Humana Reports\\Humana Reports "
					+ Get_Todaydate("dd_MM_YYY") + "\\Screenshots " + Get_Todaydate("dd_MM_YYY") + "\\" + filename);
			FileUtils.copyFile(scrFile, screenShotName);
			System.out.println("***Placed screen shot in " + System.getProperty("user.dir")
					+ "\\Output Test Reports\\Screenshots " + Get_Todaydate("dd_MM_YYY") + " ***");
			@SuppressWarnings("unused")
			String filePath = screenShotName.toString();
			String path = "<img src=\"./Screenshots " + Get_Todaydate("dd_MM_YYY") + "/" + filename + "\"";
			System.out.println(path);
			Reporter.log(path);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Xpath

	public String Xpath(String tagname, String Attribute, String option) {

		return ".//" + tagname + "[@" + Attribute + "='" + option + "']";

	}

	public String Wid_Xpath(String Widget, String Path) {

		return ".//*[@id='" + Get_Attribute(Xpath,
				"//*[contains(@id,'RadDock')]//*[text()='" + Widget + "']//following::tr[1]//*[@class='rdContent']",
				"Id") + Path;

	}

	public String Filteropt(String Option) {

		return ".//span[text()='" + Option + "']";

	}

	public String Pagination(String Pageno) {

		return ".//a[@class='rgCurrentPage']//span[text()='" + Pageno + "']";
	}

	public String NavigatePage(String Pageno) {

		return ".//a[@class='rgCurrentPage']//span[text()='" + Pageno + "']";
	}

	public String Textpath(String tagname, String option) {

		return ".//" + tagname + "[text()='" + option + "']";

	}

	public String TextpathContains(String tagname, String option) {

		return ".//" + tagname + "[contains(.,'" + option + "')]";

	}

	public String Containspath(String tagname, String attribute, String option) {

		return ".//" + tagname + "[contains(@" + attribute + ",'" + option + "')]";

	}

	public String Containstextpath(String tagname, String option) {

		return ".//" + tagname + "[contains(text(),'" + option + "')]";

	}

	public String li_value(String option) {

		return ".//li[text()='" + option + "']";

	}

	public void Select_lidropdown(String Inputarrow, String Inputtarrow_att, String dropdownvalue)
			throws InterruptedException {

		Click(Inputtarrow_att, Inputarrow);
		Wait_ajax();
		Click(Xpath, ".//li[text()='" + dropdownvalue + "']");
		Thread.sleep(2000);

	}

	public void NavigateMenu(String Hoverover, String Select) throws InterruptedException {

		Hover(Xpath, Hoverover);
		Click(Xpath, Select);
		Wait_ajax();

	}

	public String GreyedOutDate(String date_d) {

		return ".//td[@class='rcOutOfRange']//span[text()=" + date_d + "]";
	}

	public void Uploadfile(String AutoIT_exe, String File_Location) throws IOException, InterruptedException {

		// File location should be inside the project
		Thread.sleep(1000);
		Runtime.getRuntime().exec(
				System.getProperty("user.dir") + AutoIT_exe + " " + System.getProperty("user.dir") + File_Location);
		Thread.sleep(2000);
	}

	public void VerifyAssertli(String locatorType, String elementvalue) {

		Assert.assertTrue(Element_Is_Displayed(locatorType, li_value(elementvalue)),
				elementvalue + "- was not displayed");
	}

}
